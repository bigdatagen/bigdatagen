- Ideally, put compiled CppUnit in this directory (subdirectories).

- Do we want to commit the compiled CppUnit (for "restricted" unit test export)
|-> If so, create subdirectory for the system on which the CppUnit was
	compiled.
|-> TODO: create a CMakeLists.txt entry which chooses correct CppUnit library
	to link with unit_tests.


---------------------------------
Download and Installation (UNIX):
---------------------------------

1. Download from:
	http://linux.softpedia.com/dyn-postdownload.php/f436f03282a5ce506868a45c6d90d000/56c30027/81/0/1?tsf=0

2.	$ tar -xzvf cppunit-1.13.2.tar.gz
	$ cd cppunit-1.13.2
	$ ./configure --prefix=<install_location> # CXX=<g++-version>
		# the library needs to be compiled with same compiler as the Project
	$ make
	$ make check	# recommended
	$ make install

3. include the lib directory to the env variables or set the compiler
	parameters so the compiler knows where to look for the library
