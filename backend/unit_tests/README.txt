Common Guidelines:
==================

- unit_test directory follows the same structure as e.g. src, include
- the main test runner program is in the unit_tests directory

- use the CppUnit framework (already used in Poco, so we might as well reuse
  it) -  see http://cppunit.sourceforge.net/doc/cvs/cppunit_cookbook.html for
  some general idea

- each library/executable has its own CppUnit::TestFixture
  class implementation
- each source file has its own CppUnit::TestCase class implementation, these
  are included in the "testsuite"
