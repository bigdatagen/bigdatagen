/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Transformer
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/generators/csvschemaparser/csvgeneratorschemaparser.h"      // CsvGeneratorSchemaParser
#include "core/pipeline/plugins/generators/generator.h"                                     // Generator

#include <vector>       // std::vector
#include <iostream>     // std::cin, std::cout
#include <string>       // std::string

/**
 * Returns schemas on which we test the generator.
 *
 * @return Collection with test schemas.
 */
static std::vector<std::string> testSchemas()
{
    std::vector<std::string> jsonSchemas;

    jsonSchemas.push_back(R"(
    [
      {
        "label": "Root",
        "type": "object",
        "typeName": "ROOT",
        "properties": [
          {
            "type": "INT",
            "propertyName": "i"
          }
        ]
      },
      {
        "label": "DTS",
        "type": "sequence",
        "innerType": "datetime",
        "typeName": "DTS",
        "properties": {
          "visibility": "global",
          "step": "+1hour",
          "initialDate": "2016-05-02"
        }
      },
      {
        "label": "INT",
        "type": "sequence",
        "innerType": "int",
        "typeName": "INT",
        "properties": {
          "visibility": "global",
          "initialValues": [
            0,
            1,
            2
          ],
          "value": "$INT[-3]"
        }
      }
    ]
    )");
    
    return jsonSchemas;
}

/**
 * Starting point of the program.
 *
 * @param argc Number of command line arguments.
 * @param argv Command line arguments.
 *
 * @return Return value of the program.
 */
int main(int argc, char **argv)
{
    using namespace bdg::core;

    for (const auto &schema : testSchemas())
    {
        Generator mGen;
        CsvGeneratorSchemaParser jp1(mGen);
        mGen.setRank(0);
        jp1.parse(schema);

        const size_t fileCount = 100;
        mGen.setFilesCount(fileCount);

        Generator sGen1;
        CsvGeneratorSchemaParser jp2(sGen1);
        sGen1.setRank(1);
        jp2.parse(schema);

        sGen1.regRemInts(mGen.genRemInts());
        sGen1.setSeqsStart(mGen.genSeqsStart());
        sGen1.openStorage("output1.txt");

        for (size_t i = 0; i < fileCount; i++)
        {
            sGen1.generate();
            const_cast<GenStorer&>(sGen1.getStorer()).store('\n');
        }

        sGen1.closeStorage();

        Generator sGen2;
        CsvGeneratorSchemaParser jp3(sGen2);
        sGen2.setRank(2);
        jp3.parse(schema);
        
        sGen2.regRemInts(mGen.genRemInts());
        sGen2.setSeqsStart(mGen.genSeqsStart());
        sGen2.openStorage("output2.txt");

        for (size_t i = 0; i < fileCount; i++)
        {
            sGen2.generate();
            const_cast<GenStorer&>(sGen2.getStorer()).store('\n');
        }

        sGen2.closeStorage();        

        sGen1.regRemInts(mGen.genRemInts());
        sGen1.setSeqsStart(mGen.genSeqsStart());
        sGen1.openStorage("output1.txt");

        for (size_t i = 0; i < fileCount; i++)
        {
            sGen1.generate();
            const_cast<GenStorer&>(sGen1.getStorer()).store('\n');
        }

        sGen1.closeStorage();
    }

    // Wait for user input.
    std::cout << "Write something to exit: ";
    std::string input;
    std::cin >> input;
}
