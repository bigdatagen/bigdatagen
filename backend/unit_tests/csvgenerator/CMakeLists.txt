cmake_minimum_required (VERSION 3.0 FATAL_ERROR)

# Test program for JSON generator
project(CsvGeneratorTest CXX)

include_directories(
    ${HDR_DIR}
)

add_executable(CsvGeneratorTest
    "${TEST_DIR}/csvgenerator/test.cpp"

    "${SRC_DIR}/core/pipeline/plugins/generators/jsonschemaparser/jsonschemaparser.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/jsonschemaparser/jsonschemaparser.h"
    "${SRC_DIR}/core/pipeline/plugins/generators/csvschemaparser/csvgeneratorschemaparser.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/csvschemaparser/csvgeneratorschemaparser.h"
	"${SRC_DIR}/core/pipeline/plugins/generators/csvschemaparser/csvschemaparser.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/csvschemaparser/csvschemaparser.h"

	"${SRC_DIR}/core/pipeline/plugins/generators/csvgenerator/csvgenerator.cpp"
	"${HDR_DIR}/core/pipeline/plugins/generators/csvgenerator/csvgenerator.h"

    "${SRC_DIR}/core/pipeline/plugins/generators/probsetgenerator.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/probsetgenerator.h"
    "${SRC_DIR}/core/pipeline/plugins/generators/distributions.cpp"
	"${HDR_DIR}/core/pipeline/plugins/generators/distributions.h"

	"${SRC_DIR}/core/pipeline/plugins/generators/domtimegenerator.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/domtimegenerator.h"
	"${SRC_DIR}/core/pipeline/plugins/generators/randtimegenerator.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/randtimegenerator.h"
	"${SRC_DIR}/core/pipeline/plugins/generators/agenerator.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/agenerator.h"
	"${SRC_DIR}/core/pipeline/plugins/generators/jsonschemaelement.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/jsonschemaelement.h"
    "${SRC_DIR}/core/pipeline/plugins/generators/schemaelement.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/schemaelement.h"
    "${SRC_DIR}/core/pipeline/plugins/generators/jsontypefactory.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/jsontypefactory.h"
	"${SRC_DIR}/core/pipeline/plugins/generators/csvtypefactory.cpp"
	"${HDR_DIR}/core/pipeline/plugins/generators/csvtypefactory.h"
    "${SRC_DIR}/core/pipeline/plugins/generators/typefactory.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/typefactory.h"
    "${SRC_DIR}/core/pipeline/plugins/generators/constants.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/constants.h"
    "${SRC_DIR}/core/pipeline/plugins/generators/jsonutils.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/jsonutils.h"
    "${SRC_DIR}/core/pipeline/plugins/generators/referenceelement.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/referenceelement.h"
    "${SRC_DIR}/core/pipeline/plugins/generators/generator.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/generator.h"
    "${SRC_DIR}/core/pipeline/plugins/generators/schemaparser.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/schemaparser.h"
    "${SRC_DIR}/core/pipeline/plugins/generators/domaingenerator.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/domaingenerator.h"
    "${SRC_DIR}/core/pipeline/plugins/generators/doublegenerator.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/doublegenerator.h"
    "${SRC_DIR}/core/pipeline/plugins/generators/intgenerator.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/intgenerator.h"
    "${SRC_DIR}/core/pipeline/plugins/generators/nonterminal.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/nonterminal.h"
    "${SRC_DIR}/core/pipeline/plugins/generators/complexgenerator.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/complexgenerator.h"
	"${SRC_DIR}/core/pipeline/plugins/generators/cycle.cpp"
	"${HDR_DIR}/core/pipeline/plugins/generators/cycle.h"
    "${SRC_DIR}/core/pipeline/plugins/generators/arraygenerator.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/arraygenerator.h"
    "${SRC_DIR}/core/pipeline/plugins/generators/grammarelement.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/grammarelement.h"
    "${SRC_DIR}/core/pipeline/plugins/generators/runtimeterminal.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/runtimeterminal.h"
    "${SRC_DIR}/core/pipeline/plugins/generators/stringgenerator.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/stringgenerator.h"
    "${SRC_DIR}/core/pipeline/plugins/generators/terminal.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/terminal.h"
    "${SRC_DIR}/core/pipeline/plugins/generators/elementfactory.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/elementfactory.h"
    "${SRC_DIR}/core/pipeline/plugins/generators/sequencegenerator.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/sequencegenerator.h"
    "${SRC_DIR}/core/pipeline/plugins/generators/timesequencegenerator.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/timesequencegenerator.h"
    "${SRC_DIR}/core/pipeline/plugins/generators/exception.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/exception.h"
    "${SRC_DIR}/core/pipeline/plugins/generators/historystorage.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/historystorage.h"
    "${SRC_DIR}/core/pipeline/plugins/generators/elemgen.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/elemgen.h"
    "${SRC_DIR}/core/pipeline/plugins/generators/dyngenerator.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/dyngenerator.h"
    "${SRC_DIR}/core/pipeline/plugins/generators/token.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/token.h"
    "${SRC_DIR}/core/pipeline/plugins/generators/genstorer.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/genstorer.h"
    "${SRC_DIR}/core/pipeline/plugins/generators/remotegenerator.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generators/remotegenerator.h"
    "${SRC_DIR}/utils/convert.cpp"
    "${HDR_DIR}/utils/convert.h"
)

target_link_libraries(CsvGeneratorTest
    Foundation
    JSON
    BdgPluginApi
)
