/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#include "doublegeneratortest.h"                    // Header
#include "../../../include/utils/exception.h"       // Exception

#include <iostream>     // std::cout
#include <cstdlib>      // std::cstdlib
#include <limits>       // std::limits

// Registers the fixture into the 'registry'.
CPPUNIT_TEST_SUITE_REGISTRATION(DoubleGeneratorTest);

void DoubleGeneratorTest::testConstructor()
{
    double minVal = 1.1;
    double maxVal = 100.1;

    for (auto &distr : supportedDistr)
    {
        CPPUNIT_ASSERT_NO_THROW_MESSAGE(distr + " - Init", gen = gen_ptr_t(new bdg::core::DoubleGenerator(minVal, maxVal, distr)));        
    }
}

void DoubleGeneratorTest::testGenerate()
{
    for (auto &distr : supportedDistr)
    {
        // Nonnegative interval.
        double minVal = 0;
        double maxVal = 10.1;
        double res;

        CPPUNIT_ASSERT_NO_THROW_MESSAGE(distr + " - Init" + distr, gen = gen_ptr_t(new bdg::core::DoubleGenerator(minVal, maxVal, distr)));
        
        for (int i = 0; i < numOfIter; i++)
        {
            CPPUNIT_ASSERT_NO_THROW_MESSAGE(distr + " - Generate nonnegative" + distr, res = gen->generateValue());
            CPPUNIT_ASSERT_MESSAGE(reportIntervalMsg(distr, minVal, maxVal, res), minVal <= res && res <= maxVal);
        }
        
        // Nonpositive interval.
        minVal = -100.0;
        maxVal = 0;
        
        CPPUNIT_ASSERT_NO_THROW_MESSAGE(distr + " - Init" + distr, gen = gen_ptr_t(new bdg::core::DoubleGenerator(minVal, maxVal, distr)));
        
        for (int i = 0; i < numOfIter; i++)
        {
            CPPUNIT_ASSERT_NO_THROW_MESSAGE(distr + " - Generate nonpositive" + distr, res = gen->generateValue());
            CPPUNIT_ASSERT_MESSAGE(reportIntervalMsg(distr, minVal, maxVal, res), minVal <= res && res <= maxVal);
        }

        // Test of "neg-pos" interval.
        minVal = -100.1;
        maxVal = 100.1;

        CPPUNIT_ASSERT_NO_THROW_MESSAGE(distr + " - Init" + distr, gen = gen_ptr_t(new bdg::core::DoubleGenerator(minVal, maxVal, distr)));
        
        for (int i = 0; i < numOfIter; i++)
        { 
            CPPUNIT_ASSERT_NO_THROW_MESSAGE(distr + " - Generate neg-pos" + distr, res = gen->generateValue());
            CPPUNIT_ASSERT_MESSAGE(reportIntervalMsg(distr, minVal, maxVal, res), minVal <= res && res <= maxVal);
        }

        // Zero size interval.
        minVal = 10.1;
        maxVal = 10.1;

        CPPUNIT_ASSERT_NO_THROW_MESSAGE(distr + " - Init" + distr, gen = gen_ptr_t(new bdg::core::DoubleGenerator(minVal, maxVal, distr)));
        
        for (int i = 0; i < numOfIter; i++)
        {
            CPPUNIT_ASSERT_NO_THROW_MESSAGE(distr + " - Generate zero size interval" + distr, res = gen->generateValue());
            CPPUNIT_ASSERT_MESSAGE(reportIntervalMsg(distr, minVal, maxVal, res), minVal <= res && res <= maxVal);
        }

        // We do not test the following for these distributions due to long computation time.
        if (distr == "binomial" || distr == "geometric")
        {
            continue;
        }

        // Max integer interval.
        minVal = std::numeric_limits<double>::min();
        maxVal = std::numeric_limits<double>::max();

        CPPUNIT_ASSERT_NO_THROW_MESSAGE(distr + " - Init" + distr, gen = gen_ptr_t(new bdg::core::DoubleGenerator(minVal, maxVal, distr)));
        
        for (int i = 0; i < numOfIter; i++)
        {
            CPPUNIT_ASSERT_NO_THROW_MESSAGE(distr + " - Generate maximum interval" + distr, res = gen->generateValue());
            CPPUNIT_ASSERT_MESSAGE(reportIntervalMsg(distr, minVal, maxVal, res), minVal <= res && res <= maxVal);
        }
    }
}

void DoubleGeneratorTest::testUnsupportedDistr()
{
    std::string distr = "unsupDistr";
    CPPUNIT_ASSERT_THROW(gen_ptr_t(new bdg::core::DoubleGenerator(0.0, 10.0, distr)), bdg::utils::UnsupportedDistributionException);
}

void DoubleGeneratorTest::testWrongBoundaries()
{
    CPPUNIT_ASSERT_THROW(gen_ptr_t(new bdg::core::DoubleGenerator(10.0, 1.0, "uniform")), bdg::utils::BadBoundariesException);
}

void DoubleGeneratorTest::setUp()
{
    supportedDistr.push_back("uniform");
    supportedDistr.push_back("normal");
    supportedDistr.push_back("exponential");
}

void DoubleGeneratorTest::tearDown()
{
    gen.reset();
}

std::string DoubleGeneratorTest::reportIntervalMsg(std::string &distr, double minVal, double maxVal, double res)
{
    std::ostringstream oss;
    oss << distr << "Result:" << res << " Interval:[" << minVal << "," << maxVal << "]";

    return oss.str();
}
