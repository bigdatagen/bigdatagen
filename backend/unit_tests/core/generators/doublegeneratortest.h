/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#pragma once

#include "../../../include/core/pipeline/plugins/generators/doublegenerator.h"      // DoubleGenerator

#include <cppunit/extensions/HelperMacros.h>        // HelperMacros

#include <string>       // std::string
#include <vector>       // std::vector
#include <memory>       // std::unique_ptr

class DoubleGeneratorTest : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(DoubleGeneratorTest);
    CPPUNIT_TEST(testConstructor);
    CPPUNIT_TEST(testGenerate);

    CPPUNIT_TEST(testUnsupportedDistr);
    CPPUNIT_TEST(testWrongBoundaries);
    CPPUNIT_TEST_SUITE_END();

public:
    DoubleGeneratorTest() = default;
    ~DoubleGeneratorTest() = default;

    // Testcases - positive scenarios.
    void testConstructor();
    void testGenerate();
    
    // Testcases - exception handling.
    void testUnsupportedDistr();
    void testWrongBoundaries();

    // Initialization.
    void setUp();

    // Clean Up.
    void tearDown();

private:
    using gen_ptr_t = std::unique_ptr<bdg::core::DoubleGenerator>;

    std::string reportIntervalMsg(std::string &distr, double minVal, double maxVal, double res);

    const int numOfIter = 100;
    std::vector<std::string> supportedDistr;
    gen_ptr_t gen;
};
