/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#include "jsongeneratortest.h"      // Header

#include <Poco/JSON/Object.h>       // Poco::JSON::Object
#include <Poco/JSON/Parser.h>       // Poco::JSON::Parser
#include <Poco/Dynamic/Var.h>       // Poco::Dynamic::Var
#include <Poco/Exception.h>         // Poco::Exception

#include <iostream>     // std::cout

// Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION(JsonGeneratorTest);

void JsonGeneratorTest::testGenerateInt()
{
    // Nothing here.
}

void JsonGeneratorTest::setUp()
{
    // Nothing here.
}

void JsonGeneratorTest::tearDown()
{
    // Nothing here.
}
