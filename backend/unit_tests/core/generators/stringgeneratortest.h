/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#pragma once

#include "../../../include/core/pipeline/plugins/generators/stringgenerator.h"      // StringGenerator

#include <cppunit/extensions/HelperMacros.h>        // HelperMacros

#include <string>       // std::string
#include <vector>       // std::vector
#include <memory>       // std::unique_ptr

class StringGeneratorTest : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(StringGeneratorTest);
    CPPUNIT_TEST(testConstructor);
    CPPUNIT_TEST(testGenerate);

    CPPUNIT_TEST(testUnsupportedDistr);
    CPPUNIT_TEST(testWrongBoundaries);
    CPPUNIT_TEST_SUITE_END();

public:
    StringGeneratorTest() = default;
    ~StringGeneratorTest() = default;

    // Testcases - positive scenarios.
    void testConstructor();
    void testGenerate();
    
    // Testcases - exception handling.
    void testUnsupportedDistr();
    void testWrongBoundaries();

    // Initialization.
    void setUp();

    // Clean Up.
    void tearDown();

private:
    using gen_ptr_t = std::unique_ptr<bdg::core::StringGenerator>;

    std::string reportIntervalMsg(std::string &distr, int minVal, int maxVal, int res);
    int lengthNoQuotes(std::string &str);

    const int numOfIter = 100;
    std::vector<std::string> supportedDistr;
    gen_ptr_t gen;
};
