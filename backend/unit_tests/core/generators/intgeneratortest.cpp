/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#include "intgeneratortest.h"                       // Header
#include "../../../include/utils/exception.h"       // Exception

#include <iostream>     // std::cout
#include <cstdlib>      // std::rand
#include <limits>       // std::limits

// Registers the fixture into the 'registry'.
CPPUNIT_TEST_SUITE_REGISTRATION(IntGeneratorTest);

void IntGeneratorTest::testConstructor()
{
    int minVal = 1;
    int maxVal = 100;

    for (auto &distr : supportedDistr)
    {
        CPPUNIT_ASSERT_NO_THROW_MESSAGE(distr + " - Init", gen = gen_ptr_t(new bdg::core::IntGenerator(minVal, maxVal, distr)));        
        CPPUNIT_ASSERT_EQUAL_MESSAGE(distr + " - getMin", minVal, gen->getMin());
        CPPUNIT_ASSERT_EQUAL_MESSAGE(distr + " - getMax", maxVal, gen->getMax());
    }
}    

void IntGeneratorTest::testGenerate()
{
    for (auto &distr : supportedDistr)
    {
        // Nonnegative interval.
        int minVal = 0;
        int maxVal = 10;
        int res;

        CPPUNIT_ASSERT_NO_THROW_MESSAGE(distr + " - Init" + distr, gen = gen_ptr_t(new bdg::core::IntGenerator(minVal, maxVal, distr)));
        
        for (int i = 0; i < numOfIter; i++)
        {
            CPPUNIT_ASSERT_NO_THROW_MESSAGE(distr + " - Generate nonnegative" + distr, res = gen->generateValue());
            CPPUNIT_ASSERT_MESSAGE(reportIntervalMsg(distr, minVal, maxVal, res), minVal <= res && res <= maxVal);
        }
        
        // Nonpositive interval.
        minVal = -100;
        maxVal = 0;
        
        CPPUNIT_ASSERT_NO_THROW_MESSAGE(distr + " - Init" + distr, gen = gen_ptr_t(new bdg::core::IntGenerator(minVal, maxVal, distr)));
        
        for (int i = 0; i < numOfIter; i++)
        {
            CPPUNIT_ASSERT_NO_THROW_MESSAGE(distr + " - Generate nonpositive" + distr, res = gen->generateValue());
            CPPUNIT_ASSERT_MESSAGE(reportIntervalMsg(distr, minVal, maxVal, res), minVal <= res && res <= maxVal);
        }

        // Test of "neg-pos" interval.
        minVal = -100;
        maxVal = 100;

        CPPUNIT_ASSERT_NO_THROW_MESSAGE(distr + " - Init" + distr, gen = gen_ptr_t(new bdg::core::IntGenerator(minVal, maxVal, distr)));
        
        for (int i = 0; i < numOfIter; i++)
        { 
            CPPUNIT_ASSERT_NO_THROW_MESSAGE(distr + " - Generate neg-pos" + distr, res = gen->generateValue());
            CPPUNIT_ASSERT_MESSAGE(reportIntervalMsg(distr, minVal, maxVal, res), minVal <= res && res <= maxVal);
        }

        // Zero size interval.
        minVal = 10;
        maxVal = 10;

        CPPUNIT_ASSERT_NO_THROW_MESSAGE(distr + " - Init" + distr, gen = gen_ptr_t(new bdg::core::IntGenerator(minVal, maxVal, distr)));
        
        for (int i = 0; i < numOfIter; i++)
        {
            CPPUNIT_ASSERT_NO_THROW_MESSAGE(distr + " - Generate zero size interval" + distr, res = gen->generateValue());
            CPPUNIT_ASSERT_MESSAGE(reportIntervalMsg(distr, minVal, maxVal, res), minVal <= res && res <= maxVal);
        }

        // We do not test the following for these distributions due to long computation time.
        if (distr == "binomial" || distr == "geometric")
        {
            continue;
        }

        // Max integer interval.
        minVal = std::numeric_limits<int>::min();
        maxVal = std::numeric_limits<int>::max();

        CPPUNIT_ASSERT_NO_THROW_MESSAGE(distr + " - Init" + distr, gen = gen_ptr_t(new bdg::core::IntGenerator(minVal, maxVal, distr)));
        
        for (int i = 0; i < numOfIter; i++)
        {
            CPPUNIT_ASSERT_NO_THROW_MESSAGE(distr + " - Generate maximum interval" + distr, res = gen->generateValue());
            CPPUNIT_ASSERT_MESSAGE(reportIntervalMsg(distr, minVal, maxVal, res), minVal <= res && res <= maxVal);
        }
    }
}

void IntGeneratorTest::testUnsupportedDistr()
{
    std::string distr = "unsupDistr";
    CPPUNIT_ASSERT_THROW(gen_ptr_t(new bdg::core::IntGenerator(0, 10, distr)), bdg::utils::UnsupportedDistributionException);
}

void IntGeneratorTest::testWrongBoundaries()
{
    CPPUNIT_ASSERT_THROW(gen_ptr_t(new bdg::core::IntGenerator(10, 1, "uniform")), bdg::utils::BadBoundariesException);
}

void IntGeneratorTest::setUp()
{
    supportedDistr.push_back("uniform");
    supportedDistr.push_back("normal");
    supportedDistr.push_back("poisson");
    supportedDistr.push_back("exponential");
    supportedDistr.push_back("binomial");
    supportedDistr.push_back("geometric");
}

void IntGeneratorTest::tearDown()
{
    gen.reset();
}

std::string IntGeneratorTest::reportIntervalMsg(std::string &distr, int minVal, int maxVal, int res)
{
    std::ostringstream oss;
    oss << distr << "Result:" << res << " Interval:[" << minVal << "," << maxVal << "]";

    return oss.str();
}
