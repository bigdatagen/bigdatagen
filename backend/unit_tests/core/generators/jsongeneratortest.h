/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#pragma once

#include "../../../include/core/pipeline/plugins/generators/jsongenerator/jsongenerator.h"      // JsonGenerator

#include <cppunit/extensions/HelperMacros.h>        // CPP unit macros

#include <string>       // std::string
#include <vector>       // std::vector

class JsonGeneratorTest : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(JsonGeneratorTest);
    CPPUNIT_TEST(testGenerateInt);
    CPPUNIT_TEST_SUITE_END();

public:
    JsonGeneratorTest() = default;
    ~JsonGeneratorTest() = default;

    // Test cases.
    void testGenerateInt();

    // Initialization.
    void setUp();

    // Clean Up.
    void tearDown();

private:
    bdg::core::pipeline::plugin::JsonGenerator generator;
};
