/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#include "stringgeneratortest.h"                    // Header
#include "../../../include/utils/exception.h"       // Exception

#include <iostream>     // std::cout
#include <cstdlib>      // std::rand
#include <limits>       // std::limits

// Registers the fixture into the 'registry'.
CPPUNIT_TEST_SUITE_REGISTRATION(StringGeneratorTest);

void StringGeneratorTest::testConstructor()
{
    int minVal = 1;
    int maxVal = 10;

    for (auto &distr : supportedDistr)
    {
        CPPUNIT_ASSERT_NO_THROW_MESSAGE(distr + " - Init", gen = gen_ptr_t(new bdg::core::StringGenerator(minVal, maxVal, distr)));        
    }
}    

void StringGeneratorTest::testGenerate()
{
    for (auto &distr : supportedDistr)
    {
        // Basic string.
        int minVal = 0;
        int maxVal = 20;
        std::string res;

        CPPUNIT_ASSERT_NO_THROW_MESSAGE(distr + " - Init" + distr, gen = gen_ptr_t(new bdg::core::StringGenerator(minVal, maxVal, distr)));
        
        for (int i = 0; i < numOfIter; i++)
        {
            CPPUNIT_ASSERT_NO_THROW_MESSAGE(distr + " - Generate" + distr, res = gen->generateValue());
            CPPUNIT_ASSERT_MESSAGE(reportIntervalMsg(distr, minVal, maxVal, lengthNoQuotes(res)), minVal <= lengthNoQuotes(res) && lengthNoQuotes(res) <= maxVal);
        }

        // Zero size interval.
        minVal = 10;
        maxVal = 10;

        CPPUNIT_ASSERT_NO_THROW_MESSAGE(distr + " - Init" + distr, gen = gen_ptr_t(new bdg::core::StringGenerator(minVal, maxVal, distr)));
        
        for (int i = 0; i < numOfIter; i++)
        {
            CPPUNIT_ASSERT_NO_THROW_MESSAGE(distr + " - Generate fixed length" + distr, res = gen->generateValue());
            CPPUNIT_ASSERT_MESSAGE(reportIntervalMsg(distr, minVal, maxVal, lengthNoQuotes(res)), minVal <= lengthNoQuotes(res) && lengthNoQuotes(res) <= maxVal);
        }

        // Zero size string.
        minVal = 0;
        maxVal = 0;

        CPPUNIT_ASSERT_NO_THROW_MESSAGE(distr + " - Init" + distr, gen = gen_ptr_t(new bdg::core::StringGenerator(minVal, maxVal, distr)));
        
        for (int i = 0; i < numOfIter; i++)
        {
            CPPUNIT_ASSERT_NO_THROW_MESSAGE(distr + " - Generate empty strings" + distr, res = gen->generateValue());
            CPPUNIT_ASSERT_MESSAGE(reportIntervalMsg(distr, minVal, maxVal, lengthNoQuotes(res)), minVal <= lengthNoQuotes(res) && lengthNoQuotes(res) <= maxVal);
        }
    }
}

void StringGeneratorTest::testUnsupportedDistr()
{
    std::string distr = "unsupDistr";
    CPPUNIT_ASSERT_THROW(gen_ptr_t(new bdg::core::StringGenerator(0, 10, distr)), bdg::utils::UnsupportedDistributionException);
}

void StringGeneratorTest::testWrongBoundaries()
{
    CPPUNIT_ASSERT_THROW_MESSAGE("Min > Max", gen_ptr_t(new bdg::core::StringGenerator(10, 1, "uniform")), bdg::utils::BadBoundariesException);
    CPPUNIT_ASSERT_THROW_MESSAGE("Negative boundary parameter", gen_ptr_t(new bdg::core::StringGenerator(-1, 1, "uniform")), bdg::utils::BadBoundariesException);
}

void StringGeneratorTest::setUp()
{
    supportedDistr.push_back("uniform");
    supportedDistr.push_back("normal");
    supportedDistr.push_back("poisson");
    supportedDistr.push_back("exponential");
    supportedDistr.push_back("binomial");
    supportedDistr.push_back("geometric");
}

void StringGeneratorTest::tearDown()
{
    gen.reset();
}

std::string StringGeneratorTest::reportIntervalMsg(std::string &distr, int minVal, int maxVal, int res)
{
    std::ostringstream oss;
    oss << distr << "Generated string size:" << res << " Interval:[" << minVal << "," << maxVal << "]";

    return oss.str();
}

int StringGeneratorTest::lengthNoQuotes(std::string &str)
{
    return str.length() - 2;
}
