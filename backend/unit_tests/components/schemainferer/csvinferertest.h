/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#pragma once

#include <components/schemainferer/csvinferer/csvinferer.h>     // CsvInferer

#include <cppunit/extensions/HelperMacros.h>        // CPP unit macros

#include <string>       // std::string
#include <sstream>      // std::stringstream

using namespace bdg::components::schemaInferer;

class CsvInfererTest : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(CsvInfererTest);
    CPPUNIT_TEST(testConstructor);
    CPPUNIT_TEST(testInferNoFiles);
    CPPUNIT_TEST(testInferFileNotFound);
    CPPUNIT_TEST(testInferInvalid);
    CPPUNIT_TEST(testSeparators);
    CPPUNIT_TEST(testInferEmpty);
    CPPUNIT_TEST(testInferInteger);
    CPPUNIT_TEST(testInferFloat);
    CPPUNIT_TEST(testInferString);
    CPPUNIT_TEST(testMergeTwoFiles);
    CPPUNIT_TEST_SUITE_END();

public:
    CsvInfererTest() = default;
    ~CsvInfererTest() = default;

    // Test cases.
    void testConstructor();
    void testInferNoFiles();
    void testInferFileNotFound();
    void testInferInvalid();

    void testSeparators();
    void testInferEmpty();
    void testInferInteger();
    void testInferFloat();
    void testInferString();
    void testMixedColumns();
    void testMergeTwoFiles();
    void testMergeTwoFilesFail(); 

    void setUp();
    void tearDown();

private:
    Poco::JSON::Object::Ptr pluginParameters;
    CsvInferer inferer;

    void checkCorrectOutput(std::string inputFile, std::string expectedFile);
    void checkCorrectOutput(std::string inputFile1, std::string inputFile2, std::string expectedFile);
};
