/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#pragma once

#include <components/schemainferer/jsoninferer/jsoninferer.h>       // JsonInferer

#include <cppunit/extensions/HelperMacros.h>        // CPP unit macros

#include <string>       // std::string
#include <sstream>      // std::stringstream

using namespace bdg::components::schemaInferer;

class JsonInfererTest : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(JsonInfererTest);
    CPPUNIT_TEST(testConstructor);
    CPPUNIT_TEST(testAddFile);
    CPPUNIT_TEST(testAddZippedFile);
    CPPUNIT_TEST(testAddDirectory);
    CPPUNIT_TEST(testInferNoFiles);
    CPPUNIT_TEST(testInferFileNotFound);
    CPPUNIT_TEST(testInferInvalid);
    CPPUNIT_TEST(testInferEmpty);
    CPPUNIT_TEST(testInferInteger);
    CPPUNIT_TEST(testInferFloat);
    CPPUNIT_TEST(testInferString);
    CPPUNIT_TEST(testInferObject);
    CPPUNIT_TEST(testInferHomogenousArray);
    CPPUNIT_TEST(testInferHeterogenousArray);
    CPPUNIT_TEST(testMergeTwoFiles);
    CPPUNIT_TEST(testMergeIncompatibleRoot);
    CPPUNIT_TEST_SUITE_END();

public:
    JsonInfererTest() = default;
    ~JsonInfererTest() = default;

    // Test cases.
    void testConstructor();
    void testAddFile();
    void testAddZippedFile();
    void testAddDirectory();
    void testInferNoFiles();
    void testInferFileNotFound();
    void testInferInvalid();

    void testInferEmpty();
    void testInferInteger();
    void testInferFloat();
    void testInferString();
    void testInferObject();
    void testInferHomogenousArray();
    void testInferHeterogenousArray();
    void testMergeTwoFiles();
    void testMergeIncompatibleRoot();

    void setUp();
    void tearDown();

private:
    Poco::JSON::Object::Ptr pluginParameters;
    JsonInferer inferer;

    void checkCorrectOutput(std::string inputFile, std::string expectedFile);
    void checkCorrectOutput(std::string inputFile1, std::string inputFile2, std::string expectedFile);
};
