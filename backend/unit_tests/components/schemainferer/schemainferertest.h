/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#pragma once

#include "../../../include/components/schemainferer/schemainferer.h"        // SchemaInferer

#include <string>       // std::string
#include <vector>       // std::vector

#include <cppunit/extensions/HelperMacros.h>

class SchemaInfererTest : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(SchemaInfererTest);
    CPPUNIT_TEST(testRunJSONInferer);
    CPPUNIT_TEST(testNoFiles);
    CPPUNIT_TEST(testListInferers);
    CPPUNIT_TEST_SUITE_END();

public:
    SchemaInfererTest() = default;
    ~SchemaInfererTest() = default;

    // Test cases.
    void testRunJSONInferer();
    void testNoFiles();
    void testListInferers();

    // Initialization.
    void setUp();

    // Clean Up.
    void tearDown();

private:
    bdg::components::SchemaInferer inferer;
    bdg::server::ServerPlugin::Parameters pluginParameters;

    std::vector<std::string> supportedInferers;

    std::string inputFile;
    std::string expectedFile;
};
