/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#include "csvinferertest.h"             // Header

#include <Poco/JSON/Object.h>           // Poco::JSON::Object
#include <Poco/JSON/Parser.h>           // Poco::JSON::Parser
#include <Poco/Dynamic/Var.h>           // Poco::Dynamic::Var
#include <Poco/Exception.h>             // Poco::Exception
#include <Poco/JSON/JSONException.h>    // Poco::JSON::JSONException

// Registers the fixture into the 'registry'.
CPPUNIT_TEST_SUITE_REGISTRATION(CsvInfererTest);

void CsvInfererTest::testConstructor()
{
    CsvInferer inferer;
    std::size_t expected = 0;
    CPPUNIT_ASSERT_EQUAL(expected, inferer.getFilenames().size());
}

void CsvInfererTest::testInferNoFiles()
{
    std::ostringstream output;
    CPPUNIT_ASSERT_THROW(inferer.inferSchema(output, pluginParameters), Poco::InvalidArgumentException);
}

void CsvInfererTest::testInferFileNotFound()
{
    CPPUNIT_ASSERT_NO_THROW(inferer.addFile("nonExistent.json"));
    std::ostringstream output;
    CPPUNIT_ASSERT_THROW(inferer.inferSchema(output, pluginParameters), Poco::OpenFileException);
}

void CsvInfererTest::testInferInvalid()
{
    CPPUNIT_ASSERT_NO_THROW(inferer.addFile("data/invalid.csv"));
    std::ostringstream output;
    CPPUNIT_ASSERT_THROW(inferer.inferSchema(output, pluginParameters), Poco::RuntimeException);

    // TODO: more examples of invalid Csvs?
}

void CsvInfererTest::testSeparators()
{
    CPPUNIT_ASSERT_NO_THROW(inferer.addFile("data/separators.csv"));
    std::stringstream output;

    // Default separator - ",".
    std::size_t expectedSize = 6;
    CPPUNIT_ASSERT_NO_THROW(inferer.inferSchema(output, pluginParameters));
    Poco::JSON::Object::Ptr schema = Poco::JSON::Parser().parse(output).extract<Poco::JSON::Array::Ptr>()->getObject(0);
    CPPUNIT_ASSERT_EQUAL(expectedSize, schema->getArray("columns")->size());
    output.clear();

    pluginParameters->set("separator", "|");
    expectedSize = 5;
    CPPUNIT_ASSERT_NO_THROW(inferer.inferSchema(output, pluginParameters));
    schema = Poco::JSON::Parser().parse(output).extract<Poco::JSON::Array::Ptr>()->getObject(0);
    CPPUNIT_ASSERT_EQUAL(expectedSize, schema->getArray("columns")->size());
    output.clear();

    pluginParameters = new Poco::JSON::Object;
    pluginParameters->set("separator", ";");
    expectedSize = 4;
    CPPUNIT_ASSERT_NO_THROW(inferer.inferSchema(output, pluginParameters));
    schema = Poco::JSON::Parser().parse(output).extract<Poco::JSON::Array::Ptr>()->getObject(0);
    CPPUNIT_ASSERT_EQUAL(expectedSize, schema->getArray("columns")->size());
}

void CsvInfererTest::testInferEmpty()
{
    checkCorrectOutput("data/empty.csv", "data/empty_result.csv");
}

void CsvInfererTest::testInferInteger()
{
    checkCorrectOutput("data/integer.csv", "data/integer_result.csv");
}

void CsvInfererTest::testInferFloat()
{
    checkCorrectOutput("data/float.csv", "data/float_result.csv");
}

void CsvInfererTest::testInferString()
{
    checkCorrectOutput("data/string.csv", "data/string_result.csv");
}

void CsvInfererTest::testMixedColumns()
{
    checkCorrectOutput("data/mixed.csv", "data/mixed_result.csv");
}

void CsvInfererTest::testMergeTwoFiles()
{
    std::string file1 = "data/merge1.csv";
    std::string file2 = "data/merge2.csv";
    std::string fileResult = "data/merge_result.csv";

    checkCorrectOutput(file1, file2, fileResult);
}

void CsvInfererTest::testMergeTwoFilesFail()
{
    CPPUNIT_ASSERT_NO_THROW(inferer.addFile("data/merge_fail1.csv"));
    CPPUNIT_ASSERT_NO_THROW(inferer.addFile("data/merge_fail2.csv"));

    std::ostringstream output;
    CPPUNIT_ASSERT_THROW(inferer.inferSchema(output, pluginParameters), Poco::InvalidArgumentException);
}

void CsvInfererTest::setUp()
{
    inferer.clear();
    pluginParameters = new Poco::JSON::Object;
}

void CsvInfererTest::tearDown()
{
    // Nothing here.
}

void CsvInfererTest::checkCorrectOutput(std::string inputFile, std::string expectedFile)
{
    CPPUNIT_ASSERT_NO_THROW(inferer.addFile(inputFile));
    
    std::stringstream output;
    std::ifstream expected(expectedFile);
    
    CPPUNIT_ASSERT_NO_THROW(inferer.inferSchema(output, pluginParameters));
    
    Poco::Dynamic::Var outputParsed = Poco::JSON::Parser().parse(output);
    Poco::Dynamic::Var expectedParsed = Poco::JSON::Parser().parse(expected);
    
    CPPUNIT_ASSERT_EQUAL(expectedParsed.toString(), outputParsed.toString());
}

void CsvInfererTest::checkCorrectOutput(std::string inputFile1, std::string inputFile2, std::string expectedFile)
{
    CPPUNIT_ASSERT_NO_THROW(inferer.addFile(inputFile1));
    checkCorrectOutput(inputFile2, expectedFile);
}
