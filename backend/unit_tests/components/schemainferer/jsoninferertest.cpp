/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#include "jsoninferertest.h"            // Header

#include <Poco/JSON/Object.h>           // Poco::JSON::Object
#include <Poco/JSON/Parser.h>           // Poco::JSON::Parser
#include <Poco/Dynamic/Var.h>           // Poco::Dynamic::Var
#include <Poco/Exception.h>             // Poco::Exception
#include <Poco/JSON/JSONException.h>    // Poco::JSON::JSONException

// Registers the fixture into the 'registry'.
CPPUNIT_TEST_SUITE_REGISTRATION(JsonInfererTest);

void JsonInfererTest::testConstructor()
{
    JsonInferer inferer;
    std::size_t expected = 0;
    CPPUNIT_ASSERT_EQUAL(expected, inferer.getFilenames().size());
}

void JsonInfererTest::testAddFile()
{
    CPPUNIT_ASSERT_NO_THROW(inferer.addFile("nonExistent.json"));
    std::size_t expected = 1;
    CPPUNIT_ASSERT_EQUAL(expected, inferer.getFilenames().size());
}

void JsonInfererTest::testAddZippedFile()
{
    CPPUNIT_ASSERT_NO_THROW(inferer.addFile("data/zipped.zip"));
    std::size_t expected = 3;
    CPPUNIT_ASSERT_EQUAL(expected, inferer.getFilenames().size());
}

void JsonInfererTest::testAddDirectory()
{
    //CPPUNIT_FAIL("Not Implemented");
}

void JsonInfererTest::testInferNoFiles()
{
    std::ostringstream output;
    CPPUNIT_ASSERT_THROW(inferer.inferSchema(output, pluginParameters), Poco::InvalidArgumentException);
}

void JsonInfererTest::testInferFileNotFound()
{
    CPPUNIT_ASSERT_NO_THROW(inferer.addFile("nonExistent.json"));
    std::ostringstream output;
    CPPUNIT_ASSERT_THROW(inferer.inferSchema(output, pluginParameters), Poco::OpenFileException);
}

void JsonInfererTest::testInferInvalid()
{
    CPPUNIT_ASSERT_NO_THROW(inferer.addFile("data/invalid.json"));
    std::ostringstream output;
    CPPUNIT_ASSERT_THROW(inferer.inferSchema(output, pluginParameters), Poco::RuntimeException);

    // TODO: more examples of invalid Jsons?
}

void JsonInfererTest::testInferEmpty()
{
    checkCorrectOutput("data/empty.json", "data/empty_result.json");
}

void JsonInfererTest::testInferInteger()
{
    checkCorrectOutput("data/integer.json", "data/integer_result.json");
}

void JsonInfererTest::testInferFloat()
{
    checkCorrectOutput("data/float.json", "data/float_result.json");
}

void JsonInfererTest::testInferString()
{
    checkCorrectOutput("data/string.json", "data/string_result.json");
}

void JsonInfererTest::testInferObject()
{
    checkCorrectOutput("data/object.json", "data/object_result.json");
}

void JsonInfererTest::testInferHomogenousArray()
{
    checkCorrectOutput("data/array1.json", "data/array1_result.json");
}

void JsonInfererTest::testInferHeterogenousArray()
{
    checkCorrectOutput("data/array2.json", "data/array2_result.json");
}

void JsonInfererTest::testMergeTwoFiles()
{
    std::string file1 = "data/merge1.json";
    std::string file2 = "data/merge2.json";
    std::string fileResult = "data/merge_result.json";

    checkCorrectOutput(file1, file2, fileResult);
}

void JsonInfererTest::testMergeIncompatibleRoot()
{
    CPPUNIT_ASSERT_NO_THROW(inferer.addFile("data/merge1.json"));
    CPPUNIT_ASSERT_NO_THROW(inferer.addFile("data/object.json"));
    std::ostringstream output;
    CPPUNIT_ASSERT_THROW(inferer.inferSchema(output, pluginParameters), Poco::RuntimeException);
}

void JsonInfererTest::setUp()
{
    inferer.clear();
}

void JsonInfererTest::tearDown()
{
    // Nothing here.
}

void JsonInfererTest::checkCorrectOutput(std::string inputFile, std::string expectedFile)
{
    CPPUNIT_ASSERT_NO_THROW(inferer.addFile(inputFile));

    std::stringstream output;
    std::ifstream expected(expectedFile);

    CPPUNIT_ASSERT_NO_THROW(inferer.inferSchema(output, pluginParameters));

    Poco::Dynamic::Var outputParsed = Poco::JSON::Parser().parse(output);
    Poco::Dynamic::Var expectedParsed = Poco::JSON::Parser().parse(expected);

    CPPUNIT_ASSERT_EQUAL(expectedParsed.toString(), outputParsed.toString());
}

void JsonInfererTest::checkCorrectOutput(std::string inputFile1, std::string inputFile2, std::string expectedFile)
{
    CPPUNIT_ASSERT_NO_THROW(inferer.addFile(inputFile1));
    checkCorrectOutput(inputFile2, expectedFile);
}
