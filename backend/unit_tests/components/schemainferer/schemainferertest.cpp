/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#include "schemainferertest.h"                      // Header
#include "../../../include/utils/fileupload.h"      // FileUploadCollection

#include <Poco/JSON/Object.h>       // Poco::JSON::Object
#include <Poco/JSON/Parser.h>       // Poco::JSON::Parser
#include <Poco/Dynamic/Var.h>       // Poco::Dynamic::Var
#include <Poco/Exception.h>         // Poco::Exception

#include <iostream>     // std::fstream

// Registers the fixture into the 'registry'.
CPPUNIT_TEST_SUITE_REGISTRATION(SchemaInfererTest);

void SchemaInfererTest::setUp() 
{
    // TODO: use prepared testfiles instead.
    inputFile = "data/generic.json";
    expectedFile = "data/generic_result.json";

    supportedInferers.push_back("JsonInferer");
    supportedInferers.push_back("CsvInferer");
}

void SchemaInfererTest::tearDown()
{
    pluginParameters.clear();
}

void SchemaInfererTest::testRunJSONInferer()
{
    Poco::JSON::Object::Ptr parameterObj = new Poco::JSON::Object;
    parameterObj->set("command", "run");
    parameterObj->set("name", "JsonInferer");

    std::ostringstream os;
    parameterObj->stringify(os);

    pluginParameters.insert(std::pair<std::string, std::string>("configuration", os.str()));

    bdg::utils::FileUpload::FileUploadCollection fileUploads;

    fileUploads[inputFile] = std::unique_ptr<bdg::utils::FileUpload>(bdg::utils::FileUpload::create(inputFile, "text"));

    std::string output;
    std::ifstream expected(expectedFile);

    // We only want to catch Poco::InvalidArgumentException here.
    try
    {
        output = inferer.run(pluginParameters, fileUploads);
    }
    catch (Poco::InvalidArgumentException &e)
    {
        CPPUNIT_FAIL(e.displayText());
    }
    catch (Poco::Exception &e)
    {
        // Do nothing (should be seen in JSONInferer teststuite).
    }

    Poco::Dynamic::Var outputParsed = Poco::JSON::Parser().parse(output);
    Poco::Dynamic::Var expectedParsed = Poco::JSON::Parser().parse(expected);

    CPPUNIT_ASSERT_EQUAL(expectedParsed.toString(), outputParsed.toString());
}

void SchemaInfererTest::testNoFiles()
{
    Poco::JSON::Object::Ptr parameterObj = new Poco::JSON::Object;
    parameterObj->set("command", "run");
    parameterObj->set("name", "JsonInferer");

    std::ostringstream os;
    parameterObj->stringify(os);

    pluginParameters.insert(std::pair<std::string, std::string>("configuration", os.str()));

    bdg::utils::FileUpload::FileUploadCollection fileUploads;
    
    CPPUNIT_ASSERT_THROW(inferer.run(pluginParameters, fileUploads), Poco::InvalidArgumentException);
}

void SchemaInfererTest::testListInferers()
{
    Poco::JSON::Object::Ptr parameterObj = new Poco::JSON::Object;
    parameterObj->set("command", "list");

    std::ostringstream os;
    parameterObj->stringify(os);

    pluginParameters.insert(std::pair<std::string, std::string>("configuration", os.str()));

    std::string plugins;
    bdg::utils::FileUpload::FileUploadCollection fileUploads;

    CPPUNIT_ASSERT_NO_THROW(plugins = inferer.run(pluginParameters, fileUploads));

    for (const auto &inferer : supportedInferers)
    {
        CPPUNIT_ASSERT_EQUAL( false, plugins.find(inferer) == std::string::npos );
    }
}
