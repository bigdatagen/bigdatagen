# TODO: finish and test with cmake
cmake_minimum_required (VERSION 3.0 FATAL_ERROR)

# Test the grammar library
project(Grammar CXX)

include_directories(
    ${HDR_DIR}
)

add_subdirectory(${SRC_DIR}/components/grammargenerator/grammar)

add_executable(${PROJECT_NAME}
    "${TEST_DIR}/grammar/test.cpp"
)

target_link_libraries(${PROJECT_NAME}
    bdg::grammar
)
