/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#include <cppunit/CompilerOutputter.h>                      // CppUnit::CompilerOutputter
#include <cppunit/extensions/TestFactoryRegistry.h>         // CppUnit::TestFactoryRegistry
#include <cppunit/ui/text/TestRunner.h>                     // CppUnit::TextUi::TestRunner

/**
 * Starting point of the program.
 *
 * @param argc Number of command line arguments.
 * @param argv Command line arguments.
 *
 * @return Return value of the program.
 */
int main(int argc, char* argv[])
{
    // Get the top level suite from the registry.
    CppUnit::Test *suite = CppUnit::TestFactoryRegistry::getRegistry().makeTest();

    // Adds the test to the list of test to run.
    CppUnit::TextUi::TestRunner runner;
    runner.addTest(suite);

    // Change the default outputter to a compiler error format outputter.
    runner.setOutputter(new CppUnit::CompilerOutputter(&runner.result(), std::cerr));

    // Run the tests.
    bool wasSucessful = runner.run();

    // Return error code 1 if the one of test failed.
    return wasSucessful ? 0 : 1;
}
