/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "utils/checkpointer.h"         // Checkpointer

#include <iostream>         // std::cout
#include <numeric>          // std::numeric

using bdg::utils::Checkpointer;

/**
 * Starting point of the program.
 *
 * @param argc Number of command line arguments.
 * @param argv Command line arguments.
 *
 * @return Return value of the program.
 */
int main(int argc, char **argv)
{
    size_t id;
    {
        Checkpointer ci1;
        id = ci1.getId();

        for (int i = 0; i < 10; i++)
        {
            //ci1.store(i);
        }
    }

    Checkpointer ci2(id);

    for (int i = 0; i < 10; i++)
    {
        //auto loadedValue = ci2.load(i);
        //std::cout << loadedValue << std::endl;
    }

    // Wait for user input.
    std::cout << "Write something to exit: ";
    std::string input;
    std::cin >> input;
}
