/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Transformer
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/generators/jsonschemaparser/jsongeneratorschemaparser.h"    // JsonGeneratorSchemaParser
#include "core/pipeline/plugins/generators/generator.h"                                     // Generator

#include <vector>       // std::vector
#include <iostream>     // std::cin, std::cout
#include <string>       // std::string

/**
 * Returns schemas on which we test the generator.
 *
 * @return Collection with test schemas.
 */
static std::vector<std::string> testSchemas()
{
    std::vector<std::string> jsonSchemas;

    jsonSchemas.push_back(R"(  
    [
      {
        "label": "Root",
        "type": "object",
        "typeName": "ROOT",
        "properties": [
          {
            "type": "probSet",
            "properties": [
              {
                "value": 50,
                "properties": [
                  {
                    "type": "int",
                    "propertyName": "i",
                    "properties": {
                      "source": "random",
                      "min": 0,
                      "max": 100,
                      "distribution": "uniform"
                    }
                  }
                ]
              },
              {
                "value": 50,
                "properties": [
                  {
                    "type": "float",
                    "propertyName": "f",
                    "properties": {
                      "source": "random",
                      "min": 0,
                      "max": 100,
                      "distribution": "uniform"
                    }
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
    )");

    jsonSchemas.push_back(R"(
    [
      {
        "label": "Root",
        "type": "object",
        "typeName": "ROOT",
        "properties": [
          {
            "type": "probSet",
            "properties": [
              {
                "value": 50,
                "properties": [
                  {
                    "type": "int",
                    "propertyName": "i",
                    "properties": {
                      "source": "random",
                      "min": 0,
                      "max": 100,
                      "distribution": "uniform"
                    }
                  }
                ]
              },
              {
                "value": 50,
                "properties": []
              }
            ]
          }
        ]
      }
    ]
    )");
    /*
    // Test unhomogenous arrays - simple.
    jsonSchemas.emplace_back(R"(
    [{
        "typeName": "ROOT",
        "type": "array",
        "properties": {
            "min": 3,
            "max": 6,
            "distribution": "uniform",
            "source": "random",
            "innerType": [{
                "type": "int",
                "properties": {
                    "source": "random",
                    "distribution": "uniform",
                    "min": 1,
                    "max": 1000
                }
            },
            {
                "type": "float",
                "properties": {
                    "source": "random",
                    "distribution": "uniform",
                    "min": 1,
                    "max": 100
                }
            }]
        }
    }]
    )");
    
    // Test unhomogenous arrays - complex.
    jsonSchemas.emplace_back("[{\"label\":\"Root\",\"type\":\"object\",\"typeName\":\"ROOT\",\"properties\":[{\"type\":\"array\",\"propertyName\":\"a\",\"properties\":{\"min\":1,\"max\":10,\"distribution\":\"uniform\",\"innerType\":[{\"type\":\"int\",\"properties\":{\"source\":\"random\",\"min\":0,\"max\":100,\"distribution\":\"uniform\"}},{\"type\":\"float\",\"properties\":{\"source\":\"random\",\"min\":0,\"max\":100,\"distribution\":\"uniform\"}},{\"type\":\"object\",\"properties\":[{\"type\":\"string\",\"propertyName\":\"Name\",\"properties\":{\"source\":\"domain\",\"distribution\":\"uniform\",\"domain\":[\"Ladyda\"],\"min\":1,\"max\":10}},{\"type\":\"int\",\"propertyName\":\"i\",\"properties\":{\"source\":\"random\",\"min\":0,\"max\":100,\"distribution\":\"uniform\"}}]}]}}]}]");
    */
    /*
    // Sequence expressions test.
    jsonSchemas.emplace_back(R"(
        [
            {
                "label":"Root",
                "type":"object",
                "typeName":"ROOT",
                "properties":[
                    {
                        "type":"int",
                        "propertyName":"",
                        "properties":{
                            "source":"random",
                            "min": 1,
                            "max": 10,
                            "distribution":"uniform"
                        }
                    }
                ]
            }
        ]
        )");
    
    
    jsonSchemas.emplace_back(R"(
    [{
        "label": "Root",
        "type": "object",
        "typeName": "ROOT",
        "properties": [{
            "type": "probSet",
            "properties": [{
                "value": 10,
                "properties": [{
                    "type": "int",
                    "propertyName": "smallerInt",
                    "properties": {
                        "source": "random",
                        "min": 1,
                        "max": 1,
                        "distribution": "uniform"
                    }
                }]
            }]
        }]
    }]
    )");
    */

    jsonSchemas.emplace_back(R"(
    [{
        "label": "Root",
        "type": "object",
        "typeName": "ROOT",
        "properties": [{
            "type": "object",
            "propertyName": "O",
            "properties": [{
                "type": "probSet",
                "properties": [{
                    "value": 50,
                    "properties": [{
                        "type": "int",
                        "propertyName": "i",
                        "properties": {
                            "source": "random",
                            "min": 0,
                            "max": 100,
                            "distribution": "uniform"
                        }
                    }]
                }, {
                    "value": 50,
                    "properties": [{
                        "type": "float",
                        "propertyName": "f",
                        "properties": {
                            "source": "random",
                            "min": 0,
                            "max": 100,
                            "distribution": "uniform"
                        }
                    }]
                }],
                "propertyName": "PS"
            }]
        }]
    }]
    )");

    jsonSchemas.emplace_back(R"(
    [{
        "label": "Root",
        "type": "object",
        "typeName": "ROOT",
        "properties": [{
            "type": "array",
            "propertyName": "a",
            "properties": {
                "min": 1,
                "max": 10,
                "distribution": "uniform",
                "innerType": [{
                    "type": "probSet",
                    "properties": [{
                        "value": 50,
                        "properties": [{
                            "type": "int",
                            "properties": {
                                "source": "random",
                                "min": 0,
                                "max": 100,
                                "distribution": "uniform"
                            }
                        }]
                    }, {
                        "value": 50,
                        "properties": [{
                            "type": "float",
                            "properties": {
                                "source": "random",
                                "min": 0,
                                "max": 100,
                                "distribution": "uniform"
                            }
                        }]
                    }]
                }]
            }
        }]
    }]
    )");
    

    jsonSchemas.emplace_back(R"(
    [{
        "label": "Root",
        "type": "object",
        "typeName": "ROOT",
        "properties": [{
            "type": "array",
            "propertyName": "insert",
            "properties": {
                "min": 1,
                "max": 1,
                "distribution": "uniform",
                "innerType": [{
                    "type": "IntSwitching"
                }]
            }
        }]
    }, {
        "label": "IntSwitching",
        "typeName": "IntSwitching",
        "type": "object",
        "properties": [{
            "type": "string",
            "propertyName": "fqn",
            "properties": {
                "source": "domain",
                "distribution": "uniform",
                "domain": ["MyEnterprise.Public.DemoTest.IntSwitching"]
            }
        }, {
            "type": "array",
            "propertyName": "vqts",
            "properties": {
                "min": 1,
                "max": 10,
                "distribution": "uniform",
                "innerType": [{
                    "type": "object",
                    "properties": [{
                        "type": "IntSwitchingSeq",
                        "propertyName": "v"
                    }, {
                        "type": "int",
                        "propertyName": "q",
                        "properties": {
                            "source": "random",
                            "min": 192,
                            "max": 192,
                            "distribution": "uniform"
                        }
                    }, {
                        "type": "DTS3",
                        "propertyName": "t"
                    }]
                }]
            }
        }]
    }, {
        "label": "IntSwitchingSeq",
        "type": "sequence",
        "innerType": "int",
        "typeName": "IntSwitchingSeq",
        "properties": {
            "visibility": "global",
            "initialValues": [0, 1],
            "value": "$IntSwitchingSeq[-2]"
        }
    }, {
        "label": "DTS3",
        "type": "sequence",
        "innerType": "datetime",
        "typeName": "DTS3",
        "properties": {
            "visibility": "local",
            "step": "+1hour",
            "initialDate": "2016-05-01",
            "initialTime": "01:00"
        }
    }]
    )");

    jsonSchemas.emplace_back(R"(
        [
            {
                "typeName": "SeqSample",
                "type": "sequence",
                "innerType": "int",
                "properties": {
                    "visibility": "global",
                    "initialValues": [1, 2],
                    "value": "1 + $SeqSample"
                }
            },
            {
                "typeName": "ROOT",
                "type": "object",
                "properties": [
                    {
                        "propertyName": "S",
                        "type": "SeqSample"
                    },
                    {
                        "propertyName": "SS",
                        "type": "SeqSample"
                    },
                    {
                        "propertyName": "SSS",
                        "type": "SeqSample"
                    },
                    {
                        "propertyName": "SSSS",
                        "type": "SeqSample"
                    }
                ]
            }
        ]
        )");
    
    jsonSchemas.emplace_back("[{\"label\":\"Root\",\"type\":\"object\",\"typeName\":\"ROOT\",\"properties\":[{\"type\":\"array\",\"propertyName\":\"insert\",\"properties\":{\"min\":1,\"max\":1,\"distribution\":\"uniform\",\"innerType\":[{\"type\":\"object\",\"properties\":[{\"type\":\"string\",\"propertyName\":\"fqn\",\"properties\":{\"source\":\"domain\",\"distribution\":\"uniform\",\"domain\":[\"MyEnterprise.Public.MyIntTag\"]}},{\"type\":\"array\",\"propertyName\":\"vqts\",\"properties\":{\"min\":1,\"max\":1,\"distribution\":\"uniform\",\"innerType\":[{\"type\":\"object\",\"properties\":[{\"type\":\"int\",\"propertyName\":\"v\",\"properties\":{\"source\":\"random\",\"min\":0,\"max\":100,\"distribution\":\"uniform\"}},{\"type\":\"int\",\"propertyName\":\"q\",\"properties\":{\"source\":\"random\",\"min\":192,\"max\":192,\"distribution\":\"uniform\"}},{\"type\":\"DTS\",\"propertyName\":\"t\"}]}]}}]}]}}]},{\"label\":\"DTS\",\"type\":\"sequence\",\"innerType\":\"datetime\",\"typeName\":\"DTS\",\"properties\":{\"visibility\":\"local\",\"initialDate\":\"2016-05-08\",\"initialTime\":\"10:30\",\"step\":\"+1hour\"}}]");

    jsonSchemas.emplace_back(R"(
    [{
        "label": "Root",
        "type": "object",
        "typeName": "ROOT",
        "properties": [{
            "type": "array",
            "propertyName": "insert",
            "properties": {
                "min": 1,
                "max": 1,
                "distribution": "uniform",
                "innerType": [{
                    "type": "object",
                    "properties": [{
                        "type": "string",
                        "propertyName": "fqn",
                        "properties": {
                            "source": "domain",
                            "distribution": "uniform",
                            "domain": ["MyEnterprise.Public.MyIntTag"]
                        }
                    }, {
                        "type": "array",
                        "propertyName": "vqts",
                        "properties": {
                            "max": 10,
                            "distribution": "uniform",
                            "innerType": [{
                                "type": "object",
                                "properties": [{
                                    "type": "int",
                                    "propertyName": "v",
                                    "properties": {
                                        "source": "random",
                                        "min": 0,
                                        "max": 100,
                                        "distribution": "uniform"
                                    }
                                }, {
                                    "type": "int",
                                    "propertyName": "q",
                                    "properties": {
                                        "source": "random",
                                        "min": 192,
                                        "max": 192,
                                        "distribution": "uniform"
                                    }
                                }, {
                                    "type": "DTS",
                                    "propertyName": "t"
                                }]
                            }],
                            "min": 10
                        }
                    }]
                }]
            }
        }]
    }, {
        "label": "DTS",
        "type": "sequence",
        "innerType": "datetime",
        "typeName": "DTS",
        "properties": {
            "visibility": "global",
            "initialDate": "2016-01-01",
            "initialTime": "01:00",
            "step": "+1day"
        }
    }]
    )");
    
    jsonSchemas.emplace_back(R"(
        [{
                "typeName": "ROOT",
                "type": "object",
                "properties": [
                    {
                        "propertyName": "A",
                        "type": "array",
                        "properties": {
                            "min": 5,
                            "max": 10,
                            "innerType": ["DTS"]
                        }
                    },
                    {
                        "propertyName": "S",
                        "type": "DTS"
                    },
                    {
                        "propertyName": "SS",
                        "type": "DTS"
                    },
                    {
                        "propertyName": "SSS",
                        "type": "DTS"
                    },
                    {
                        "propertyName": "SSSS",
                        "type": "DTS"
                    }
                ]
            }, {
            "label": "DTS",
            "type": "sequence",
            "innerType": "datetime",
            "typeName": "DTS",
            "properties": {
                "visibility": "global",
                "initialDate": "2016-05-01",
                "initialTime": "10:00",
                "step": "+1day"
            }
        }]
        )");
        

    jsonSchemas.emplace_back(R"(
    [{
        "typeName": "ROOT",
        "type": "array",
        "properties": {
            "min": 3,
            "max": 6,
            "innerType": [{
                "type": "int",
                "properties": {
                    "source": "random",
                    "distribution": "uniform",
                    "min": 1,
                    "max": 1000
                }
            }]
        }
    }]
    )");

    jsonSchemas.emplace_back(R"(
    [{
        "typeName": "ROOT",
        "type": "object",
        "properties": [{
            "propertyName": "IntSample1",
            "type": "int",
            "properties": {
                "source": "random",
                "min": 0,
                "max": 100,
                "distribution": "uniform"
            }
        }, {
            "propertyName": "IntSample2",
            "type": "int",
            "properties": {
                "source": "domain",
                "domain": [2, 4, 6, 8, 10],
                "distribution": "uniform"
            }
        }]
    }]
    )");
    
    jsonSchemas.emplace_back(R"(
        [
            {
                "typeName": "SeqSample",
                "type": "sequence",
                "properties": {
                    "visibility": "global",
                    "initialValues": [0],
                    "innerType": "int",
                    "value": "1 + $SeqSample"
                }
            },
            {
                "typeName": "MyArray",
                "type": "array",
                "properties": {
                    "min": 1,
                    "max": 10,
                    "innerType": ["SeqSample"]
                }
            },
            {
                "typeName": "ROOT",
                "type": "object",
                "properties": [
                    {
                        "propertyName": "MA",
                        "type": "MyArray"
                    }
                ]
            }
        ]
        )");
        
    jsonSchemas.emplace_back(R"(
    [{
        "typeName": "MyInt",
        "type": "array",
        "properties": {
            "min": 1,
            "max": 100,
            "innerType": [{
                "type": "int",
                "propertyName": "IntSample3",
                "properties": {
                    "source": "random",
                    "distribution": "uniform",
                    "min": 1,
                    "max": 1000
                }
            }]
        }
    }, {
        "typeName": "MyArray",
        "type": "array",
        "properties": {
            "min": 11,
            "max": 100,
            "innerType": [{
                "type": "int",
                "propertyName": "IntSample3",
                "properties": {
                    "source": "random",
                    "distribution": "uniform",
                    "min": 1,
                    "max": 1000
                }
            }, {
                "propertyName": "IntSample2",
                "type": "int",
                "properties": {
                    "source": "domain",
                    "domain": [2, 4, 6, 8, 10],
                    "distribution": "uniform"
                }
            }, {
                "propertyName": "FloatSample2",
                "type": "float",
                "properties": {
                    "source": "domain",
                    "domain": [53.1, 1, 2, 13.3, 10.0],
                    "distribution": "uniform"
                }
            }]
        }
    }, {
        "typeName": "ROOT",
        "type": "object",
        "properties": [{
            "propertyName": "IntSample1",
            "type": "int",
            "properties": {
                "source": "random",
                "min": 0,
                "max": 100,
                "distribution": "uniform"
            }
        }, {
            "propertyName": "FloatSample1",
            "type": "float",
            "properties": {
                "source": "random",
                "min": 0.3,
                "max": 53.1,
                "distribution": "uniform"
            }
        }, {
            "propertyName": "StringSample1",
            "type": "string",
            "properties": {
                "source": "random",
                "min": 10,
                "max": 20,
                "distribution": "uniform"
            }
        }, {
            "propertyName": "IntSample2",
            "type": "int",
            "properties": {
                "source": "domain",
                "domain": [2, 4, 6, 8, 10],
                "distribution": "uniform"
            }
        }, {
            "propertyName": "FloatSample2",
            "type": "float",
            "properties": {
                "source": "domain",
                "domain": [53.1, 1, 2, 13.3, 10.0],
                "distribution": "uniform"
            }
        }, {
            "propertyName": "StringSample2",
            "type": "string",
            "properties": {
                "source": "domain",
                "domain": ["nazdar", "ahoj", "jak", "se", "vede"],
                "distribution": "uniform"
            }
        }, {
            "propertyName": "IntSample3",
            "type": "MyInt"
        }, {
            "propertyName": "MyUnhomogenous Array",
            "type": "MyArray"
        }]
    }]
    )");

    jsonSchemas.emplace_back(R"(
    [{
        "label": "Root",
        "type": "object",
        "typeName": "ROOT",
        "properties": [{
            "type": "int",
            "propertyName": "i",
            "properties": {
                "source": "random",
                "min": 0,
                "max": 19,
                "distribution": "uniform"
            }
        }, {
            "type": "array",
            "propertyName": "a",
            "properties": {
                "min": 10,
                "max": 20,
                "distribution": "uniform",
                "innerType": [{
                    "type": "int",
                    "properties": {
                        "source": "random",
                        "min": 0,
                        "max": 10,
                        "distribution": "uniform"
                    }
                }]
            }
        }, {
            "type": "obj2",
            "propertyName": "o",
            "properties": {
                "source": "random",
                "min": 0,
                "max": 10,
                "distribution": "uniform"
            }
        }]
    }, {
        "label": "obj2",
        "type": "object",
        "typeName": "obj2",
        "properties": [{
            "type": "float",
            "propertyName": "f",
            "properties": {
                "source": "random",
                "min": 0,
                "max": 10,
                "distribution": "uniform"
            }
        }, {
            "type": "int",
            "propertyName": "i",
            "properties": {
                "source": "domain",
                "domain": [1],
                "distribution": "uniform"
            }
        }]
    }]
    )");
    
    jsonSchemas.push_back(R"(
    [{
        "label": "Root",
        "type": "object",
        "typeName": "ROOT",
        "properties": [{
            "type": "array",
            "propertyName": "insert",
            "properties": {
                "min": 1,
                "max": 1,
                "distribution": "uniform",
                "innerType": [{
                    "type": "object",
                    "properties": [{
                        "type": "string",
                        "propertyName": "fqn",
                        "properties": {
                            "source": "domain",
                            "distribution": "uniform",
                            "domain": [
                                "MyEnterprise.Public.MyIntTag"
                            ]
                        }
                    }, {
                        "type": "array",
                        "propertyName": "vqts",
                        "properties": {
                            "min": 1,
                            "max": 1,
                            "distribution": "uniform",
                            "innerType": [{
                                "type": "object",
                                "properties": [{
                                    "type": "int",
                                    "propertyName": "v",
                                    "properties": {
                                        "source": "random",
                                        "min": 0,
                                        "max": 100,
                                        "distribution": "uniform"
                                    }
                                }, {
                                    "type": "int",
                                    "propertyName": "q",
                                    "properties": {
                                        "source": "random",
                                        "min": 192,
                                        "max": 192,
                                        "distribution": "uniform"
                                    }
                                }, {
                                    "type": "string",
                                    "propertyName": "t",
                                    "properties": {
                                        "source": "domain",
                                        "distribution": "uniform",
                                        "domain": [
                                            "2016-04-03T11:00:00.478Z"
                                        ],
                                        "min": 1,
                                        "max": 10
                                    }
                                }]
                            }]
                        }
                    }]
                }]
            }
        }]
    }]
    )");

    return jsonSchemas;
}

/**
 * Starting point of the program.
 *
 * @param argc Number of command line arguments.
 * @param argv Command line arguments.
 *
 * @return Return value of the program.
 */
int main(int argc, char **argv)
{
    using namespace bdg::core;

    for (const auto &schema : testSchemas())
    {
        Generator mGen;
        JsonGeneratorSchemaParser jp1(mGen);
        mGen.setRank(0);
        jp1.parse(schema);

        const size_t fileCount = 100;
        mGen.setFilesCount(fileCount);

        Generator sGen1;
        JsonGeneratorSchemaParser jp2(sGen1);
        sGen1.setRank(1);
        jp2.parse(schema);

        sGen1.regRemInts(mGen.genRemInts());
        sGen1.setSeqsStart(mGen.genSeqsStart());
        sGen1.openStorage("output1.txt");

        for (size_t i = 0; i < fileCount; i++)
        {
            sGen1.generate();
            const_cast<GenStorer&>(sGen1.getStorer()).store('\n');
        }

        sGen1.closeStorage();

        Generator sGen2;
        JsonGeneratorSchemaParser jp3(sGen2);
        sGen2.setRank(2);
        jp3.parse(schema);
        
        sGen2.regRemInts(mGen.genRemInts());
        sGen2.setSeqsStart(mGen.genSeqsStart());
        sGen2.openStorage("output2.txt");

        for (size_t i = 0; i < fileCount; i++)
        {
            sGen2.generate();
            const_cast<GenStorer&>(sGen2.getStorer()).store('\n');
        }

        sGen2.closeStorage();        

        sGen1.regRemInts(mGen.genRemInts());
        sGen1.setSeqsStart(mGen.genSeqsStart());
        sGen1.openStorage("output1.txt");

        for (size_t i = 0; i < fileCount; i++)
        {
            sGen1.generate();
            const_cast<GenStorer&>(sGen1.getStorer()).store('\n');
        }

        sGen1.closeStorage();
    }

    // Wait for user input.
    std::cout << "Write something to exit: ";
    std::string input;
    std::cin >> input;
}
