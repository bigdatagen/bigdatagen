/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "utils/logger.h"       // Logger
#include <iostream>             // std::cout

/**
 * Starting point of the program.
 *
 * @param argc Number of command line arguments.
 * @param argv Command line arguments.
 *
 * @return Return value of the program.
 */
int main()
{
    Poco::Logger &consoleLogger = bdg::utils::Logger::getInstance().getConsoleLogger();
    consoleLogger.debug("debug");
    consoleLogger.information("information");
    consoleLogger.warning("warning");
    consoleLogger.error("error");
    consoleLogger.fatal("fatal");

    // Wait for user input.
    std::cout << "Write something to exit: ";
    std::string input;
    std::cin >> input;
}
