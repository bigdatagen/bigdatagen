/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Transformer
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/generators/jsongenerator/jsongen.h"         // JsonGen

#include <iostream>     // std::cout
#include <string>       // std::string

/**
 * Returns schemas on which we test the generator.
 *
 * @return Collection with test schemas.
 */
static std::vector<std::string> testSchemas()
{
    std::vector<std::string> schemas;

    schemas.emplace_back(R"(
        [{
            "typeName": "ROOT",
            "label": "Root Nonterminal",
            "definition": {
                "type": "complex",
                "children": [{
                    "type": "int",
                    "properties": {}
                }, {
                    "type": "string",
                    "properties": {}
                }, {
                    "type": "cycle",
                    "cycleType": "repetitions",
                    "cycleMin": 2,
                    "cycleMax": 5,
                    "innerElement": {
                        "type": "alternation",
                        "children": [{
                            "type": "cycle",
                            "probability": 25,
                            "cycleType": "probability",
                            "cycleProbability": 75,
                            "innerElement": {
                                "type": "string",
                                "properties": {}
                            }
                        }, {
                            "type": "complex",
                            "probability": 50,
                            "children": [{
                                "type": "string",
                                "properties": {}
                            }, {
                                "type": "float",
                                "properties": {}
                            }, {
                                "type": "alternation",
                                "children": [{
                                    "type": "empty",
                                    "probability": 20
                                }, {
                                    "type": "empty",
                                    "probability": 5
                                }, {
                                    "type": "datetime",
                                    "probability": 5,
                                    "properties": {}
                                }, {
                                    "type": "empty",
                                    "probability": 10
                                }, {
                                    "type": "complex",
                                    "probability": 10,
                                    "children": [{
                                        "type": "int",
                                        "properties": {}
                                    }, {
                                        "type": "empty"
                                    }, {
                                        "type": "string",
                                        "properties": {}
                                    }]
                                }, {
                                    "type": "int",
                                    "probability": 10,
                                    "properties": {}
                                }, {
                                    "type": "ROOT",
                                    "probability": 25
                                }, {
                                    "type": "empty",
                                    "probability": 15
                                }]
                            }, {
                                "type": "empty"
                            }]
                        }, {
                            "type": "string",
                            "probability": 25,
                            "properties": {}
                        }]
                    }
                }, {
                    "type": "float",
                    "properties": {}
                }]
            }
        }]
        )");

    return schemas;
}

using namespace bdg::core;

/**
 * Starting point of the program.
 *
 * @param argc Number of command line arguments.
 * @param argv Command line arguments.
 *
 * @return Return value of the program.
 */
int main(int argc, char **argv)
{
    for (auto const &schema : testSchemas())
    {
        // Do something with a schema.
    }

    // Wait for user input.
    std::cout << "Write something to exit: ";
    std::string input;
    std::cin >> input;
}
