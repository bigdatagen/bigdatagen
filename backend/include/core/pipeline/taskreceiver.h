/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/messagetype.h"      // MessageType
#include "core/pipeline/plugins/plugin.h"   // PipelinePlugin
#include "core/pipeline/pipeline.h"         // Pipeline
#include "utils/mpiproxy.h"                 // MPIProxy

#include <vector>       // std::vector

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Instance of this class runs in each worker thread, i.e. in each thread
             * that has rank greater than 0. Thread with the rank 0 is the master thread.
             * This class receives messages (commands) from the master and it processes
             * then and sends back messages with responses. The most important command
             * is a command with a task. Task is processed and then the task result is
             * sent back to the master in a message. Data being sent between master
             * and workers are serialized in JSON.
             */
            class TaskReceiver
            {
            public:
                TaskReceiver(const bdg::core::pipeline::Pipeline &pipeline, const bdg::utils::MPIProxy &mpi);

                void receive();

            private:
                bdg::core::pipeline::Task *createTask(const std::string &messageData, const int pluginIndex) const;
                bdg::core::pipeline::TaskResult *processTask(bdg::core::pipeline::Task *task, const int pluginIndex) const;

                void initialize(const std::string &messageData) const;
                void sendResponseToMaster(bdg::core::pipeline::Task *task, bdg::core::pipeline::TaskResult *taskResult, const int pluginIndex) const;
                void handleError(bdg::core::pipeline::Task *task, const bdg::core::pipeline::TaskResult *taskResult) const;
                void handleFatalError(const bdg::core::pipeline::TaskResult *taskResult) const;
                void handleFatalError(const std::string &error) const;
                void cleanUp() const;

                /**
                 * Instance of the pipeline with plugins.
                 */
                const bdg::core::pipeline::Pipeline &pipeline;

                /**
                 * MPI wrapper.
                 */
                const bdg::utils::MPIProxy &mpi;

                /**
                 * How many milliseconds should the current thread sleep when an idle message
                 * is received.
                 */
                static const int IDLE_SLEEP_MILLISECONDS;
            };
        }
    }
}
