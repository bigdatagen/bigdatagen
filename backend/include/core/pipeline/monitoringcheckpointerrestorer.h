/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "utils/checkpointer.h"     // Checkpointer

#include <Poco/JSON/Object.h>       // Poco::JSON::Object

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Class which is able to restore monitoring checkpoints, i.e. it loads
             * content of these checkpoints into JSON objects.
             */
            class MonitoringCheckpointerRestorer : public bdg::utils::Checkpointer
            {
            public:
                explicit MonitoringCheckpointerRestorer(const int pipelineJobId);
                virtual ~MonitoringCheckpointerRestorer() = default;

                virtual inline std::string getClassId() const override;

                void restore(Poco::JSON::Object &json) const;
                static inline const std::string getPathToStopFile(const int jobId);
            };
        }
    }
}

/**
 * Returns name of the checkpointer. It appears in the names of checkpoint files.
 *
 * @return Name of the checkpointer.
 */
inline std::string bdg::core::pipeline::MonitoringCheckpointerRestorer::getClassId() const
{
    return "monitoring";
}

/**
 * Returns path to the stop file. This file is used to pause or stop jobs - when this files
 * appears a job is stopped.
 *
 * @param jobId Id of currently running job.
 *
 * @return Path to the stop file.
 */
inline const std::string bdg::core::pipeline::MonitoringCheckpointerRestorer::getPathToStopFile(const int jobId)
{
    return bdg::utils::Config::getInstance().getCheckpointerStoragePath() + "/stop-" + std::to_string(jobId);
}
