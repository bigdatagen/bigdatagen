/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include <Poco/Timestamp.h>     // Poco::Timestamp

#include <string>       // std::string

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Class reprezenting one event that occured on some worker. We
             * distinguish two types of events - start of task processing and
             * end of task processing. These events are used to reconstruct
             * the schedule of tasks.
             */
            class WorkerEvent
            {
            public:
                WorkerEvent(const std::string &name, const Poco::Timestamp &time, const Poco::Int64 intNote = 0);
                ~WorkerEvent() = default;

                inline const std::string &getName() const;
                inline const Poco::Timestamp &getTime() const;
                inline const Poco::Int64 getIntNote() const;

                /**
                 * Name of the event of task start.
                 */
                static const std::string EVENT_TASK_STARTED;

                /**
                 * Name of the event of task end.
                 */
                static const std::string EVENT_TASK_FINISHED;

            private:
                /**
                 * Name of the event.
                 */
                const std::string name;

                /**
                 * Timestamp when event occured.
                 */
                const Poco::Timestamp time;

                /**
                 * Additional information of the event.
                 */
                const Poco::Int64 intNote;
            };
        }
    }
}

/**
 * Returns the name of the event.
 *
 * @return Name of the event.
 */
inline const std::string &bdg::core::pipeline::WorkerEvent::getName() const
{
    return name;
}

/**
 * Returns timestamp when event occured.
 *
 * @return Timestamp when event occured.
 */
inline const Poco::Timestamp &bdg::core::pipeline::WorkerEvent::getTime() const
{
    return time;
}

/**
 * Returns additional information of the event.
 *
 * @return Additional information of the event.
 */
inline const Poco::Int64 bdg::core::pipeline::WorkerEvent::getIntNote() const
{
    return intNote;
}
