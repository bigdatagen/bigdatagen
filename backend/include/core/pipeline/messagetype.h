/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include <string>           // std::string

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Defines types of messages that are sent from master to worker
             * or vice versa.
             */
            enum MessageType
            {
                MESSAGE_TYPE_INITIALIZE,
                MESSAGE_TYPE_INITIALIZED,
                MESSAGE_TYPE_TASK_NEW,
                MESSAGE_TYPE_TASK_COMPLETED,
                MESSAGE_TYPE_TASK_ERROR,
                MESSAGE_TYPE_FATAL_ERROR,
                MESSAGE_TYPE_IDLE,
                MESSAGE_TYPE_IDLE_NO_SLEEP,
                MESSAGE_TYPE_BYTES_STORED,
                MESSAGE_TYPE_JOB_COMPLETED
            };

            /**
             * Messages have tag greater or equal to this number are messages
             * of type MESSAGE_TYPE_TASK_NEW.
             */
            const int MESSAGE_TYPE_TASK_NEW_THRESHOLD = 16;

            std::string translateMessageType(const int type);
            int createTaskNewMessage(const int pluginIndex);
            int getPluginIndexFromTaskNewMessage(const int message);
            bool isTaskNewMessage(const int type);
        }
    }
}
