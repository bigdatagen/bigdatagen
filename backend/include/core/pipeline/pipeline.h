/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/pipelineinfocheckpointerrestorer.h"     // PipelineInfoCheckpointerRestorer
#include "core/pipeline/plugins/plugin.h"                       // Plugin
#include "utils/pluginmanager.h"                                // PluginManager
#include "utils/mpiproxy.h"                                     // MPIProxy

#include <string>       // std::string
#include <memory>       // std::unique_ptr
#include <vector>       // std::vector

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Class that holds GTL pipeline created from loaded plugins.
             * Between two plugins placed next to themselves is created queue 
             * through which data can be sent.
             */
            class Pipeline
            {
            public:
                Pipeline(bdg::utils::MPIProxy &mpi, const int jobId);
                Pipeline(const Pipeline &) = delete;
                Pipeline &operator=(const Pipeline &) = delete;
                Pipeline(Pipeline &&) = default;
                Pipeline &operator=(Pipeline &&) = default;

                void run();
                const int getPluginRelativePosition(const int pluginIndex) const;
                
                inline const int getJobId() const;
                inline const PipelineInfo &getInfo() const;
                inline const std::unique_ptr<bdg::core::pipeline::plugin::Plugin> &getPlugin(const int pluginIndex) const;
                inline const int getPluginsCount() const;
                inline bool useGlobalFileIds() const;
                inline const std::vector<bool> &getResultQueuesUsedAsPriorityQueue() const;

            private:
                void registerPlugin(const std::string &pluginName, std::vector<std::string> &pluginNames, std::vector<int> &oneRankPlugins);
                void optimizePipeline(std::vector<std::string> &pluginNames);

                /**
                 * Id of currently running job.
                 */
                const int jobId;

                /**
                 * MPI wrapper.
                 */
                bdg::utils::MPIProxy &mpi;

                /**
                 * Plugin manager which loads pipeline plugins from the specified directory.
                 */
                bdg::utils::PluginManager<bdg::core::pipeline::plugin::Plugin> pipelinePluginManager;

                /**
                 * Plugins placed in the pipeline.
                 */
                std::vector<std::unique_ptr<bdg::core::pipeline::plugin::Plugin>> plugins;

                /**
                 * Checkpointer for loadding job configuration file.
                 */
                PipelineInfoCheckpointerRestorer pipelineCheckpointer;

                /**
                 * Info about pipeline loaded from the job configuration file.
                 */
                PipelineInfo pipelineInfo;
                
                /**
                 * Which plugins require to use result queue as a priority queue.
                 */
                std::vector<bool> resultQueuesUsedAsPriorityQueue;

                /**
                 * Can we use global ids for generated files? If not, we have to
                 * use local ids for each worker and plugin.
                 */
                bool canUseGlobalFileIds = true;

                /**
                 * Maximum number of post-processor plugins that one plugin can have in a chain.
                 */
                static const int MAX_POSTPROCESSOR_PLUGINS;
            };
        }
    }
}

/**
 * Returns the id of currently running job.
 *
 * @return Id of currently running job.
 */
inline const int bdg::core::pipeline::Pipeline::getJobId() const
{
    return jobId;
}

/**
 * Returns the info about pipeline loaded from the job configuration file.
 *
 * @return Info about pipeline loaded from the job configuration file.
 */
inline const bdg::core::pipeline::PipelineInfo &bdg::core::pipeline::Pipeline::getInfo() const
{
    return pipelineInfo;
}

/**
 * Returns pipeline plugin placed on the index specified as "pluginIndex" in the pipeline.
 *
 * @param pluginIndex Index of the plugin in the pipeline.
 *
 * @return Plugin from the pipeline.
 */
inline const std::unique_ptr<bdg::core::pipeline::plugin::Plugin> &bdg::core::pipeline::Pipeline::getPlugin(const int pluginIndex) const
{
    return plugins.at(pluginIndex);
}

/**
 * Returns total number of plugins placed in the pipeline.
 *
 * @return Total number of plugins placed in the pipeline.
 */
inline const int bdg::core::pipeline::Pipeline::getPluginsCount() const
{
    return plugins.size();
}

/**
 * Returns whether we can use global ids for generated files? If not, we have to use local
 * ids for each worker and plugin.
 *
 * @return Can we use global ids for generated files?
 */
inline bool bdg::core::pipeline::Pipeline::useGlobalFileIds() const
{
    return canUseGlobalFileIds;
}

/**
 * Returns which plugins require to use result queue as a priority queue.
 *
 * @return Which plugins require to use result queue as a priority queue.
 */
const std::vector<bool> &bdg::core::pipeline::Pipeline::getResultQueuesUsedAsPriorityQueue() const
{
    return resultQueuesUsedAsPriorityQueue;
}
