/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 * @brief    Structure for storing/saving information for pipeline.
 */

#pragma once

#include "core/pipeline/plugins/plugin.h"       // Plugin
#include "core/pipeline/schedulerinfo.h"        // SchedulerInfo

#include <Poco/JSON/Object.h>       // Poco::JSON::Object
#include <Poco/Dynamic/Var.h>       // Poco::Dynamic::Var

#include <vector>       // std::vector
#include <iostream>     // std::cout
#include <string>       // std::string
#include <map>          // std::map

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Instance of this class reprezents information about pipeline loaded
             * from the job configuration file.
             */
            class PipelineInfo
            {
            public:
                typedef std::map<std::string, std::string> Properties;

                PipelineInfo(const Properties &parameters, const int clusterSize);
                PipelineInfo(const std::string &json, const int clusterSize);
                
                std::string toString() const;
                size_t getClusterSize() const;

                void setPipelinePluginProperties(plugin::Plugin *plugin, const int pluginRelativePosition) const;
                Poco::Dynamic::Var getPipelinePluginProperty(const std::string &pluginName, const std::string &propertyName) const;
                Poco::Dynamic::Var getPipelinePluginProperty(const int pluginIndex, const std::string &propertyName) const;
                std::string getPipelinePluginPropertyStr(const std::string &pluginName, const std::string &propertyName) const;
                std::string getPipelinePluginPropertyStr(const int pluginIndex, const std::string &propertyName) const;

                int getPipelinePluginIndex(const std::string &pluginName) const;
                void setPipelinePluginNames(const std::vector<std::string> &pluginNames);

                inline const std::string getPipelinePluginName(const int pluginIndex) const;
                inline const std::vector<std::string> &getPipelinePluginNames() const;
                inline const SchedulerInfo &getSchedulerInfo() const;
                inline SchedulerInfo &getSchedulerInfo();
                
                static const std::string PARAM_PIPELINE_CONFIGURATION;
                static const std::string PROPERTY_PIPELINE_PLUGINS;
                static const std::string PROPERTY_SCHEDULER;
                static const std::string PROPERTY_CLUSTER;

            private:
                void addPluginName(const std::string &pluginName);
                void loadFromString(const std::string &json);
                void loadSchedulerInfo();
                void loadSchedulerInfoPlugins(const Poco::JSON::Object::Ptr schedulerConfiguration);
                void loadSchedulerInfoRanks(const Poco::JSON::Object::Ptr schedulerConfiguration);

                /**
                 * Size of the cluster where BDG runs.
                 */
                const int clusterSize;
                
                /**
                 * Configuration of pipeline.
                 */
                std::string configuration;

                /**
                 * Names of plugins that form a pipeline.
                 */
                std::vector<std::string> pipelinePluginNames;

                /**
                 * Configuration of the scheduler.
                 */
                SchedulerInfo schedulerInfo;

                /**
                 * Root object of the pipeline configuration.
                 */
                Poco::JSON::Object::Ptr rootObject;
            };
        }
    }
}

/**
 * Returns the name of plugin that is placed in the pipeline on given index.
 *
 * @param pluginIndex Index of the plugin in the pipeline.
 *
 * @return Name of plugin.
 */
inline const std::string bdg::core::pipeline::PipelineInfo::getPipelinePluginName(const int pluginIndex) const
{
    return pipelinePluginNames[pluginIndex];
}

/**
 * Returns names of plugins that form a pipeline.
 *
 * @return Names of plugins that form a pipeline.
 */
inline const std::vector<std::string> &bdg::core::pipeline::PipelineInfo::getPipelinePluginNames() const
{
    return pipelinePluginNames;
}

/**
 * Returns configuration of the scheduler. 
 *
 * @return Configuration of the scheduler. 
 */
inline const bdg::core::pipeline::SchedulerInfo &bdg::core::pipeline::PipelineInfo::getSchedulerInfo() const
{
    return schedulerInfo;
}

/**
 * Returns configuration of the scheduler. 
 *
 * @return Configuration of the scheduler. 
 */
inline bdg::core::pipeline::SchedulerInfo &bdg::core::pipeline::PipelineInfo::getSchedulerInfo()
{
    return schedulerInfo;
}
