/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/progresscounter.h"                  // ProgressCounter
#include "core/pipeline/plugins/valuewithunit.h"            // ValueWithUnit
#include "core/pipeline/plugins/successtaskresult.h"        // SuccessTaskResult
#include "core/pipeline/plugins/errortaskresult.h"          // ErrorTaskResult

#include <memory>       // std::unique_ptr

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Class for counting statistics about currently running job.
             */
            class StatisticsCounter
            {
            public:
                StatisticsCounter(const ProgressCounter &progressCounter, const size_t workersCount, const size_t pluginsCount);
                ~StatisticsCounter() = default;

                void taskProcessed(const SuccessTaskResult *taskResult);
                void taskError(const ErrorTaskResult *taskResult);
                bool tooManyErrors(const int pluginIndex) const;

                const size_t getUsualGeneratedFiles(const int pluginIndex) const;
                const size_t getUsualGeneratedBytes(const int pluginIndex) const;
                const size_t getOptimalFilesToGenerate(const size_t demandedFilesPerTask) const;

                inline const Poco::UInt64 getProcessedPluginTasks(const int pluginIndex) const;
                inline const Poco::UInt64 getProcessedTasksCount() const;
                inline const Poco::UInt64 getErrorPluginTasks(const int pluginIndex) const;
                inline const Poco::UInt64 getErrorTasksCount() const;
                inline const Poco::UInt64 getSuccessfulPluginTasks(const int pluginIndex) const;
                inline const Poco::UInt64 getSuccessfulTasksCount() const;

                void serialize(Poco::JSON::Object &json) const;
                void unserialize(const Poco::JSON::Object &json);

            private:
                /**
                 * Object for counting the progress of the current job.
                 */
                const ProgressCounter &progressCounter;

                /**
                 * Size of the cluster where BDG runs.
                 */
                const size_t workersCount;

                /**
                 * Number of plugins placed in the pipeline.
                 */
                const size_t pluginsCount;

                /**
                 * How many files were generated in each stage of the pipeline.
                 */
                std::vector<Poco::UInt64> generatedFilesPerPlugin;

                /**
                 * What is the size of files generated in each stage of the pipeline.
                 */
                std::vector<Poco::UInt64> generatedBytesPerPlugin;

                /**
                 * How much time did we spend in tasks for each stage of the pipeline.
                 */
                std::vector<Poco::UInt64> machineTimeSpentPerPlugin;

                /**
                 * How many tasks were finished (successfully or with an error) in
                 * each stage of the pipeline.
                 */
                std::vector<Poco::UInt64> processedTasksPerPlugin;

                /**
                 * How many error tasks appeared in each stage of the pipeline.
                 */
                std::vector<Poco::UInt64> errorTasksPerPlugin;

                /**
                 * What is the length of generator task that we demand.
                 */
                static const size_t OPTIMAL_GENERATOR_TASK_LENGTH_SECONDS;

                /**
                 * How many requests do we want in maximum from workers to the master.
                 */
                static const size_t MAXIMUM_MASTER_REQUESTS_PER_SECOND;

                /**
                 * How many tasks must be processed before we can signal a fatal error.
                 */
                static const size_t PROCESSED_TASKS_BEFORE_FATAL_ERROR;

                /**
                 * What is the ration between error tasks and processed tasks to signal a fatal error.
                 */
                static const double FATAL_ERROR_THRESHOLD;
            };
        }
    }
}

/**
 * Returns the total number of processed tasks for the given stage of the pipeline. Number
 * of processed tasks is number of successful tasks + number of error tasks.
 *
 * @param pluginIndex Index of the plugin in the pipeline - determines stage of the pipeline.
 *
 * @return Total number of processed tasks.
 */
inline const Poco::UInt64 bdg::core::pipeline::StatisticsCounter::getProcessedPluginTasks(const int pluginIndex) const
{
    return processedTasksPerPlugin[pluginIndex];
}

/**
 * Returns the total number of processed tasks in the job.
 *
 * @return Total number of processed tasks.
 */
inline const Poco::UInt64 bdg::core::pipeline::StatisticsCounter::getProcessedTasksCount() const
{
    Poco::UInt64 total = 0;

    for (int i = 0; i < pluginsCount; ++i)
    {
        total += getProcessedPluginTasks(i);
    }

    return total;
}

/**
 * Returns the total number of error tasks for the given stage of the pipeline.
 *
 * @param pluginIndex Index of the plugin in the pipeline - determines stage of the pipeline.
 *
 * @return Total number of error tasks.
 */
inline const Poco::UInt64 bdg::core::pipeline::StatisticsCounter::getErrorPluginTasks(const int pluginIndex) const
{
    return errorTasksPerPlugin[pluginIndex];
}

/**
 * Returns the total number of error tasks in the job.
 *
 * @return Total number of error tasks.
 */
inline const Poco::UInt64 bdg::core::pipeline::StatisticsCounter::getErrorTasksCount() const
{
    Poco::UInt64 total = 0;

    for (int i = 0; i < pluginsCount; ++i)
    {
        total += getErrorPluginTasks(i);
    }

    return total;
}

/**
 * Returns the total number of successful tasks for the given stage of the pipeline.
 *
 * @param pluginIndex Index of the plugin in the pipeline - determines stage of the pipeline.
 *
 * @return Total number of successful tasks.
 */
inline const Poco::UInt64 bdg::core::pipeline::StatisticsCounter::getSuccessfulPluginTasks(const int pluginIndex) const
{
    return getProcessedPluginTasks(pluginIndex) - getErrorPluginTasks(pluginIndex);
}

/**
 * Returns the total number of successful tasks in the job.
 *
 * @return Total number of successful tasks.
 */
inline const Poco::UInt64 bdg::core::pipeline::StatisticsCounter::getSuccessfulTasksCount() const
{
    Poco::UInt64 total = 0;

    for (int i = 0; i < pluginsCount; ++i)
    {
        total += getSuccessfulPluginTasks(i);
    }

    return total;
}
