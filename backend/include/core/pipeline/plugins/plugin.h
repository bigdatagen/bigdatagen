/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/common.h"                   // Plugin API common
#include "core/pipeline/plugins/task.h"                     // Task
#include "core/pipeline/plugins/taskresult.h"               // TaskResult
#include "core/pipeline/plugins/errortaskresult.h"          // ErrorTaskResult
#include "core/pipeline/plugins/fatalerrortaskresult.h"     // FatalErrorTaskResult
#include "core/pipeline/plugins/successtaskresult.h"        // SuccessTaskResult
#include "core/pipeline/plugins/taskmetadata.h"             // TaskMetadata
#include "core/pipeline/plugins/filetransferer.h"           // FileTransferer
#include "core/pipeline/plugins/valuewithunit.h"            // ValueWithUnit
#include "core/pipeline/plugins/pluginsettingstring.h"      // PluginSettingString
#include "core/pipeline/plugins/pluginsettingint.h"         // PluginSettingInt
#include "core/pipeline/plugins/pluginsettingcheckbox.h"    // PluginSettingCheckbox
#include "core/pipeline/plugins/pluginsettingselect.h"      // PluginSettingSelect
#include "core/pipeline/plugins/pluginsettingarray.h"       // PluginSettingArray
#include "core/pipeline/plugins/pluginsettinggroup.h"       // PluginSettingGroup
#include "core/pipeline/plugins/pluginpostprocessor.h"      // PluginPostprocessor
#include "utils/exception.h"                                // Exceptions
#include "utils/mpiproxy.h"                                 // MPIProxy
#include "utils/logger.h"                                   // Logger

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            namespace plugin
            {
                // Forward declaration.
                struct PluginImpl;

                /**
                 * Base class for pipeline plugins. Each plugin can be placed in the pipeline
                 * and it processes one stage of the pipeline. In the master thread plugin
                 * creates tasks which are then sent to some worker. In the worker thread
                 * plugin then processes those tasks. Each task is intended to process amount
                 * of files. As a result of each run, plugin return instance of TaskResult class.
                 */
                class BDG_PLUGIN_API Plugin : public bdg::core::pipeline::PropertiesObject
                {
                public:
                    /**
                     * Available types of plugins.
                     */
                    enum PluginType
                    {
                        PLUGIN_TYPE_GENERATOR,
                        PLUGIN_TYPE_TRANSFORMER,
                        PLUGIN_TYPE_STORER
                    };

                    /**
                     * Available clean up policies.
                     */
                    enum CleanUpType
                    {
                        CLEAN_UP_NONE,
                        CLEAN_UP_POSTPROCESS,
                        CLEAN_UP_CONTINUOUS,
                        CLEAN_UP_CONTINUOUS_POSTPROCESS
                    };

                    typedef std::vector<std::unique_ptr<PluginSetting>> PluginSettings;

                    Plugin(const std::string &name, const std::string &description, const std::string &defaultOutputPath, const bool outputPathIsMultiplied, const bool outputPathIsAdvancedSetting);
                    virtual ~Plugin();
                    
                    virtual PluginType getType() const = 0;
                    virtual inline PluginPostprocessor getPostprocessor() const;
                    virtual inline bool preservesFilesCounts() const;

                    const PluginSettings &getSettings() const;
                    const std::string &getName() const;
                    const std::string &getDescription() const;
                    const std::vector<std::string> &getOutputPaths() const;
                    const std::vector<std::string> &getOutputPathMasks() const;

                    virtual bdg::core::pipeline::Task *taskFactory() = 0;
                    virtual bdg::core::pipeline::Task *createTask(const bdg::core::pipeline::TaskMetadata *metadata) = 0;
                    virtual bdg::core::pipeline::TaskResult *processTask(const bdg::core::pipeline::Task *task);
                    virtual void taskBeforePassToWorker(bdg::core::pipeline::Task *task);
                    
                    void setSettingValue(const std::string &settingName, const Poco::Dynamic::Var &settingJson);
                    void initialize(bdg::utils::MPIProxy &mpi, Poco::Logger &logger, const FileTransferer &fileTransferer, const int pluginRelativePosition, const size_t pluginsCountInPipeline, const CleanUpType cleanUpType, const bool canUseGlobalFileIds);
                    void initOutputPaths();
                    void createOutputPaths() const;
                    void cleanUp();
                    void terminatePlugin();
                    
                    const std::string getTypeString() const;
                    const std::string getOutputPath() const;
                    const std::string getOutputPathMask() const;
                    
                    virtual void serialize(Poco::JSON::Object &json) const override;

                    /**
                     * Name of the setting which contains path where generated file are stored.
                     */
                    static const std::string OUTPUT_PATH_SETTING;

                    /**
                     * Maximum percent of files that failed to be processed. If more files fail, a task
                     * is considered as erroneous and ErrorTaskResult is returned from the plugin.
                     */
                    static const double MAX_ERROR_FILES_RATIO;

                protected:
                    virtual void serializeAdditional(Poco::JSON::Object &json) const override;
                    virtual void unserializeAdditional(const Poco::JSON::Object &json) override;

                    virtual void init();
                    virtual void clean();
                    virtual void terminate();
                    virtual void taskStart(const bdg::core::pipeline::Task *task);
                    virtual void taskEnd(const bdg::core::pipeline::Task *task, const bdg::core::pipeline::TaskResult *result);
                    virtual bdg::core::pipeline::TaskResult *processTaskImpl(const bdg::core::pipeline::Task *task) = 0;
                    bool createOutputDirectories(const Poco::UInt64 fileIdFrom, const size_t filesCount) const;

                    std::string convertToDashes(const std::string &str) const;
                    std::string getFileId(const int pluginIndex, const int rank, const Poco::UInt64 fileId) const;

                    PluginSetting *getSetting(const std::string &settingName) const;
                    std::string getSettingValue(const std::string &settingName) const;
                    int getSettingValueInt(const std::string &settingName) const;
                    bool getSettingValueCheckbox(const std::string &settingName) const;
                    
                    void addSetting(PluginSetting *setting);
                    inline bool canContinue(const bdg::core::pipeline::Task *task, const Poco::UInt64 processedFiles, const Poco::UInt64 totalBytes, const Poco::Timestamp &startTime) const;

                    const bdg::utils::MPIProxy &getMpi() const;
                    const bdg::core::pipeline::FileTransferer &getFileTransferer() const;
                    Poco::Logger &getLogger() const;
                    const int getPluginRelativePosition() const;
                    const size_t getPluginsCountInPipeline() const;
                    const CleanUpType getCleanUpType() const;
                    const bool useGlobalFileIds() const;

                    /**
                     * Id of the next generated file.
                     */
                    Poco::UInt64 nextFileIdLocal = 1;

                private:
                    const std::string getCommonOutputPath(const std::string &outputPathMask) const;

                    /**
                     * Implementation of the plugin - PIMPL design pattern.
                     */
                    std::unique_ptr<PluginImpl> impl;
                };
            }
        }
    }
}

/**
 * Returns the plugin post-processor. Each plugin can define one plugin that is its post-processor.
 * This plugin is then automatically placed into the pipeline and it behaves as it was defined by
 * the user (creator of the pipeline). In default no plugin post-processor is returned by this method.
 *
 * @return Plugin post-processor.
 */
inline bdg::core::pipeline::plugin::PluginPostprocessor bdg::core::pipeline::plugin::Plugin::getPostprocessor() const
{
    return PluginPostprocessor::none();
}

/**
 * Returns whether plugin preserves count of files given to process. If it generates exactly the same
 * amount of files as it was given, then this method should return true; itherwise is should return false.
 * Generator and storer plugins should always return true, so this method is relevant only for transformer
 * plugins.
 *
 * @return Does the plugin preserve count of files given to process.
 */
inline bool bdg::core::pipeline::plugin::Plugin::preservesFilesCounts() const
{
    return true;
}

/**
 * Returns whether plugin can continue in processing the current task.
 *
 * @param task Processed task.
 * @param processedFiles Number of processed files so far in the task.
 * @param totalBytes Size of processed files so far in the task.
 * @param startTime Time stamp when the task started to be processed.
 *
 * @return Can we continue in processing the current task?
 */
inline bool bdg::core::pipeline::plugin::Plugin::canContinue(const bdg::core::pipeline::Task *task, const Poco::UInt64 processedFiles, const Poco::UInt64 totalBytes, const Poco::Timestamp &startTime) const
{
    return (task->getFilesLimit() == 0 || processedFiles < task->getFilesLimit())
        && (task->getBytesLimit() == 0 || totalBytes < task->getBytesLimit())
        && (task->getSecondsLimit() == 0 || convertMicrosecondsToSeconds(startTime.elapsed()) < task->getSecondsLimit());
}

