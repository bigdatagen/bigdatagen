/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/pluginsetting.h"        // PluginSetting

#include <memory>       // std::unique_ptr
#include <vector>       // std::vector

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            namespace plugin
            {
                /**
                 * Setting of pipeline plugin which is able to multiply nested setting.
                 * On frontend it appears as a setting with the "+" button.
                 */
                class BDG_PLUGIN_API PluginSettingArray : public PluginSetting
                {
                public:
                    typedef std::vector<std::unique_ptr<PluginSetting>> SettingsCollection;

                    explicit PluginSettingArray(PluginSetting *multipliedSetting);
                    virtual ~PluginSettingArray() = default;

                    inline const SettingsCollection &getSettings() const;

                    virtual inline SettingType getType() const override;
                    virtual PluginSetting *clone() const override;

                    virtual void setValue(const Poco::Dynamic::Var &valueJson) override;
                    virtual bool containsScalars() const;
                    virtual void serialize(Poco::JSON::Object &json) const override;

                private:
                    /**
                     * Setting that is multiplied on the frontend.
                     */
                    const std::unique_ptr<PluginSetting> multipliedSetting;

                    /**
                     * Multiplied settings with their values.
                     */
                    SettingsCollection settings;
                };
            }
        }
    }
}

/**
 * Returns setting contained in the array.
 *
 * @return Setting contained in the array.
 */
inline const bdg::core::pipeline::plugin::PluginSettingArray::SettingsCollection &bdg::core::pipeline::plugin::PluginSettingArray::getSettings() const
{
    return settings;
}

/**
 * Returns the type of the setting.
 *
 * @return Type of the setting.
 */
inline bdg::core::pipeline::plugin::PluginSetting::SettingType bdg::core::pipeline::plugin::PluginSettingArray::getType() const
{
    return SettingType::SETTING_TYPE_ARRAY;
}
