/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Utils
 * @author   Big Data Generator Team
 */

#pragma once

#if defined(_WIN32) && defined(_DLL)
    #if defined(BDG_PLUGIN_API_EXPORTS)
        #define BDG_PLUGIN_API __declspec(dllexport)
    #else
        #define BDG_PLUGIN_API __declspec(dllimport)
    #endif
#else
    #define BDG_PLUGIN_API
#endif
