/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/taskmetadata.h"             // TaskMetadata
#include "core/pipeline/plugins/successtaskresult.h"        // SuccessTaskResult

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Metadata for the transformer task containing the information for the task.
             * Instance of this class is given to the transformer plugin in the master
             * thread when the transformer task is created.
             */
            class BDG_PLUGIN_API TransformerTaskMetadata : public TaskMetadata
            {
            public:
                TransformerTaskMetadata() = default;
                explicit TransformerTaskMetadata(const Properties &properties);
                explicit TransformerTaskMetadata(const SuccessTaskResult &taskRestult);
                virtual ~TransformerTaskMetadata() = default;

                virtual inline MetadataType getType() const override;
                inline void setResultWorkerFileMask(const std::string &resultWorkerFileMask);
                inline const std::string &getResultWorkerFileMask() const;
                inline void setResultWorkerRank(const int resultWorkerRank);
                inline const int getResultWorkerRank() const;
                inline void setResultWorkerFileFrom(const Poco::UInt64 resultWorkerFileFrom);
                inline const Poco::UInt64 getResultWorkerFileFrom() const;
                inline void setResultWorkerFilesCount(const size_t resultWorkerFilesCount);
                inline const size_t getResultWorkerFilesCount() const;
                inline void setResultWorkerBytes(const Poco::UInt64 resultWorkerFilesCount);
                inline const Poco::UInt64 getResultWorkerBytes() const;

            private:
                /**
                 * Mask of the file path where the files that should be processed are stored.
                 */
                std::string resultWorkerFileMask;

                /**
                 * Rank of the worker which holds the files that should be processed.
                 */
                int resultWorkerRank;

                /**
                 * Id of the first file that should be processed.
                 */
                Poco::UInt64 resultWorkerFileFrom;

                /**
                 * Total number of files that should be processed.
                 */
                size_t resultWorkerFilesCount;

                /**
                 * Overall size of all the files that should be processed.
                 */
                Poco::UInt64 resultWorkerBytes;
            };
        }
    }
}

/**
 * Returns type of the task metadata.
 *
 * @return Type of the task metadata.
 */
inline bdg::core::pipeline::TaskMetadata::MetadataType bdg::core::pipeline::TransformerTaskMetadata::getType() const
{
    return MetadataType::METADATA_TYPE_TRANSFORMER;
}

/**
 * Sets the mask of the file path where the files that should be processed are stored.
 *
 * @param resultWorkerFileMask Mask of the file path where the files that should be processed are stored.
 */
inline void bdg::core::pipeline::TransformerTaskMetadata::setResultWorkerFileMask(const std::string &resultWorkerFileMask)
{
    this->resultWorkerFileMask = resultWorkerFileMask;
}

/**
 * Returns the mask of the file path where the files that should be processed are stored.
 *
 * @return Mask of the file path where the files that should be processed are stored.
 */
inline const std::string &bdg::core::pipeline::TransformerTaskMetadata::getResultWorkerFileMask() const
{
    return resultWorkerFileMask;
}

/**
 * Sets the rank of the worker which holds the files that should be processed.
 *
 * @param resultWorkerRank Rank of the worker which holds the files that should be processed.
 */
inline void bdg::core::pipeline::TransformerTaskMetadata::setResultWorkerRank(const int resultWorkerRank)
{
    this->resultWorkerRank = resultWorkerRank;
}

/**
 * Returns the rank of the worker which holds the files that should be processed.
 *
 * @return Rank of the worker which holds the files that should be processed.
 */
inline const int bdg::core::pipeline::TransformerTaskMetadata::getResultWorkerRank() const
{
    return resultWorkerRank;
}

/**
 * Sets the id of the first file that should be processed.
 *
 * @param resultWorkerFileFrom Id of the first file that should be processed.
 */
inline void bdg::core::pipeline::TransformerTaskMetadata::setResultWorkerFileFrom(const Poco::UInt64 resultWorkerFileFrom)
{
    this->resultWorkerFileFrom = resultWorkerFileFrom;
}

/**
 * Returns the id of the first file that should be processed.
 *
 * @return Id of the first file that should be processed.
 */
inline const Poco::UInt64 bdg::core::pipeline::TransformerTaskMetadata::getResultWorkerFileFrom() const
{
    return resultWorkerFileFrom;
}

/**
 * Sets the total number of files that should be processed.
 *
 * @param resultWorkerFilesCount Total number of files that should be processed.
 */
inline void bdg::core::pipeline::TransformerTaskMetadata::setResultWorkerFilesCount(const size_t resultWorkerFilesCount)
{
    this->resultWorkerFilesCount = resultWorkerFilesCount;
}

/**
 * Returns the total number of files that should be processed.
 *
 * @return Total number of files that should be processed.
 */
inline const size_t bdg::core::pipeline::TransformerTaskMetadata::getResultWorkerFilesCount() const
{
    return resultWorkerFilesCount;
}

/**
 * Sets the overall size of all the files that should be processed.
 *
 * @param resultWorkerBytes Overall size of all the files that should be processed.
 */
inline void bdg::core::pipeline::TransformerTaskMetadata::setResultWorkerBytes(const Poco::UInt64 resultWorkerBytes)
{
    this->resultWorkerBytes = resultWorkerBytes;
}

/**
 * Returns the overall size of all the files that should be processed.
 *
 * @return Overall size of all the files that should be processed.
 */
inline const Poco::UInt64 bdg::core::pipeline::TransformerTaskMetadata::getResultWorkerBytes() const
{
    return resultWorkerBytes;
}
