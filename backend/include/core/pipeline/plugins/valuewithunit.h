/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/common.h"       // Plugin API common

#include <Poco/Types.h>         // Poco::Int64

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Class which represents a value with a unit. E.g. "2 seconds", "14 files", etc.
             */
            class BDG_PLUGIN_API ValueWithUnit
            {
            public:
                /**
                 * Types of supported units.
                 */
                enum Unit
                {
                    UNIT_BYTE,
                    UNIT_FILE,
                    UNIT_SECOND,
                    UNIT_UNDEFINED,
                    UNIT_INVALID
                };

                ValueWithUnit();
                explicit ValueWithUnit(const std::string &definition);
                ~ValueWithUnit() = default;

                inline bool isUndefined() const;
                inline const Poco::Int64 getValue() const;
                inline void setUnit(const Unit unit);
                inline Unit getUnit() const;
                
                /**
                 * Constant indicating that the value is not defined.
                 */
                static const Poco::Int64 VALUE_UNDEFINED;

            private:
                void multiplyValue(const int base, const int times);

                /**
                 * Value.
                 */
                Poco::Int64 value;

                /**
                 * Unit.
                 */
                Unit unit;
            };

            inline size_t convertMicrosecondsToSeconds(Poco::Int64 microseconds);
            inline double convertSize(double size, std::string &unit);
            inline std::string convertSize(double size);
        }
    }
}

/**
 * Returns whether the value is undefined.
 *
 * @return Is the value undefined?
 */
inline bool bdg::core::pipeline::ValueWithUnit::isUndefined() const
{
    return value == VALUE_UNDEFINED || unit == Unit::UNIT_UNDEFINED;
}

/**
 * Returns the value.
 *
 * @return Value.
 */
inline const Poco::Int64 bdg::core::pipeline::ValueWithUnit::getValue() const
{
    return value;
}

/**
 * Sets the unit.
 *
 * @param unit Unit.
 */
inline void bdg::core::pipeline::ValueWithUnit::setUnit(const ValueWithUnit::Unit unit)
{
    this->unit = unit;
}

/**
 * Returns the unit.
 *
 * @return Unit.
 */
inline bdg::core::pipeline::ValueWithUnit::Unit bdg::core::pipeline::ValueWithUnit::getUnit() const
{
    return unit;
}

/**
 * Converts microseconds to seconds.
 *
 * @param microseconds Microseconds.
 *
 * @return Seconds.
 */
inline size_t bdg::core::pipeline::convertMicrosecondsToSeconds(Poco::Int64 microseconds)
{
    return (size_t)(microseconds / 1000000);
}

/**
 * Converts the size given in bytes.
 *
 * @param size Size in bytes.
 * @param unit Output parameter for the resulting size unit.
 *
 * @return Converted size to the unit outputed in the parameter "unit".
 */
inline double bdg::core::pipeline::convertSize(double size, std::string &unit)
{
    unit = "bytes";

    if (size > 1024)
    {
        size /= 1024;
        unit = "kB";
    }

    if (size > 1024)
    {
        size /= 1024;
        unit = "MB";
    }

    if (size > 1024)
    {
        size /= 1024;
        unit = "GB";
    }

    if (size > 1024)
    {
        size /= 1024;
        unit = "TB";
    }

    if (size > 1024)
    {
        size /= 1024;
        unit = "PB";
    }

    return size;
}

/**
 * Converts the size given in bytes.
 *
 * @param sizeSize in bytes.
 *
 * @return Converted size with its unit.
 */
inline std::string bdg::core::pipeline::convertSize(double size)
{
    std::string unit;
    size = convertSize(size, unit);

    return std::to_string(size) + " " + unit;
}
