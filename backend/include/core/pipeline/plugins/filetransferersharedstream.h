/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/filetransfererstream.h"         // FileTransfererStream

#include <Poco/File.h>      // Poco::File

#include <fstream>          // std::ifstream

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Stream for reading files from the shared file system.
             */
            class BDG_PLUGIN_API FileTransfererSharedStream : public FileTransfererStream
            {
            public:
                FileTransfererSharedStream(const std::string &fileName, const Poco::UInt64 fileId);
                virtual ~FileTransfererSharedStream();

                virtual inline std::istream &getStream() override;
                virtual inline const std::string &getFileName() const override;
                virtual inline const Poco::UInt64 getSize() const override;
                virtual inline const Poco::UInt64 getFileId() const override;

            private:
                /**
                 * Stream for reading the file.
                 */
                std::ifstream inputStream;

                /**
                 * Path to the file.
                 */
                const std::string fileName;

                /**
                 * Id of the file.
                 */
                const Poco::UInt64 fileId;
            };
        }
    }
}

/**
 * Returns stream for reading the file.
 *
 * @return Stream for reading the file.
 */
inline std::istream &bdg::core::pipeline::FileTransfererSharedStream::getStream()
{
    return inputStream;
}

/**
 * Returns path to the file.
 *
 * @return Path to the file.
 */
inline const std::string &bdg::core::pipeline::FileTransfererSharedStream::getFileName() const
{
    return fileName;
}

/**
 * Returns size of the file.
 *
 * @return Size of the file.
 */
inline const Poco::UInt64 bdg::core::pipeline::FileTransfererSharedStream::getSize() const
{
    return Poco::File(fileName).getSize();
}

/**
 * Returns id of the file.
 *
 * @return Id of the file.
 */
inline const Poco::UInt64 bdg::core::pipeline::FileTransfererSharedStream::getFileId() const
{
    return fileId;
}
