/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/plugin.h"                   // Plugin
#include "core/pipeline/plugins/storertaskmetadata.h"       // StorerTaskMetadata
#include "core/pipeline/plugins/storertask.h"               // StorerTask

#include <Poco/Path.h>          // Poco::Path
#include <Poco/File.h>          // Poco::File

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            namespace plugin
            {
                /**
                 * Storer plugins are meant to be used as the last plugins in the pipeline.
                 * They store files given in a form of tasks to their final destination. For
                 * example it can be a file system, cloud storage, socket, etc.
                 */
                class BDG_PLUGIN_API StorerPlugin : public Plugin
                {
                public:
                    StorerPlugin(const std::string &name, const std::string &description, const std::string &extension = "", const bool outputPathIsMultiplied = false);
                    virtual ~StorerPlugin() = default;

                    virtual inline PluginType getType() const override;
                    virtual bdg::core::pipeline::Task *taskFactory() override;
                    virtual bdg::core::pipeline::Task *createTask(const bdg::core::pipeline::TaskMetadata *metadata) override;
                    
                protected:
                    virtual bdg::core::pipeline::TaskResult *processTaskImpl(const bdg::core::pipeline::Task *task) override;
                    virtual void processOneFile(const bdg::core::pipeline::TransformerTask *task, std::unique_ptr<FileTransfererStream> &inputFileStream, const Poco::Path &inputFilePath) = 0;

                    bdg::core::pipeline::StorerTaskMetadata *convertMetadata(const bdg::core::pipeline::TaskMetadata *metadata) const;
                    bdg::core::pipeline::StorerTask *convertTask(const bdg::core::pipeline::Task *task) const;
                };
            }
        }
    }
}

/**
 * Returns type of the plugin.
 *
 * @return Type of the plugin.
 */
inline bdg::core::pipeline::plugin::Plugin::PluginType bdg::core::pipeline::plugin::StorerPlugin::getType() const
{
    return PluginType::PLUGIN_TYPE_STORER;
}
