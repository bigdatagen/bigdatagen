/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/pluginsetting.h"        // PluginSetting

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            namespace plugin
            {
                /**
                 * String setting of pipeline plugin. On frontend it is rendered as an input
                 * acception only numbers.
                 */
                class BDG_PLUGIN_API PluginSettingInt : public PluginSetting
                {
                public:
                    PluginSettingInt(const std::string &name, const std::string &description = "", const int defaultValue = 0, const int min = 0, const int max = -1, const std::string &help = "", const bool isAdvancedSetting = false);
                    virtual ~PluginSettingInt() = default;

                    inline int getDefaultValueInt() const;
                    inline int getValueInt() const;
                    inline int getMin() const;
                    inline int getMax() const;
                    inline void setValue(const int value);

                    virtual inline SettingType getType() const override;
                    virtual PluginSetting *clone() const override;
                    virtual std::string validate(const std::string &value) const override;
                    int validate(const int value) const;

                    virtual void serialize(Poco::JSON::Object &json) const override;

                private:
                    int validate(const int value, const int minArg, const int maxArg) const;

                    /**
                     * Setting minimum allowed value.
                     */
                    const int min;

                    /**
                     * Setting maximum allowed value.
                     */
                    const int max;
                };
            }
        }
    }
}

/**
 * Returns the setting default value as an integer.
 *
 * @return Setting default value.
 */
inline int bdg::core::pipeline::plugin::PluginSettingInt::getDefaultValueInt() const
{
    return std::stoi(getDefaultValue());
}

/**
 * Returns the setting value as an integer.
 *
 * @return Setting value.
 */
inline int bdg::core::pipeline::plugin::PluginSettingInt::getValueInt() const
{
    return std::stoi(getValue());
}

/**
 * Returns setting minimum allowed value.
 *
 * @return Setting minimum allowed value.
 */
inline int bdg::core::pipeline::plugin::PluginSettingInt::getMin() const
{
    return min;
}

/**
 * Returns setting maximum allowed value.
 *
 * @return Setting maximum allowed value.
 */
inline int bdg::core::pipeline::plugin::PluginSettingInt::getMax() const
{
    return max;
}

/**
 * Sets the setting value.
 *
 * @param value Setting value.
 */
inline void bdg::core::pipeline::plugin::PluginSettingInt::setValue(const int value)
{
    PluginSetting::setValue(std::to_string(value));
}

/**
 * Returns the type of the setting.
 *
 * @return Type of the setting.
 */
inline bdg::core::pipeline::plugin::PluginSetting::SettingType bdg::core::pipeline::plugin::PluginSettingInt::getType() const
{
    return SettingType::SETTING_TYPE_INT;
}
