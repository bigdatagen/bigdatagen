/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/pluginsetting.h"        // PluginSetting

#include <map>          // std::map

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            namespace plugin
            {
                /**
                 * String setting of pipeline plugin. On frontend it is rendered as a select
                 * box which has options defined in the constructor of this class as argument "items".
                 */
                class BDG_PLUGIN_API PluginSettingSelect : public PluginSetting
                {
                public:
                    PluginSettingSelect(const std::string &name, const std::map<std::string, std::string> &items, const std::string &description = "", const std::string &defaultValue = "", const std::string &help = "", const bool isAdvancedSetting = false);
                    virtual ~PluginSettingSelect() = default;

                    inline const std::map<std::string, std::string> &getItems() const;

                    virtual inline SettingType getType() const override;
                    virtual PluginSetting *clone() const override;
                    virtual std::string validate(const std::string &value) const override;
                    
                    virtual void serialize(Poco::JSON::Object &json) const override;

                private:
                    std::string validate(const std::string &value, const std::map<std::string, std::string> &itemsArg) const;

                    /**
                     * Items from which a user can select a value. It defines select box options on the frontend.
                     */
                    const std::map<std::string, std::string> items;
                };
            }
        }
    }
}

/**
 * Returns items from which a user can select a value.
 *
 * @return Items from which a user can select a value.
 */
inline const std::map<std::string, std::string> &bdg::core::pipeline::plugin::PluginSettingSelect::getItems() const
{
    return items;
}

/**
 * Returns the type of the setting.
 *
 * @return Type of the setting.
 */
inline bdg::core::pipeline::plugin::PluginSetting::SettingType bdg::core::pipeline::plugin::PluginSettingSelect::getType() const
{
    return SettingType::SETTING_TYPE_SELECT;
}
