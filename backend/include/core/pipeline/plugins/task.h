/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/propertiesobject.h"         // PropertiesObject

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Base class for tasks. Tasks are sent from the master to workers. Each
             * instance of this class describes a part of the job that should be processed
             * on the worker.
             */
            class BDG_PLUGIN_API Task : public PropertiesObject
            {
            public:
                /**
                 * Available types of tasks.
                 */
                enum TaskType
                {
                    TASK_TYPE_GENERATOR,
                    TASK_TYPE_TRANSFORMER,
                    TASK_TYPE_STORER,
                    TASK_TYPE_IDLE
                };

                Task() = default;
                explicit Task(const Properties &properties);
                virtual ~Task() = default;

                virtual TaskType getType() const = 0;

                inline void setPluginIndex(const int pluginIndex);
                inline const int getPluginIndex() const;
                inline void restarted();
                inline const size_t getRestarts() const;
                inline void setFilesCount(const size_t filesCount);
                inline const size_t getFilesCount() const;
                inline void setFileFrom(const Poco::UInt64 fileFrom);
                inline const Poco::UInt64 getFileFrom() const;

                inline void setPromissedFiles(const size_t bytes);
                inline const size_t getPromissedFiles() const;
                inline void setPromissedBytes(const Poco::UInt64 bytes);
                inline const Poco::UInt64 getPromissedBytes() const;

                inline void setFilesLimit(const Poco::UInt64 filesLimit);
                inline const Poco::UInt64 getFilesLimit() const;
                inline void setBytesLimit(const Poco::UInt64 bytesLimit);
                inline const Poco::UInt64 getBytesLimit() const;
                inline void setSecondsLimit(const time_t secondsLimit);
                inline const time_t getSecondsLimit() const;
                
            protected:
                virtual void serializeAdditional(Poco::JSON::Object &json) const override;
                virtual void unserializeAdditional(const Poco::JSON::Object &json) override;

            private:
                /**
                 * Index of the pipeline plugin that should process the task.
                 */
                int pluginIndex = 0;

                /**
                 * How many times was the task restarted so far.
                 */
                size_t restarts = 0;

                /**
                 * Total number of files that should be processed in the task.
                 */
                size_t filesCount = 0;

                /**
                 * Id of the first file that should be processed in the task.
                 */
                Poco::UInt64 fileFrom = 0;

                /**
                 * How many files have the task promissed to master to process.
                 */
                size_t promissedFiles = 0;

                /**
                 * What size of files have the task promissed to master to process.
                 */
                Poco::UInt64 promissedBytes = 0;

                /**
                 * Maximum number of files that the task can process. This value is relevant
                 * near the end of a job.
                 */
                Poco::UInt64 filesLimit = 0;

                /**
                 * Maximum number of bytes that the task can process. This value is relevant
                 * near the end of a job.
                 */
                Poco::UInt64 bytesLimit = 0;

                /**
                 * What is the maximum allowed length of the task. Master sets this value and
                 * it is relevant near the end of a job, so the job does not run longer than
                 * was demanded.
                 */
                time_t secondsLimit = 0;
            };
        }
    }
}

/**
 * Sets the index of the pipeline plugin that should process the task.
 *
 * @param pluginIndex Index of the pipeline plugin that should process the task.
 */
inline void bdg::core::pipeline::Task::setPluginIndex(const int pluginIndex)
{
    this->pluginIndex = pluginIndex;
}

/**
 * Returns the index of the pipeline plugin that should process the task.
 *
 * @return Index of the pipeline plugin that should process the task.
 */
inline const int bdg::core::pipeline::Task::getPluginIndex() const
{
    return pluginIndex;
}

/**
 * This method is called when the task is restarted.
 */
inline void bdg::core::pipeline::Task::restarted()
{
    ++restarts;
}

/**
 * Returns how many times was the task restarted so far.
 *
 * @return How many times was the task restarted so far.
 */
inline const size_t bdg::core::pipeline::Task::getRestarts() const
{
    return restarts;
}

/**
 * Sets the total number of files that should be processed in the task.
 *
 * @param filesCount Total number of files that should be processed in the task.
 */
inline void bdg::core::pipeline::Task::setFilesCount(const size_t filesCount)
{
    this->filesCount = filesCount;
}

/**
 * Returns the total number of files that should be processed in the task.
 *
 * @return Total number of files that should be processed in the task.
 */
inline const size_t bdg::core::pipeline::Task::getFilesCount() const
{
    return filesCount;
}

/**
 * Sets the id of the first file that should be processed in the task.
 *
 * @param fileFrom Id of the first file that should be processed in the task.
 */
inline void bdg::core::pipeline::Task::setFileFrom(const Poco::UInt64 fileFrom)
{
    this->fileFrom = fileFrom;
}

/**
 * Returns the id of the first file that should be processed in the task.
 *
 * @return Id of the first file that should be processed in the task.
 */
inline const Poco::UInt64 bdg::core::pipeline::Task::getFileFrom() const
{
    return fileFrom;
}

/**
 * Sets how many files have the task promissed to master to process.
 *
 * @param promissedFiles How many files have the task promissed to master to process.
 */
inline void bdg::core::pipeline::Task::setPromissedFiles(const size_t promissedFiles)
{
    this->promissedFiles = promissedFiles;
}

/**
 * Returns how many files have the task promissed to master to process.
 *
 * @return How many files have the task promissed to master to process.
 */
inline const size_t bdg::core::pipeline::Task::getPromissedFiles() const
{
    return promissedFiles;
}

/**
 * Sets the size of files that the task have promissed to master to process.
 *
 * @param promissedBytes What size of files have the task promissed to master to process.
 */
inline void bdg::core::pipeline::Task::setPromissedBytes(const Poco::UInt64 promissedBytes)
{
    this->promissedBytes = promissedBytes;
}

/**
 * Returns the size of files that the task have promissed to master to process.
 *
 * @return What size of files have the task promissed to master to process.
 */
inline const Poco::UInt64 bdg::core::pipeline::Task::getPromissedBytes() const
{
    return promissedBytes;
}

/**
 * Sets the maximum number of files that the task can process.
 *
 * @param filesLimit Maximum number of files that the task can process.
 */
inline void bdg::core::pipeline::Task::setFilesLimit(const Poco::UInt64 filesLimit)
{
    this->filesLimit = filesLimit;
}

/**
 * Returns the maximum number of files that the task can process.
 *
 * @return Maximum number of files that the task can process.
 */
inline const Poco::UInt64 bdg::core::pipeline::Task::getFilesLimit() const
{
    return filesLimit;
}

/**
 * Sets the maximum number of bytes that the task can process.
 *
 * @param bytesLimit Maximum number of bytes that the task can process.
 */
inline void bdg::core::pipeline::Task::setBytesLimit(const Poco::UInt64 bytesLimit)
{
    this->bytesLimit = bytesLimit;
}

/**
 * Returns the maximum number of bytes that the task can process.
 *
 * @return Maximum number of bytes that the task can process.
 */
inline const Poco::UInt64 bdg::core::pipeline::Task::getBytesLimit() const
{
    return bytesLimit;
}

/**
 * Sets the maximum allowed length of the task.
 *
 * @param secondsLimit What is the maximum allowed length of the task.
 */
inline void bdg::core::pipeline::Task::setSecondsLimit(const time_t secondsLimit)
{
    this->secondsLimit = secondsLimit;
}

/**
 * Returns the maximum allowed length of the task.
 *
 * @return What is the maximum allowed length of the task.
 */
inline const time_t bdg::core::pipeline::Task::getSecondsLimit() const
{
    return secondsLimit;
}
