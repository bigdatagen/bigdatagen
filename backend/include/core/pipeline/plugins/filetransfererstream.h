/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/common.h"           // Plugin API common

#include <Poco/Types.h>     // Poco::UInt64

#include <istream>          // std::istream
#include <string>           // std::string

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Base class for stream for reading files. It has only one descendant
             * for reading files from the shared file system. But we would like
             * to implement a new class for reading files from file systems that
             * are not shared.
             */
            class BDG_PLUGIN_API FileTransfererStream
            {
            public:
                FileTransfererStream() = default;
                virtual ~FileTransfererStream() = default;
                    
                virtual std::istream &getStream() = 0;
                virtual const std::string &getFileName() const = 0;
                virtual const Poco::UInt64 getSize() const = 0;
                virtual const Poco::UInt64 getFileId() const = 0;
            };
        }
    }
}
