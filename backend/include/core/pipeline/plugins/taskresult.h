/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/propertiesobject.h"       // PropertiesObject

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Base class for task results. Instance of this class is sent from
             * a worker to the master when a task is finished.
             */
            class BDG_PLUGIN_API TaskResult : public PropertiesObject
            {
            public:
                /**
                 * Available types of task results.
                 */
                enum ResultType
                {
                    RESULT_TYPE_SUCCESSS,
                    RESULT_TYPE_ERROR,
                    RESULT_TYPE_FATAL_ERROR
                };

                TaskResult() = default;
                virtual ~TaskResult() = default;

                virtual ResultType getType() const = 0;

                inline void setPluginIndex(const int pluginIndex);
                inline int getPluginIndex() const;
                inline void setRank(const int rank);
                inline int getRank() const;
                inline void setFileFrom(const Poco::UInt64 fileFrom);
                inline const Poco::UInt64 getFileFrom() const;
                inline void setFilesCount(const size_t filesCount);
                inline const size_t getFilesCount() const;

            protected:
                virtual void serializeAdditional(Poco::JSON::Object &json) const override;
                virtual void unserializeAdditional(const Poco::JSON::Object &json) override;

            private:
                /**
                 * Index of the plugin that prossed the task.
                 */
                int pluginIndex = 0;

                /**
                 * Rank of the currently running MPI thread.
                 */
                int rank = 0;

                /**
                 * Id of the first processed file.
                 */
                Poco::UInt64 fileFrom = 0;

                /**
                 * Total number of files processed in the task.
                 */
                size_t filesCount = 0;
            };
        }
    }
}

/**
 * Sets the index of the plugin that prossed the task.
 *
 * @param pluginIndex Index of the plugin that prossed the task.
 */
inline void bdg::core::pipeline::TaskResult::setPluginIndex(const int pluginIndex)
{
    this->pluginIndex = pluginIndex;
}

/**
 * Returns the index of the plugin that prossed the task.
 *
 * @return Index of the plugin that prossed the task.
 */
inline int bdg::core::pipeline::TaskResult::getPluginIndex() const
{
    return pluginIndex;
}

/**
 * Sets the rank of the currently running MPI thread.
 *
 * @param rank Rank of the currently running MPI thread.
 */
inline void bdg::core::pipeline::TaskResult::setRank(const int rank)
{
    this->rank = rank;
}

/**
 * Returns the rank of the currently running MPI thread.
 *
 * @return Rank of the currently running MPI thread.
 */
inline int bdg::core::pipeline::TaskResult::getRank() const
{
    return rank;
}

/**
 * Sets the id of the first processed file.
 *
 * @param fileFrom Id of the first processed file.
 */
inline void bdg::core::pipeline::TaskResult::setFileFrom(const Poco::UInt64 fileFrom)
{
    this->fileFrom = fileFrom;
}

/**
 * Returns the id of the first processed file.
 *
 * @return Id of the first processed file.
 */
inline const Poco::UInt64 bdg::core::pipeline::TaskResult::getFileFrom() const
{
    return fileFrom;
}

/**
 * Sets the total number of files processed in the task.
 *
 * @param filesCount Total number of files processed in the task.
 */
inline void bdg::core::pipeline::TaskResult::setFilesCount(const size_t filesCount)
{
    this->filesCount = filesCount;
}

/**
 * Returns the total number of files processed in the task.
 *
 * @return Total number of files processed in the task.
 */
inline const size_t bdg::core::pipeline::TaskResult::getFilesCount() const
{
    return filesCount;
}
