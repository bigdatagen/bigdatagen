/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/taskresult.h"       // TaskResult

#include <Poco/Types.h>         // Poco::UInt64

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             *
             */
            class BDG_PLUGIN_API SuccessTaskResult : public TaskResult
            {
            public:
                SuccessTaskResult() = default;
                SuccessTaskResult(const std::string &fileMask, const Poco::UInt64 fileFrom, const size_t filesCount, const Poco::UInt64 bytes, const Poco::UInt64 totalTime);
                virtual ~SuccessTaskResult() = default;

                virtual inline ResultType getType() const override;

                inline const std::string &getFileMask() const;
                inline const Poco::UInt64 getBytes() const;
                inline const Poco::UInt64 getTotalTime() const;

                inline void setPluginState(const Poco::JSON::Object &pluginState);
                inline const Poco::JSON::Object &getPluginState() const;

            protected:
                virtual void serializeAdditional(Poco::JSON::Object &json) const override;
                virtual void unserializeAdditional(const Poco::JSON::Object &json) override;

            private:
                /**
                 * Mask of the path where the files that were processed are stored.
                 */
                std::string fileMask;

                /**
                 * Size of the processed files in bytes.
                 */
                Poco::UInt64 bytes;

                /**
                 * Total time of the task run in milliseconds.
                 */
                Poco::UInt64 totalTime;

                /**
                 * JSON reprezenting the current state of the plugin that processed the task.
                 * This state is needed by the master in order to store a checkpoint. A plugin
                 * can be restored from that state when a job crashes.
                 */
                Poco::JSON::Object pluginState;
            };
        }
    }
}

/**
 * Returns type of the task result.
 *
 * @return Type of the task result.
 */
inline bdg::core::pipeline::TaskResult::ResultType bdg::core::pipeline::SuccessTaskResult::getType() const
{
    return ResultType::RESULT_TYPE_SUCCESSS;
}

/**
 * Returns the mask of the path where the files that were processed are stored.
 *
 * @return Mask of the path where the files that were processed are stored.
 */
inline const std::string &bdg::core::pipeline::SuccessTaskResult::getFileMask() const
{
    return fileMask;
}

/**
 * Returns the size of the processed files in bytes.
 *
 * @return Size of the processed files in bytes.
 */
inline const Poco::UInt64 bdg::core::pipeline::SuccessTaskResult::getBytes() const
{
    return bytes;
}

/**
 * Returns the total time of the task run in milliseconds.
 *
 * @return Total time of the task run in milliseconds.
 */
inline const Poco::UInt64 bdg::core::pipeline::SuccessTaskResult::getTotalTime() const
{
    return totalTime;
}

/**
 * Sets the JSON reprezenting the current state of the plugin that processed the task.
 *
 * @param pluginState JSON reprezenting the current state of the plugin that processed the task.
 */
inline void bdg::core::pipeline::SuccessTaskResult::setPluginState(const Poco::JSON::Object &pluginState)
{
    this->pluginState = pluginState;
}

/**
 * Returns the JSON reprezenting the current state of the plugin that processed the task.
 *
 * @return JSON reprezenting the current state of the plugin that processed the task.
 */
inline const Poco::JSON::Object &bdg::core::pipeline::SuccessTaskResult::getPluginState() const
{
    return pluginState;
}
