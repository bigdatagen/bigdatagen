/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Transformer
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/bdgapi.h"       // Plugin API
#include <Poco/Zip/Compress.h>                  // Poco::Zip::Compress

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            namespace plugin
            {
                /**
                 * Pipeline plugin which creates zip archives from files generated in the
                 * previous stage of the pipeline. A number of files stored in each archive
                 * can be configured in the plugin settings.
                 */
                class ArchiverTransformer : public TransformerPlugin
                {
                public:
                    ArchiverTransformer();

                    virtual inline bool preservesFilesCounts() const override;
                    
                protected:
                    virtual void processOneFile(const bdg::core::pipeline::TransformerTask *task, std::unique_ptr<FileTransfererStream> inputFileStream, const Poco::Path &inputFilePath, const Poco::Path &outputFilePath, size_t &newlyCreatedFilesCount) override;
                    virtual void clean() override;

                private:
                    /**
                     * Stream of currently open zip archive.
                     */
                    std::unique_ptr<std::ofstream> outputStream;

                    /**
                     * Currently open zip archive.
                     */
                    std::unique_ptr<Poco::Zip::Compress> zip;

                    /**
                     * Files stored so far in the currently open zip archive.
                     */
                    size_t filesInArchive = 0;
                };
            }
        }
    }
}

/**
 * Returns whether plugin preserves the total number of files generated in the previous
 * stage of the pipeline. When more than one file is stored in each created archive, it
 * returns false.
 *
 * @return Returns false when more than one file is stored in each created archive.
 */
inline bool bdg::core::pipeline::plugin::ArchiverTransformer::preservesFilesCounts() const
{
    return getSettingValueInt("filesPerArchive") == 1;
}
