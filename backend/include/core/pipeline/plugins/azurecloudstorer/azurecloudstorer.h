/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Storer
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/bdgapi.h"       // Plugin API
#include <Poco/Net/HTTPResponse.h>              // Poco::Net::HTTPResponse

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            namespace plugin
            {
                /**
                 * Struct for holding the token returned from the Rockwell authentizaion service.
                 */
                struct TokenHolder
                {
                    std::string serverToken;
                    std::string serverUri;
                    std::string storageToken;
                    std::string storageUri;
                    std::string queueToken;
                    std::string queueUri;
                    std::string expires;
                };

                /**
                 * Pipeline plugin which sends all the generated files to the Azure Cloud.
                 * This plugin was developed specially for Rockwell automation company to
                 * suit their needs. Each generated file is stored in the Azure Blob Storage
                 * and its name is stored in Azure Queue. These files are then automatically
                 * processed on the cloud. For authentization a special authentizaion service
                 * is used - authentization credentials can be configured in plugin setting.
                 * These manuals can be read in order to get more information about implementation
                 * of this plugin:
                 * https://msdn.microsoft.com/en-us/library/azure/dd179451.aspx
                 * https://msdn.microsoft.com/en-us/library/azure/dd179346.aspx
                 * https://gist.github.com/t-mat/5c1b68d56179d7af7e98
                 */
                class AzureCloudStorer : public StorerPlugin
                {
                public:
                    AzureCloudStorer();
                    
                protected:
                    virtual void init() override;
                    virtual void processOneFile(const bdg::core::pipeline::TransformerTask *task, std::unique_ptr<FileTransfererStream> &inputFileStream, const Poco::Path &inputFilePath) override;
                    
                private:
                    void uploadToCloudServer(const bdg::core::pipeline::TransformerTask *task, std::unique_ptr<FileTransfererStream> &inputFileStream, const Poco::Path &inputFilePath) const;
                    void uploadToAzureStorage(const bdg::core::pipeline::TransformerTask *task, std::unique_ptr<FileTransfererStream> &inputFileStream, const Poco::Path &inputFilePath) const;

                    TokenHolder getAccessToken() const;
                    std::string sendHttpRequest(const std::string &uri, const std::string &method, std::istream &dataStream, const Poco::UInt64 contentLength, const std::map<std::string, std::string> &headers, Poco::Net::HTTPResponse &response) const;
                    std::string encode(const std::string &data) const;
                    std::string decodeToken(const std::string &tokenEncoded) const;

                    /**
                     * Token returned from the Rockwell authentizaion service.
                     */
                    TokenHolder usedTokenHolder;

                    /**
                     * Time when the token was received from the Rockwell authentizaion service.
                     */
                    time_t tokenReceiveTime;

                    /**
                     * How often should be token renewed.
                     */
                    static const size_t RENEW_TOKEN_SECONDS;

                    /**
                     * Used version of Azure Cloud API.
                     */
                    static const std::string AZURE_API_VERSION;
                };
            }
        }
    }
}
