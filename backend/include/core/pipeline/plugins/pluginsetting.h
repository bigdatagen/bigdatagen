/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/common.h"           // Plugin API common

#include <Poco/JSON/Object.h>       // Poco::JSON::Object

#include <memory>       // std::unique_ptr
#include <string>       // std::string

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            namespace plugin
            {
                // Forward declaration.
                struct PluginSettingImpl;

                /**
                 * One setting of pipeline plugin.
                 */
                class BDG_PLUGIN_API PluginSetting
                {
                public:
                    /**
                     * Type of the setting.
                     */
                    enum SettingType
                    {
                        SETTING_TYPE_STRING,
                        SETTING_TYPE_INT,
                        SETTING_TYPE_CHECKBOX,
                        SETTING_TYPE_SELECT,
                        SETTING_TYPE_ARRAY,
                        SETTING_TYPE_GROUP
                    };

                    explicit PluginSetting(const std::string &name, const std::string &description = "", const std::string &defaultValue = "", const std::string &help = "", const bool isAdvancedSetting = false);
                    virtual ~PluginSetting();

                    const std::string &getName() const;
                    const std::string &getDescription() const;
                    const std::string &getDefaultValue() const;
                    const std::string &getHelp() const;
                    bool isAdvanced() const;
                    void setValue(const std::string &value);
                    const std::string &getValue() const;
                    
                    virtual SettingType getType() const = 0;
                    virtual PluginSetting *clone() const = 0;

                    virtual void setValue(const Poco::Dynamic::Var &valueJson);
                    virtual std::string validate(const std::string &value) const;
                    virtual std::string getTypeString() const;

                    virtual bool containsScalars() const;
                    virtual void serialize(Poco::JSON::Object &json) const;

                    template<class T>
                    inline const T *convert() const;

                private:
                    /**
                     * Setting implementation - PIMPL design pattern.
                     */
                    std::unique_ptr<PluginSettingImpl> impl;
                };
            }
        }
    }
}

/**
 * Converts the setting to the setting of the type specified as type parameter of this method.
 *
 * @return Converted setting.
 */
template<class T>
inline const T *bdg::core::pipeline::plugin::PluginSetting::convert() const
{
    return static_cast<const T*>(this);
}

