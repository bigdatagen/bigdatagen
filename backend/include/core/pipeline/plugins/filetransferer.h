/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/common.h"                           // Plugin API common
#include "core/pipeline/plugins/filetransfererstreamiterator.h"     // FileTransfererStreamIterator
#include "utils/mpiproxy.h"                                         // MPIProxy

#include <vector>       // std::vector

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Class which handles transferring of files between MPI threads. Now it works only
             * with shared file system, but this class is the only point in the application
             * through which we read shared files. I.e. it could be extended to work also with
             * file system that are not shared.
             */
            class BDG_PLUGIN_API FileTransferer
            {
            public:
                explicit FileTransferer(const int jobId = 0, const size_t filesPerDirectory = 0);
                ~FileTransferer() = default;

                FileTransfererStreamIterator getFilesFromWorker(const std::string &workerFileMask, const int workerRank, const Poco::UInt64 fileFrom, const size_t filesCount) const;
                std::unique_ptr<FileTransfererStream> getFileFromRank(const std::string &filePath, const int rank, const Poco::UInt64 fileId = 0) const;
                
                inline bool isSharedFileSystem() const;
                inline Poco::UInt64 getDirectoryNumber(const Poco::UInt64 fileId) const;
                void createDirectories(const std::vector<std::string> &paths, const Poco::UInt64 fileFrom, const size_t filesCount) const;
                
                const std::string formatDirectoryNumber(const Poco::UInt64 directoryId) const;
                const std::string expandPathMask(const std::string &pathStr, const int pluginRelativePosition, const int rank, const bool useGlobalFileIds, const Poco::UInt64 fileId = 0) const;
                const std::string expandFileMaskJobId(const std::string &path) const;
                const std::string expandFileMaskFileId(const std::string &path, const Poco::UInt64 fileId) const;
                
                static const std::string expandFileMaskPluginRelativePosition(const std::string &path, const int pluginRelativePosition);
                static const std::string expandFileMaskRank(const std::string &path, const int rank);
                static const std::string expandFileExtension(const std::string &path, const std::string &fileExtension);
                static bool detectSharedFileSystem(const bdg::utils::MPIProxy &mpi, const int jobId = 0);
            
            private:
                std::string getDefaultExtension(const std::string &defaultExtension) const;

                /**
                 * Do we have a shared file system (e.g. NFS)?
                 */
                static bool isSharedFsStatic;

                /**
                 * Do we have a shared file system (e.g. NFS)?
                 */
                bool isSharedFs;

                /**
                 * Id of currently running job.
                 */
                int jobId;

                /**
                 * How many files can be stored in one directory in maximum.
                 */
                size_t filesPerDirectory;
            };
        }
    }
}

/**
 * Returns whether we have a shared file system (e.g. NFS).
 *
 * @return Do we have a shared file system?
 */
inline bool bdg::core::pipeline::FileTransferer::isSharedFileSystem() const
{
    return isSharedFs;
}

/**
 * Returns id of the directory where the file with given id should be stored.
 *
 * @param fileId Id of a file.
 *
 * @return Id of the directory.
 */
inline Poco::UInt64 bdg::core::pipeline::FileTransferer::getDirectoryNumber(const Poco::UInt64 fileId) const
{
    if (filesPerDirectory > 0)
    {
        return ((fileId - 1) / filesPerDirectory) + 1;
    }
    else
    {
        return 1;
    }
}
