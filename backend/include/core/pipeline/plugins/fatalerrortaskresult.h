/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/errortaskresult.h"          // ErrorTaskResult

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Class reprezenting task result. This task result is returned to the
             * master when a fatal error has occured in the task, i.e. when
             * TaskFatalErrorException exception is thrown.
             */
            class BDG_PLUGIN_API FatalErrorTaskResult : public ErrorTaskResult
            {
            public:
                FatalErrorTaskResult() = default;
                explicit FatalErrorTaskResult(const std::string &msg);
                explicit FatalErrorTaskResult(const std::exception &ex);
                explicit FatalErrorTaskResult(const Poco::Exception &ex);
                virtual ~FatalErrorTaskResult() = default;

                virtual inline ResultType getType() const override;
            };
        }
    }
}

/**
 * Returns type of the task result.
 *
 * @return Type of the task result.
 */
inline bdg::core::pipeline::TaskResult::ResultType bdg::core::pipeline::FatalErrorTaskResult::getType() const
{
    return ResultType::RESULT_TYPE_FATAL_ERROR;
}
