/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/common.h"       // Plugin API common

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            namespace plugin
            {
                // Forward declaration.
                class Plugin;

                /**
                 * Definition of plugin post-processor.
                 */
                class BDG_PLUGIN_API PluginPostprocessor
                {
                public:
                    ~PluginPostprocessor() = default;

                    inline Plugin *getPlugin() const;
                    inline bool isUndefined() const;
                    inline bool isUnique() const;
                    inline bool requiresOrderedInput() const;

                    static PluginPostprocessor createUniqueOrdered(Plugin *plugin);
                    static PluginPostprocessor createUnique(Plugin *plugin);
                    static PluginPostprocessor create(Plugin *plugin);
                    static PluginPostprocessor none();

                private:
                    PluginPostprocessor(Plugin *plugin = nullptr, const bool isUnique = false, const bool requiresOrderedInput = false);

                    /**
                     * Plugin which is a post-processor to some other plugin.
                     */
                    Plugin *plugin;

                    /**
                     * Is it a unique plugin? Unique plugin has one allocated thread just for itself.
                     */
                    const bool pluginIsUnique;

                    /**
                     * Does the plugin need input files ordered by their ids?
                     */
                    const bool pluginRequiresOrderedInput;
                };
            }
        }
    }
}

/**
 * Returns plugin which is a post-processor to some other plugin.
 *
 * @return Plugin which is a post-processor to some other plugin.
 */
inline bdg::core::pipeline::plugin::Plugin *bdg::core::pipeline::plugin::PluginPostprocessor::getPlugin() const
{
    return plugin;
}

/**
 * Returns whether no plugin is defined as plugin post-processor.
 *
 * @return Is there no plugin defined as plugin post-processor?
 */
inline bool bdg::core::pipeline::plugin::PluginPostprocessor::isUndefined() const
{
    return plugin == nullptr;
}

/**
 * Returns whether the plugin is unique. Unique plugin has one allocated thread just for itself.
 *
 * @return Is it a unique plugin?
 */
inline bool bdg::core::pipeline::plugin::PluginPostprocessor::isUnique() const
{
    return pluginIsUnique;
}

/**
 * Returns whether the plugin need input files ordered by their ids.
 *
 * @return Does the plugin need input files ordered by their ids?
 */
inline bool bdg::core::pipeline::plugin::PluginPostprocessor::requiresOrderedInput() const
{
    return pluginRequiresOrderedInput;
}
