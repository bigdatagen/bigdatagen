/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Transformer
 * @author   Big Data Generator Team
 */

#pragma once

#include <Poco/JSON/Object.h>           // Poco::JSON::Object
#include <Poco/JSON/Array.h>            // Poco::JSON::Array
#include <Poco/MongoDB/Element.h>       // Poco::MongoDB::Element

#include <iostream>         // std::stream

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            namespace plugin
            {
                /**
                 * Class for converting input given in JSON format to BSON format.
                 */
                class JsonToBsonConverter
                {
                public:
                    static void convert(std::istream &istr, std::ostream &ostr);

                private:
                    static Poco::MongoDB::Element::Ptr convert(const std::string &fieldName, const Poco::Dynamic::Var &any);
                    static Poco::MongoDB::Element::Ptr convertObject(const std::string &fieldName, const Poco::JSON::Object &json);
                    static Poco::MongoDB::Element::Ptr convertArray(const std::string &fieldName, const Poco::JSON::Array &json);
                };
            }
        }
    }
}
