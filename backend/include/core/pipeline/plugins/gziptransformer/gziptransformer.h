/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Transformer
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/bdgapi.h"       // Plugin API

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            namespace plugin
            {
                /**
                 * Pipeline plugin which gzipes files generated in the previous stage of the pipeline.
                 * Gzipped files are stored to the location configured in the "outputPath" setting.
                 */
                class GzipTransformer : public TransformerPlugin
                {
                public:
                    GzipTransformer();

                protected:
                    virtual void processOneFile(const bdg::core::pipeline::TransformerTask *task, std::unique_ptr<FileTransfererStream> inputFileStream, const Poco::Path &inputFilePath, const Poco::Path &outputFilePath, size_t &newlyCreatedFilesCount) override;
                };
            }
        }
    }
}
