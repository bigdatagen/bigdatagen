/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/plugin.h"                       // Plugin
#include "core/pipeline/plugins/transformertaskmetadata.h"      // TransformerTaskMetadata
#include "core/pipeline/plugins/transformertask.h"              // TransformerTask

#include <Poco/Path.h>              // Poco::Path

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            namespace plugin
            {
                /**
                 * Transformer plugins are meant to be used in the middle of the pipeline.
                 * They transform files given in a form of tasks, i.e. from these files
                 * they produce different files. For example transformer task can compress
                 * given files or it can change their format.
                 */
                class BDG_PLUGIN_API TransformerPlugin : public Plugin
                {
                public:
                    TransformerPlugin(const std::string &name, const std::string &description, const std::string &extension);
                    virtual ~TransformerPlugin() = default;

                    virtual inline PluginType getType() const override;
                    virtual bdg::core::pipeline::Task *taskFactory() override;
                    virtual bdg::core::pipeline::Task *createTask(const bdg::core::pipeline::TaskMetadata *metadata) override;

                protected:
                    virtual bdg::core::pipeline::TaskResult *processTaskImpl(const bdg::core::pipeline::Task *task) override;
                    virtual void processOneFile(const bdg::core::pipeline::TransformerTask *task, std::unique_ptr<FileTransfererStream> inputFileStream, const Poco::Path &inputFilePath, const Poco::Path &outputFilePath, size_t &newlyCreatedFilesCount) = 0;

                    bdg::core::pipeline::TransformerTaskMetadata *convertMetadata(const bdg::core::pipeline::TaskMetadata *metadata) const;
                    bdg::core::pipeline::TransformerTask *convertTask(const bdg::core::pipeline::Task *task) const;
                };
            }
        }
    }
}

/**
 * Returns type of the plugin.
 *
 * @return Type of the plugin.
 */
inline bdg::core::pipeline::plugin::Plugin::PluginType bdg::core::pipeline::plugin::TransformerPlugin::getType() const
{
    return PluginType::PLUGIN_TYPE_TRANSFORMER;
}
