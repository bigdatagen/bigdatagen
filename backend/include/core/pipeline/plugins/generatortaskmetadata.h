/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/taskmetadata.h"         // TaskMetadata

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Metadata for the generator task containing the information for the task.
             * Instance of this class is given to the generator plugin in the master
             * thread when the generator task is created.
             */
            class BDG_PLUGIN_API GeneratorTaskMetadata : public TaskMetadata
            {
            public:
                GeneratorTaskMetadata() = default;
                explicit GeneratorTaskMetadata(const Properties &properties);
                virtual ~GeneratorTaskMetadata() = default;

                virtual inline MetadataType getType() const override;
                inline void setFilesCount(const int filesCount);
                inline int getFilesCount() const;
                inline void setSchema(const std::string &schema);
                inline std::string getSchema() const;

            private:
                /**
                 * Number of files that should be generated.
                 */
                int filesCount = 0;

                /**
                 * Schema according to which we should generate data.
                 */
                std::string schema;
            };
        }
    }
}

/**
 * Returns type of the task metadata.
 *
 * @return Type of the task metadata.
 */
inline bdg::core::pipeline::TaskMetadata::MetadataType bdg::core::pipeline::GeneratorTaskMetadata::getType() const
{
    return MetadataType::METADATA_TYPE_GENERATOR;
}

/**
 * Sets number of files that should be generated.
 *
 * @param filesCount Number of files that should be generated.
 */
inline void bdg::core::pipeline::GeneratorTaskMetadata::setFilesCount(const int filesCount)
{
    this->filesCount = filesCount;
}

/**
 * Returns number of files that should be generated.
 *
 * @return Number of files that should be generated.
 */
inline int bdg::core::pipeline::GeneratorTaskMetadata::getFilesCount() const
{
    return filesCount;
}

/**
 * Sets schema according to which we should generate data.
 *
 * @param schema Schema according to which we should generate data.
 */
inline void bdg::core::pipeline::GeneratorTaskMetadata::setSchema(const std::string &schema)
{
    this->schema = schema;
}

/**
 * Returns schema according to which we should generate data.
 *
 * @return Schema according to which we should generate data.
 */
inline std::string bdg::core::pipeline::GeneratorTaskMetadata::getSchema() const
{
    return schema;
}
