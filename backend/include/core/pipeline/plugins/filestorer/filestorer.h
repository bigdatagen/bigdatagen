/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Storer
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/bdgapi.h"       // Plugin API

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            namespace plugin
            {
                /**
                 * Pipeline plugin which copies all the generated files to one or more locations
                 * in the file system. These paths are configured in the settings of this plugin.
                 */
                class FileStorer : public StorerPlugin
                {
                public:
                    FileStorer();

                protected:
                    virtual void processOneFile(const bdg::core::pipeline::TransformerTask *task, std::unique_ptr<FileTransfererStream> &inputFileStream, const Poco::Path &inputFilePath) override;
                };
            }
        }
    }
}
