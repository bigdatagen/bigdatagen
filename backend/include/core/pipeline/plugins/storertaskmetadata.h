/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/transformertaskmetadata.h"      // TransformerTaskMetadata
#include "core/pipeline/plugins/successtaskresult.h"            // SuccessTaskResult
#include "core/pipeline/plugins/filefrequency.h"                // FileFrequency

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Metadata for the storer task containing the information for the task.
             * Instance of this class is given to the storer plugin in the master
             * thread when the storer task is created.
             */
            class BDG_PLUGIN_API StorerTaskMetadata : public TransformerTaskMetadata
            {
            public:
                StorerTaskMetadata() = default;
                explicit StorerTaskMetadata(const Properties &properties);
                explicit StorerTaskMetadata(const SuccessTaskResult &taskRestult);
                virtual ~StorerTaskMetadata() = default;

                virtual inline MetadataType getType() const override;
            };
        }
    }
}

/**
 * Returns type of the task metadata.
 *
 * @return Type of the task metadata.
 */
inline bdg::core::pipeline::TaskMetadata::MetadataType bdg::core::pipeline::StorerTaskMetadata::getType() const
{
    return MetadataType::METADATA_TYPE_STORER;
}
