/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/task.h"                         // Task
#include "core/pipeline/plugins/generatortaskmetadata.h"        // GeneratorTaskMetadata

#include <Poco/Dynamic/Var.h>       // Poco::Dynamic::Var

#include <vector>           // std::vector
#include <functional>       // std::function

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Task for a generator plugin.
             */
            class BDG_PLUGIN_API GeneratorTask : public Task
            {
            public:
                /**
                 * Matching generator and its random numbers.
                 */
                template<typename T>
                using nums_t = std::vector<T>;

                GeneratorTask() = default;
                GeneratorTask(const GeneratorTaskMetadata *metadata);
                virtual ~GeneratorTask() = default;

                virtual inline TaskType getType() const override;
                inline void setSchema(const std::string &schema);
                inline const std::string &getSchema() const;

                inline void setSeqsStart(const std::vector<std::size_t> &seqsIdcs);
                inline void setSeqsStart(std::vector<std::size_t> &&seqsIdcs);
                inline std::vector<std::size_t> getSeqsStart() const;

                inline void setRemInts(const std::vector<std::vector<int>> &remInts);
                inline void setRemInts(std::vector<std::vector<int>> &&remInts);
                inline std::vector<std::vector<int>> getRemInts() const;
                
            protected:
                virtual void serializeAdditional(Poco::JSON::Object &json) const override;
                virtual void unserializeAdditional(const Poco::JSON::Object &json) override;

            private:
                template<typename T>
                using serializer_fn_t = std::function<void(Poco::JSON::Array &arr, const T&)>;

                template<typename T>
                using deserializer_fn_t = std::function<T (const Poco::Dynamic::Var&)>;

                template<typename T>
                static inline Poco::JSON::Array serializeVector(const std::vector<T> &v,  const serializer_fn_t<T> &serializer);

                template<typename T>
                static inline std::vector<T> deserializeVector(const Poco::JSON::Array::Ptr &pArr, const deserializer_fn_t<T> &serializer);

                /**
                 * Name of the property which holds the schema.
                 */
                static const std::string schemaName;
                
                /**
                 * Name of the property which holds the start indices of sequences.
                 */
                static const std::string seqsStartName;
                
                /**
                 * Name of the property which holds generated random numbers.
                 */
                static const std::string remIntsName;

                /**
                 * Schema used for generation.
                 */
                std::string schema;
                
                /**
                 * Indices for sequences.
                 */
                std::vector<std::size_t> seqsStart;
                
                /**
                 * Random int numbers for generators.
                 */
                std::vector<std::vector<int>> remInts;
            };
            
        }
    }
}

/**
 * Returns type of the task.
 *
 * @return Type of the task.
 */
inline bdg::core::pipeline::Task::TaskType bdg::core::pipeline::GeneratorTask::getType() const
{
    return TaskType::TASK_TYPE_GENERATOR;
}

/**
 * Sets the schema used for generation.
 *
 * @param schema Schema used for generation.
 */
inline void bdg::core::pipeline::GeneratorTask::setSchema(const std::string &schema)
{
    this->schema = schema;
}

/**
 * Returns the schema used for generation.
 *
 * @return Schema used for generation.
 */
inline const std::string& bdg::core::pipeline::GeneratorTask::getSchema() const
{
    return schema;
}

/**
 * Sets indices for sequences.
 *
 * @param seqsStart Indices for sequences.
 */
inline void bdg::core::pipeline::GeneratorTask::setSeqsStart(const std::vector<std::size_t> &seqsStart)
{
    this->seqsStart = seqsStart;
}

/**
 * Sets indices for sequences.
 *
 * @param seqsStart Indices for sequences.
 */
inline void bdg::core::pipeline::GeneratorTask::setSeqsStart(std::vector<std::size_t> &&seqsIdcs)
{
    this->seqsStart = std::move(seqsIdcs);
}

/**
 * Returns indices for sequences.
 *
 * @return Indices for sequences.
 */
inline std::vector<std::size_t> bdg::core::pipeline::GeneratorTask::getSeqsStart() const
{
    return seqsStart;
}

/**
 * Sets random int numbers for generators.
 *
 * @param remInts Random int numbers for generators.
 */
inline void bdg::core::pipeline::GeneratorTask::setRemInts(const std::vector<std::vector<int>> &remInts)
{
    this->remInts = remInts;
}

/**
 * Sets random int numbers for generators.
 *
 * @param remInts Random int numbers for generators.
 */
inline void bdg::core::pipeline::GeneratorTask::setRemInts(std::vector<std::vector<int>> &&remInts)
{
    this->remInts = std::move(remInts);
}

/**
 * Returns random int numbers for generators.
 *
 * @return Random int numbers for generators.
 */
inline std::vector<std::vector<int>> bdg::core::pipeline::GeneratorTask::getRemInts() const
{
    return remInts;
}

/**
 * Serializes a vector.
 *
 * @param v Vector to be serialized.
 * @param sfn Function used for the serialization of each vector item.
 *
 * @return Serialized vector.
 */
template<typename T>
inline Poco::JSON::Array bdg::core::pipeline::GeneratorTask::serializeVector(
    const std::vector<T> &v, 
    const bdg::core::pipeline::GeneratorTask::serializer_fn_t<T> &sfn)
{
    Poco::JSON::Array ret;

    for (const auto &t : v)
    {
        sfn(ret, t);
    }
    
    return ret;
}

/**
 * Deserializes a vector.
 *
 * @param pArr Serialized vector.
 * @param dfn Function used for the deserialization of each vector item.
 *
 * @return Deserialized vector.
 */
template<typename T>
inline std::vector<T> bdg::core::pipeline::GeneratorTask::deserializeVector(
    const Poco::JSON::Array::Ptr &pArr, 
    const bdg::core::pipeline::GeneratorTask::deserializer_fn_t<T> &dfn)
{
    std::vector<T> res;

    for (size_t i = 0; i < pArr->size(); i++)
    {
        res.push_back(dfn(pArr->get(i)));
    }

    return res;
}