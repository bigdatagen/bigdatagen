/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/task.h"                         // Task
#include "core/pipeline/plugins/transformertaskmetadata.h"      // TransformerTaskMetadata

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Task for a transformer plugin.
             */
            class BDG_PLUGIN_API TransformerTask : public Task
            {
            public:
                TransformerTask() = default;
                explicit TransformerTask(const TransformerTaskMetadata *metadata);
                virtual ~TransformerTask() = default;
                
                virtual inline TaskType getType() const override;
                inline void setResultWorkerFileMask(const std::string &resultWorkerFileMask);
                inline const std::string &getResultWorkerFileMask() const;
                inline void setResultWorkerRank(const int resultWorkerRank);
                inline const int getResultWorkerRank() const;

            protected:
                virtual void serializeAdditional(Poco::JSON::Object &json) const override;
                virtual void unserializeAdditional(const Poco::JSON::Object &json) override;

            private:
                /**
                 * Mask of the path where the files that should be processed in the task are stored.
                 */
                std::string resultWorkerFileMask;

                /**
                 * Rank of the worker from which we should process files.
                 */
                int resultWorkerRank;
            };
        }
    }
}

/**
 * Returns type of the task.
 *
 * @return Type of the task.
 */
inline bdg::core::pipeline::Task::TaskType bdg::core::pipeline::TransformerTask::getType() const
{
    return TaskType::TASK_TYPE_TRANSFORMER;
}

/**
 * Sets the mask of the path where the files that should be processed in the task are stored.
 *
 * @param resultWorkerFileMask Mask of the file path.
 */
inline void bdg::core::pipeline::TransformerTask::setResultWorkerFileMask(const std::string &resultWorkerFileMask)
{
    this->resultWorkerFileMask = resultWorkerFileMask;
}

/**
 * Returns the mask of the path where the files that should be processed in the task are stored.
 * 
 * @return Mask of the file path.
 */
inline const std::string &bdg::core::pipeline::TransformerTask::getResultWorkerFileMask() const
{
    return resultWorkerFileMask;
}

/**
 * Sets the rank of the worker from which we should process files.
 *
 * @param resultWorkerRank Rank of the worker from which we should process files.
 */
inline void bdg::core::pipeline::TransformerTask::setResultWorkerRank(const int resultWorkerRank)
{
    this->resultWorkerRank = resultWorkerRank;
}

/**
 * Returns the rank of the worker from which we should process files.
 *
 * @return Rank of the worker from which we should process files.
 */
inline const int bdg::core::pipeline::TransformerTask::getResultWorkerRank() const
{
    return resultWorkerRank;
}
