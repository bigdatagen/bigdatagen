/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/common.h"       // Plugin API common

#include <Poco/JSON/Object.h>       // Poco::JSON::Object

#include <string>       // std::string
#include <map>          // std::map

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Base class for all the classes that have some properties and that need serialization.
             */
            class BDG_PLUGIN_API PropertiesObject
            {
            public:
                typedef std::map<std::string, std::string> Properties;

                PropertiesObject() = default;
                explicit PropertiesObject(const Properties &properties);
                virtual ~PropertiesObject() = default;
                
                inline void setProperty(const std::string& propertyName, const std::string& propertyValue);
                inline void setProperty(const std::string& propertyName, const int propertyValue);

                inline std::string getProperty(const std::string &propertyName, const std::string defaultValue = "") const;
                inline int getPropertyInt(const std::string &propertyName, const int defaultValue = 0) const;
                inline bool getPropertyBool(const std::string &propertyName, const bool defaultValue = false) const;
                inline double getPropertyDouble(const std::string &propertyName, const double defaultValue = 0.0) const;
                inline const Properties &getProperties() const;
                
                virtual const std::string serialize() const;
                virtual void serialize(Poco::JSON::Object &json) const;
                virtual void unserialize(const std::string &serializedObject);
                virtual void unserialize(const Poco::JSON::Object &json);
                
            protected:
                virtual void serializeAdditional(Poco::JSON::Object &json) const;
                virtual void unserializeAdditional(const Poco::JSON::Object &json);

                /**
                 * Object properties that can be serialized.
                 */
                Properties properties;
            };
        }
    }
}

/**
 * Sets an object property.
 *
 * @param propertyName Name of the property.
 * @param propertyValue Value of the property.
 */
inline void bdg::core::pipeline::PropertiesObject::setProperty(const std::string &propertyName, const std::string &propertyValue)
{
    properties.emplace(propertyName, propertyValue);
}

/**
 * Sets an object property.
 *
 * @param propertyName Name of the property.
 * @param propertyValue Value of the property.
 */
inline void bdg::core::pipeline::PropertiesObject::setProperty(const std::string &propertyName, const int propertyValue)
{
    properties.emplace(propertyName, std::to_string(propertyValue));
}

/**
 * Returns the value of an object property.
 *
 * @param propertyName Name of the property.
 * @param defaultValue Default value returned when property has no value defined.
 *
 * @return Value of an object property or default value.
 */
inline std::string bdg::core::pipeline::PropertiesObject::getProperty(const std::string &propertyName, const std::string defaultValue) const
{
    Properties::const_iterator it = properties.find(propertyName);
    
    if (it != properties.end())
    {
        return it->second;
    }
    else
    {
        return defaultValue;
    }
}

/**
 * Returns the value of an object property.
 *
 * @param propertyName Name of the property.
 * @param defaultValue Default value returned when property has no value defined.
 *
 * @return Value of an object property or default value.
 */
inline int bdg::core::pipeline::PropertiesObject::getPropertyInt(const std::string &propertyName, const int defaultValue) const
{
    return std::atoi(getProperty(propertyName, std::to_string(defaultValue)).c_str());
}

/**
 * Returns the value of an object property.
 *
 * @param propertyName Name of the property.
 * @param defaultValue Default value returned when property has no value defined.
 *
 * @return Value of an object property or default value.
 */
inline bool bdg::core::pipeline::PropertiesObject::getPropertyBool(const std::string &propertyName, const bool defaultValue) const
{
    return Poco::toLower(getProperty(propertyName, std::to_string(defaultValue))) == "true" ? true : false;
}

/**
 * Returns the value of an object property.
 *
 * @param propertyName Name of the property.
 * @param defaultValue Default value returned when property has no value defined.
 *
 * @return Value of an object property or default value.
 */
inline double bdg::core::pipeline::PropertiesObject::getPropertyDouble(const std::string &propertyName, const double defaultValue) const
{
    return std::atof(getProperty(propertyName, std::to_string(defaultValue)).c_str());
}

/**
 * Returns all object properties.
 *
 * @return Object properties.
 */
inline const bdg::core::pipeline::PropertiesObject::Properties &bdg::core::pipeline::PropertiesObject::getProperties() const
{
    return properties;
}
