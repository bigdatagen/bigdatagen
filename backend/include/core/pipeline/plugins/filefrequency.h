/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/common.h"           // Plugin API common

#include <Poco/Types.h>     // Poco::Int64

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Class reprezenting a frequency with which files should be stored by
             * a storer plugin. This frequency can be of two kinds - files per second
             * or bytes per second.
             */
            class BDG_PLUGIN_API FileFrequency
            {
            public:
                FileFrequency();
                FileFrequency(const Poco::Int64 bytes, const Poco::Int64 files, const Poco::Int64 seconds);
                ~FileFrequency() = default;

                inline void setBytes(const Poco::Int64 bytes);
                inline const Poco::Int64 getBytes() const;
                inline void setFiles(const Poco::Int64 files);
                inline const Poco::Int64 getFiles() const;
                inline void setSeconds(const Poco::Int64 seconds);
                inline const Poco::Int64 getSeconds() const;

                inline bool isUndefined() const;

                static const Poco::Int64 UNDEFINED;

            private:
                /**
                 * How many bytes should be stored in each second? When defined a frequency
                 * is of type "bytes per second".
                 */
                Poco::Int64 bytes;

                /**
                 * How many files should be stored in each second? When defined a frequency
                 * is of type "files per second".
                 */
                Poco::Int64 files;

                /**
                 * Determines how many seconds are used in the frequency definition. E.g. if
                 * this property is set to 5 and files property to 2, it means that 2 files
                 * per each 5 seconds should be stored.
                 */
                Poco::Int64 seconds;
            };
        }
    }
}

/**
 * Sets the bytes property.
 * 
 * @param bytes How many bytes should be stored in each second.
 */
inline void bdg::core::pipeline::FileFrequency::setBytes(const Poco::Int64 bytes)
{
    this->bytes = bytes;
}

/**
 * Returns the bytes property.
 *
 * @return How many bytes should be stored in each second.
 */
inline const Poco::Int64 bdg::core::pipeline::FileFrequency::getBytes() const
{
    return bytes;
}

/**
 * Sets the files property.
 *
 * @param files How many files should be stored in each second.
 */
inline void bdg::core::pipeline::FileFrequency::setFiles(const Poco::Int64 files)
{
    this->files = files;
}

/**
 * Returns the files property.
 *
 * @return How many files should be stored in each second.
 */
inline const Poco::Int64 bdg::core::pipeline::FileFrequency::getFiles() const
{
    return files;
}

/**
 * Sets the seconds property.
 *
 * @param seconds How many seconds are used in the frequency definition.
 */
inline void bdg::core::pipeline::FileFrequency::setSeconds(const Poco::Int64 seconds)
{
    this->seconds = seconds;
}

/**
 * Returns the seconds property.
 *
 * @return How many seconds are used in the frequency definition.
 */
inline const Poco::Int64 bdg::core::pipeline::FileFrequency::getSeconds() const
{
    return seconds;
}

/**
 * When this method returns true, it means that no frequency is defined, i.e.
 * the pipeline should store with unlimited speed.
 *
 * @return Is the frequency undefined.
 */
inline bool bdg::core::pipeline::FileFrequency::isUndefined() const
{
    return (files <= 0 && bytes <= 0) || seconds <= 0;
}
