/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/pluginsetting.h"        // PluginSetting

#include <memory>       // std::unique_ptr
#include <vector>       // std::vector

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            namespace plugin
            {
                /**
                 * Setting of pipeline plugin which contains other settings.
                 */
                class BDG_PLUGIN_API PluginSettingGroup : public PluginSetting
                {
                public:
                    typedef std::vector<std::unique_ptr<PluginSetting>> SettingsCollection;

                    PluginSettingGroup(const std::string &name, const std::string &caption, const std::vector<PluginSetting*> &groupSettings, const bool isAdvancedSetting = false);
                    virtual ~PluginSettingGroup() = default;

                    inline const std::string &getCaption() const;
                    PluginSetting *getSetting(const std::string &settingName) const;

                    virtual inline SettingType getType() const override;
                    virtual PluginSetting *clone() const override;

                    virtual void setValue(const Poco::Dynamic::Var &valueJson) override;
                    virtual bool containsScalars() const;
                    virtual void serialize(Poco::JSON::Object &json) const override;

                private:
                    /**
                     * Caption of the setting.
                     */
                    const std::string caption;

                    /**
                     * Settings contained in the group.
                     */
                    SettingsCollection settings;
                };
            }
        }
    }
}

/**
 * Returns the caption of the setting.
 *
 * @return Caption of the setting.
 */
inline const std::string &bdg::core::pipeline::plugin::PluginSettingGroup::getCaption() const
{
    return caption;
}

/**
 * Returns the type of the setting.
 *
 * @return Type of the setting.
 */
inline bdg::core::pipeline::plugin::PluginSetting::SettingType bdg::core::pipeline::plugin::PluginSettingGroup::getType() const
{
    return SettingType::SETTING_TYPE_GROUP;
}
