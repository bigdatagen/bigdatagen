/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/propertiesobject.h"         // PropertiesObject

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Base class for task metadata. Instance of this class is given to
             * a plugin when it creates an instance of a task in the master thread.
             */
            class BDG_PLUGIN_API TaskMetadata : public PropertiesObject
            {
            public:
                /**
                 * Available types of task metadata.
                 */
                enum MetadataType
                {
                    METADATA_TYPE_GENERATOR,
                    METADATA_TYPE_TRANSFORMER,
                    METADATA_TYPE_STORER
                };

                TaskMetadata() = default;
                explicit TaskMetadata(const Properties &properties);
                virtual ~TaskMetadata() = default;

                virtual MetadataType getType() const = 0;
            };
        }
    }
}
