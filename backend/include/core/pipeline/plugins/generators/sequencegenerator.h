/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file sequencesgenerator.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include "elemgen.h" // Generator
#include "exception.h" // Exception
#include "token.h" // Token
#include "intgenerator.h"   // IntGenerator
#include "doublegenerator.h" // DoubleGenerator

#include "utils/convert.h" // Convert functions.

#include <vector> // vector
#include <string> // string
#include <sstream>
#include <stack>
#include <memory> // ptr
#include <algorithm> // transform
#include <iterator> // back_inserter
#include <cassert> // assert
#include <utility> // pair
#include <cctype> // isdigit, isalpha

namespace bdg
{
    namespace core
    {
        /**
        * @brief Base class for all sequence generators.
        */
        class DynSeqGenerator
        {
        public:
            explicit DynSeqGenerator() = default;
            virtual ~DynSeqGenerator() = default;

            /**
            * @brief Reference record contains name and needed history.
            */
            typedef std::pair<std::string, int> refRecord_t;

            const std::vector<refRecord_t>& getRefs() const;

            void setStart(std::size_t startIdx);

        protected:
            static void procSeq(
                const GrammarElement::element_map_t& parsedEls,
                const GrammarElement::seq_vector_t& seqs);

            /**
            * @brief Process global sequences
            * @param locGens Generators on which is current sequence dependant.
            * @param remGens All remote int generators.
            * @param remGensGen For each generator holds dependant generator.
            * @param globSeqs Array of all global sequences.
            */
            void procGlobSeq(
                GrammarElement::intGenTypeFnc_vec_t& locGens,
                GrammarElement::intGenTypeFnc_vec_t& remGens,
                std::vector<std::vector<GrammarElement::genIdxTypeFnc_t>>& remGensGens,
                std::vector<GrammarElement::GlobSeq>& globSeqs);

            virtual void setStartImpl(std::size_t startIdx) = 0;

            /*
            * @brief Needed references.
            */
            std::vector<refRecord_t> refs;

        };

        template<typename T>
        class SequenceGenerator :
            public DynSeqGenerator, public ElemGen<T, SequenceGenerator<T>>
        {
        public:
            inline explicit SequenceGenerator(const std::vector<T>& initValues, const std::string& exp, bool isLocal);

            inline void regDataRef(const T* data, const std::string& ref);

            inline void init(HistoryStorage<T>& h);
            inline T generateValue();
            inline void generate(GenStorer& gs);

        private:

            typedef typename Token<T>::ptr_t TokPtr;

            virtual inline void repairImpl(
                const GrammarElement::element_map_t& parsedEls,
                const GrammarElement::seq_vector_t& locSeqs,
                GrammarElement::intGenTypeFnc_vec_t& locGens,
                GrammarElement::intGenTypeFnc_vec_t& remGens,
                std::vector<std::vector<GrammarElement::genIdxTypeFnc_t>>& remGensGens,
                std::vector<GrammarElement::GlobSeq>& globSeqs,
                std::vector<const GrammarElement*>& pathEls) override;


            inline void preGenImpl(std::vector<GrammarElement::gen_fn_t>& preGenFncs) override;

            // Inherited via DynSeqGenerator
            inline void setStartImpl(std::size_t startIdx) override;

            static inline void addOperator(
                std::stack<Operator<T>*>& ops,
                std::vector<TokPtr>& postExp,
                Operator<T>* op);

            static inline void addRightBracket(
                std::stack<Operator<T>*>& ops,
                std::vector<TokPtr>& postExp);

            inline void addVar(
                std::stack<Operator<T>*>& ops,
                std::vector<TokPtr>& postExp,
                const std::string& var);

            inline void parseValue(const std::string& exp);

            /*
            * @brief Postfix expression.
            */
            std::vector<TokPtr> postExp;

            /*
            * @brief Sequence vector with constants used sequence.
            */
            std::vector<T> consts;

            /*
            * @brief Sequence vector with values (constants and variables).
            */
            //std::vector<T*> vals;

            /**
            * @brief References to values.
            */
            std::vector<dataRef_t<T>> valRefs;

            /**
             * @brief Temporary values
             */
            std::vector<T> tmps;

            /**
            * @brief Evaluation functions that evalues sequence
            */
            std::vector<seqFn_t<T>> evalFncs;

            /**
             * @brief Start index of sequence.
             */
            std::size_t startIdx = 0;

            /**
            * @brief Initial values
            */
            std::vector<T> initVals;

            /**
            * @brief Currently generated index of sequence.
            */
            std::size_t idx = 0;

            /**
            * @brief Flag that says whether is sequence local
            */
            const bool isLocal;

            bool isPreGenRegistered = false;
        };
    }
}



template<typename T>
inline bdg::core::SequenceGenerator<T>::SequenceGenerator(
    const std::vector<T>& initValues, 
    const std::string& exp,
    bool isLocal):
isLocal(isLocal),
initVals(initValues)
{
    parseValue(exp);
    
    // todo: Change to getter!
    this->history.resize(initValues.size());
    // It will be store at proper time while generating.
    /*
    for (const T& val : initValues)
    {
        this->history.store(val);
    }
    */
}

template<typename T>
inline void bdg::core::SequenceGenerator<T>::regDataRef(const T* data, const std::string& ref)
{
    valRefs.emplace_back(ref, data);
}

template<typename T>
inline void bdg::core::SequenceGenerator<T>::init(HistoryStorage<T>& h)
{
    // Idx is incremented inside generate value.
    while(idx < startIdx)
    {
        h.store(generateValue());
    }
}

template<typename T>
inline T bdg::core::SequenceGenerator<T>::generateValue()
{
    // todo: Add it higher, to get rid of "if".
    if (idx < initVals.size())
    {
        return initVals[idx++];
    }

    // Copy data stack.
    for (auto& fnc : evalFncs)
    {
        fnc(tmps);
    }

    ++idx;
    return *std::rbegin(tmps);
}

template<typename T>
inline void bdg::core::SequenceGenerator<T>::generate(GenStorer& gs)
{
    gs.store(std::move(bdg::utils::toString(generateValue())));
}

template<typename T>
inline void bdg::core::SequenceGenerator<T>::parseValue(const std::string& exp)
{
    static const char PLUS = '+';
    static const char MINUS = '-';
    static const char MUL = '*';
    static const char MOD = '%';
    static const char DIV = '/';
    static const char LBR = '(';
    static const char RBR = ')';
    static const char VAR = '$';
    static const char LSBR = '[';
    static const char RSBR = ']';
    static const std::vector<char> specialSymbols{ PLUS, MINUS, MUL, MOD, DIV, LBR, RBR, VAR };

    if (exp.empty())
        throw Exception("Missing value expression.");

    std::stack<Operator<T>*> ops;

    std::size_t fnCount{ 0 };
    std::size_t constCount{ 0 };
    
    bool lsbr = false;

    std::string buff;
    for(char c : exp)
    {
        // We are loading variables square braces.
        if (lsbr)
        {
            buff.push_back(c);
            // Found right square brace.
            if (c == RSBR)
            {
                addVar(ops, postExp, buff);
                buff.erase();
                lsbr = false;
            }

            continue;
        }

        // Is not special symbol or whitespace.
        if (!std::isspace(c) &&
            std::find(std::cbegin(specialSymbols), std::cend(specialSymbols), c) == std::cend(specialSymbols))
        {
            // We got left square brace and have variable.
            if (c == LSBR && !buff.empty() && buff[0] == VAR)
            {
                lsbr = true;
            }
            else
            {
                // Square brackets are used to access old members in references.
                if (c != LSBR && c != RSBR && !std::isalnum(c))
                    throw Exception(Error::INVALID_EXPR_CHAR + c);
            }

            buff.push_back(c);
        }
        // Found special symbol or whitespace -> end of reading to buffer.
        else
        {
            if (!buff.empty())
            {
                // First char is $ - variable.
                if (buff[0] == VAR)
                {
                    addVar(ops, postExp, buff);
                }
                // Is normal constant.
                else
                {
                    postExp.push_back(TokPtr(new Const<T>(bdg::utils::fromString<T>(buff))));
                    ++constCount;
                }
                buff.erase();
            }

            switch (c)
            {
            case PLUS:
                addOperator(ops, postExp, new Plus<T>());
                ++fnCount;
                break;

            case MINUS:
                addOperator(ops, postExp, new Minus<T>());
                ++fnCount;
                break;

            case MUL:
                addOperator(ops, postExp, new Mul<T>());
                ++fnCount;
                break;

            case DIV:
                addOperator(ops, postExp, new Div<T>());
                ++fnCount;
                break;

            case MOD:
                addOperator(ops, postExp, new Mod<T>());
                ++fnCount;
                break;

            case LBR:
                // Just simply push it on stack.
                ops.push(new LeftBracket<T>());
                break;

            case RBR:
                addRightBracket(ops, postExp);
                break;

            case VAR:
                buff.push_back(VAR);
                break;

            default:
                // Should be only whitespace.
                assert(std::isspace(c));
            }
        }
    }

    // Didn't flush buffer at the end.
    if (!buff.empty())
    {
        // Don't have right square brace.
        if (lsbr)
            throw Exception(Error::INVALID_EXPR);

        // First char is $ - variable.
        if (buff[0] == VAR)
        {
            addVar(ops, postExp, buff);
        }
        // Is normal constant.
        else
        {
            postExp.push_back(TokPtr(new Const<T>(bdg::utils::fromString<T>(buff))));
            ++constCount;
        }
        buff.erase();
    }

    // Empty stack
    while (!ops.empty())
    {
        postExp.push_back(TokPtr(ops.top()));
        ops.pop();
    }

    // Init member variables. We need to allocate here, because we use pointers to inner elemnts.
    consts.reserve(constCount);
    tmps.reserve(fnCount);
}

template<typename T>
void bdg::core::SequenceGenerator<T>::repairImpl(
    const GrammarElement::element_map_t& parsedEls,
    const GrammarElement::seq_vector_t& seqs,
    GrammarElement::intGenTypeFnc_vec_t& locGens,
    GrammarElement::intGenTypeFnc_vec_t& remGens,
    std::vector<std::vector<GrammarElement::genIdxTypeFnc_t>>& remGensGens,
    std::vector<GrammarElement::GlobSeq>& globSeqs,
    std::vector<const GrammarElement*>& pathEls)
{
    procSeq(parsedEls, seqs);
    if (!isLocal)
    {
        procGlobSeq(locGens, remGens, remGensGens, globSeqs);
    }
}

template<typename T>
inline void bdg::core::SequenceGenerator<T>::preGenImpl(std::vector<GrammarElement::gen_fn_t>& preGenFncs)
{
    // We want to register sequences only once. 
    if (!isPreGenRegistered)
    {
        std::stack<const T*> tokVals;
        for (auto& tok : postExp)
        {
            tok->registerSeqGen(tokVals, valRefs, consts, tmps, evalFncs);
        }

        // On stack should left only result from last function.
        if (tokVals.size() != 1)
            throw Exception(Error::INVALID_EXPR);

        // Special type of sequences, that only directs its history.
        if (tmps.empty())
        {
            // Add special "move" function, that moves value to tmps[0].
            tmps.resize(1);
            T* const res = tmps.data();
            const T* const val = tokVals.top();
            evalFncs.push_back([res, val](std::vector<T>& v)
            {
                *res = *val;
            });
            
            // Add to token stack for below check.
            tokVals.push(tmps.data());
        }

        // Below happens only if we parse expression badly.
        if(tokVals.top() != &*std::rbegin(tmps))
            throw Exception(Error::INVALID_EXPR);

        // If local, have to generate from initial values.
        if (isLocal)
        {
            HistoryStorage<T>* const hs = &this->history;
            const size_t nextIdx = preGenFncs.size() + 1;
            preGenFncs.push_back([this, hs, nextIdx](GenStorer& gs)
            {
                for (const T& val : initVals)
                {
                    hs->store(val);
                }
                idx = 0;

                return nextIdx;
            });
        }

        isPreGenRegistered = true;
    }
}

template<typename T>
inline void bdg::core::SequenceGenerator<T>::setStartImpl(std::size_t startIdx)
{
    this->startIdx = startIdx;
}

template<typename T>
inline void bdg::core::SequenceGenerator<T>::addOperator(
    std::stack<bdg::core::Operator<T>*>& ops,
    std::vector<TokPtr>& postExp,
    bdg::core::Operator<T>* op)
{
    while (!ops.empty() && *ops.top() <= *op)
    {
        postExp.push_back(TokPtr(ops.top()));
        ops.pop();
    }

    ops.push(op);
}

template<typename T>
inline void bdg::core::SequenceGenerator<T>::addRightBracket(
    std::stack<bdg::core::Operator<T>*>& ops,
    std::vector<TokPtr>& postExp)
{
    LeftBracket<T> compLB;
    // Looking for left bracket on stack.
    // Push all tokens on stack on the path.
    while (!ops.empty() && compLB != *ops.top())
    {
        postExp.push_back(TokPtr(ops.top()));
        ops.pop();
    }

    // Didn't find pairing bracket.
    if (ops.empty())
        throw Exception(Error::INVALID_EXPR);

    assert(compLB == *ops.top());
    // Discard created bracket from stack. We have to delete manualy.
    // todo: CHANGE THIS!!!!
    Operator<T>* op = ops.top();
    ops.pop();
    delete op;
}

template<typename T>
inline void bdg::core::SequenceGenerator<T>::addVar(
    std::stack<bdg::core::Operator<T>*>& ops,
    std::vector<TokPtr>& postExp,
    const std::string& var)
{
    // Default value if no [] is -1.
    int num = -1;
    auto pos = var.find('[');
    if (pos != std::string::npos)
    {
        auto bpos = var.find(']');
        if (bpos == std::string::npos || bpos < pos)
            throw Exception("Can't find pairing ] in value expression.");

        try
        {
            num = std::stoi(var.substr(pos + 1, bpos - pos - 1));
        }
        catch (...)
        {
            throw Exception("Can't convert value in [] to int in value expression");
        }

        if (num > -1)
            throw Exception("Number in [] has to be below zero in value expression");
    }

    // Store signed number.
    typename DynSeqGenerator::refRecord_t newRef{ var.substr(1, pos - 1), -num - 1 };
    postExp.push_back(TokPtr(new Var<T>(newRef.first, newRef.second)));

    auto foundIt = std::find_if(this->refs.begin(), this->refs.end(), [&newRef](typename DynSeqGenerator::refRecord_t& ref)
    {
        return newRef.first == ref.first;
    });

    if (foundIt == this->refs.end())
    {
        this->refs.push_back(newRef);
    }
    // Need more history.
    else if (foundIt->second < newRef.second)
    {
        foundIt->second = newRef.second;
    }
}
