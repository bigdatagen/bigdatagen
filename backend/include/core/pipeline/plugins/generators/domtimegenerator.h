/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file domtimegenerator.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include "domaingenerator.h"

#include <string> // string
#include <utility> // pair.

namespace bdg
{
	namespace core
	{
        /**
        * Generator for domain time. Is only wrapper around domaingenerator.
        */
		class DomTimeGenerator :
            public DomainGenerator<std::string>
		{
		public:
            /**
            * Pair with date and time.
            */
            using date_time_t = std::pair<std::string, std::string>;

            DomTimeGenerator(
                const std::string& format,
                std::vector<date_time_t>&& vals);

        private:
            static std::vector<std::string> transformDt(const std::string& format, std::vector<date_time_t>&& vals);
		};
	}
}