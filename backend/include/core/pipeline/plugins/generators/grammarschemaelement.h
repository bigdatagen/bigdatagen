#pragma once

/**
*
* @file grammarschemaelement.h
*
*/

#include "jsonschemaelement.h" 

#include <Poco/JSON/Object.h> 

namespace bdg
{
    namespace core
    {
        struct GrammarSchemaElement :
            public JsonSchemaElement
        {
        public:
            GrammarSchemaElement(const Poco::JSON::Object::Ptr& obj, bool hasTypeName = false);
            //GrammarSchemaElement(const Poco::JSON::Object::Ptr& obj, const std::string& typeName);

            virtual std::string getTypeName() const override;
            virtual std::string getType() const override;

        private:
            const std::string typeName;
        };
    }
}