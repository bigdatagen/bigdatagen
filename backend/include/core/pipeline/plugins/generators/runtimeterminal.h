/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file runtimeterminal.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include "grammarelement.h" // GrammarElement

#include <vector> // vector
#include <limits> // numeric_limits

namespace bdg
{
	namespace core
	{
        /**
        * @todo: Unused class remote it.
        *
        */
		class RuntimeTerminal :
			public GrammarElement
		{
		public:
            RuntimeTerminal(const std::vector<GrammarElement::ptr_t>& elems);
            RuntimeTerminal(std::vector<GrammarElement::ptr_t>&& elems);

            void move(std::vector<GrammarElement::ptr_t>&& elems);
		};
	}
}
