/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Generator
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/bdgapi.h"                                                   // Plugin API
#include "core/pipeline/plugins/generators/generator.h"	                                    // Generator
#include "core/pipeline/plugins/generators/csvschemaparser/csvgeneratorschemaparser.h"      // CsvGeneratorSchemaParser
#include "core/pipeline/plugins/generators/agenerator.h"                                    // Generator

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            namespace plugin
            {
                /**
                 * Plugin which is able to generate CSV files according to a given schema.
                 */
                class CsvGenerator: public Generator<CsvGeneratorSchemaParser>
                {
                public:
                    CsvGenerator();
                };
            }
        }
    }
}
