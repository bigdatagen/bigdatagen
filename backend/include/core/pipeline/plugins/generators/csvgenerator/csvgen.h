#pragma once

/**
 *
 * @file jsongen.h
 *
 */

#include "../genstorer.h" // GenStorer
#include "../generator.h" // Generator
#include "../grammarelement.h" // GrammarElement

#include <string> // string
#include <functional> // function
#include <vector>	 // vector

namespace bdg
{
    namespace core
    {
		// Forward declarations.
		class CsvSchemaParser;
        class IntGenerator;
        class DynSeqGenerator;

		class CsvGen :
			public Generator
		{
		public:
            CsvGen() = default;
            CsvGen(const CsvGen&) = default;
            CsvGen& operator=(const CsvGen&) = default;

            void setMaster();

            void openStorage(const std::string& fileName);
            void closeStorage();
            
			void generate();

			void setFilesCount(std::size_t fcount);

            /**
            * @brief Generate function.
            * @note Should be used only on master.
            */
            const std::vector<int>& genRemInts();

            /**
            * @brief Function generates start for each sequence
            */
            const std::vector<std::size_t>& genSeqsStart();
            
            /**
            * @brief Function register remote ints. 
            * @note Should be used only on slave.
            */
            //void regRemInts(std::vector<int>&& remInts);
            void regRemInts(const std::vector<int>& remInts);

            /**
            * @brief Set start index for each sequence
            */
            //void setSeqsStart(std::vector<std::size_t>&& startIdcs);
            void setSeqsStart(const std::vector<std::size_t>& startIdcs);

		private:
            /**
            * @brief Remote int generators.
            */
            std::vector<IntGenerator*> remIntGens;
            std::vector<std::vector<int>> remIntVals;

            /**
            * @brief Generated multiplicator. 
            */
            std::size_t genMult = 0;

            /**
            * @brief Dependant generator for each generator.
            */
            std::vector<std::size_t> remGensGen;

            /**
            * @brief Global sequences.
            */
            std::vector<GrammarElement::GlobSeq> globSeqs;
            
            /**
            * @brief Holds sequence index number for each global sequence.
            */
            std::vector<std::size_t> seqsIdx;
            std::vector<std::size_t> lastSeqsIdx;

            /**
            * @brief Pre generate functions. 
            */
            std::vector<gen_fn_t> preGenFncs;

            /**
            * @brief Generate functions.
            */
			std::vector<gen_fn_t> genFncs;

            GenStorer genStorer;

            bool isMaster = false;
		};			
    }
}
