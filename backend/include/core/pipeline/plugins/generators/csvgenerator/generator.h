#pragma once

/**
*
* @file generator.h
*
*/

#include "historystorage.h" // historystorage
#include "dyngenerator.h" // DynGenerator
//#include "sequencegenerator.h" // DynSeqGenerator
#include "exception.h" // Exception
#include "utils/convert.h" // Convert functions
#include "genstorer.h" // GenStorer
#include "remotegenerator.h" // RemoteGenerator

#include <cassert> // assert

namespace bdg
{
	namespace core
	{
        class GenStorer;

		class DynSeqGenerator;
		template<typename T>
        class SequenceGenerator;

        template<typename T, class Der>
        class Generator:
            public DynGenerator
        {
        public:
            inline Generator() = default;

            inline void regData(const std::vector<T>& data);
            inline void regData(std::vector<T>&& data);

            inline bool isRemote() const;
            
            // Has to implement.

            // void init(HistoryStorage<T>& h);
            // T generateValue();
            // void generate(GenStorer& gs);
            
        protected:
            HistoryStorage<T> history;
            RemoteGenerator<T> remote;

        private:
            virtual inline void setRemoteImpl() override;

            // Inherited via DynGenerator
            inline void regHistoryImpl(std::size_t length) override;

            // Inherited via DynGenerator
            inline void regSeqRefImpl(DynSeqGenerator* dynSeqGen, const std::string& ref) override;

            // Inherited via DynGenerator
            inline void registerGeneratorImpl(
                std::vector<GrammarElement::gen_fn_t>& preGenFncs,
                std::vector<GrammarElement::gen_fn_t>& genFncs) override;

            inline Der& baseCast();

            /// @brief Is remote?
            bool isRem = false;
        };
    }
}

#include "sequencegenerator.h" // SequenceGenerator

template<typename T, class Der>
inline void bdg::core::Generator<T, Der>::setRemoteImpl()
{
    isRem = true;
}

template<typename T, class Der>
inline void bdg::core::Generator<T, Der>::regData(const std::vector<T>& data)
{
    remote.regData(data);
}

template<typename T, class Der>
inline void bdg::core::Generator<T, Der>::regData(std::vector<T>&& data)
{
    remote.regData(std::move(data));
}

template<typename T, class Der>
inline bool bdg::core::Generator<T, Der>::isRemote() const
{
    return isRem;
}

template<typename T, class Der>
inline void bdg::core::Generator<T, Der>::regHistoryImpl(std::size_t length)
{
    history.resize(length + 1);
}

template<typename T, class Der>
inline void bdg::core::Generator<T, Der>::regSeqRefImpl(DynSeqGenerator* dynSeqGen, const std::string& ref)
{
    bdg::core::SequenceGenerator<T>* sg = dynamic_cast<SequenceGenerator<T>*>(dynSeqGen);
    if (sg == nullptr)
        throw Exception(Error::BAD_REFERENCE_TYPE);

    sg->regDataRef(history.getData(), ref);
}

template<typename T, class Der>
inline void bdg::core::Generator<T, Der>::registerGeneratorImpl(
    std::vector<bdg::core::GrammarElement::gen_fn_t>& preGenFncs,
    std::vector<bdg::core::GrammarElement::gen_fn_t>& genFncs)
{
    // Prepare generating.
    preGenImpl();

    preGenFncs.push_back([this](GenStorer& gs)
    {
        (void)gs;
        baseCast().init(history);
    });
    
    if (history.isEnabled())
    {
        if (isRem)
        {
            genFncs.push_back([this](GenStorer& gs)
            {
                T val = remote.generate();
                history.store(val);
                gs.store(std::move(bdg::utils::toString<T>(val)));
            });
        }
        else
        {
            genFncs.push_back([this](GenStorer& gs)
            {
                T val = baseCast().generateValue();
                history.store(val);
                gs.store(std::move(bdg::utils::toString<T>(val)));
            });
        }
    }
    else
    {
        // Always has to be local.
        assert(!isRem);

        genFncs.push_back([this](GenStorer& gs)
        {
            baseCast().generate(gs);
        });
    }
}

template<typename T, class Der>
inline Der& bdg::core::Generator<T, Der>::baseCast()
{
    return static_cast<Der&>(*this);
}
