/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file intgenerator.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include "elemgen.h"
#include "distributions.h"
#include "constants.h"

#include <random> // gen

namespace bdg
{
	namespace core
	{
        /**
        * Generator for integers.
        */
		class IntGenerator:
            public ElemGen<int, IntGenerator>
		{
		public:
			explicit IntGenerator(int minValue, int maxValue, const std::string &distrName = bdg::core::Constants::DISTR_NORMAL);
            virtual ~IntGenerator() = default;

            int getMin() const;
            int getMax() const;
            bool isConst() const;

            void registerInc(const std::function<bool(int)>& incrementer);
            const std::function<bool(int)>& getInc() const;

            void init(HistoryStorage<int>& h);
			int generateValue();
            int remGenValue();
            void generate(GenStorer& gs);

		private:
            /**
            * Real generator.
            */
			std::default_random_engine gen;

            /**
            * Generators distribution.
            */
			DistrBase<int>::ptr_t distr;

            /**
            * Unused.
            * @todo: Remove me.
            */
            std::function<bool(int)> incFnc = [](int) { return false; };

		};
	}
}
