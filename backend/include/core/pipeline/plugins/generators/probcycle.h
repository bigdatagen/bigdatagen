#pragma once

/**
*
* @file arraygenerator.h
*
*/

#include "intgenerator.h" // IntGenerator
#include "cycle.h" 

#include <string> // string
#include <random> // gen
#include <memory> // shared_ptr
#include <functional> // function

namespace bdg
{
	namespace core
	{
		class ProbCycle:
			public Cycle<ProbCycle>
		{
		public:
            ProbCycle(
                const GrammarElement::ptr_t& elem,
                size_t cycleProb);
                
            ProbCycle(
                GrammarElement::ptr_t&& elem,
                size_t cycleProb);

            void initLoop();
            bool isEnd();
            void inc();

		private:
            virtual void repairImpl(
                const GrammarElement::element_map_t& parsedEls,
                const GrammarElement::seq_vector_t& locSeqs,
                GrammarElement::intGenTypeFnc_vec_t& locGens,
                GrammarElement::intGenTypeFnc_vec_t& remGens,
                std::vector<std::vector<GrammarElement::genIdxTypeFnc_t>>& remGensGens,
                std::vector<GrammarElement::GlobSeq>& globSeqs,
                std::vector<const GrammarElement*>& pathEls) override;
            
            IntGenerator sizeGen;
            const int cycleProb;
            // Stores function.
            std::function<int(void)> gen;
		};
	}
}
