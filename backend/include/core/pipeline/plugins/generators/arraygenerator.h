/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file arraygenerator.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include "intgenerator.h" // IntGenerator
#include "cycle.h" 

#include <string> // string
#include <random> // gen
#include <memory> // shared_ptr

namespace bdg
{
	namespace core
	{
        /**
        * Generator of arrays. 
        */
		class ArrayGenerator :
			public Cycle<ArrayGenerator>
		{
		public:
            ArrayGenerator(
                int minLen, 
                int maxLen, 
                const std::vector<GrammarElement::ptr_t>& elems, 
                const std::string &distrName,
                const std::string& brStart = "[", 
                const std::string& brEnd = "]", 
                const std::string& delim = ",");

            ArrayGenerator(
                int minLen, 
                int maxLen, 
                std::vector<GrammarElement::ptr_t>&& elems, 
                const std::string &distrName,
                const std::string& brStart = "[", 
                const std::string& brEnd = "]", 
                const std::string& delim = ",");

            void initLoop();
            bool isEnd();
            void inc();

		private:
            virtual void repairImpl(
                const GrammarElement::element_map_t& parsedEls,
                const GrammarElement::seq_vector_t& locSeqs,
                GrammarElement::intGenTypeFnc_vec_t& locGens,
                GrammarElement::intGenTypeFnc_vec_t& remGens,
                std::vector<std::vector<GrammarElement::genIdxTypeFnc_t>>& remGensGens,
                std::vector<GrammarElement::GlobSeq>& globSeqs,
                std::vector<const GrammarElement*>& pathEls) override;

            /**
            * Generator to generate array size
            */
            IntGenerator sizeGen;

            /**
            * Generated size of array
            */
            int sizeVal;

            /**
            * Index in array
            */
            int i;
		};
	}
}
