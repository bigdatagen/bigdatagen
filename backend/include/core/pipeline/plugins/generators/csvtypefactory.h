#pragma once

/**
*
* @file typefactory.h
*
*/

#include "typefactory.h" // TypeFactory, SchemaElement.
#include "jsontypefactory.h"
#include "jsonschemaelement.h"
//#include "sequencegenerator.h"
#include "schemaparser.h"

#include <Poco/JSON/Object.h>

#include <functional> // Function.
#include <vector>
#include <string>

namespace bdg
{
    namespace core
    {
        class CsvTypeFactory:
            public JsonTypeFactory
        {
        public:
            static GrammarElement::ptr_t createRoot(
                TypeFactory& tf,
                const SchemaElement::uptr_t& se,
                SchemaParser& parser);
            
			CsvTypeFactory(const factory_fnc_t& nonFoundFnc, std::vector<name_factory_fnc_t>&& init);

        private:
			static GrammarElement::ptr_t createRootImpl(
                SchemaParser& parser,
                TypeFactory& tf,
                Poco::JSON::Object::Ptr const& jsonObject,
                GrammarElement::element_map_t &parsedElements,
                std::vector<DynSeqGenerator*>& seqs);

			/* Just a helper method for the createRootImpl method. */
            static GrammarElement::ptr_t createRowElement(
                SchemaParser& parser,
                TypeFactory& tf,
                Poco::JSON::Object::Ptr const& jsonObject,
                GrammarElement::element_map_t &parsedElements,
                std::vector<DynSeqGenerator*>& seqs,
                const std::string &delim);
        };
    }
}
