/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file referencelement.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include "nonterminal.h" // Nonterminal

#include <string> // string

namespace bdg
{
	namespace core
	{
        /**
        * Holds reference to some kind of element. Its possible to add 
        * grammar element later. Is used for later definitions.
        */
		class ReferenceElement :
            // Could be terminal but it doesn't make sense semanticaly.
			public Nonterminal
		{
		public:
            ReferenceElement(const std::string& type);

        private:
            virtual void repairImpl(
                const GrammarElement::element_map_t& parsedEls,
                const GrammarElement::seq_vector_t& locSeqs,
                GrammarElement::intGenTypeFnc_vec_t& locGens,
                GrammarElement::intGenTypeFnc_vec_t& remGens,
                std::vector<std::vector<GrammarElement::genIdxTypeFnc_t>>& remGensGens,
                std::vector<GrammarElement::GlobSeq>& globSeqs,
                std::vector<const GrammarElement*>& pathEls) override;

            void checkRecursion(
                const std::vector<const GrammarElement*>& pathEls,
                const std::string& typeName);

            /**
            * Name of element that it wait for.
            */
            const std::string name;
		};
	}
}