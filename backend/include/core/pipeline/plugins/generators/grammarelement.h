/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file grammarelement.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include <vector> // vector
#include <memory> // shared_ptr
#include <unordered_map> // unordederd_map
#include <functional> // function
#include <utility> // forward


namespace bdg
{
    namespace core
    {
        // Forward declarations.
        class ReferenceElement;
        class DynSeqGenerator;
        class IntGenerator;
        class DoubleGenerator;
        class DynGenerator;
        class GenStorer;
        
        template<typename T>
        class RefNumGenerator;

        /**
        * Base class for all elements.
        */
        class GrammarElement
        {
        public:
            using element_map_t = std::unordered_map<std::string, std::shared_ptr<GrammarElement>>;
            using element_vector_t = std::vector<std::shared_ptr<GrammarElement>>;
            using ref_vector_t = std::vector<ReferenceElement*>;
            using seq_vector_t = std::vector<DynSeqGenerator*>;
            using gen_vector_t = std::vector<DynGenerator*>;
            
            using ptr_t = std::shared_ptr<GrammarElement>;
            
            /// @brief Function gets storer and returns new function index number.
            using gen_fn_t = std::function<std::size_t(GenStorer&)>;
            
            template<typename T>
            using refNumGen_vector_t = std::vector<RefNumGenerator<T>*>;
            
            /**
            * @brief Generator type
            */
            enum class IntGenType
            {
                /// @brief Needs only one time generation. (Array)
                ONE_TIME,

                /// @brief Needs generation until some end. (ProbCycle)
                CONTINUOUS
            };

            /**
            * Functor that mimics lambda function. We needed because
            * we want to be able to compare them.
            */
            struct CountTransFtor
            {
                using fnc_t = std::function<int(const CountTransFtor& refThis, int genVal)>;

                CountTransFtor();
                CountTransFtor(const CountTransFtor& rhs);
                CountTransFtor(const fnc_t& fnc, size_t sa1, size_t sa2);
                CountTransFtor(const fnc_t& fnc, int ia1);
                CountTransFtor(const fnc_t& fnc, int ia1, int ia2);
                
                CountTransFtor& operator=(const CountTransFtor& rhs);

                int operator()(int genValue);
                bool operator==(const CountTransFtor& rhs) const;

                fnc_t f;

                bool sarg1Used;
                size_t sarg1;
                bool sarg2Used;
                size_t sarg2;

                bool iarg1Used;
                int iarg1;
                bool iarg2Used;
                int iarg2;
            };

            /**
            * @brief Function transform number of appearences for ONE_TYPE generators.
            *   For CONTINUOUS generators it return 0, 1 whether we should continue
            */
            using countTransformer_t = CountTransFtor;

            using genIdxTypeFnc_t = std::tuple<size_t, IntGenType, countTransformer_t>;

            using typeFnc_t = std::tuple<IntGenType, countTransformer_t>;

            /**
            * @brief Holds generator, its type and count transformer function.
            */
            using intGenTypeFnc_t = std::tuple<IntGenerator*, IntGenType, countTransformer_t>;

            using typeGen_t = std::tuple<IntGenType, IntGenerator*>;
            using typeGen_vec_t = std::vector<typeGen_t>;
            
            using intGenTypeFnc_vec_t = std::vector<intGenTypeFnc_t>;
            using intGen_vector_t = std::vector<IntGenerator*>;
            using doubleGen_vector_t = std::vector<DoubleGenerator*>;

            struct GlobSeq
            {
                /**
                * @brief Ptr to real generator
                */
                DynSeqGenerator* seq;
                
                /**
                * @brief Sequence dependant generators.
                */
                std::vector<std::size_t> depGens;

                /**
                * @brief Remembers for each dependant generator it's type and function.
                */
                std::vector<typeFnc_t> typeFncs;

                /**
                * @brief Number of use of global sequnce without dependence
                */
                std::size_t simpleCount = 0;


            };

            template<class GT, class... Args>
            static inline ptr_t makeGe(Args&&... args);

            GrammarElement() = default;

            void registerGenerator(
                std::size_t& currIdx,
                std::vector<gen_fn_t> &preGenFncs,
                std::vector<gen_fn_t> &genFncs);
            
            void repair(
                const GrammarElement::element_map_t& parsedEls,
                const GrammarElement::seq_vector_t& seqs,
                GrammarElement::intGenTypeFnc_vec_t& locGens,
                GrammarElement::intGenTypeFnc_vec_t& remGens,
                std::vector<std::vector<GrammarElement::genIdxTypeFnc_t>>& remGensGens,
                std::vector<GrammarElement::GlobSeq>& globSeqs,
                std::vector<const GrammarElement*>& pathEls);

        protected:
            virtual void registerGeneratorImpl(
                std::size_t& currIdx,
                std::vector<gen_fn_t> &preGenFncs,
                std::vector<gen_fn_t> &genFncs) = 0;

            virtual void repairImpl(
                const GrammarElement::element_map_t& parsedEls,
                const GrammarElement::seq_vector_t& seqs,
                GrammarElement::intGenTypeFnc_vec_t& locGens,
                GrammarElement::intGenTypeFnc_vec_t& remGens,
                std::vector<std::vector<GrammarElement::genIdxTypeFnc_t>>& remGensGens,
                std::vector<GrammarElement::GlobSeq>& globSeqs,
                std::vector<const GrammarElement*>& pathEls);

        };
    }
}

/**
* Function for creating derived class from grammar element and insert it into 
* grammar element shared_ptr.
*/
template<class GT, class... Args>
inline bdg::core::GrammarElement::ptr_t bdg::core::GrammarElement::makeGe(Args&&... args)
{
    return GrammarElement::ptr_t(new GT(std::forward<Args>(args)...));
}

