/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file timesequencegenerator.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include "sequencegenerator.h" // DynSeqGenerator

#include <string> // string
#include <chrono>

namespace bdg
{
	namespace core
	{
        /**
        * Geneator for timesequences.
        */
		class TimeSeqGenerator :
            public DynSeqGenerator, public ElemGen<std::string, TimeSeqGenerator>
		{
		public:
            TimeSeqGenerator(
                const std::string& initDate,
                const std::string& initTime,
                const std::string& step,
                const std::string& format,
                bool isLocal);

            void init(HistoryStorage<std::string>& h);
            std::string generateValue();
            void generate(GenStorer& gs);

        private:
            static const char timeSplit = ':';
            static const char dateSplit = '-';

            static const std::string SEC;
            static const std::string MIN;
            static const std::string HOUR;
            static const std::string DAY;
            static const std::string MONTH;
            static const std::string YEAR;

            static const std::string TIME_FORMAT;
            static const std::string DATE_FORMAT;

            static std::chrono::system_clock::time_point parseInitial(
                const std::string& initTime,
                const std::string& initDate);
            static std::chrono::seconds parseStep(const std::string& step);

            virtual void setStartImpl(std::size_t startIdx) override;

            void preGenImpl(std::vector<GrammarElement::gen_fn_t>& preGenFncs) override;

            virtual void repairImpl(
                const GrammarElement::element_map_t& parsedEls,
                const GrammarElement::seq_vector_t& seqs,
                GrammarElement::intGenTypeFnc_vec_t& locGens,
                GrammarElement::intGenTypeFnc_vec_t& remGens,
                std::vector<std::vector<GrammarElement::genIdxTypeFnc_t>>& remGensGens,
                std::vector<GrammarElement::GlobSeq>& globSeqs,
                std::vector<const GrammarElement*>& pathEls) override;

            /**
            * Time format.
            */
            std::string format;
            
            /// @brief Step in seconds.
            const std::chrono::seconds step;

            /// @brief Initial time.
            const std::chrono::system_clock::time_point initPoint;

            /// @brief Generated time.
            std::chrono::system_clock::time_point currTime;

            /// @brief Start index. 
            std::size_t startIdx = 0;

            /// @brief Index of last generated value.
            std::size_t idx = 0;

            /**
            * Type of sequnce.
            */
            const bool isLocal;

            bool isPreGenRegistered = false;
		};
	}
}