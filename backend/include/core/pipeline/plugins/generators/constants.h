/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file constants.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include <string> // string

namespace bdg
{
    namespace core
    {
        /**
        * Holds all constants used while parsing.
        */
        namespace Constants {

            const std::string ITEM_TYPE_NAME = "typeName";
            const std::string ITEM_PROPERTY_NAME = "propertyName";
            const std::string ITEM_TYPE = "type";
            const std::string ITEM_PROPERTIES = "properties";
            const std::string ITEM_SEPARATOR = "separator";
            const std::string ITEM_COLUMNS = "columns";
            const std::string ITEM_DEFINITION = "definition";
            const std::string ITEM_CHILDREN = "children";

            const std::string ITEM_TYPE_OBJECT = "object";
            const std::string ITEM_TYPE_ARRAY = "array";
            const std::string ITEM_TYPE_STRING = "string";
            const std::string ITEM_TYPE_INT = "int";
            const std::string ITEM_TYPE_FLOAT = "float";
            const std::string ITEM_TYPE_ROOT = "ROOT";
            const std::string ITEM_TYPE_SEQUENCE = "sequence";
            const std::string ITEM_TYPE_PROBSET = "probSet";
            const std::string ITEM_TYPE_DATETIME = "datetime";

            const std::string ITEM_TYPE_COMPLEX = "complex";
            const std::string ITEM_TYPE_CYCLE = "cycle";
            const std::string ITEM_TYPE_ALT = "alternation";
            const std::string ITEM_TYPE_EMPTY = "empty";
            const std::string ITEM_TYPE_GRAMMAR = "grammar";
            const std::string ITEM_TYPE_WHITE_SPACE = "whiteSpace";

            const std::string PROPERTY_CYCLE_TYPE = "cycleType";
            const std::string PROPERTY_MIN = "min";
            const std::string PROPERTY_MAX = "max";
            const std::string PROPERTY_SOURCE = "source";
            const std::string PROPERTY_RANDOM = "random";
            const std::string PROPERTY_DOMAIN = "domain";
            const std::string PROPERTY_DISTRIBUTION = "distribution";
            const std::string PROPERTY_VISIBILITY = "visibility";
            const std::string PROPERTY_VALUE = "value";
            const std::string PROPERTY_INITIAL_VALUE = "initialValues";
            const std::string PROPERTY_INNER_TYPE = "innerType";
            const std::string PROPERTY_PROB = "value";
            const std::string PROPERTY_INIT_TIME = "initialTime";
            const std::string PROPERTY_INIT_DATE = "initialDate";
            const std::string PROPERTY_STEP = "step";
            const std::string PROPERTY_FORMAT = "format";
            const std::string PROPERTY_INNER_ELEMENT = "innerElement";
            const std::string PROPERTY_CHAR = "char";
            const std::string PROPERTY_CHAR_SPACE = "space";
            const std::string PROPERTY_CHAR_TAB = "tab";
            const std::string PROPERTY_CHAR_NEW_LINE = "newLine";

            const std::string PROP_VISIBILITY_GLOBAL = "global";
            const std::string PROP_VISIBILITY_LOCAL = "local";

            const std::string PROP_PROBABILITY = "probability";
            const std::string PROP_CYCLE_REP = "repetitions";
            const std::string PROP_CYCLE_MIN = "cycleMin";
            const std::string PROP_CYCLE_MAX = "cycleMax";
            const std::string PROP_CYCLE_PROB = "cycleProbability";

            const std::string MIN_DATE = "minDate";
            const std::string MAX_DATE = "maxDate";
            const std::string MIN_TIME = "minTime";
            const std::string MAX_TIME = "maxTime";

            const std::string DATE = "date";
            const std::string TIME = "time";
            

            const std::string DISTR_UNIFORM = "uniform";
            const std::string DISTR_NORMAL = "normal";
            //const std::string DISTR_BERNOULI = "bernouli";
            const std::string DISTR_BINOMIAL = "binomial";
            const std::string DISTR_GEOMETRIC = "geometric";
            const std::string DISTR_POISSON = "poisson";
            const std::string DISTR_EXP = "exponential";

            const std::string EMPTY = "";
        };
    }
}