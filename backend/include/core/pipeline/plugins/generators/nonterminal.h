/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file nonterminal.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include "grammarelement.h" // GrammarElement

#include <vector> // vector
#include <memory> // shared_ptr

namespace bdg
{
	namespace core
	{
        /**
        * Holds multiple grammar elements together. It represents composite.
        */
		class Nonterminal :
			public GrammarElement
		{
		public:
            Nonterminal() = default;
            explicit Nonterminal(const std::vector<GrammarElement::ptr_t>& elems);
            explicit Nonterminal(std::vector<GrammarElement::ptr_t>&& elems);
            explicit Nonterminal(std::initializer_list<GrammarElement::ptr_t> l);
            explicit Nonterminal(GrammarElement::ptr_t&& elem);

			void push_back(const std::shared_ptr<GrammarElement>& element);
            void push_back(std::shared_ptr<GrammarElement>&& element);

            void move(std::vector<GrammarElement::ptr_t>&& elems);

			const std::shared_ptr<GrammarElement> &get(size_t index) const;
			std::shared_ptr<GrammarElement> &get(size_t index);
			size_t size() const;

        protected:
            virtual void repairImpl(
                const GrammarElement::element_map_t& parsedEls,
                const GrammarElement::seq_vector_t& seqs,
                GrammarElement::intGenTypeFnc_vec_t& locGens,
                GrammarElement::intGenTypeFnc_vec_t& remGens,
                std::vector<std::vector<GrammarElement::genIdxTypeFnc_t>>& remGensGens,
                std::vector<GrammarElement::GlobSeq>& globSeqs,
                std::vector<const GrammarElement*>& pathEls) override;

            /**
            * Stored grammar elements.
            */
            std::vector<GrammarElement::ptr_t> elements;

		private:
            virtual void registerGeneratorImpl(
                std::size_t& currIdx,
                std::vector<GrammarElement::gen_fn_t>& preGenFncs,
                std::vector<GrammarElement::gen_fn_t>& genFncs) override;
		};
	}
}