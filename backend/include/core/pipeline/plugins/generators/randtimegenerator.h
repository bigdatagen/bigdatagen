/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file randtimegenerator.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include "elemgen.h" // generator
#include "intgenerator.h"

#include <string> // string
#include <utility> // pair.

namespace bdg
{
	namespace core
	{
        /**
        * Generator for random time in given interval.
        */
		class RandTimeGenerator :
            public ElemGen<std::string, RandTimeGenerator>
		{
		public:
            RandTimeGenerator(
                const std::string& format,
                const std::string& minDate,
                const std::string& minTime,
                const std::string& maxDate,
                const std::string& maxTime);

            void init(HistoryStorage<std::string>& h);
            std::string generateValue();
            void generate(GenStorer& gs);

        private:
            static std::chrono::system_clock::time_point parseDateTime(const std::string& date, const std::string& time);

            std::string genRandValImpl();
            std::string genDomainValImpl();

            /**
            * Initial date and time.
            */
            const std::chrono::system_clock::time_point initDt;
            
            /**
            * End date and time
            */
            const std::chrono::system_clock::time_point endDt;
            
            /**
            * Format to print in.
            */
            const std::string format;

            /**
            * Random generator that picks time and date inside interval.
            */
            IntGenerator intGen;

		};
	}
}