/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file elementfactory.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include "grammarelement.h"
#include "stringgenerator.h"
#include "intgenerator.h"
#include "doublegenerator.h"
#include "domaingenerator.h"

#include <string>

namespace bdg
{
    namespace core
    {
        /**
        * Forward declaration.
        */
        class IntGenerator;
        /**
        * Forward declaration.
        */
        class DoubleGenerator;
        /**
        * Forward declaration.
        */
        class StringGenerator;
        /**
        * Forward declaration.
        */
        template<typename InnerT>
        class DomainGenerator;
        /**
        * Forward declaration.
        */
        template<typename InnerT>
        class SequenceGenerator;
        
        /**
        * Generator info traits. Holds information about types and
        * its connected things.
        */
        template<typename T>
        struct GenInfo
        {
            using type = void;
            // Nothing here.
        };
        
        template<>
        struct GenInfo<int>
        {
            using type = int;
            using rnd_gen_t = IntGenerator;
            using dom_gen_t = DomainGenerator<int>;
            using seq_gen_t = SequenceGenerator<int>;
            using limit_t = int;
        };

        template<>
        struct GenInfo<double>
        {
            using type = double;
            using rnd_gen_t = DoubleGenerator;
            using dom_gen_t = DomainGenerator<double>;
            using seq_gen_t = SequenceGenerator<double>;
            using limit_t = double;
        };

        template<>
        struct GenInfo<std::string>
        {
            using type = std::string;
            using rnd_gen_t = StringGenerator;
            using dom_gen_t = DomainGenerator<std::string>;
            using seq_gen_t = SequenceGenerator<std::string>;
            using limit_t = int;
        };

        template<typename T>
        struct ElementFactory
        {
            template<typename... Params>
            static inline GrammarElement::ptr_t create(Params&&... params);

            template<typename... Params>
            static inline GrammarElement::ptr_t createSeq(Params&&... params);
        };
    }
}

template<typename T>
template<typename... Params>
inline bdg::core::GrammarElement::ptr_t bdg::core::ElementFactory<T>::create(Params&&... params)
{
    return GrammarElement::makeGe<typename GenInfo<T>::rnd_gen_t>(std::forward<Params>(params)...);
}

template<typename T>
template<typename... Params>
inline bdg::core::GrammarElement::ptr_t bdg::core::ElementFactory<T>::createSeq(Params&&... params)
{
    return GrammarElement::makeGe<typename GenInfo<T>::seq_gen_t>(std::forward<Params>(params)...);
}