/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file schemaparser.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once 

#include "grammarelement.h" // GrammarElement
#include "schemaelement.h"
#include "jsonutils.h"
#include "typefactory.h"

#include <string>
#include <vector>

namespace bdg
{
	namespace core
	{
        // Forward declaration.
        class Generator;
        class IntGenerator;
        class TypeFactory;
        class JsonTypeFactory;
        class DynSeqGenerator;

        /**
        * Parser of all schemas.
        */
        class SchemaParser
        {
        public:
            SchemaParser(Generator& gen, TypeFactory& tf);

            void parse(const std::string& schema);

            friend class JsonTypeFactory;
			friend class CsvTypeFactory;
            friend class GrammarTypeFactory;
        protected:
            /**
            * @brief Map holds name-element mapping
            */
            GrammarElement::element_map_t allElems;

            /// @brief All sequences.
            std::vector<DynSeqGenerator*> allSeqs;

        private:
            virtual void parseSchema(const std::string& schema) = 0;

            template<bool isUdt>
            inline GrammarElement::ptr_t createElement(
                TypeFactory& tf,
                const SchemaElement::uptr_t& se,
                GrammarElement::element_map_t& parsedTypes,
                std::vector<DynSeqGenerator*>& seqs);

            /**
            * @return true if next udt exists.
            */
            virtual bool hasNextUdt() const = 0;

            /*
            * @brief returns Next schema element.
            */
            virtual SchemaElement::uptr_t getNextUdt() = 0;

            TypeFactory& factory;
            Generator& gen;
		};
	}
}

template<bool isUdt>
inline bdg::core::GrammarElement::ptr_t bdg::core::SchemaParser::createElement(
    TypeFactory& tf,
    const SchemaElement::uptr_t& se,
    GrammarElement::element_map_t& parsedTypes,
    std::vector<DynSeqGenerator*>& seqs)
{
    const auto& factoryFnc = tf.create(se->getType());
    auto res = factoryFnc(tf, se, const_cast<SchemaParser&>(*this));

    const std::string typeName = se->getTypeName();
    // If it is UDT or it has typeName.
    if (isUdt || !typeName.empty())
    {
        // Is udt without typeName.
        if (typeName.empty())
            throw Exception(Error::TYPE_NOT_EXISTS + typeName);

        if (parsedTypes.find(typeName) != parsedTypes.cend())
        {
            throw Exception(Error::TYPE_REDEFINITION + typeName);
        }
        parsedTypes[typeName] = res;
    }

    return res;
}
