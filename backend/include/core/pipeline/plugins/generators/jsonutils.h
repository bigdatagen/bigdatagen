/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file jsonutils.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include "elementfactory.h" // ElementFactory, GenInfo
#include "domaingenerator.h" // DomainGenerator
#include "sequencegenerator.h" // SequenceGenerator.
#include "constants.h" // Constants
#include "grammarelement.h" // GrammarGenerator.

#include <Poco/JSON/Object.h>
#include <Poco/Dynamic/Var.h> // Var

#include <string> // string
#include <memory> // unique_ptr
#include <unordered_map> // unordered_(multi)map
#include <iterator> // iterator
#include <functional> // function

namespace bdg
{
    namespace core
    {
        /**
        * Utilities for parsing json parser.
        * Contains wrappers around POCO functions and adds
        * check of some things. 
        */
        class JsonUtils
        {
        public:
            template<typename T>
            static inline T getPropertyValue(
                const Poco::JSON::Object::Ptr &obj, 
                const std::string &propertyName,
                bool isPositive = false);

            template<typename T>
            static inline T getArraySimpleValue(
                const Poco::JSON::Array::Ptr& arr,
                std::size_t idx);

            template<typename T>
            static inline std::vector<T> getArraySimpleValues(const Poco::JSON::Array::Ptr& arr);

            static Poco::JSON::Array::Ptr getPropertyArray(
                const Poco::JSON::Object::Ptr &obj,
                const std::string &propertyName);

            static Poco::JSON::Object::Ptr getPropertyObject(
                const Poco::JSON::Object::Ptr &obj,
                const std::string &propertyName);

            static Poco::JSON::Object::Ptr getObject(const Poco::Dynamic::Var &vobj);
            static Poco::JSON::Array::Ptr getArray(const Poco::Dynamic::Var &varr);
        };

        /**
        * Returns typed property of object by name.
        */
        template<typename T>
        inline T JsonUtils::getPropertyValue(
            const Poco::JSON::Object::Ptr &obj,
            const std::string &propertyName,
            bool isPositive)
        {
            auto var = obj->get(propertyName);
            if (var.isEmpty())
            {
                throw Exception(Error::MISSING_PROPERTY + propertyName);
            }

            if (var.isEmpty())
                throw Exception(Error::BAD_PROPERTY_VALUE + propertyName + " - property is empty.");

            try
            {
                return var.convert<T>();
            }
            catch (...)
            {
                throw Exception(Error::BAD_PROPERTY_TYPE + propertyName);
            }
        }

        /**
        * Returns property of type int form object.
        * Adds check for positivity.
        */
        template<>
        inline int JsonUtils::getPropertyValue<int>(
            const Poco::JSON::Object::Ptr &obj,
            const std::string &propertyName,
            bool isPositive)
        {
            auto var = obj->get(propertyName);
            if (var.isEmpty())
            {
                throw Exception(Error::MISSING_PROPERTY + propertyName);
            }

            int res;
            try
            {
                res = { var.convert<int>() };
            }
            catch (...)
            {
                throw Exception(Error::BAD_PROPERTY_TYPE + propertyName);
            }

            if (isPositive && res < 0)
                throw Exception(Error::BAD_PROPERTY_VALUE + propertyName + " - property is negative.");

            return res;
        }

        /**
        * Returns property of type string from object.
        * Checks for nonemptyness.
        */
        template<>
        inline std::string JsonUtils::getPropertyValue<std::string>(
            const Poco::JSON::Object::Ptr &obj,
            const std::string &propertyName,
            bool isPositive)
        {
            auto var = obj->get(propertyName);
            if (var.isEmpty())
            {
                throw Exception(Error::MISSING_PROPERTY + propertyName);
            }

            std::string res;
            try
            {
                res = { var.convert<std::string>() };
            }
            catch (...)
            {
                throw Exception(Error::BAD_PROPERTY_TYPE + propertyName);
            }

            if (res.empty())
                throw Exception(Error::BAD_PROPERTY_VALUE + propertyName + " - property is empty.");

            return res;
        }

    }
}

/**
* Returns element value on given index in array.
*/
template<typename T>
inline T bdg::core::JsonUtils::getArraySimpleValue(const Poco::JSON::Array::Ptr& arr, std::size_t idx)
{
    return arr->get(idx).convert<T>();
}

/**
* Converts whole array to typed vector.
*/
template<typename T>
inline std::vector<T> bdg::core::JsonUtils::getArraySimpleValues(const Poco::JSON::Array::Ptr& arr)
{
    std::vector<T> res(arr->size());
    std::transform(arr->begin(), arr->end(), std::begin(res), [](const Poco::Dynamic::Var& val)
    {
        return val.convert<T>();
    });
    return res;
}
