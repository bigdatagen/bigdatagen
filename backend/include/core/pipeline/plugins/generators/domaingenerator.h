/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file domaingenerator.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include "intgenerator.h" // RandomIntGenerator
#include "utils/convert.h" // toString
#include "elemgen.h" // Generator

#include <vector>   
#include <string>

namespace bdg
{
	namespace core
	{
        /**
        * Domain generator for single types
        */
		template<typename InnerType>
		class DomainGenerator :
			public ElemGen<InnerType, DomainGenerator<InnerType>>
		{
		public:
			inline DomainGenerator(const std::vector<InnerType> &domainValues, const std::string &distrName = Constants::DISTR_NORMAL, bool decorate = true);

            inline void init(HistoryStorage<InnerType>& h);
            inline InnerType generateValue();
            inline void generate(GenStorer& gs);

		private:
            /**
            * Start character.
            */
            static const char START_CHAR = '\"';

            /**
            * End character.
            */
            static const char END_CHAR = '\"';

            InnerType genDecVal();
            InnerType genUndecVal();

            /**
            * Member function pointer to function that generates.
            * We don't want check in runtime so we store pointer
            * in constructor.
            */
            InnerType(DomainGenerator<InnerType>::* const genImpl)();
            
            /**
            * Domain values. 
            */
			std::vector<InnerType> domainValues;

            /**
            * Generator that chooses values from domains.
            */
			IntGenerator chooserGenerator;
		};

	}
}

template<typename InnerType>
inline bdg::core::DomainGenerator<InnerType>::DomainGenerator(
    const std::vector<InnerType>& domainValues, 
    const std::string &distrName,
    bool decorate) :
genImpl(
    (decorate && typeid(InnerType) == typeid(std::string)) ? 
    &DomainGenerator<InnerType>::genDecVal :
    &DomainGenerator<InnerType>::genUndecVal),
domainValues(domainValues),
chooserGenerator(0, static_cast<int>(domainValues.size() - 1), distrName)
{
}

template<typename InnerType>
inline void bdg::core::DomainGenerator<InnerType>::init(HistoryStorage<InnerType>& h)
{
    // Do nothing.
}

/**
* Function only calls member function pointer.
*/
template<typename InnerType>
inline InnerType bdg::core::DomainGenerator<InnerType>::generateValue()
{
    return (this->*genImpl)();
}

template<typename InnerType>
inline void bdg::core::DomainGenerator<InnerType>::generate(GenStorer &gs)
{
    gs.store(std::move(bdg::utils::toString(generateValue())));
}

/**
* Generates decorated values.
*/
template<typename InnerType>
inline InnerType bdg::core::DomainGenerator<InnerType>::genDecVal()
{
    return START_CHAR + domainValues[chooserGenerator.generateValue()] + END_CHAR;
}

/**
* Generates undecorated values.
*/
template<typename InnerType>
inline InnerType bdg::core::DomainGenerator<InnerType>::genUndecVal()
{
    return domainValues[chooserGenerator.generateValue()];
}
