/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file distributions.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include "utils/exception.h"

#include <memory>
#include <vector>
#include <string>
#include <random>
#include <iostream>

namespace bdg
{
    namespace core
    {
		/*
		 * Distr class for all distributions
		 * typename T - int or double
		 */
		template<typename T>
		class DistrBase
		{
			public:
				using ptr_t = std::unique_ptr<DistrBase<T>>;

				virtual T generate(std::default_random_engine &gen) = 0;
				virtual T getMin() = 0;
				virtual T getMax() = 0;

				virtual ~DistrBase() {};
		};
		

		/*
		 * Wrapper for the default <random> distributions.
		 * Distr class for the modified <random> distributions.
		 * class D - distribution from the <random> library
		 */
		template<typename T, class D>
		class SmartDistr :
			public DistrBase<T>
		{
			public:
				inline SmartDistr(T min, T max);

				inline virtual T generate(std::default_random_engine &gen) override;
				inline virtual T getMin() override;
				inline virtual T getMax() override;

			protected:
				inline SmartDistr(T min, T max, D distr);

				D distr;
				T minVal;
				T maxVal;
		};

		/*
		 * Implementation of the normal distribution with fixed mean and variance.
		 * The generated value is rescaled with regard to the desired min-max interval.
		 */
		template<typename T>
		class NormalDistr :
			public SmartDistr<T, std::normal_distribution<double>>
		{
			public:
				NormalDistr(T min, T max);

				inline virtual T generate(std::default_random_engine &gen);
		};
		
		/*
         * Implementation of the poisson distribution with mean parameter derived
		 * from the min-max values.
         * The value is generated until it fits into the min-max interval.
         */
		template<typename T>
        class PoissonDistr :
            public SmartDistr<T, std::poisson_distribution<int>>
        {
            public:
                PoissonDistr(T min, T max);

                inline virtual T generate(std::default_random_engine &gen);
        };

        /*
         * Implementation of the exponential distribution with a fixed parameter.
         * The value is generated until it fits into the min-max interval.
         */
        template<typename T>
        class ExpDistr :
            public SmartDistr<T, std::exponential_distribution<double>>
        {
            public:
                ExpDistr(T min, T max);

                inline virtual T generate(std::default_random_engine &gen);
        };

		/*
         * Implementation of the geometric distribution with a parameter derived
		 * from the size of the interval.
         */
        template<typename T>
        class BinomDistr :
            public SmartDistr<T, std::binomial_distribution<int>>
        {
            public:
                BinomDistr(T min, T max);

                inline virtual T generate(std::default_random_engine &gen);
        };

        /*
         * Implementation of the binomial distribution with a fixed parameter
         * on a given interval.
         */
        template<typename T>
        class GeomDistr :
            public SmartDistr<T, std::geometric_distribution<int>>
        {
            public:
                GeomDistr(T min, T max);

                inline virtual T generate(std::default_random_engine &gen);
        };

    }
}

template<typename T, class D>
inline bdg::core::SmartDistr<T,D>::SmartDistr(T min, T max) :
	minVal(min),
	maxVal(max),
	distr(min, max)
{
}

template<typename T, class D>
inline bdg::core::SmartDistr<T,D>::SmartDistr(T min, T max, D distr) :
	minVal(min),
	maxVal(max),
	distr(distr)
{
}

template<typename T, class D>
inline T bdg::core::SmartDistr<T,D>::generate(std::default_random_engine &gen)
{
	return (T)(this->distr(gen));
}

template<typename T, class D>
inline T bdg::core::SmartDistr<T,D>::getMin()
{
	return minVal;
}

template<typename T, class D>
inline T bdg::core::SmartDistr<T,D>::getMax()
{
	return maxVal;
}

template<typename T>
inline bdg::core::NormalDistr<T>::NormalDistr(T min, T max) :
	SmartDistr<T, std::normal_distribution<double>>(min, max, std::normal_distribution<double>(0,1))
{
}

template<typename T>
inline T bdg::core::NormalDistr<T>::generate(std::default_random_engine &gen)
{
	double resNormalized = this->distr(gen);
	while (resNormalized < -1.0 || resNormalized > 1.0)
	{
		resNormalized = this->distr(gen);
	}
	double mean = (this->minVal + this->maxVal) / 2.0;
	return (T)(mean + ((this->maxVal - mean) * resNormalized));
}

template<typename T>
inline bdg::core::PoissonDistr<T>::PoissonDistr(T min, T max) :
    SmartDistr<T, std::poisson_distribution<int>>(min, max, std::poisson_distribution<int>((max-min)/2))
{
}

template<typename T>
inline T bdg::core::PoissonDistr<T>::generate(std::default_random_engine &gen)
{
	while(true)
	{
		int res = this->distr(gen) + this->minVal;
		/* Beware of overflows */
		if (res <= this->maxVal && res >= this->minVal)
		{
			return (T)(res);
		}
	}
}

template<typename T>
inline bdg::core::ExpDistr<T>::ExpDistr(T min, T max) :
    SmartDistr<T, std::exponential_distribution<double>>(min, max, std::exponential_distribution<double>(3.5))
{
}

/*
 * We only take values from the interval [0,1] and then compute the value
 * in the min-max interval.
 */
template<typename T>
inline T bdg::core::ExpDistr<T>::generate(std::default_random_engine &gen)
{
    while(true)
    {
        double res = this->distr(gen);
        if (res <= 1.0)
        {
            return (T)(((this->maxVal - this->minVal) * res) + this->minVal);
        }
    }
}

template<typename T>
inline bdg::core::BinomDistr<T>::BinomDistr(T min, T max) :
    SmartDistr<T, std::binomial_distribution<int>>(min, max, std::binomial_distribution<int>((max-min), 0.5))
{
}

/*
 * We only take values from the interval [0,1] and then compute the value
 * in the min-max interval.
 */
template<typename T>
inline T bdg::core::BinomDistr<T>::generate(std::default_random_engine &gen)
{
	return this->distr(gen) + this->minVal;
}

/*
 * Due to the way of setting the seed, we do not allow equal min and max values.
 */
template<typename T>
inline bdg::core::GeomDistr<T>::GeomDistr(T min, T max) :
    SmartDistr<T, std::geometric_distribution<int>>(min, max, std::geometric_distribution<int>(1.0/(max-min)))
{
	if(min == max) { throw bdg::utils::BadBoundariesException("GeomDistr: equal min and max"); }
}

/*
 * We only take values from the interval [0,1] and then compute the value
 * in the min-max interval.
 */
template<typename T>
inline T bdg::core::GeomDistr<T>::generate(std::default_random_engine &gen)
{
    while(true)
    {
        int res = this->distr(gen) + this->minVal;
		/* Beware of overflows */
        if (res <= this->maxVal && res >= this->minVal)
        {
            return (T)(res);
        }
    }
}
