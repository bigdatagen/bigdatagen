/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file doublegenerator.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include "elemgen.h" // Generator
#include "distributions.h" // DistrBase

#include <random> // gen

namespace bdg
{
	namespace core
	{
        /**
        * Generator that generates double numbers.
        */
		class DoubleGenerator :
			public ElemGen<double, DoubleGenerator>
		{
		public:
			DoubleGenerator(double minValue, double maxValue, const std::string &distrName);

            void init(HistoryStorage<double>& h);
            double generateValue();
            void generate(GenStorer& gs);

		private:
            /**
            * Random engine.
            */
			std::default_random_engine gen;

            /**
            * Distribution. 
            */
            DistrBase<double>::ptr_t distr;
		};
	}
}
