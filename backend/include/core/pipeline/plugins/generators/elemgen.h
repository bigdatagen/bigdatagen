/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file elemgen.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include "historystorage.h" // historystorage
#include "dyngenerator.h" // DynGenerator
#include "exception.h" // Exception
#include "genstorer.h" // GenStorer
#include "remotegenerator.h" // RemoteGenerator
#include "utils/convert.h" // Convert functions

#include <cassert> // assert

namespace bdg
{
	namespace core
	{
        // Forward declaration.
        class GenStorer;

		class DynSeqGenerator;
		template<typename T>
        class SequenceGenerator;

        /**
        * Base class generator for things that generates data of type T.
        * Handle storing history, remote generators.
        * Uses CRTP. Derived class only needs to implement:
        *   - void init(HistoryStorage<T>& h)
        *   - T generateValue()
        *   - void generate(GenStorer& gs)
        */
        template<typename T, class Der>
        class ElemGen:
            public DynGenerator
        {
        public:
            inline ElemGen() = default;

            inline void regData(const std::vector<T>& data);
            inline void regData(std::vector<T>&& data);
            inline void resetRemote();

            inline bool isRemote() const;

            // Has to implement.
            // void init(HistoryStorage<T>& h);
            // T generateValue();
            // void generate(GenStorer& gs);
            
        protected:
            /**
            * Handles storing history of generated values.
            */
            HistoryStorage<T> history;

            /**
            * Holds remote generating.
            */
            RemoteGenerator<T> remote;

        private:
            virtual inline void setRemoteImpl() override;

            // Inherited via DynGenerator
            inline void regHistoryImpl(std::size_t length) override;

            // Inherited via DynGenerator
            inline void regSeqRefImpl(DynSeqGenerator* dynSeqGen, const std::string& ref) override;

            // Inherited via DynGenerator
            inline void registerGeneratorImpl(
                std::size_t& currIdx,
                std::vector<GrammarElement::gen_fn_t>& preGenFncs,
                std::vector<GrammarElement::gen_fn_t>& genFncs) override;

            inline Der& baseCast();

            /**
            * Stores info whether generator is remote.
            */
            bool isRem = false;

            /**
            * todo: Not used but not sure.
            */
            size_t r = 1;
        };
    }
}

// Have to include here because of stupid dependencies.
#include "sequencegenerator.h" // SequenceGenerator

/**
* Marks generator as remote.
*/
template<typename T, class Der>
inline void bdg::core::ElemGen<T, Der>::setRemoteImpl()
{
    isRem = true;
}

/**
* Register given data to remote generator.
*/
template<typename T, class Der>
inline void bdg::core::ElemGen<T, Der>::regData(const std::vector<T>& data)
{
    remote.regData(data);
}

/**
* Register given data to remote generator.
*/
template<typename T, class Der>
inline void bdg::core::ElemGen<T, Der>::regData(std::vector<T>&& data)
{
    remote.regData(std::move(data));
}

/**
* Reset remote generator counters.
*/
template<typename T, class Der>
inline void bdg::core::ElemGen<T, Der>::resetRemote()
{
    remote.reset();
}

/**
* Tells whether generator is remote.
*/
template<typename T, class Der>
inline bool bdg::core::ElemGen<T, Der>::isRemote() const
{
    return isRem;
}

/**
* Register history.
*/
template<typename T, class Der>
inline void bdg::core::ElemGen<T, Der>::regHistoryImpl(std::size_t length)
{
    // Why?
    history.resize(length + 1);
}

/**
* Cast given pointer to this type and register data to it, we can't do it
* later because we don't know date type.
*/
template<typename T, class Der>
inline void bdg::core::ElemGen<T, Der>::regSeqRefImpl(DynSeqGenerator* dynSeqGen, const std::string& ref)
{
    bdg::core::SequenceGenerator<T>* sg = dynamic_cast<SequenceGenerator<T>*>(dynSeqGen);
    if (sg == nullptr)
        throw Exception(Error::BAD_REFERENCE_TYPE);

    sg->regDataRef(history.getData(), ref);
}

/**
* Calls preGenImpl for last adjustment before registering.
* Generates values and store them. If the history is enabled 
* than stores also results to history.
* If generator is remote, uses remote generator instead.
*/
template<typename T, class Der>
inline void bdg::core::ElemGen<T, Der>::registerGeneratorImpl(
    std::size_t& currIdx,
    std::vector<bdg::core::GrammarElement::gen_fn_t>& preGenFncs,
    std::vector<bdg::core::GrammarElement::gen_fn_t>& genFncs)
{
    // Prepare generating.
    preGenImpl(preGenFncs);
    
    const std::size_t idx = ++currIdx;
    if (history.isEnabled())
    {
        if (isRem)
        {
            genFncs.push_back([this, idx](GenStorer& gs)
            {
                baseCast().init(history);
                T val = remote.generate();
                history.store(val);
                gs.store(std::move(bdg::utils::toString<T>(val)));
                return idx;
            });
        }
        else
        {
            genFncs.push_back([this, idx](GenStorer& gs)
            {
                baseCast().init(history);
                T val = baseCast().generateValue();
                history.store(val);
                gs.store(std::move(bdg::utils::toString<T>(val)));
                return idx;
            });
        }
    }
    else
    {
        // Always has to be local.
        assert(!isRem);

        genFncs.push_back([this, idx](GenStorer& gs)
        {
            baseCast().init(history);
            baseCast().generate(gs);
            return idx;
        });
    }
}

/**
* Statically cast this ptr to base. (CRTP)
*/
template<typename T, class Der>
inline Der& bdg::core::ElemGen<T, Der>::baseCast()
{
    return static_cast<Der&>(*this);
}
