/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file terminal.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include "grammarelement.h"

#include <string> // string
#include <vector> // vector


namespace bdg
{
	namespace core
	{
        /**
        * Generator for terminal (constants) elements.
        */
		class Terminal :
			public GrammarElement
		{
		public:
			Terminal(const std::string& value);
            Terminal(std::string&& value);

        protected:
            virtual void registerGeneratorImpl(
                std::size_t& currIdx,
                std::vector<gen_fn_t> &preGenFncs,
                std::vector<gen_fn_t> &genFncs) override;

		private:
            /**
            * Constant value that terminal holds.
            */
			const std::string value;

        };
	}
}