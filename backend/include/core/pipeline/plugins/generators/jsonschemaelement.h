/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file jsonschemaelement.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include "schemaelement.h" // SchemaElement.

#include <Poco/JSON/Object.h> 

namespace bdg
{
    namespace core
    {
        /**
        * Schema element for jsons.
        */
        struct JsonSchemaElement :
            public SchemaElement
        {
        public:
            JsonSchemaElement(const Poco::JSON::Object::Ptr& obj, bool hasProps = false);

            virtual std::string getTypeName() const override;
            virtual std::string getType() const override;

            /**
            * Pointer to current json object.
            */
            Poco::JSON::Object::Ptr obj;
            
            /**
            * Flag whether we should look for properties.
            */
            bool hasProps;
        };
    }
}