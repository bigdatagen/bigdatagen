/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file stringgenerator.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include "intgenerator.h" // IntGenerator
#include "elemgen.h" // Generator

namespace bdg
{
	namespace core
	{
        /**
        * Generator of random strings.
        */
		class StringGenerator :
            public ElemGen<std::string, StringGenerator>
		{
		public:
			//StringGenerator(StringGenerator &&) = delete;
            StringGenerator(
                int minLength,
                int maxLength,
                const std::string& distrName,
                bool decorate = true);

            void init(HistoryStorage<std::string>& h);
            std::string generateValue();
            void generate(GenStorer& gs);

		private:
			static const char firstChar = 'a';
			static const char lastChar = 'z';
            static const char START_CHAR = '\"';
            static const char END_CHAR = '\"';

            std::string genValDecImpl();
            std::string genValUndecImpl();

            /**
            * Member function pointer to set whether we want to decore or not.
            */
            std::string(StringGenerator::* const genValImpl)(void);
            /**
            * Random generator of string size.
            */
			IntGenerator lengthGenerator;
            /**
            * Random generator of characters.
            */
			IntGenerator characterGenerator;
		};
	}
}
