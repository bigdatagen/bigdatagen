/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file complexgenerator.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include "nonterminal.h" // Nonterminal

#include <vector>
#include <functional> // function

namespace bdg
{
	namespace core
	{
        /** 
        * Generator of complex types with multiple inner elements
        * with given delimiter and decorators around each element
        */
		class ComplexGenerator :
			public Nonterminal
		{
		public:
            /**
            * Pair of property name and pointer to property value.
            */
            using prop_elem_t = std::pair<std::string, GrammarElement::ptr_t>;

            ComplexGenerator(
                const std::vector<prop_elem_t>& propElems,
                const std::string& startDelim,
                const std::string& endDelim,
                const std::string& delim,
                const std::function<GrammarElement::ptr_t(const prop_elem_t&)>& decorator = emptyDec);

            static GrammarElement::ptr_t emptyDec(const prop_elem_t& pe);
		};
	}
}