/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file genstorer.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include <Poco/Types.h>         // Poco::Int64

#include <string>       // std::string
#include <future>       // std::future
#include <vector>       // std::vector
#include <cstdio>       // FILE

namespace bdg
{
    namespace core
    {
        /**
        * @brief Class for storing data from generator
        */
        class GenStorer
        {
        public:
            GenStorer(std::size_t buffSize = 0);
            GenStorer(const GenStorer&) = default;
            GenStorer& operator=(const GenStorer&) = default;
            ~GenStorer();

            void open(const std::string& fName, const int rank);
            void close();
            void flushFiles();
            
            void store(char c);
            void store(std::string&& str);
            void store(const std::string& str);
            void flush();

            inline Poco::Int64 getStoredBytes() const;

        private:
            char* buff;
            std::size_t size = 0;
            Poco::Int64 storedBytes = 0;
            const std::size_t buffSize;
            FILE* file = nullptr;
            std::string fileName;
            std::string fileNameLocal;
            std::vector<std::future<int>> fileFutures;
        };
    }
}

inline Poco::Int64 bdg::core::GenStorer::getStoredBytes() const
{
    return storedBytes;
}
