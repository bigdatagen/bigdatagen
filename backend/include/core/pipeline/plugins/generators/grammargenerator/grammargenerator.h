/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Generator
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/generators/agenerator.h" // Generator
#include "core/pipeline/plugins/generators/grammarschemaparser/grammargeneratorschemaparser.h"

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            namespace plugin
            {
                /**
                 * Plugin which is able to generate files according to a given grammar.
                 */
                class GrammarGenerator : 
                    public Generator<GrammarGeneratorSchemaParser>
                {
                public:
                    GrammarGenerator();
                };
            }
        }
    }
}
