/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file cycle.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include "nonterminal.h" // Nonterminel
#include "genstorer.h" // GenStorer

#include <string> // string
#include <random> // gen
#include <memory> // shared_ptr
#include <cassert> // assert

namespace bdg
{
	namespace core
	{
        /**
        * Base class for all cycles.
        * Uses CRTP. Derivated class has to implement function: 
        *   - void initLoop()
        *   - bool isEnd()
        *   - void inc()
        */
        template<class Der>
		class Cycle :
			public Nonterminal
		{
		public:
            inline Cycle(
                const std::vector<GrammarElement::ptr_t>& elems, 
                const std::string& brStart = "[", 
                const std::string& brEnd = "]", 
                const std::string& delim = ",");

            inline Cycle(
                std::vector<GrammarElement::ptr_t>&& elems, 
                const std::string& brStart = "[", 
                const std::string& brEnd = "]", 
                const std::string& delim = ",");

            // Der has to have initialized:
            // void initLoop();
            // bool isEnd();
            // void inc();

		private:
            virtual void registerGeneratorImpl(
                std::size_t& currIdx,
                std::vector<gen_fn_t> &preGenFncs,
                std::vector<gen_fn_t> &genFncs) override;

            /**
            * Start string that is inserted at start of generation.
            */
            const std::string BR_START;

            /**
            * End string that is inserted at the end of generation.
            */
            const std::string BR_END;

            /**
            * Delimiter that is included between all elements
            */
            const std::string DELIM;

            /**
            * Starting indices of all nonterminals in global generator
            * array. i-th nonterminal is <i, i+1).
            */
            std::vector<std::size_t> nonIdcs;
		};
	}
}

template<class Der>
inline bdg::core::Cycle<Der>::Cycle(
    const std::vector<GrammarElement::ptr_t>& elems, 
    const std::string& brStart, 
    const std::string& brEnd, 
    const std::string& delim) :
Nonterminal(elems),
BR_START(brStart),
BR_END(brEnd),
DELIM(delim)
{
}

template<class Der>
inline bdg::core::Cycle<Der>::Cycle(
    std::vector<GrammarElement::ptr_t>&& elems, 
    const std::string& brStart, 
    const std::string& brEnd, 
    const std::string& delim) :
Nonterminal(elems),
BR_START(brStart),
BR_END(brEnd),
DELIM(delim)
{
}

/**
* Register generator for cycle. First position in generator array holds
* initLoop() function. Next function goes in cycle through all registred elements
* (we hold borders in nonIdcs) and calls inc() after each nonterminal
* until isEnd returns true.
*/
template<class Der>
void bdg::core::Cycle<Der>::registerGeneratorImpl(
    std::size_t& currIdx,
    std::vector<GrammarElement::gen_fn_t> &preGenFncs,
    std::vector<GrammarElement::gen_fn_t> &genFncs)
{
    assert(size() > 0);

    Der* const der = static_cast<Der*>(this);

    ++currIdx;
    // Set "header" function that prepare number loop params.
    genFncs.push_back([der, currIdx](GenStorer& gs)
    {
        der->initLoop();
        return currIdx;
    });

    ++currIdx;
    // Placeholder.
    const size_t nonStart = genFncs.size();
    genFncs.push_back([](GenStorer& gs)
    {
        (void)gs;
        assert(false);
        return static_cast<size_t>(-1);
    });

    // Indices for each nonterminal - i-th is <i, i+1).
    nonIdcs.reserve(size() + 1);
    for (size_t i = 0; i < size(); i++)
    {
        nonIdcs.push_back(currIdx);
        get(i)->registerGenerator(currIdx, preGenFncs, genFncs);
    }
    nonIdcs.push_back(currIdx);

    // Replace placeholder with array header, that does looping.
    genFncs[nonStart] = std::move([der, this, &genFncs](GenStorer& gs)
    {
        // Start brace.
        gs.store(BR_START);

        std::size_t idx = *std::begin(nonIdcs);
        // Nonterminal index (is not from 0, because of nonIdcs).
        size_t j = 1;
        const size_t endIdx = *nonIdcs.rbegin();

        // Do first iteration manually.
        if (!der->isEnd())
        {
            while (idx < nonIdcs[j])
            {
                idx = genFncs[idx](gs);
            }

            if (idx >= endIdx)
            {
                idx = *std::begin(nonIdcs);
                j = 1;
            }
            else
            {
                ++j;
            }

            // Increment.
            der->inc();
        }

        for(; !der->isEnd(); der->inc())
        {
            // Prints delimiter.
            gs.store(DELIM);

            // Generate nonterminal.
            while (idx < nonIdcs[j])
            {
                idx = genFncs[idx](gs);
            }

            // No nonterminals to generate.
            if (idx >= endIdx)
            {
                idx = *std::begin(nonIdcs);
                j = 1;
            }
            else
            {
                // Jumt to next nonterminal.
                ++j;
            }
        }

        // End brace.
        gs.store(BR_END);

        // Return index to next element after all nonterminals.
        return endIdx;
    });
}
