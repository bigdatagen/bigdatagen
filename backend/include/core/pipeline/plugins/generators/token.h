/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file token.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include "exception.h" // Exception

#include <vector> // vector
#include <string> // string
#include <memory> // ptr
#include <deque>  // dequeue
#include <stack>    // stack
#include <cassert> // assert
#include <functional>   // function
#include <algorithm>	// find_if

namespace bdg
{
    namespace core
    {
        /// @brief Function that holds temporaries and its real size.
        /// We don't want to resize vector, because we holds pointers to inner elements.
        template<typename T>
        using seqFn_t = std::function<void(std::vector<T>&)>;

        template<typename T>
        using dataRef_t = std::pair<std::string, const T*>;

        /**
        * Class that holds token of type T.
        */
        template<typename T>
        class Token
        {
        public:
            using ptr_t = std::unique_ptr<Token<T>>;

            inline void registerSeqGen(
                std::stack<const T*>& toks,
                const std::vector<dataRef_t<T>>& valRefs,
                std::vector<T>& consts,
                std::vector<T>& tmps,
                std::vector<seqFn_t<T>>& fncs);

        protected:
            virtual void regSeqGenImpl(
                std::stack<const T*>& toks,
                const std::vector<dataRef_t<T>>& valRefs,
                std::vector<T>& consts,
                std::vector<T>& tmps,
                std::vector<seqFn_t<T>>& fncs) = 0;
        };

        /**
        * Class holds operation on token.
        */
        template<typename T>
        class Operator :
            public Token<T>
        {
        public:
            inline explicit Operator(uint8_t priority);

            inline bool operator<(const Operator& rhs) const;
            inline bool operator<=(const Operator& rhs) const;
            inline bool operator==(const Operator& rhs) const;
            inline bool operator!=(const Operator& rhs) const;

        private:
            virtual inline void regSeqGenImpl(
                std::stack<const T*>& toks,
                const std::vector<dataRef_t<T>>& valRefs,
                std::vector<T>& consts,
                std::vector<T>& tmps,
                std::vector<seqFn_t<T>>& fncs) override;

            const uint8_t priority;
        };

        /**
        * Binary operator
        */
        template<typename T, class Der>
        class BinOp :
            public Operator<T>
        {
        public:
            inline explicit BinOp(uint8_t priority);

            // Derived classes needs to implement:
            // T eval(const T* p1, const T* p2) const;

        private:
            virtual inline void regSeqGenImpl(
                std::stack<const T*>& toks,
                const std::vector<dataRef_t<T>>& valRefs,
                std::vector<T>& consts,
                std::vector<T>& tmps,
                std::vector<seqFn_t<T>>& fncs) override;
        };

        /**
        * Unary operator.
        */
        template<typename T, class Der>
        class UnOp :
            public Operator<T>
        {
        public:
            inline explicit UnOp(uint8_t priority);

            // Derived classes needs to implement:
            // T eval(const T* p) const;

        private:
            virtual inline void regSeqGenImpl(
                std::stack<const T*>& toks,
                const std::vector<dataRef_t<T>>& valRefs,
                std::vector<T>& consts,
                std::vector<T>& tmps,
                std::vector<seqFn_t<T>>& fncs) override;
        };

        template<typename T>
        class Plus :
            public BinOp<T, Plus<T>>
        {
        public:
            inline Plus();

            inline T eval(const T* p1, const T* p2) const;
        };

        template<typename T>
        class Minus :
            public BinOp<T, Minus<T>>
        {
        public:
            inline Minus();

            inline T eval(const T* p1, const T* p2) const;
        };

        template<>
        class Minus<std::string> :
            public BinOp<std::string, Minus<std::string>>
        {
        public:
            Minus();

            std::string eval(const std::string* p1, const std::string* p2) const;

        private:
            void regSeqGenImpl(
                std::stack<const std::string*>& toks,
                const std::vector<dataRef_t<std::string>>& valRefs,
                std::vector<std::string>& consts,
                std::vector<std::string>& tmps,
                std::vector<seqFn_t<std::string>>& fncs) override;
        };

        /// @brief Left bracket.
        /// @note Priority has to be the highest number, so it could add elements on it.
        template<typename T>
        class LeftBracket :
            public Operator<T>
        {
        public:
            inline LeftBracket();

        private:
            inline void regSeqGenImpl(
                std::stack<const T*>& toks,
                const std::vector<dataRef_t<T>>& valRefs,
                std::vector<T>& consts,
                std::vector<T>& tmps,
                std::vector<seqFn_t<T>>& fncs) override;
        };

        /// @brief Right bracket.
        /// @note Priority has to be the lowest number, so it pop elements pushed on stack.
        template<typename T>
        class RightBracket :
            public Operator<T>
        {
        public:
            inline RightBracket();

        private:
            inline void regSeqGenImpl(
                std::stack<const T*>& toks,
                const std::vector<dataRef_t<T>>& valRefs,
                std::vector<T>& consts,
                std::vector<T>& tmps,
                std::vector<seqFn_t<T>>& fncs) override;
        };

        template<typename T>
        class Mul :
            public BinOp<T, Mul<T>>
        {
        public:
            Mul();

            inline T eval(const T* p1, const T* p2) const;
        };

        template<>
        class Mul<std::string> :
            public BinOp<std::string, Mul<std::string>>
        {
        public:
            Mul();

            std::string eval(const std::string* p1, const std::string* p2) const;

        private:
            void regSeqGenImpl(
                std::stack<const std::string*>& toks,
                const std::vector<dataRef_t<std::string>>& valRefs,
                std::vector<std::string>& consts,
                std::vector<std::string>& tmps,
                std::vector<seqFn_t<std::string>>& fncs) override;
        };

        template<typename T>
        class Div :
            public BinOp<T, Div<T>>
        {
        public:
            inline Div();

            inline T eval(const T* p1, const T* p2) const;
        };

        template<>
        class Div<std::string> :
            public BinOp<std::string, Div<std::string>>
        {
        public:
            Div();

            std::string eval(const std::string* p1, const std::string* p2) const;

        private:
            void regSeqGenImpl(
                std::stack<const std::string*>& toks,
                const std::vector<dataRef_t<std::string>>& valRefs,
                std::vector<std::string>& consts,
                std::vector<std::string>& tmps,
                std::vector<seqFn_t<std::string>>& fncs) override;
        };

        template<typename T>
        class Mod :
            public BinOp<T, Mod<T>>
        {
        public:
            inline Mod();

            inline T eval(const T* p1, const T* p2) const;
        };

        template<>
        class Mod<std::string> :
            public BinOp<std::string, Mod<std::string>>
        {
        public:
            Mod();

            std::string eval(const std::string* p1, const std::string* p2) const;

        private:
            void regSeqGenImpl(
                std::stack<const std::string*>& toks,
                const std::vector<dataRef_t<std::string>>& valRefs,
                std::vector<std::string>& consts,
                std::vector<std::string>& tmps,
                std::vector<seqFn_t<std::string>>& fncs) override;
        };

        template<>
        class Mod<double> :
            public BinOp<double, Mul<double>>
        {
        public:
            Mod();

            double eval(const double* p1, const double* p2) const;

        private:
            void regSeqGenImpl(
                std::stack<const double*>& toks,
                const std::vector<dataRef_t<double>>& valRefs,
                std::vector<double>& consts,
                std::vector<double>& tmps,
                std::vector<seqFn_t<double>>& fncs) override;
        };

        template<typename T>
        class Const :
            public Token<T>
        {
        public:
            explicit inline Const(T value);

        private:
            inline void regSeqGenImpl(
                std::stack<const T*>& toks,
                const std::vector<dataRef_t<T>>& valRefs,
                std::vector<T>& consts,
                std::vector<T>& tmps,
                std::vector<seqFn_t<T>>& fncs) override;

            const T value;
        };

        template<typename T>
        class Var :
            public Token<T>
        {
        public:
            explicit inline Var(const std::string& name, int offset = 0);

            inline void regSeqGenImpl(
                std::stack<const T*>& toks,
                const std::vector<dataRef_t<T>>& valRefs,
                std::vector<T>& consts,
                std::vector<T>& tmps,
                std::vector<seqFn_t<T>>& fncs) override;

        private:
            const std::string name;
            const int off;
        };
}
}

// Token

template<typename T>
inline void bdg::core::Token<T>::registerSeqGen(
    std::stack<const T*>& toks,
    const std::vector<bdg::core::dataRef_t<T>>& valRefs,
    std::vector<T>& consts, 
    std::vector<T>& tmps,
    std::vector<bdg::core::seqFn_t<T>>& fncs)
{
    regSeqGenImpl(toks, valRefs, consts, tmps, fncs);
}

// Operator

template<typename T>
bdg::core::Operator<T>::Operator(uint8_t priority) :
    priority(priority)
{
}

template<typename T>
bool bdg::core::Operator<T>::operator<(const Operator& rhs) const
{
    return priority < rhs.priority;
}

template<typename T>
bool bdg::core::Operator<T>::operator<=(const Operator& rhs) const
{
    return priority <= rhs.priority;
}

template<typename T>
bool bdg::core::Operator<T>::operator==(const Operator& rhs) const
{
    return priority == rhs.priority;
}

template<typename T>
bool bdg::core::Operator<T>::operator!=(const Operator& rhs) const
{
    return !this->operator==(rhs);
}

template<typename T>
inline void bdg::core::Operator<T>::regSeqGenImpl(
    std::stack<const T*>& toks,
    const std::vector<dataRef_t<T>>& valRefs, 
    std::vector<T>& consts, 
    std::vector<T>& tmps, 
    std::vector<seqFn_t<T>>& fncs)
{
    throw Exception(Error::UNSUPPORTED_OPERATION + "unknown");
}

// BINARY OPERATOR

template<typename T, class Der>
bdg::core::BinOp<T, Der>::BinOp(uint8_t priority) :
    Operator<T>(priority)
{
}

template<typename T, class Der>
inline void bdg::core::BinOp<T, Der>::regSeqGenImpl(
    std::stack<const T*>& toks,
    const std::vector<dataRef_t<T>>& valRefs,
    std::vector<T>& consts,
    std::vector<T>& tmps,
    std::vector<seqFn_t<T>>& fncs)
{
    // Insert dummy to temporaries.
    const size_t tmpIdx = tmps.size();
    tmps.emplace_back();

    if (toks.size() < 2)
        throw Exception(Error::INVALID_EXPR);

    // On stack's top is right operand.
    const T* p2 = toks.top();
    toks.pop();
    const T* p1 = toks.top();
    toks.pop();
    // Add to stack address to result.
    toks.push(&tmps[tmpIdx]);

    const Der* thisPtr = static_cast<const Der*>(this);
    fncs.push_back([p1, p2, thisPtr, tmpIdx](std::vector<T>& t)
    {
        t[tmpIdx] = thisPtr->eval(p1, p2);
    });
}

// UNARY OPERATOR

template<typename T, class Der>
bdg::core::UnOp<T, Der>::UnOp(uint8_t priority) :
    Operator<T>(priority)
{
}

template<typename T, class Der>
inline void bdg::core::UnOp<T, Der>::regSeqGenImpl(
    std::stack<const T*>& toks,
    const std::vector<dataRef_t<T>>& valRefs,
    std::vector<T>& consts,
    std::vector<T>& tmps,
    std::vector<seqFn_t<T>>& fncs)
{
    // Insert dummy to temporaries.
    const size_t tmpIdx = tmps.size();
    tmps.emplace_back();
    this->val = &tmps[tmpIdx];

    if (toks.empty())
        throw Exception(Error::INVALID_EXPR);

    const T* p = toks.top();
    toks.pop();
    // Push address to result on stack.
    toks.push(&tmps[tmpIdx]);

    const Der* thisPtr = static_cast<const Der*>(this);
    fncs.push_back([p, tmpIdx, thisPtr](std::vector<T>& t)
    {
        t[tmpIdx] = thisPtr->eval(p);
    });
}

// PLUS

template<typename T>
bdg::core::Plus<T>::Plus() :
    BinOp<T, Plus<T>>(6)
{
}

template<typename T>
inline T bdg::core::Plus<T>::eval(const T * p1, const T * p2) const
{
    return (*p1) + (*p2);
}

// MINUS

template<typename T>
bdg::core::Minus<T>::Minus() :
    BinOp<T, Minus<T>>(6)
{
}

template<typename T>
inline T bdg::core::Minus<T>::eval(const T * p1, const T * p2) const
{
    return (*p1) - (*p2);
}

// LEFT BRACKET

template<typename T>
inline bdg::core::LeftBracket<T>::LeftBracket():
    Operator<T>(10)
{
}

template<typename T>
inline void bdg::core::LeftBracket<T>::regSeqGenImpl(
    std::stack<const T*>& toks,
    const std::vector<dataRef_t<T>>& valRefs,
    std::vector<T>& consts,
    std::vector<T>& tmps,
    std::vector<seqFn_t<T>>& fncs)
{
    throw Exception(Error::INVALID_EXPR);
}

// RIGHT BRACKET

template<typename T>
inline bdg::core::RightBracket<T>::RightBracket() :
    Operator<T>(1)
{
}

template<typename T>
inline void bdg::core::RightBracket<T>::regSeqGenImpl(
    std::stack<const T*>& toks,
    const std::vector<dataRef_t<T>>& valRefs,
    std::vector<T>& consts,
    std::vector<T>& tmps,
    std::vector<seqFn_t<T>>& fncs)
{
    // Do nothing.
}

// MUL

template<typename T>
bdg::core::Mul<T>::Mul() :
    BinOp<T, Mul<T>>(5)
{
}

template<typename T>
inline T bdg::core::Mul<T>::eval(const T* p1, const T* p2) const
{
    return (*p1) * (*p2);
}

// DIV

template<typename T>
bdg::core::Div<T>::Div() :
    BinOp<T, Div<T>>(5)
{
}

template<typename T>
inline T bdg::core::Div<T>::eval(const T * p1, const T * p2) const
{
    return (*p1) / (*p2);
}

// MOD

template<typename T>
bdg::core::Mod<T>::Mod() :
    BinOp<T, Mod<T>>(5)
{
}

template<typename T>
inline T bdg::core::Mod<T>::eval(const T * p1, const T * p2) const
{
    return (*p1) % (*p2);
}

// CONST

template<typename T>
inline bdg::core::Const<T>::Const(T value) :
    value(value)
{
}

template<typename T>
inline void bdg::core::Const<T>::regSeqGenImpl(
    std::stack<const T*>& toks,
    const std::vector<dataRef_t<T>>& valRefs,
    std::vector<T>& consts,
    std::vector<T>& tmps,
    std::vector<seqFn_t<T>>& fncs)
{
    // Store value to consts.
    consts.push_back(value);
    // Push constant's address on stack.
    toks.push(&*std::rbegin(consts));
}


// VAR

template<typename T>
bdg::core::Var<T>::Var(const std::string &name, int offset) :
    name(name), off(offset)
{
}

template<typename T>
inline void bdg::core::Var<T>::regSeqGenImpl(
    std::stack<const T*>& toks,
    const std::vector<dataRef_t<T>>& valRefs,
    std::vector<T>& consts,
    std::vector<T>& tmps,
    std::vector<seqFn_t<T>>& fncs)
{
    auto drIt = std::find_if(valRefs.cbegin(), valRefs.cend(), [this](const dataRef_t<T>& dr)
    {
        return dr.first == name;
    });

    assert(drIt != std::cend(valRefs));
    // Store address to variable on stack.
    toks.push(&drIt->second[off]);
}

