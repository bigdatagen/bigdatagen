/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Generator
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/bdgapi.h"                       // Plugin API
#include "core/pipeline/plugins/generators/generator.h"        // Generator

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            namespace plugin
            {
                /**
                 * @brief Abstract generator covering all common parts.
                 */
                template<typename SP>
                class Generator : public GeneratorPlugin
                {
                public:
                    Generator(const std::string &name, const std::string &description, const std::string &extension);

                    virtual void taskBeforePassToWorker(bdg::core::pipeline::Task *task) override;
                    
                protected:
                    virtual void init() override;
                    virtual void taskStart(const Task* task) override;
                    virtual Poco::Int64 generateOneFile(const bdg::core::pipeline::GeneratorTask *task, const Poco::Path &outputFilePath) override;
                    virtual void taskEnd(const bdg::core::pipeline::Task *task, const bdg::core::pipeline::TaskResult *result) override;
                    virtual void terminate() override;

                private:
                    /**
                    * @brief Generator.
                    */
                    bdg::core::Generator gen;

                    /**
                    * @brief Schema parser
                    */
                    SP schemaParser;
                };
            }
        }
    }
}


template<typename SP>
bdg::core::pipeline::plugin::Generator<SP>::Generator(const std::string &name, const std::string &description, const std::string &extension) :
    GeneratorPlugin(name, description, extension),
    schemaParser(gen)
{
    // Never do anything here!
}

/**
* @brief Function prepares task for sending to worker.
*
*   Function prepares task for worker and sets sequence's start, remote generators values
* and number of files.
*
* @param task Task that is adjusted.
*/
template<typename SP>
void bdg::core::pipeline::plugin::Generator<SP>::taskBeforePassToWorker(bdg::core::pipeline::Task* task)
{
    GeneratorPlugin::taskBeforePassToWorker(task);

    GeneratorTask& gt = *dynamic_cast<GeneratorTask*>(task);
    gen.setFilesCount(gt.getFilesCount());
    gt.setRemInts(gen.genRemInts());
    gt.setSeqsStart(gen.genSeqsStart());
}

/**
* @brief Initialize generator and parse schema.
*
*/
template<typename SP>
void bdg::core::pipeline::plugin::Generator<SP>::init()
{
    GeneratorPlugin::init();

    gen.setRank(getMpi().getRank());
    schemaParser.parse(getSchema());
}

/**
* @brief Parse task and sets all received values.
*/
template<typename SP>
void bdg::core::pipeline::plugin::Generator<SP>::taskStart(const Task* task)
{
    const GeneratorTask& gt = *dynamic_cast<const GeneratorTask*>(task);
    gen.regRemInts(gt.getRemInts());
    gen.setSeqsStart(gt.getSeqsStart());
}


/**
* @brief Reset remote generators.
*
* @param task
* @param result
*/
template<typename SP>
void bdg::core::pipeline::plugin::Generator<SP>::taskEnd(const bdg::core::pipeline::Task *task, const bdg::core::pipeline::TaskResult *result)
{
    gen.resetRemote();
    GeneratorPlugin::taskEnd(task, result);

    // Close all the asynchronously closed files if we need them in the next stage of pipeline.
    if (getPluginsCountInPipeline() > 1)
    {
        gen.flushStorageFiles();
    }
}

/**
* Terminates generator plugin.
*/
template<typename SP>
void bdg::core::pipeline::plugin::Generator<SP>::terminate()
{
    GeneratorPlugin::terminate();

    if (!getMpi().isMaster())
    {
        gen.flushStorageFiles();
    }
}

/**
* Generates data to given file.
*
* @param task
* @param outputFilePath
*
* @return
*/
template<typename SP>
Poco::Int64 bdg::core::pipeline::plugin::Generator<SP>::generateOneFile(
    const bdg::core::pipeline::GeneratorTask *task,
    const Poco::Path &outputFilePath)
{
    gen.openStorage(outputFilePath.toString());
    gen.generate();
    gen.closeStorage();

    bdg_debug(getLogger(), "Generated JSON file [" + outputFilePath.toString() + "]; "
        + "ID: " + getFileId(task->getPluginIndex(), getMpi().getRank(), nextFileIdLocal) + ", "
        + "Size: " + std::to_string(gen.getStorer().getStoredBytes()) + " bytes");

    return gen.getStorer().getStoredBytes();
}