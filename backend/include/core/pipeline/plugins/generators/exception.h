/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file exception.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include <string>
#include <stdexcept>

namespace bdg
{
    namespace core
    {
        /**
        * Holds all exceptions errors.
        */
        namespace Error
        {
            const std::string MISSING_PROPERTY = "Missing property: ";
            const std::string BAD_PROPERTY_TYPE = "Bad type of property: ";
            const std::string BAD_PROPERTY_VALUE = "Bad value of property: ";
            const std::string BAD_PROPERTIES = "Bad properties: ";
            const std::string BAD_BOUNDARIES = "Bad boundaries: ";
            const std::string BAD_TYPE = "Bad type: ";
            const std::string INVALID_JSON = "Invalid json: ";
            const std::string ROOT_NOT_ARRAY = "Schema is not array.";
            const std::string NOT_OBJECT = "Type is not object: ";
            const std::string NOT_ARRAY = "Type is not array: ";
            const std::string TYPE_REDEFINITION = "Multiple types with same name defined: ";
            const std::string TYPE_NOT_EXISTS = "Type doesn't exist: ";
            const std::string MISSING_ROOT = "Missing ROOT element.";
            const std::string UNKNOWN_REFERENCE = "Unknown reference to ";
            const std::string BAD_REFERENCE = "Referencing bad type with reference: ";
            const std::string BAD_REFERENCE_TYPE = "Referencing different type from sequnce type in an sequence.";
            const std::string INVALID_PROB_SUM = "The probabilities do not sum up to 1.";
            const std::string TS_INVALID_STEP = "Invalid step format in timesequence.";
            const std::string TS_INVALID_TIME = "Invalid time format in timesequence.";
            const std::string TS_INVALID_DATE = "Invalid date format in timesequence.";
            const std::string UNSUPPORTED_OPERATION = "Unsupported operation on: ";
            const std::string UNSUPPORTED_DISTRIBUTION = "Distribution not supported.";
            const std::string INVALID_EXPR = "Invalid sequence expression.";
            const std::string INVALID_EXPR_CHAR = "Invalid character in sequnce expression: ";
            const std::string EMPTY_PROPERTIES = "Empty properties.";
            const std::string BAD_DTIME = "Bad date or time: ";
            const std::string EMPTY_DOM_VALS = "Empty domain values.";
            const std::string RECURSIVE_TYPE = "Recursion is prohibited. Recursive type found: ";
        };

        class Exception :
            public std::runtime_error
        {
        public:
            Exception(const std::string &msg);
            Exception(const std::string &msg, const std::runtime_error &innerException);
        };
    }
}
