/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file schemaelement.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include <memory> // unique_ptr, shared_ptr.
#include <string>

namespace bdg
{
    namespace core
    {
        /// @brief Base class for all schemas elements.
        struct SchemaElement
        {
        public:
            using uptr_t = std::unique_ptr<SchemaElement>;
            using ptr_t = std::shared_ptr<SchemaElement>;

            SchemaElement() = default;
            virtual ~SchemaElement() = default;

            virtual std::string getTypeName() const = 0;
            virtual std::string getType() const = 0;
        };
    }
}