/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file historystorage.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include <vector> // vector
#include <algorithm>

namespace bdg
{
	namespace core
	{
        /**
        * Storage for storing historic generated values.
        */
        template<typename T>
		class HistoryStorage
		{
		public:
            inline HistoryStorage(std::size_t length = 0);

            inline bool isEnabled() const;

            inline void resize(std::size_t length);

            inline std::size_t size() const;

            inline T get(int offset) const;
            inline const T* getData() const;
            inline void store(const T& val);
            inline void store(T&& val);

        private:
            inline void rotateData();

            /**
            * Storage for data.
            */
            std::vector<T> data;
		};
	}
}

template<typename T>
inline bdg::core::HistoryStorage<T>::HistoryStorage(std::size_t length) :
    data(length)
{
}

/**
* Returns true if its enabled.
*/
template<typename T>
inline bool bdg::core::HistoryStorage<T>::isEnabled() const
{
    return !data.empty();
}

/**
*  Resize storage to be able contain length elements.
*/
template<typename T>
inline void bdg::core::HistoryStorage<T>::resize(std::size_t length)
{
    if (length > data.size())
    {
        data.resize(length);
    }
}

/**
* Returns possible number of stored elements.
*/
template<typename T>
inline std::size_t bdg::core::HistoryStorage<T>::size() const
{
    return data.size();
}

/**
* Returns data for given offset.
*
* @param offset Offset is negative number.
*/
template<typename T>
inline T bdg::core::HistoryStorage<T>::get(int offset) const
{
    return data[-offset];
}

/**
* Returns pointer to data.
*/
template<typename T>
inline const T* bdg::core::HistoryStorage<T>::getData() const
{
    return &data[0];
}

/**
* Stores new value to storage.
*/
template<typename T>
inline void bdg::core::HistoryStorage<T>::store(const T& val)
{
    rotateData();
    data[0] = val;
}

/**
* Stores new value into storage.
*/
template<typename T>
inline void bdg::core::HistoryStorage<T>::store(T&& val)
{
    rotateData();
    data[0] = std::move(val);
}

/**
* Shift all data to the right.
*/
template<typename T>
inline void bdg::core::HistoryStorage<T>::rotateData()
{
    std::rotate(std::begin(data), std::end(data) - 1, std::end(data));
}
