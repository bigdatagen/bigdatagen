/** 
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file typefactory.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include "grammarelement.h" // GrammarElement.
#include "schemaelement.h"

#include <functional> // Function.
#include <vector>
#include <string>

namespace bdg
{
    namespace core
    {
        // Forward declarations.
        class SchemaParser;

        /**
        * Base class of all factory types.
        */
        class TypeFactory
        {
        public:
            //typedef GrammarElement::ptr_t(TypeFactory::*factory_fnc_t)(const SchemaElement::uptr_t&, SchemaParser&);
            using factory_fnc_t = std::function<GrammarElement::ptr_t(TypeFactory&, const SchemaElement::uptr_t&, SchemaParser&)>;
            using name_factory_fnc_t = std::pair<std::string, factory_fnc_t>;

            template<class TF, class... Args>
            static inline TypeFactory& instance(Args&&... args);

            const factory_fnc_t& create(const std::string& type) const;

        protected:
            TypeFactory(const factory_fnc_t& notFoundFnc, std::vector<name_factory_fnc_t>&& init);

        private:
            const factory_fnc_t notFoundFnc;

            /// @brief Suported types and its factory methods. 
            /// @note Using vector instead map-> types count < 10, vector should be faster.
            const std::vector<name_factory_fnc_t> supTypes;

            static const std::string NO_SUPP_TYPES;
            static const std::string TYPE_NOT_EXIST;
        };
        
    }
}

template<class TF, class... Args>
inline bdg::core::TypeFactory& bdg::core::TypeFactory::instance(Args&&... args)
{
    static TF tf(std::forward<Args>(args)...);
    return tf;
}
