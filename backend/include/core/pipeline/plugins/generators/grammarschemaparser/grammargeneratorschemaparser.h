#pragma once

/**
 *
 * @file grammarschemaparser.h
 *
 */

#include "grammarschemaparser.h"

namespace bdg
{
	namespace core
	{
		class GrammarGeneratorSchemaParser :
            public GrammarSchemaParser
		{
		public:
            GrammarGeneratorSchemaParser(Generator& gen);

        };
	}
}