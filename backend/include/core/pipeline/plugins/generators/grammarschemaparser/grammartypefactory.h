#pragma once

/**
*
* @file grammartypefactory.h
*
*/

#include "../jsontypefactory.h"

#include <Poco/JSON/Object.h>

#include <functional> // Function.
#include <vector>
#include <string>

namespace bdg
{
    namespace core
    {
        class GrammarTypeFactory:
            public JsonTypeFactory
        {
        public:
            static GrammarElement::ptr_t createGrammar(
                TypeFactory& tf,
                const SchemaElement::uptr_t& se,
                SchemaParser& parser);

            static GrammarElement::ptr_t createComplex(
                TypeFactory& tf,
                const SchemaElement::uptr_t& se,
                SchemaParser& parser);
            
            static GrammarElement::ptr_t createCycle(
                TypeFactory& tf,
                const SchemaElement::uptr_t& se,
                SchemaParser& parser);

            static GrammarElement::ptr_t createAlternation(
                TypeFactory& tf,
                const SchemaElement::uptr_t& se,
                SchemaParser& parser);

            static GrammarElement::ptr_t createString(
                TypeFactory& tf,
                const SchemaElement::uptr_t& se,
                SchemaParser& parser);

            static GrammarElement::ptr_t createWhiteSpace(
                TypeFactory& tf,
                const SchemaElement::uptr_t& se,
                SchemaParser& parser);

            static GrammarElement::ptr_t createEmpty(
                TypeFactory& tf,
                const SchemaElement::uptr_t& se,
                SchemaParser& parser);

			GrammarTypeFactory(const factory_fnc_t& nonFoundFnc, std::vector<name_factory_fnc_t>&& init);

        private:
            static GrammarElement::ptr_t createGrammarImpl(
                SchemaParser& parser,
                TypeFactory& tf,
                Poco::JSON::Object::Ptr const& jsonObject,
                GrammarElement::element_map_t &parsedElements,
                std::vector<DynSeqGenerator*>& seqs);

            static GrammarElement::ptr_t createComplexImpl(
                SchemaParser& parser,
                TypeFactory& tf,
                Poco::JSON::Object::Ptr const& jsonObject,
                GrammarElement::element_map_t &parsedElements,
                std::vector<DynSeqGenerator*>& seqs);

            static GrammarElement::ptr_t createCycleImpl(
                SchemaParser& parser,
                TypeFactory& tf,
                Poco::JSON::Object::Ptr const& jsonObject,
                GrammarElement::element_map_t &parsedElements,
                std::vector<DynSeqGenerator*>& seqs);

            static GrammarElement::ptr_t createAlternationImpl(
                SchemaParser& parser,
                TypeFactory& tf,
                Poco::JSON::Object::Ptr const& jsonObject,
                GrammarElement::element_map_t &parsedElements,
                std::vector<DynSeqGenerator*>& seqs);

            static GrammarElement::ptr_t createStringImpl(
                SchemaParser& parser,
                TypeFactory& tf,
                Poco::JSON::Object::Ptr const& jsonObject,
                GrammarElement::element_map_t &parsedElements,
                std::vector<DynSeqGenerator*>& seqs);

            static GrammarElement::ptr_t createWsImpl(
                SchemaParser& parser,
                TypeFactory& tf,
                Poco::JSON::Object::Ptr const& jsonObject,
                GrammarElement::element_map_t &parsedElements,
                std::vector<DynSeqGenerator*>& seqs);

            static GrammarElement::ptr_t createEmptyImpl(
                SchemaParser& parser,
                TypeFactory& tf,
                Poco::JSON::Object::Ptr const& jsonObject,
                GrammarElement::element_map_t &parsedElements,
                std::vector<DynSeqGenerator*>& seqs);
        };
    }
}
