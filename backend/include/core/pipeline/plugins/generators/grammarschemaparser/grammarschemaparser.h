#pragma once

/**
 *
 * @file parser.h
 *
 */

#include "../schemaparser.h" // SchemaParser
#include "../elementfactory.h" // ElementFactory

#include <string> // string
#include <memory> // unique_ptr
#include <unordered_map> // unordered_(multi)map
#include <iterator> // iterator
#include <functional> // function

#include <Poco/JSON/Object.h>
#include <Poco/Dynamic/Var.h> // Var



namespace bdg
{
	namespace core
	{
		class GrammarSchemaParser:
            public SchemaParser
		{
        public:
            GrammarSchemaParser(
                Generator& gen, 
                const TypeFactory::factory_fnc_t& createUnknown, 
                std::vector<TypeFactory::name_factory_fnc_t>&& factoryMethods);

        private:
            
            // Inherited via SchemaParser
            virtual void parseSchema(const std::string & schema) override;
            virtual bool hasNextUdt() const override;
            virtual SchemaElement::uptr_t getNextUdt() override;

            Poco::Dynamic::Var parsedSchema;
            Poco::JSON::Array::Ptr rootArray;
            std::size_t nextIdx;
        };
	}
}