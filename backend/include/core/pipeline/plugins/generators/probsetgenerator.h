/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file probsetgenerator.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include "nonterminal.h" // NonTerminal
#include "doublegenerator.h" // DoubleGenerator

#include <vector> // vector
#include <string> // string
#include <random> // gen
#include <memory> // shared_ptr

#include <Poco/Dynamic/Var.h> // Var

namespace bdg
{
	namespace core
	{
		// Fwd declaration
		class DynSeqGenerator;
        
        /**
        * Generator for probability sets.
        */
		class ProbSetGenerator :
			public Nonterminal
		{
		public:
            ProbSetGenerator(const std::vector<GrammarElement::ptr_t>& els, const std::vector<int>& probs);
            ProbSetGenerator(std::vector<GrammarElement::ptr_t>&& els, std::vector<int>&& probs);

			virtual ~ProbSetGenerator() = default;

		private:
			virtual void registerGeneratorImpl(
				std::size_t& idx,
                std::vector<gen_fn_t> &preGenFncs,
  	            std::vector<gen_fn_t> &genFncs) override;

            virtual void repairImpl(
                const GrammarElement::element_map_t& parsedEls,
                const GrammarElement::seq_vector_t& seqs,
                GrammarElement::intGenTypeFnc_vec_t& locGens,
                GrammarElement::intGenTypeFnc_vec_t& remGens,
                std::vector<std::vector<GrammarElement::genIdxTypeFnc_t>>& remGensGens,
                std::vector<GrammarElement::GlobSeq>& globSeqs,
                std::vector<const GrammarElement*>& pathEls) override;

            static std::vector<int> transformProbs(const std::vector<int>& probs);

            /**
            * Indices of generated function of each nonterminal in global array.
            */
            std::vector<size_t> nonIdcs;

            /**
            * Probabilites for each probability set.
            */
			std::vector<int> probs;
            
            /**
            * Generator that chooses branch.
            */
            IntGenerator chooser;

            /// @brief Generated probability.
            int prob;
		};
	}
}
