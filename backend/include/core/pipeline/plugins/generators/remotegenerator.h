/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file remotegenerator.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include <vector>

namespace bdg
{
	namespace core
	{
        /**
        * Used for generating remote datas that are remotely 
        * pregenerated od master.
        */
        template<typename T>
		class RemoteGenerator
		{
		public:
            inline T generate();

            inline void regData(const std::vector<T>& data);
            inline void regData(std::vector<T>&& data);
            inline void reset();
          
        private:
            /**
            * Real data.
            */
            std::vector<T> d;

            /**
            * Index of next generated dato.
            */
            std::size_t i = 0;
		};
	}
}

/**
* Return next value.
*/
template<typename T>
inline T bdg::core::RemoteGenerator<T>::generate()
{
    return d[i++];
}

/**
* We register all datas that we should need.
*/
template<typename T>
inline void bdg::core::RemoteGenerator<T>::regData(const std::vector<T>& data)
{
    d = data;
}

/**
* Registers all data that we should need.
*/
template<typename T>
inline void bdg::core::RemoteGenerator<T>::regData(std::vector<T>&& data)
{
    d = std::move(data);
}

/**
* Reset index counter.
*/
template<typename T>
inline void bdg::core::RemoteGenerator<T>::reset()
{
    i = 0;
}
