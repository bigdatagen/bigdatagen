/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file jsongeneratorschemaparser.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include "jsonschemaparser.h"

namespace bdg
{
	namespace core
	{
		// Forward declaration.
		class Generator;

        /**
        * Schema parser for json generator.
        */
		class JsonGeneratorSchemaParser:
            public JsonSchemaParser
		{
		public:
            JsonGeneratorSchemaParser(Generator& gen);

		};
	}
}