/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file jsonschemaparser.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include "../schemaparser.h" // SchemaParser
#include "../typefactory.h" 

#include <Poco/JSON/Object.h>
#include <Poco/Dynamic/Var.h> // Var

#include <string> // string

namespace bdg
{
	namespace core
	{
		// Forward declaration.
		class Generator;

        /**
        * Schema parser for json schemas.
        */
		class JsonSchemaParser:
            public SchemaParser
		{
		public:
			JsonSchemaParser(
                Generator& gen, 
                const TypeFactory::factory_fnc_t& createUnknown, 
                std::vector<TypeFactory::name_factory_fnc_t>&& factoryMethods);

		private:
            virtual void parseSchema(const std::string& schema) override;

            virtual bool hasNextUdt() const override;

            virtual SchemaElement::uptr_t getNextUdt() override;

            /**
            * Stored ptr to first element in parsed schema.
            */
            Poco::Dynamic::Var parsedSchema;
            /**
            * Ptr to root array.
            */
            Poco::JSON::Array::Ptr rootArray;
            /**
            * Index of next UDT to get from root array.
            */
            std::size_t nextIdx;
		};
	}
}