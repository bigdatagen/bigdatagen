/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file jsontypefactory.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include "typefactory.h" // TypeFactory, SchemaElement.
#include "jsonschemaelement.h"
#include "sequencegenerator.h"
#include "schemaparser.h"

#include <Poco/JSON/Object.h>

#include <functional> // Function.
#include <vector>
#include <string>

namespace bdg
{
    namespace core
    {
        /**
        * Factory methods for all types used in json generator parser.  
        */
        class JsonTypeFactory:
            public TypeFactory
        {
        public:
            template<typename InnerT>
            static inline GrammarElement::ptr_t createSimpleType(
                TypeFactory& tf,
                const SchemaElement::uptr_t& se,
                SchemaParser& parser);

            static GrammarElement::ptr_t createDateTime(
                TypeFactory& tf,
                const SchemaElement::uptr_t& se,
                SchemaParser& parser);

            static GrammarElement::ptr_t createObject(
                TypeFactory& tf,
                const SchemaElement::uptr_t& se,
                SchemaParser& parser);

            static GrammarElement::ptr_t createArray(
                TypeFactory& tf,
                const SchemaElement::uptr_t& se,
                SchemaParser& parser);

            static GrammarElement::ptr_t createProbSet(
                TypeFactory& tf,
                const SchemaElement::uptr_t& se,
                SchemaParser& parser);

            static GrammarElement::ptr_t createSequence(
                TypeFactory& tf,
                const SchemaElement::uptr_t& se,
                SchemaParser& parser);

            static GrammarElement::ptr_t createReference(
                TypeFactory& tf,
                const SchemaElement::uptr_t& se,
                SchemaParser& parser);

            static GrammarElement::ptr_t createUnknown(
                TypeFactory& tf,
                const SchemaElement::uptr_t& se,
                SchemaParser& parser);

            static GrammarElement::ptr_t prototype(
                TypeFactory& tf,
                const SchemaElement::uptr_t& se,
                SchemaParser& parser);

            JsonTypeFactory(const factory_fnc_t& nonFoundFnc, std::vector<name_factory_fnc_t>&& init);

        private:
            template<typename InnerT>
            static inline GrammarElement::ptr_t createSimpleTypeImpl(
                const Poco::JSON::Object::Ptr& jsonObject);

            static inline GrammarElement::ptr_t createDateTimeImpl(
                const Poco::JSON::Object::Ptr& jsonObject);

            static GrammarElement::ptr_t createObjectImpl(
                SchemaParser& parser,
                TypeFactory& tf,
                Poco::JSON::Object::Ptr const& jsonObject,
                GrammarElement::element_map_t &parsedElements,
                std::vector<DynSeqGenerator*>& seqs,
                const std::string &startDelim,
                const std::string &endDelim);

            static GrammarElement::ptr_t createArrayImpl(
                SchemaParser& parser,
                TypeFactory& tf,
                Poco::JSON::Object::Ptr const& jsonObject,
                GrammarElement::element_map_t &parsedElements,
                std::vector<DynSeqGenerator*>& seqs);

            static GrammarElement::ptr_t createProbSetImpl(
                SchemaParser& parser,
                TypeFactory& tf,
                Poco::JSON::Object::Ptr const& jsonObject,
                bool hasProps,
                GrammarElement::element_map_t& parsedElements,
                std::vector<DynSeqGenerator*>& seqs);

            static GrammarElement::ptr_t createSequenceTypeImpl(
                const Poco::JSON::Object::Ptr& jsonObject,
                std::vector<DynSeqGenerator*>& seqs);

            template<typename T>
            static inline SequenceGenerator<T>* createSeq(
                const Poco::JSON::Object::Ptr& propObj,
                std::vector<DynSeqGenerator*>& seqs,
                const std::string& value,
                bool isLocal);

            static GrammarElement::ptr_t createDateTimeSeq(
                const Poco::JSON::Object::Ptr& propObj

            );

            static GrammarElement::ptr_t createUnknownImpl(
                const Poco::JSON::Object::Ptr& propObj,
                SchemaParser& parser);
        };
    }
}

template<typename InnerT>
inline bdg::core::GrammarElement::ptr_t bdg::core::JsonTypeFactory::createSimpleType(
    TypeFactory& tf,
    const SchemaElement::uptr_t& se,
    SchemaParser& parser)
{
    (void)parser;
    (void)tf;
    JsonSchemaElement* jse = dynamic_cast<JsonSchemaElement*>(se.get());
    return createSimpleTypeImpl<InnerT>(jse->obj);
}

/**
* Factory methods for single types - string, int, floats.
* Can create random and domain generators.
*/
template<typename InnerT>
bdg::core::GrammarElement::ptr_t bdg::core::JsonTypeFactory::createSimpleTypeImpl(
    const Poco::JSON::Object::Ptr& jsonObject)
{
    const auto vprops = JsonUtils::getPropertyObject(jsonObject, Constants::ITEM_PROPERTIES);
    const std::string distr = JsonUtils::getPropertyValue<std::string>(vprops, Constants::PROPERTY_DISTRIBUTION);
    const std::string source = JsonUtils::getPropertyValue<std::string>(vprops, Constants::PROPERTY_SOURCE);

    if (source == Constants::PROPERTY_DOMAIN)
    {
        const auto arrayPtr = JsonUtils::getPropertyArray(vprops, Constants::PROPERTY_DOMAIN);
        return GrammarElement::makeGe<DomainGenerator<InnerT>>(JsonUtils::getArraySimpleValues<InnerT>(arrayPtr), distr);
    }
    else if (source == Constants::PROPERTY_RANDOM)
    {
        using LimitT = typename GenInfo<InnerT>::limit_t;

        return ElementFactory<InnerT>::create(
            JsonUtils::getPropertyValue<LimitT>(vprops, Constants::PROPERTY_MIN),
            JsonUtils::getPropertyValue<LimitT>(vprops, Constants::PROPERTY_MAX),
			distr);
    }
    else
    {
        throw Exception(Error::BAD_PROPERTY_VALUE + source);
    }
}

/**
* Creates sequence of type T.
*/
template<typename T>
inline bdg::core::SequenceGenerator<T>* bdg::core::JsonTypeFactory::createSeq(
    const Poco::JSON::Object::Ptr& propObj,
    std::vector<DynSeqGenerator*>& seqs,
    const std::string& value,
    bool isLocal)
{
    auto initValArr = JsonUtils::getArraySimpleValues<T>(JsonUtils::getPropertyArray(propObj, Constants::PROPERTY_INITIAL_VALUE));
    SequenceGenerator<T>* seq = new SequenceGenerator<T>(initValArr, value, isLocal);
    seqs.push_back(dynamic_cast<DynSeqGenerator*>(seq));
    return seq;
}
