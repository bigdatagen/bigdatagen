/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file dyngenerator.cpp
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include "nonterminal.h" // Nonterminal

namespace bdg
{
	namespace core
	{
        /**
        * Forward declaration.
        */
        class DynSeqGenerator;

        /**
        * Base class for generators.
        */
		class DynGenerator :
			public Nonterminal
		{
		public:
            DynGenerator() = default;

            inline void regHistory(int length);

            inline void regSeqRef(DynSeqGenerator* dynSeqGen, const std::string& ref);

            inline void setRemote();

        protected:
            virtual void regHistoryImpl(std::size_t length) = 0;

            virtual void regSeqRefImpl(DynSeqGenerator* dynSeqGen, const std::string& ref) = 0;

            virtual void setRemoteImpl() = 0;

            virtual void preGenImpl(std::vector<GrammarElement::gen_fn_t>& preGenFncs);
		};
    }
}

/**
* Calls implementation.
*/
inline void bdg::core::DynGenerator::regHistory(int length)
{
    regHistoryImpl(static_cast<std::size_t>(length));
}

/**
* Calls implementation
*/
inline void bdg::core::DynGenerator::regSeqRef(DynSeqGenerator* dynSeqGen, const std::string& ref)
{
    regSeqRefImpl(dynSeqGen, ref);
}

/**
* Calls implementation.
*/
inline void bdg::core::DynGenerator::setRemote()
{
    setRemoteImpl();
}

inline void bdg::core::DynGenerator::preGenImpl(std::vector<GrammarElement::gen_fn_t>& preGenFncs)
{
    // Do nothing by default.
}
