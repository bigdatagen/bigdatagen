#pragma once

/**
*
* @file objectgenerator.h
*
*/

#include "../nonterminal.h" // Nonterminal
#include "../terminal.h" // Terminal

#include <Poco/Dynamic/Var.h> // Var.
#include <Poco/JSON/Array.h> // Array

namespace bdg
{
	namespace core
	{
        // Forward declaration.
        class DynSeqGenerator;

		template<char StartChar, char EndChar>
		class ObjectGenerator :
			public Nonterminal
		{
		public:
			ObjectGenerator(
				const Poco::Dynamic::Var &properties,
				GrammarElement::element_map_t &parsedElements,
				GrammarElement::element_vector_t &unresolvedElements,
                std::vector<DynSeqGenerator*>& seqs,
                std::vector<DynSeqGenerator*>& globSeqs);

        private:
            static const char DELIM = ',';

            inline void registerGeneratorImpl(
                std::vector<GrammarElement::gen_fn_t>& preGenFncs,
                std::vector<GrammarElement::gen_fn_t>& genFncs) override;

            static inline std::shared_ptr<GrammarElement> decorate(std::shared_ptr<GrammarElement>& ge, const std::string &propertyName);
		};
	}
}

/* This has to be included AFTER the class declaration, otherwise g++ will report compilation error "incomplete type used in nested name specifier" - Dusan */
#include "jsonparser.h" // JsonParser

template<char StartChar, char EndChar>
inline bdg::core::ObjectGenerator<StartChar, EndChar>::ObjectGenerator(
	const Poco::Dynamic::Var &properties,
	GrammarElement::element_map_t &parsedElements,
	GrammarElement::element_vector_t &unresolvedElements,
    std::vector<DynSeqGenerator*>& seqs,
    std::vector<DynSeqGenerator*>& globSeqs)
{
	Poco::JSON::Array::Ptr arr = JsonParser::getArray(properties);
	//std::for_each(arr->begin(), arr->end(), [&](const Poco::Dynamic::Var &obj)
    for (size_t i = 0; i < arr->size(); i++)
    {
        if (arr->isObject(i))
        {
            push_back(JsonParser::createElement<JsonParser::ElementType::NORMAL>(
                JsonParser::getObject(arr->get(i)),
                parsedElements,
                unresolvedElements,
                ObjectGenerator::decorate,
                seqs,
                globSeqs));
        }
        else
        {
            push_back(JsonParser::createElement(
                JsonParser::getArraySimpleValue<std::string>(arr, i),
                parsedElements,
                unresolvedElements));
        }
    }
}

template<char StartChar, char EndChar>
inline void bdg::core::ObjectGenerator<StartChar, EndChar>::registerGeneratorImpl(
    std::vector<bdg::core::GrammarElement::gen_fn_t> &preGenFncs,
    std::vector<bdg::core::GrammarElement::gen_fn_t> &genFncs)
{
	genFncs.push_back([](GenStorer& gs)
	{ 
        gs.store({ StartChar });
	});

    using innerGen_t = std::remove_reference<decltype(genFncs)>::type;
    std::vector<innerGen_t> innerGens(Nonterminal::size());
    for (size_t i = 0; i < innerGens.size(); i++)
    {
        Nonterminal::get(i)->registerGenerator(preGenFncs, innerGens[i]);
    }

    genFncs.push_back([this, innerGens](GenStorer& gs)
    {
        for (size_t i = 0; i < innerGens.size() - 1; i++)
        {
            for (auto& gfn : innerGens[i])
            {
                gfn(gs);
            }
            gs.store({ DELIM });
        }

        // Do last loop manualy.
        for (auto& gfn : *innerGens.rbegin())
        {
            gfn(gs);
        }
        gs.store({ EndChar });
    });
}

template<char StartChar, char EndChar>
inline std::shared_ptr<bdg::core::GrammarElement> bdg::core::ObjectGenerator<StartChar, EndChar>::decorate(
    std::shared_ptr<bdg::core::GrammarElement>& ge,
    std::string const& propertyName)
{
    bdg::core::Nonterminal *nonTerminal = new bdg::core::Nonterminal();
    nonTerminal->push_back(std::make_shared<Terminal>("\"" + propertyName + "\"" + ":"));
    nonTerminal->push_back(ge);
    //nonTerminal->push_back(std::make_shared<Terminal>(","));

    return std::shared_ptr<bdg::core::GrammarElement>(nonTerminal);
}
