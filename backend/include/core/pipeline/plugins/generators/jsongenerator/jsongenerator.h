/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file jsongenerator.h
* @category Generator
* @author   Big Data Generator Team
*
*/

#pragma once

#include "core/pipeline/plugins/bdgapi.h"                                                   // Plugin API
#include "core/pipeline/plugins/generators/generator.h"                                     // Generator
#include "core/pipeline/plugins/generators/jsonschemaparser/jsongeneratorschemaparser.h"    // JsonGeneratorSchemaParser
#include "core/pipeline/plugins/generators/agenerator.h"                                    // Generator

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            namespace plugin
            {
                /**
                 * Plugin which is able to generate JSON files according to a given schema.
                 */
                class JsonGenerator : public Generator<JsonGeneratorSchemaParser>
                {
                public:
                    JsonGenerator();

                    virtual inline PluginPostprocessor getPostprocessor() const override;
                };

                /**
                 * Post-processing plugin for the JSON generator plugin. This plugin was originally
                 * intended to be used for post-processing of generated sequences, but in the end
                 * it was not implemented and it is not used.
                 */
                class JsonGeneratorPostprocessor : public TransformerPlugin
                {
                public:
                    JsonGeneratorPostprocessor();

                protected:
                    virtual void processOneFile(const bdg::core::pipeline::TransformerTask *task, std::unique_ptr<FileTransfererStream> inputFileStream, const Poco::Path &inputFilePath, const Poco::Path &outputFilePath, size_t &newlyCreatedFilesCount) override;
                };
            }
        }
    }
}

/**
 * Returns post-processing plugin. No plugin is returned, because it was not implemented.
 *
 * @return Post-processing plugin.
 */
inline bdg::core::pipeline::plugin::PluginPostprocessor bdg::core::pipeline::plugin::JsonGenerator::getPostprocessor() const
{
    // Some decission making about need of the postprocessor should be done here. If we do not
    // need it, we should return PluginPostprocessor::none(), thus we avoid needless data move
    // between nodes and scheduling overhead.
    if (false)
    {
        return PluginPostprocessor::createUniqueOrdered(new JsonGeneratorPostprocessor);
    }
    else
    {
        return PluginPostprocessor::none();
    }
}
