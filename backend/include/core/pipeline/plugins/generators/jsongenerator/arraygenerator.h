#pragma once

/**
*
* @file arraygenerator.h
*
*/

#include "nonterminal.h" // Nonterminel
#include "intgenerator.h" // IntGenerator

#include <string> // string
#include <random> // gen
#include <memory> // shared_ptr

#include <Poco/Dynamic/Var.h> // Var.

namespace bdg
{
	namespace core
	{
        // Fwd declaration.
        class DynSeqGenerator;

		class ArrayGenerator :
			public Nonterminal
		{
		public:
			ArrayGenerator(
				const Poco::Dynamic::Var &properties,
				GrammarElement::element_map_t &parsedElements,
				GrammarElement::element_vector_t &unresolvedElements,
                std::vector<DynSeqGenerator*>& seqs,
                std::vector<DynSeqGenerator*>& globSeqs);

		private:
            static const char BR_START = '[';
            static const char BR_END = ']';
            static const char DELIM = ',';

            virtual void registerGeneratorImpl(
                std::vector<gen_fn_t> &preGenFncs,
                std::vector<gen_fn_t> &genFncs) override;

            virtual void repairImpl(
                const GrammarElement::element_map_t& parsedEls,
                const GrammarElement::seq_vector_t& locSeqs,
                GrammarElement::intGen_vector_t& locGens,
                GrammarElement::intGen_vector_t& remGens,
                std::vector<std::size_t>& remGensGen,
                std::vector<GrammarElement::GlobSeq>& globSeqs) override;

            static std::shared_ptr<GrammarElement> decorate(std::shared_ptr<GrammarElement>& ge, std::string const& propertyName);

            IntGenerator sizeGen;
		};
	}
}