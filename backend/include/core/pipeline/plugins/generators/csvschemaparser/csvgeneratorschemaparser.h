#pragma once

/**
 *
 * @file jsongeneratorschemaparser.h
 *
 */

#include "../csvschemaparser/csvschemaparser.h"

namespace bdg
{
	namespace core
	{
		// Forward declaration.
		class Generator;

		/*
		 * We inherit from JsonSchemaParser because the schemas
		 * for the CsvGenerator are almost similar.
		 */
		class CsvGeneratorSchemaParser :
            public CsvSchemaParser
		{
		public:
            CsvGeneratorSchemaParser(Generator& gen);

		};
	}
}
