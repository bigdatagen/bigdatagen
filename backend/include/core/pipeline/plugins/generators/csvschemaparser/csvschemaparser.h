#pragma once

/**
 *
 * @file jsonschemaparser.h
 *
 */

#include "../schemaparser.h" // SchemaParser
#include "../typefactory.h" 

#include <Poco/JSON/Object.h>
#include <Poco/Dynamic/Var.h> // Var

#include <string> // string

namespace bdg
{
	namespace core
	{
		// Forward declaration.
		class Generator;

		class CsvSchemaParser:
            public SchemaParser
		{
		public:
			CsvSchemaParser(
                Generator& gen, 
                const TypeFactory::factory_fnc_t& createUnknown, 
                std::vector<TypeFactory::name_factory_fnc_t>&& factoryMethods);

		private:
            virtual void parseSchema(const std::string& schema) override;

            virtual bool hasNextUdt() const override;

            virtual SchemaElement::uptr_t getNextUdt() override;

            Poco::Dynamic::Var parsedSchema;
            Poco::JSON::Array::Ptr rootArray;
            std::size_t nextIdx;
		};
	}
}
