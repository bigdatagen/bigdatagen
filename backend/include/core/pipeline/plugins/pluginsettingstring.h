/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/pluginsetting.h"        // PluginSetting

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            namespace plugin
            {
                /**
                 * String setting of pipeline plugin. On frontend it is rendered as a text input.
                 */
                class BDG_PLUGIN_API PluginSettingString : public PluginSetting
                {
                public:
                    PluginSettingString(const std::string &name, const std::string &description = "", const std::string &defaultValue = "", const std::string &help = "", const bool isAdvancedSetting = false);
                    virtual ~PluginSettingString() = default;

                    virtual inline SettingType getType() const override;
                    virtual PluginSetting *clone() const override;
                };
            }
        }
    }
}

/**
 * Returns the type of the setting.
 *
 * @return Type of the setting.
 */
inline bdg::core::pipeline::plugin::PluginSetting::SettingType bdg::core::pipeline::plugin::PluginSettingString::getType() const
{
    return SettingType::SETTING_TYPE_STRING;
}
