/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/taskresult.h"       // TaskResult

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Class reprezenting task result when a task has failed.
             */
            class BDG_PLUGIN_API ErrorTaskResult : public TaskResult
            {
            public:
                ErrorTaskResult() = default;
                explicit ErrorTaskResult(const std::string &msg);
                explicit ErrorTaskResult(const std::exception &ex);
                explicit ErrorTaskResult(const Poco::Exception &ex);
                virtual ~ErrorTaskResult() = default;

                virtual inline ResultType getType() const override;

                inline std::string getMessage() const;

            protected:
                virtual void serializeAdditional(Poco::JSON::Object &json) const override;
                virtual void unserializeAdditional(const Poco::JSON::Object &json) override;

            private:
                /**
                 * Error message stored in the task result.
                 */
                std::string msg;
            };
        }
    }
}

/**
 * Returns type of the task result.
 *
 * @return Type of the task result.
 */
inline bdg::core::pipeline::TaskResult::ResultType bdg::core::pipeline::ErrorTaskResult::getType() const
{
    return ResultType::RESULT_TYPE_ERROR;
}

/**
 * Returns the error message stored in the task result.
 *
 * @return Error message.
 */
inline std::string bdg::core::pipeline::ErrorTaskResult::getMessage() const
{
    return msg;
}
