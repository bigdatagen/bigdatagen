/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/common.h"               // Plugin API common
#include "core/pipeline/plugins/filefrequency.h"        // FileFrequency
#include "utils/mpiproxy.h"                             // MPIProxy

#include <Poco/Timestamp.h>         // Poco::Timestamp

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Limiter of the storing speed which is instancied on worker ranks. It
             * works only in connection with storer plugins. It divides the file storing
             * into batches, each batch of files is processed within a certain amount of time.
             * The batch size is determined by the storing speed passed in the constructor.
             */
            class BDG_PLUGIN_API SpeedLimiter
            {
            public:
                SpeedLimiter(const FileFrequency &fileFrequency, const int targetFilesCount);
                ~SpeedLimiter() = default;

                inline bool canProcessFile() const;
                
                void fileProcessed(const bdg::utils::MPIProxy &mpi, const Poco::UInt64 bytes, const bool moreFilesToProcess);
                void waitForNextBatch();
                void waitTillPeriodEnd() const;

            private:
                /**
                 * How many files are processed in each batch.
                 */
                Poco::Int64 filesPerBatch;

                /**
                 * How many bytes are processed in each batch.
                 */
                Poco::Int64 bytesPerBatch;

                /**
                 * How much time is reserved for each batch.
                 */
                Poco::Int64 timePerBatch;

                /**
                 * When did we start to process current batch.
                 */
                Poco::Timestamp currentBatchTime;

                /**
                 * When we sent the last message about how many files and bytes were stored.
                 */
                Poco::Timestamp currentBytesPeriodTime;

                /**
                 * How many files have been stored in the current batch.
                 */
                size_t storedFilesInBatch = 0;

                /**
                 * What the size of files stored in the current batch is.
                 */
                Poco::UInt64 storedBytesInBatch = 0;

                /**
                 * How many files have beed stored from the last message sent to the master.
                 */
                size_t newlyStoredFiles = 0;

                /**
                 * How many bytes have beed stored from the last message sent to the master.
                 */
                Poco::UInt64 newlyStoredBytes = 0;

                /**
                 * Number of microsecond that have to pass between two messages sent to the master.
                 */
                static const size_t BYTES_STORED_MESSAGE_PERIOD;
            };
        }
    }
}

/**
 * Returns whether we can process another file in the current batch.
 *
 * @return Can we process another file?
 */
inline bool bdg::core::pipeline::SpeedLimiter::canProcessFile() const
{
    return storedFilesInBatch < filesPerBatch || storedBytesInBatch < bytesPerBatch;
}
