/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/transformertask.h"          // TransformerTask
#include "core/pipeline/plugins/storertaskmetadata.h"       // StorerTaskMetadata

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Task for a storer plugin.
             */
            class BDG_PLUGIN_API StorerTask : public TransformerTask
            {
            public:
                StorerTask() = default;
                explicit StorerTask(const StorerTaskMetadata *metadata);
                virtual ~StorerTask() = default;

                virtual inline TaskType getType() const override;
                inline void setFrequency(const FileFrequency &fileFrequency);
                inline const FileFrequency &getFrequency() const;
                
            protected:
                virtual void serializeAdditional(Poco::JSON::Object &json) const override;
                virtual void unserializeAdditional(const Poco::JSON::Object &json) override;

            private:
                /**
                 * Speed which should be used for storing of generated data.
                 */
                FileFrequency fileFrequency;
            };
        }
    }
}

/**
 * Returns type of the task.
 *
 * @return Type of the task.
 */
inline bdg::core::pipeline::Task::TaskType bdg::core::pipeline::StorerTask::getType() const
{
    return TaskType::TASK_TYPE_STORER;
}

/**
 * Sets the storing speed.
 *
 * @param fileFrequency Speed which should be used for storing of generated data.
 */
inline void bdg::core::pipeline::StorerTask::setFrequency(const FileFrequency &fileFrequency)
{
    this->fileFrequency = fileFrequency;
}

/**
 * Returns the storing speed.
 *
 * @return Speed which should be used for storing of generated data.
 */
inline const bdg::core::pipeline::FileFrequency &bdg::core::pipeline::StorerTask::getFrequency() const
{
    return fileFrequency;
}
