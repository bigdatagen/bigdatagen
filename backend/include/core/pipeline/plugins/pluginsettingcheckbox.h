/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/pluginsetting.h"        // PluginSetting

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            namespace plugin
            {
                /**
                 * String setting of pipeline plugin. On frontend it is rendered as a checkbox.
                 */
                class BDG_PLUGIN_API PluginSettingCheckbox : public PluginSetting
                {
                public:
                    PluginSettingCheckbox(const std::string &name, const std::string &description = "", const bool defaultValue = false, const std::string &help = "", const bool isAdvancedSetting = false);
                    virtual ~PluginSettingCheckbox() = default;

                    inline bool getDefaultValueBool() const;
                    inline bool getValueBool() const;
                    inline void setValue(const bool value);

                    virtual inline SettingType getType() const override;
                    virtual PluginSetting *clone() const override;
                    virtual std::string validate(const std::string &value) const override;

                    virtual void serialize(Poco::JSON::Object &json) const override;
                };
            }
        }
    }
}

/**
 * Returns the setting default value as a boolean.
 *
 * @return Setting value.
 */
inline bool bdg::core::pipeline::plugin::PluginSettingCheckbox::getDefaultValueBool() const
{
    return getDefaultValue() == "true" || getDefaultValue() == "1" ? true : false;
}

/**
 * Returns the setting value as a boolean.
 *
 * @return Setting value.
 */
inline bool bdg::core::pipeline::plugin::PluginSettingCheckbox::getValueBool() const
{
    return getValue() == "true" || getValue() == "1" ? true : false;
}

/**
 * Sets the setting value.
 *
 * @param value Setting value.
 */
inline void bdg::core::pipeline::plugin::PluginSettingCheckbox::setValue(const bool value)
{
    PluginSetting::setValue(std::to_string(value));
}

/**
 * Returns the type of the setting.
 *
 * @return Type of the setting.
 */
inline bdg::core::pipeline::plugin::PluginSetting::SettingType bdg::core::pipeline::plugin::PluginSettingCheckbox::getType() const
{
    return SettingType::SETTING_TYPE_CHECKBOX;
}
