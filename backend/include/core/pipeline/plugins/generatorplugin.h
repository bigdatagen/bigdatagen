/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/plugin.h"                   // Plugin
#include "core/pipeline/plugins/generatortaskmetadata.h"    // GeneratorTaskMetadata
#include "core/pipeline/plugins/generatortask.h"            // GeneratorTask

#include <Poco/Path.h>              // Poco::Path

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            namespace plugin
            {
                /**
                 * Generator plugins are meant to be used as the first plugins in the pipeline.
                 * They generate files according to a given schema.
                 */
                class BDG_PLUGIN_API GeneratorPlugin : public Plugin
                {
                public:
                    GeneratorPlugin(const std::string &name, const std::string &description, const std::string &extension);
                    virtual ~GeneratorPlugin() = default;

                    virtual inline PluginType getType() const override;
                    virtual bdg::core::pipeline::Task *taskFactory() override;
                    virtual bdg::core::pipeline::Task *createTask(const bdg::core::pipeline::TaskMetadata *metadata) override;

                    /**
                     * Name of the setting which contains schema.
                     */
                    static const std::string SCHEMA_SETTING;

                protected:
                    virtual bdg::core::pipeline::TaskResult *processTaskImpl(const bdg::core::pipeline::Task *task) override;
                    virtual Poco::Int64 generateOneFile(const bdg::core::pipeline::GeneratorTask *task, const Poco::Path &outputFilePath) = 0;

                    bdg::core::pipeline::GeneratorTaskMetadata *convertMetadata(const bdg::core::pipeline::TaskMetadata *metadata) const;
                    bdg::core::pipeline::GeneratorTask *convertTask(const bdg::core::pipeline::Task *task) const;
                    std::string getSchema() const;
                };
            }
        }
    }
}

/**
 * Returns type of the plugin.
 *
 * @return Type of the plugin.
 */
inline bdg::core::pipeline::plugin::Plugin::PluginType bdg::core::pipeline::plugin::GeneratorPlugin::getType() const
{
    return PluginType::PLUGIN_TYPE_GENERATOR;
}
