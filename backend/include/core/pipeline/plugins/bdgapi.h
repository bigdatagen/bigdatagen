/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/generatorplugin.h"          // GeneratorPlugin
#include "core/pipeline/plugins/transformerplugin.h"        // TransformerPlugin
#include "core/pipeline/plugins/storerplugin.h"             // StorerPlugin

#include "core/pipeline/plugins/pluginsettingstring.h"      // PluginSettingString
#include "core/pipeline/plugins/pluginsettingint.h"         // PluginSettingInt
#include "core/pipeline/plugins/pluginsettingcheckbox.h"    // PluginSettingCheckbox
#include "core/pipeline/plugins/pluginsettingselect.h"      // PluginSettingSelect
#include "core/pipeline/plugins/pluginsettingarray.h"       // PluginSettingArray
#include "core/pipeline/plugins/pluginsettinggroup.h"       // PluginSettingGroup

#include <Poco/File.h>                  // Poco::File
#include <Poco/Path.h>                  // Poco::Path
#include <Poco/StreamCopier.h>          // Poco::StreamCopier
#include <Poco/String.h>                // Poco::String
#include <Poco/StringTokenizer.h>       // Poco::StringTokenizer
#include <Poco/ClassLibrary.h>          // POCO Export class

#include <iostream>         // std::cout
#include <fstream>          // std::fstream
#include <sstream>          // std::stringstream
