/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/common.h"                   // Plugin API common
#include "core/pipeline/plugins/filetransfererstream.h"     // FileTransfererStream

#include <memory>           // std::unique_ptr

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            // Forward declaration.
            class FileTransferer;

            /**
             * Iterator for reading files.
             */
            class BDG_PLUGIN_API FileTransfererStreamIterator
            {
            public:
                FileTransfererStreamIterator(const FileTransferer *fileTransferer, const std::string &workerFileMask, const int workerRank, const Poco::UInt64 fileFrom, const size_t filesCount);
                ~FileTransfererStreamIterator() = default;

                inline bool canMoveToNextFile() const;
                inline bool isLast() const;

                bool moveToNextFile();

                std::string getCurrentPath() const;
                std::unique_ptr<FileTransfererStream> getCurrentFile() const;

            private:
                /**
                 * File transferer.
                 */
                const FileTransferer *fileTransferer;

                /**
                 * Mask of the path to read files.
                 */
                const std::string workerFileMask;

                /**
                 * Rank of the worker from which we want to read files.
                 */
                const int workerRank;

                /**
                 * Id of the first file that should be read.
                 */
                const Poco::UInt64 fileFrom;

                /**
                 * Total number of read files.
                 */
                const size_t filesCount;

                /**
                 * Id of the next file that should be returned by the iterator.
                 */
                Poco::UInt64 currentFile;
            };
        }
    }
}

/**
 * Returns whether the iterator can move to the next file.
 *
 * @return Can the iterator move to the next file?
 */
inline bool bdg::core::pipeline::FileTransfererStreamIterator::canMoveToNextFile() const
{
    return currentFile < fileFrom + filesCount - 1;
}

/**
 * Returns whether we read the last file in the iterator.
 *
 * @return Did we read the last file in the iterator?
 */
inline bool bdg::core::pipeline::FileTransfererStreamIterator::isLast() const
{
    return !canMoveToNextFile();
}
