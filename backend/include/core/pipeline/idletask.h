/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/task.h"         // Task

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Task indicating that there is nothing to do, i.e. no task should
             * be processed on a worker.
             */
            class IdleTask : public Task
            {
            public:
                IdleTask() = default;

                virtual inline TaskType getType() const override;

            private:
            };
        }
    }
}

/**
 * Returns type of the task.
 *
 * @return Type of the task.
 */
inline bdg::core::pipeline::Task::TaskType bdg::core::pipeline::IdleTask::getType() const
{
    return TaskType::TASK_TYPE_IDLE;
}
