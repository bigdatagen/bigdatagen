/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @category Pipeline
* @author   Big Data Generator Team
*/

#pragma once

#include "core/pipeline/multiqueue.h"                       // MultiQueue
#include "core/pipeline/plugins/errortaskresult.h"          // ErrorTaskResult
#include "core/pipeline/plugins/successtaskresult.h"        // SuccessTaskResult
#include "utils/mpiproxy.h"                                 // MpiProxy

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Queue with task results. Each task result is poped from the queue in order
             * to define a new task based on that one. Queue can be used as a priority queue,
             * then task results are ordered in the queue according to the first id of the file
             * they processed.
             */
            class ResultQueue : public MultiQueue<TaskResult>
            {
            public:
                ResultQueue(const bdg::utils::MPIProxy &mpi, const bdg::core::pipeline::SchedulerInfo &schedulerInfo, const size_t workersCount, const size_t pluginsCount, const std::vector<bool> &useAsPriorityQueue);
                virtual ~ResultQueue() = default;

                virtual void push(std::unique_ptr<TaskResult> &result) override;
                virtual std::unique_ptr<TaskResult> pop(const int requiredPluginIndex = -1, const int workerRank = -1) override;
                std::unique_ptr<SuccessTaskResult> popSuccessResult(const int requiredPluginIndex = -1, const int workerRank = -1);

                virtual void serialize(Poco::JSON::Object &json) const override;
                virtual void unserialize(const Poco::JSON::Object &json) override;

            protected:
                virtual TaskResult *createEntry(const Poco::JSON::Object::Ptr entryJson) const override;

            private:
                std::unique_ptr<TaskResult> getResultFromQueueBack(const int pluginIndex);
                std::unique_ptr<TaskResult> getResultFromQueueFront(const int pluginIndex, const int workerRank);

                bool canPopFromFront(const int pluginIndex, const int workerRank);
                void removeErrorsFromBeginningOfQueue(const int pluginIndex);

                /**
                 * MPI wrapper.
                 */
                const bdg::utils::MPIProxy &mpi;

                /**
                 * The file id that should appear in the beginning of each queue. When there is
                 * a higher id in the beginning of the queue than the required one, we have to
                 * wait until the task result with required id appears in the beginning of the
                 * queue. This mechanism is used to implement the preservation of files order
                 * when the "preserveOrder" setting in the job configuration file is turned on.
                 */
                std::vector<Poco::UInt64> requiredFileFromIds;
            };
        }
    }
}
