/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/schedulerinfo.h"            // SchedulerInfo

#include <Poco/JSON/Array.h>        // Poco::JSON::Array
#include <Poco/JSON/Object.h>       // Poco::JSON::Object

#include <algorithm>    // std::min, std::max
#include <memory>       // std::unique_ptr
#include <queue>        // std::queue
#include <vector>       // std::vector

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * This is a base class for the task queue and task result queue. Each stage of
             * the pipeline has one queue inside of this multi queue. I.e. tasks for each
             * stage of the pipeline are stored separately and task results from each stage
             * of the pipeline are stored separately. Queue can be used as a priority queue
             * on each of its stages (none, some of them or all). When the stage is marked
             * as a priority queue, tasks or task results are stored ordered according to
             * the first file the have processed.
             */
            template<class T>
            class MultiQueue
            {
            public:
                MultiQueue(const bdg::core::pipeline::SchedulerInfo &schedulerInfo, const size_t workersCount, const size_t pluginsCount, const std::vector<bool> &useAsPriorityQueue);
                virtual ~MultiQueue() = default;

                size_t size() const;
                bool empty() const;
                bool full(const int requiredPluginIndex) const;

                virtual void push(std::unique_ptr<T> &entry);
                virtual std::unique_ptr<T> pop(const int workerRank = -1, const int requiredPluginIndex = -1) = 0;

                virtual void serialize(Poco::JSON::Object &json) const;
                virtual void unserialize(const Poco::JSON::Object &json);

            protected:
                virtual T *createEntry(const Poco::JSON::Object::Ptr entryJson) const = 0;

                typedef std::deque<std::unique_ptr<T>> Queue;
                typedef std::vector<Queue> QueuesCollection;

                /**
                 * Instance of configuration of the scheduler.
                 */
                const bdg::core::pipeline::SchedulerInfo &schedulerInfo;

                /**
                 * Vector which indicates whether to use each stage of the queue as
                 * a priority queue.
                 */
                const std::vector<bool> useAsPriorityQueue;

                /**
                 * Maximum number of entries stored in the queue.
                 */
                const size_t MAX_QUEUE_SIZE;

                /**
                 * Queues used to store entries for each stage of the pipeline.
                 */
                QueuesCollection queues;
            };
        }
    }
}

/**
 * Constructor of the queue.
 *
 * @param schedulerInfo Instance of configuration of the scheduler.
 * @param workersCount Numer of available workers.
 * @param pluginsCount Size of the pipeline - number of used pipeline plugins.
 * @param useAsPriorityQueue Should we use the queue as the priority queue?
 */
template<class T>
bdg::core::pipeline::MultiQueue<T>::MultiQueue(const bdg::core::pipeline::SchedulerInfo &schedulerInfo, const size_t workersCount, const size_t pluginsCount, const std::vector<bool> &useAsPriorityQueue) :
    schedulerInfo(schedulerInfo),
    useAsPriorityQueue(useAsPriorityQueue),
    MAX_QUEUE_SIZE(std::max<size_t>(4, std::min<size_t>(workersCount, 128))),
    queues(pluginsCount)
{
    // Nothing here.
}

/**
 * Returns total number of entries stored in the queue - on all the stages together.
 *
 * @return Total number of entries stored in the queue.
 */
template<class T>
size_t bdg::core::pipeline::MultiQueue<T>::size() const
{
    size_t size = 0;

    for (typename QueuesCollection::const_iterator it = queues.begin(); it != queues.end(); ++it)
    {
        size += it->size();
    }

    return size;
}

/**
 * Returns true when there are no entries in the queue.
 *
 * @return Is the queue empty?
 */
template<class T>
bool bdg::core::pipeline::MultiQueue<T>::empty() const
{
    return size() == 0;
}

/**
 * Returns whether the inner queue for the passed stage is full, i.e. no more entries can
 * be stored there.
 *
 * @param requiredPluginIndex Index of the plugin - specifies the inner queue we ask about.
 *
 * @return Is the queue full?
 */
template<class T>
bool bdg::core::pipeline::MultiQueue<T>::full(const int requiredPluginIndex) const
{
    if (requiredPluginIndex >= 0 && requiredPluginIndex < queues.size())
    {
        return queues[requiredPluginIndex].size() >= MAX_QUEUE_SIZE;
    }
    else
    {
        return true;
    }
}

/**
 * Pushes passed entry to the end of the queue.
 *
 * @param entry Entry added to the queue.
 */
template<class T>
void bdg::core::pipeline::MultiQueue<T>::push(std::unique_ptr<T> &entry)
{
    int pluginIndex = entry->getPluginIndex();
    Queue &queue = queues[pluginIndex];

    if (!useAsPriorityQueue[pluginIndex])
    {
        queue.push_back(std::move(entry));
    }
    else
    {
        // We want to have the queue sorted by fileFrom property of inserted entry. This is
        // a O(n) algorithm, if we used priority queue, it could be O(log(n)), there is
        // a space for improvement. We loop from the end, because there is a higher probability
        // to find the first lower element than if we looped from the beginning and we searched
        // for the first higher element in the queue.
        typename Queue::reverse_iterator it = queue.rbegin();

        while (it != queue.rend() && it->get()->getFileFrom() > entry->getFileFrom())
        {
            ++it;
        }

        queue.insert(it.base(), std::move(entry));
    }
}

/**
 * Serializes the queue.
 *
 * @param json JSON where the queue is going to be serialized.
 */
template<class T>
void bdg::core::pipeline::MultiQueue<T>::serialize(Poco::JSON::Object &json) const
{
    Poco::JSON::Array entriesJson;

    for (int i = 0; i < queues.size(); ++i)
    {
        Poco::JSON::Array queueJson;

        for (typename Queue::const_iterator it = queues[i].begin(); it != queues[i].end(); ++it)
        {
            Poco::JSON::Object entryJson;
            it->get()->serialize(entryJson);
            queueJson.add(entryJson);
        }

        entriesJson.add(queueJson);
    }
    
    json.set("entries", entriesJson);
}

/**
 * Deserializes the queue.
 *
 * @param json JSON with serialized result queue.
 */
template<class T>
void bdg::core::pipeline::MultiQueue<T>::unserialize(const Poco::JSON::Object &json)
{
    Poco::JSON::Array::Ptr entriesJson = json.getArray("entries");
    
    for (int i = 0; i < queues.size() && i < entriesJson->size(); ++i)
    {
        Poco::JSON::Array::Ptr pluginEntriesJson = entriesJson->getArray(i);

        for (int j = 0; j < pluginEntriesJson->size(); ++j)
        {
            Poco::JSON::Object::Ptr entryJson = pluginEntriesJson->getObject(j);

            T *entry = createEntry(entryJson);
            entry->unserialize(*entryJson);

            queues[i].push_back(std::unique_ptr<T>(entry));
        }
    }
}
