/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "utils/checkpointer.h"             // Checkpointer

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * This class does not store checkpoints in the literal meaning of the term.
             * Rather it stores job configuration files.
             */
            class PipelineInfoCheckpointer : public bdg::utils::Checkpointer
            {
            public:
                PipelineInfoCheckpointer() = default;
                explicit PipelineInfoCheckpointer(const int pipelineJobId);
                virtual ~PipelineInfoCheckpointer() = default;

                virtual inline std::string getClassId() const override;

                void store(const std::string &pipelineInfoConfiguration) const;
            };
        }
    }
}

/**
 * Returns name of the checkpointer. It appears in the names of job configuration files.
 *
 * @return Name of the checkpointer.
 */
inline std::string bdg::core::pipeline::PipelineInfoCheckpointer::getClassId() const
{
    return "pipeline";
}
