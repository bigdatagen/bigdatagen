/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/multiqueue.h"           // MultiQueue
#include "core/pipeline/pipeline.h"             // Pipeline
#include "core/pipeline/plugins/task.h"         // Task

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Queue with tasks that should be processed in the future. In default this queue is not
             * used by the task scheduler, but it can be turned on by the "useTaskQueue" setting
             * in the job configuration file.
             */
            class TaskQueue : public MultiQueue<Task>
            {
            public:
                TaskQueue(const bdg::core::pipeline::Pipeline &pipeline, const size_t workersCount, const size_t pluginsCount);
                virtual ~TaskQueue() = default;

                virtual std::unique_ptr<Task> pop(const int requiredPluginIndex = -1, const int workerRank = -1) override;

            protected:
                virtual Task *createEntry(const Poco::JSON::Object::Ptr entryJson) const override;

            private:
                std::unique_ptr<Task> getTaskFromQueueFront(const int pluginIndex);
                
                /**
                 * Instance of the pipeline.
                 */
                const bdg::core::pipeline::Pipeline &pipeline;
            };
        }
    }
}
