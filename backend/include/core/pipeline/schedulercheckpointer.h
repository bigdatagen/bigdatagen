/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @category Pipeline
* @author   Big Data Generator Team
*/

#pragma once

#include "utils/checkpointer.h"             // Checkpointer
#include "core/pipeline/resultqueue.h"      // ResultQueue
#include "core/pipeline/taskqueue.h"        // TaskQueue

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            // Forward declaration.
            class TaskScheduler;

            /**
             * Checkpointer which stores checkpoints with the state of task scheduler.
             * When the pipeline application crashes, it is re-launched by PipelineLauncher
             * server plugin and its state is restored from the last checkpoint.
             */
            class SchedulerCheckpointer : public bdg::utils::Checkpointer
            {
            public:
                SchedulerCheckpointer(const int pipelineJobId, const size_t checkpointPeriodSeconds);
                virtual ~SchedulerCheckpointer() = default;

                static inline std::string getClassId(const int jobId);
                virtual inline std::string getClassId() const override;

                void store(TaskScheduler *scheduler, const int indent = 0);
                void tryStore(TaskScheduler *scheduler, const int indent = 0);
                bool tryRestore(TaskScheduler *scheduler) const;
                void cleanUp() const;

            private:
                /**
                 * Id of currently running job.
                 */
                const int pipelineJobId;
                
                /**
                 * How often do we create checkpoints, 0 turns checkpoints off.
                 */
                const size_t checkpointPeriodSeconds;

                /**
                 * Timestamp of the last created checkpoint.
                 */
                Poco::Timestamp lastCheckpointTime;
            };
        }
    }
}

/**
 * Returns name of the checkpointer. It appears in the names of checkpoint files.
 *
 * @param jobId Id of currently running job.
 *
 * @return Name of the checkpointer.
 */
inline std::string bdg::core::pipeline::SchedulerCheckpointer::getClassId(const int jobId)
{
    return "scheduler-" + std::to_string(jobId);
}

/**
 * Returns name of the checkpointer. It appears in the names of checkpoint files.
 *
 * @return Name of the checkpointer.
 */
inline std::string bdg::core::pipeline::SchedulerCheckpointer::getClassId() const
{
    return getClassId(pipelineJobId);
}
