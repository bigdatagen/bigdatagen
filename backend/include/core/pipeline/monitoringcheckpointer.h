/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "utils/checkpointer.h"     // Checkpointer

#include <Poco/JSON/Object.h>       // Poco::JSON::Object
#include <Poco/Timestamp.h>         // Poco::Timestamp

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            // Forward declaration.
            class TaskScheduler;

            /**
             * Checkpointer which stores monitoring checkpoints. These checkpoints
             * are then read by the PipelineLauncher server plugin and passed to the
             * frontend which renders graphs from data from these checkpoints.
             * In each checkpoint there are various statistics about the job progress.
             */
            class MonitoringCheckpointer : public bdg::utils::Checkpointer
            {
            public:
                MonitoringCheckpointer() = default;
                explicit MonitoringCheckpointer(const int pipelineJobId);
                virtual ~MonitoringCheckpointer() = default;

                virtual inline std::string getClassId() const override;

                void store(TaskScheduler *scheduler, const int indent = 0);
                void store(TaskScheduler *scheduler, Poco::JSON::Object &json, bool useUnits = true);
                
                /**
                 * How often should we store monitoring checkpoints.
                 */
                static const size_t CHECKPOINT_PERIOD_SECONDS;
            };
        }
    }
}

/**
 * Returns name of the checkpointer. It appears in the names of checkpoint files.
 *
 * @return Name of the checkpointer.
 */
inline std::string bdg::core::pipeline::MonitoringCheckpointer::getClassId() const
{
    return "monitoring";
}