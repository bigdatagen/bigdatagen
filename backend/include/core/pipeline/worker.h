/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/task.h"     // Task
#include "core/pipeline/pipeline.h"         // Pipeline
#include "core/pipeline/workerevent.h"      // WorkerEvent

#include <memory>       // std::unique_ptr
#include <queue>        // std::deque

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            // Forward declaration.
            class Pipeline;

            /**
             * Class which reprezents one worker which processes tasks given from the master.
             * Master has an instance of this class for each available worker to keep track
             * about the state of each worker.
             */
            class Worker
            {
            public:
                /**
                 * Defined in which states a worker can be.
                 */
                enum State
                {
                    STATE_IDLE,
                    STATE_PROCESSING,
                    STATE_FINISHING,
                    STATE_FINISHED
                };

                Worker();
                explicit Worker(const int pluginsCount);
                ~Worker() = default;

                inline void setPluginsCount(const int pluginsCount);
                inline void setIdle();
                inline void setProcessing();
                inline void setProcessing(std::unique_ptr<bdg::core::pipeline::Task> &processedTask);
                inline void setFinishing();
                inline void setFinished();
                inline void addEvent(const std::string &name, const Poco::Int64 intNote = 0);

                inline void taskSuccessfull();
                inline void taskError(const bdg::core::pipeline::ErrorTaskResult *taskResult);

                inline State getState() const;
                inline const std::unique_ptr<bdg::core::pipeline::Task> &getProcessedTask() const;
                inline std::unique_ptr<bdg::core::pipeline::Task> getProcessedTaskMove();

                inline void setPluginState(const int pluginIndex, const Poco::JSON::Object &pluginState);
                inline const Poco::JSON::Object &getPluginState(const int pluginIndex) const;
                inline const Poco::UInt64 getProcessedTasks() const;
                inline const Poco::UInt64 getErrorTasks() const;
                inline const std::vector<std::string> &getTaskErrors() const;
                inline const std::vector<WorkerEvent> &getEvents() const;

                void addTaskToQueue(std::unique_ptr<Task> &task);
                std::unique_ptr<Task> getTaskFromQueue();

                const std::string serialize() const;
                void serialize(Poco::JSON::Object &json) const;
                void unserialize(const std::string &serializedWorker, const Pipeline &pipeline);
                void unserialize(const Poco::JSON::Object &json, const Pipeline &pipeline);

            private:
                typedef std::deque<std::unique_ptr<Task>> WorkerTaskQueue;

                /**
                 * Current state of the worker.
                 */
                State state;

                /**
                 * Task that is currently processed on the worker when it is in the "processing" state.
                 */
                std::unique_ptr<Task> processedTask;

                /**
                 * Queue with tasks that should be processed on the worker.
                 */
                WorkerTaskQueue taskQueue;

                /**
                 * States of plugins on the worker. Each plugin can have its state which is stored
                 * in checkpoints and restored during the error recovery process.
                 */
                std::vector<Poco::JSON::Object> pluginStates;

                /**
                 * Number of processed tasks so far (including error tasks).
                 */
                Poco::UInt64 processedTasks = 0;

                /**
                 * Number of error tasks so far. Task is marked as error task, when it did not
                 * finish successufully even after configured number of task restarts.
                 */
                Poco::UInt64 errorTasks = 0;

                /**
                 * Errors that occured during task processing on the worker.
                 */
                std::vector<std::string> taskErrors;

                /**
                 * Events that occured on the worker.
                 */
                std::vector<WorkerEvent> events;
            };
        }
    }
}

/**
 * Sets the size of the pipeline - number of used pipeline plugins.
 *
 * @param pluginsCount Size of the pipeline.
 */
inline void bdg::core::pipeline::Worker::setPluginsCount(const int pluginsCount)
{
    pluginStates = std::vector<Poco::JSON::Object>(pluginsCount);
}

/**
 * Sets that the worker is now in "idle" state.
 */
inline void bdg::core::pipeline::Worker::setIdle()
{
    state = State::STATE_IDLE;
    processedTask = nullptr;
}

/**
 * Sets that the worker is now in "processing" state.
 */
inline void bdg::core::pipeline::Worker::setProcessing()
{
    state = State::STATE_PROCESSING;
    processedTask = nullptr;
}

/**
 * Sets that the worker is now in "processing" state and it processes a given task.
 *
 * @param processedTask
 */
inline void bdg::core::pipeline::Worker::setProcessing(std::unique_ptr<bdg::core::pipeline::Task> &processedTask)
{
    this->state = State::STATE_PROCESSING;
    this->processedTask = std::move(processedTask);
}

/**
 * Sets that the worker is now in "finishing" state.
 */
inline void bdg::core::pipeline::Worker::setFinishing()
{
    state = State::STATE_FINISHING;
    processedTask = nullptr;
}

/**
 * Sets that the worker is now in "finished" state.
 */
inline void bdg::core::pipeline::Worker::setFinished()
{
    state = State::STATE_FINISHED;
    processedTask = nullptr;
}

/**
 * Registers a new event that occured on the worker.
 *
 * @param name Name of the event.
 * @param intNote Additional information of the event.
 */
inline void bdg::core::pipeline::Worker::addEvent(const std::string &name, const Poco::Int64 intNote)
{
    events.push_back(WorkerEvent(name, Poco::Timestamp(), intNote));
}

/**
 * This method is called when a task has been successfully processed on the worker.
 */
inline void bdg::core::pipeline::Worker::taskSuccessfull()
{
    ++processedTasks;
}

/**
 * This method is called when a task is considered as erroneous.
 *
 * @param taskResult Result of the task processing.
 */
inline void bdg::core::pipeline::Worker::taskError(const bdg::core::pipeline::ErrorTaskResult *taskResult)
{
    taskErrors.emplace_back("Plugin " + std::to_string(taskResult->getPluginIndex()) + ": " + taskResult->getMessage());
    ++processedTasks;
    ++errorTasks;
}

/**
 * Returns the state of the worker.
 *
 * @return Current worker state.
 */
inline bdg::core::pipeline::Worker::State bdg::core::pipeline::Worker::getState() const
{
    return state;
}

/**
 * Returns task that is currently processed on the worker.This is only relevant when the
 * worker is in the "processing" state, otherwise it returns nullptr.
 *
 * @return Task that is currently processed on the worker
 */
inline const std::unique_ptr<bdg::core::pipeline::Task> &bdg::core::pipeline::Worker::getProcessedTask() const
{
    return processedTask;
}

/**
 * Returns task that is currently processed on the worker.This is only relevant when the
 * worker is in the "processing" state, otherwise it returns nullptr.
 *
 * @return Task that is currently processed on the worker
 */
inline std::unique_ptr<bdg::core::pipeline::Task> bdg::core::pipeline::Worker::getProcessedTaskMove()
{
    return std::move(processedTask);
}

/**
 * Sets the state of some plugin running on the worker. This state is stored in checkpoints
 * and when error recovery is processed, plugin state is loaded from that JSON.
 * 
 * @param pluginState JSON object reprezenting the state of a plugin running on the worker.
 */
inline void bdg::core::pipeline::Worker::setPluginState(const int pluginIndex, const Poco::JSON::Object &pluginState)
{
    pluginStates[pluginIndex] = pluginState;
}

/**
 * Returns the state of some plugin running on the worker.
 *
 * @param pluginIndex Index of the worker in the pipeline.
 *
 * @return State of some plugin running on the worker.
 */
inline const Poco::JSON::Object &bdg::core::pipeline::Worker::getPluginState(const int pluginIndex) const
{
    return pluginStates[pluginIndex];
}

/**
 * Returns total number of processed tasks so far (including error tasks). 
 *
 * @return Number of processed tasks so far.
 */
inline const Poco::UInt64 bdg::core::pipeline::Worker::getProcessedTasks() const
{
    return processedTasks;
}

/**
 * Returns number of error tasks so far. Task is marked as error task, when it did not
 * finish successufully even after configured number of task restarts.
 *
 * @return Number of error tasks so far.
 */
inline const Poco::UInt64 bdg::core::pipeline::Worker::getErrorTasks() const
{
    return errorTasks;
}

/**
 * Returns errors that occured during task processing on the worker.
 *
 * @return Errors that occured during task processing on the worker.
 */
inline const std::vector<std::string> &bdg::core::pipeline::Worker::getTaskErrors() const
{
    return taskErrors;
}

/**
 * Returns events that occured on the worker.
 *
 * @return Events that occured on the worker.
 */
inline const std::vector<bdg::core::pipeline::WorkerEvent> &bdg::core::pipeline::Worker::getEvents() const
{
    return events;
}
