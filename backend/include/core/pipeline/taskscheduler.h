/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/successtaskresult.h"        // SuccessTaskResult
#include "core/pipeline/plugins/plugin.h"                   // Plugin
#include "core/pipeline/resultqueue.h"                      // ResultQueue
#include "core/pipeline/taskqueue.h"                        // TaskQueue
#include "core/pipeline/taskfactory.h"                      // TaskFactory
#include "core/pipeline/pipeline.h"                         // Pipeline
#include "core/pipeline/progresscounter.h"                  // ProgressCounter
#include "core/pipeline/speedlimiterscheduler.h"            // SpeedLimiterScheduler
#include "core/pipeline/statisticscounter.h"                // StatisticsCounter
#include "core/pipeline/schedulercheckpointer.h"            // SchedulerCheckpointer
#include "core/pipeline/monitoringcheckpointer.h"           // MonitoringCheckpointer
#include "core/pipeline/monitoringcheckpointerrestorer.h"   // MonitoringCheckpointerRestorer
#include "core/pipeline/workergroup.h"                      // WorkerGroup
#include "utils/logger.h"                                   // Logger

#include <Poco/Thread.h>                // Poco::Thread
#include <Poco/RunnableAdapter.h>       // Poco::RunnableAdapter

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Instance of this class runs in the master thread, i.e. in the thread with
             * rank 0. It splits the job into multiple tasks that are scheduled to worker
             * threads (all the MPI threads with rank greater than 0). Tasks are sent
             * to workers as MPI messages. Content of these messages is are tasks setialized
             * into JSON strings. Workers send back messages with task results which are
             * then analyzed for statistics about the job progress.
             */
            class TaskScheduler
            {
            public:
                TaskScheduler(const bdg::core::pipeline::Pipeline &pipeline, const bdg::utils::MPIProxy &mpi);
                ~TaskScheduler() = default;

                void schedule();

                inline const Pipeline &getPipeline() const;
                inline Poco::Logger &getLogger() const;
                inline WorkerGroup &getWorkers();
                inline ResultQueue &getResultQueue();
                inline TaskQueue &getTaskQueue();
                inline ProgressCounter &getProgressCounter();
                inline SpeedLimiterScheduler &getSpeedLimiterScheduler();
                inline StatisticsCounter &getStatisticsCounter();

                void serialize(Poco::JSON::Object &json) const;
                void unserialize(const Poco::JSON::Object &json);

            private:
                std::unique_ptr<bdg::core::pipeline::Task> getTaskToProcess(const int workerRank);

                void progressThreadWork();
                void monitoringThreadWork();

                void periodicWork();
                void generateTaskToQueue();
                void modifyProcessedTask(std::unique_ptr<bdg::core::pipeline::Task> &task);
                void sendInitialCommandsToWorkers();
                void sendCommandToWorker(const int workerRank);
                void initializeWorker(const int workerRank);
                void sendTaskToWorker(const int workerRank, std::unique_ptr<bdg::core::pipeline::Task> &task);
                void setWorkerIdle(const int workerRank);
                void terminateWorker(const int workerRank);
                void handleInitError(const std::string &debugError, const std::string &error = "");
                void handleErrorResult(const int workerRank, std::unique_ptr<bdg::core::pipeline::TaskResult> &taskResult);
                void handleFatalError(const std::string &error);
                void writeStatistics();
                void cleanUp();
                
                bool checkConfiguration();
                bool checkPipelineConfiguration();
                bool checkOutputConfiguration();
                bool checkSchedulerConfiguration();
                bool isJobStopped() const;
                
                void processSuccessTaskResult(const int workerRank, std::unique_ptr<bdg::core::pipeline::TaskResult> &taskResult);

                bool canCreateGeneratorTask() const;
                bool isJobTerminating() const;
                bool isJobFinished() const;

                /**
                 * Instance of the pipeline with plugins.
                 */
                const bdg::core::pipeline::Pipeline &pipeline;

                /**
                 * MPI wrapper.
                 */
                const bdg::utils::MPIProxy &mpi;
                
                /**
                 * Factory for tasks sent to workers.
                 */
                TaskFactory taskFactory;

                /**
                 * Information about the state of all available workers. This object
                 * is stored in checkpoints.
                 */
                WorkerGroup workers;

                /**
                 * Queue with task results received from workers. This object is stored
                 * in checkpoints.
                 */
                ResultQueue resultQueue;

                /**
                 * Queue with tasks to be processed by workers. This object is stored
                 * in checkpoints.
                 */
                TaskQueue taskQueue;

                /**
                 * Object for counting the progress of the current job. This object is
                 * stored in checkpoints.
                 */
                ProgressCounter progressCounter;

                /**
                 * Object for setting the storing speed to storer tasks. This object is
                 * stored in checkpoints.
                 */
                SpeedLimiterScheduler speedLimiterScheduler;

                /**
                 * Object for counting statictics about currently running job. This object
                 * is stored in checkpoints.
                 */
                StatisticsCounter statisticsCounter;

                /**
                 * Object for storing checkpoints which are needed for error recovery.
                 * When a job crashes and it gets re-launched, it loads its state from
                 * the last checkpoint.
                 */
                SchedulerCheckpointer checkpointer;

                /**
                 * Object for storing monitoring checkpoints with statistical information
                 * about the job. These checkpoints are then read by PipelineLauncher
                 * server plugin and sent to the frontend where graphs are created from
                 * data contained in these checkpoints.
                 */
                MonitoringCheckpointer monitoringCheckpointer;

                /**
                 * Id assigned to the next file that should be generated.
                 */
                Poco::UInt64 nextFileIdGlobal = 1;

                /**
                 * Total number of promissed files to be generated in the last stage
                 * of the pipeline.
                 */
                Poco::UInt64 totalPromissedFiles = 0;

                /**
                 * Runnable for the progress thread.
                 */
                Poco::RunnableAdapter<TaskScheduler> progressRunnable;

                /**
                 * Thread which periodically writes the progress of the job to stdout.
                 */
                Poco::Thread progressThread;

                /**
                 *  Runnable for the monitoring thread.
                 */
                Poco::RunnableAdapter<TaskScheduler> monitoringRunnable;

                /**
                 * Thread which periodically stores a monitoring checkpoint.
                 */
                Poco::Thread monitoringThread;

                /**
                 * Should we terminate the job? This variable is set to true when a fatal
                 * error occurs.
                 */
                bool forceTerminate = false;

                /**
                 * Maximum number of allowed task restarts before it is considered as
                 * permanently failed.
                 */
                static const size_t ALLOWED_TASK_RESTARTS;
            };
        }
    }
}

/**
 * Returns the instance of the pipeline with plugins.
 *
 * @return Instance of the pipeline with plugins.
 */
inline const bdg::core::pipeline::Pipeline &bdg::core::pipeline::TaskScheduler::getPipeline() const
{
    return pipeline;
}

/**
 * Returns the application logger.
 *
 * @return Application logger.
 */
inline Poco::Logger &bdg::core::pipeline::TaskScheduler::getLogger() const
{
    return bdg::utils::Logger::getInstance().getLogger(mpi.getRank());
}

/**
 * Returns information about the state of all available workers.
 *
 * @return Information about the state of all available workers.
 */
inline bdg::core::pipeline::WorkerGroup &bdg::core::pipeline::TaskScheduler::getWorkers()
{
    return workers;
}

/**
 * Returns queue with task results received from workers.
 *
 * @return Queue with task results received from workers.
 */
inline bdg::core::pipeline::ResultQueue &bdg::core::pipeline::TaskScheduler::getResultQueue()
{
    return resultQueue;
}

/**
 * Returns queue with tasks to be processed by workers.
 *
 * @return Queue with tasks to be processed by workers.
 */
inline bdg::core::pipeline::TaskQueue &bdg::core::pipeline::TaskScheduler::getTaskQueue()
{
    return taskQueue;
}

/**
 * Returns object for counting the progress of the current job.
 *
 * @return Object for counting the progress of the current job.
 */
inline bdg::core::pipeline::ProgressCounter &bdg::core::pipeline::TaskScheduler::getProgressCounter()
{
    return progressCounter;
}

/**
 * Returns object for setting the storing speed to storer tasks.
 *
 * @return Object for setting the storing speed to storer tasks.
 */
inline bdg::core::pipeline::SpeedLimiterScheduler &bdg::core::pipeline::TaskScheduler::getSpeedLimiterScheduler()
{
    return speedLimiterScheduler;
}

/**
 * Returns object for counting statictics about currently running job.
 *
 * @return Object for counting statictics about currently running job.
 */
inline bdg::core::pipeline::StatisticsCounter &bdg::core::pipeline::TaskScheduler::getStatisticsCounter()
{
    return statisticsCounter;
}
