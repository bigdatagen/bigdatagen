/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @category Pipeline
* @author   Big Data Generator Team
*/

#pragma once

#include "core/pipeline/pipeline.h"             // Pipeline
#include "core/pipeline/resultqueue.h"          // ResultQueue
#include "core/pipeline/plugins/task.h"         // Task

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Factory for tasks that are then processed on some worker. Master has
             * an instance of this factory to create tasks.
             */
            class TaskFactory
            {
            public:
                TaskFactory(const Pipeline &pipeline, ResultQueue &resultQueue);

                std::unique_ptr<bdg::core::pipeline::Task> createTask(const bool canCreateGeneratorTask, const int requiredPluginIndex = -1, const int workerRank = 0);

            private:
                /**
                 * Instance of the pipeline.
                 */
                const Pipeline &pipeline;

                /**
                 * Queue with task results.
                 */
                ResultQueue &resultQueue;
            };
        }
    }
}
