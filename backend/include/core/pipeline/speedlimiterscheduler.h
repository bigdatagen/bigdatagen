/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/filefrequency.h"        // FileFrequency
#include "core/pipeline/plugins/storertask.h"           // StorerTask
#include "core/pipeline/schedulerinfo.h"                // SchedulerInfo

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Instance of this class is created on master. It schedules the speed limitation
             * for workers. Instance of SpeedLimiter class is limiting the speed on each worker
             * in storer plugin. SpeedLimiter class is a counterpart of this class on each worker.
             */
            class SpeedLimiterScheduler
            {
            public:
                SpeedLimiterScheduler(const SchedulerInfo &schedulerInfo, const size_t pluginsCount, const size_t workersCount);
                ~SpeedLimiterScheduler() = default;

                inline void storerTaskStarted();
                inline void storerTaskFinished();
                inline bool canRunAnotherStorerWorker() const;

                void handleBytesStoredMessage(const std::string &messageData);
                void tryUpdateOptimalCurrentStorersCount();
                void setStorerTaskFrequency(StorerTask *task, const FileFrequency &configuredFrequency) const;
                void updateTimes();

                double getCurrentSpeed() const;
                double getDemandedSpeed() const;
                double getBestSpeed(size_t &bestStorersCount) const;
                
                void serialize(Poco::JSON::Object &json) const;
                void unserialize(const Poco::JSON::Object &json);

            private:
                void updateOptimalCurrentStorersCount(const double currentSpeed, const double demandedSpeed);

                /**
                 * Configured demanded store speed.
                 */
                const FileFrequency &demandedFrequency;

                /**
                 * Current number of storers processing storer task. This number is between 0 (there
                 * might be no storers currently processing a task) and optimalCurrentStorersCount (we would
                 * like to maintain currentStorersCount == optimalCurrentStorersCount).
                 */
                size_t currentStorersCount = 0;

                /**
                 * Current optimal number of storers used to store generated files. I.e. we would
                 * like to preserve this number of storers. This number is between 1 (we need at least
                 * one storer) and maxStorersCount.
                 */
                size_t optimalCurrentStorersCount = 1;

                /**
                 * Maximum number of storers that can be used to store generated files.
                 */
                size_t maxStorersCount;

                /**
                 * Timestamp when we started to measure time after the last change of optimal
                 * number of storer workers.
                 */
                Poco::Timestamp storersCountStartTime;
                
                /**
                 * Index in this vector is number of used storers. Value is number of
                 * files stored when that number of storers was in use (i.e. scheduler used
                 * that number of storers to store data).
                 */
                std::vector<Poco::UInt64> storedFilesPerStorersCount;

                /**
                 * Index in this vector is number of used storers. Value is number of
                 * bytes stored when that number of storers was in use (i.e. scheduler used
                 * that number of storers to store data). This vector is used to determine
                 * the number of storers with the highest average speed.
                 */
                std::vector<Poco::UInt64> storedBytesPerStorersCount;

                /**
                 * Index in this vector is number of used storers. Value is number of
                 * seconds that passed when that number of storers was in use (i.e. scheduler used
                 * that number of storers to store data). This vector is used to determine
                 * the number of storers with the highest average speed.
                 */
                std::vector<size_t> secondsInUsePerStorersCount;

                /**
                 * Was the scheduler started? It starts when the first storer task is
                 * ready to be processed.
                 */
                bool started = false;

                /**
                 * Should we use automatic scaling of storer plugins (set to true, default) or do we
                 * want to use as many storers as possible (set to false).
                 */
                bool useProgressiveStorersCount = true;

                /**
                 * How long time do we have to count statistics per number of storers
                 * in order to consider it as "relevant". I.e. if we count the statistics
                 * at least this number of seconds, we can base our further calculations
                 * about the best number of storers on it.
                 */
                static const size_t MIN_SECONDS_PER_STORER;
                
                /**
                 * Number of seconds that must past since the last change of optimal
                 * number of storer workers.
                 */
                static const size_t MIN_SECONDS_PER_LAST_STORERS_COUNT_CHANGE;

                /**
                 * Coefficient used to determine, whether we should increase or decrease
                 * currently used number of storers when we do not keep pace with the demanded speed.
                 */
                static const double BEST_SPEED_COEFFICIENT;
            };
        }
    }
}

/**
 * Called when a new storer task is sent to the worker.
 */
inline void bdg::core::pipeline::SpeedLimiterScheduler::storerTaskStarted()
{
    if (!started)
    {
        storersCountStartTime.update();
        started = true;
    }

    ++currentStorersCount;
}

/**
 * Called when a storer task is finished.
 */
inline void bdg::core::pipeline::SpeedLimiterScheduler::storerTaskFinished()
{
    --currentStorersCount;
}

/**
 * Returns whether the task scheduler can start another storer task, i.e. if we
 * do not run more simultaneous storer tasks than allowed in the moment.
 *
 * @return Can we run another storer task?
 */
inline bool bdg::core::pipeline::SpeedLimiterScheduler::canRunAnotherStorerWorker() const
{
    return currentStorersCount < optimalCurrentStorersCount;
}
