/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/worker.h"       // Worker

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Class reprezenting all the available workers. Master thread has one
             * instance of this class to keep track of state of all the workers.
             */
            class WorkerGroup
            {
            public:
                WorkerGroup(const Pipeline &pipeline, const int mpiSize, const int pluginsCount);
                ~WorkerGroup() = default;

                void setWorkerState(const int workerRank, const Worker::State state);
                void setWorkerProcessing(const int workerRank, std::unique_ptr<bdg::core::pipeline::Task> &task);
                
                inline void setWorkerPluginState(const int workerRank, const int pluginIndex, const Poco::JSON::Object &pluginState);

                inline Worker &getWorker(const int workerRank);
                inline const size_t getIdleWorkersCount() const;
                inline const size_t getProcessingWorkersCount() const;
                inline const size_t getFinishingWorkersCount() const;
                inline const size_t getFinishedWorkersCount() const;
                inline const size_t getSize() const;

                void serialize(Poco::JSON::Array &json) const;
                void unserialize(const Poco::JSON::Array &json);
                void storeTaskSchedule(const int jobId, const Poco::UInt64 elapsedMicroseconds) const;

            private:
                /**
                 * Instance of the pipeline.
                 */
                const bdg::core::pipeline::Pipeline &pipeline;

                /**
                 * Available workers.
                 */
                std::vector<Worker> workers;

                /**
                 * Number of workers in the idle state.
                 */
                size_t idleWorkersCount = 0;

                /**
                 * Number of workers in the processing state.
                 */
                size_t processingWorkersCount = 0;

                /**
                 * Number of workers in the finishing state.
                 */
                size_t finishingWorkersCount = 0;

                /**
                 * Number of workers in the finished state.
                 */
                size_t finishedWorkersCount = 0;
            };
        }
    }
}

/**
 * Sets the state of one plugin running on the worker.
 *
 * @param workerRank Rank of the worker.
 * @param pluginIndex Index of the plugin in the pipeline.
 * @param pluginState State of the plugin.
 */
inline void bdg::core::pipeline::WorkerGroup::setWorkerPluginState(const int workerRank, const int pluginIndex, const Poco::JSON::Object &pluginState)
{
    if (workerRank >= 0 && workerRank < workers.size())
    {
        workers[workerRank].setPluginState(pluginIndex, pluginState);
    }
    else
    {
        throw Poco::InvalidArgumentException("Worker rank out of range!");
    }
}

/**
 * Returns the worker with a given rank.
 *
 * @param workerRank Rank of the worker.
 *
 * @return Worker with a given rank.
 */
inline bdg::core::pipeline::Worker &bdg::core::pipeline::WorkerGroup::getWorker(const int workerRank)
{
    if (workerRank >= 0 && workerRank < workers.size())
    {
        return workers[workerRank];
    }
    else
    {
        throw Poco::InvalidArgumentException("Worker rank out of range!");
    }
}

/**
 * Returns number of workers in the idle state.
 *
 * @return Number of workers in the idle state.
 */
inline const size_t bdg::core::pipeline::WorkerGroup::getIdleWorkersCount() const
{
    return idleWorkersCount;
}

/**
 * Returns number of workers processing some task.
 *
 * @return Number of workers processing some task.
 */
inline const size_t bdg::core::pipeline::WorkerGroup::getProcessingWorkersCount() const
{
    return processingWorkersCount;
}

/**
 * Returns number of workers in the finishing state.
 *
 * @return Number of workers in the finishing state.
 */
inline const size_t bdg::core::pipeline::WorkerGroup::getFinishingWorkersCount() const
{
    return finishingWorkersCount;
}

/**
 * Returns number of workers that has already finished the job.
 *
 * @return Number of workers that has already finished the job.
 */
inline const size_t bdg::core::pipeline::WorkerGroup::getFinishedWorkersCount() const
{
    return finishedWorkersCount;
}

/**
 * Returns total number of available workers.
 *
 * @return Total number of available workers.
 */
inline const size_t bdg::core::pipeline::WorkerGroup::getSize() const
{
    return workers.size() - 1;
}
