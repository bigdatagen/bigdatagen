/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/pipelineinfo.h"     // PipelineInfo
#include "utils/checkpointer.h"             // Checkpointer

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Loads pipeline job configuration files into the instance of
             * PipelineInfo class.
             */
            class PipelineInfoCheckpointerRestorer : public bdg::utils::Checkpointer
            {
            public:
                explicit PipelineInfoCheckpointerRestorer(const int pipelineJobId);
                virtual ~PipelineInfoCheckpointerRestorer() = default;

                virtual inline std::string getClassId() const override;

                PipelineInfo restore(const int mpiMasterRank, const int mpiSize) const;
            };
        }
    }
}

/**
 * Returns name of the checkpointer. It appears in the names of job configuration files.
 *
 * @return Name of the checkpointer.
 */
inline std::string bdg::core::pipeline::PipelineInfoCheckpointerRestorer::getClassId() const
{
    return "pipeline";
}
