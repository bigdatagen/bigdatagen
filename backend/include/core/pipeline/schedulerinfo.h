/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/filefrequency.h"        // FileFrequency
#include "core/pipeline/plugins/valuewithunit.h"        // ValueWithUnit
#include "utils/config.h"                               // Config

#include <Poco/JSON/Object.h>       // Poco::JSON::Object

#include <vector>       // std::vector
#include <map>          // std::map
#include <iostream>     // std::cout
#include <climits>      // INT_MAX

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            /**
             * Values of scheduler configuration directives loaded from the job
             * configuration file.
             */
            class SchedulerInfo
            {
            public:
                explicit SchedulerInfo(const size_t clusterSize);
                ~SchedulerInfo() = default;

                void setWherePluginCanRun(const int pluginIndex, const int rank);
                void setWhatCanRunOnRank(const int rank, const int pluginIndex);
                void setFileFrequency(const std::string &speedStr);
                void setTotal(const std::string &totalStr);
                void setSchedulerConfiguration(const Poco::JSON::Object::Ptr schedulerConfiguration);

                bool canRunPluginAtRank(const int pluginIndex, const int rank) const;
                int findLeastBusyRank() const;
                int getRandomRankForPlugin(const int pluginIndex, const int defaultRank) const;

                inline const FileFrequency &getFileFrequency() const;
                inline const ValueWithUnit &getTotal() const;
                inline const size_t getFilesPerTask() const;
                inline const size_t getFilesPerDirectory() const;
                inline const size_t getSchedulerCheckpointPeriodSeconds() const;

                inline const bool displayGeneratedFiles() const;
                inline const bool preserveFilesOrder() const;
                inline const bool cleanUp() const;
                inline const bool continuousCleanUp() const;
                inline const bool useTaskQueue() const;
                inline const bool useStorerPipelineOptimization() const;
                inline const bool useProgressiveStorersCount() const;
                inline const bool requiresPipelineTasksOnSameNode() const;

                void reset();
                std::string debug(const bool output = true) const;

            private:
                typedef std::map<int, std::vector<int>> IntToVectorMap;

                /**
                 * Size of the cluster where BDG runs.
                 */
                const size_t clusterSize;

                /**
                 * Key in the map is an index of the plugin in the pipeline. Value is a vector of
                 * ranks where the plugin is allowed to run.
                 */
                IntToVectorMap wherePluginsCanRun;

                /**
                 * Key in the map is a rank. Valu is a vector of indexes of plugins in the pipeline
                 * which are allowed to run on this particular rank in the cluster.
                 */
                IntToVectorMap whatCanRunOnRanks;

                /**
                 * Frequency of file storing (files per second or bytes per second).
                 */
                FileFrequency fileFrequency;

                /**
                 * Total number of generated files/total size of generated files/total generation time.
                 */
                ValueWithUnit total;

                /**
                 * How many files should be generated per one scheduler task.
                 */
                size_t filesPerTask = 0;

                /**
                 * How many files should be stored in one directory on the worker's filesystem.
                 */
                size_t filesPerDirectory = 10000;
                
                /**
                 * How often do we create checkpoints.
                 */
                size_t schedulerCheckpointPeriodSeconds;

                /**
                 * Should we write out the names of all the generated files? This might really slow
                 * down the scheduler or even make it run out of memory. Set this to true only for testing
                 * purposes and with relatively small data sets.
                 */
                bool doDisplayGeneratedFiles = false;

                /**
                 * Should we preserve order of generated files when processing them? This can be
                 * useful, when we store files to some external cloud and we want to send there files
                 * in the same order as generated (e.g. we use some sequences in generated files).
                 */
                bool doPreserveFilesOrder = false;

                /**
                 * When the job is done, should we automatically delete all the intermediate files? I.e.
                 * files generated by other plugins than the last one in the pipeline.
                 */
                bool doCleanUp;

                /**
                 * We can use a continuous version of clean up, i.e. files are deleted immediately after
                 * they are processed by the next stage of the pipeline .It might lead to some error situations
                 * when error tasks occur, so turn it on wisely.
                 */
                bool doContinuousCleanUp;

                /**
                 * Should we use task queue? Using of task queue might speed up scheduler, but it might
                 * also mean more generated tasks.
                 */
                bool canUseTaskQueue;

                /**
                 * Should we use the pipeline optimization which includes removing FileStorer
                 * plugin from the pipeline on certain occassions?
                 */
                bool canUseStorerPipelineOptimization = true;

                /**
                 * Should we use automatic scaling of storer plugins (set to true, default) or do we
                 * want to use as many storers as possible (set to false).
                 */
                bool canUseProgressiveStorersCount = true;

                /**
                 * If we have more plugins in a pipeline, some other rank (possibly on different node)
                 * can process a result of a task. If you set this to true, then task results must be processed
                 * on the node which generated them.
                 */
                bool requirePipelineTasksOnSameNode = false;
            };
        }
    }
}

/**
 * Returns frequency of file storing (files per second or bytes per second).
 *
 * @return Frequency of file storing.
 */
inline const bdg::core::pipeline::FileFrequency &bdg::core::pipeline::SchedulerInfo::getFileFrequency() const
{
    return fileFrequency;
}

/**
 * Returns the total number of generated files or total size of generated files or total generation time.
 *
 * @return Job target.
 */
inline const bdg::core::pipeline::ValueWithUnit &bdg::core::pipeline::SchedulerInfo::getTotal() const
{
    return total;
}

/**
 * Returns how many files should be generated per one scheduler task.
 *
 * @return How many files should be generated per one scheduler task.
 */
inline const size_t bdg::core::pipeline::SchedulerInfo::getFilesPerTask() const
{
    return filesPerTask;
}

/**
 * Returns how many files should be stored in one directory on the worker's filesystem.
 *
 * @return How many files should be stored in one directory on the worker's filesystem.
 */
inline const size_t bdg::core::pipeline::SchedulerInfo::getFilesPerDirectory() const
{
    return filesPerDirectory;
}

/**
 * Returns how often do we create checkpoints.
 *
 * @return How often do we create checkpoints.
 */
inline const size_t bdg::core::pipeline::SchedulerInfo::getSchedulerCheckpointPeriodSeconds() const
{
    return schedulerCheckpointPeriodSeconds;
}

/**
 * Returns whether we should write out the names of all the generated files. This might really
 * slow down the scheduler or even make it run out of memory. Set this to true only for testing
 * purposes and with relatively small data sets.
 *
 * @return Should we write out the contents of all the generated files?
 */
inline const bool bdg::core::pipeline::SchedulerInfo::displayGeneratedFiles() const
{
    return doDisplayGeneratedFiles;
}

/**
 * Returns whether we should preserve order of generated files when processing them? This can be
 * useful, when we store files to some external cloud and we want to send there files in the same
 * order as generated (e.g. we use some sequences in generated files).
 *
 * @return Should we preserve order of generated files when processing them?
 */
inline const bool bdg::core::pipeline::SchedulerInfo::preserveFilesOrder() const
{
    return doPreserveFilesOrder;
}

/**
 * When the job is done, should we automatically delete all the intermediate files? I.e.
 * files generated by other plugins than the last one in the pipeline.
 *
 * @return Should we automatically delete all the intermediate files?
 */
inline const bool bdg::core::pipeline::SchedulerInfo::cleanUp() const
{
    return doCleanUp;
}

/**
 * Returns whether we use a continuous version of clean up, i.e. files are deleted immediately after
 * they are processed by the next stage of the pipeline .It might lead to some error situations
 * when error tasks occur, so turn it on wisely.
 *
 * @return Do we use a continuous version of clean up?
 */
inline const bool bdg::core::pipeline::SchedulerInfo::continuousCleanUp() const
{
    return doContinuousCleanUp;
}

/**
 * Returns whether we should use task queue. Using of task queue might speed up scheduler, but it
 * might also mean more generated tasks.
 *
 * @return Should we use task queue?
 */
inline const bool bdg::core::pipeline::SchedulerInfo::useTaskQueue() const
{
    return canUseTaskQueue;
}

/**
 * Returns whether we should use the pipeline optimization which includes removing FileStorer
 * plugin from the pipeline on certain occassions.
 *
 * @return Should we use the pipeline optimization?
 */
inline const bool bdg::core::pipeline::SchedulerInfo::useStorerPipelineOptimization() const
{
    return canUseStorerPipelineOptimization;
}

/**
 * Returns whether we should use automatic scaling of storer plugins (set to true, default) or
 * whrther we want to use as many storers as possible (set to false).
 *
 * @return Should we use automatic scaling of storer plugins?
 */
inline const bool bdg::core::pipeline::SchedulerInfo::useProgressiveStorersCount() const
{
    return canUseProgressiveStorersCount;
}

/**
 * If we have more plugins in a pipeline, some other rank (possibly on different node)
 * can process a result of a task. If you set this to true, then task results must be processed
 * on the node which generated them.
 *
 * @return Should the following tasks be processed on the same physical node?
 */
inline const bool bdg::core::pipeline::SchedulerInfo::requiresPipelineTasksOnSameNode() const
{
    return requirePipelineTasksOnSameNode;
}
