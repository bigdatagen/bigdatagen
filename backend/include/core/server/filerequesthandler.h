/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#pragma once

#include "utils/logger.h"                       // Logger

#include <Poco/Net/HTTPRequestHandler.h>        // Poco::Net::HTTPRequestHandler
#include <Poco/Net/HTTPServerRequest.h>         // Poco::Net::HTTPServerRequest
#include <Poco/Net/HTTPServerResponse.h>        // Poco::Net::HTTPServerResponse

namespace bdg
{
    namespace server
    {
        /**
         * Class which handles one request to the server for a specific file.
         */
        class FileRequestHandler : public Poco::Net::HTTPRequestHandler
        {
        public:
            FileRequestHandler(const std::string &file, Poco::Logger &logger);
            virtual void handleRequest(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response) override;

        private:
            bool setContentType(const std::string &extension, Poco::Net::HTTPServerResponse &response);

            /**
             * HTML returned to the client when demanded file does not exist.
             */
            static const std::string NOT_FOUND_HTML;

            /**
             * Path to the demanded file.
             */
            const std::string file;

            /**
             * Application logger.
             */
            Poco::Logger &logger;
        };
    }
}
