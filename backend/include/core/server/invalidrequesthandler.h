/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#pragma once

#include "utils/logger.h"                       // Logger

#include <Poco/Net/HTTPRequestHandler.h>        // Poco::Net::HTTPRequestHandler
#include <Poco/Net/HTTPServerRequest.h>         // Poco::Net::HTTPServerRequest
#include <Poco/Net/HTTPServerResponse.h>        // Poco::Net::HTTPServerResponse

namespace bdg
{
    namespace server
    {
        /**
         * Class which handles one invalid request to the server.
         */
        class InvalidRequestHandler : public Poco::Net::HTTPRequestHandler
        {
        public:
            InvalidRequestHandler(Poco::Logger &logger);
            virtual void handleRequest(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response) override;

        private:
            /**
             * Application logger.
             */
            Poco::Logger &logger;
        };
    }
}
