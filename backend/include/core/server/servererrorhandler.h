/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#pragma once

#include "utils/logger.h"           // Logger
#include <Poco/ErrorHandler.h>      // Poco::ErrorHandler

namespace bdg
{
    namespace server
    {
        /**
         * Class handling unhanded exceptions in server.
         */
        class ServerErrorHandler : public Poco::ErrorHandler
        {
        public:
            explicit ServerErrorHandler(Poco::Logger &logger, const bool displayErrors = false);

            virtual void exception(const Poco::Exception &e) override;
            virtual void exception(const std::exception &e) override;
            virtual void exception() override;

        private:
            void handleException(const std::string &message);

            /**
             * Application logger.
             */
            Poco::Logger &logger;

            /**
             * Display all the unhandled error to std::cerr?
             */
            const bool displayErrors;
        };
    }
}
