/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#pragma once

#include "components/serverplugin.h"                // ServerPlugin
#include "core/server/loginprovider.h"              // LoginProvider
#include "utils/pluginmanager.h"                    // PluginManager
#include "utils/logger.h"                           // Logger

#include <Poco/Net/HTTPRequestHandler.h>            // Poco::Net::HTTPRequestHandler
#include <Poco/Net/HTTPRequestHandlerFactory.h>     // Poco::Net::HTTPRequestHandlerFactory
#include <Poco/Net/HTTPServerRequest.h>             // Poco::Net::HTTPServerRequest

namespace bdg
{
    namespace server
    {
        /**
         * Factory for the request handlers.
         */
        class RequestHandlerFactory: public Poco::Net::HTTPRequestHandlerFactory
        {
        public:
            RequestHandlerFactory(Poco::Logger &logger, LoginProvider &loginProvider);
            virtual Poco::Net::HTTPRequestHandler* createRequestHandler(const Poco::Net::HTTPServerRequest &request) override;

        private:
            const bdg::utils::PluginManager<bdg::server::ServerPlugin> pluginManager;

            /**
             * Application logger.
             */
            Poco::Logger &logger;

            /**
             * Information about logged in users.
             */
            LoginProvider &loginProvider;
        };
    }
}
