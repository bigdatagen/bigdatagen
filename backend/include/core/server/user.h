/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#pragma once

#include <string>       // std::string

namespace bdg
{
    namespace server
    {
        /**
         * Implementation of user identity.
         */
        class User
        {
        public:
            User(const std::string &username = "", const bool loggedIn = false);
            ~User() = default;

            inline const std::string getUsername() const;
            inline bool isLoggedIn() const;

        private:
            /**
             * Username of logged in user.
             */
            const std::string username;

            /**
             * Is the user logged in?
             */
            const bool loggedIn;
        };
    }
}

/**
 * Returns the username of logged in user.
 *
 * @return Username of logged in user.
 */
inline const std::string bdg::server::User::getUsername() const
{
    return username;
}

/**
 * Returns whether the user is currently logged in.
 *
 * @return Is the user currently logged in?
 */
inline bool bdg::server::User::isLoggedIn() const
{
    return loggedIn;
}
