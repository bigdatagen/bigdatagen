/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/server/user.h"                   // User

#include <Poco/Net/HTTPServerRequest.h>         // Poco::Net::HTTPServerRequest
#include <Poco/Net/HTTPServerResponse.h>        // Poco::Net::HTTPServerResponse

#include <map>      // std::map

namespace bdg
{
    namespace server
    {
        /**
         * Class which maintains information about logged in users and provides
         * interface for logging in and out.
         */
        class LoginProvider
        {
        public:
            LoginProvider() = default;
            ~LoginProvider() = default;

            const User login(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response);
            const User getUser(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response) const;
            void logout(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response);

        private:
            typedef std::map<std::string, User> UserCollection;

            void storeAccessTokenCookie(Poco::Net::HTTPServerResponse &response, const std::string &accessToken, const int maxAge) const;
            const std::string getAccessTokenFromCookie(Poco::Net::HTTPServerRequest &request) const;

            /**
             * Loggen in users.
             */
            UserCollection loggedUsers;

            /**
             * Name of the cookie which stores authorization token in the user's web browser.
             */
            static const std::string ACCESS_TOKEN_COOKIE_NAME;

            /**
             * Expiration of the cookie which stores authorization token.
             */
            static const int ACCESS_TOKEN_COOKIE_EXPIRATION_SECONDS;
        };
    }
}
