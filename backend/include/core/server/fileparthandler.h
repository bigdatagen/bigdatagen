/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#pragma once

#include "utils/fileupload.h"           // FileUpload

#include <Poco/Net/PartHandler.h>       // Poco::Net::PartHandler
#include <Poco/Net/MessageHeader.h>     // Poco::Net::MessageHeader

namespace bdg
{
    namespace server
    {
        /**
         * Class which handles upload of files to the server.
         */
        class FilePartHandler : public Poco::Net::PartHandler
        {
        public:
            FilePartHandler() = default;
            ~FilePartHandler() = default;

            void handlePart(const Poco::Net::MessageHeader &header, std::istream &stream);
            inline const bdg::utils::FileUpload::FileUploadCollection &getUploadedFiles() const;

        private:
            /**
             * Collection with all files uploaded to the server.
             */
            bdg::utils::FileUpload::FileUploadCollection uploadedFiles;
        };
    }
}

/**
 * Returns collection with all files uploaded to the server.
 *
 * @return Files uploaded to the server.
 */
inline const bdg::utils::FileUpload::FileUploadCollection &bdg::server::FilePartHandler::getUploadedFiles() const
{
    return uploadedFiles;
}
