/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/server/loginprovider.h"          // LoginProvider
#include "utils/logger.h"                       // Logger

#include <Poco/Net/HTTPRequestHandler.h>        // Poco::Net::HTTPRequestHandler
#include <Poco/Net/HTTPServerRequest.h>         // Poco::Net::HTTPServerRequest
#include <Poco/Net/HTTPServerResponse.h>        // Poco::Net::HTTPServerResponse

namespace bdg
{
    namespace server
    {
        /**
         * Class which handles a login request to the server.
         */
        class LoginRequestHandler : public Poco::Net::HTTPRequestHandler
        {
        public:
            /**
             * Enum with actions that can be handled by this request handler.
             */
            enum Action
            {
                ACTION_LOGIN,
                ACTION_LOGOUT,
                ACTION_CHECK_LOGIN,
                ACTION_REQUIRES_LOGIN
            };

            LoginRequestHandler(Poco::Logger &logger, LoginProvider &loginProvider, Action action);
            virtual void handleRequest(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response) override;

        private:
            /**
             * Application logger.
             */
            Poco::Logger &logger;

            /**
             * Information about logged in users.
             */
            LoginProvider &loginProvider;

            /**
             * Which action should be performed.
             */
            Action action;
        };
    }
}
