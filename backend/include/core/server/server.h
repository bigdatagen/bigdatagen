/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/server/loginprovider.h"          // LoginProvider

#include <Poco/Util/ServerApplication.h>        // Poco::Util::ServerApplication
#include <Poco/Net/ServerSocket.h>              // Poco::Net::ServerSocket

#include <string>       // std::string
#include <vector>       // std::vector

namespace bdg
{
    namespace server
    {
        /**
         * Server which accepts HTTP requests and provides responses to the client.
         */
        class Server : public Poco::Util::ServerApplication
        {
        protected:
            void initialize(Poco::Util::Application &self);
            int main(const std::vector<std::string> &args);

        private:
            Poco::Net::ServerSocket *createSocket(const std::string &socketPluginName);

            /**
             * Information about logged in users.
             */
            LoginProvider loginProvider;
        };
    }
}
