/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#pragma once

#include "components/serverplugin.h"            // ServerPlugin
#include "core/server/fileparthandler.h"        // FilePartHandler
#include "core/server/loginprovider.h"          // LoginProvider
#include "utils/pluginmanager.h"                // PluginManager

#include <Poco/Net/HTTPRequestHandler.h>        // Poco::Net::HTTPRequestHandler
#include <Poco/Net/HTTPServerRequest.h>         // Poco::Net::HTTPServerRequest
#include <Poco/Net/HTTPServerResponse.h>        // Poco::Net::HTTPServerResponse

#include <sstream>      // std::stringstream

namespace bdg
{
    namespace server
    {
        /**
         * Class which handles one request to the server. Request is served when it
         * is created with the POST method and it contains the parameter "plugin" in
         * posted data. This handler guarantees to call the appropriate plugin with
         * a name given in the parameter "plugin" and pass it all the remaining parameters.
         */
        class PluginRequestHandler : public Poco::Net::HTTPRequestHandler
        {
        public:
            PluginRequestHandler(const bdg::utils::PluginManager<ServerPlugin> &pluginManager, Poco::Logger &logger, LoginProvider &loginProvider);
            virtual void handleRequest(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response) override;

        private:
            void handleDataRequest(const Poco::Net::HTTPServerRequest &request, std::stringstream &requestStream, std::string &pluginName, ServerPlugin::Parameters &pluginParameters) const;
            void handleFormRequest(const Poco::Net::HTTPServerRequest &request, std::stringstream &requestStream, FilePartHandler &filePartHandler, std::string &pluginName, ServerPlugin::Parameters &pluginParameters) const;
            
            /**
             * Manager of server plugins - loads them from the plugin directory when
             * the server is started.
             */
            const bdg::utils::PluginManager<ServerPlugin> &pluginManager;

            /**
             * Application logger.
             */
            Poco::Logger &logger;

            /**
             * Information about logged in users.
             */
            LoginProvider &loginProvider;
        };
    }
}
