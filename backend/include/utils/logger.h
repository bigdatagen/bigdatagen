/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Utils
 * @author   Big Data Generator Team
 */

#pragma once

#include "utils/common.h"       // Utils API common

#include <Poco/Logger.h>        // Poco::Logger

#include <string>   // std::string
#include <map>      // std::map

// We should always use bdg_debug macro, because with this one we can be sure
// that POCO_LOG_DEBUG macro is defined.
#if defined(_DEBUG) || defined(POCO_LOG_DEBUG)
    #define bdg_debug(logger, msg) if ((logger).debug()) (logger).debug(msg, __FILE__, __LINE__); else (void) 0
    #define bdg_trace(logger, msg) if ((logger).trace()) (logger).trace(msg, __FILE__, __LINE__); else (void) 0
#else
    #define bdg_debug(logger, msg)
    #define bdg_trace(logger, msg)
#endif

#define bdg_information(logger, msg)    poco_information(logger, msg)
#define bdg_notice(logger, msg)         poco_notice(logger, msg)
#define bdg_warning(logger, msg)        poco_warning(logger, msg)
#define bdg_error(logger, msg)          poco_error(logger, msg)
#define bdg_critical(logger, msg)       poco_critical(logger, msg)
#define bdg_fatal(logger, msg)          poco_fatal(logger, msg)

namespace bdg
{
    namespace utils
    {
        /**
         * Class providing various loggers - to console, to file or a logger
         * which just throws away all the logged content.
         */
        class BDG_UTIL_API Logger
        {
        public:
            ~Logger() = default;

            static Logger &getInstance();
            Poco::Logger &getConsoleLogger();
            Poco::Logger &getNullLogger();
            Poco::Logger &getServerLogger();
            Poco::Logger &getLogger(const int rank);
            Poco::Logger &getLogger(const std::string &loggerName, const std::string &fileName);
            Poco::Logger &getLoggerNoCache(const std::string &loggerName, const std::string &fileName, const int rank = -1);

            static void setJobId(const int newJobId);
            static const std::string FILE_CONSOLE;
            static const std::string FILE_NULL;

        private:
            typedef std::map<std::string, Poco::Logger*> LoggerMap;

            Logger() = default;
            Logger(const Logger &logger) = default;
            Logger &operator=(const Logger &logger) = default;

            /**
             * Instance of the logger which outputs log messages to the console.
             */
            Poco::Logger *consoleLogger;

            /**
             * Instance of the logger which throws away all log messages.
             */
            Poco::Logger *nullLogger;

            /**
             * Instance of the logger which is used to log in the server application.
             */
            Poco::Logger *serverLogger;

            /**
             * Instance of the logger which is used to log in the pipeline application.
             */
            Poco::Logger *mpiLogger;

            /**
             * Map between names of loggers and instances of loggers.
             */
            LoggerMap loggers;

            /**
             * Id of currently running job.
             */
            static int jobId;
        };
    }
}
