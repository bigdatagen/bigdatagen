/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Utils
 * @author   Big Data Generator Team
 */

#pragma once

#include "utils/common.h"       // Utils API common

#include <istream>      // std::istream
#include <string>       // std::string
#include <map>          // std::map
#include <memory>       // std::unique_ptr

namespace bdg
{
    namespace utils
    {
        // Forward declaration.
        class FileUploadImpl;
        
        /**
         * Class representing one file uploaded to the server over HTTP protocol.
         */
        class BDG_UTIL_API FileUpload
        {
        public:
            typedef std::map<std::string, std::unique_ptr<FileUpload>> FileUploadCollection;
            
            virtual ~FileUpload() = default;
            
            static std::unique_ptr<FileUpload> create(const std::string &name, const std::string &contentType, std::istream &inputStream);
            static std::unique_ptr<FileUpload> create(const std::string &name, const std::string &contentType);
            
            const std::string &getName() const;
            const std::string &getContentType() const;
            const std::string &getTemporaryFile() const;
            const size_t getSize() const;

        private:
            friend class FileUploadImpl;
            
            FileUpload() = default;
            const FileUploadImpl *impl() const;
            FileUploadImpl *impl();
        };
    }
}
