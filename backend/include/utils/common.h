/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Utils
 * @author   Big Data Generator Team
 */

#pragma once

#if defined(_WIN32) && defined(_DLL)
    #if defined(BDG_UTIL_API_EXPORTS)
        #define BDG_UTIL_API __declspec(dllexport)
    #else
        #define BDG_UTIL_API __declspec(dllimport)
    #endif
#else
    #define BDG_UTIL_API
#endif

#define UNREFERENCED_VARIABLE(p) (void)p;

// Enable the poco_debug_* and poco_trace_* macros
// even if the _DEBUG variable is not set.
// This allows the use of these macros in a release version.
#define POCO_LOG_DEBUG
