/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file convert.h
* @category Utils
* @author   Big Data Generator Team
*
*/

#pragma once

#include <string>

namespace bdg
{
    namespace utils
    {
        /**
        * Conversion function from string to given type.
        *
        * @param str string it converts from.
        */
        template<typename T>
        T fromString(const std::string& str);

        template<>
        inline int fromString<int>(const std::string& str)
        {
            return std::stoi(str);
        }

        template<>
        inline double fromString<double>(const std::string& str)
        {
            return std::stod(str);
        }

        template<>
        inline std::string fromString<std::string>(const std::string& str)
        {
            return str;
        }

        template<typename T>
        inline std::string toString(const T& val)
        {
            return std::to_string(val);
        }

        template<>
        inline std::string toString(const std::string& str)
        {
            return str;
        }
    }
}
