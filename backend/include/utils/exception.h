/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#pragma once

#include "core/pipeline/plugins/common.h"           // Plugin API common

#include <Poco/Exception.h>     // Poco::Exception

namespace bdg
{
    namespace utils
    {
        POCO_DECLARE_EXCEPTION(BDG_PLUGIN_API, MpiException, Poco::Exception)
        POCO_DECLARE_EXCEPTION(BDG_PLUGIN_API, TaskErrorException, Poco::Exception)
        POCO_DECLARE_EXCEPTION(BDG_PLUGIN_API, TaskFatalErrorException, Poco::Exception)
        POCO_DECLARE_EXCEPTION(BDG_PLUGIN_API, MissingPropertyException, Poco::Exception);
        POCO_DECLARE_EXCEPTION(BDG_PLUGIN_API, BadPropertyTypeException, Poco::Exception);
        POCO_DECLARE_EXCEPTION(BDG_PLUGIN_API, BadPropertyValueException, Poco::Exception);
        POCO_DECLARE_EXCEPTION(BDG_PLUGIN_API, BadPropertiesException, Poco::Exception);
        POCO_DECLARE_EXCEPTION(BDG_PLUGIN_API, BadBoundariesException, Poco::Exception);
        POCO_DECLARE_EXCEPTION(BDG_PLUGIN_API, BadTypeException, Poco::Exception);
        POCO_DECLARE_EXCEPTION(BDG_PLUGIN_API, InvalidJsonException, Poco::Exception);
        POCO_DECLARE_EXCEPTION(BDG_PLUGIN_API, RootNotArrayException, Poco::Exception);
        POCO_DECLARE_EXCEPTION(BDG_PLUGIN_API, NotObjectException, Poco::Exception);
        POCO_DECLARE_EXCEPTION(BDG_PLUGIN_API, NotArrayException, Poco::Exception);
        POCO_DECLARE_EXCEPTION(BDG_PLUGIN_API, TypeRedefinitionException, Poco::Exception);
        POCO_DECLARE_EXCEPTION(BDG_PLUGIN_API, TypeNotExistsException, Poco::Exception);
        POCO_DECLARE_EXCEPTION(BDG_PLUGIN_API, MissingRootException, Poco::Exception);
        POCO_DECLARE_EXCEPTION(BDG_PLUGIN_API, UnknownReferenceException, Poco::Exception);
        POCO_DECLARE_EXCEPTION(BDG_PLUGIN_API, BadReferenceException, Poco::Exception);
        POCO_DECLARE_EXCEPTION(BDG_PLUGIN_API, BadReferenceTypeException, Poco::Exception);
        POCO_DECLARE_EXCEPTION(BDG_PLUGIN_API, InvalidProbSumException, Poco::Exception);
        POCO_DECLARE_EXCEPTION(BDG_PLUGIN_API, TsInvalidStepException, Poco::Exception);
        POCO_DECLARE_EXCEPTION(BDG_PLUGIN_API, TsInvalidTimeException, Poco::Exception);
        POCO_DECLARE_EXCEPTION(BDG_PLUGIN_API, TsInvalidDateException, Poco::Exception);
        POCO_DECLARE_EXCEPTION(BDG_PLUGIN_API, UnsupportedOperationException, Poco::Exception);
        POCO_DECLARE_EXCEPTION(BDG_PLUGIN_API, UnsupportedDistributionException, Poco::Exception);
    }
}
