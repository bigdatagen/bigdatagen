/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Utils
 * @author   Big Data Generator Team
 */

#pragma once

#include "utils/common.h"               // Utils API common
#include "utils/logger.h"               // Logger

#include <Poco/ClassLoader.h>           // Poco::ClassLoader
#include <Poco/DirectoryIterator.h>     // Poco::DirectoryIterator
#include <Poco/Manifest.h>              // Poco::Manifest
#include <Poco/MetaObject.h>            // Poco::MetaObject
#include <Poco/String.h>                // Poco::String

#include <iostream>             // std::cout
#include <memory>               // std::unique_ptr
#include <string>               // std::string
#include <unordered_set>        // std::unordered_set
#include <vector>               // std::vector

namespace bdg
{
    namespace utils
    {
        /**
         * Class for plugin manager. Plugin manager is capable of loading all the
         * plugins (.dll or .so files) from a given directory. In the plugin file
         * must be exported class which ihnerits from class given to this class
         * as parameter T. Plugin class must have the same name as the file it is
         * contained in, otherwise it will not be available.
         *
         * Plugins can be exported either by POCO_EXPORT_CLASS or POCO_EXPORT_SINGLETON.
         * When exported by POCO_EXPORT_CLASS they can be accessed by getPlugin() method.
         * When exported by POCO_EXPORT_SINGLETON they can be accessed by getPluginSingleton()
         * method.
         */
        template<class T>
        class PluginManager
        {
        public:
            typedef typename Poco::ClassLoader<T> PluginLoader;

            PluginManager(Poco::Logger &logger, const std::string &pluginsPath);
            ~PluginManager() = default;

            inline const std::unordered_set<std::string> &getPluginNames() const;
            inline const std::unordered_set<std::string> &getSingletonPluginNames() const;

            bool hasPlugin(const std::string &pluginName) const;
            bool hasPluginSingleton(const std::string &pluginName) const;

            std::unique_ptr<T> getPlugin(const std::string &pluginName) const;
            T &getPluginSingleton(const std::string &pluginName) const;

            void unloadPlugins();

        private:
            void loadPlugins(const std::string &pluginsPath);

            std::string translatePluginName(const std::string &pluginName) const;
            std::string translateSingletonPluginName(const std::string &pluginName) const;
            std::string translatePluginName(const std::string &pluginName, const std::unordered_set<std::string> &pluginNames) const;

            /**
             * Loader of plugins.
             */
            PluginLoader loader;

            /**
             * Application logger.
             */
            Poco::Logger &logger;

            /**
             * Names of plugins which are not singletons.
             */
            std::unordered_set<std::string> plugins;

            /**
             * Names of singleton plugins.
             */
            std::unordered_set<std::string> singletonPlugins;
        };
    }
}

/**
 * Constructor of the plugin manager. Loads all the plugins from a given
 * directory which is relative to the current working directory. All the
 * plugins are immediately instantiated, see the class comment for the rules
 * for plugin creation.
 * 
 * @param logger Logger used to loged events in the manager.
 * @param pluginsPath Directory with plugins given relatively to cwd.
 */
template<class T>
bdg::utils::PluginManager<T>::PluginManager(Poco::Logger &logger, const std::string &pluginsPath) :
    logger(logger)
{
    loadPlugins(pluginsPath);
}

/**
 * Gets the names of all the loaded non-signleton plugins.
 * 
 * @return Names of loaded non-signleton plugins.
 */
template<class T>
inline const std::unordered_set<std::string> &bdg::utils::PluginManager<T>::getPluginNames() const
{
    return plugins;
}

/**
 * Gets the names of all the loaded signleton plugins.
 *
 * @return Names of loaded signleton plugins.
 */
template<class T>
inline const std::unordered_set<std::string> &bdg::utils::PluginManager<T>::getSingletonPluginNames() const
{
    return singletonPlugins;
}

/**
 * Checks whether the plugin with a given name is loaded and it is instantiable.
 *
 * @param pluginName Name of the plugin.
 *
 * @return Is the plugin with a given name loaded as instantiable?
 */
template<class T>
bool bdg::utils::PluginManager<T>::hasPlugin(const std::string &pluginName) const
{
    return plugins.find(translatePluginName(pluginName)) != plugins.end();
}

/**
 * Checks whether the plugin with a given name is loaded and it is singleton.
 *
 * @param pluginName Name of the plugin.
 *
 * @return Is the plugin with a given name loaded as singleton?
 */
template<class T>
bool bdg::utils::PluginManager<T>::hasPluginSingleton(const std::string &pluginName) const
{
    return singletonPlugins.find(translateSingletonPluginName(pluginName)) != singletonPlugins.end();
}

/**
 * Returns a new instance of the plugin with a given name. When no plugin with such a name
 * exists, an exception is thrown.
 *
 * @param pluginName Name of the plugin.
 *
 * @return Found plugin.
 * @throws Poco::NotFoundException Exception thrown in case that no plugin with a given name is found.
 * @throws Poco::InvalidAccessException Exception thrown in case that the plugin is a singleton.
 */
template<class T>
std::unique_ptr<T> bdg::utils::PluginManager<T>::getPlugin(const std::string &pluginName) const
{
    if (hasPlugin(pluginName))
    {
        std::unique_ptr<T> pluginPtr(loader.create(translatePluginName(pluginName)));
        return pluginPtr;
    }
    else
    {
        throw Poco::NotFoundException("Plugin [" + pluginName + "] is not loaded or instantiable");
    }
}

/**
 * Returns the singleton instance of the plugin with a given name. When no plugin with such a name
 * exists, an exception is thrown.
 *
 * @param pluginName Name of the plugin.
 *
 * @return Found plugin.
 * @throws Poco::NotFoundException Exception thrown in case that no plugin with a given name is found.
 * @throws Poco::InvalidAccessException Exception thrown in case that the plugin is not a singleton.
 */
template<class T>
T &bdg::utils::PluginManager<T>::getPluginSingleton(const std::string &pluginName) const
{
    if (hasPluginSingleton(pluginName))
    {
        return loader.instance(translateSingletonPluginName(pluginName));
    }
    else
    {
        throw Poco::NotFoundException("Plugin [" + pluginName + "] is not loaded or singleton");
    }
}

/**
 * Loads all the plugins from a given plugin directory which is relative
 * to the current working directory. All the plugins are immediately instantiated,
 * see the class comment for the rules for plugin creation.
 * 
 * @param pluginsPath Directory with plugins given relatively to cwd.
 */
template<class T>
void bdg::utils::PluginManager<T>::loadPlugins(const std::string &pluginsPath)
{
    bdg_debug(logger, "PluginManager: Loading plugins from [" + pluginsPath + "]");

    std::string pluginExt(Poco::replace(Poco::SharedLibrary::suffix(), "d.", "."));        // Contains .dll or .so
    Poco::Path dirPath(pluginsPath);
    std::string path(dirPath.toString());

    // Handle relative path.
    if (dirPath.isRelative())
    {
        path = Poco::Path::current() + path;
    }

    // Check whether the plugin directory exists.
    Poco::File dir(path);

    bdg_debug(logger, "PluginManager: Absolute path to plugins [" + dir.path() + "]");

    if (!dir.exists())
    {
        bdg_error(logger, "PluginManager ERROR: Plugin directory [" + path + "] does not exist!");
        std::cerr << "-- ERROR: Plugin directory [" << path << "] does not exist!" << std::endl;
        return;
    }

    // Loop over all the files in the current working directory and find there plugins
    // i.e. files ending with .dll or .so.
    Poco::DirectoryIterator dirIt(path);
    Poco::DirectoryIterator dirEnd;
    std::vector<std::string> libPaths;
    std::vector<std::string> libPathsToRemove;

    while (dirIt != dirEnd)
    {
        const std::string &fileName = dirIt.path().getFileName();

        if (dirIt->isFile() && fileName.find(pluginExt) != std::string::npos && fileName.find("Poco") == std::string::npos && fileName.find("Bdg") == std::string::npos)
        {
            libPaths.emplace_back(dirIt.path().toString());
        }

        ++dirIt;
    }

    // Get all the libraries that exist in both - debug and release mode.
    for (const std::string &path : libPaths)
    {
        const std::string debugLibPath = Poco::replace(path, pluginExt, "d" + pluginExt);

        if (std::find(libPaths.begin(), libPaths.end(), debugLibPath) != libPaths.end())
        {
#if defined(_DEBUG)
            libPathsToRemove.emplace_back(path);
#else
            libPathsToRemove.emplace_back(debugLibPath);
#endif
        }
    }
    
    // Remove debug libraries from loaded libraries when we are in release mode and
    // remove release libraries from loaded libraries when we are in debug mode.
    for (const std::string &path : libPathsToRemove)
    {
        libPaths.erase(std::find(libPaths.begin(), libPaths.end(), path));
    }

    // Load all the relevant plugins.
    for (const std::string &path : libPaths)
    {
        std::string libraryName = Poco::replace(Poco::Path(path).getFileName(), pluginExt.c_str(), "");
        
        bdg_debug(logger, "PluginManager: Loading library [" + libraryName + "]");

        try
        {
            // Load the library file with the plugin.
            loader.loadLibrary(path);
                
            // Loop over all the loaded plugins and find the plugin with the right name.
            const typename PluginLoader::Manif &manifest = loader.manifestFor(path);

            for (typename PluginLoader::Manif::Iterator it = manifest.begin(); it != manifest.end(); ++it)
            {
                std::string pluginName = it->name();
                bdg_debug(logger, "PluginManager: Library contains plugin [" + pluginName + "]");

                // Store the plugin as singleton or as instantiable.
                if (it->canCreate())
                {
                    plugins.insert(pluginName);
                }
                else
                {
                    singletonPlugins.insert(pluginName);
                }

                bdg_debug(logger, "PluginManager: Plugin [" + pluginName + "] loaded");
            }
        }
        catch (Poco::LibraryLoadException &e)
        {
            bdg_error(logger, "PluginManager ERROR: Library [" + libraryName + "] could not be loaded!");

            // This should be logged at least in debug mode.
            bdg_debug(logger, e.displayText());
            UNREFERENCED_VARIABLE(e);

            std::cerr << "-- ERROR: Library [" + libraryName + "] could not be loaded! Possible reasons:" << std::endl;
            std::cerr << "  - Do you have the latest version of your library in the plugin include directory?" << std::endl;
            std::cerr << "  - Do you have all the necessary POCO libraries next to your plugin .dll or .so files?" << std::endl;
            std::cerr << "  - Does your plugin implement the base class abstract methods in the right way?" << std::endl;
        }
        catch (Poco::NotFoundException&)
        {
            bdg_error(logger, "PluginManager ERROR: Plugin manifest could not be loaded!");
            std::cerr << "-- ERROR: Plugin manifest could not be loaded!" << std::endl;
        }
    }

    bdg_debug(logger, "PluginManager: Plugins loaded");
}

/**
 * Unloads all the loaded plugins.
 */
template<class T>
void bdg::utils::PluginManager<T>::unloadPlugins()
{
    bdg_debug(logger, "PluginManager: Unloading plugins");

    // Unload all the plugins from the memory.
    std::vector<std::string> allLibraries;

    for (typename PluginLoader::Iterator it = loader.begin(); it != loader.end(); ++it)
    {
        allLibraries.push_back(it->first);
    }

    for (std::vector<std::string>::iterator it = allLibraries.begin(); it != allLibraries.end(); ++it)
    {
        try
        {
            loader.unloadLibrary(*it);
        }
        catch (Poco::NotFoundException&)
        {
            bdg_error(logger, "PluginManager ERROR: Plugin could not be unloaded [" + *it + "]!");
            std::cerr << "-- ERROR: Plugin could not be unloaded [" << *it << "]!" << std::endl;
        }
    }

    // Plugins are not loaded any more.
    plugins.clear();
    singletonPlugins.clear();

    bdg_debug(logger, "PluginManager: Plugins unloaded");
}

/**
 * Translates the name of the plugin to its original name, it is a case-insensitive method.
 * 
 * @param pluginName Name of the plugin.
 *
 * @return Original name of the plugin.
 */
template<class T>
std::string bdg::utils::PluginManager<T>::translatePluginName(const std::string &pluginName) const
{
    return translatePluginName(pluginName, plugins);
}

/**
 * Translates the name of the singleton plugin to its original name, it is a case-insensitive method.
 * 
 * @param pluginName Name of the plugin.
 *
 * @return Original name of the plugin.
 */
template<class T>
std::string bdg::utils::PluginManager<T>::translateSingletonPluginName(const std::string &pluginName) const
{
    return translatePluginName(pluginName, singletonPlugins);
}

/**
 * Translates the name of the plugin to its original name, it is a case-insensitive method.
 * 
 * @param pluginName Name of the plugin.
 * @param pluginNames Collection of original plugin names.
 *
 * @return Original name of the plugin.
 */
template<class T>
std::string bdg::utils::PluginManager<T>::translatePluginName(const std::string &pluginName, const std::unordered_set<std::string> &pluginNames) const
{
    std::string pluginNameLower = Poco::toLower(pluginName);

    for (std::unordered_set<std::string>::const_iterator it = pluginNames.begin(); it != pluginNames.end(); ++it)
    {
        if (Poco::toLower(*it) == pluginNameLower)
        {
            return *it;
        }
    }

    return pluginName;
}
