/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Utils
 * @author   Big Data Generator Team
 */

#pragma once

#include "utils/common.h"                       // Utils API common

#include <Poco/Util/IniFileConfiguration.h>     // Poco::Util::IniFileConfiguration
#include <Poco/AutoPtr.h>                       // Poco::AutoPtr

#include <string>           // std::string

namespace bdg
{
    namespace utils
    {
        /**
         * Singleton class reprezenting application configuration loaded from the
         * file config.ini. When searching for configuration file, a current directory
         * is first searched, then parent directory and so on up to the root.
         */
        class BDG_UTIL_API Config
        {
        public:
            ~Config() = default;

            static Config &getInstance();

            inline std::string getMpiProgram() const;
            inline std::string getPipelineProgram() const;
            inline std::string getPipelinePluginManagerProgram() const;
            inline std::string getPipelinePluginsPath(const std::string &defaultValue = "") const;
            inline size_t getPipelineCheckpointPeriodSeconds() const;
            inline bool pipelineCleanUp() const;
            inline bool pipelineContinuousCleanUp() const;
            inline bool pipelineUseTaskQueue() const;

            inline std::string getOutputStoragePath() const;
            inline std::string getOutputStoragePathLocal() const;
            inline size_t getOutputBufferSizeBytes() const;
            inline std::string getCheckpointerStoragePath() const;
            inline std::string getCheckpointerFileExtension() const;

            inline std::string getLogStoragePath() const;
            inline std::string getLogLevel() const;

            inline unsigned short getServerPort() const;
            inline std::string getServerSocket() const;
            inline std::string getServerClientPath() const;
            inline std::string getServerPluginsPath(const std::string &defaultValue = "") const;
            inline std::string getServerSocketPluginsPath(const std::string &defaultValue = "") const;
            inline std::string getServerUploadsPath() const;

            inline std::string getServerHttpsPrivateKeyFile() const;
            inline std::string getServerHttpsCertificateFile() const;
            inline std::string getServerHttpsCaLocation() const;
            inline std::string getServerHttpsVerificationMode() const;
            inline bool getServerHttpsLoadDefaultCAs() const;

            inline bool useServerLogin() const;
            inline std::string getServerLoginUsername() const;
            inline std::string getServerLoginPassword() const;

            inline std::string getInfererPluginsPath(const std::string &defaultValue = "") const;

        private:
            Config();
            Config(const Config &config) = default;
            Config &operator=(const Config &config) = default;
            void checkConfig() const;

            /**
             * Loaded values of configuration directives from the config.ini file.
             */
            Poco::AutoPtr<Poco::Util::IniFileConfiguration> config;
        };
    }
}

/**
 * Returns command to execute MPI, it is needed in the PipelineLauncherApp – it launches the Pipeline
 * application over MPI.
 *
 * @return Command to execute MPI.
 */
inline std::string bdg::utils::Config::getMpiProgram() const
{
    return config->getString("Pipeline.MpiProgram", "");
}

/**
 * Returns path to the Pipeline program. On Windows it is path to the executable called "Pipeline.exe",
 * on Unix to binary called "Pipeline".
 *
 * @return Path to the Pipeline program.
 */
inline std::string bdg::utils::Config::getPipelineProgram() const
{
    return config->getString("Pipeline.Program", "");
}

/**
 * Returns path to the “PipelinePluginManagerApp.exe” executable.
 *
 * @return Path to the “PipelinePluginManagerApp.exe” executable.
 */
inline std::string bdg::utils::Config::getPipelinePluginManagerProgram() const
{
    return config->getString("Pipeline.PluginManagerProgram", "");
}

/**
 * Returns path to the directory where pipeline plugins are located. When pipeline application is
 * launched it automatically loads all the plugins from this directory.
 *
 * @param defaultValue Default value used when setting is not specified.
 *
 * @return Path to the directory where pipeline plugins are located.
 */
inline std::string bdg::utils::Config::getPipelinePluginsPath(const std::string &defaultValue) const
{
    return config->getString("Pipeline.PluginsPath", defaultValue);
}

/**
 * Returns how often should scheduler store checkpoints about progress, efault value is 10, i.e.
 * every 10 seconds.
 *
 * @return How often should scheduler store checkpoints (in seconds).
 */
inline size_t bdg::utils::Config::getPipelineCheckpointPeriodSeconds() const
{
    return config->getInt("Pipeline.CheckpointPeriodSeconds", 10);
}

/**
 * When this setting is set to true, all the generated temporary files will be deleted when a job is
 * finished. I.e. all the files from all the stages of the pipeline except the last one will be deleted.
 *
 * @return Should the temporary files be deleted when a job is finished?
 */
inline bool bdg::utils::Config::pipelineCleanUp() const
{
    return config->getBool("Pipeline.CleanUp", true);
}

/**
 * When this setting is set to true, temporary files are automatically removed - it does it while
 * the job is running. 
 *
 * @return Should continuous clean up algorithm be used?
 */
inline bool bdg::utils::Config::pipelineContinuousCleanUp() const
{
    return config->getBool("Pipeline.ContinuousCleanUp", false);
}

/**
 * When this setting is set to true, queue for tasks is used by task scheduler. We recommend leaving
 * this setting off, because it does not improve performance. 
 *
 * @return Should the queue for tasks be used?
 */
inline bool bdg::utils::Config::pipelineUseTaskQueue() const
{
    return config->getBool("Pipeline.UseTaskQueue", false);
}

/**
 * Returns path to the directory where all the generated files from all the stages of pipeline are stored.
 *
 * @return Path to the directory where all the generated files are stored.
 */
inline std::string bdg::utils::Config::getOutputStoragePath() const
{
    return config->getString("Output.StoragePath", "");
}

/**
 * When this setting is set to some path, generator plugins use it as a temporary storage, preferable
 * in local file system. Then generated files are asynchronously copied to the shared file system. This
 * is an experimental feature
 *
 * @return Path to the temporary directory in the local file system.
 */
inline std::string bdg::utils::Config::getOutputStoragePathLocal() const
{
    return config->getString("Output.StoragePathLocal", "");
}

/**
 * Returns size of the output buffer used in generator plugins in bytes. Default value is 4096, i.e. 4 kB.
 *
 * @return Size of the output buffer used in generator plugins in bytes.
 */
inline size_t bdg::utils::Config::getOutputBufferSizeBytes() const
{
    return config->getInt("Output.BufferSizeBytes", 4096);
}

/**
 * Returns path to the directory where checkpoints are stored. We have three types of checkpoints: pipeline
 * checkpoints (job configuration files), monitoring checkpoints and scheduler checkpoints (used for
 * error recovery)
 *
 * @return Path to the directory where checkpoints are stored.
 */
inline std::string bdg::utils::Config::getCheckpointerStoragePath() const
{
    return config->getString("Checkpointer.StoragePath", "");
}

/**
 * Returns extension of files with checkpoints, default value is “chp”.
 *
 * @return Extension of files with checkpoints.
 */
inline std::string bdg::utils::Config::getCheckpointerFileExtension() const
{
    return config->getString("Checkpointer.FileExtension", "chp");
}

/**
 * Returns path of the directory where logs of each job are stored.
 *
 * @return Path of the directory where logs are stored.
 */
inline std::string bdg::utils::Config::getLogStoragePath() const
{
    return config->getString("Logger.StoragePath", "");
}

/**
 * Returns set level of logging. There are 8 available levels: fatal, critical, error, warning,
 * notice, information, debug and trace.
 *
 * @return Level of logging.
 */
inline std::string bdg::utils::Config::getLogLevel() const
{
    return config->getString("Logger.Level", "");
}

/**
 * Returns port on which the server runs, default port is 9980.
 *
 * @return Port on which the server runs.
 */
inline unsigned short bdg::utils::Config::getServerPort() const
{
    return (unsigned short)config->getInt("Server.Port", 9980);
}

/**
 * Returns which socket plugin is used to create a listening socket on the server. When this
 * setting is set to value "ServerSslPlugin", server runs in ssl mode; otherwise it runs in http more.
 *
 * @return Which socket plugin is used to create a listening socket on the server.
 */
inline std::string bdg::utils::Config::getServerSocket() const
{
    return config->getString("Server.Socket", "default");
}

/**
 * Returns path to the "frontend/app" directory where the whole frontend is located. 
 *
 * @return Path to the "frontend/app" directory.
 */
inline std::string bdg::utils::Config::getServerClientPath() const
{
    return config->getString("Server.ClientPath", "");
}

/**
 * Returns path to the directory where server plugins are located. All of those plugins are loaded when
 * server is launched and client is then able to call them.
 *
 * @param defaultValue Default value of the setting used when it is not set in the configuration file.
 *
 * @return Path to the directory where server plugins are located.
 */
inline std::string bdg::utils::Config::getServerPluginsPath(const std::string &defaultValue) const
{
    return config->getString("Server.PluginsPath", defaultValue);
}

/**
 * Returns path to the directory where server socket plugins are located.
 *
 * @param defaultValue Default value of the setting used when it is not set in the configuration file.
 *
 * @return Path to the directory where server socket plugins are located.
 */
inline std::string bdg::utils::Config::getServerSocketPluginsPath(const std::string &defaultValue) const
{
    return config->getString("Server.SocketPluginsPath", defaultValue);
}

/**
 * Returns path to the directory where temporary files uploaded from the frontend are stored.
 *
 * @return Path to the directory where uploaded files are stored.
 */
inline std::string bdg::utils::Config::getServerUploadsPath() const
{
    return config->getString("Server.UploadsPath", "");
}

/**
 * Returns path to the private key file, it should have a .key extension.
 *
 * @return Path to the private key file.
 */
inline std::string bdg::utils::Config::getServerHttpsPrivateKeyFile() const
{
    return config->getString("Https.PrivateKeyFile", "");
}

/**
 * Returns path to the certificate file, it should have a .crt extension. See
 * http://pocoproject.org/docs/Poco.Net.Context.html
 *
 * @return Path to the certificate file.
 */
inline std::string bdg::utils::Config::getServerHttpsCertificateFile() const
{
    return config->getString("Https.CertificateFile", "");
}

/**
 * Returns path to the certificate. It is not necessary to use this setting.
 *
 * @return Path to the certificate.
 */
inline std::string bdg::utils::Config::getServerHttpsCaLocation() const
{
    return config->getString("Https.CaLocation", "");
}

/**
 * Returns mode of certificate verification, default value is "relaxed". Possible values
 * are "relaxed", "once", "strict" and "none".
 *
 * @return Mode of certificate verification.
 */
inline std::string bdg::utils::Config::getServerHttpsVerificationMode() const
{
    return config->getString("Https.VerificationMode", "relaxed");
}

/**
 * Returns whether to load default CAs. It is not necessary to use this setting.
 *
 * @return Load default CAs?
 */
inline bool bdg::utils::Config::getServerHttpsLoadDefaultCAs() const
{
    return config->getBool("Https.LoadDefaultCAs", false);
}

/**
 * When this setting is set to true, a login is required on the frontend.
 *
 * @return Is the login required on frontend?
 */
inline bool bdg::utils::Config::useServerLogin() const
{
    return config->getBool("Login.Use", false);
}

/**
 * Returns username used to log in the user on the frontend.
 *
 * @return Username used to log in the user on the frontend.
 */
inline std::string bdg::utils::Config::getServerLoginUsername() const
{
    return config->getString("Login.Username", "");
}

/**
 * Returns password used to log in the user on the frontend.
 *
 * @return Password used to log in the user on the frontend.
 */
inline std::string bdg::utils::Config::getServerLoginPassword() const
{
    return config->getString("Login.Password", "");
}

/**
 * Returns path to the directory where inferrer plugins are located. These plugins are used
 * to infer a schema for generator from a set of uploaded files.
 *
 * @return Path to the directory where inferrer plugins are located.
 */
inline std::string bdg::utils::Config::getInfererPluginsPath(const std::string &defaultValue) const
{
    return config->getString("SchemaInferer.PluginsPath", defaultValue);
}
