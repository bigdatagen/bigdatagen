/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Utils
 * @author   Big Data Generator Team
 */

#pragma once

#include "utils/common.h"           // Utils API common

#include <string>       // std::string
#include <vector>       // std::vector

namespace bdg
{
    namespace utils
    {
        /**
         * Class with utilities associated with a command line.
         */
        class BDG_UTIL_API CommandLine
        {
        public:
            static std::string parseCommandLineArguments(const std::string &cmd, std::vector<std::string> &args);
        };
    }
}
