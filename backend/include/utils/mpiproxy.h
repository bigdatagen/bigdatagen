/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Utils
 * @author   Big Data Generator Team
 * @brief    Class can be used as wrapper around MPI's raw C API.
 */

#pragma once

#include "core/pipeline/plugins/common.h"       // Plugin API common

#include <string>       // std::string
#include <vector>       // std::vector

#define MPI_EMPTY_STRING ""

namespace bdg
{
    namespace utils
    {
        /**
         * Wrapper for MPI.
         */
        class BDG_PLUGIN_API MPIProxy
        {
        public:
            MPIProxy() = default;
            ~MPIProxy();

            inline bool isMaster() const;
            inline size_t getRank() const;
            inline size_t getMasterRank() const;
            inline size_t getSize() const;

            void sendMessageToMaster(const std::string &str, const int tag) const;
            void sendMessageToWorker(const std::string &str, const int rank, const int tag) const;
            bool canReceiveMessage(int &rank) const;
            std::string receiveMessageFromMaster(int &tag) const;
            std::string receiveMessageFromWorker(const int rank, int &tag) const;

            void init(int argc, char **argv);
            void barrier() const;
            void broadcastInt(int &data) const;
            void allGatherInts(int data, int *dataBuffer) const;
            void detectNodes();
            bool areOnSameNode(const int firstRank, const int secondRank) const;
            void finalize() const;
            
            static const int MASTER_RANK = 0;
            static const int NO_TAG = 0;

        private:
            /**
             * Number of running MPI threads.
             */
            int size;

            /**
             * Rank of the currently running MPI thread.
             */
            int rank;

            /**
             * Map between ranks and physical nodes on the cluster.
             */
            int *nodeRankMap;
        };
    }
}

/**
 * Returns whether currently running MPI thread is the master thread. Master thread
 * has rank 0.
 *
 * @return Do we run in master thread?
 */
inline bool bdg::utils::MPIProxy::isMaster() const
{
    return rank == MASTER_RANK;
}

/**
 * Returns rank of the currently running MPI thread.
 *
 * @return Rank of the currently running MPI thread.
 */
inline size_t bdg::utils::MPIProxy::getRank() const
{
    return static_cast<size_t>(rank);
}

/**
 * Returns rank of the master thread.
 *
 * @return Rank of the master thread.
 */
inline size_t bdg::utils::MPIProxy::getMasterRank() const
{
    return static_cast<size_t>(MASTER_RANK);
}

/**
 * Returns number of running MPI threads.
 *
 * @return Number of running MPI threads.
 */
inline size_t bdg::utils::MPIProxy::getSize() const
{
    return static_cast<size_t>(size);
}
