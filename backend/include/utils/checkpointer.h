/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 * 
 * @category Utils
 * @author   Big Data Generator Team
 * @brief    Class for creating checkpoints.
 * @note     Is not multiprocess safe!
 */

#pragma once

#include "utils/common.h"       // Utils API common
#include "utils/config.h"       // Config

#include <fstream>      // std::fstream
#include <string>       // std::string
#include <vector>       // std::vector
#include <random>       // std::random

namespace bdg
{
    namespace utils
    {
        /**
         * Base class for checkpointers - classes able to store checkpoints.
         */
        class BDG_UTIL_API Checkpointer
        {
        public:
            Checkpointer();
            explicit Checkpointer(const int checkpointId);
            virtual ~Checkpointer() = default;

            inline int getId() const;
            inline const std::string getPathToFile() const;
            virtual inline std::string getClassId() const;

            void createStorageDir() const;

            static inline const std::string getPathToFile(const std::string &classId, const int checkpointId);

        protected:
            static inline int initId();

            void writeToFile(const std::string &stringToWrite) const;
            std::string readFromFile(size_t sizeToRead) const;

            /**
             * Unique id of the checkpoint.
             */
            int checkpointId;
        };
    }
}

/**
 * Returns unique id of the checkpoint.
 *
 * @return Unique id of the checkpoint.
 */
inline int bdg::utils::Checkpointer::getId() const
{
    return checkpointId;
}

/**
 * Returns path to the checkpoint file.
 *
 * @return Path to the checkpoint file.
 */
inline const std::string bdg::utils::Checkpointer::getPathToFile() const
{
    return getPathToFile(getClassId(), checkpointId);
}

/**
 * Returns path to the checkpoint file.
 *
 * @param classId Name of the checkpointer.
 * @param id Id of the checkpoint.
 *
 * @return Path to the checkpoint file.
 */
inline const std::string bdg::utils::Checkpointer::getPathToFile(const std::string &classId, const int checkpointId)
{
    return Config::getInstance().getCheckpointerStoragePath() + "/checkpointer-" + classId + "-" + std::to_string(checkpointId) + "." + Config::getInstance().getCheckpointerFileExtension();
}

/**
 * Returns name of the checkpointer. It appears in the names of checkpoint files.
 *
 * @return Name of the checkpointer.
 */
inline std::string bdg::utils::Checkpointer::getClassId() const
{
    return "default";
}

/**
 * Assigned a randomly selected id of the checkpoint to the checkpointer.
 *
 * @return Randomly selected id of the checkpoint.
 */
inline int bdg::utils::Checkpointer::initId()
{
    std::random_device random;
    std::default_random_engine engine(random());
    std::uniform_int_distribution<int> uniformDist;

    return uniformDist(engine);
}
