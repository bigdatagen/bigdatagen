/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Sockets
 * @author   Big Data Generator Team
 */

#pragma once

#include "sockets/socketplugin.h"       // SocketPlugin

namespace bdg
{
    namespace sockets
    {
        /**
         * Server plugin which allows server to listen on SSL socket. See
         * https://schneide.wordpress.com/2011/02/07/ssl-with-poco/ to read
         * more information about POCO and its SSL support.
         */
        class ServerSslPlugin : public server::SocketPlugin
        {
        public:
            ServerSslPlugin();

            virtual Poco::Net::ServerSocket *createSocket() override;
        };
    }
}
