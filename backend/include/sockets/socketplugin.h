/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#pragma once

#include <Poco/Net/ServerSocket.h>          // Poco::Net::ServerSocket

namespace bdg
{
    namespace server
    {
        /**
         * Base class for all the socket plugins which are loadable by POCO class loader.
         */
        class SocketPlugin
        {
        public:
            /**
             * Plugin constructor.
             */
            SocketPlugin() = default;

            /**
             * Virtual destructor - right destructor must be called when the plugin is destroyed.
             */
            virtual ~SocketPlugin() = default;
            
            /**
             * Creates the listening socket used by server.
             *
             * @return Created socket.
             */
            virtual Poco::Net::ServerSocket *createSocket() = 0;
        };
    }
}
