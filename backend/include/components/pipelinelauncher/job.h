/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Components
 * @author   Big Data Generator Team
 */

#pragma once

#include <Poco/JSON/Object.h>           // Poco::JSON::Object
#include <Poco/Thread.h>                // Poco::Thread
#include <Poco/RunnableAdapter.h>       // Poco::RunnableAdapter
#include <Poco/Mutex.h>                 // Poco::Mutex
#include <Poco/Event.h>                 // Poco::Event

namespace bdg
{
    namespace components
    {
        /**
         * Class representing one running job.
         */
        class Job
        {
        public:
            /**
             * Possible states of a job - running, paused or finished.
             */
            enum State
            {
                STATE_RUNNING,
                STATE_PAUSED,
                STATE_FINISHED
            };

            explicit Job(const int jobId, const size_t clusterSize, const bool copyOutput);
            ~Job() = default;

            inline int getId() const;
            inline State getState() const;

            std::string getStateStr() const;
            Poco::JSON::Object getJobState();

            const std::string play();
            const std::string pause();
            const std::string stop();
            void wait();

        private:
            void runPipeline();
            void loadState(Poco::JSON::Object &json, const std::vector<std::string> &errors);
            bool canRelaunch(std::string &lastCheckpoint, int &numOfRelaunchesWithoutCheckpointChange) const;
            std::string createCmd(const std::string &pipelineProgram) const;

            bool createStopFile() const;
            bool removeStopFile() const;

            /**
             * Id of the job.
             */
            const int jobId;

            /**
             * Number of MPI threads used to generate files in the job.
             */
            const size_t clusterSize;

            /**
             * When set to true, the whole output of pipeline program is copied to std::cout.
             * Errors are copied to std::cerr.
             */
            const bool copyOutput;

            /**
             * Number of re-launches of the pipeline program due to errors or because of the
             * pause/play commands.
             */
            int relaunches = -1;

            /**
             * Current state of the job.
             */
            State state;

            /**
             * Runnable adapter for the job thread.
             */
            Poco::RunnableAdapter<Job> runnable;

            /**
             * A job is running in a separate thread.
             */
            Poco::Thread thread;

            /**
             * Result of the job run.
             */
            Poco::JSON::Object result;

            /**
             * Mutex for operating with the job state - it can be accessed from multiple threads.
             */
            Poco::Mutex stateMutex;

            /**
             * Mutex for operating with the job result - it can be accessed from multiple threads.
             */
            Poco::Mutex resultMutex;

            /**
             * Event signalling that the running state of the job has been reached.
             */
            Poco::Event runningStateReachedEvent;

            /**
             * Event signalling that the paused state of the job has been reached.
             */
            Poco::Event pausedStateReachedEvent;

            /**
             * Event signalling that the finished state of the job has been reached.
             */
            Poco::Event finishedStateReachedEvent;

            /**
             * Event signalling that the job is able to continue in its run. It is signalled
             * when a "play" command is executed to the thread where the job is waiting in
             * the "paused" state.
             */
            Poco::Event continueRunningEvent;

            /**
             * How many times is the job automatically re-launched without any progress before
             * it is considered as permanently failed.
             */
            static const int MAX_NUM_OF_RELAUNCHES_WITHOUT_CHECKPOINT_CHANGE;

            /**
             * Maximum number of milliseconds which we wait for an event to occur. When it does
             * not occur within this timeout, the "pause", "play" or "stop" command is considered
             * as failed.
             */
            static const int MAX_EVENT_WAIT_MS;

            /**
             * Return value from the pipeline program which signals that it terminated successfully
             * and the job can be considered as finisged.
             */
            static const int PROGRAM_RETURN_VALUE_OK;

            /**
             * Return value from the pipeline program which signals that there was an error and
             * the job should be re-launched.
             */
            static const int PROGRAM_RETURN_VALUE_ERROR;

            /**
             * Return value from the pipeline program which signals that the pipeline program could
             * not run, because the command for its launch is probably configured in a wrong way.
             */
            static const int PROGRAM_RETURN_VALUE_WRONG_PROGRAM;
        };
    }
}

/**
 * Returns id of the job.
 *
 * @return Id of the job.
 */
inline int bdg::components::Job::getId() const
{
    return jobId;
}

/**
 * Returns the state of the job.
 *
 * @return State of the job.
 */
inline bdg::components::Job::State bdg::components::Job::getState() const
{
    return state;
}
