/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Components
 * @author   Big Data Generator Team
 */

#pragma once

#include "components/bdgapi.h"                      // Plugin API
#include "components/pipelinelauncher/job.h"        // Job

#include <Poco/Mutex.h>         // Poco::Mutex

namespace bdg
{
    namespace components
    {
        /**
         * Server plugin which is able to run, pause, re-launch and stop jobs. This plugin
         * is exported as singleton, so it is able to store its state - it knows about all
         * the launched and finished jobs.
         */
        class PipelineLauncher : public server::ServerPlugin
        {
        public:
            PipelineLauncher() = default;
            virtual ~PipelineLauncher() = default;

            virtual std::string run(const server::ServerPlugin::Parameters &parameters, const bdg::utils::FileUpload::FileUploadCollection &uploadedFiles) override;

        private:
            typedef std::map<int, std::unique_ptr<Job>> JobsCollection;

            std::string processCommandLaunch(const bdg::server::ServerPlugin::Parameters &parameters, const Poco::JSON::Object::Ptr configurationJson, const std::string &configuration);
            std::string processCommandStatus(const Poco::JSON::Object::Ptr configurationJson);
            std::string processCommandPlay(const Poco::JSON::Object::Ptr configurationJson);
            std::string processCommandPause(const Poco::JSON::Object::Ptr configurationJson);
            std::string processCommandStop(const Poco::JSON::Object::Ptr configurationJson);
            std::string processCommandListJobs(const Poco::JSON::Object::Ptr configurationJson);

            std::string loadParametersFromFile(const std::string &path, int &jobId, size_t &clusterSize, const bool waitForResponse);
            std::string loadParametersFromPipelineConfiguration(const Poco::JSON::Object::Ptr configurationJson, const std::string &configuration, int &jobId, size_t &clusterSize, const bool waitForResponse);

            size_t getClusterSizeFromConfiguration(const Poco::JSON::Object::Ptr configurationJson) const;
            Job *getJob(const Poco::JSON::Object::Ptr configurationJson);

            /**
             * Collection of running, paused and finished jobs.
             */
            JobsCollection jobs;

            /**
             * Lock for the jobs collection - it is accessed from multiple threads.
             */
            Poco::Mutex jobsMutex;

            /**
             * Name of the parameter with a job configuration.
             */
            static const std::string PARAM_PIPELINE_CONFIGURATION;
        };
    }
}
