/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Components
 * @author   Big Data Generator Team
 */

#pragma once

#include "components/bdgapi.h"          // Plugin API
#include "utils/pluginmanager.h"        // PluginManager
#include "infererplugin.h"              // InfererPlugin

#include <unordered_set>    // std::unordered_set

namespace bdg
{
    namespace components
    {
        /**
         * Plugin managing all the available inferer classes.
         */
        class SchemaInferer : public server::ServerPlugin
        {
        public:
            SchemaInferer();
            SchemaInferer(Poco::Logger &logger);
            virtual ~SchemaInferer() = default;

            virtual std::string run(const server::ServerPlugin::Parameters &parameters, const bdg::utils::FileUpload::FileUploadCollection &uploadedFiles) override;

        private:
            inline Poco::JSON::Array::Ptr listPlugins() const;

            void runInferer(std::string &name, const Poco::JSON::Object::Ptr &configObject, const bdg::utils::FileUpload::FileUploadCollection &uploadedFiles, std::stringstream &os);

            /**
             * Application server logger.
             */
            Poco::Logger &logger;

            /**
             * Manages available inferer plugins.
             */
            const bdg::utils::PluginManager<bdg::components::schemaInferer::InfererPlugin> pluginManager;
        };
    }
}

/**
 * Lists all currently loaded inferer plugins.
 *
 * @return List of all currently loaded inferer plugins.
 */
inline Poco::JSON::Array::Ptr bdg::components::SchemaInferer::listPlugins() const
{
    Poco::JSON::Array::Ptr plugins = new Poco::JSON::Array;

    for(const auto &pluginName : pluginManager.getPluginNames())
    {
        plugins->add(pluginManager.getPlugin(pluginName)->getPluginInfo());
    }

    return plugins;
}
