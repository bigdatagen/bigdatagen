/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#pragma once

#include "components/schemainferer/infererplugin.h"     // InfererPlugin

#include <Poco/JSON/Object.h>       // Poco::JSON::Object::Ptr
#include <Poco/JSON/Array.h>        // Poco::JSON::Array::Ptr
#include <Poco/Dynamic/Var.h>       // Poco::Dynamic::Var

#include <iostream>         // std::istream, std::ostream
#include <string>           // std::string
#include <unordered_set>    // std::unordered_set

namespace bdg
{
    namespace components
    {
        namespace schemaInferer
        {
            /**
             * Implementation of inferrer plugin for JSON files.
             */
            class JsonInferer : public InfererPlugin
            {
            public:
                JsonInferer();

                void inferSchema(std::ostream &os, const Poco::JSON::Object::Ptr &params) override;

            private:
                Poco::JSON::Object::Ptr jsonToSchema(std::istream &ifs);

                Poco::JSON::Array::Ptr getObjectProperties(const Poco::JSON::Object::Ptr &object);
                Poco::JSON::Object::Ptr getArrayProperties(const Poco::JSON::Array::Ptr &array);

                Poco::JSON::Object::Ptr getSimpleTypeSchema(const Poco::Dynamic::Var &val, const std::string &name);
                Poco::JSON::Object::Ptr getObjectSchema(const Poco::JSON::Object::Ptr &obj, const std::string &name);
                Poco::JSON::Object::Ptr getArraySchema(const Poco::JSON::Array::Ptr &arr, const std::string &name);

                void mergeRoot(Poco::JSON::Object::Ptr &root1, Poco::JSON::Object::Ptr &root2);
                void merge(Poco::JSON::Object::Ptr &obj1, Poco::JSON::Object::Ptr &obj2);
                void mergeObjects(Poco::JSON::Object::Ptr &obj1, Poco::JSON::Object::Ptr &obj2);
                void mergeArrays(Poco::JSON::Object::Ptr &obj1, Poco::JSON::Object::Ptr &obj2);
                void mergeSimpleTypes(Poco::JSON::Object::Ptr &obj1, Poco::JSON::Object::Ptr &obj2);

                bool canMerge(Poco::JSON::Object::Ptr &obj1, Poco::JSON::Object::Ptr &obj2);
                template<typename T> void updateMinMax(Poco::JSON::Object::Ptr &obj1, Poco::JSON::Object::Ptr &obj2);
            };
        }
    }
}
