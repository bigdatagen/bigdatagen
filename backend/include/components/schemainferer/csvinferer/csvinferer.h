/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#pragma once

#include "components/schemainferer/infererplugin.h"     // InfererPlugin

#include <Poco/JSON/Object.h>       // Poco::JSON::Object
#include <Poco/JSON/Array.h>        // Poco::JSON::Array
#include <Poco/Dynamic/Var.h>       // Poco::Dynamic::Var

#include <iostream>     // std::istream, std::ostream
#include <string>       // std::string

namespace bdg
{
    namespace components
    {
        namespace schemaInferer
        {
            /**
             * Implementation of inferrer plugin for CSV files.
             */
            class CsvInferer : public InfererPlugin
            {
            public:
                CsvInferer();

                void inferSchema(std::ostream &os, const Poco::JSON::Object::Ptr &params) override;

            private:
                Poco::JSON::Object::Ptr csvToSchema(std::istream &ifs, std::string &separator);
                void mergeRoot(Poco::JSON::Object::Ptr &obj1, Poco::JSON::Object::Ptr &obj2);
                    
                Poco::JSON::Array::Ptr getColumns(std::string &line, std::string &separator);

                void mergeLines(Poco::JSON::Array::Ptr &line1, Poco::JSON::Array::Ptr &line2);
                void mergeColumns(Poco::JSON::Object::Ptr &column1, Poco::JSON::Object::Ptr &column2);

                template<class T> void updateMinMax(Poco::JSON::Object::Ptr &obj1, Poco::JSON::Object::Ptr &obj2);
            };
        }
    }
}
