/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#pragma once

#include <Poco/JSON/Object.h>       // Poco::JSON::Object

#include <iostream>         // istream, ostream
#include <string>           // std::string
#include <map>              // std::map
#include <unordered_set>    // std::unordered_set

namespace bdg
{
    namespace components
    {
        /**
         * So, it looks like it is actually pronounced "inferrer" and not "inferer". Somehow, we didn't
         * noticed it not until the late part of the project and changing it now might break a few things,
         * so we just roll with it now.
         * If someone in the future insists on correcting the typo, feel free to do so, but you have been warned.
         */
        namespace schemaInferer
        {
            /**
             * Base class for all the inferer plugins.
             * Must be loadable by POCO class loader.
             */
            class InfererPlugin
            {
            public:
                InfererPlugin();

                /**
                 * Virtual destructor - right destructor must be called when the plugin is destroyed.
                 */
                virtual ~InfererPlugin() = default;
            
                /** 
                 * Default methods for loading the filenames of the files
                 * from which a schema will be infered.
                 * May be overridden.
                 */
                virtual void addDirectory(const std::string &relPath);
                virtual std::string addFile(const std::string &relPath);

                inline void clear();
                inline std::unordered_set<std::string> const& getFilenames() const;
                inline Poco::JSON::Object::Ptr getPluginInfo() const;

                /**
                 * Abstract method which must be specified
                 * individually for each inferer implementation.
                 *
                 * @param os Output stream containing the infered schema.
                 * @param params JSON object with parameters modifying the inferer behaviour
                 */
                virtual void inferSchema(std::ostream &os, const Poco::JSON::Object::Ptr &params) = 0;

            protected:
                inline void removeFilename(const std::string &filename);

                /**
                 * Paths to the files from which we infer a schema.
                 */
                std::unordered_set<std::string> filenames;

                /**
                 * Name of the plugin.
                 */
                std::string name;

                /**
                 * Description of the plugin.
                 */
                std::string description;

                /**
                 * Optional parameters specific for the inferer implementation. Each
                 * parameter should have its default value set by the implementation.
                 */
                std::map<std::string, std::string> parameters;
            };
        }
    }
}

/**
 * Clears the list of files that are to be infererred.
 */
void bdg::components::schemaInferer::InfererPlugin::clear()
{
    filenames.clear();
}

/**
 * Returns paths to the files from which we infer a schema.
 *
 * @return Paths to the files.
 */
std::unordered_set<std::string> const &bdg::components::schemaInferer::InfererPlugin::getFilenames() const
{
    return filenames;
}

/**
 * Creates a JSON object containing the information about the inferrer plugin.
 *
 * @return Poco::JSON::Object containing the inferrer information.
 */
Poco::JSON::Object::Ptr bdg::components::schemaInferer::InfererPlugin::getPluginInfo() const
{
    Poco::JSON::Object::Ptr info = new Poco::JSON::Object;
    info->set("name", name);
    info->set("description", description);

    Poco::JSON::Array::Ptr paramsArr = new Poco::JSON::Array;

    for (auto const &param : parameters)
    {
        Poco::JSON::Object::Ptr paramObj = new Poco::JSON::Object;
        paramObj->set("name", param.first);
        paramObj->set("description", param.second);
        paramsArr->add(paramObj);
    }

    info->set("parameters", paramsArr);

    return info;
}

/**
 * Removes a specified file from "filenames". Used when the file cannot be processed
 * by the specific inferer implementation and we want to "skip" it.
 *
 * @param filename File name.
 */
void bdg::components::schemaInferer::InfererPlugin::removeFilename(const std::string &filename)
{
    filenames.erase(filename);
}
