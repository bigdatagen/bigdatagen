/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Components/Grammar/GrammarGenerator
 * @author   Big Data Generator Team
 *
 *    grammar.h - class handling ebnf-style grammar
 */

#pragma once

#include "rule.h"
#include "parser.h"
#include "scanner.h"
#include <vector>
#include <unordered_set>
#include <stdio.h>
#include <fstream>
#include <algorithm>
#include <sstream>

namespace bdg
{
    namespace components
    {
        namespace grammar
        {

        using symbolType::TSymbolType;

        class Grammar
        {
            Coco::Scanner* scanner;
            Coco::Parser* parser;
            bool consistent = false;            // did we modified the rules recently?

            Symbol* start;                  // starting nonterminal
            std::vector<Rule> rules;               // list of the grammar rules

            std::unordered_set<std::string> nterms;  // set of all nonterminals 
            std::unordered_set<std::string> terms;   // set of all terminal symbols


            /* Probability distributions */
            /*
             * Can be specified either as an predefined distr. (constant) or from a file
             *
             */
            //String[] _distr_nont;        // NonT
            //String[] _distr_term;        // term


            // TODO: What else?

            /* actual loader */
            void loadGrammar();

            void updateSets(const Rule&);
            void pruneGrammar();

        public:
            /* load and save interfaces */
            //void loadGrammar(std::ifstream&);
            void loadGrammar(FILE*);
            void saveGrammar(std::ofstream&);
            //void saveGrammar(FILE*);

            void toJSON(std::ofstream&);

            /* member functions */
            void addRule(const Rule&);
            void removeRule(const Rule&);
            void setStart(const Symbol*);

            Symbol* getStart() const;
            Rule* getRule(const Symbol*);
            void getRules(std::vector<Rule>&);

            /* returns list of rules used to generate the file */
            // std::vector<Rule> ParseFile(const std::fstream&) const;

            ~Grammar();



            // METHOD SUGGESTIONS - crazier ideas, and stuff

            /* by parsing "source" and looking at the rules containing "nont" as a left-hand
             * side, produces a "nont.distr" file containing the distribution of related rules
             * rewrite - should we append or rewrite "nont.distr"
             */
            //void get_distribution(String nont, File in, bool rewrite);

            // this should probably be in the generator part
            //void generate_file();

        };
        } // namespace grammar
    } // namespace components
} // namespace bdg
