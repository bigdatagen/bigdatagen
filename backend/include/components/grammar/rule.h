/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category GrammarGenerator/Grammar
 * @author   Big Data Generator Team
 * 
 *  rule.h - class representing grammar rules
 */

#pragma once

#include <vector>
#include <unordered_set>
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <assert.h>
#include <string>
#include <iostream>

namespace bdg
{
    namespace components
    {
        namespace grammar
        {

        /* Symbol type constants */
        namespace symbolType
        {

            /* Do not change the order of these (but more entries can be added) */
            /* UNDEF should be defined last */
            enum TSymbolType
            {
                /* The symbol types were taken from the EbnfVisualizer project */
                /* Simple symbols */
                T        = 0,        /* terminal symbol */
                NT        = 1,        /* nonterminal symbol */
                EPS        = 2,        /* empty */
                /* Complex symbols */
                ALT        = 3,        /* alternative: | */
                ITER    = 4,        /* iteration: { } */
                OPT        = 5,        /* option: [ ] */
                UNDEF    = 6,        /* undefined type */
            };

            /* String representations of the types */
            static const char * TSymbolTypeStrings[] =
            {
                "terminal",
                "nonterminal",
                "empty",
                "alternative",
                "iteration",
                "option",
                "undefined",
            };    

            /* type control methods (mainly for debug) */
            inline bool isValidType(TSymbolType t)
            {
                return t >= 0 && t < UNDEF;
            }
    
            inline bool isNT(TSymbolType t)
            {
                return t == NT;
            }

            inline bool isEmpty(TSymbolType t)
            {
                return t == EPS;
            }

            inline bool isSimple(TSymbolType t)
            {
                return t >= T || t <= EPS;
            }

            inline bool isComplex(TSymbolType t)
            {
                return t > EPS && t < UNDEF;
            }
        
            inline const char * toString( int enumVal )
            {
                return TSymbolTypeStrings[enumVal];
            }

        };


        using symbolType::TSymbolType;


        /* Abstract Base class for representing and creating Grammar symbols */ 
        class Symbol
        {
            virtual bool equals(const Symbol*) const;
        public:
            /* accessors */
            TSymbolType getType() const;

            /* These functions are defined only in some of the derived-classes*/
            virtual std::string getValue() const;
            virtual void addChild(Symbol*);
            virtual void getChildren(std::vector<Symbol*>&) const;
            virtual void getSimpleSymbols(std::unordered_set<std::string>&, TSymbolType) const;
    
            virtual std::string toString() const = 0;
            virtual std::string toJSON() const = 0;

            /* Comparators */
            bool operator==(const Symbol&) const;
            bool operator!=(const Symbol&) const;
        
            virtual ~Symbol() { };
    
        protected:
            TSymbolType type;         /* type of a symbol */

            Symbol() { };
            Symbol(TSymbolType);
        };
    

        /* TERM, NONT */
        class SimpleSymbol: public Symbol
        {
            virtual bool equals(const Symbol*) const;
        public:
            SimpleSymbol(std::string, TSymbolType);
        
            std::string getValue() const;
            std::string toString() const;
            std::string toJSON() const;

        protected:
            std::string value;
        };


        /* ALT, ITER, OPT */
        class ComplexSymbol: public Symbol
        {
            virtual bool equals(const Symbol*) const;
        public:
            ComplexSymbol(TSymbolType);

            void addChild(Symbol*);
            void getChildren(std::vector<Symbol*>&) const;
            virtual void getSimpleSymbols(std::unordered_set<std::string>&, TSymbolType) const;
            std::string toString() const;
            std::string toJSON() const;

            ~ComplexSymbol();

        protected:
            std::vector<Symbol*> childPtrs;
        };

        /* Symbol Factory Class */
        class SymbolCreator
        {
            SymbolCreator() { };
            SymbolCreator(const SymbolCreator &) { };
            SymbolCreator& operator=(const SymbolCreator &) { return *this; };
    
        public:
            static SymbolCreator* get()
            {
                static SymbolCreator instance;
                return &instance;
            };

            Symbol* createSymbol(TSymbolType);
            Symbol* createSymbol(std::string, TSymbolType);
        };


        class Rule 
        {
            /* The rhs of the rule is defined as a hierarchical structure of symbols */
            Symbol* lhsPtr;                   /* left-hand side of the rule */
            Symbol* rhsPtr;                   /* right-hand side of the rule */

        public:
            Rule(Symbol*, Symbol*);
            Rule(const Rule &);

            /* member functions */
            Symbol* getLhs() const;        /* returns left-hand side of the rule */
            Symbol* getRhs() const;       /* returns right-hand side of the rule */
            void getSimpleSymbols(std::unordered_set<std::string>&, TSymbolType) const; /* list of all TERMs and NONTs seen in the rule */

            void merge(const Rule&);

            /* operators */
            Rule& operator=(const Rule&);
            bool operator==(const Rule&) const;
            bool operator!=(const Rule&) const;

            /* misc */
            std::string toString() const;
            std::string toJSON() const;
            void print() const;

            ~Rule();
        };
        } // namespace grammar
    } // namespace components
} // namespace bdg
