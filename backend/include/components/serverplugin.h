/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#pragma once

#include "utils/fileupload.h"           // FileUpload

#include <string>       // std::string
#include <map>          // std::map

namespace bdg
{
    namespace server
    {
        /**
         * Base class for all the plugins which are loadable by POCO class loader.
         */
        class ServerPlugin
        {
        public:
            typedef std::map<std::string, std::string> Parameters;

            /**
             * Plugin constructor.
             */
            ServerPlugin() = default;

            /**
             * Virtual destructor - right destructor must be called when the plugin is destroyed.
             */
            virtual ~ServerPlugin() = default;
            
            /**
             * Method called by the server to process the plugin code. Each HTTP request to the server
             * requiring this plugin results in one call of this method.
             *
             * @param parameters Parameters for the plugin run.
             * @param uploadedFiles Collection of uploaded files.
             *
             * @return Result of the plugin run.
             */
            virtual std::string run(const Parameters &parameters, const bdg::utils::FileUpload::FileUploadCollection &uploadedFiles) = 0;
        };
    }
}
