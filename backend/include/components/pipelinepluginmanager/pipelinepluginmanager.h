/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Components
 * @author   Big Data Generator Team
 */

#pragma once

#include "components/bdgapi.h"                  // Plugin API

namespace bdg
{
    namespace components
    {
        /**
         * Server plugin which provides information about all available pipeline plugins
         * and their settings in JSON format.
         */
        class PipelinePluginManager : public server::ServerPlugin
        {
        public:
            PipelinePluginManager() = default;
            virtual ~PipelinePluginManager() = default;

            virtual std::string run(const server::ServerPlugin::Parameters &parameters, const bdg::utils::FileUpload::FileUploadCollection &uploadedFiles) override;
        };
    }
}
