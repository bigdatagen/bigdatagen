/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#pragma once

#include "components/serverplugin.h"    // ServerPlugin
#include "utils/config.h"               // Config
#include "utils/logger.h"               // Logger

#include <Poco/File.h>                  // Poco::File
#include <Poco/Path.h>                  // Poco::Path
#include <Poco/StreamCopier.h>          // Poco::StreamCopier
#include <Poco/String.h>                // Poco::String
#include <Poco/StringTokenizer.h>       // Poco::StringTokenizer
#include <Poco/ClassLibrary.h>          // POCO Export class

#include <iostream>         // std::cout
#include <fstream>          // std::fstream
#include <sstream>          // std::stringstream
