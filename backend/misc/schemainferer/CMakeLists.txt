cmake_minimum_required (VERSION 3.0 FATAL_ERROR)

# Program for launching SchemaInferer
project(SchemaInfererApp CXX)

include_directories(
    ${HDR_DIR}
)

add_executable(${PROJECT_NAME}
	"${MISC_DIR}/schemainferer/main.cpp"
	"${SRC_DIR}/components/schemainferer/schemainferer.cpp"
	"${HDR_DIR}/components/schemainferer/schemainferer.h"
	"${SRC_DIR}/components/schemainferer/infererplugin.cpp"
	"${HDR_DIR}/components/schemainferer/infererplugin.h"
)

target_link_libraries(${PROJECT_NAME}
	SchemaInferer
	Zip
	Foundation
	Util
	JSON
)

POST_BUILD_BINARY_COPY()

install(TARGETS ${PROJECT_NAME} RUNTIME DESTINATION ${BDG_BIN_DIR_FULL})
