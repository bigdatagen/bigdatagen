/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 * 
 * @category Main
 * @author   Big Data Generator Team
 */

#include "components/schemainferer/schemainferer.h"         // SchemaInferer

#include <Poco/Zip/Decompress.h>        // Poco::Zip::Decompress
#include <Poco/JSON/Parser.h>           // Poco::JSON::Parser

#include <random>       // std::random
#include <fstream>      // std::fstream

/**
 * Print the Usage message.
 */
void printUsage()
{
#if defined(_WIN32)
    std::cerr << "Usage: SchemaInfererApp.exe run <infererName> <directoryToInfer|fileToInfer> [<infererParameters>]" << std::endl;
    std::cerr << "Usage: SchemaInfererApp.exe list [<infererName>]" << std::endl;
#else
    std::cerr << "Usage: ./SchemaInfererApp run <infererName> <directoryToInfer|fileToInfer> [<infererParameters>]" << std::endl;
    std::cerr << "Usage: ./SchemaInfererApp list [<infererName>]" << std::endl;
#endif
    std::cerr << "" << std::endl;
}

/**
 * Print the information about a specific plugin.
 *
 * @param plugin Plugin which we print info about.
 *
 * @return Return value of the method.
 */
int printPluginInfo(const Poco::JSON::Object::Ptr &plugin)
{
    std::map<std::string, std::string> descriptions;
    std::cerr << plugin->get("name").convert<std::string>() << std::endl;
    
    std::cerr << " - infererParameters: \"";
    Poco::JSON::Array::Ptr parameters = plugin->getArray("parameters");

    for (int i = 0; i < parameters->size(); ++i)
    {
        Poco::JSON::Object::Ptr parameter = parameters->getObject(i);
        std::string name = parameter->get("name").convert<std::string>();
        std::string descr = parameter->get("description").convert<std::string>();
        descriptions[name] = descr;
        std::cerr << "<" << name << "> ";
    }

    std::cerr << "\"" << std::endl;

    for (const auto &description : descriptions)
    {
        std::cerr << " |-> " << description.first << " - " << description.second << std::endl;
    }

    std::cerr << std::endl;

    return 0;
}

/**
 * Choose a plugin for which the information will be printed.
 *
 * @param plugins List of available plugins.
 * @param pluginName Name of the plugin.
 *
 * @return Return value of the method.
 */
int printPluginInfo(const Poco::JSON::Array::Ptr &plugins, const std::string &pluginName)
{
    for (int i = 0; i < plugins->size(); ++i)
    {
        Poco::JSON::Object::Ptr plugin = plugins->getObject(i);

        if (plugin->get("name") == pluginName)
        {
            printPluginInfo(plugin);
            return 0;
        }
    }

    std::cerr << "Inferer plugin \"" + pluginName + "\" not found (run \"schemaInfererApp list\") to see available plugins." << std::endl;
    
    return 1;
}

/**
 * Register the names of parameters available to the plugin.
 *
 * @param plugins List of available plugins.
 * @param pluginName Name of the plugin.
 * @param parameterNames Returned vector of available parameters.
 *
 * @return Return value of the method.
 */
int getPluginParameterNames(const Poco::JSON::Array::Ptr &plugins, const std::string &pluginName, std::vector<std::string> &parameterNames)
{
    parameterNames.clear();

    for (int i = 0; i < plugins->size(); ++i)
    {
        Poco::JSON::Object::Ptr plugin = plugins->getObject(i);
        
        if (plugin->get("name") == pluginName)
        {
            Poco::JSON::Array::Ptr parameters = plugin->getArray("parameters");
            
            for (int j = 0; j < parameters->size(); ++j)
            {
                parameterNames.push_back(parameters->getObject(j)->get("name").convert<std::string>());
            }

            return 0;
        }
    }

    std::cerr << "Inferer plugin \"" + pluginName + "\" not found (run \"schemaInfererApp list\" to see available plugins)." << std::endl;
    
    return 1;
}

/**
 * Starting point of the program. It has four command line arguments, first of them is mandatory.
 * First argument is either "run" or "list". List command lists all the available inferrers. Run
 * command runs the inferrer and it has three more arguments. Second argument is the name of the
 * inferrer plugin that should run. Third argument is a path to the directory with file from
 * which a schema should be inferrer. Last argument are the parameters for the selected
 * inferrer plugin.
 *
 * @param argc Number of command line arguments.
 * @param argv Command line arguments.
 *
 * @return Return value of the program.
 */
int main(int argc, char **argv)
{
    // Check infererplugins location configuration.
    try
    {
        bdg::utils::Config::getInstance().getInfererPluginsPath("infererplugins");
    }
    catch (Poco::Exception &e)
    {
        std::cerr << "ERROR: " << e.displayText() << "!" << std::endl;
        return 1;
    }
    catch (std::exception &e)
    {
        std::cerr << "ERROR: " << e.what() << "!" << std::endl;
        return 1;
    }

    bdg::server::ServerPlugin::Parameters params;
    bdg::utils::FileUpload::FileUploadCollection uploadedFiles;

    // Run this program as a test.
    if (argc < 2 || (argc < 4 && strcmp(argv[1], "list") != 0) || (strcmp(argv[1], "list") != 0 && strcmp(argv[1], "run") != 0))
    {
        printUsage();
        return 1;
    }
    // Run this program as an entry point to the pipeline launcher plugin.
    else
    {
        std::stringstream ss;
        bdg::components::SchemaInferer inferer(bdg::utils::Logger::getInstance().getConsoleLogger());

        int retVal = 0;

        // Construct the configuration JSON.
        Poco::JSON::Object::Ptr config = new Poco::JSON::Object;

        config->set("command", "list");
        config->stringify(ss);
        params["configuration"] = ss.str();

        Poco::JSON::Object::Ptr info = Poco::JSON::Parser().parse(inferer.run(params, uploadedFiles)).extract<Poco::JSON::Object::Ptr>();
        Poco::JSON::Array::Ptr plugins = info->getArray("plugins");

        if (strcmp(argv[1], "list") == 0)
        {
            if (argc > 2)
            {    
                retVal = printPluginInfo(plugins, argv[2]);
            }
            else
            {
                for(int i = 0; i < plugins->size(); ++i)
                {
                    retVal = printPluginInfo(plugins->getObject(i));
                }
            }

            return retVal;
        }

        if (strcmp(argv[1], "run") == 0)
        {
            config->clear();
            config->set("command", "run");
            config->set("name", std::string(argv[2]));

            std::vector<std::string> parameterNames;
            retVal = getPluginParameterNames(plugins, std::string(argv[2]), parameterNames);
            
            if (retVal)
            {
                return retVal;
            }

            if (parameterNames.size() + 4 != argc)
            {
                std::cerr << "Invalid number of plugin parameters." << std::endl;
                std::cerr << std::endl;
                printUsage();
                printPluginInfo(plugins, std::string(argv[2]));
                return 1;
            }

            for(int i = 0; i < parameterNames.size(); ++i)
            {
                config->set(parameterNames[i], std::string(argv[4+i]));
            }

            try
            {
                Poco::Path path(argv[3]); 

                if (Poco::File(path).isDirectory())
                {
                    Poco::DirectoryIterator end;
                    
                    for (Poco::DirectoryIterator dirIt(path); dirIt != end; ++dirIt)
                    {
                        if (!dirIt->isDirectory()) {
                            uploadedFiles[dirIt.name()] = std::unique_ptr<bdg::utils::FileUpload>(bdg::utils::FileUpload::create(dirIt->path(), "text"));
                        }
                    }
                }
                else
                {
                    uploadedFiles[path.getFileName()] = std::unique_ptr<bdg::utils::FileUpload>(bdg::utils::FileUpload::create(path.toString(), "text"));
                }
            }
            catch(Poco::Exception &e)
            {
                std::cerr << "ERROR: " << e.displayText() << "!" << std::endl;
                return 1;
            }

            ss.str("");
            config->stringify(ss);
            params["configuration"] = ss.str();
        }

        std::cout << inferer.run(params, uploadedFiles) << std::endl;

        return 0;
    }
}
