/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category App
 * @author   Big Data Generator Team
 */

#include <Poco/Net/ServerSocket.h>          // Poco::Net::ServerSocket
#include <Poco/Net/StreamSocket.h>          // Poco::Net::StreamSocket
#include <Poco/Net/SocketStream.h>          // Poco::Net::SocketStream
#include <Poco/Net/SocketAddress.h>         // Poco::Net::SocketAddress
#include <Poco/StreamCopier.h>              // Poco::StreamCopier

#include <iostream>     // std::cout
#include <string>       // std::string

/**
 * Starting point of the program.
 *
 * @param argc Number of command line arguments.
 * @param argv Command line arguments.
 *
 * @return Return value of the program.
 */
int main(int argc, char** argv)
{
    int port = 0;

    if (argc > 1)
    {
        port = atoi(argv[1]);
    }

    if (port <= 0)
    {
        port = 9900;
    }

    Poco::Net::ServerSocket serverSocket(port);     // Does bind + listen.
    std::cout << "Waiting for connection on port " << port << "..." << std::endl;

    // Wait for a request.
    while (true)
    {
        Poco::Net::StreamSocket streamSocket = serverSocket.acceptConnection();
        std::cout << "Accepted connection from [" << streamSocket.address().host().toString() << "]" << std::endl;

        // Receive data.
        char buffer[2048];
        int bytesReceived = streamSocket.receiveBytes(buffer, sizeof(buffer));

        std::string data(buffer, bytesReceived);
        std::cout << "Data: " << data << std::endl;
        
        // Send response.
        Poco::Net::SocketStream responseStream(streamSocket);
        responseStream << "Processed" << std::flush;
    }

    // Wait for user input.
    std::cout << "Write something to exit: ";
    std::string input;
    std::cin >> input;
}
