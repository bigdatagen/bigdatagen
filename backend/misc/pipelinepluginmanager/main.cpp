/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 * 
 * @category App
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/plugin.h"       // Plugin in pipeline
#include "utils/config.h"                       // Config
#include "utils/logger.h"                       // Logger
#include "utils/pluginmanager.h"                // PluginManager

typedef std::unordered_set<std::string> PluginNamesCollection;

/**
 * Adds a new plugin to the JSON representing all pipeline plugins with their settings.
 *
 * @param plugin Pipeline plugin.
 * @param pluginName Name of the pipeline plugin.
 * @param isSingleton Is the plugin singleton or not?
 * @param pluginsJson Output parameter with JSON representing all pipeline plugins.
 */
void addPluginJson(const bdg::core::pipeline::plugin::Plugin *plugin, const std::string &pluginName, const bool isSingleton, Poco::JSON::Array &pluginsJson)
{
    const bdg::core::pipeline::plugin::Plugin::PluginSettings &pluginSettings = plugin->getSettings();

    Poco::JSON::Object pluginJson;
    Poco::JSON::Array pluginSettingsJson;

    // Set the plugin's basic properties.
    pluginJson.set("name", pluginName);
    pluginJson.set("description", plugin->getDescription());
    pluginJson.set("type", Poco::toLower(plugin->getTypeString()));
    pluginJson.set("isSingleton", isSingleton);

    // Set the plugin's settings.
    for (bdg::core::pipeline::plugin::Plugin::PluginSettings::const_iterator it = pluginSettings.begin(); it != pluginSettings.end(); ++it)
    {
        Poco::JSON::Object oneSettingJson;
        it->get()->serialize(oneSettingJson);
        pluginSettingsJson.add(oneSettingJson);
    }

    pluginJson.set("settings", pluginSettingsJson);

    // Add the plugin JSON to the overall JSON.
    pluginsJson.add(pluginJson);
}

/**
 * Starting point of the program. This program is used by the server to get the information
 * about all the available pipeline plugins and their settings.
 *
 * @param argc Number of command line arguments.
 * @param argv Command line arguments.
 *
 * @return Return value of the program.
 */
int main(int argc, char **argv)
{
    bdg::utils::PluginManager<bdg::core::pipeline::plugin::Plugin> pipelinePluginManager(bdg::utils::Logger::getInstance().getNullLogger(), bdg::utils::Config::getInstance().getPipelinePluginsPath("pipelineplugins"));
    const PluginNamesCollection &pluginNames = pipelinePluginManager.getPluginNames();
    const PluginNamesCollection &singletonPluginNames = pipelinePluginManager.getSingletonPluginNames();

    // Create the response.
    Poco::JSON::Object response;
    Poco::JSON::Array plugins;

    // Loop through all the available pipeline plugins.
    for (PluginNamesCollection::const_iterator it = pluginNames.begin(); it != pluginNames.end(); ++it)
    {
        addPluginJson(pipelinePluginManager.getPlugin(*it).get(), *it, false, plugins);
    }

    for (PluginNamesCollection::const_iterator it = singletonPluginNames.begin(); it != singletonPluginNames.end(); ++it)
    {
        addPluginJson(&pipelinePluginManager.getPluginSingleton(*it), *it, true, plugins);
    }

    response.set("plugins", plugins);
    response.stringify(std::cout);

    return 0;
}
