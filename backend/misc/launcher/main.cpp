/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 * 
 * @category App
 * @author   Big Data Generator Team
 */

#include "components/pipelinelauncher/pipelinelauncher.h"       // PipelineLauncher
#include "core/pipeline/pipelineinfo.h"                         // PipelineInfo

/**
 * Starting point of the program. It has two command line arguments - first is a jobId
 * which is the id of job configuration file that should be loaded and used for the
 * generation. Second (non-mandatory) arguments sets the number of used MPI threads
 * to generate files according to a schema given in the first argument.
 *
 * @param argc Number of command line arguments.
 * @param argv Command line arguments.
 *
 * @return Return value of the program.
 */
int main(int argc, char **argv)
{
    // Check configuration.
    try
    {
        bdg::utils::Config::getInstance();
    }
    catch (Poco::Exception &e)
    {
        std::cerr << "ERROR: " << e.displayText() << "!" << std::endl;
        return 1;
    }
    catch (std::exception &e)
    {
        std::cerr << "ERROR: " << e.what() << "!" << std::endl;
        return 1;
    }

    bdg::server::ServerPlugin::Parameters params;

    // Run this program as a test.
    if (argc < 2)
    {
#if defined(_WIN32)
        std::cerr << "Usage: PipelineLauncherApp.exe [jobId|configurationPath] [clusterSize]" << std::endl;
#else
        std::cerr << "Usage: ./PipelineLauncherApp [jobId|configurationPath] [clusterSize]" << std::endl;
#endif
        return 1;
    }
    // Run this program as an entry point to the pipeline launcher plugin.
    else
    {
        params.emplace("path", argv[1]);
        params.emplace("waitForResponse", "true");

        if (argc >= 3)
        {
            params.emplace("clusterSize", argv[2]);
        }

        // Run the launcher.
        bdg::utils::FileUpload::FileUploadCollection uploadedFiles;
        bdg::components::PipelineLauncher launcher;

        launcher.run(params, uploadedFiles);

        return 0;
    }
}
