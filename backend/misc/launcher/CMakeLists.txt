cmake_minimum_required (VERSION 3.0 FATAL_ERROR)

# Program for launching of pipeline
project(PipelineLauncherApp CXX)

include_directories(
    ${HDR_DIR}
)

add_executable(${PROJECT_NAME}
	"${MISC_DIR}/launcher/main.cpp"
    "${SRC_DIR}/components/pipelinelauncher/pipelinelauncher.cpp"
	"${HDR_DIR}/components/pipelinelauncher/pipelinelauncher.h"
    "${SRC_DIR}/components/pipelinelauncher/job.cpp"
	"${HDR_DIR}/components/pipelinelauncher/job.h"
	"${SRC_DIR}/core/pipeline/pipelineinfocheckpointer.cpp"
	"${HDR_DIR}/core/pipeline/pipelineinfocheckpointer.h"
	"${SRC_DIR}/core/pipeline/monitoringcheckpointerrestorer.cpp"
	"${HDR_DIR}/core/pipeline/monitoringcheckpointerrestorer.h"
)

target_link_libraries(${PROJECT_NAME}
	BdgUtil
	Foundation
	Util
	JSON
)

POST_BUILD_BINARY_COPY()

install(TARGETS ${PROJECT_NAME} RUNTIME DESTINATION ${BDG_BIN_DIR_FULL})
