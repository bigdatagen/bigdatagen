/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Utils
 * @author   Big Data Generator Team
 */

#include "utils/logger.h"                   // Logger
#include "utils/config.h"                   // Config

#include <Poco/ConsoleChannel.h>            // Poco::ConsoleChannel
#include <Poco/NullChannel.h>               // Poco::NullChannel
#include <Poco/SimpleFileChannel.h>         // Poco::SimpleFileChannel
#include <Poco/FormattingChannel.h>         // Poco::FormattingChannel
#include <Poco/PatternFormatter.h>          // Poco::PatternFormatter
#include <Poco/AutoPtr.h>                   // Poco::AutoPtr
#include <Poco/Path.h>                      // Poco::Path
#include <Poco/File.h>                      // Poco::File
#include <Poco/String.h>                    // Poco::toLower

const std::string bdg::utils::Logger::FILE_CONSOLE = "console";
const std::string bdg::utils::Logger::FILE_NULL = "null";
int bdg::utils::Logger::jobId = 0;

/**
 * Returns single unique instance of application logger - singleton pattern.
 *
 * @return Instance of application logger.
 */
bdg::utils::Logger &bdg::utils::Logger::getInstance()
{
    static bdg::utils::Logger instance;
    return instance;
}

/**
 * Returns instance of the logger which outputs log messages to the console. This method is not thread safe.
 *
 * @return Instance of the logger which outputs log messages to the console.
 */
Poco::Logger &bdg::utils::Logger::getConsoleLogger()
{
    if (consoleLogger == nullptr)
    {
        consoleLogger = &getLoggerNoCache("ConsoleLogger", FILE_CONSOLE);
    }

    return *consoleLogger;
}

/**
 * Returns instance of the logger which throws away all log messages. This method is not thread safe.
 *
 * @return Instance of the logger which throws away all log messages.
 */
Poco::Logger &bdg::utils::Logger::getNullLogger()
{
    if (nullLogger == nullptr)
    {
        nullLogger = &getLoggerNoCache("NullLogger", FILE_NULL);
    }

    return *nullLogger;
}

/**
 * Returns instance of the logger which is used to log in the server application. This method is not thread safe.
 *
 * @returnInstance of the logger which is used to log in the server application. 
 */
Poco::Logger &bdg::utils::Logger::getServerLogger()
{
    if (serverLogger == nullptr)
    {
        serverLogger = &getLoggerNoCache("ServerLogger", "server.log");
    }

    return *serverLogger;
}

/**
 * Returns instance of the logger which is used to log in the pipeline application. This method is not thread safe.
 *
 * @param rank Rank of the processor on which the MPI process is running.
 *
 * @return Instance of the logger which is used to log in the pipeline application.
 */
Poco::Logger &bdg::utils::Logger::getLogger(const int rank)
{
    if (mpiLogger == nullptr)
    {
        mpiLogger = &getLoggerNoCache(rank == 0 ? "Logger - Master" : "Logger - Worker " + std::to_string(rank), "log-" + std::to_string(jobId) + Poco::Path::separator() + "log-rank-" + std::to_string(rank) + ".log", rank);
    }

    return *mpiLogger;
}

/**
 * Returns a logger which stores messages to a given file. This method is not thread safe.
 *
 * @param loggerName Name of the logger.
 * @param fileName Path to the log file where log messages are stored.
 *
 * @return Logger which stores messages to a given file.
 */
Poco::Logger &bdg::utils::Logger::getLogger(const std::string &loggerName, const std::string &fileName)
{
    LoggerMap::const_iterator loggerIt = loggers.find(loggerName);

    if (loggerIt == loggers.end())
    {
        Poco::Logger &logger = getLoggerNoCache(loggerName, fileName);
        loggers.emplace(loggerName, &logger);

        return logger;
    }
    else
    {
        return *loggerIt->second;
    }
}

/**
 * Returns instance of the logger which is used to log in the pipeline application.
 *
 * @param loggerName Name of the logger.
 * @param fileName Path to the log file where log messages are stored.
 * @param rank Rank of the processor on which the MPI process is running.
 *
 * @return Instance of the logger which is used to log in the pipeline application.
 */
Poco::Logger &bdg::utils::Logger::getLoggerNoCache(const std::string &loggerName, const std::string &fileName, const int rank)
{
    // Channels.
    Poco::AutoPtr<Poco::PatternFormatter> patternFormater(new Poco::PatternFormatter);
    Poco::AutoPtr<Poco::Channel> logChannel;

    // Console logger.
    if (fileName == FILE_CONSOLE)
    {
        logChannel = new Poco::ConsoleChannel;
        patternFormater->setProperty(Poco::PatternFormatter::PROP_PATTERN, "  >> %t");
    }
    // Null logger - throws away all the log messages.
    else if (fileName == FILE_NULL)
    {
        logChannel = new Poco::NullChannel;
    }
    // File logger.
    else
    {
        const std::string logFile = bdg::utils::Config::getInstance().getLogStoragePath() + Poco::Path::separator() + fileName;
        const Poco::Path directoryPath(Poco::Path(logFile).parent());
        
        // Create the directory for logging if it does not exist yet.
        if (rank <= 0)
        {
            try
            {
                Poco::File directory(directoryPath);
                directory.createDirectories();
            }
            catch (Poco::Exception&)
            {
                throw Poco::CreateFileException("Could not create directory [" + directoryPath.toString() + "] as a log storage!");
            }
        }

        logChannel = new Poco::SimpleFileChannel;
        logChannel->setProperty(Poco::SimpleFileChannel::PROP_PATH, logFile);
        
        // For more information see http://www.appinf.com/docs/poco/Poco.PatternFormatter.html.
        patternFormater->setProperty(Poco::PatternFormatter::PROP_PATTERN, "%Y-%m-%d %H:%M:%S | %p | %s | Thread: %I | Process: %P | %t");
    }

    // Setup logger.
    Poco::Logger &logger(Poco::Logger::get(loggerName));
    logger.setChannel(new Poco::FormattingChannel(patternFormater, logChannel));
    
    try
    {
        logger.setLevel(Poco::toLower(bdg::utils::Config::getInstance().getLogLevel()));
    }
    catch (Poco::Exception&)
    {
        logger.setLevel("information");
    }

    return logger;
}


/**
 * Sets the id of currently running job.
 *
 * @param newJobId Id of currently running job.
 */
void bdg::utils::Logger::setJobId(const int newJobId)
{
    jobId = newJobId;
}
