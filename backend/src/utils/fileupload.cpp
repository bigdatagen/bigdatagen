/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Utils
 * @author   Big Data Generator Team
 */

#include "utils/fileupload.h"           // Header
#include "utils/config.h"               // Config

#include <Poco/CountingStream.h>        // Poco::CountingStream
#include <Poco/File.h>                  // Poco::File
#include <Poco/Path.h>                  // Poco::Path
#include <Poco/StreamCopier.h>          // Poco::StreamCopier
#include <Poco/UUID.h>                  // Poco::UUID
#include <Poco/UUIDGenerator.h>         // Poco::UUIDGenerator

#include <fstream>      // std::ofstream

namespace bdg
{
    namespace utils
    {
        /**
         * Class representing data of one file uploaded to the server over HTTP
         * protocol - PIMPL design pattern.
         */
        class FileUploadImpl : public FileUpload
        {
        private:
            friend class FileUpload;

            FileUploadImpl(const std::string &name, const std::string &contentType, std::istream &inputStream);
            FileUploadImpl(const std::string &name, const std::string &contentType);
            
            /**
             * Name of the file.
             */
            const std::string name;

            /**
             * Type of the file.
             */
            const std::string contentType;

            /**
             * Path to the file in the file system.
             */
            std::string temporaryFile;

            /**
             * Size of the file.
             */
            size_t size;
        };
    }
}

/**
 * Constructor of the uploaded file implementation.
 *
 * @param name Name of the file.
 * @param contentType Type of the file.
 * @param inputStream Stream which can read uploaded file.
 */
bdg::utils::FileUploadImpl::FileUploadImpl(const std::string &name, const std::string &contentType, std::istream &inputStream) :
    name(name),
    contentType(contentType)
{
    // Create the directory for file uploads if it does not exist.
    const std::string &uploadsPath = bdg::utils::Config::getInstance().getServerUploadsPath();
    const Poco::Path directoryPath(uploadsPath);

    // Create the directory for logging if it does not exist yet.
    try
    {
        Poco::File directory(directoryPath);
        directory.createDirectories();
    }
    catch (Poco::Exception&)
    {
        throw Poco::CreateFileException("Could not create directory [" + directoryPath.toString() + "] as an uploads storage!");
    }

    // Get the random temporary file name.
    Poco::UUIDGenerator &generator = Poco::UUIDGenerator::defaultGenerator();
    Poco::UUID randomUuid(generator.createRandom());

    temporaryFile = bdg::utils::Config::getInstance().getServerUploadsPath() + Poco::Path::separator() + randomUuid.toString();

    std::string extension(Poco::Path(name).getExtension());

    if (extension != "")
    {
        temporaryFile.append("." + extension);
    }

    // Copy file from the stream to its destination.
    Poco::CountingInputStream countingInputStream(inputStream);
    std::ofstream outputStream(temporaryFile, std::ios::binary);

    Poco::StreamCopier::copyStream(countingInputStream, outputStream);
    size = countingInputStream.chars();

    outputStream.close();
}

/**
 * Constructor used to construct fileCollections during unit testing.
 * We do not want to create a temprorary directory nor temporary filenames.
 *
 * @param name Name of the file.
 * @param contentType Type of the file.
 */
bdg::utils::FileUploadImpl::FileUploadImpl(const std::string &name, const std::string &contentType) :
    name(name),
    contentType(contentType),
    temporaryFile(name)
{
    std::ifstream ifs(name);
    Poco::CountingInputStream countingInputStream(ifs);

    size = countingInputStream.chars();
}

/**
 * Factory method creating an instance of uploaded file.
 *
 * @param name Name of the file.
 * @param contentType Type of the file.
 * @param inputStream Stream which can read uploaded file.
 *
 * @return Instance uploaded file.
 */
std::unique_ptr<bdg::utils::FileUpload> bdg::utils::FileUpload::create(const std::string &name, const std::string &contentType, std::istream &inputStream)
{
    return std::unique_ptr<FileUpload>(new FileUploadImpl(name, contentType, inputStream));
}

/**
 * Factory method creating an instance of uploaded file.
 *
 * @param name Name of the file.
 * @param contentType Type of the file.
 *
 * @return Instance uploaded file.
 */
std::unique_ptr<bdg::utils::FileUpload> bdg::utils::FileUpload::create(const std::string &name, const std::string &contentType)
{
    return std::unique_ptr<FileUpload>(new FileUploadImpl(name, contentType));
}

/**
 * Returns name of the uploaded file.
 *
 * @return Name of the uploaded file.
 */
const std::string &bdg::utils::FileUpload::getName() const
{
    return impl()->name;
}

/**
 * Returns type of the file.
 *
 * @return Type of the file.
 */
const std::string &bdg::utils::FileUpload::getContentType() const
{
    return impl()->contentType;
}

/**
 * Returns path to the file in the file system.
 *
 * @return Path to the file in the file system.
 */
const std::string &bdg::utils::FileUpload::getTemporaryFile() const
{
    return impl()->temporaryFile;
}

/**
 * Returns size of the uploaded file.
 *
 * @return Size of the uploaded file.
 */
const size_t bdg::utils::FileUpload::getSize() const
{
    return impl()->size;
}

/**
 * Returns pointer to the file upload implementation - PIMPL design pattern.
 *
 * @return Pointer to the file upload implementation.
 */
const bdg::utils::FileUploadImpl *bdg::utils::FileUpload::impl() const
{
    return static_cast<const FileUploadImpl*>(this);
}

/**
 * Returns pointer to the file upload implementation - PIMPL design pattern.
 *
 * @return Pointer to the file upload implementation.
 */
bdg::utils::FileUploadImpl *bdg::utils::FileUpload::impl()
{
    return static_cast<FileUploadImpl*>(this);
}
