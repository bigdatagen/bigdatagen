/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Utils
 * @author   Big Data Generator Team
 */

#include "utils/mpiproxy.h"     // Header
#include "utils/logger.h"       // Logger
#include "utils/exception.h"    // Exceptions

#include <iostream>             // std::cout
#include <memory>               // std::unique_ptr
#include <mpi.h>                // MPI

 /**
  * Call function X, if function doesn't return MPI_SUCCESS, throws MPIException with error code inside.
  */
#define SAFE_CALL(X) \
    do { \
        int _mpiRes = X; \
        if (_mpiRes != MPI_SUCCESS) \
            throw MpiException("MPI returned error code " + std::to_string(_mpiRes)); \
    } while(false)

/**
 * Initializes the MPI.
 *
 * @param argc Number of command line arguments.
 * @param argv Command line arguments.
 */
void bdg::utils::MPIProxy::init(int argc, char **argv)
{
    const int demanded = MPI_THREAD_FUNNELED;
    int provided;

    SAFE_CALL(MPI_Init_thread(&argc, &argv, demanded, &provided));
    SAFE_CALL(MPI_Comm_size(MPI_COMM_WORLD, &size));
    SAFE_CALL(MPI_Comm_rank(MPI_COMM_WORLD, &rank));

    nodeRankMap = new int[size];

    if (provided < demanded)
    {
        throw MpiException("Could not initialize MPI with threading support");
    }
}

/**
 * Destroys the MPI wrapper.
 */
bdg::utils::MPIProxy::~MPIProxy()
{
    delete nodeRankMap;
}

/**
 * Sends a message to the master thread.
 *
 * @param str Message to be sent.
 * @param tag Tag atttached to the message.
 */
void bdg::utils::MPIProxy::sendMessageToMaster(const std::string &str, const int tag) const
{
    sendMessageToWorker(str, MASTER_RANK, tag);
}

/**
 * Sends a message to a worker thread.
 *
 * @param str Message to be sent.
 * @param rank Rank of the worker we want to send the message to.
 * @param tag Tag atttached to the message.
 */
void bdg::utils::MPIProxy::sendMessageToWorker(const std::string &str, const int rank, const int tag) const
{
    SAFE_CALL(MPI_Send(str.c_str(), static_cast<int>(str.size()), MPI_CHAR, rank, tag, MPI_COMM_WORLD));
}

/**
 * Returns whether the worker receive a message.
 *
 * @param rank Output parameter for the rank of the thread from which the message was sent.
 *
 * @return Can the worker receive a message?
 */ 
bool bdg::utils::MPIProxy::canReceiveMessage(int &rank) const
{
    MPI_Status status;
    int flag;

    SAFE_CALL(MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, &status));

    // Some message arrived.
    if (flag)
    {
        rank = status.MPI_SOURCE;
    }

    return flag != 0 ? true : false;
}

/**
 * Receives the message from the master thread.
 *
 * @param tag Output parameter for the tag atttached to the message.
 *
 * @return Message content.
 */
std::string bdg::utils::MPIProxy::receiveMessageFromMaster(int &tag) const
{
    return receiveMessageFromWorker(MASTER_RANK, tag);
}

/**
 * Receives the message from some MPI thread.
 *
 * @param rank Rank of the thread from which we want to receive the message.
 * @param tag Output parameter for the tag atttached to the message.
 *
 * @return Message content.
 */
std::string bdg::utils::MPIProxy::receiveMessageFromWorker(const int rank, int &tag) const
{
    MPI_Status status;
    int dataLen;

    SAFE_CALL(MPI_Probe(rank, MPI_ANY_TAG, MPI_COMM_WORLD, &status));
    SAFE_CALL(MPI_Get_count(&status, MPI_CHAR, &dataLen));
    
    std::vector<char> buf(dataLen);
    SAFE_CALL(MPI_Recv(buf.data(), dataLen, MPI_CHAR, rank, MPI_ANY_TAG, MPI_COMM_WORLD, &status));
    
    std::string str(buf.data(), dataLen);
    
    tag = status.MPI_TAG;
    return str;
}

/**
 * MPI barrier - all the threads meet there.
 */
void bdg::utils::MPIProxy::barrier() const
{
    SAFE_CALL(MPI_Barrier(MPI_COMM_WORLD));
}

/**
 * Master bthread roadcasts one integer to all the MPI threads.
 *
 * @param data Output parameter for the broadcasted data.
 */
void bdg::utils::MPIProxy::broadcastInt(int &data) const
{
    SAFE_CALL(MPI_Bcast(static_cast<void *>(&data), 1, MPI_INT, 0, MPI_COMM_WORLD));
}

/**
 * Gathers data from all the MPI threads. Each thread sends one piece of data.
 *
 * @param data Data sent by the thread.
 * @param dataBuffer Data received from all MPI threads.
 */
void bdg::utils::MPIProxy::allGatherInts(int data, int *dataBuffer) const
{
    SAFE_CALL(MPI_Allgather(&data, 1, MPI_INT, dataBuffer, 1, MPI_INT, MPI_COMM_WORLD));
}

/**
 * Detects what physical nodes are present on the cluster.
 */
void bdg::utils::MPIProxy::detectNodes()
{
    // Get the node name.
    char name[BUFSIZ];
    int nameLength;
    int hash = 0;

    SAFE_CALL(MPI_Get_processor_name(name, &nameLength));

    // Count node name hash.
    for (int i = 0; i < nameLength; ++i)
    {
        hash += name[i];
    }

    // Broadcast name hashes.
    allGatherInts(hash, nodeRankMap);
}

/**
 * Returns whether threads with given ranks run on the same physical node.
 *
 * @param firstRank Rank of the first MPI thread.
 * @param secondRank Rank of the second MPI thread.
 *
 * @return Do threads run on the same physical node?
 */
bool bdg::utils::MPIProxy::areOnSameNode(const int firstRank, const int secondRank) const
{
    if (firstRank < getSize() && secondRank < getSize())
    {
        return nodeRankMap[firstRank] == nodeRankMap[secondRank];
    }
    else
    {
        return true;
    }
}

/**
 * Finalizes MPI.
 */
void bdg::utils::MPIProxy::finalize() const
{
    int mpiRes = MPI_Finalize();

    if (mpiRes != MPI_SUCCESS)
    {
        bdg_error(bdg::utils::Logger::getInstance().getLogger(getRank()), "MPI_Finalize finished unsuccesfully, result:" + std::to_string(mpiRes));
    }
}
