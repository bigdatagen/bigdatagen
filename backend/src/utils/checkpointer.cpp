/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Utils
 * @author   Big Data Generator Team
 */

#include "utils/checkpointer.h"         // Header

#include <Poco/Path.h>      // Poco::Path
#include <Poco/File.h>      // Poco::File

#include <iostream>         // std::cout
#include <streambuf>        // std::istreambuf

/**
 * Constructor of the checkpointer.
 */
bdg::utils::Checkpointer::Checkpointer() :
    Checkpointer(initId())
{
    // Nothing here.
}

/**
 * Constructor of the checkpointer.
 *
 * @param checkpointId Id of the checkpoint.
 */
bdg::utils::Checkpointer::Checkpointer(const int checkpointId) :
    checkpointId(checkpointId)
{
    // Nothing here.
}

/**
 * Creates the directory where checkpoints are stored.
 */
void bdg::utils::Checkpointer::createStorageDir() const
{
    const std::string outputPath = getPathToFile();
    const Poco::Path directoryPath(Poco::Path(outputPath).parent());

    // Create the directory for checkpointer if it does not exist yet.
    try
    {
        Poco::File directory(directoryPath);
        directory.createDirectories();
    }
    catch (...)
    {
        throw Poco::CreateFileException("Could not create directory [" + directoryPath.toString() + "] as a checkpointer storage!");
    }
}

/**
 * Writes content to the checkpoint file.
 *
 * @param stringToWrite Content to be written.
 */
void bdg::utils::Checkpointer::writeToFile(const std::string &stringToWrite) const
{
    std::ofstream file(getPathToFile(), std::ios::binary);

    if (file.fail())
    {
        throw Poco::OpenFileException("Can't open file: " + getPathToFile());
    }

    file << stringToWrite;
    file.close();
}

/**
 * Reads content from the checkpoint file.
 *
 * @param sizeToRead Size of read content.
 *
 * @return Content of the checkpoint file.
 */
std::string bdg::utils::Checkpointer::readFromFile(size_t sizeToRead) const
{
    std::ifstream file(getPathToFile(), std::ios::ate | std::ios::binary);

    std::string data;
    data.reserve(sizeToRead);
    data.assign((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());

    file.close();

    return data;
}
