/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Utils
 * @author   Big Data Generator Team
 */

#include "utils/config.h"       // Header

#include <Poco/File.h>          // Poco::File
#include <Poco/Path.h>          // Poco::Path

/**
 * Private constructor of the config - singleton pattern.
 */
bdg::utils::Config::Config()
{
    Poco::Path path(Poco::Path::current());
    Poco::File file(path.toString() + "config.ini");

    // Find the config file.
    while (!file.exists() && path.toString() != path.parent().toString())
    {
        path = path.parent();
        file = Poco::File(path.toString() + "config.ini");
    }
    
    // Open the config file.
    if (file.exists())
    {
        config = Poco::AutoPtr<Poco::Util::IniFileConfiguration>(new Poco::Util::IniFileConfiguration(file.path()));
    }
    else
    {
        throw Poco::FileNotFoundException("Config file [config.ini] not found");
    }

    checkConfig();
}

/**
 * Returns single unique instance of application configuration - singleton pattern.
 *
 * @return Instance of application configuration.
 */
bdg::utils::Config &bdg::utils::Config::getInstance()
{
    static bdg::utils::Config instance;
    return instance;
}

/**
 * Checks whether all the mandatory settings are present in the application configuration file.
 */
void bdg::utils::Config::checkConfig() const
{
    if (getMpiProgram().empty())
    {
        throw Poco::DataException("Missing required [Pipeline.MpiProgram] setting in the config file");
    }

    if (getPipelineProgram().empty())
    {
        throw Poco::DataException("Missing required [Pipeline.Program] setting in the config file");
    }

    if (getOutputStoragePath().empty())
    {
        throw Poco::DataException("Missing required [Output.StoragePath] setting in the config file");
    }

    if (getCheckpointerStoragePath().empty())
    {
        throw Poco::DataException("Missing required [Checkpointer.StoragePath] setting in the config file");
    }

    if (getLogStoragePath().empty())
    {
        throw Poco::DataException("Missing required [Logger.StoragePath] setting in the config file");
    }

    if (getServerClientPath().empty())
    {
        throw Poco::DataException("Missing required [Server.ClientPath] setting in the config file");
    }

    if (getServerUploadsPath().empty())
    {
        throw Poco::DataException("Missing required [Server.UploadsPat] setting in the config file");
    }
}
