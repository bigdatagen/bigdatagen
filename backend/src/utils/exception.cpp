/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "utils/exception.h"            // Exceptions

#include <typeinfo>     // Needed by POCO::Exception

namespace bdg
{
    namespace utils
    {
        POCO_IMPLEMENT_EXCEPTION(MpiException, Poco::Exception, "MPI")
        POCO_IMPLEMENT_EXCEPTION(TaskErrorException, Poco::Exception, "Task Error")
        POCO_IMPLEMENT_EXCEPTION(TaskFatalErrorException, Poco::Exception, "Task Fatal Error")
        POCO_IMPLEMENT_EXCEPTION(MissingPropertyException, Poco::Exception, "Missing property: ")
        POCO_IMPLEMENT_EXCEPTION(BadPropertyTypeException, Poco::Exception, "Bad type of property: ")
        POCO_IMPLEMENT_EXCEPTION(BadPropertyValueException, Poco::Exception, "Bad value of property: ")
        POCO_IMPLEMENT_EXCEPTION(BadPropertiesException, Poco::Exception, "Bad properties: ")
        POCO_IMPLEMENT_EXCEPTION(BadBoundariesException, Poco::Exception, "Bad boundaries: ")
        POCO_IMPLEMENT_EXCEPTION(BadTypeException, Poco::Exception, "Bad type: ")
        POCO_IMPLEMENT_EXCEPTION(InvalidJsonException, Poco::Exception, "Invalid json: ")
        POCO_IMPLEMENT_EXCEPTION(RootNotArrayException, Poco::Exception, "Schema is not array")
        POCO_IMPLEMENT_EXCEPTION(NotObjectException, Poco::Exception, "Type is not object: ")
        POCO_IMPLEMENT_EXCEPTION(NotArrayException, Poco::Exception, "Type is not array: ")
        POCO_IMPLEMENT_EXCEPTION(TypeRedefinitionException, Poco::Exception, "Multiple types with same name defined: ")
        POCO_IMPLEMENT_EXCEPTION(TypeNotExistsException, Poco::Exception, "Type doesn't exist: ")
        POCO_IMPLEMENT_EXCEPTION(MissingRootException, Poco::Exception, "Missing ROOT element")
        POCO_IMPLEMENT_EXCEPTION(UnknownReferenceException, Poco::Exception, "Unknown reference: ")
        POCO_IMPLEMENT_EXCEPTION(BadReferenceException, Poco::Exception, "Referencing bad type with reference: ")
        POCO_IMPLEMENT_EXCEPTION(BadReferenceTypeException, Poco::Exception, "Referencing different type from sequnce type in an sequence.")
        POCO_IMPLEMENT_EXCEPTION(InvalidProbSumException, Poco::Exception, "The probabilities do not sum up to 1")
        POCO_IMPLEMENT_EXCEPTION(TsInvalidStepException, Poco::Exception, "Invalid step format in timesequence")
        POCO_IMPLEMENT_EXCEPTION(TsInvalidTimeException, Poco::Exception, "Invalid time format in timesequence")
        POCO_IMPLEMENT_EXCEPTION(TsInvalidDateException, Poco::Exception, "Invalid date format in timesequence")
        POCO_IMPLEMENT_EXCEPTION(UnsupportedOperationException, Poco::Exception, "Unsupported operation on: ")
        POCO_IMPLEMENT_EXCEPTION(UnsupportedDistributionException, Poco::Exception, "Distribution not supported")
    }
}
