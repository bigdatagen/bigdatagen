/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Utils
 * @author   Big Data Generator Team
 */

#include "utils/commandline.h"          // Header

#include <Poco/String.h>                // Poco::replace
#include <Poco/StringTokenizer.h>       // Poco::StringTokenizer

/**
 * Parses the command passed a the first argument into a command and its arguments.
 *
 * @param cmd Command with arguments separated by white characters.
 * @param args Output vector with command arguments.
 *
 * @return Command without arguments.
 */
std::string bdg::utils::CommandLine::parseCommandLineArguments(const std::string &cmd, std::vector<std::string> &args)
{
    Poco::StringTokenizer argumentTokens(cmd, " \t\r\n");
    std::string programCmd;
    bool isInQuotes = false;

    for (const auto &oneArgument : argumentTokens)
    {
        if (oneArgument.size() > 0)
        {
#if defined(_WIN32)
            std::string argumentStr = oneArgument;
#else
            std::string argumentStr = Poco::replace(oneArgument, "\"", "");
#endif

            if (programCmd.empty() || (args.empty() && isInQuotes))
            {
                if (!isInQuotes)
                {
                    programCmd = argumentStr;
                }
                else
                {
                    programCmd.append(" " + argumentStr);
                }
            }
            else
            {
                if (!isInQuotes)
                {
                    args.push_back(argumentStr);
                }
                else
                {
                    args.back().append(" " + argumentStr);
                }
            }

            if (!isInQuotes && oneArgument.front() == '"')
            {
                isInQuotes = true;
            }

            if (isInQuotes && oneArgument.back() == '"')
            {
                isInQuotes = false;
            }
        }
    }

    return programCmd;
}
