/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file convert.cpp
* @category Utils
* @author   Big Data Generator Team
*
*/

#include "utils/convert.h"           // Header

// Nothing here.