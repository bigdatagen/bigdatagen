/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#include "core/server/fileparthandler.h"        // Header

/**
 * Handles one file uploaded over HTTP.
 *
 * @param header Headers attached to the uploaded file.
 * @param stream Stream with file content.
 */
void bdg::server::FilePartHandler::handlePart(const Poco::Net::MessageHeader &header, std::istream &stream)
{
    if (header.has("Content-Disposition"))
    {
        std::string dispositionValue;               // Should have value "form-data" after splitParameters(...) call.
        Poco::Net::NameValueCollection params;      // Collection of parameters passed in Content-Disposition header.
        Poco::Net::MessageHeader::splitParameters(header["Content-Disposition"], dispositionValue, params);

        const std::string contentType = header.get("Content-Type", "text/plain");
        const std::string fieldName = params.get("name", "");
        const std::string fileName = params.get("filename", "");

        // Was some file uploaded?
        if (fieldName != "" && fileName != "")
        {
            uploadedFiles[fieldName] = bdg::utils::FileUpload::create(fileName, contentType, stream);
        }
    }
}
