/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#include "core/server/requesthandlerfactory.h"      // Header
#include "core/server/loginrequesthandler.h"        // LoginRequestHandler
#include "core/server/pluginrequesthandler.h"       // PluginRequestHandler
#include "core/server/filerequesthandler.h"         // FileRequestHandler
#include "core/server/invalidrequesthandler.h"      // InvalidRequestHandler
#include "utils/config.h"                           // Config
#include "utils/logger.h"                           // Logger

#include <Poco/URI.h>       // Poco::URI

#include <sstream>          // std::stringstream
#include <string>           // std::string

/**
 * Constructor of the factory for request handlers.
 *
 * @param logger Event logger.
 * @param loginProvider Manager for logged in users.
 */
bdg::server::RequestHandlerFactory::RequestHandlerFactory(Poco::Logger &logger, LoginProvider &loginProvider) :
    pluginManager(bdg::utils::Logger::getInstance().getConsoleLogger(), bdg::utils::Config::getInstance().getServerPluginsPath("plugins")),
    logger(logger),
    loginProvider(loginProvider)
{
    // Nothing here.
}

/**
 * Creates object which handles passed HTTP request.
 *
 * @param request HTTP request.
 *
 * @return Object which handles passed HTTP request.
 */
Poco::Net::HTTPRequestHandler *bdg::server::RequestHandlerFactory::createRequestHandler(const Poco::Net::HTTPServerRequest &request)
{
    bdg_information(logger, "Accepted request: " + request.getURI());

    const std::string pluginPrefix = "/plugin/";
    const std::string clientPrefix = "/client/";
    const std::string &uriStr = request.getURI();

    if (uriStr == "/login/" || uriStr == "/login")
    {
        return new bdg::server::LoginRequestHandler(logger, loginProvider, bdg::server::LoginRequestHandler::Action::ACTION_LOGIN);
    }
    else if (uriStr == "/logout/" || uriStr == "/logout")
    {
        return new bdg::server::LoginRequestHandler(logger, loginProvider, bdg::server::LoginRequestHandler::Action::ACTION_LOGOUT);
    }
    else if (uriStr == "/check-login/" || uriStr == "/check-login")
    {
        return new bdg::server::LoginRequestHandler(logger, loginProvider, bdg::server::LoginRequestHandler::Action::ACTION_CHECK_LOGIN);
    }
    else if (uriStr == "/requires-login/" || uriStr == "/requires-login")
    {
        return new bdg::server::LoginRequestHandler(logger, loginProvider, bdg::server::LoginRequestHandler::Action::ACTION_REQUIRES_LOGIN);
    }
    else if (!uriStr.compare(0, pluginPrefix.size(), pluginPrefix) || uriStr == "/plugin")
    {
        return new bdg::server::PluginRequestHandler(pluginManager, logger, loginProvider);
    }
    else if (!uriStr.compare(0, clientPrefix.size(), clientPrefix) || uriStr == "/client" || uriStr == "/favicon.ico")
    {
        Poco::URI uri;

        // Redirect requests for 'favicon.ico' to '/client/favicon.ico'.
        if (uriStr == "/favicon.ico")
        {
            uri = Poco::URI(uriStr);
        }
        // Process normal request.
        else
        {
            uri = Poco::URI(uriStr.substr(clientPrefix.size() - 1));
        }
        
        return new bdg::server::FileRequestHandler(uri.getPath(), logger);
    }
    else
    {
        return new bdg::server::InvalidRequestHandler(logger);
    }
}
