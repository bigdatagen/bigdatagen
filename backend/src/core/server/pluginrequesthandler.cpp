/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#include "core/server/pluginrequesthandler.h"       // Header
#include "utils/config.h"                           // Config
#include "utils/logger.h"                           // Logger

#include <Poco/StreamCopier.h>          // Poco::StreamCopier
#include <Poco/StringTokenizer.h>       // Poco::StringTokenizer
#include <Poco/Net/HTMLForm.h>          // Poco::Net::HTMLForm

/**
 * Constructor of the request handler.
 *
 * @param pluginManager Manager for server plugins.
 * @param logger Event logger.
 * @param loginProvider Manager for logged in users.
 */
bdg::server::PluginRequestHandler::PluginRequestHandler(const bdg::utils::PluginManager<bdg::server::ServerPlugin> &pluginManager, Poco::Logger &logger, LoginProvider &loginProvider) :
    pluginManager(pluginManager),
    logger(logger),
    loginProvider(loginProvider)
{
    // Nothing here.
}

/**
 * Handles one request to the server. Calls the plugin which is in the "plugin"
 * parameter in data posted to the server.
 *
 * @param request HTTP request.
 * @param response HTTP response.
 */
void bdg::server::PluginRequestHandler::handleRequest(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response)
{
    response.setChunkedTransferEncoding(true);
    response.setContentType("text/plain");

    std::stringstream responseStreamData;
    
    if (request.getMethod() != "POST")
    {
        bdg_error(logger, "ERROR: Invalid plugin request HTTP method, must be POST!");
        responseStreamData << R"({"error": "Invalid request HTTP method, must be POST!"})";
    }
    else if (!loginProvider.getUser(request, response).isLoggedIn() && bdg::utils::Config::getInstance().useServerLogin())
    {
        bdg_error(logger, "ERROR: Attempt to run plugin when not logged in!");
        responseStreamData << R"({"error": "You have to be logged in!"})";
    }
    else
    {
        // Get the data sent in the request.
        std::stringstream requestStream;
        Poco::StreamCopier::copyStream(request.stream(), requestStream);

        // Find the plugin that should be called.
        ServerPlugin::Parameters pluginParameters;
        std::string pluginName;
        
        // Plugin name can be embeded in the URL.
        handleDataRequest(request, requestStream, pluginName, pluginParameters);

        // Handle data sent by form.
        FilePartHandler filePartHandler;
        handleFormRequest(request, requestStream, filePartHandler, pluginName, pluginParameters);

        // Plugins may throw exceptions, so we want to catch them and log them.
        try
        {
            // Plugin found, run it.
            if (pluginManager.hasPluginSingleton(pluginName))
            {
                bdg_information(logger, "Running plugin singleton [" + pluginName + "]");
                responseStreamData << pluginManager.getPluginSingleton(pluginName).run(pluginParameters, filePartHandler.getUploadedFiles());
            }
            else if (pluginManager.hasPlugin(pluginName))
            {
                bdg_information(logger, "Running plugin [" + pluginName + "]");
                responseStreamData << pluginManager.getPlugin(pluginName)->run(pluginParameters, filePartHandler.getUploadedFiles());
            }
            else
            {
                bdg_error(logger, "ERROR: Invalid request, plugin [" + pluginName + "] not found!");
                responseStreamData << R"({"error": "Invalid request, plugin [)" << pluginName << R"(] not found!"})";
            }
        }
        catch (Poco::Exception &e)
        {
            bdg_error(logger, "ERROR: Failed to process the plugin request");
            bdg_debug(logger, "Exception: " + e.displayText());
            responseStreamData << R"({"error": "Failed to process the request: )" << e.displayText() << R"(!"})";
        }
        catch (std::exception &e)
        {
            bdg_error(logger, "ERROR: Failed to process the plugin request");
            bdg_debug(logger, "Exception: " + std::string(e.what()));
            responseStreamData << R"({"error": "Failed to process the request: )" << e.what() << R"(!"})";
        }
    }

    // Send the response.
    std::ostream &responseStream = response.send();
    Poco::StreamCopier::copyStream(responseStreamData, responseStream);
}

/**
 * Handles one request to the server done by data method, e.g. by sending JSON.
 *
 * @param request HTTP request.
 * @param requestStream Data sent by request.
 * @param pluginName Output parameter for requested plugin.
 * @param pluginParameters Output parameter for parameters that should be passed to the requested plugin.
 */
void bdg::server::PluginRequestHandler::handleDataRequest(
    const Poco::Net::HTTPServerRequest &request, 
    std::stringstream &requestStream,
    std::string &pluginName, 
    ServerPlugin::Parameters &pluginParameters) const
{
    std::string uri = Poco::replace(request.getURI(), "/plugin", "");
    Poco::StringTokenizer uriParts(uri, "/");

    if (uriParts.count() >= 2 && uriParts[1] != "")
    {
        pluginName = uriParts[1];
    }

    pluginParameters["configuration"] = requestStream.str();
}

/**
 * Handles one request to the server done by sending some form.
 *
 * @param request HTTP request.
 * @param requestStream Data sent by request.
 * @param filePartHandler Handler for uploaded files.
 * @param pluginName Output parameter for requested plugin.
 * @param pluginParameters Output parameter for parameters that should be passed to the requested plugin.
 */
void bdg::server::PluginRequestHandler::handleFormRequest(
    const Poco::Net::HTTPServerRequest &request,
    std::stringstream &requestStream,
    FilePartHandler &filePartHandler,
    std::string &pluginName,
    ServerPlugin::Parameters &pluginParameters) const
{
    try
    {
        Poco::Net::HTMLForm form(request, requestStream, filePartHandler);

        // Handle POST data.
        for (Poco::Net::NameValueCollection::ConstIterator it = form.begin(); it != form.end(); ++it)
        {
            if (it->first == "plugin")
            {
                pluginName = it->second;
            }
            else
            {
                pluginParameters[it->first] = it->second;
            }
        }
    }
    catch (...)
    {
        // We just need to handle the error with wrong encoding due to the data sent to
        // server not by form (but as plain text, e.g. in case of JSON request).
    }
}
