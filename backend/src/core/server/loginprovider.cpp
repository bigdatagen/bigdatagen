/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#include "core/server/loginprovider.h"          // Header
#include "utils/config.h"                       // Config

#include <Poco/JSON/Object.h>           // Poco::JSON::Object
#include <Poco/JSON/Parser.h>           // Poco::JSON::Parser
#include <Poco/StreamCopier.h>          // Poco::StreamCopier
#include <Poco/UUID.h>                  // Poco::UUID
#include <Poco/UUIDGenerator.h>         // Poco::UUIDGenerator
#include <Poco/Net/HTTPCookie.h>        // Poco::Net::HTTPCookie

const std::string bdg::server::LoginProvider::ACCESS_TOKEN_COOKIE_NAME = "accessToken";
const int bdg::server::LoginProvider::ACCESS_TOKEN_COOKIE_EXPIRATION_SECONDS = 3600;

/**
 * Handles the login of user. Stores the cookie which identifies him for further communication.
 *
 * @param request HTTP request.
 * @param response HTTP response.
 *
 * @return Identity of logged in user.
 */
const bdg::server::User bdg::server::LoginProvider::login(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response)
{
    std::string requestData;
    Poco::StreamCopier::copyToString(request.stream(), requestData);

    // Parse the request JSON.
    Poco::JSON::Parser parser;
    Poco::Dynamic::Var requestVar = parser.parse(requestData);
    Poco::JSON::Object::Ptr requestJson = requestVar.extract<Poco::JSON::Object::Ptr>();
   
    const std::string username = requestJson->get("username").toString();
    const std::string password = requestJson->get("password").toString();
    const std::string requiredUsername = bdg::utils::Config::getInstance().getServerLoginUsername();
    const std::string requiredPassword = bdg::utils::Config::getInstance().getServerLoginPassword();

    // Check username and password.
    if (username == requiredUsername && password == requiredPassword)
    {
        // Store cookie with randomly generated hash which represents our login session.
        Poco::UUIDGenerator &generator = Poco::UUIDGenerator::defaultGenerator();
        const Poco::UUID randomUuid(generator.createRandom());
        const std::string accessToken = randomUuid.toString();

        storeAccessTokenCookie(response, accessToken, ACCESS_TOKEN_COOKIE_EXPIRATION_SECONDS);

        // Store the identity of newly logged in user.
        User user(username, true);
        loggedUsers.emplace(accessToken, user);

        return user;
    }
    else
    {
        return User(username);
    }
}

/**
 * Gets the identity of logged in user from the cookies sent with HTTP request.
 *
 * @param request HTTP request.
 * @param response HTTP response.
 *
 * @return Identity of logged in user.
 */
const bdg::server::User bdg::server::LoginProvider::getUser(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response) const
{
    const std::string accessToken = getAccessTokenFromCookie(request);
    const UserCollection::const_iterator it = loggedUsers.find(accessToken);

    // User logged in.
    if (it != loggedUsers.end())
    {
        // Prolong the cookie expiration.
        storeAccessTokenCookie(response, accessToken, ACCESS_TOKEN_COOKIE_EXPIRATION_SECONDS);
        return it->second;
    }
    // User not logged in.
    else
    {
        return User();
    }
}

/**
 * Logs out the logged in user.
 *
 * @param request HTTP request.
 * @param response HTTP response.
 */
void bdg::server::LoginProvider::logout(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response)
{
    const std::string accessToken = getAccessTokenFromCookie(request);

    // Remove the access token and delete the cookie.
    loggedUsers.erase(accessToken);
    storeAccessTokenCookie(response, "", 0);
}

/**
 * Stores the cookie with access token identifying logged in user.
 *
 * @param response HTTP response.
 * @param accessToken Token identifying logged in user.
 * @param maxAge Number of seconds from now when the cookie expires.
 */
void bdg::server::LoginProvider::storeAccessTokenCookie(Poco::Net::HTTPServerResponse &response, const std::string &accessToken, const int maxAge) const
{
    Poco::Net::HTTPCookie cookie(ACCESS_TOKEN_COOKIE_NAME, accessToken);
    cookie.setPath("/");
    cookie.setMaxAge(maxAge);

    response.addCookie(cookie);
}

/**
 * Returns the access token stored in the cookie sent via HTTP request.
 *
 * @param request HTTP request.
 *
 * @return Access token stored in the cookie.
 */
const std::string bdg::server::LoginProvider::getAccessTokenFromCookie(Poco::Net::HTTPServerRequest &request) const
{
    Poco::Net::NameValueCollection cookies;
    request.getCookies(cookies);

    const Poco::Net::NameValueCollection::ConstIterator it = cookies.find(ACCESS_TOKEN_COOKIE_NAME);

    if (it != cookies.end())
    {
        return it->second;
    }
    else
    {
        return "";
    }
}
