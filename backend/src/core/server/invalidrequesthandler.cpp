/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#include "core/server/invalidrequesthandler.h"      // Header

/**
 * Constructor of the request handler.
 *
 * @param logger Event logger.
 */
bdg::server::InvalidRequestHandler::InvalidRequestHandler(Poco::Logger &logger) :
    logger(logger)
{
    // Nothing here.
}

/**
 * Handles one invalid request to the server.
 *
 * @param request HTTP request.
 * @param response HTTP response.
 */
void bdg::server::InvalidRequestHandler::handleRequest(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response)
{
    response.setChunkedTransferEncoding(true);
    response.setContentType("text/plain");

    std::ostream &responseStream = response.send();
    responseStream << "Invalid request!";

    bdg_error(logger, "ERROR: Invalid request [" + request.getURI() + "]!");
}
