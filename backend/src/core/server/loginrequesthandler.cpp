/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#include "core/server/loginrequesthandler.h"        // Header
#include "utils/config.h"                           // Config
#include "utils/logger.h"                           // Logger

#include <Poco/JSON/Object.h>           // Poco::JSON::Object
#include <Poco/StreamCopier.h>          // Poco::StreamCopier

/**
 * Constructor of the request handler.
 *
 * @param logger Event logger.
 * @param loginProvider Manager for logged in users.
 * @param action Which actions should be handled.
 */
bdg::server::LoginRequestHandler::LoginRequestHandler(Poco::Logger &logger, LoginProvider &loginProvider, Action action) :
    logger(logger),
    loginProvider(loginProvider),
    action(action)
{
    // Nothing here.
}

/**
 * Handles a login request to the server.
 *
 * @param request HTTP request.
 * @param response HTTP response.
 */
void bdg::server::LoginRequestHandler::handleRequest(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response)
{
    response.setChunkedTransferEncoding(true);
    response.setContentType("application/json");

    std::stringstream responseStreamData;

    if (request.getMethod() == "POST")
    {
        try
        {
            Poco::JSON::Object responseJson;

            // Perform login action.
            if (action == Action::ACTION_LOGIN)
            {
                User user = loginProvider.login(request, response);

                // Check the username and password.
                if (user.isLoggedIn())
                {
                    bdg_information(logger, "Successfully logged in as [" + user.getUsername() + "]");
                    responseJson.set("result", "success");
                }
                else
                {
                    bdg_information(logger, "Login with username [" + user.getUsername() + "] failed");
                    responseJson.set("result", "failure");
                }

                responseJson.stringify(responseStreamData);
            }
            // Perform logout action.
            else if (action == Action::ACTION_LOGOUT)
            {
                loginProvider.logout(request, response);
                responseJson.set("result", "success");
                responseJson.stringify(responseStreamData);
            }
            // Check user login.
            else if (action == Action::ACTION_CHECK_LOGIN)
            {
                responseJson.set("result", loginProvider.getUser(request, response).isLoggedIn());
                responseJson.stringify(responseStreamData);
            }
            // Perform logout action.
            else if (action == Action::ACTION_REQUIRES_LOGIN)
            {
                responseJson.set("result", bdg::utils::Config::getInstance().useServerLogin());
                responseJson.stringify(responseStreamData);
            }
            // Invalid action.
            else
            {
                bdg_error(logger, "ERROR: Invalid login action");
                responseStreamData << R"({"error": "Invalid login action!"})";
            }
        }
        catch (Poco::Exception &e)
        {
            bdg_error(logger, "ERROR: Failed to process the login request");
            bdg_debug(logger, "Exception: " + e.displayText());
            responseStreamData << R"({"error": "Failed to process the request: )" << e.displayText() << R"(!"})";
        }
    }
    else
    {
        bdg_error(logger, "ERROR: Invalid login request HTTP method, must be POST!");
        responseStreamData << R"({"error": "Invalid request HTTP method, must be POST!"})";
    }

    // Send the response.
    std::ostream &responseStream = response.send();
    Poco::StreamCopier::copyStream(responseStreamData, responseStream);
}
