/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#include "core/server/filerequesthandler.h"         // Header
#include "utils/config.h"                           // Config
#include "utils/logger.h"                           // Logger

#include <Poco/File.h>          // Poco::File
#include <Poco/Path.h>          // Poco::Path
#include <Poco/String.h>        // Poco::replace
#include <Poco/StreamCopier.h>  // Poco::StreamCopier
#include <Poco/URI.h>           // Poco::URI

#include <fstream>              // std::fstream

/**
 * Content of error 404 page.
 */
const std::string bdg::server::FileRequestHandler::NOT_FOUND_HTML = R"(<!DOCTYPE html>
<html>
    <head>
        <title>Not found!</title>
    </head>
    <body>
        <p>
            Error 404: File <strong>{$fileName}</strong> not found on this server!
        </p>
    </body>
</html>)";

/**
 * Constructor of the request handler.
 *
 * @param file Relative path to the requested file.
 * @param logger Event logger.
 */
bdg::server::FileRequestHandler::FileRequestHandler(const std::string &file, Poco::Logger &logger) :
    file(file),
    logger(logger)
{
    // Nothing here.
}

/**
 * Handles one request to the server. Reads the contents of file requested in the
 * URL and returns this content. If a file does not exist, returns error 404.
 *
 * @param request HTTP request.
 * @param response HTTP response.
 */
void bdg::server::FileRequestHandler::handleRequest(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response)
{
    response.setChunkedTransferEncoding(true);

    // Creating a Poco::Path object normalizes the path, i.e. we get rid of potential "../" parts
    // of the path which would be security gap.
    const Poco::Path clientDirectoryPath(bdg::utils::Config::getInstance().getServerClientPath());
    const std::string clientDirectoryPathStr = clientDirectoryPath.toString();
    const std::string &uriStr = request.getURI();
    
    Poco::File requestedFile(clientDirectoryPathStr + file);
    
    if (requestedFile.exists() && requestedFile.isDirectory())
    {
        // If we want a content of directory and URL does not end with "/", redirect.
        if (uriStr.substr(uriStr.size() - 1) != "/")
        {
            response.redirect(uriStr + "/", Poco::Net::HTTPServerResponse::HTTP_MOVED_PERMANENTLY);
            return;
        }

        requestedFile = Poco::File(requestedFile.path() + Poco::Path::separator() + "index.html");
    }
    
    const Poco::Path requestedFilePath(requestedFile.path());
    const std::string requestedFilePathStr = requestedFilePath.toString();
    
    // Test whether the requested file is in the configured root directory.
    if (!requestedFilePathStr.compare(0, clientDirectoryPathStr.size(), clientDirectoryPathStr) && requestedFile.exists())
    {
        response.setStatus(Poco::Net::HTTPResponse::HTTPStatus::HTTP_OK);
        setContentType(Poco::toLower(requestedFilePath.getExtension()), response);

        std::ostream &responseStream = response.send();
        std::ifstream ifs(requestedFilePathStr, std::ios::binary);
        Poco::StreamCopier::copyStream(ifs, responseStream);
        ifs.close();

        bdg_information(logger, "File [" + requestedFilePath.getFileName() + "] OK");
    }
    // File not found -> error 404.
    else
    {
        response.setStatus(Poco::Net::HTTPResponse::HTTPStatus::HTTP_NOT_FOUND);
        response.setContentType("text/html");

        std::ostream &responseStream = response.send();
        responseStream << Poco::replace(NOT_FOUND_HTML, "{$fileName}", file.c_str());

        bdg_error(logger, "ERROR: File [" + file + "] not found!");
    }
}

/**
 * Sets the right content-type to the response.
 *
 * @param extension File extension.
 * @param response HTTP response.
 *
 * @return Should the response be in binary format?
 */
bool bdg::server::FileRequestHandler::setContentType(const std::string &extension, Poco::Net::HTTPServerResponse &response)
{
    bool isBinary = false;

    // Set the appropriate content-type.
    if (extension == "js")
    {
        response.setContentType("application/javascript");
    }
    else if (extension == "css")
    {
        response.setContentType("text/css");
    }
    else if (extension == "txt")
    {
        response.setContentType("text/plain");
    }
    else if (extension == "png")
    {
        response.setContentType("image/png");
        isBinary = true;
    }
    else if (extension == "ico")
    {
        response.setContentType("image/x-icon");
        isBinary = true;
    }
    else if (extension == "jpeg")
    {
        response.setContentType("image/jpeg");
        isBinary = true;
    }
    else if (extension == "gif")
    {
        response.setContentType("image/gif");
        isBinary = true;
    }
    else if (extension == "woff")
    {
        response.setContentType("application/font-woff");
        isBinary = true;
    }
    else if (extension == "woff2")
    {
        response.setContentType("application/font-woff2");
        isBinary = true;
    }
    else if (extension == "ttf")
    {
        response.setContentType("application/octet-stream");
        isBinary = true;
    }
    else
    {
        response.setContentType("text/html");
    }
    
    return isBinary;
}
