/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#include "core/server/server.h"         // Server

/**
 * Entry point of the server.
 *
 * @param argc Number of arguments passed from the command line.
 * @param argv Arguments passed from the command line.
 *
 * @return Return code for the operating system.
 */
int main(int argc, char **argv)
{
    bdg::server::Server server;
    return server.run(argc, argv);
}
