/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#include "core/server/server.h"                     // Header
#include "core/server/servererrorhandler.h"         // ServerErrorHandler
#include "core/server/requesthandlerfactory.h"      // PluginRequestHandlerFactory
#include "sockets/socketplugin.h"                   // SocketPlugin
#include "utils/config.h"                           // Config
#include "utils/logger.h"                           // Logger

#include <Poco/Net/HTTPServer.h>                    // Poco::Net::HTTPServer
#include <Poco/Net/HTTPServerParams.h>              // Poco::Net::HTTPServerParams
#include <Poco/Net/ServerSocket.h>                  // Poco::Net::ServerSocket
#include <Poco/Util/ServerApplication.h>            // Poco::Util::ServerApplication
#include <Poco/SimpleFileChannel.h>                 // Poco::SimpleFileChannel
#include <Poco/FormattingChannel.h>                 // Poco::FormattingChannel
#include <Poco/PatternFormatter.h>                  // Poco::PatternFormatter

/**
 * Initializes the server.
 *
 * @param self This server.
 */
void bdg::server::Server::initialize(Poco::Util::Application &self)
{
    loadConfiguration();
    ServerApplication::initialize(self);
}

/**
 * Main method of the server - starts the server which waits for the HTTP
 * requests from some client.
 *
 * @param args Arguments passed from the command line.
 *
 * @return Return code for the operating system.
 */
int bdg::server::Server::main(const std::vector<std::string> &args)
{
    // Check configuration.
    try
    {
        bdg::utils::Config::getInstance();
    }
    catch (Poco::Exception &e)
    {
        std::cerr << "ERROR: " << e.displayText() << "!" << std::endl;
        return Poco::Util::Application::EXIT_CONFIG;
    }
    catch (std::exception &e)
    {
        std::cerr << "ERROR: " << e.what() << "!" << std::endl;
        return Poco::Util::Application::EXIT_CONFIG;
    }

    bdg::utils::Config &config = bdg::utils::Config::getInstance();
    Poco::Logger &logger = bdg::utils::Logger::getInstance().getServerLogger();

    // Set the exception handler.
    Poco::ErrorHandler::set(new ServerErrorHandler(logger));

    // Create a socket.
    std::string mode = "http";
    std::string socketPluginName = Poco::toLower(config.getServerSocket());
    std::unique_ptr<Poco::Net::ServerSocket> socket;

    if (socketPluginName == "default")
    {
        socket.reset(new Poco::Net::ServerSocket(config.getServerPort()));
    }
    else
    {
        socket.reset(createSocket(socketPluginName));
        mode = Poco::replace(Poco::replace(socketPluginName, "server", ""), "plugin", "");;
    }
    
    if (socket == nullptr)
    {
        std::cerr << "ERROR: Could not create a listening socket! Used socket plugin: [" << config.getServerSocket() << "]." << std::endl;
        return Poco::Util::Application::EXIT_CONFIG;
    }

    // Run the server.
    Poco::Net::HTTPServer server(new bdg::server::RequestHandlerFactory(logger, loginProvider), *socket, new Poco::Net::HTTPServerParams);
    server.start();

    std::cout << "Server is running on port " << config.getServerPort()  << " in " << mode <<" mode..." << std::endl;

    waitForTerminationRequest();
    server.stop();

    return Poco::Util::Application::EXIT_OK;
}

/**
 * Tries to load the specified socket plugin and create a socket for listening.
 *
 * @param socketPluginName Name of the used socket plugin.
 *
 * @return Created server listening socket.
 */
Poco::Net::ServerSocket *bdg::server::Server::createSocket(const std::string &socketPluginName)
{
    bdg::utils::Config &config = bdg::utils::Config::getInstance();
    Poco::Logger &logger = bdg::utils::Logger::getInstance().getServerLogger();

    const bdg::utils::PluginManager<bdg::server::SocketPlugin> pluginManager(bdg::utils::Logger::getInstance().getConsoleLogger(), config.getServerSocketPluginsPath("socketplugins"));
    
    try
    {
        // Plugin found, run it.
        if (pluginManager.hasPluginSingleton(socketPluginName))
        {
            bdg_information(logger, "Using socket plugin singleton [" + socketPluginName + "]");
            return pluginManager.getPluginSingleton(socketPluginName).createSocket();
        }
        else if (pluginManager.hasPlugin(socketPluginName))
        {
            bdg_information(logger, "Using socket plugin [" + socketPluginName + "]");
            return pluginManager.getPlugin(socketPluginName)->createSocket();
        }
        else
        {
            bdg_error(logger, "ERROR: Socket plugin [" + socketPluginName + "] not found!");
        }
    }
    catch (Poco::Exception &e)
    {
        bdg_error(logger, "ERROR: Failed to create a socket with plugin [" + socketPluginName +"]");
        bdg_debug(logger, "Exception: " + e.displayText());
        UNREFERENCED_VARIABLE(e);
    }
    catch (std::exception &e)
    {
        bdg_error(logger, "ERROR: Failed to create a socket with plugin [" + socketPluginName + "]");
        bdg_debug(logger, "Exception: " + std::string(e.what()));
        UNREFERENCED_VARIABLE(e);
    }

    return nullptr;
}
