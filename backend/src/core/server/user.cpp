/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#include "core/server/user.h"           // Header

/**
 * Constructor of the user.
 *
 * @param username Username of logged in user.
 * @param loggedIn Is the user currently logged in?
 */
bdg::server::User::User(const std::string &username, const bool loggedIn) :
    username(username),
    loggedIn(loggedIn)
{
    // Nothing here.
}
