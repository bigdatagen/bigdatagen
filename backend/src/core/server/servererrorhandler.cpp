/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#include "core/server/servererrorhandler.h"         // Header

#include <iostream>     // std::cerr

/**
 * Constructor of the error handler.
 *
 * @param logger Event logger.
 * @param displayErrors Display exceptions on stderr?
 */
bdg::server::ServerErrorHandler::ServerErrorHandler(Poco::Logger &logger, const bool displayErrors) :
    logger(logger),
    displayErrors(displayErrors)
{
    // Nothing here.
}

/**
 * Handles unhandled poco exception.
 *
 * @param exception Unhandled exception.
 */
void bdg::server::ServerErrorHandler::exception(const Poco::Exception &exception)
{
    handleException(exception.displayText());
}

/**
 * Handles unhandled std exception.
 *
 * @param exception Unhandled exception.
 */
void bdg::server::ServerErrorHandler::exception(const std::exception &exception)
{
    handleException(exception.what());
}

/**
 * Handles unhandled unknown exception.
 */
void bdg::server::ServerErrorHandler::exception()
{
    handleException("Unknown exception");
}

/**
 * Logs a message from an unhandled exception.
 *
 * @param message Message to be logged.
 */
void bdg::server::ServerErrorHandler::handleException(const std::string &message)
{
    bdg_error(logger, "Unhandled exception: " + message);

    if (displayErrors)
    {
        std::cerr << "Unhandled exception: " << message << std::endl;
    }
}
