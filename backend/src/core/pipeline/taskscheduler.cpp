/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/taskscheduler.h"                        // Header
#include "core/pipeline/plugins/filetransferer.h"               // FileTransferer
#include "core/pipeline/plugins/errortaskresult.h"              // ErrorTaskResult
#include "core/pipeline/plugins/fatalerrortaskresult.h"         // FatalErrorTaskResult
#include "core/pipeline/messagetype.h"                          // MessageType

#include <Poco/String.h>        // Poco::String

#include <thread>       // std::this_thread::sleep_for
#include <chrono>       // std::chrono::seconds

using bdg::utils::MPIProxy;
using bdg::core::pipeline::MessageType;
using bdg::core::pipeline::Task;
using bdg::core::pipeline::plugin::Plugin;

const size_t bdg::core::pipeline::TaskScheduler::ALLOWED_TASK_RESTARTS = 1;

/**
 * Constructor of the task scheduler.
 *
 * @param pipeline Instance of the pipeline with plugins.
 * @param mpi MPI wrapper.
 */
bdg::core::pipeline::TaskScheduler::TaskScheduler(const bdg::core::pipeline::Pipeline &pipeline, const MPIProxy &mpi) :
    pipeline(pipeline),
    mpi(mpi),
    workers(pipeline, mpi.getSize(), pipeline.getPluginsCount()),
    resultQueue(mpi, pipeline.getInfo().getSchedulerInfo(), workers.getSize(), pipeline.getPluginsCount(), pipeline.getResultQueuesUsedAsPriorityQueue()),
    taskQueue(pipeline, workers.getSize(), pipeline.getPluginsCount()),
    taskFactory(pipeline, resultQueue),
    progressCounter(pipeline.getInfo().getSchedulerInfo().getTotal()),
    speedLimiterScheduler(pipeline.getInfo().getSchedulerInfo(), pipeline.getPluginsCount(), workers.getSize()),
    statisticsCounter(progressCounter, workers.getSize(), pipeline.getPluginsCount()),
    checkpointer(pipeline.getJobId(), pipeline.getInfo().getSchedulerInfo().getSchedulerCheckpointPeriodSeconds()),
    monitoringCheckpointer(pipeline.getJobId()),
    progressRunnable(*this, &TaskScheduler::progressThreadWork),
    monitoringRunnable(*this, &TaskScheduler::monitoringThreadWork)
{
    // Nothing here.
}

/**
 * Main loop of the master thread. Sends messages with tasks to workers and receives
 * responses with results of task processing.
 */
void bdg::core::pipeline::TaskScheduler::schedule()
{
    if (!checkConfiguration())
    {
        return;
    }
    
    // Initialization.
    Poco::Logger &logger = getLogger();
    const bdg::core::pipeline::SchedulerInfo &schedulerInfo = pipeline.getInfo().getSchedulerInfo();

    int workerRank;
    int tag;
    
    progressThread.start(progressRunnable);
    monitoringThread.start(monitoringRunnable);

    // Log the generation target.
    switch (schedulerInfo.getTotal().getUnit())
    {
        case ValueWithUnit::Unit::UNIT_BYTE:
            bdg_information(logger, "Generating " + std::to_string(schedulerInfo.getTotal().getValue()) + " bytes of data");
            break;

        case ValueWithUnit::Unit::UNIT_FILE:
            bdg_information(logger, "Generating " + std::to_string(schedulerInfo.getTotal().getValue()) + " files");
            break;

        case ValueWithUnit::Unit::UNIT_SECOND:
            bdg_information(logger, "Generating for " + std::to_string(schedulerInfo.getTotal().getValue()) + " seconds");
            break;
    }
    
    // Main task loop.
    sendInitialCommandsToWorkers();
    
    while (!isJobFinished())
    {
        // Asynchronously wait for a message from some worker.
        if (mpi.canReceiveMessage(workerRank))
        {
            // Receive the message.
            std::string messageData = mpi.receiveMessageFromWorker(workerRank, tag);
            bdg_debug(logger, "Received message " + bdg::core::pipeline::translateMessageType(tag) + " from " + std::to_string(workerRank) + "; Generated Files: " + std::to_string(progressCounter.getGeneratedFiles()) + ", Promissed Files: " + std::to_string(progressCounter.getPromissedFiles()) + ", Received Data : " + Poco::replace(messageData, "\n", " ").substr(0, 250));

            // Some task has been completed.
            if (tag == MessageType::MESSAGE_TYPE_TASK_COMPLETED)
            {
                // Unserialize a task result.
                std::unique_ptr<bdg::core::pipeline::TaskResult> taskResult(new bdg::core::pipeline::SuccessTaskResult);
                taskResult->unserialize(messageData);

                // Process the result - store it in a queue.
                processSuccessTaskResult(workerRank, taskResult);
                
                // Communicate with the worker.
                sendCommandToWorker(workerRank);
            }
            // Worker has been initialized.
            else if (tag == MessageType::MESSAGE_TYPE_INITIALIZED)
            {
                sendCommandToWorker(workerRank);
            }
            // Worker has been idle.
            else if (tag == MessageType::MESSAGE_TYPE_IDLE)
            {
                sendCommandToWorker(workerRank);
            }
            // Worker (processing storer plugin) has stored some more bytes of data.
            else if (tag == MessageType::MESSAGE_TYPE_BYTES_STORED)
            {
                speedLimiterScheduler.handleBytesStoredMessage(messageData);
                progressCounter.handleBytesStoredMessage(messageData);
            }
            // There has been an error during task execution.
            else if (tag == MessageType::MESSAGE_TYPE_TASK_ERROR)
            {
                std::unique_ptr<bdg::core::pipeline::TaskResult> taskResult(new bdg::core::pipeline::ErrorTaskResult);
                taskResult->unserialize(messageData);

                handleErrorResult(workerRank, taskResult);
                sendCommandToWorker(workerRank);
            }
            // There has been a fatal error during task execution.
            else if (tag == MessageType::MESSAGE_TYPE_FATAL_ERROR)
            {
                handleFatalError(messageData);
                sendCommandToWorker(workerRank);
            }
            // Worker has finished.
            else if (tag == MessageType::MESSAGE_TYPE_JOB_COMPLETED)
            {
                workers.setWorkerState(workerRank, Worker::State::STATE_FINISHED);
            }
            // Unknown message.
            else
            {
                handleFatalError("Scheduler - unexpected message " + std::to_string(tag));
            }
        }
        // Pre-generate tasks to a queue.
        else
        {
            generateTaskToQueue();
        }

        periodicWork();
    }
    
    writeStatistics();
    cleanUp();
}

/**
 * Serializes the state of this object into JSON.
 *
 * @param json JSON where we serialize the state.
 */
void bdg::core::pipeline::TaskScheduler::serialize(Poco::JSON::Object &json) const
{
    json.set("nextFileIdGlobal", nextFileIdGlobal);
    json.set("totalPromissedFiles", totalPromissedFiles);
}

/**
 * Deserializes the state of this object from the given JSON.
 *
 * @param json JSON containing the state of this object.
 */
void bdg::core::pipeline::TaskScheduler::unserialize(const Poco::JSON::Object &json)
{
    nextFileIdGlobal = json.getValue<Poco::UInt64>("nextFileIdGlobal");
    totalPromissedFiles = json.getValue<Poco::UInt64>("totalPromissedFiles");
}

/**
 * Implementation of the progress thread - it periodically writes information about
 * job progress to stdout.
 */
void bdg::core::pipeline::TaskScheduler::progressThreadWork()
{
    while (!isJobTerminating())
    {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        std::cout << "Processing... " << progressCounter.getProgressGenerated() << "%" << std::endl;
    }
}

/**
 * Implementation of the monitoring thread - it periodically stores a monitoring checkpoint.
 */
void bdg::core::pipeline::TaskScheduler::monitoringThreadWork()
{
    while (!isJobTerminating())
    {
        std::this_thread::sleep_for(std::chrono::seconds(MonitoringCheckpointer::CHECKPOINT_PERIOD_SECONDS));
        monitoringCheckpointer.store(this);
    }
}

/**
 * Method called in the main scheduler loop when there is no message to be received
 * from some worker. Beside other things, it stores checkpoints.
 */
void bdg::core::pipeline::TaskScheduler::periodicWork()
{
    // It might happen that some generated files are not counted in, so we handle this situation.
    if (!forceTerminate && progressCounter.getProgressPromissed() == 100 && workers.getProcessingWorkersCount() == 0)
    {
        progressCounter.syncGeneratedAndPromissed();
    }

    // Stop the job?
    if (isJobStopped())
    {
        forceTerminate = true;
    }

    speedLimiterScheduler.tryUpdateOptimalCurrentStorersCount();
    progressCounter.updateTimes();
    checkpointer.tryStore(this);
}

/**
 * Returns task that should be processed on the worker with a given rank. This method can
 * also return nullptr when there is no task to be processed.
 *
 * @param workerRank Rank of the worker.
 *
 * @return Task that should be processed on the worker.
 */
std::unique_ptr<bdg::core::pipeline::Task> bdg::core::pipeline::TaskScheduler::getTaskToProcess(const int workerRank)
{
    std::unique_ptr<bdg::core::pipeline::Task> task = workers.getWorker(workerRank).getTaskFromQueue();

    if (task != nullptr)
    {
        return task;
    }

    // We can process the task for the last plugin in the pipeline only under these conditions:
    //   - If it is a storer task, we must be able to start another storer worker, i.e. we cannot
    //     exceed maximum number of simultaneously running storer workers. If the last plugin is
    //     not storer, method canRunAnotherStorerWorker() returns true, therefore this part of
    //     condition is irrelevant then.
    //  -  If we have already promissed to generate demanded amount of data (files/bytes), we do not
    //     want to run another task which is the last in the pipeline (where the data amount is counted),
    //     because we do not want to exceed this demanded amount of data. Promissed amount can be decreased
    //     when some task (last in the pipeline) finishes, i.e. getProgressPromissed() may return less
    //     than 100 then.
    const int maxAllowedPluginIndex = speedLimiterScheduler.canRunAnotherStorerWorker() && progressCounter.getProgressPromissed() < 100
        ? pipeline.getPluginsCount() - 1
        : pipeline.getPluginsCount() - 2;

    // Get a suitable task from the task queue - we have to check whether its result fits in the result queue.
    if (pipeline.getInfo().getSchedulerInfo().useTaskQueue())
    {
        for (int i = maxAllowedPluginIndex; i >= 0; --i)
        {
            // Can we store the result of the task or is it the task for the last plugin in the pipeline?
            if (!resultQueue.full(i) || i == pipeline.getPluginsCount() - 1)
            {
                task = taskQueue.pop(i, workerRank);

                if (task != nullptr)
                {
                    break;
                }
            }
        }
    }

    if (task != nullptr)
    {
        return task;
    }

    // Create a new task - we have to check whether its result fits in the result queue.
    for (int i = maxAllowedPluginIndex; i >= 0; --i)
    {
        // Can we store the result of the task or is it the task for the last plugin in the pipeline?
        if (!resultQueue.full(i) || i == pipeline.getPluginsCount() - 1)
        {
            task = taskFactory.createTask(canCreateGeneratorTask(), i, workerRank);

            if (task->getType() != Task::TaskType::TASK_TYPE_IDLE)
            {
                break;
            }
        }
    }

    return task;
}

/**
 * Generates one task to the task queue.
 */
void bdg::core::pipeline::TaskScheduler::generateTaskToQueue()
{
    // We create tasks only when the job can continue to run.
    if (!isJobTerminating() && pipeline.getInfo().getSchedulerInfo().useTaskQueue())
    {
        for (int i = pipeline.getPluginsCount() - 1; i >= 0; --i)
        {
            if (!taskQueue.full(i))
            {
                std::unique_ptr<bdg::core::pipeline::Task> task = taskFactory.createTask(canCreateGeneratorTask(), i);

                // Idle task is created only when there are no task results to process and all the
                // generator tasks have been created.
                if (task->getType() != Task::TaskType::TASK_TYPE_IDLE)
                {
                    taskQueue.push(task);
                    break;
                }
            }
        }
    }
}

/**
 * Sets the properties of the task that should be processed.
 *
 * @param task Task that should be processed.
 */
void bdg::core::pipeline::TaskScheduler::modifyProcessedTask(std::unique_ptr<bdg::core::pipeline::Task> &task)
{
    // Set how many files we should generate.
    if (task->getType() == Task::TaskType::TASK_TYPE_GENERATOR)
    {
        if (task->getFilesCount() == 0)
        {
            const size_t filesToGenerate = statisticsCounter.getOptimalFilesToGenerate(pipeline.getInfo().getSchedulerInfo().getFilesPerTask());
            task->setFilesCount(filesToGenerate);
        }

        // We do not set starting file id when we restart the task (it has finished with error
        // or we restart it after restoring pipeline from a checkpoint.
        if (task->getRestarts() == 0)
        {
            // Generator always uses global file ids.
            task->setFileFrom(nextFileIdGlobal);
            nextFileIdGlobal += task->getFilesCount();
        }

        task->setPromissedFiles(task->getFilesCount());
    }

    // Set estimated number of generated files/bytes. Only transformer task can change its promissed
    // number of files (i.e. it can generate less/more files than it promissed to).
    const size_t usualGeneratedFiles = statisticsCounter.getUsualGeneratedFiles(task->getPluginIndex());
    const size_t usualGeneratedBytes = statisticsCounter.getUsualGeneratedBytes(task->getPluginIndex());

    if (usualGeneratedFiles > 0 && task->getType() == Task::TaskType::TASK_TYPE_TRANSFORMER && !pipeline.getPlugin(task->getPluginIndex())->preservesFilesCounts())
    {
        task->setPromissedFiles(usualGeneratedFiles);
    }

    if (usualGeneratedBytes > 0)
    {
        task->setPromissedBytes(usualGeneratedBytes);
    }

    // Is it a task for the last plugin in the pipeline.
    if (task->getPluginIndex() == pipeline.getPluginsCount() - 1)
    {
        if (task->getRestarts() == 0)
        {
            totalPromissedFiles += task->getFilesCount();
        }

        // Is it a storer task? If so, set its store speed w.r.t. the scheduler's "frequency" setting. 
        if (task->getType() == Task::TaskType::TASK_TYPE_STORER)
        {
            speedLimiterScheduler.setStorerTaskFrequency((StorerTask*)task.get(), pipeline.getInfo().getSchedulerInfo().getFileFrequency());
        }

        // We have to check, that we do not generate more data than demanded.
        switch (progressCounter.getTargetAmount().getUnit())
        {
            case ValueWithUnit::Unit::UNIT_BYTE:
                task->setBytesLimit(progressCounter.getRemainingBytes());
                break;
                
            case ValueWithUnit::Unit::UNIT_FILE:
                task->setFilesLimit(progressCounter.getRemainingFiles());
                break;
                
            case ValueWithUnit::Unit::UNIT_SECOND:
                task->setSecondsLimit(progressCounter.getRemainingSeconds());
                break;
        }

        // The last plugin in the pipeline just promissed to process some files/bytes.
        progressCounter.addPromissedFiles(task->getPromissedFiles());
        progressCounter.addPromissedBytes(task->getPromissedBytes());
    }
}

/**
 * Initializes workers. When the job is recovering from an error, an init message with
 * state of each worker is sent. Otherwise a message with a task to be processed is sent.
 */
void bdg::core::pipeline::TaskScheduler::sendInitialCommandsToWorkers()
{
    // Error recovery from the last checkpoint if present.
    if (checkpointer.tryRestore(this))
    {
        for (int i = 0; i < workers.getSize(); ++i)
        {
            initializeWorker(i + 1);
        }

        std::cout << "Restored from the last checkpoint..." << std::endl;
    }
    // There was no checkpoint (or it was invalid) -> we do not have to initialize workers.
    else
    {
        for (int i = 0; i < workers.getSize(); ++i)
        {
            sendCommandToWorker(i + 1);
        }
    }
}

/**
 * Sends a command to the worker. Command can be either to process a task, to get into the
 * idle state when there are no tasks to be processed, or to terminate when the job is finished.
 *
 * @param workerRank Rank of the worker.
 */
void bdg::core::pipeline::TaskScheduler::sendCommandToWorker(const int workerRank)
{
    // Is the job completed? If so, mmediately terminate the worker (and eventually all the other workers too).
    if (isJobTerminating())
    {
        terminateWorker(workerRank);
    }
    // Continue with processing.
    else
    {
        std::unique_ptr<bdg::core::pipeline::Task> task = getTaskToProcess(workerRank);

        // Send the new task to the worker if there is something to process.
        if (task != nullptr && task->getType() != Task::TaskType::TASK_TYPE_IDLE)
        {
            modifyProcessedTask(task);
            pipeline.getPlugin(task->getPluginIndex())->taskBeforePassToWorker(task.get());
            sendTaskToWorker(workerRank, task);
        }
        // Set worker to the idle state - there are no tasks to process in the moment.
        else
        {
            setWorkerIdle(workerRank);
        }
    }
}

/**
 * Initializes the worker with its state when error recovery is performed.
 *
 * @param workerRank Rank of the initialized worker.
 */
void bdg::core::pipeline::TaskScheduler::initializeWorker(const int workerRank)
{
    mpi.sendMessageToWorker(workers.getWorker(workerRank).serialize(), workerRank, MessageType::MESSAGE_TYPE_INITIALIZE);
    workers.setWorkerState(workerRank, Worker::State::STATE_PROCESSING);
}

/**
 * Sends a task to the worker.
 *
 * @param workerRank Rank of the worker which should process the task.
 * @param task Task to be processed on the worker.
 */
void bdg::core::pipeline::TaskScheduler::sendTaskToWorker(const int workerRank, std::unique_ptr<bdg::core::pipeline::Task> &task)
{
    // Is it a storer task? If so, increase the number of used storers.
    if (task->getType() == Task::TaskType::TASK_TYPE_STORER)
    {
        speedLimiterScheduler.storerTaskStarted();
    }

    mpi.sendMessageToWorker(task->serialize(), workerRank, createTaskNewMessage(task->getPluginIndex()));
    workers.getWorker(workerRank).addEvent(WorkerEvent::EVENT_TASK_STARTED, task->getPluginIndex());
    workers.setWorkerProcessing(workerRank, task);
}

/**
 * Sends the idle message to the worker. This message is sent when there are no tasks
 * to be processed by the worker. In idle state worker sleeps for a while and then it
 * sends back message to the master demanding some work.
 *
 * @param workerRank Rank of the worker.
 */
void bdg::core::pipeline::TaskScheduler::setWorkerIdle(const int workerRank)
{
    mpi.sendMessageToWorker(MPI_EMPTY_STRING, workerRank, MessageType::MESSAGE_TYPE_IDLE);
    workers.setWorkerState(workerRank, Worker::State::STATE_IDLE);
}

/**
 * Terminates the worker with a given rank.
 *
 * @param workerRank Rank of the worker.
 */
void bdg::core::pipeline::TaskScheduler::terminateWorker(const int workerRank)
{
    // Start to terminate the worker and clean it up.
    if (workers.getWorker(workerRank).getState() != Worker::State::STATE_FINISHING && workers.getWorker(workerRank).getState() != Worker::State::STATE_FINISHED)
    {
        mpi.sendMessageToWorker(MPI_EMPTY_STRING, workerRank, MessageType::MESSAGE_TYPE_JOB_COMPLETED);
        workers.setWorkerState(workerRank, Worker::State::STATE_FINISHING);
    }

    // When we terminate the first worker, we create a new checkpoint. Reason: It might happen that
    // some node fails when some intermediate files have already been deleted in the clean up process.
    // When the job is relaunched than, some tasks that need those files might be restarted and this
    // would lead to a failure. With checkpoint here, this situation cannot happen.
    if (workers.getFinishingWorkersCount() == 1 && workers.getFinishedWorkersCount() == 0 && statisticsCounter.getProcessedTasksCount() > 0)
    {
        checkpointer.store(this);
    }
}

/**
 * Handles an error that occured during the initialization of the job, probably it
 * is an error in job configuration.
 *
 * @param debugError Error stored in the log.
 * @param error Additional error displayed on the stderr.
 */
void bdg::core::pipeline::TaskScheduler::handleInitError(const std::string &debugError, const std::string &error)
{
    bdg_error(getLogger(), "ERROR: " + debugError + "!");
    std::cerr << "ERROR: " << debugError << "!" << std::endl;

    if (!error.empty())
    {
        std::cerr << error << std::endl;
    }
    
    // Send JOB_COMPLETED message to all workers.
    for (int i = 0; i < workers.getSize(); ++i)
    {
        terminateWorker(i + 1);
    }

    // Receive JOB_COMPLETED message from all workers.
    for (int i = 0; i < workers.getSize(); ++i)
    {
        int tag;
        mpi.receiveMessageFromWorker(i + 1, tag);
    }
}

/**
 * Handles an error task result. It tries to re-launch the task on different worker. If
 * re-launch is not allowed any more (maximum number of re-launches was reached), the
 * task is considered as permanently failed.
 *
 * @param workerRank Rank of the worker that processed the task.
 * @param taskResult Result of the task processing.
 */
void bdg::core::pipeline::TaskScheduler::handleErrorResult(const int workerRank, std::unique_ptr<bdg::core::pipeline::TaskResult> &taskResult)
{
    const SchedulerInfo &schedulerInfo = pipeline.getInfo().getSchedulerInfo();
    const ErrorTaskResult *errorTaskResult = (ErrorTaskResult*)taskResult.get();
    const int resultPluginIndex = errorTaskResult->getPluginIndex();
    std::unique_ptr<Task> failedTask = workers.getWorker(workerRank).getProcessedTaskMove();

    // Has a storer task been finished?
    if (failedTask->getType() == Task::TaskType::TASK_TYPE_STORER)
    {
        speedLimiterScheduler.storerTaskFinished();
    }

    // Task for the last plugin has been finished.
    if (resultPluginIndex == pipeline.getPluginsCount() - 1)
    {
        // Subtract all the promissed files/bytes.
        progressCounter.addPromissedBytes(-(Poco::Int64)failedTask->getPromissedBytes());
        progressCounter.addPromissedFiles(-(Poco::Int64)failedTask->getPromissedFiles());
    }

    bdg_warning(getLogger(), "Task finished unsuccessfully: " + errorTaskResult->getMessage() + "; Rank: " + std::to_string(workerRank) + "; Plugin: " + std::to_string(errorTaskResult->getPluginIndex()) + "; File From: " + std::to_string(errorTaskResult->getFileFrom()));

    // Can we restart the task on another worker?
    if (failedTask->getRestarts() < ALLOWED_TASK_RESTARTS)
    {
        const int restartWorkerRank = schedulerInfo.getRandomRankForPlugin(resultPluginIndex, workerRank);

        failedTask->restarted();
        workers.getWorker(restartWorkerRank).addTaskToQueue(failedTask);        // Ownership of failedTask moved to the queue.

        bdg_debug(getLogger(), "Task restarted on worker " + std::to_string(restartWorkerRank));
    }
    // Task permanently terminated with error.
    else
    {
        bdg_debug(getLogger(), "Task cannot be restarted anymore");

        statisticsCounter.taskError(errorTaskResult);
        workers.getWorker(workerRank).taskError(errorTaskResult);

        // When we want to preserve the order of generated files, we have to add the result to the
        // queue, because otherwise the queue would wait for the result from a given task and the
        // whole program would get stuck.
        if (schedulerInfo.preserveFilesOrder())
        {
            // If we use global file ids, we have to add the error task result to all the queues,
            // because this task will not be processed any more. This is the reason why the queues
            // can move on and they do not get stuck.
            if (pipeline.useGlobalFileIds())
            {
                const int maxPluginIndex = pipeline.getInfo().getSchedulerInfo().displayGeneratedFiles() ? pipeline.getPluginsCount() : pipeline.getPluginsCount() - 1;

                for (int i = resultPluginIndex; i < maxPluginIndex; ++i)
                {
                    std::unique_ptr<TaskResult> taskResultCopy(new bdg::core::pipeline::ErrorTaskResult);
                    taskResultCopy->unserialize(errorTaskResult->serialize());
                    taskResultCopy->setPluginIndex(i);

                    resultQueue.push(taskResultCopy);
                }
            }
            // We use local file ids - add the error result only to the queue for a given plugin.
            else
            {
                if (resultPluginIndex < pipeline.getPluginsCount() - 1 || pipeline.getInfo().getSchedulerInfo().displayGeneratedFiles())
                {
                    resultQueue.push(taskResult);       // Ownership of taskResult moved to the queue.
                }
            }
        }
    }
    
    // Have we reached the error level threashold?
    if (statisticsCounter.tooManyErrors(resultPluginIndex))
    {
        handleFatalError("Maximum error level reached - there have been too many errors to continue in the job. Last error: " + errorTaskResult->getMessage());
    }
}

/**
 * Handles fatal error that occured on the master. It is mainly caused by a configuration
 * error or fatal error in a task processed on some worker.
 *
 * @param error Error message.
 */
void bdg::core::pipeline::TaskScheduler::handleFatalError(const std::string &error)
{
    // We want to write just one error (if all workers fail).
    if (!forceTerminate)
    {
        std::cerr << "FATAL ERROR: " << error << "!" << std::endl;
    }

    forceTerminate = true;
    bdg_error(getLogger(), "FATAL ERROR: " + error + "!");
}

/**
 * Writes statistics about job run to stdout and into the log.
 */
void bdg::core::pipeline::TaskScheduler::writeStatistics()
{
    progressThread.join();
    monitoringThread.join();

    // Store checkpoints.
    speedLimiterScheduler.updateTimes();
    checkpointer.store(this);
    monitoringCheckpointer.store(this);
    
    // Get statistics from the monitoring checkpointer.
    Poco::Logger &logger = getLogger();
    Poco::JSON::Object resultJson;

    monitoringCheckpointer.store(this, resultJson);

    // Write statistics about generation speed.
    std::cout << std::endl << "------------------------------" << std::endl << std::endl;

    if (progressCounter.getGeneratedFiles() == 0)
    {
        std::cout << "WARNING: No files were generated!" << std::endl << std::endl;
    }
    else
    {
        std::cout << "Generated Files: " << resultJson.getValue<std::string>("generatedFiles") << std::endl;
        std::cout << "Generated Size: " << resultJson.getValue<std::string>("generatedSize") << std::endl;
    }
    
    std::cout << "Average Speed Files: " << resultJson.getValue<std::string>("avgSpeedFiles") << std::endl;
    std::cout << "Average Speed Size: " << resultJson.getValue<std::string>("avgSpeedSize") << std::endl;
    std::cout << "Average Speed Files Per Worker: " << resultJson.getValue<std::string>("avgSpeedFilesPerWorker") << std::endl;
    std::cout << "Average Speed Size Per Worker: " << resultJson.getValue<std::string>("avgSpeedSizePerWorker") << std::endl;
    std::cout << "Average Files Per Task: " << resultJson.getValue<std::string>("avgFilesPerTask") << " files" << std::endl;
    std::cout << "Average Size Per Task: " << resultJson.getValue<std::string>("avgSizePerTask") << std::endl;

    bdg_information(logger, "Average Speed Files: " + resultJson.getValue<std::string>("avgSpeedFiles"));
    bdg_information(logger, "Average Speed Size: " + resultJson.getValue<std::string>("avgSpeedSize"));
    bdg_information(logger, "Average Speed Files Per Worker: " + resultJson.getValue<std::string>("avgSpeedFilesPerWorker"));
    bdg_information(logger, "Average Speed Size Per Worker: " + resultJson.getValue<std::string>("avgSpeedSizePerWorker"));
    bdg_information(logger, "Average Files Per Task: " + resultJson.getValue<std::string>("avgFilesPerTask") + " files");
    bdg_information(logger, "Average Size Per Task: " + resultJson.getValue<std::string>("avgSizePerTask"));

    // Write statistics about optimal speed (only when some storer is used in the pipeline).
    if (!pipeline.getInfo().getSchedulerInfo().getFileFrequency().isUndefined() && pipeline.getPlugin(pipeline.getPluginsCount() - 1)->getType() == Plugin::PluginType::PLUGIN_TYPE_STORER)
    {
        std::cout << "Demanded Speed: " << resultJson.getValue<std::string>("demandedSpeed") << std::endl;
        std::cout << "Best Speed: " << resultJson.getValue<std::string>("bestSpeed") << std::endl;
        std::cout << "Optimal Storers Count: " << resultJson.getValue<std::string>("optimalStorersCount") << std::endl;

        bdg_information(logger, "Demanded Speed: " + resultJson.getValue<std::string>("demandedSpeed"));
        bdg_information(logger, "Best Storing Speed: " + resultJson.getValue<std::string>("bestSpeed"));
        bdg_information(logger, "Optimal Storers Count : " + resultJson.getValue<std::string>("optimalStorersCount"));
    }

    std::cout << "Total Time: " << resultJson.getValue<std::string>("totalTime") << " s" << std::endl;
    std::cout << "Total Tasks: " << resultJson.getValue<std::string>("totalTasks") << std::endl;
    std::cout << "Error Tasks: " << resultJson.getValue<std::string>("errorTasks") << std::endl;
    std::cout << "Average Task Length: " << resultJson.getValue<std::string>("avgTaskLength") << " s" << std::endl;
    std::cout << "Workers Count: " << resultJson.getValue<std::string>("workersCount") << std::endl;
    std::cout << std::endl;

    bdg_information(logger, "Total Time: " + resultJson.getValue<std::string>("totalTime") + " s");
    bdg_information(logger, "Total Tasks: " + resultJson.getValue<std::string>("totalTasks"));
    bdg_information(logger, "Error Tasks: " + resultJson.getValue<std::string>("errorTasks"));
    bdg_information(logger, "Average Task Length: " + resultJson.getValue<std::string>("avgTaskLength") + " s");
    bdg_information(logger, "Workers Count: " + resultJson.getValue<std::string>("workersCount"));

    // Write content of all the generated files.
    if (!forceTerminate && pipeline.getInfo().getSchedulerInfo().displayGeneratedFiles())
    {
        FileTransferer fileTransferer(pipeline.getJobId(), pipeline.getInfo().getSchedulerInfo().getFilesPerDirectory());
        std::unique_ptr<bdg::core::pipeline::SuccessTaskResult> taskResult;

        do
        {
            taskResult = resultQueue.popSuccessResult(pipeline.getPluginsCount());

            if (taskResult != nullptr)
            {
                for (int i = 0; i < taskResult->getFilesCount(); ++i)
                {
                    const std::string filePath = fileTransferer.expandFileMaskFileId(taskResult->getFileMask(), taskResult->getFileFrom() + i);

                    if (Poco::File(filePath).exists())
                    {
                        std::cout << "File: " << filePath << std::endl;
                    }
                    else
                    {
                        std::cout << "Generated file [" << filePath << "] does not exist locally or deleted" << std::endl;
                    }
                }
            }
        }
        while (taskResult != nullptr);
    }
    
    // Store the task schedule to a file.
    if (logger.trace())
    {
        workers.storeTaskSchedule(pipeline.getJobId(), progressCounter.getElapsedMicroseconds());
    }

    bdg_information(logger, "Master done!");
}

/**
 * Cleans up the worker - this method is called when the job is finished. It calls
 * cleanUp method of each plugin except the last one. This method deletes
 * all temporary files. It also calls terminatePlugin method of each plugin in the
 * pipeline.
 */
void bdg::core::pipeline::TaskScheduler::cleanUp()
{
    for (int i = 0; i < pipeline.getPluginsCount(); ++i)
    {
        pipeline.getPlugin(i)->terminatePlugin();
    }

    // Clean up all the plugins except the last one (we want to preserve its files).
    if (pipeline.getInfo().getSchedulerInfo().cleanUp())
    {
        for (int i = 0; i < pipeline.getPluginsCount() - 1; ++i)
        {
            pipeline.getPlugin(i)->cleanUp();
        }
    }

    // If everything went right, remove the last checkpoint.
    if (!forceTerminate)
    {
        checkpointer.cleanUp();
    }
}

/**
 * Checks whether the job configuration does not contain errors.
 *
 * @return Returns true when the job configuration does not contain errors, false otherwise.
 */
bool bdg::core::pipeline::TaskScheduler::checkConfiguration()
{
    // Check workers.
    if (mpi.getSize() < 2)
    {
        handleInitError("There are no workers");
        return false;
    }

    // Check the plugins in the pipeline.
    if (!checkPipelineConfiguration())
    {
        return false;
    }

    // Check the configuration of output paths.
    if (!checkOutputConfiguration())
    {
        return false;
    }

    // Check the scheduler configuration.
    if (!checkSchedulerConfiguration())
    {
        return false;
    }

    // Is job stopped? Try if it does not get un-stopped within 5 seconds.
    int counter = 0;

    while (isJobStopped() && counter < 50)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        ++counter;
    }

    if (isJobStopped())
    {
        handleInitError("Job is stopped");
        return false;
    }

    return true;
}

/**
 * Checks the configuration of pipeline if there are some plugins in the pipeline, if
 * the first plugin in the pipeline is generator, last plugin in the pipeline is
 * transformer or storer and if plugins in the middle of the pipeline are transformers.
 *
 * @return Returns true when pipeline is all right.
 */
bool bdg::core::pipeline::TaskScheduler::checkPipelineConfiguration()
{
    if (pipeline.getPluginsCount() == 0)
    {
        handleInitError("There are no plugins in the pipeline");
        return false;
    }

    const int firstPluginIndex = 0;
    const int lastPluginIndex = pipeline.getPluginsCount() - 1;

    Plugin::PluginType firstPluginType = pipeline.getPlugin(firstPluginIndex)->getType();
    Plugin::PluginType lastPluginType = pipeline.getPlugin(lastPluginIndex)->getType();

    if (firstPluginType != Plugin::PluginType::PLUGIN_TYPE_GENERATOR)
    {
        handleInitError("First plugin in the pipeline must be generator, " + Poco::toLower(pipeline.getPlugin(firstPluginIndex)->getTypeString()) + " given");
        return false;
    }

    if (pipeline.getPluginsCount() > 1 && lastPluginType != Plugin::PluginType::PLUGIN_TYPE_TRANSFORMER && lastPluginType != Plugin::PluginType::PLUGIN_TYPE_STORER)
    {
        handleInitError("Last plugin in the pipeline must be transformer or storer, " + Poco::toLower(pipeline.getPlugin(lastPluginIndex)->getTypeString()) + " given");
        return false;
    }

    for (int i = 1; i < pipeline.getPluginsCount() - 1; ++i)
    {
        Plugin::PluginType pluginType = pipeline.getPlugin(i)->getType();

        if (pluginType != Plugin::PluginType::PLUGIN_TYPE_TRANSFORMER)
        {
            handleInitError("Plugin placed on index " + std::to_string(i) + " in the pipeline [" + pipeline.getInfo().getPipelinePluginName(i) + "] must be transformer, " + Poco::toLower(pipeline.getPlugin(i)->getTypeString()) + " given");
            return false;
        }
    }

    return true;
}

/**
 * Checks whether there are not two output paths that are the same. We do not want generated
 * data to colide or get rewritten by another plugins in the pipeline.
 *
 * @return Are there two equal output paths?
 */
bool bdg::core::pipeline::TaskScheduler::checkOutputConfiguration()
{
    for (int i = 0; i < pipeline.getPluginsCount(); ++i)
    {
        for (int j = 0; j < i; ++j)
        {
            const std::vector<std::string> &firstOutputPaths = pipeline.getPlugin(i)->getOutputPaths();
            const std::vector<std::string> &secondOutputPaths = pipeline.getPlugin(j)->getOutputPaths();
        
            for (const std::string &firstOutputPath : firstOutputPaths)
            {
                for (const std::string &secondOutputPath : secondOutputPaths)
                {
                    if (!firstOutputPath.empty() && firstOutputPath == secondOutputPath)
                    {
                        handleInitError("Two plugins in the pipeline [" + pipeline.getPlugin(j)->getName() + " and " + pipeline.getPlugin(i)->getName() + "], have the same output path [" + firstOutputPath + "]");
                        return false;
                    }
                }
            }
        }
    }

    return true;
}

/**
 * Checks whether each plugin in the pipeline can run at least at one worker.
 *
 * @return Is the scheduling of pipeline all right?
 */
bool bdg::core::pipeline::TaskScheduler::checkSchedulerConfiguration()
{
    const bdg::core::pipeline::SchedulerInfo &schedulerInfo = pipeline.getInfo().getSchedulerInfo();

    for (int plugin = 0; plugin < pipeline.getPluginsCount(); ++plugin)
    {
        bool canRunPlugin = false;

        // Loop through all the available worker ranks.
        for (int rank = 1; rank < mpi.getSize(); ++rank)
        {
            // Can we run the plugin on a given rank?
            if (schedulerInfo.canRunPluginAtRank(plugin, rank))
            {
                canRunPlugin = true;
                break;
            }
        }

        // Check if we can run the plugin at some rank.
        if (!canRunPlugin)
        {
            handleInitError("Plugin placed on index " + std::to_string(plugin) + " in the pipeline [" + pipeline.getInfo().getPipelinePluginName(plugin) + "] cannot be scheduled at any worker, change the scheduler configuration", pipeline.getInfo().getSchedulerInfo().debug(false));
            return false;
        }
    }

    return true;
}

/**
 * Returns true when the job is stopped. Job can be stopped by creating a stop file.
 *
 * @return Is the job stopped?
 */
bool bdg::core::pipeline::TaskScheduler::isJobStopped() const
{
    return Poco::File(bdg::core::pipeline::MonitoringCheckpointerRestorer::getPathToStopFile(pipeline.getJobId())).exists();
}

/**
 * Handles a successful task result, i.e. task was successfully processed on the worker.
 * This method stores statistics about processed task.
 *
 * @param workerRank Rank of the worker that processed the task.
 * @param taskResult Result of the task processing.
 */
void bdg::core::pipeline::TaskScheduler::processSuccessTaskResult(const int workerRank, std::unique_ptr<bdg::core::pipeline::TaskResult> &taskResult)
{
    const SuccessTaskResult *successTaskResult = (SuccessTaskResult*)taskResult.get();
    const std::unique_ptr<Task> &finishedTask = workers.getWorker(workerRank).getProcessedTask();
    const Poco::Int64 promissedBytes = finishedTask->getPromissedBytes();
    const size_t promissedFiles = finishedTask->getPromissedFiles();
    const size_t resultFilesCount = successTaskResult->getFilesCount();
    const int resultPluginIndex = successTaskResult->getPluginIndex();

    // Task for the last plugin has been finished.
    if (resultPluginIndex == pipeline.getPluginsCount() - 1)
    {
        // Store the info about total generated files/bytes. We store it only if the last plugin
        // is not a storer (it is generator or transformer). If it is a storer, we use special message
        // MessageType::MESSAGE_TYPE_BYTES_STORED to indicate the progress which is more precise.
        if (finishedTask->getType() != Task::TaskType::TASK_TYPE_STORER)
        {
            progressCounter.addGeneratedBytes(successTaskResult->getBytes());
            progressCounter.addGeneratedFiles(resultFilesCount);
        }

        // We have to add the difference between promissed bytes and really generated bytes, e.g.
        // task promissed to process 1000 bytes, but as a result it has 1200 bytes -> we add
        // 200 bytes as promissed, because the end of job is determined by promissed amount of data.
        progressCounter.addPromissedBytes((Poco::Int64)successTaskResult->getBytes() - promissedBytes);

        // It is also possible, that promissed and generated number of files are not the same, e.g.
        // transformer may generate less files, because it is able to combine more files into one.
        progressCounter.addPromissedFiles((Poco::Int64)resultFilesCount - promissedFiles);
    }
    
    // Has a storer task been finished?
    if (finishedTask->getType() == Task::TaskType::TASK_TYPE_STORER)
    {
        speedLimiterScheduler.storerTaskFinished();
    }

    statisticsCounter.taskProcessed(successTaskResult);
    workers.getWorker(workerRank).taskSuccessfull();
    workers.getWorker(workerRank).addEvent(WorkerEvent::EVENT_TASK_FINISHED, successTaskResult->getTotalTime());
    workers.setWorkerPluginState(workerRank, resultPluginIndex, successTaskResult->getPluginState());

    // Check the promissed files and generated files.
    if (promissedFiles != resultFilesCount && pipeline.getPlugin(resultPluginIndex)->preservesFilesCounts() && progressCounter.getProgressPromissed() < 100)
    {
        handleFatalError("Error in implementation of plugin [" + pipeline.getPlugin(resultPluginIndex)->getName() + "]. Plugin promissed not to change files count, but it did so -> generator could not use global file ids");
    }

    // It is possible, that no files are generated/processed, we do not want to process those results.
    if (resultFilesCount > 0)
    {
        // If this is not the result of a last plugin in the pipeline, we store it, because it will
        // be used as a base to generate new tasks for a next plugin in the pipeline.
        if (resultPluginIndex < pipeline.getPluginsCount() - 1)
        {
            resultQueue.push(taskResult);
        }
        // Store the result, because from that result we can reconstruct the paths of all the generated files.
        // When we want to preserve the order of generated files, result queue works as priority queue, so
        // the order is preserved automatically by the queue.
        else if (pipeline.getInfo().getSchedulerInfo().displayGeneratedFiles())
        {
            resultQueue.push(taskResult);
        }
    }
}

/**
 * Returns whether we are allowed to create a new generator task. We want to limit
 * the number of created generator tasks, so we do not have too big overhead. We do
 * not want to generate more than twice more files in the first stage of the pipeline
 * than in the last stage of the pipeline.
 *
 * @return Are we allowed to create a new generator task?
 */
bool bdg::core::pipeline::TaskScheduler::canCreateGeneratorTask() const
{
    return statisticsCounter.getProcessedPluginTasks(0) < 3
        || nextFileIdGlobal - 1 <= 2 * totalPromissedFiles
        || !pipeline.useGlobalFileIds()
        || (resultQueue.empty() && taskQueue.empty());
}

/**
 * Returns true when the job is completed or when there has been some fatal error. 
 *
 * @return Is the job completed?
 */
bool bdg::core::pipeline::TaskScheduler::isJobTerminating() const
{
    return progressCounter.getProgressGenerated() == 100 || forceTerminate;
}

/**
 * Returns whether the job is finished and we can terminate the master thread.
 *
 * @return Is the job finished?
 */
bool bdg::core::pipeline::TaskScheduler::isJobFinished() const
{
    return workers.getFinishedWorkersCount() == workers.getSize();
}
