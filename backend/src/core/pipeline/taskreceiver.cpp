/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/taskreceiver.h"                     // Header
#include "core/pipeline/plugins/fatalerrortaskresult.h"     // FatalErrorTaskResult
#include "core/pipeline/plugins/successtaskresult.h"        // SuccessTaskResult
#include "core/pipeline/worker.h"                           // Worker
#include "utils/logger.h"                                   // Logger

#include <Poco/String.h>    // Poco::String

#include <thread>           // std::this_thread::sleep_for
#include <chrono>           // std::chrono::seconds

using bdg::utils::MPIProxy;
using bdg::utils::Logger;
using bdg::core::pipeline::MessageType;

const int bdg::core::pipeline::TaskReceiver::IDLE_SLEEP_MILLISECONDS = 500;

/**
 * Constructor of the task receiver.
 *
 * @param pipeline Instance of the pipeline with plugins.
 * @param mpi MPI wrapper.
 */
bdg::core::pipeline::TaskReceiver::TaskReceiver(const bdg::core::pipeline::Pipeline &pipeline, const MPIProxy &mpi) :
    pipeline(pipeline),
    mpi(mpi)
{
    // Nothing here.
}

/**
 * Main loop of the worker thread. Receives messages from the master, processes
 * received tasks and sends back responses with results of task processing.
 */
void bdg::core::pipeline::TaskReceiver::receive()
{
    Poco::Logger &logger = Logger::getInstance().getLogger(mpi.getRank());
    bool done = false;
    int tag;

    while (!done)
    {
        // Accept message from master.
        std::string messageData = mpi.receiveMessageFromMaster(tag);
        bdg_debug(logger, "Received message " + bdg::core::pipeline::translateMessageType(tag) + "; Received Data: " + Poco::replace(messageData, "\n", " ").substr(0, 250));

        // Accept new task and process it.
        if (isTaskNewMessage(tag))
        {
            // Create the task to be processed.
            int pluginIndex = getPluginIndexFromTaskNewMessage(tag);
            std::unique_ptr<bdg::core::pipeline::Task> task(createTask(messageData, pluginIndex));

            // If there was an error, continue.
            if (task == nullptr)
            {
                continue;
            }

            // Process task.
            std::unique_ptr<bdg::core::pipeline::TaskResult> taskResult(processTask(task.get(), pluginIndex));

            // If there was an error, continue.
            if (taskResult == nullptr)
            {
                continue;
            }
            
            // Send the task result back to the master.
            sendResponseToMaster(task.get(), taskResult.get(), pluginIndex);
        }
        // Initialization from checkpoint.
        else if (tag == MessageType::MESSAGE_TYPE_INITIALIZE)
        {
            bdg_debug(logger, "Initializing from checkpoint");
            initialize(messageData);
            mpi.sendMessageToMaster(MPI_EMPTY_STRING, MessageType::MESSAGE_TYPE_INITIALIZED);
        }
        // Go to the idle state.
        else if (tag == MessageType::MESSAGE_TYPE_IDLE)
        {
            bdg_debug(logger, "Idle state - sleeping for " + std::to_string(IDLE_SLEEP_MILLISECONDS) + " milliseconds");
            std::this_thread::sleep_for(std::chrono::milliseconds(IDLE_SLEEP_MILLISECONDS));
            mpi.sendMessageToMaster(MPI_EMPTY_STRING, MessageType::MESSAGE_TYPE_IDLE);
        }
        // Go to the idle state with no sleep.
        else if (tag == MessageType::MESSAGE_TYPE_IDLE_NO_SLEEP)
        {
            bdg_debug(logger, "Idle state - no sleeping");
            mpi.sendMessageToMaster(MPI_EMPTY_STRING, MessageType::MESSAGE_TYPE_IDLE);
        }
        // Job has been terminated.
        else if (tag == MessageType::MESSAGE_TYPE_JOB_COMPLETED)
        {
            bdg_debug(logger, "Cleaning up");
            cleanUp();
            done = true;
        }
        // Unknown message.
        else
        {
            handleFatalError("Receiver - unexpected message " + std::to_string(tag));
        }
    }

    bdg_information(logger, "Done!");
    mpi.sendMessageToMaster(MPI_EMPTY_STRING, MessageType::MESSAGE_TYPE_JOB_COMPLETED);
}

/**
 * Creates a task from received message.
 *
 * @param messageData Message with a task serialized in a JSON string.
 * @param pluginIndex Index of the plugin in the pipeline which will process the task.
 *
 * @return Deserialized task to be processed.
 */
bdg::core::pipeline::Task *bdg::core::pipeline::TaskReceiver::createTask(const std::string &messageData, const int pluginIndex) const
{
    try
    {
        bdg::core::pipeline::Task *task = pipeline.getPlugin(pluginIndex)->taskFactory();
        task->unserialize(messageData);

        return task;
    }
    catch (Poco::Exception &e)
    {
        FatalErrorTaskResult taskResult(e);
        handleFatalError(&taskResult);
    }
    catch (std::exception &e)
    {
        FatalErrorTaskResult taskResult(e);
        handleFatalError(&taskResult);
    }

    return nullptr;
}

/**
 * Processes the given task.
 *
 * @param task Task to be processed.
 * @param pluginIndex Index of the plugin in the pipeline which will process the task.
 *
 * @return Result of the task processing.
 */
bdg::core::pipeline::TaskResult *bdg::core::pipeline::TaskReceiver::processTask(bdg::core::pipeline::Task *task, const int pluginIndex) const
{
    try
    {
        FileTransferer fileTransferer(pipeline.getJobId(), pipeline.getInfo().getSchedulerInfo().getFilesPerDirectory());
        return pipeline.getPlugin(pluginIndex)->processTask(task);
    }
    catch (Poco::Exception &e)
    {
        FatalErrorTaskResult taskResult(e);
        handleFatalError(&taskResult);
    }
    catch (std::exception &e)
    {
        FatalErrorTaskResult taskResult(e);
        handleFatalError(&taskResult);
    }

    return nullptr;
}

/**
 * Initializes the worker. This method is called during the error recovery - it loads
 * the state of plugins in the pipeline.
 *
 * @param messageData
 */
void bdg::core::pipeline::TaskReceiver::initialize(const std::string &messageData) const
{
    Worker worker(pipeline.getPluginsCount());
    worker.unserialize(messageData, pipeline);

    for (int i = 0; i < pipeline.getPluginsCount(); ++i)
    {
        pipeline.getPlugin(i)->unserialize(worker.getPluginState(i));
    }
}

/**
 * Sends the result of task processing to the master. This method is called when the task
 * is finished (successfully or with an error).
 *
 * @param task Processed task.
 * @param taskResult Result of task processing.
 * @param pluginIndex Index of the plugin in the pipeline which processed the given task.
 */
void bdg::core::pipeline::TaskReceiver::sendResponseToMaster(bdg::core::pipeline::Task *task, bdg::core::pipeline::TaskResult *taskResult, const int pluginIndex) const
{
    // Setup the task result.
    taskResult->setPluginIndex(pluginIndex);
    taskResult->setRank(mpi.getRank());

    // Send back result of the task.
    switch (taskResult->getType())
    {
        case bdg::core::pipeline::TaskResult::ResultType::RESULT_TYPE_SUCCESSS:
        {
            Poco::JSON::Object pluginData;
            pipeline.getPlugin(pluginIndex)->serialize(pluginData);

            bdg::core::pipeline::SuccessTaskResult *successTaskResult = (SuccessTaskResult*)taskResult;
            successTaskResult->setPluginState(pluginData);

            mpi.sendMessageToMaster(taskResult->serialize(), MessageType::MESSAGE_TYPE_TASK_COMPLETED);
            break;
        }

        case bdg::core::pipeline::TaskResult::ResultType::RESULT_TYPE_ERROR:
            handleError(task, taskResult);
            break;

        case bdg::core::pipeline::TaskResult::ResultType::RESULT_TYPE_FATAL_ERROR:
            handleFatalError(taskResult);
            break;

        default:
            handleFatalError("Unknown task result type: " + std::to_string(taskResult->getType()));
            break;
    }
}

/**
 * Sends the result of task processing to the master. This method is called when the task
 * is finished with an error.
 *
 * @param task Processed task.
 * @param taskResult Result of task processing.
 */
void bdg::core::pipeline::TaskReceiver::handleError(bdg::core::pipeline::Task *task, const bdg::core::pipeline::TaskResult *taskResult) const
{
    bdg::core::pipeline::ErrorTaskResult *errorTaskResult = (bdg::core::pipeline::FatalErrorTaskResult*)taskResult;
    errorTaskResult->setFileFrom(task->getFileFrom());
    errorTaskResult->setFilesCount(task->getFilesCount());

    bdg_error(Logger::getInstance().getLogger(mpi.getRank()), "TASK ERROR: " + errorTaskResult->getMessage() + "!");
    mpi.sendMessageToMaster(errorTaskResult->serialize(), MessageType::MESSAGE_TYPE_TASK_ERROR);
}

/**
 * Handles the fatal error that occured during task processing. Sends the message informing
 * about this error to the master.
 *
 * @param taskResult Result of task processing.
 */
void bdg::core::pipeline::TaskReceiver::handleFatalError(const bdg::core::pipeline::TaskResult *taskResult) const
{
    bdg::core::pipeline::FatalErrorTaskResult *errorTaskResult = (bdg::core::pipeline::FatalErrorTaskResult*)taskResult;

    bdg_error(Logger::getInstance().getLogger(mpi.getRank()), "TASK FATAL ERROR: " + errorTaskResult->getMessage() + "!");
    mpi.sendMessageToMaster(errorTaskResult->getMessage(), MessageType::MESSAGE_TYPE_FATAL_ERROR);
}

/**
 * Handles the fatal error that occured on the worker. Sends the message informing
 * about this error to the master.
 *
 * @param error Error message.
 */
void bdg::core::pipeline::TaskReceiver::handleFatalError(const std::string &error) const
{
    bdg_error(Logger::getInstance().getLogger(mpi.getRank()), "FATAL ERROR: " + error + "!");
    mpi.sendMessageToMaster(error, MessageType::MESSAGE_TYPE_FATAL_ERROR);
}

/**
 * Cleans up the worker - this method is called when the job is finished. It calls
 * cleanUp method of each plugin except the last one. This method deletes
 * all temporary files. It also calls terminatePlugin method of each plugin in the
 * pipeline.
 */
void bdg::core::pipeline::TaskReceiver::cleanUp() const
{
    for (int i = 0; i < pipeline.getPluginsCount(); ++i)
    {
        pipeline.getPlugin(i)->terminatePlugin();
    }

    // Clean up all the plugins except the last one (we want to preserve its files).
    if (pipeline.getInfo().getSchedulerInfo().cleanUp())
    {
        for (int i = 0; i < pipeline.getPluginsCount() - 1; ++i)
        {
            pipeline.getPlugin(i)->cleanUp();
        }
    }
}
