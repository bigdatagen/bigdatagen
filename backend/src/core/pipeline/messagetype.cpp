/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/messagetype.h"     // Header

using bdg::core::pipeline::MessageType;

/**
 * Returns the string reprezentation of the message type.
 *
 * @param type Type of the message.
 *
 * @return String reprezentation of the message type.
 */
std::string bdg::core::pipeline::translateMessageType(const int type)
{
    if (isTaskNewMessage(type))
    {
        return "TASK_NEW";
    }

    switch (type)
    {
        case MessageType::MESSAGE_TYPE_INITIALIZE:
            return "INITIALIZE";
            break;

        case MessageType::MESSAGE_TYPE_INITIALIZED:
            return "INITIALIZED";
            break;

        case MessageType::MESSAGE_TYPE_TASK_NEW:
            return "TASK_NEW";
            break;

        case MessageType::MESSAGE_TYPE_TASK_COMPLETED:
            return "TASK_COMPLETED";
            break;

        case MessageType::MESSAGE_TYPE_TASK_ERROR:
            return "TASK_ERROR";
            break;

        case MessageType::MESSAGE_TYPE_FATAL_ERROR:
            return "FATAL_ERROR";
            break;

        case MessageType::MESSAGE_TYPE_IDLE:
            return "IDLE";
            break;

        case MessageType::MESSAGE_TYPE_IDLE_NO_SLEEP:
            return "IDLE_NO_SLEEP";
            break;

        case MessageType::MESSAGE_TYPE_BYTES_STORED:
            return "BYTES_STORED";
            break;
            
        case MessageType::MESSAGE_TYPE_JOB_COMPLETED:
            return "JOB_COMPLETED";
            break;

        default:
            return "UNKNOWN";
            break;
    }
}

/**
 * Returns the number reprezenting a message with the task for the plugin with passed index.
 *
 * @param pluginIndex Index of the plugin which should process the task.
 *
 * @return Number reprezenting a message with the task that should be processed.
 */
int bdg::core::pipeline::createTaskNewMessage(const int pluginIndex)
{
    return MessageType::MESSAGE_TYPE_TASK_NEW + bdg::core::pipeline::MESSAGE_TYPE_TASK_NEW_THRESHOLD + pluginIndex;
}

/**
 * Returns the index of the plugin that should process the task.
 *
 * @param message Tag of the received message.
 *
 * @return Index of the plugin that should process the task.
 */
int bdg::core::pipeline::getPluginIndexFromTaskNewMessage(const int message)
{
    return message - MessageType::MESSAGE_TYPE_TASK_NEW - bdg::core::pipeline::MESSAGE_TYPE_TASK_NEW_THRESHOLD;
}

/**
 * Returns whether the message with passed task is the message which indicates that a task
 * should be processed.
 *
 * @param message Tag of the received message.
 *
 * @return Is it a message for processing a task?
 */
bool bdg::core::pipeline::isTaskNewMessage(const int message)
{
    return message >= bdg::core::pipeline::MESSAGE_TYPE_TASK_NEW_THRESHOLD;
}
