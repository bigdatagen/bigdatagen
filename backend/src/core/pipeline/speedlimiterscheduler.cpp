/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/speedlimiterscheduler.h"            // Header

#include <Poco/Random.h>                // Poco::Random
#include <Poco/StringTokenizer.h>       // Poco::StringTokenizer

const size_t bdg::core::pipeline::SpeedLimiterScheduler::MIN_SECONDS_PER_STORER = 10;
const size_t bdg::core::pipeline::SpeedLimiterScheduler::MIN_SECONDS_PER_LAST_STORERS_COUNT_CHANGE = 2;
const double bdg::core::pipeline::SpeedLimiterScheduler::BEST_SPEED_COEFFICIENT = 0.8;

/**
 * Constructor.
 *
 * @param schedulerInfo Configuration of the scheduler.
 * @param pluginsCount Number of plugins placed in the pipeline.
 * @param workersCount Size of the cluster where BDG runs.
 */
bdg::core::pipeline::SpeedLimiterScheduler::SpeedLimiterScheduler(const SchedulerInfo &schedulerInfo, const size_t pluginsCount, const size_t workersCount) :
    demandedFrequency(schedulerInfo.getFileFrequency()),
    useProgressiveStorersCount(schedulerInfo.useProgressiveStorersCount())
{
    size_t workersAllowingStorer = 0;

    for (int rank = 1; rank <= workersCount; ++rank)
    {
        if (schedulerInfo.canRunPluginAtRank(pluginsCount - 1, rank))
        {
            ++workersAllowingStorer;
        }
    }
    
    if (useProgressiveStorersCount)
    {
        maxStorersCount = std::max<size_t>(1, std::min<size_t>(workersAllowingStorer, workersCount));
    }
    else
    {
        maxStorersCount = workersAllowingStorer;
    }
    
    storedFilesPerStorersCount = std::vector<Poco::UInt64>(maxStorersCount + 1, 0);
    storedBytesPerStorersCount = std::vector<Poco::UInt64>(maxStorersCount + 1, 0);
    secondsInUsePerStorersCount = std::vector<size_t>(maxStorersCount + 1, 0);

    // When the store speed is undefined, we want as many storer workers as possible. This is because
    // we cannot get any "I do not keep pace" message, so we cannot alter this number at all.
    if (!useProgressiveStorersCount || demandedFrequency.isUndefined())
    {
        optimalCurrentStorersCount = maxStorersCount;
    }
}

/**
 * Handles the message about progress received from some worker. This message contains
 * the number of newly generated files and their size.
 *
 * @param messageData Progress message sent from the worker.
 */
void bdg::core::pipeline::SpeedLimiterScheduler::handleBytesStoredMessage(const std::string &messageData)
{
    Poco::StringTokenizer parts(messageData, ";");

    storedFilesPerStorersCount[optimalCurrentStorersCount] += std::stoi(parts[0]);
    storedBytesPerStorersCount[optimalCurrentStorersCount] += std::stoi(parts[1]);
}

/**
 * Updates used optimal number of simultaneously running storrer tasks.
 */
void bdg::core::pipeline::SpeedLimiterScheduler::tryUpdateOptimalCurrentStorersCount()
{
    if (!demandedFrequency.isUndefined() && useProgressiveStorersCount && started)
    {
        // Get how long time do we count statistics for current number of storers.
        const size_t currentSeconds = convertMicrosecondsToSeconds(storersCountStartTime.elapsed());
        const size_t totalSeconds = secondsInUsePerStorersCount[optimalCurrentStorersCount] + currentSeconds;

        // Do we count statistics long enough?
        if (totalSeconds >= MIN_SECONDS_PER_STORER && currentSeconds >= MIN_SECONDS_PER_LAST_STORERS_COUNT_CHANGE)
        {
            const double currentSpeed = getCurrentSpeed();
            const double demandedSpeed = getDemandedSpeed();

            // We do not keep pace -> change the number of storer workers.
            if (!(demandedSpeed * BEST_SPEED_COEFFICIENT <= currentSpeed && currentSpeed <= demandedSpeed * (2 - BEST_SPEED_COEFFICIENT)))
            {
                updateOptimalCurrentStorersCount(currentSpeed, demandedSpeed);
            }
        }
    }
}

/**
 * Sets the speed with which a storer task should store data.
 *
 * @param task Storer task.
 * @param configuredFrequency Demanded overall speed.
 */
void bdg::core::pipeline::SpeedLimiterScheduler::setStorerTaskFrequency(StorerTask *task, const FileFrequency &configuredFrequency) const
{
    // Store speed is n times less than demanded speed where n is the number of optimally used storer workers.
    // E.g. if we have demanded speed 21 files per second and we have 3 storer workers, than each of them
    // stores data with speed of 21 files per 3 seconds (i.e. 7 files per second), 1/3 of originally demanded speed.
    FileFrequency fileFrequency(configuredFrequency.getBytes(), configuredFrequency.getFiles(), configuredFrequency.getSeconds() * optimalCurrentStorersCount);
    task->setFrequency(fileFrequency);
}

/**
 * Updates how long is the job running with particular number of simultaneous storer tasks.
 */
void bdg::core::pipeline::SpeedLimiterScheduler::updateTimes()
{
    secondsInUsePerStorersCount[optimalCurrentStorersCount] += convertMicrosecondsToSeconds(storersCountStartTime.elapsed());
    storersCountStartTime.update();
}

/**
 * Returns the storing speed for current number of simultaneous storer tasks.
 *
 * @return Current storing speed.
 */
double bdg::core::pipeline::SpeedLimiterScheduler::getCurrentSpeed() const
{
    if (demandedFrequency.getBytes() > 0)
    {
        return (double)storedBytesPerStorersCount[optimalCurrentStorersCount] / (secondsInUsePerStorersCount[optimalCurrentStorersCount] + convertMicrosecondsToSeconds(storersCountStartTime.elapsed()));
    }
    else
    {
        return (double)storedFilesPerStorersCount[optimalCurrentStorersCount] / (secondsInUsePerStorersCount[optimalCurrentStorersCount] + convertMicrosecondsToSeconds(storersCountStartTime.elapsed()));
    }
}

/**
 * Returns demanded storing speed.
 *
 * @return Demanded storing speed.
 */
double bdg::core::pipeline::SpeedLimiterScheduler::getDemandedSpeed() const
{
    if (demandedFrequency.getBytes() > 0)
    {
        return (double)demandedFrequency.getBytes() / demandedFrequency.getSeconds();
    }
    else
    {
        return (double)demandedFrequency.getFiles() / demandedFrequency.getSeconds();
    }
}

/**
 * Returns the speed that is closest to the demanded speed.
 *
 * @param bestStorersCount Output parameter for the best found number of simultaneous storer tasks.
 *
 * @return Best storing speed.
 */
double bdg::core::pipeline::SpeedLimiterScheduler::getBestSpeed(size_t &bestStorersCount) const
{
    // Count the best average speed - with best ratio to the demanded speed.
    const double demandedSpeed = getDemandedSpeed();
    double bestSpeed = 0;
    
    bestStorersCount = 1;

    for (int i = 1; i <= maxStorersCount; ++i)
    {
        if (secondsInUsePerStorersCount[i] > 0)
        {
            // Count the average speed for i storers.
            double speed;

            // We do not want to divide those two possibly huge numbers directly, because of
            // double's precission problems that might occur.
            if (demandedFrequency.getBytes() > 0)
            {
                speed = (double)((storedBytesPerStorersCount[i] *100) / secondsInUsePerStorersCount[i]) / 100;
            }
            else
            {
                speed = (double)((storedFilesPerStorersCount[i] * 100) / secondsInUsePerStorersCount[i]) / 100;
            }

            // Did we find a better speed? Is it closer to the demanded speed?
            if (std::abs(speed / demandedSpeed - 1) < std::abs(bestSpeed / demandedSpeed - 1))
            {
                bestSpeed = speed;
                bestStorersCount = i;
            }
        }
    }

    return bestSpeed;
}

/**
 * Serializes the state of this object into JSON.
 *
 * @param json JSON where we serialize the state.
 */
void bdg::core::pipeline::SpeedLimiterScheduler::serialize(Poco::JSON::Object &json) const
{
    json.set("optimalCurrentStorersCount", optimalCurrentStorersCount);
    
    Poco::JSON::Array storedFilesPerStorersCountJson;
    Poco::JSON::Array storedBytesPerStorersCountJson;
    Poco::JSON::Array secondsInUsePerStorersCountJson;

    for (int i = 0; i <= maxStorersCount; ++i)
    {
        storedFilesPerStorersCountJson.add(storedFilesPerStorersCount[i]);
        storedBytesPerStorersCountJson.add(storedBytesPerStorersCount[i]);

        size_t secondsInUse = secondsInUsePerStorersCount[i];

        if (started && i == optimalCurrentStorersCount)
        {
            secondsInUse += convertMicrosecondsToSeconds(storersCountStartTime.elapsed());
        }
        
        secondsInUsePerStorersCountJson.add(secondsInUse);
    }

    json.set("storedFilesPerStorersCount", storedFilesPerStorersCountJson);
    json.set("storedBytesPerStorersCount", storedBytesPerStorersCountJson);
    json.set("secondsInUsePerStorersCount", secondsInUsePerStorersCountJson);
}

/**
 * Deserializes the state of this object from the given JSON.
 *
 * @param json JSON containing the state of this object.
 */
void bdg::core::pipeline::SpeedLimiterScheduler::unserialize(const Poco::JSON::Object &json)
{
    optimalCurrentStorersCount = json.getValue<size_t>("optimalCurrentStorersCount");

    Poco::JSON::Array::Ptr storedFilesPerStorersCountJson = json.getArray("storedFilesPerStorersCount");
    Poco::JSON::Array::Ptr storedBytesPerStorersCountJson = json.getArray("storedBytesPerStorersCount");
    Poco::JSON::Array::Ptr secondsInUsePerStorersCountJson = json.getArray("secondsInUsePerStorersCount");

    for (int i = 0; i <= maxStorersCount && i < storedFilesPerStorersCountJson->size(); ++i)
    {
        storedFilesPerStorersCount[i] = storedFilesPerStorersCountJson->getElement<Poco::UInt64>(i);
    }

    for (int i = 0; i <= maxStorersCount && i < storedBytesPerStorersCountJson->size(); ++i)
    {
        storedBytesPerStorersCount[i] = storedBytesPerStorersCountJson->getElement<Poco::UInt64>(i);
    }

    for (int i = 0; i <= maxStorersCount && i < secondsInUsePerStorersCountJson->size(); ++i)
    {
        secondsInUsePerStorersCount[i] = secondsInUsePerStorersCountJson->getElement<size_t>(i);
    }
}

/**
 * Updates the current used simultaneously running storer tasks.
 *
 * @param currentSpeed Current storing speed.
 * @param demandedSpeed Demanded storing speed.
 */
void bdg::core::pipeline::SpeedLimiterScheduler::updateOptimalCurrentStorersCount(const double currentSpeed, const double demandedSpeed)
{
    // Save how much time we measured a current number of storers.
    updateTimes();

    // Count the best average speed - with best ratio to the demanded speed.
    size_t bestStorersCount;
    double bestSpeed = getBestSpeed(bestStorersCount);
    
    // Does it still worth it to increase/decrease the number of storers?
    if (bestSpeed * BEST_SPEED_COEFFICIENT <= currentSpeed && currentSpeed <= bestSpeed * (2 - BEST_SPEED_COEFFICIENT))
    {
        static Poco::Random random;
        Poco::UInt32 rndNumber = random.next(100);

        // With 95% probability increase the number of storers and with 5% probability decrease it when the speed is too low
        // and with 95% probability decrease the number of storers and with 5% probability increase it when the speed is too high.
        if ((rndNumber < 95 && currentSpeed < demandedSpeed) || (rndNumber < 5 && currentSpeed > demandedSpeed))
        {
            optimalCurrentStorersCount = std::min<size_t>(optimalCurrentStorersCount + 1, maxStorersCount);
        }
        else
        {
            optimalCurrentStorersCount = std::max<size_t>(1, optimalCurrentStorersCount - 1);
        }
    }
    // Set the number of storers to the statistical optimum.
    else
    {
        optimalCurrentStorersCount = bestStorersCount;
    }
}
