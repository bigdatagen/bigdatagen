/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/pipeline.h"             // Header
#include "core/pipeline/taskscheduler.h"        // Scheduler
#include "core/pipeline/taskreceiver.h"         // Receiver
#include "utils/config.h"                       // Config
#include "utils/logger.h"                       // Logger

#include <iostream>         // std::cout

using bdg::utils::Config;
using bdg::utils::Logger;

const int bdg::core::pipeline::Pipeline::MAX_POSTPROCESSOR_PLUGINS = 8;

/**
 * Constructor of the pipeline.
 *
 * @param mpi MPI wrapper.
 * @param jobId Id of currently running job.
 */
bdg::core::pipeline::Pipeline::Pipeline(bdg::utils::MPIProxy &mpi, const int jobId) :
    jobId(jobId),
    mpi(mpi),
    pipelinePluginManager(Logger::getInstance().getLogger(mpi.getRank()), Config::getInstance().getPipelinePluginsPath("pipelineplugins")),
    pipelineCheckpointer(jobId),
    pipelineInfo(pipelineCheckpointer.restore(mpi.getMasterRank(), mpi.getSize()))
{
    SchedulerInfo &schedulerInfo = pipelineInfo.getSchedulerInfo();
    std::vector<std::string> pluginNames;
    std::vector<int> oneRankPlugins;

    for (const auto &pluginName : pipelineInfo.getPipelinePluginNames())
    {
        registerPlugin(pluginName, pluginNames, oneRankPlugins);
    }

    if (plugins.empty())
    {
        throw Poco::InvalidArgumentException("There are no plugins in the pipeline!");
    }

    // This call reloads the scheduler info. We need to reload it, because we might alter
    // the pipeline with postprocessor plugins.
    pipelineInfo.setPipelinePluginNames(pluginNames);

    // Set plugin properties.
    for (int i = 0; i < plugins.size(); ++i)
    {
        pipelineInfo.setPipelinePluginProperties(plugins[i].get(), getPluginRelativePosition(i));
        
        // Can we use global ids?
        if (!plugins[i]->preservesFilesCounts())
        {
            canUseGlobalFileIds = false;
        }
    }

    // Initialize all the plugins in the pipeline.
    for (int i = 0; i < plugins.size(); ++i)
    {
        FileTransferer fileTransferer(jobId, pipelineInfo.getSchedulerInfo().getFilesPerDirectory());
        plugin::Plugin::CleanUpType cleanUpType = plugin::Plugin::CleanUpType::CLEAN_UP_NONE;

        if (pipelineInfo.getSchedulerInfo().cleanUp() && pipelineInfo.getSchedulerInfo().continuousCleanUp())
        {
            cleanUpType = plugin::Plugin::CleanUpType::CLEAN_UP_CONTINUOUS_POSTPROCESS;
        }
        else if (pipelineInfo.getSchedulerInfo().cleanUp())
        {
            cleanUpType = plugin::Plugin::CleanUpType::CLEAN_UP_POSTPROCESS;
        }
        else if (pipelineInfo.getSchedulerInfo().continuousCleanUp())
        {
            cleanUpType = plugin::Plugin::CleanUpType::CLEAN_UP_CONTINUOUS;
        }

        plugins[i]->initialize(mpi, Logger::getInstance().getLogger(mpi.getRank()), fileTransferer, getPluginRelativePosition(i), plugins.size(), cleanUpType, canUseGlobalFileIds);
    }

    optimizePipeline(pluginNames);

    // If we cannot use global file ids, but we want to preserve files order, there is only one
    // option to achieve that. We have to limit all the plugins except the first one (generator)
    // to exactly one rank.
    if (schedulerInfo.preserveFilesOrder() && !canUseGlobalFileIds)
    {
        schedulerInfo.reset();      // Reset is necessary, we do not want to preserve user settings.
        oneRankPlugins.clear();

        for (int i = 1; i < plugins.size(); ++i)
        {
            oneRankPlugins.emplace_back(i);
        }
    }

    // Set scheduling of plugins that were automatically placed to the pipeline. Those
    // plugins are supposed to run exactly at one rank. Alternating the schedule may
    // make the pipeline unschedulable, but the risk is small and error of unschedulable
    // pipeline is handled in the scheduler.
    for (const int pluginIndex : oneRankPlugins)
    {
        int rank = schedulerInfo.findLeastBusyRank();
        schedulerInfo.setWherePluginCanRun(pluginIndex, rank);
        schedulerInfo.setWhatCanRunOnRank(rank, pluginIndex);
    }
}

/**
 * Runs the master or worker process.
 */
void bdg::core::pipeline::Pipeline::run()
{
    if (mpi.isMaster())
    {
        bdg::core::pipeline::TaskScheduler scheduler((*this), mpi);
        scheduler.schedule();
    }
    else
    {
        bdg::core::pipeline::TaskReceiver taskReceiver((*this), mpi);
        taskReceiver.receive();
    }
}

/**
 * Returns the plugin relative position of the plugin specified by its index in the pipeline.
 *
 * @param pluginIndex Index of the plugin in the pipeline. First plugin has index 0.
 *
 * @return Relative position of the plugin in the pipeline.
 */
const int bdg::core::pipeline::Pipeline::getPluginRelativePosition(const int pluginIndex) const
{
    std::string usedPluginName = pipelineInfo.getPipelinePluginName(pluginIndex);
    int counter = 0;
    int index = 0;

    for (const auto pluginName : pipelineInfo.getPipelinePluginNames())
    {
        if (pluginName == usedPluginName && index < pluginIndex)
        {
            ++counter;
        }

        ++index;
    }

    return counter;
}

/**
 * Registers plugin with a given name.
 *
 * @param pluginName Name of the plugin.
 * @param pluginNames Output parameter for names of all plugins in the pipeline.
 * @param oneRankPlugins Output parameter indexes of plugins that are allowed to run only on one rank.
 */
void bdg::core::pipeline::Pipeline::registerPlugin(
    const std::string &pluginName,
    std::vector<std::string> &pluginNames,
    std::vector<int> &oneRankPlugins)
{
    bdg_debug(Logger::getInstance().getLogger(mpi.getRank()), "Placing plugin [" + pluginName + "] to pipeline");

    // This line may throw an exception when the plugin does not exist, but this exception is handled in the caller.
    // Note: support of singleton plugins does not bring any advantage, because all the plugins behave as singlerons
    // in the pipeline. They are created here and automatically destroyed when the pipeline gets destroyed which is
    // when all the work is done.
    pluginNames.emplace_back(pluginName);
    plugins.emplace_back(pipelinePluginManager.getPlugin(pluginName));

    // std::vector<bool>.emplace_back - since C++14
    resultQueuesUsedAsPriorityQueue.push_back(pipelineInfo.getSchedulerInfo().preserveFilesOrder());

    // Add postprocessor plugins.
    int counter = 0;

    while (!plugins.back()->getPostprocessor().isUndefined() && counter < MAX_POSTPROCESSOR_PLUGINS)
    {
        bdg::core::pipeline::plugin::PluginPostprocessor postprocessor = plugins.back()->getPostprocessor();
        std::unique_ptr<bdg::core::pipeline::plugin::Plugin> plugin(postprocessor.getPlugin());

        bdg_debug(Logger::getInstance().getLogger(mpi.getRank()), "Placing postprocessor plugin #" + std::to_string(counter + 1) + " [" + plugin->getName() + "] of [" + pluginName + "] to pipeline");
        
        if (postprocessor.requiresOrderedInput())
        {
            // All the result queues before the plugin requiring ordered input must be working
            // as priotity queues.
            for (int i = 0; i < resultQueuesUsedAsPriorityQueue.size(); ++i)
            {
                resultQueuesUsedAsPriorityQueue[i] = true;
            }
        }

        pluginNames.emplace_back(plugin->getName());
        plugins.emplace_back(std::move(plugin));
        resultQueuesUsedAsPriorityQueue.push_back(pipelineInfo.getSchedulerInfo().preserveFilesOrder());

        if (postprocessor.isUnique())
        {
            oneRankPlugins.emplace_back(plugins.size() - 1);
        }

        ++counter;
    }
}

/**
 * When we have a file storer as the last plugin and we use it only for copying
 * files into one location, we can use previous plugin to do that and completely remove file
 * storer from the pipeline.
 *
 * @param pluginNames Names of all plugins in the pipeline.
 */
void bdg::core::pipeline::Pipeline::optimizePipeline(std::vector<std::string> &pluginNames)
{
    if (plugins.size() >= 2)
    {
        const SchedulerInfo &schedulerInfo = pipelineInfo.getSchedulerInfo();
        const plugin::Plugin *lastPlugin = plugins.back().get();
        size_t workersAllowingStorer = 0;

        // Count how many workers are able to run storer.
        for (int rank = 1; rank <= mpi.getSize() && workersAllowingStorer <= 1; ++rank)
        {
            if (schedulerInfo.canRunPluginAtRank(plugins.size() - 1, rank))
            {
                ++workersAllowingStorer;
            }
        }

        // Remove file storer plugin from the pipeline.
        if (lastPlugin->getName() == "FileStorer"
            && lastPlugin->getOutputPaths().size() == 1
            && schedulerInfo.getFileFrequency().isUndefined()
            && workersAllowingStorer > 1
            && pipelineInfo.getSchedulerInfo().useStorerPipelineOptimization())
        {
            const std::string origOutputPath = plugins[plugins.size() - 2]->getOutputPath();
            const std::string extension = Poco::Path(origOutputPath).getExtension();
            const std::string outputPath = FileTransferer::expandFileExtension(lastPlugin->getOutputPathMask(), extension);

            // Remove the file storer plugin.
            pluginNames.pop_back();
            plugins.pop_back();

            // Set the output path of plugin previous to the file storer to the path of the storer.
            plugins.back()->setSettingValue(plugin::Plugin::OUTPUT_PATH_SETTING, outputPath);
            plugins.back()->initOutputPaths();

            // We need to reload scheduler info, because we have just removed a plugin from the pipeline.
            pipelineInfo.setPipelinePluginNames(pluginNames);

            bdg_information(Logger::getInstance().getLogger(mpi.getRank()), "Optimization: FileStorer plugin removed from the pipeline - there is no need for it");
        }
    }

    for (int i = 0; i < plugins.size(); ++i)
    {
        plugins[i]->createOutputPaths();
    }
}
