/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/statisticscounter.h"            // Header

const size_t bdg::core::pipeline::StatisticsCounter::OPTIMAL_GENERATOR_TASK_LENGTH_SECONDS = 5;
const size_t bdg::core::pipeline::StatisticsCounter::MAXIMUM_MASTER_REQUESTS_PER_SECOND = 200;
const size_t bdg::core::pipeline::StatisticsCounter::PROCESSED_TASKS_BEFORE_FATAL_ERROR = 10;
const double bdg::core::pipeline::StatisticsCounter::FATAL_ERROR_THRESHOLD = 0.25;

/**
 * Constructor.
 *
 * @param progressCounter Object for counting the progress of the current job.
 * @param workersCount Size of the cluster where BDG runs.
 * @param pluginsCount Number of plugins placed in the pipeline.
 */
bdg::core::pipeline::StatisticsCounter::StatisticsCounter(const ProgressCounter &progressCounter, const size_t workersCount, const size_t pluginsCount) :
    progressCounter(progressCounter),
    workersCount(workersCount),
    pluginsCount(pluginsCount),
    generatedFilesPerPlugin(pluginsCount, 0),
    generatedBytesPerPlugin(pluginsCount, 0),
    machineTimeSpentPerPlugin(pluginsCount, 0),
    processedTasksPerPlugin(pluginsCount, 0),
    errorTasksPerPlugin(pluginsCount, 0)
{
    // Nothing here.
}

/**
 * Method called when a task is successfully finished. It stores stastics about processed task.
 *
 * @param taskResult Result of task processing.
 */
void bdg::core::pipeline::StatisticsCounter::taskProcessed(const bdg::core::pipeline::SuccessTaskResult *taskResult)
{
    generatedFilesPerPlugin[taskResult->getPluginIndex()] += taskResult->getFilesCount();
    generatedBytesPerPlugin[taskResult->getPluginIndex()] += taskResult->getBytes();
    machineTimeSpentPerPlugin[taskResult->getPluginIndex()] += taskResult->getTotalTime() / 1000;       // Convert microseconds to milliseconds.

    ++processedTasksPerPlugin[taskResult->getPluginIndex()];
}

/**
 * Method called when a task is finished with an error. It stores stastics about the task.
 *
 * @param taskResult Result of task processing.
 */
void bdg::core::pipeline::StatisticsCounter::taskError(const bdg::core::pipeline::ErrorTaskResult *taskResult)
{
    ++processedTasksPerPlugin[taskResult->getPluginIndex()];
    ++errorTasksPerPlugin[taskResult->getPluginIndex()];
}

/**
 * Returns whether we have reached a threshold for error tasks. If more than FATAL_ERROR_THRESHOLD
 * percent of tasks end with an error, this method returns true and currently running job is terminated.
 *
 * @param pluginIndex Index of the plugin in the pipeline for which we want to know if it has reached a error threshold.
 *
 * @return Have we reached a threshold for error tasks?
 */
bool bdg::core::pipeline::StatisticsCounter::tooManyErrors(const int pluginIndex) const
{
    return processedTasksPerPlugin[pluginIndex] > PROCESSED_TASKS_BEFORE_FATAL_ERROR
        && (double)errorTasksPerPlugin[pluginIndex] / processedTasksPerPlugin[pluginIndex] > FATAL_ERROR_THRESHOLD;
}

/**
 * Returns the usual number of files generated in one task for a given plugin.
 *
 * @param pluginIndex Index of the plugin in the pipeline.
 *
 * @return Usual number of files generated in one task.
 */
const size_t bdg::core::pipeline::StatisticsCounter::getUsualGeneratedFiles(const int pluginIndex) const
{
    if (processedTasksPerPlugin[pluginIndex] > 0)
    {
        return std::max<size_t>(1, (size_t)(generatedFilesPerPlugin[pluginIndex] / processedTasksPerPlugin[pluginIndex]));
    }
    else
    {
        return 0;
    }
}

/**
 * Returns the usual size of files generated in one task for a given plugin.
 *
 * @param pluginIndex Index of the plugin in the pipeline.
 *
 * @return Usual size of files generated in one task.
 */
const size_t bdg::core::pipeline::StatisticsCounter::getUsualGeneratedBytes(const int pluginIndex) const
{
    if (processedTasksPerPlugin[pluginIndex] > 0)
    {
        return std::max<size_t>(1, (size_t)(generatedBytesPerPlugin[pluginIndex] / processedTasksPerPlugin[pluginIndex]));
    }
    else
    {
        return 0;
    }
}

/**
 * Returns the number of files that a generator task should generate in OPTIMAL_GENERATOR_TASK_LENGTH_SECONDS
 * seconds. I.e. we want to have the generator task that runs for a given number of seconds.
 *
 * @param Demanded number of files per task from the job configuration file.
 *
 * @return Optimal number of files that a generator task should generate.
 */
const size_t bdg::core::pipeline::StatisticsCounter::getOptimalFilesToGenerate(const size_t demandedFilesPerTask) const
{
    // First plugin in the pipeline is always generator.
    if (machineTimeSpentPerPlugin[0] > 0)
    {
        // Precise to 2 decimal points. We do not want to divide those two possibly huge numbers directly,
        // because of double's precission problems that might occur. Note: machineTimeSpentPerPlugin is in milliseconds.
        const double filesPerSecond = (double)((generatedFilesPerPlugin[0] * 100000) / machineTimeSpentPerPlugin[0]) / 100;

        size_t demandedSeconds = std::max<size_t>(workersCount / MAXIMUM_MASTER_REQUESTS_PER_SECOND, OPTIMAL_GENERATOR_TASK_LENGTH_SECONDS);
        
        if (progressCounter.getTargetAmount().getUnit() == ValueWithUnit::Unit::UNIT_SECOND && progressCounter.getRemainingSeconds() < demandedSeconds)
        {
            demandedSeconds = (size_t)progressCounter.getRemainingSeconds();
        }
        
        size_t filesToGenerate = demandedFilesPerTask > 0 ? demandedFilesPerTask : std::min<size_t>((size_t)(filesPerSecond * demandedSeconds), 1000);
        Poco::UInt64 bytesPerFile = generatedBytesPerPlugin[0] / generatedFilesPerPlugin[0];
        
        // We do not want to generate more files than necessary to avoid unneeded delays.
        switch (progressCounter.getTargetAmount().getUnit())
        {
            case ValueWithUnit::Unit::UNIT_BYTE:
                filesToGenerate = (size_t)std::min<Poco::Int64>(filesToGenerate, (progressCounter.getRemainingBytes() / bytesPerFile) + 1);
                break;

            case ValueWithUnit::Unit::UNIT_FILE:
                filesToGenerate = (size_t)std::min<Poco::Int64>(filesToGenerate, progressCounter.getRemainingFiles());
                break;
        }

        return std::max<size_t>(1, filesToGenerate);
    }
    // We do not have any statistics yet.
    else
    {
        // We have manually configured the task size.
        if (demandedFilesPerTask > 0)
        {
            if (progressCounter.getTargetAmount().getUnit() == ValueWithUnit::Unit::UNIT_FILE)
            {
                // We multiply by 2, because we want to generate at least a bit more. We want to avoid the situation
                // when we have too small tasks in the end of the job.
                return std::max<size_t>(1, (size_t)std::min<Poco::Int64>(demandedFilesPerTask, progressCounter.getRemainingFiles()));
            }
            else
            {
                return demandedFilesPerTask;
            }
        }
        // When the target amount is set as number of files, we start with generating of 1..100 files per task.
        else if (progressCounter.getTargetAmount().getUnit() == ValueWithUnit::Unit::UNIT_FILE)
        {
            return std::max<size_t>(1, (size_t)std::min<Poco::UInt64>(progressCounter.getTargetAmount().getValue() / workersCount, 100));
        }
        // When the target amount is in bytes or seconds, we start with generating of 1 file per task.
        // But this number will grow quickly, because when the first task is finished, this method
        // will go to its first branch, where the optimal number of generated files is counted more exactly.
        else
        {
            return 1;
        }
    }
}

/**
 * Serializes the state of this object into JSON.
 *
 * @param json JSON where we serialize the state.
 */
void bdg::core::pipeline::StatisticsCounter::serialize(Poco::JSON::Object &json) const
{
    Poco::JSON::Array generatedFilesPerPluginJson;
    Poco::JSON::Array generatedBytesPerPluginJson;
    Poco::JSON::Array machineTimeSpentPerPluginJson;
    Poco::JSON::Array processedTasksPerPluginJson;
    Poco::JSON::Array errorTasksPerPluginJson;

    for (int i = 0; i < pluginsCount; ++i)
    {
        generatedFilesPerPluginJson.add(generatedFilesPerPlugin[i]);
        generatedBytesPerPluginJson.add(generatedBytesPerPlugin[i]);
        machineTimeSpentPerPluginJson.add(machineTimeSpentPerPlugin[i]);
        processedTasksPerPluginJson.add(processedTasksPerPlugin[i]);
        errorTasksPerPluginJson.add(errorTasksPerPlugin[i]);
    }

    json.set("generatedFilesPerPlugin", generatedFilesPerPluginJson);
    json.set("generatedBytesPerPlugin", generatedBytesPerPluginJson);
    json.set("machineTimeSpentPerPlugin", machineTimeSpentPerPluginJson);
    json.set("processedTasksPerPlugin", processedTasksPerPluginJson);
    json.set("errorTasksPerPlugin", errorTasksPerPluginJson);
}

/**
 * Deserializes the state of this object from the given JSON.
 *
 * @param json JSON containing the state of this object.
 */
void bdg::core::pipeline::StatisticsCounter::unserialize(const Poco::JSON::Object &json)
{
    Poco::JSON::Array::Ptr generatedFilesPerPluginJson = json.getArray("generatedFilesPerPlugin");
    Poco::JSON::Array::Ptr generatedBytesPerPluginJson = json.getArray("generatedBytesPerPlugin");
    Poco::JSON::Array::Ptr machineTimeSpentPerPluginJson = json.getArray("machineTimeSpentPerPlugin");
    Poco::JSON::Array::Ptr processedTasksPerPluginJson = json.getArray("processedTasksPerPlugin");
    Poco::JSON::Array::Ptr errorTasksPerPluginJson = json.getArray("errorTasksPerPlugin");

    for (int i = 0; i < pluginsCount && i < generatedFilesPerPluginJson->size(); ++i)
    {
        generatedFilesPerPlugin[i] = generatedFilesPerPluginJson->getElement<Poco::UInt64>(i);
    }

    for (int i = 0; i < pluginsCount && i < generatedBytesPerPluginJson->size(); ++i)
    {
        generatedBytesPerPlugin[i] = generatedBytesPerPluginJson->getElement<Poco::UInt64>(i);
    }

    for (int i = 0; i < pluginsCount && i < machineTimeSpentPerPluginJson->size(); ++i)
    {
        machineTimeSpentPerPlugin[i] = machineTimeSpentPerPluginJson->getElement<Poco::UInt64>(i);
    }

    for (int i = 0; i < pluginsCount && i < processedTasksPerPluginJson->size(); ++i)
    {
        processedTasksPerPlugin[i] = processedTasksPerPluginJson->getElement<Poco::UInt64>(i);
    }

    for (int i = 0; i < pluginsCount && i < errorTasksPerPluginJson->size(); ++i)
    {
        errorTasksPerPlugin[i] = errorTasksPerPluginJson->getElement<Poco::UInt64>(i);
    }
}
