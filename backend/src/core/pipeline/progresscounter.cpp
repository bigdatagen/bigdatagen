/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/progresscounter.h"      // Header

#include <Poco/StringTokenizer.h>       // Poco::StringTokenizer

/**
 * Constructor of the progress counter.
 *
 * @param targetAmount Target amount of generated data.
 */
bdg::core::pipeline::ProgressCounter::ProgressCounter(const ValueWithUnit &targetAmount) :
    targetAmount(targetAmount)
{
    // Nothing here.
}

/**
 * Returns what the current progress of the job in percent is.
 *
 * @return Current progress of the job in percent.
 */
double bdg::core::pipeline::ProgressCounter::getProgressGenerated() const
{
    return getProgress(generatedFiles, generatedBytes);
}

/**
 * Returns what part of the job was promissed to be done by currently running tasks.
 * This number should be always at least the same as getProgressGenerated method
 * returns.
 *
 * @return What part of the job was promissed to be done.
 */
double bdg::core::pipeline::ProgressCounter::getProgressPromissed() const
{
    return getProgress(promissedFiles, promissedBytes);
}

/**
 * Updates how long the job is running.
 */
void bdg::core::pipeline::ProgressCounter::updateTimes()
{
    elapsedMicroseconds += clock.elapsed();
    clock.update();
}

/**
 * Handles the message about progress received from some worker. This message contains
 * the number of newly generated files and their size.
 *
 * @param messageData Progress message sent from the worker.
 */
void bdg::core::pipeline::ProgressCounter::handleBytesStoredMessage(const std::string &messageData)
{
    Poco::StringTokenizer parts(messageData, ";");

    generatedFiles += std::stoi(parts[0]);
    generatedBytes += std::stoi(parts[1]);
}

/**
 * Serializes the state of this object into JSON.
 *
 * @param json JSON where we serialize the state.
 */
void bdg::core::pipeline::ProgressCounter::serialize(Poco::JSON::Object &json) const
{
    json.set("generatedFiles", generatedFiles);
    json.set("promissedFiles", promissedFiles);
    json.set("generatedBytes", generatedBytes);
    json.set("promissedBytes", promissedBytes);
    json.set("elapsedMicroseconds", elapsedMicroseconds);
}

/**
 * Deserializes the state of this object from the given JSON.
 *
 * @param json JSON containing the state of this object.
 */
void bdg::core::pipeline::ProgressCounter::unserialize(const Poco::JSON::Object &json)
{
    generatedFiles = json.getValue<Poco::Int64>("generatedFiles");
    promissedFiles = json.getValue<Poco::Int64>("promissedFiles");
    generatedBytes = json.getValue<Poco::Int64>("generatedBytes");
    promissedBytes = json.getValue<Poco::Int64>("promissedBytes");
    elapsedMicroseconds = json.getValue<Poco::UInt64>("elapsedMicroseconds");
}

/**
 * Returns the job progress in percent.
 *
 * @param files Number of generated files so far.
 * @param bytes Size of generated files so far.
 * 
 * @return Job progress in percent.
 */
double bdg::core::pipeline::ProgressCounter::getProgress(const Poco::Int64 files, const Poco::Int64 bytes) const
{
    if (targetAmount.getValue() <= 0)
    {
        return 100;
    }

    Poco::Int64 progress = 0;

    switch (targetAmount.getUnit())
    {
        case ValueWithUnit::Unit::UNIT_FILE:
            // We do not want to divide those two possibly huge numbers directly, because of
            // double's precission problems that might occur. We could also count this as
            // files / targetAmount.getValue() and then in the return clause multiply
            // variable progress by 100 (instead of dividing it).
            progress = (files * 10000) / targetAmount.getValue();
            break;

        case ValueWithUnit::Unit::UNIT_BYTE:
            progress = (bytes * 10000) / targetAmount.getValue();
            break;

        case ValueWithUnit::Unit::UNIT_SECOND:
            progress = (getElapsedSeconds() * 10000) / targetAmount.getValue();
            break;
    }

    return std::max<double>(0, std::min<double>((double)progress / 100, 100));
}
