/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/workerevent.h"          // Header

const std::string bdg::core::pipeline::WorkerEvent::EVENT_TASK_STARTED = "TaskStarted";
const std::string bdg::core::pipeline::WorkerEvent::EVENT_TASK_FINISHED = "TaskFinished";

/**
 * Constructor of the worker event.
 *
 * @param name Name of the event.
 * @param time Timestamp when event occured.
 * @param intNote Additional information to the event.
 */
bdg::core::pipeline::WorkerEvent::WorkerEvent(const std::string &name, const Poco::Timestamp &time, const Poco::Int64 intNote) :
    name(name),
    time(time),
    intNote(intNote)
{
    // Nothing here.
}
