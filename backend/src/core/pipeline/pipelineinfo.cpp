/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/pipelineinfo.h"     // Header

#include <Poco/JSON/Array.h>                // Poco::JSON::Array
#include <Poco/JSON/JSON.h>                 // Poco::JSON::JSON
#include <Poco/JSON/Stringifier.h>          // Poco::JSON::Stringifier
#include <Poco/JSON/JSONException.h>        // Poco::JSON::JSONException
#include <Poco/JSON/Parser.h>               // Poco::JSON::Parser
#include <Poco/String.h>                    // Poco::String

const std::string bdg::core::pipeline::PipelineInfo::PARAM_PIPELINE_CONFIGURATION = "configuration";
const std::string bdg::core::pipeline::PipelineInfo::PROPERTY_PIPELINE_PLUGINS = "pipelinePlugins";
const std::string bdg::core::pipeline::PipelineInfo::PROPERTY_SCHEDULER = "scheduler";
const std::string bdg::core::pipeline::PipelineInfo::PROPERTY_CLUSTER = "cluster";

/**
 * Construcotr of the pipeline info.
 *
 * @param parameters Parameters given by the server.
 * @param clusterSize Number of used MPI threads.
 */
bdg::core::pipeline::PipelineInfo::PipelineInfo(const bdg::core::pipeline::PipelineInfo::Properties &parameters, const int clusterSize) :
    clusterSize(clusterSize),
    schedulerInfo(clusterSize)
{
    const auto configurationIt = parameters.find(PARAM_PIPELINE_CONFIGURATION);

    // Configuration not found.
    if (configurationIt == parameters.cend())
    {
        throw Poco::InvalidArgumentException("Missing required pipeline configuration parameter.");
    }

    loadFromString(configurationIt->second);
}

/**
 * Construcotr of the pipeline info.
 *
 * @param json JSON with the job configuration.
 * @param clusterSize Number of used MPI threads.
 */
bdg::core::pipeline::PipelineInfo::PipelineInfo(const std::string &json, const int clusterSize) :
    clusterSize(clusterSize),
    schedulerInfo(clusterSize)
{
    loadFromString(json);
}

/**
 * Cast operator to std::string.
 *
 * @return Job configuration.
 */
std::string bdg::core::pipeline::PipelineInfo::toString() const
{
    return configuration;
}

/**
 * Returns the total number of used MPI threads.
 *
 * @return Number of used MPI threads.
 */
size_t bdg::core::pipeline::PipelineInfo::getClusterSize() const
{
    Poco::Dynamic::Var sizeVar = getPipelinePluginProperty(PROPERTY_CLUSTER, "size");

    if (sizeVar.isInteger())
    {
        return sizeVar.extract<int>();
    }
    else
    {
        return 0;
    }
}

/**
 * Loads the values of settings and properties of the plugin. Values are loaded from
 * the job configuration.
 * 
 * @param plugin Plugin which should get its properties loaded.
 * @param pluginRelativePosition Relative position of the plugin in the pipeline.
 */
void bdg::core::pipeline::PipelineInfo::setPipelinePluginProperties(bdg::core::pipeline::plugin::Plugin *plugin, const int pluginRelativePosition) const
{
    const std::string pluginNameLower = Poco::toLower(plugin->getName());
    
    if (rootObject->has(pluginNameLower))
    {
        Poco::JSON::Object::Ptr pluginConfigurationObject;

        if (rootObject->isObject(pluginNameLower))
        {
            pluginConfigurationObject = rootObject->getObject(pluginNameLower);
        }
        else if (rootObject->isArray(pluginNameLower) && rootObject->getArray(pluginNameLower)->isObject(pluginRelativePosition))
        {
            pluginConfigurationObject = rootObject->getArray(pluginNameLower)->getObject(pluginRelativePosition);
        }

        if (!pluginConfigurationObject.isNull())
        {
            for (Poco::JSON::Object::ConstIterator it = pluginConfigurationObject->begin(); it != pluginConfigurationObject->end(); ++it)
            {
                plugin->setSettingValue(it->first, it->second);
            }
        }
    }
}

/**
 * Returns the value of the property of the plugin with given name.
 * 
 * @param pluginName Name of the plugin.
 * @param propertyName Name of the property or setting.
 *
 * @return Value of the property.
 */
Poco::Dynamic::Var bdg::core::pipeline::PipelineInfo::getPipelinePluginProperty(const std::string &pluginName, const std::string &propertyName) const
{
    const std::string pluginNameLower = Poco::toLower(pluginName);

    if (rootObject->has(pluginNameLower))
    {
        Poco::Dynamic::Var pluginConfigurationVar = rootObject->get(pluginNameLower);

        if (pluginConfigurationVar.type() == typeid(Poco::JSON::Object::Ptr))
        {
            Poco::JSON::Object::Ptr pluginConfigurationObject = pluginConfigurationVar.extract<Poco::JSON::Object::Ptr>();

            return pluginConfigurationObject->get(propertyName);
        }
    }

    return "";
}

/**
 * Returns the value of the property of the plugin placed in the pipeline on the given index.
 * 
 * @param pluginIndex Index of the plugin in the pipeline.
 * @param propertyName Name of the property or setting.
 *
 * @return Value of the property.
 */
Poco::Dynamic::Var bdg::core::pipeline::PipelineInfo::getPipelinePluginProperty(const int pluginIndex, const std::string &propertyName) const
{
    return getPipelinePluginProperty(getPipelinePluginName(pluginIndex), propertyName);
}

/**
 * Returns the value of the property of the plugin with given name. Value is returned as a string.
 *
 * @param pluginName Name of the plugin.
 * @param propertyName Name of the property or setting.
 *
 * @return Value of the property.
 */
std::string bdg::core::pipeline::PipelineInfo::getPipelinePluginPropertyStr(const std::string &pluginName, const std::string &propertyName) const
{
    Poco::Dynamic::Var property = getPipelinePluginProperty(pluginName, propertyName);

    if (property.isString()
        || property.isInteger()
        || property.isNumeric()
        || property.isBoolean()
        || property.type() == typeid(Poco::JSON::Object::Ptr)
        || property.type() == typeid(Poco::JSON::Array::Ptr))
    {
        return property.toString();
    }
    else
    {
        return "";
    }
}

/**
 * Returns the value of the property of the plugin placed in the pipeline on the given index.
 * Value is returned as a string.
 *
 * @param pluginIndex Index of the plugin in the pipeline.
 * @param propertyName Name of the property or setting.
 *
 * @return Value of the property.
 */
std::string bdg::core::pipeline::PipelineInfo::getPipelinePluginPropertyStr(const int pluginIndex, const std::string &propertyName) const
{
    return getPipelinePluginPropertyStr(getPipelinePluginName(pluginIndex), propertyName);
}

/**
 * Returns the index of the plugin with the given name.
 *
 * @param pluginName Name of the plugin.
 *
 * @return Index of the plugin in the pipeline.
 */
int bdg::core::pipeline::PipelineInfo::getPipelinePluginIndex(const std::string &pluginName) const
{
    const std::string pluginNameLower = Poco::toLower(pluginName);
    int index = 0;

    for (std::vector<std::string>::const_iterator it = pipelinePluginNames.begin(); it != pipelinePluginNames.end(); ++it)
    {
        if (Poco::toLower(*it) == pluginNameLower)
        {
            return index;
        }

        ++index;
    }

    return -1;
}

/**
 * Sets the names of the plugins. This method also calls loadSchedulerInfo method, i.e. scheduler
 * info is reloaded.
 *
 * @param pluginNames Names of plugins placed in the pipeline.
 */
void bdg::core::pipeline::PipelineInfo::setPipelinePluginNames(const std::vector<std::string> &pluginNames)
{
    pipelinePluginNames.clear();

    for (std::vector<std::string>::const_iterator it = pluginNames.begin(); it != pluginNames.end(); ++it)
    {
        addPluginName(*it);
    }

    // Reload scheduler configuration - we have changed plugin names, so their indexes have changed.
    schedulerInfo.reset();
    loadSchedulerInfo();
}

/**
 * Adds one plugin name to the collection with plugin names.
 *
 * @param pluginName Name of a plugin placed in the pipeline.
 */
void bdg::core::pipeline::PipelineInfo::addPluginName(const std::string &pluginName)
{
    pipelinePluginNames.push_back(Poco::toLower(pluginName));
}

/**
 * Loads the pipeline informarion from the JSON string.
 *
 * @param json Serialized information about pipeline and scheduling.
 */
void bdg::core::pipeline::PipelineInfo::loadFromString(const std::string &json)
{
    configuration = json;

    Poco::Dynamic::Var configurationVar = Poco::JSON::Parser().parse(configuration);
    rootObject = configurationVar.extract<Poco::JSON::Object::Ptr>();
        
    // Load plugin names.
    Poco::Dynamic::Var pipelinePluginsVar = rootObject->get(PROPERTY_PIPELINE_PLUGINS);
    Poco::JSON::Array::Ptr pipelinePluginsArray = pipelinePluginsVar.extract<Poco::JSON::Array::Ptr>();

    for (Poco::JSON::Array::ConstIterator it = pipelinePluginsArray->begin(); it != pipelinePluginsArray->end(); ++it)
    {
        addPluginName(it->toString());
    }

    // Load scheduler configuration.
    loadSchedulerInfo();
}

/**
 * Loads the information about scheduling - values of scheduler configuration directives.
 */
void bdg::core::pipeline::PipelineInfo::loadSchedulerInfo()
{
    Poco::JSON::Object::Ptr schedulerConfiguration = rootObject->getObject(PROPERTY_SCHEDULER);

    if (!schedulerConfiguration.isNull())
    {
        // Get where the specified plugins are allowed to run.
        loadSchedulerInfoPlugins(schedulerConfiguration);

        // Get what plugins can run on specified ranks.
        loadSchedulerInfoRanks(schedulerConfiguration);

        // Get store frequency.
        if (schedulerConfiguration->has("speed"))
        {
            schedulerInfo.setFileFrequency(schedulerConfiguration->getValue<std::string>("speed"));

            if (schedulerInfo.getFileFrequency().isUndefined())
            {
                throw Poco::InvalidArgumentException("Invalid value of scheduler.speed parameter!");
            }
        }

        // Get generation target.
        if (schedulerConfiguration->has("total"))
        {
            schedulerInfo.setTotal(schedulerConfiguration->getValue<std::string>("total"));

            if (schedulerInfo.getTotal().getValue() <= 0 || schedulerInfo.getTotal().getUnit() == ValueWithUnit::Unit::UNIT_UNDEFINED || schedulerInfo.getTotal().getUnit() == ValueWithUnit::Unit::UNIT_INVALID)
            {
                throw Poco::InvalidArgumentException("Invalid value of scheduler.total parameter!");
            }
        }
        else
        {
            throw Poco::InvalidArgumentException("Parameter scheduler.total must be specified!");
        }

        schedulerInfo.setSchedulerConfiguration(schedulerConfiguration);
    }
}

/**
 * Loads the mapping between ranks and plugins - where can plugins run.
 *
 * @param schedulerConfiguration JSON object with scheduler configuration.
 */
void bdg::core::pipeline::PipelineInfo::loadSchedulerInfoPlugins(const Poco::JSON::Object::Ptr schedulerConfiguration)
{
    Poco::JSON::Object::Ptr plugins = schedulerConfiguration->getObject("plugins");

    if (!plugins.isNull())
    {
        for (Poco::JSON::Object::ConstIterator it = plugins->begin(); it != plugins->end(); ++it)
        {
            if (it->second.type() == typeid(Poco::JSON::Object::Ptr))
            {
                const int pluginIndex = getPipelinePluginIndex(it->first);
                Poco::JSON::Object::Ptr pluginConfiguration = it->second.extract<Poco::JSON::Object::Ptr>();
                Poco::JSON::Array::Ptr pluginAllowedRanks = pluginConfiguration->getArray("ranks");

                if (!pluginAllowedRanks.isNull() && pluginIndex >= 0)
                {
                    for (Poco::JSON::Array::ConstIterator rankIt = pluginAllowedRanks->begin(); rankIt != pluginAllowedRanks->end(); ++rankIt)
                    {
                        const int rank = std::atoi(rankIt->toString().c_str());

                        if (rank >= 1 && rank < clusterSize)
                        {
                            schedulerInfo.setWherePluginCanRun(pluginIndex, rank);
                        }
                    }
                }
            }
        }
    }
}

/**
 * Loads the mapping between ranks and plugins - what plugins can run on each rank.
 *
 * @param schedulerConfiguration JSON object with scheduler configuration.
 */
void bdg::core::pipeline::PipelineInfo::loadSchedulerInfoRanks(const Poco::JSON::Object::Ptr schedulerConfiguration)
{
    Poco::JSON::Object::Ptr ranks = schedulerConfiguration->getObject("ranks");

    if (!ranks.isNull())
    {
        for (Poco::JSON::Object::ConstIterator it = ranks->begin(); it != ranks->end(); ++it)
        {
            if (it->second.type() == typeid(Poco::JSON::Object::Ptr))
            {
                const int rank = std::atoi(it->first.c_str());
                Poco::JSON::Object::Ptr rankConfiguration = it->second.extract<Poco::JSON::Object::Ptr>();
                Poco::JSON::Array::Ptr rankAllowedPlugins = rankConfiguration->getArray("plugins");

                if (!rankAllowedPlugins.isNull() && rank >= 1 && rank < clusterSize)
                {
                    for (Poco::JSON::Array::ConstIterator plugIt = rankAllowedPlugins->begin(); plugIt != rankAllowedPlugins->end(); ++plugIt)
                    {
                        const int pluginIndex = getPipelinePluginIndex(plugIt->toString());

                        if (pluginIndex >= 0)
                        {
                            schedulerInfo.setWhatCanRunOnRank(rank, pluginIndex);
                        }
                    }
                }
            }
        }
    }
}
