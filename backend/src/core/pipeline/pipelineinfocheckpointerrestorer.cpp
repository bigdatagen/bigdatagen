/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/pipelineinfocheckpointerrestorer.h"         // Header
#include "core/pipeline/plugins/filetransferer.h"                   // FileTransferer

#include <Poco/StreamCopier.h>      // Poco::StreamCopier

/**
 * Constructor of the checkpointer.
 *
 * @param pipelineJobId Id of currently running job.
 */
bdg::core::pipeline::PipelineInfoCheckpointerRestorer::PipelineInfoCheckpointerRestorer(const int pipelineJobId):
    Checkpointer(pipelineJobId)
{
    // Nothing here.
}

/**
 * Loads the pipeline job configuration files to the instance of PipelineInfo.
 *
 * @param mpiMasterRank Rank of the master MPI thread.
 * @param mpiSize Total number of used MPI threads.
 *
 * @return Loaded pipeline job configuration file.
 */
bdg::core::pipeline::PipelineInfo bdg::core::pipeline::PipelineInfoCheckpointerRestorer::restore(const int mpiMasterRank, const int mpiSize) const
{
    std::unique_ptr<FileTransfererStream> stream = FileTransferer().getFileFromRank(getPathToFile(), mpiMasterRank);
    std::string data;

    Poco::StreamCopier::copyToString(stream->getStream(), data);

    return bdg::core::pipeline::PipelineInfo(data, mpiSize);
}
