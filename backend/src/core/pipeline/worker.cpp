/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/worker.h"       // Header

#include <Poco/JSON/Parser.h>           // Poco::JSON::Parser

/**
 * Constructor of the worker.
 */
bdg::core::pipeline::Worker::Worker() :
    state(State::STATE_IDLE)
{
    // Nothing here.
}

/**
 * Constructor of the worker.
 *
 * @param pluginsCount Size of the pipeline - number of used pipeline plugins.
 */
bdg::core::pipeline::Worker::Worker(const int pluginsCount) :
    Worker()
{
    setPluginsCount(pluginsCount);
}

/**
 * Adds a new task to the task queue waiting to be processed on the worker.
 *
 * @param task Task to be processed on the worker.
 */
void bdg::core::pipeline::Worker::addTaskToQueue(std::unique_ptr<Task> &task)
{
    taskQueue.push_back(std::move(task));
}

/**
 * Returns the first task from the task queue. If the queue is empty, nullptr is returned.
 *
 * @return First task from the task queue or nullptr.
 */
std::unique_ptr<bdg::core::pipeline::Task> bdg::core::pipeline::Worker::getTaskFromQueue()
{
    if (!taskQueue.empty())
    {
        std::unique_ptr<bdg::core::pipeline::Task> task = std::move(taskQueue.front());
        taskQueue.pop_front();

        return task;
    }
    else
    {
        return nullptr;
    }
}

/**
 * Serializes the information about the state the worker. This method is used for checkpoints.
 *
 * @return String with serialized worker in the JSON format.
 */
const std::string bdg::core::pipeline::Worker::serialize() const
{
    Poco::JSON::Object json;

    serialize(json);

    std::stringstream stream;
    json.stringify(stream);

    return stream.str();
}

/**
 * Serializes the information about the state the worker. This method is used for checkpoints.
 *
 * @param json JSON object to which we serialize information the worker.
 */
void bdg::core::pipeline::Worker::serialize(Poco::JSON::Object &json) const
{
    Poco::JSON::Array taskQueueJson;
    Poco::JSON::Array pluginStatesJson;
    Poco::JSON::Array taskErrorsJson;

    // Serialize the task to the queue, so it is the first task to start to run at
    // worker when the program is restored from a checkpoint.
    if (processedTask != nullptr)
    {
        Poco::JSON::Object taskJson;
        processedTask->serialize(taskJson);
        taskQueueJson.add(taskJson);
    }

    // Serialize the rest of the task queue.
    for (WorkerTaskQueue::const_iterator it = taskQueue.begin(); it != taskQueue.end(); ++it)
    {
        Poco::JSON::Object taskJson;
        it->get()->serialize(taskJson);
        taskQueueJson.add(taskJson);
    }

    // Serialize plugin states:
    for (int i = 0; i < pluginStates.size(); ++i)
    {
        pluginStatesJson.add(pluginStates[i]);
    }

    // Serialize task errors:
    for (const std::string &error : taskErrors)
    {
        taskErrorsJson.add(error);
    }

    json.set("taskQueue", taskQueueJson);
    json.set("pluginStates", pluginStatesJson);
    json.set("processedTasks", processedTasks);
    json.set("errorTasks", errorTasks);
    json.set("taskErrors", taskErrorsJson);
}

/**
 * Deserializes the information about the state of the workers. This method is used when the job
 * is restored from a checkpoint during error recovery.
 *
 * @param serializedWorker String which contains information about the worker in the JSON.
 * @param pipeline Instance of the pipeline.
 */
void bdg::core::pipeline::Worker::unserialize(const std::string &serializedWorker, const Pipeline &pipeline)
{
    Poco::Dynamic::Var rootVar = Poco::JSON::Parser().parse(serializedWorker);
    Poco::JSON::Object::Ptr rootJson = rootVar.extract<Poco::JSON::Object::Ptr>();
    unserialize(*rootJson, pipeline);
}

/**
 * Deserializes the information about the state of the workers. This method is used when the job
 * is restored from a checkpoint during error recovery.
 *
 * @param json JSON object which contains information about the worker.
 * @param pipeline Instance of the pipeline.
 */
void bdg::core::pipeline::Worker::unserialize(const Poco::JSON::Object &json, const Pipeline &pipeline)
{
    Poco::JSON::Array::Ptr taskQueueJson = json.getArray("taskQueue");
    Poco::JSON::Array::Ptr pluginStatesJson = json.getArray("pluginStates");
    Poco::JSON::Array::Ptr taskErrorsJson = json.getArray("taskErrors");

    for (int i = 0; i < taskQueueJson->size(); ++i)
    {
        Poco::JSON::Object::Ptr taskJson = taskQueueJson->getObject(i);

        Task *task = pipeline.getPlugin(taskJson->getValue<int>("pluginIndex"))->taskFactory();
        task->unserialize(*taskJson);

        taskQueue.push_back(std::unique_ptr<Task>(task));
    }

    for (int i = 0; i < pluginStates.size() && i < pluginStatesJson->size(); ++i)
    {
        pluginStates[i] = *pluginStatesJson->getObject(i);
    }

    for (int i = 0; i < taskErrorsJson->size(); ++i)
    {
        taskErrors.push_back(taskErrorsJson->get(i).toString());
    }

    processedTasks = json.getValue<Poco::UInt64>("processedTasks");
    errorTasks = json.getValue<Poco::UInt64>("errorTasks");
}
