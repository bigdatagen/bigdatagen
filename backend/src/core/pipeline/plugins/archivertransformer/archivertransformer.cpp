/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Transformer
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/archivertransformer/archivertransformer.h"      // Header

/**
 * Constructor of the plugin - registers all its settings.
 */
bdg::core::pipeline::plugin::ArchiverTransformer::ArchiverTransformer() :
    TransformerPlugin("ArchiverTransformer", "Plugin which can compress all the generated files to ZIP", "zip")
{
    addSetting(new PluginSettingInt("filesPerArchive", "How many files should be compressed into one archive", 1, 1, 1000000));
}

/**
 * Processes one generated file - the file is added to a zip archive.
 *
 * @param task Processed task.
 * @param inputFileStream Stream with content of file which should be processed.
 * @param inputFilePath Path to the file which should be processed.
 * @param outputFilePath Path where the result of the transformation should be stored.
 * @param newlyCreatedFilesCount Output parameter for the number of files created in this method.
 */
void bdg::core::pipeline::plugin::ArchiverTransformer::processOneFile(
    const bdg::core::pipeline::TransformerTask *task,
    std::unique_ptr<FileTransfererStream> inputFileStream,
    const Poco::Path &inputFilePath,
    const Poco::Path &outputFilePath,
    size_t &newlyCreatedFilesCount)
{
    if (inputFilePath.toString() != outputFilePath.toString())
    {
        const size_t filesPerArchive = getSettingValueInt("filesPerArchive");

        // We did not created a new archive - we have to inform about that a caller of this method.
        if (filesInArchive < filesPerArchive - 1)
        {
            newlyCreatedFilesCount = 0;
        }

        // Create a new archive.
        if (filesInArchive == 0)
        {
            outputStream = std::unique_ptr<std::ofstream>(new std::ofstream(outputFilePath.toString(), std::ios::binary));
            zip = std::unique_ptr<Poco::Zip::Compress>(new Poco::Zip::Compress(*outputStream.get(), true));
        }

        // Add file to the archive.
        zip->addFile(inputFileStream->getStream(), Poco::DateTime(), inputFilePath.getFileName());
        ++filesInArchive;

        bdg_debug(getLogger(), "Added file to ZIP archive [" + outputFilePath.toString() + "]; "
            + "ID from: " + getFileId(task->getPluginIndex() - 1, task->getResultWorkerRank(), inputFileStream->getFileId()) + ", "
            + "ID: " + getFileId(task->getPluginIndex(), getMpi().getRank(), nextFileIdLocal));

        // Save the archive.
        if (filesInArchive == filesPerArchive)
        {
            filesInArchive = 0;

            zip->close();
            zip = nullptr;
            outputStream = nullptr;

            bdg_debug(getLogger(), "Created ZIP archive [" + outputFilePath.toString() + "]; "
                + "ID: " + getFileId(task->getPluginIndex(), getMpi().getRank(), nextFileIdLocal) + ", "
                + "Size: " + std::to_string(Poco::File(outputFilePath).getSize()) + " bytes");
        }
    }
    else
    {
        throw bdg::utils::TaskFatalErrorException("Archiver: Cannot compress the file [" + inputFilePath.toString() + "] to the same location where it already is");
    }
}

/**
 * Does clean up when a job is finished - closes open stream.
 */
void bdg::core::pipeline::plugin::ArchiverTransformer::clean()
{
    zip = nullptr;
    outputStream = nullptr;
}

using bdg::core::pipeline::plugin::Plugin;
using bdg::core::pipeline::plugin::ArchiverTransformer;

/**
 * Export the plugin for the POCO class loader.
 */
POCO_BEGIN_MANIFEST(Plugin)
    POCO_EXPORT_CLASS(ArchiverTransformer)
POCO_END_MANIFEST
