/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/filetransferer.h"                   // Header
#include "core/pipeline/plugins/filetransferersharedstream.h"       // FileTransfererSharedStream
#include "utils/config.h"                                           // Config

#include <Poco/File.h>              // Poco::File
#include <Poco/Path.h>              // Poco::Path
#include <Poco/String.h>            // Poco::String

#include <random>                   // std::random

bool bdg::core::pipeline::FileTransferer::isSharedFsStatic = false;

/**
 * Constructor of the file transferer.
 *
 * @param filesPerDirectory How many files can be stored in one directory in maximum.
 */
bdg::core::pipeline::FileTransferer::FileTransferer(const int jobId, const size_t filesPerDirectory) :
    isSharedFs(isSharedFsStatic),
    jobId(jobId),
    filesPerDirectory(filesPerDirectory)
{
    // Nothing here.
}

/**
 * Reads files from a worker.
 *
 * @param workerRank Rank of the worker from which we want to read files.
 * @param workerFileMask Mask of files we want to read.
 * @param fileFrom Id of the first file we want to read.
 * @param filesCount Total number of files we want to read.
 *
 * @return Iterator to files we want to read.
 */
bdg::core::pipeline::FileTransfererStreamIterator bdg::core::pipeline::FileTransferer::getFilesFromWorker(const std::string &workerFileMask, const int workerRank, const Poco::UInt64 fileFrom, const size_t filesCount) const
{
    FileTransfererStreamIterator iterator(this, workerFileMask, workerRank, fileFrom, filesCount);
    return iterator;
}

/**
 * Returns stream to the file specified by its id.
 *
 * @param filePath Path to the file.
 * @param rank Rank of the worker from which we want to read the file.
 * @param fileId Id of the file we want to read.
 *
 * @return Stream from which we can read the file.
 */
std::unique_ptr<bdg::core::pipeline::FileTransfererStream> bdg::core::pipeline::FileTransferer::getFileFromRank(const std::string &filePath, const int rank, const Poco::UInt64 fileId) const
{
    if (isSharedFileSystem())
    {
        std::unique_ptr<FileTransfererStream> stream(new FileTransfererSharedStream(filePath, fileId));
        return stream;
    }
    else
    {
        throw Poco::NotImplementedException("File transferer not implemented on not shared file system");
    }
}

/**
 * Creates the structure of directories where generated files are stored.
 *
 * @param paths Paths where the generated files are stored.
 * @param fileFrom Id of the first stored file.
 * @param filesCount Total number of stored files.
 */
void bdg::core::pipeline::FileTransferer::createDirectories(const std::vector<std::string> &paths, const Poco::UInt64 fileFrom, const size_t filesCount) const
{
    for (const auto &path : paths)
    {
        if (!path.empty())
        {
            const bool hasDirectoryNumberPlaceholder = path.find("%d") != std::string::npos;
            const Poco::UInt64 firstDirectoryNumber = hasDirectoryNumberPlaceholder ? getDirectoryNumber(fileFrom) : 1;
            const Poco::UInt64 lastDirectoryNumber = hasDirectoryNumberPlaceholder ? getDirectoryNumber(fileFrom + filesCount - 1) : 1;

            for (Poco::UInt64 i = firstDirectoryNumber; i <= lastDirectoryNumber; ++i)
            {
                // Directory path cannot contain '%i' string - serial number of a file. This is why we use parent() method.
                const Poco::Path filesPath(Poco::replace(path, std::string("%d"), formatDirectoryNumber(i)));
                const Poco::Path directoryPath(filesPath.parent());

                // Create the directory if it does not exist yet.
                try
                {
                    Poco::File directory(directoryPath);
                    directory.createDirectories();
                }
                catch (Poco::Exception&)
                {
                    throw Poco::CreateFileException("Could not create directory [" + directoryPath.toString() + "] as a file storage");
                }
            }
        }
    }
}

/**
 * Returns the formated directory id - string aligned to 7 characters with leading zeroes.
 *
 * @param directoryId Id of the directory.
 *
 * @return Formated directory id.
 */
const std::string bdg::core::pipeline::FileTransferer::formatDirectoryNumber(const Poco::UInt64 directoryId) const
{
    std::string directoryNumber = std::to_string(directoryId);

    const size_t length = directoryNumber.length();
    const size_t requiredLength = 7;

    if (length < requiredLength)
    {
        directoryNumber.insert(0, requiredLength - length, '0');
    }

    return directoryNumber;
}

/**
 * Expands the file mask with % placeholders to the path.
 *
 * @param pathStr File mask.
 * @param pluginRelativePosition Relative position of the plugin in the pipeline - replaces placeholder %p.
 * @param rank Rank of the current thread - replaces placeholder %r.
 * @param useGlobalFileIds Can we use global file ids?
 * @param fileId Id of the file - replaces placeholder %i.
 *
 * @return Expanded file mask - path to the file.
 */
const std::string bdg::core::pipeline::FileTransferer::expandPathMask(const std::string &pathStr, const int pluginRelativePosition, const int rank, const bool useGlobalFileIds, const Poco::UInt64 fileId) const
{
    const Poco::Path path(pathStr);
    const std::string extension(path.getExtension());

    std::string expandedPath(pathStr);

    if (!path.isAbsolute())
    {
        expandedPath = bdg::utils::Config::getInstance().getOutputStoragePath() + Poco::Path::separator() + expandedPath;
    }

    const Poco::Path absolutePath(expandedPath);

    // Directory path cannot contain '%i' string - serial number of file.
    if (absolutePath.parent().toString().find("%i") != std::string::npos)
    {
        expandedPath = Poco::replace(absolutePath.parent().toString(), "%i", "") + absolutePath.getFileName();
    }

    // File nanme must contain '%i' string - serial number of file.
    if (expandedPath.find("%i") == std::string::npos)
    {
        if (extension.length() > 0)
        {
            expandedPath = expandedPath.substr(0, expandedPath.length() - extension.length() - 1) + "-%i." + extension;
        }
        else
        {
            expandedPath += "-%i";
        }
    }

    // Path must contain '%r' string - number of worker's rank.
    if (!useGlobalFileIds && isSharedFileSystem() && expandedPath.find("%r") == std::string::npos)
    {
        expandedPath = Poco::replace(expandedPath, "%i", "%r-%i");
    }

    expandedPath = expandFileMaskPluginRelativePosition(expandedPath, pluginRelativePosition);
    expandedPath = expandFileMaskJobId(expandedPath);
    expandedPath = expandFileMaskRank(expandedPath, rank);

    // Expand file id.
    if (fileId > 0)
    {
        return expandFileMaskFileId(expandedPath, fileId);
    }
    else
    {
        return expandedPath;
    }
}

/**
 * Replaces the placeholder %j for the id of currently running job.
 *
 * @param path Path to the file.
 *
 * @return Expanded path.
 */
const std::string bdg::core::pipeline::FileTransferer::expandFileMaskJobId(const std::string &path) const
{
    return Poco::replace(path, std::string("%j"), std::to_string(jobId));
}

/**
 * Replaces placeholders %i and %d for the id the file and the id of related directory.
 *
 * @param path Path to the file.
 * @param fileId Id of the file.
 *
 * @return Expanded path.
 */
const std::string bdg::core::pipeline::FileTransferer::expandFileMaskFileId(const std::string &path, const Poco::UInt64 fileId) const
{
    std::string expandedPath = Poco::replace(path, std::string("%i"), std::to_string(fileId));
    expandedPath = Poco::replace(expandedPath, std::string("%d"), formatDirectoryNumber(getDirectoryNumber(fileId)));

    return expandedPath;
}

/**
 * Replaces the placeholder %p for the plugin relative position. This is relevant only for transformer
 * plugins - when a plugin is used in the pipeline more than once, the first usage has relative position
 * "1", second usage of the plugin has relative position "2", etc.
 *
 * @param path Path to the file.
 * @param pluginRelativePosition Plugin relative position.
 *
 * @return Expanded path.
 */
const std::string bdg::core::pipeline::FileTransferer::expandFileMaskPluginRelativePosition(const std::string &path, const int pluginRelativePosition)
{
    return Poco::replace(path, std::string("%p"), std::to_string(pluginRelativePosition));
}

/**
 * Replaces the placeholder %r for the rank of currently running MPI thread.
 *
 * @param path Path to the file.
 * @param rank Rank of currently running MPI thread.
 *
 * @return Expanded path.
 */
const std::string bdg::core::pipeline::FileTransferer::expandFileMaskRank(const std::string &path, const int rank)
{
    return Poco::replace(path, std::string("%r"), std::to_string(rank));
}

/**
 * Replaces the placeholder %e for the given file extension.
 *
 * @param path Path to the file.
 * @param fileExtension File extension.
 *
 * @return Expanded path.
 */
const std::string bdg::core::pipeline::FileTransferer::expandFileExtension(const std::string &path, const std::string &fileExtension)
{
    return Poco::replace(path, std::string("%e"), fileExtension);
}

/**
 * Returns the given extension (with "." leading character) or empty string when no extension is used.
 *
 * @param defaultExtension Used extension.
 *
 * @return Extension or an empty string.
 */
std::string bdg::core::pipeline::FileTransferer::getDefaultExtension(const std::string &defaultExtension) const
{
    if (!defaultExtension.empty())
    {
        return "." + defaultExtension;
    }
    else
    {
        return "";
    }
}

/**
 * Detects whether we run on a shared file system or not.
 *
 * @param mpi Instance of MPI.
 * @param jobId If of currently running job.
 *
 * @return Do we run on a shared file system?
 */
bool bdg::core::pipeline::FileTransferer::detectSharedFileSystem(const bdg::utils::MPIProxy &mpi, const int jobId)
{
    std::string outputDirectoryPathStr = bdg::utils::Config::getInstance().getOutputStoragePath() + Poco::Path::separator();
    outputDirectoryPathStr = Poco::replace(outputDirectoryPathStr, std::string("%j"), std::to_string(jobId));
    int fileNumber;

    // Create a random test test file on master.
    if (mpi.isMaster())
    {
        std::random_device rd;
        std::default_random_engine engine(rd());
        std::uniform_int_distribution<int> uniformDist;

        fileNumber = uniformDist(engine);

        // Try to create the output directory.
        const Poco::Path directoryPath(outputDirectoryPathStr);

        try
        {
            Poco::File directory(directoryPath);
            directory.createDirectories();
        }
        catch (Poco::Exception&)
        {
            // Nothing here, we just need to get rid of the exception.
        }

        // Try to create the file.
        Poco::File testFile(outputDirectoryPathStr + std::to_string(fileNumber));

        try
        {
            testFile.createFile();
        }
        catch (Poco::Exception&)
        {
            // Nothing here, we just need to get rid of the exception.
        }
    }

    // Broadcast the file id.
    mpi.broadcastInt(fileNumber);

    // Try to read the file on all the workers.
    Poco::File testFile(outputDirectoryPathStr + std::to_string(fileNumber));

    if (testFile.exists())
    {
        isSharedFsStatic = true;
    }

    mpi.barrier();

    // Delete the test file on master.
    if (mpi.isMaster())
    {
        try
        {
            testFile.remove();
        }
        catch (Poco::Exception&)
        {
            // Nothing here, we just need to get rid of the exception.
        }
    }

    return isSharedFsStatic;
}
