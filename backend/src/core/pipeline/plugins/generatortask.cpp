/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/generatortask.h"            // Header

#include <cassert>      // std::assert
#include <utility>      // std::pair

const std::string bdg::core::pipeline::GeneratorTask::schemaName = "schema";
const std::string bdg::core::pipeline::GeneratorTask::seqsStartName = "seqsStart";
const std::string bdg::core::pipeline::GeneratorTask::remIntsName = "remInts";

/**
 * Constructor of the task.
 *
 * @param metadata Task metadata.
 * @param filesCount Number of files that should be generated.
 */
bdg::core::pipeline::GeneratorTask::GeneratorTask(const GeneratorTaskMetadata *metadata) :
    Task(metadata->getProperties()),
    schema(metadata->getSchema())
{
    setFilesCount(metadata->getFilesCount());
    setPromissedFiles(metadata->getFilesCount());
}

/**
 * Serializes the task. Serialization is needed when the task is sent from the master to a worker.
 *
 * @param json JSON object where the serialized task is stored.
 */
void bdg::core::pipeline::GeneratorTask::serializeAdditional(Poco::JSON::Object &json) const
{
    Task::serializeAdditional(json);
    json.set(schemaName, schema);

    json.set(seqsStartName, serializeVector<std::size_t>(seqsStart, [](Poco::JSON::Array& arr, const std::size_t& idx)
    {
        arr.add(idx);
    }));

    json.set(remIntsName, serializeVector<std::vector<int>>(remInts, [](Poco::JSON::Array& arr, const std::vector<int>& nums)
    {
        arr.add(serializeVector<int>(nums, [](Poco::JSON::Array& inArr, const int& num)
        {
            inArr.add(num);
        }));
    }));
}

/**
 * Deserializes the task. Deserialization is performed on a worker, when a message with the task
 * is delivered.
 *
 * @param json JSON object where the serialized task is stored.
 */
void bdg::core::pipeline::GeneratorTask::unserializeAdditional(const Poco::JSON::Object &json)
{
    Task::unserializeAdditional(json);
    schema = json.getValue<std::string>(schemaName);

    seqsStart = deserializeVector<std::size_t>(json.getArray(seqsStartName), [](const Poco::Dynamic::Var& v)
    {
        return v.convert<std::size_t>();
    });

    remInts = deserializeVector<std::vector<int>>(json.getArray(remIntsName), [](const Poco::Dynamic::Var& v)
    {
        auto arr = v.extract<Poco::JSON::Array::Ptr>();
        return deserializeVector<int>(arr, [](const Poco::Dynamic::Var& num)
        {
            return num.convert<int>();
        });
    });
}
