/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/pluginpostprocessor.h"      // Header

/**
 * Constructor of the plugin post-processor
 *
 * @param plugin Plugin which is a post-processor to some other plugin.
 * @param isUnique Is it a unique plugin? Unique plugin has one allocated thread just for itself.
 * @param requiresOrderedInput Does the plugin need input files ordered by their ids?
 */
bdg::core::pipeline::plugin::PluginPostprocessor::PluginPostprocessor(bdg::core::pipeline::plugin::Plugin *plugin, const bool isUnique, const bool requiresOrderedInput) :
    plugin(plugin),
    pluginIsUnique(isUnique),
    pluginRequiresOrderedInput(requiresOrderedInput)
{
    // Nothing here.
}

/**
 * Creates plugin post-processor definition which runs plugin in one allocated thread
 * and which needs input files ordered by their ids. I.e. there is only once unique
 * instance of the plugin which processes tasks in the whole cluster.
 *
 * @param plugin Plugin which is a post-processor to some other plugin.
 *
 * @return Plugin post-processor definition.
 */
bdg::core::pipeline::plugin::PluginPostprocessor bdg::core::pipeline::plugin::PluginPostprocessor::createUniqueOrdered(Plugin *plugin)
{
    return PluginPostprocessor(plugin, true, true);
}

/**
 * Creates plugin post-processor definition which runs plugin in one allocated thread.
 * I.e. there is only once unique instance of the plugin which processes tasks in the
 * whole cluster.
 *
 * @param plugin Plugin which is a post-processor to some other plugin.
 *
 * @return Plugin post-processor definition.
 */
bdg::core::pipeline::plugin::PluginPostprocessor bdg::core::pipeline::plugin::PluginPostprocessor::createUnique(Plugin *plugin)
{
    return PluginPostprocessor(plugin, true);
}

/**
 * Creates plugin post-processor definition which can run in arbitrary thread.
 *
 * @param plugin Plugin which is a post-processor to some other plugin.
 *
 * @return Plugin post-processor definition.
 */
bdg::core::pipeline::plugin::PluginPostprocessor bdg::core::pipeline::plugin::PluginPostprocessor::create(Plugin *plugin)
{
    return PluginPostprocessor(plugin);
}

/**
 * Creates plugin post-processor definition which indicates that plugin has no post-processor.
 *
 * @return Plugin post-processor definition.
 */
bdg::core::pipeline::plugin::PluginPostprocessor bdg::core::pipeline::plugin::PluginPostprocessor::none()
{
    return PluginPostprocessor();
}
