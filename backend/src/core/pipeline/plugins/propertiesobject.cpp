/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/propertiesobject.h"       // Header

#include <Poco/JSON/Stringifier.h>      // Poco::JSON::Stringifier
#include <Poco/JSON/Parser.h>           // Poco::JSON::Parser
#include <Poco/Dynamic/Var.h>           // Poco::Dynamic::Var

/**
 * Constructor of the object.
 *
 * @param properties Properties assigned to the object.
 */
bdg::core::pipeline::PropertiesObject::PropertiesObject(const Properties &properties) :
    properties(properties)
{
    // Nothing here.
}

/**
 * Serializes the object to the JSON.
 *
 * @return JSON string with the serialized object.
 */
const std::string bdg::core::pipeline::PropertiesObject::serialize() const
{
    Poco::JSON::Object json;

    serialize(json);

    std::stringstream stream;
    json.stringify(stream);

    return stream.str();
}

/**
 * Serializes the object to the JSON.
 *
 * @param json JSON object reprezenting this object.
 */
void bdg::core::pipeline::PropertiesObject::serialize(Poco::JSON::Object &json) const
{
    if (properties.size() > 0)
    {
        Poco::JSON::Object jsonProperties;

        for (Properties::const_iterator it = properties.begin(); it != properties.end(); ++it)
        {
            jsonProperties.set(it->first, it->second);
        }

        json.set("properties", jsonProperties);
    }

    serializeAdditional(json);
}

/**
 * Deserializes the object from the JSON.
 *
 * @param serializedObject JSON string reprezenting this object.
 */
void bdg::core::pipeline::PropertiesObject::unserialize(const std::string &serializedObject)
{
    Poco::Dynamic::Var rootVar = Poco::JSON::Parser().parse(serializedObject);
    Poco::JSON::Object::Ptr rootJson = rootVar.extract<Poco::JSON::Object::Ptr>();
    unserialize(*rootJson);
}

/**
 * Deserializes the object from the JSON.
 *
 * @param json JSON object reprezenting this object.
 */
void bdg::core::pipeline::PropertiesObject::unserialize(const Poco::JSON::Object &json)
{
    if (json.has("properties"))
    {
        Poco::JSON::Object::Ptr propertiesObject = json.getObject("properties");

        for (Poco::JSON::Object::ConstIterator it = propertiesObject->begin(); it != propertiesObject->end(); ++it)
        {
            properties.emplace(it->first, it->second.toString());
        }
    }

    unserializeAdditional(json);
}

/**
 * Method called from the serialize method where additional properties can be serialized
 * to the JSON. This method is meant to be overriden.
 *
 * @param json JSON where this object should be serialized.
 */
void bdg::core::pipeline::PropertiesObject::serializeAdditional(Poco::JSON::Object &json) const
{
    // Nothing here.
}

/**
 * Method called from the deserialize method where additional properties can be deserialized
 * from the JSON. This method is meant to be overriden.
 *
 * @param json JSON where this object is serialized.
 */
void bdg::core::pipeline::PropertiesObject::unserializeAdditional(const Poco::JSON::Object &json)
{
    // Nothing here.
}
