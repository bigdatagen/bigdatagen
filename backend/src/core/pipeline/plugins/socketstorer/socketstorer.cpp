/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Storer
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/socketstorer/socketstorer.h"        // Header

#include <Poco/Net/SocketAddress.h>         // Poco::Net::SocketAddress
#include <Poco/Net/StreamSocket.h>          // Poco::Net::StreamSocket
#include <Poco/Net/SocketStream.h>          // Poco::Net::SocketStream

/**
 * Constructor of the plugin - registers all its settings.
 */
bdg::core::pipeline::plugin::SocketStorer::SocketStorer() :
    StorerPlugin("SocketStorer", "Plugin which can send data to a socket specified by its IP address and port")
{
    addSetting(new PluginSettingArray(new PluginSettingGroup("api", "API", {
        new PluginSettingString("server", "Server where to send generated data", "localhost"),
        new PluginSettingInt("port", "Port of the server", 9900, 0, 65535)
    })));
}

/**
 * Processes one generated file - it sends each file to the socket specified in the
 * plugin settings. Multiple sockets can be configured.
 *
 * @param task Processed task.
 * @param inputFileStream Stream with content of file which should be processed.
 * @param inputFilePath Path to the file which should be processed.
 */
void bdg::core::pipeline::plugin::SocketStorer::processOneFile(
    const bdg::core::pipeline::TransformerTask *task,
    std::unique_ptr<FileTransfererStream> &inputFileStream,
    const Poco::Path &inputFilePath)
{
    const PluginSettingArray::SettingsCollection &apiSettings = getSetting("api")->convert<PluginSettingArray>()->getSettings();

    for (PluginSettingArray::SettingsCollection::const_iterator it = apiSettings.begin(); it != apiSettings.end(); ++it)
    {
        const PluginSettingGroup *apiGroup = it->get()->convert<PluginSettingGroup>();
        const std::string server = apiGroup->getSetting("server")->getValue();
        const std::string port = apiGroup->getSetting("port")->getValue();

        // Create the socket.
        Poco::Net::SocketAddress address(server, port);
        Poco::Net::StreamSocket socket(address);
        Poco::Net::SocketStream socketStream(socket);

        // Copy the file to the socket.
        socketStream << inputFileStream->getStream().rdbuf() << std::flush;

        bdg_debug(getLogger(), "File sent to socket [" + server + ":" + port + "]; "
            + "ID: " + getFileId(task->getPluginIndex() - 1, task->getResultWorkerRank(), inputFileStream->getFileId()) + ", "
            + "Size: " + std::to_string(inputFileStream->getSize()) + " bytes");
    }
}

using bdg::core::pipeline::plugin::Plugin;
using bdg::core::pipeline::plugin::SocketStorer;

/**
 * Export the plugin for the POCO class loader.
 */
POCO_BEGIN_MANIFEST(Plugin)
    POCO_EXPORT_CLASS(SocketStorer)
POCO_END_MANIFEST
