/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/pluginsettingint.h"         // Header

#include <algorithm>        // std::max, std::min

/**
 * Constructor of the setting.
 *
 * @param name Name of the setting.
 * @param description Description of the setting.
 * @param defaultValue Setting default value.
 * @param min Setting minimum allowed value.
 * @param max Setting maximum allowed value.
 * @param help Setting help.
 * @param isAdvancedSetting Should the setting be in advanced settings on the frontend?
 */
bdg::core::pipeline::plugin::PluginSettingInt::PluginSettingInt(
    const std::string &name,
    const std::string &description,
    const int defaultValue,
    const int min,
    const int max,
    const std::string &help,
    const bool isAdvancedSetting
) :
    PluginSetting(name, description, std::to_string(validate(defaultValue, min, max)), help, isAdvancedSetting),
    min(min),
    max(max)
{
    // Nothing here.
}

/**
 * Clones the setting.
 *
 * @return Clone of the setting.
 */
bdg::core::pipeline::plugin::PluginSetting *bdg::core::pipeline::plugin::PluginSettingInt::clone() const
{
    return new PluginSettingInt(getName(), getDescription(), getDefaultValueInt(), min, max, getHelp(), isAdvanced());
}

/**
 * Validates the setting value. It has to be a string representing a number.
 *
 * @param value Setting value.
 *
 * @return Validated setting value.
 */
std::string bdg::core::pipeline::plugin::PluginSettingInt::validate(const std::string &value) const
{
    return std::to_string(validate(std::atoi(value.c_str())));
}

/**
 * Validates the setting value for the minimum and maximum bounds.
 *
 * @param value Setting value.
 *
 * @return Validated setting value.
 */
int bdg::core::pipeline::plugin::PluginSettingInt::validate(const int value) const
{
    return validate(value, min, max);
}

/**
 * Serializes the setting to a JSON.
 *
 * @param json JSON object where the setting should be serialized.
 */
void bdg::core::pipeline::plugin::PluginSettingInt::serialize(Poco::JSON::Object &json) const
{
    PluginSetting::serialize(json);
    json.remove("value");
    json.set("value", getDefaultValueInt());        // We want to serialize the default value as int.
    json.set("min", min);
    json.set("max", max);
}

/**
 * Validates the setting value.
 *
 * @param value Setting value.
 * @param minArg Setting minimum value.
 * @param maxArg Setting maximum value.
 *
 * @return Validated setting value.
 */
int bdg::core::pipeline::plugin::PluginSettingInt::validate(const int value, const int minArg, const int maxArg) const
{
    if (maxArg - minArg >= 0)
    {
        return std::max<int>(minArg, std::min<int>(value, maxArg));
    }
    else
    {
        return value;
    }
}
