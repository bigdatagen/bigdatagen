/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/fatalerrortaskresult.h"     // Header

/**
 * Constructor of the fatal error result.
 *
 * @param msg Message which should be hold in the result and delivered to the master.
 */
bdg::core::pipeline::FatalErrorTaskResult::FatalErrorTaskResult(const std::string &msg) :
    ErrorTaskResult(msg)
{
    // Nothing here.
}

/**
 * Constructor of the fatal error result.
 *
 * @param ex Thrown exception.
 */
bdg::core::pipeline::FatalErrorTaskResult::FatalErrorTaskResult(const std::exception &ex) :
    ErrorTaskResult(ex)
{
    // Nothing here.
}

/**
 * Constructor of the fatal error result.
 *
 * @param ex Thrown exception.
 */
bdg::core::pipeline::FatalErrorTaskResult::FatalErrorTaskResult(const Poco::Exception &ex) :
    ErrorTaskResult(ex)
{
    // Nothing here.
}
