/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Storer
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/filestorer/filestorer.h"    // Header

/**
 * Constructor of the plugin - registers all its settings.
 */
bdg::core::pipeline::plugin::FileStorer::FileStorer() :
    StorerPlugin("FileStorer", "Plugin which can store generated files in a specific directory on the server's filesystem", "%e", true)
{
    // Nothing here.
}

/**
 * Processes one generated file - it copies each file to the location in the file system
 * specified in the plugin settings. Multiple file locations can be configured.
 *
 * @param task Processed task.
 * @param inputFileStream Stream with content of file which should be processed.
 * @param inputFilePath Path to the file which should be processed.
 */
void bdg::core::pipeline::plugin::FileStorer::processOneFile(
    const bdg::core::pipeline::TransformerTask *task,
    std::unique_ptr<FileTransfererStream> &inputFileStream,
    const Poco::Path &inputFilePath)
{
    const auto &fileTransferer = getFileTransferer();

    for (const auto &outputPathMask : getOutputPaths())
    {
        const std::string outputFilePathStr(fileTransferer.expandFileExtension(fileTransferer.expandFileMaskFileId(outputPathMask, nextFileIdLocal), inputFilePath.getExtension()));
   
        // We do not want to copy file to the same destination where it already is.
        if (inputFilePath.toString() != outputFilePathStr || (!fileTransferer.isSharedFileSystem() && task->getResultWorkerRank() != getMpi().getRank()))
        {
            const Poco::File inputFile(inputFilePath);

            if (inputFile.exists())
            {
                inputFile.copyTo(outputFilePathStr);
            }
            else
            {
                std::ofstream outputStream(outputFilePathStr, std::ios::binary);
                Poco::StreamCopier::copyStream(inputFileStream->getStream(), outputStream);
                outputStream.close();
            }
        }
        
        bdg_debug(getLogger(), "Stored file [" + outputFilePathStr + "]; "
            + "ID from: " + getFileId(task->getPluginIndex() - 1, task->getResultWorkerRank(), inputFileStream->getFileId()) + ", "
            + "ID: " + getFileId(task->getPluginIndex(), getMpi().getRank(), nextFileIdLocal) + ", "
            + "Size: " + std::to_string(inputFileStream->getSize()) + " bytes");
    }
}

using bdg::core::pipeline::plugin::Plugin;
using bdg::core::pipeline::plugin::FileStorer;

/**
 * Export the plugin for the POCO class loader.
 */
POCO_BEGIN_MANIFEST(Plugin)
    POCO_EXPORT_CLASS(FileStorer)
POCO_END_MANIFEST
