/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Storer
 * @author   Big Data Generator Team
 *
 * https://msdn.microsoft.com/en-us/library/azure/dd179451.aspx
 * https://msdn.microsoft.com/en-us/library/azure/dd179346.aspx
 * https://gist.github.com/t-mat/5c1b68d56179d7af7e98
 */

#include "core/pipeline/plugins/azurecloudstorer/azurecloudstorer.h"        // Header
#include "utils/common.h"                                                   // UNREFERENCED_VARIABLE

#include <Poco/JSON/Parser.h>                       // Poco::JSON::Parser
#include <Poco/Base64Decoder.h>                     // Poco::Base64Decoder
#include <Poco/Base64Encoder.h>                     // Poco::Base64Encoder
#include <Poco/InflatingStream.h>                   // Poco::InflatingInputStream
#include <Poco/URI.h>                               // Poco::URI

#include <Poco/Net/HTTPClientSession.h>             // Poco::Net::HTTPClientSession
#include <Poco/Net/HTTPSClientSession.h>            // Poco::Net::HTTPSClientSession
#include <Poco/Net/HTTPMessage.h>                   // Poco::Net::HTTPMessage
#include <Poco/Net/HTTPRequest.h>                   // Poco::Net::HTTPRequest
#include <Poco/Net/HTTPStreamFactory.h>             // Poco::Net::HTTPStreamFactory
#include <Poco/Net/HTTPSStreamFactory.h>            // Poco::Net::HTTPSStreamFactory
#include <Poco/Net/AcceptCertificateHandler.h>      // Poco::Net::AcceptCertificateHandler
#include <Poco/Net/InvalidCertificateHandler.h>     // Poco::Net::InvalidCertificateHandler
#include <Poco/Net/Context.h>                       // Poco::Net::Context
#include <Poco/Net/SSLManager.h>                    // Poco::Net::SSLManager
#include <Poco/Net/KeyFileHandler.h>                // Poco::Net::KeyFileHandler

const size_t bdg::core::pipeline::plugin::AzureCloudStorer::RENEW_TOKEN_SECONDS = 3600;
const std::string bdg::core::pipeline::plugin::AzureCloudStorer::AZURE_API_VERSION = "2015-04-05";

/**
 * Constructor of the plugin - registers all its settings.
 */
bdg::core::pipeline::plugin::AzureCloudStorer::AzureCloudStorer() :
    StorerPlugin("AzureCloudStorer", "Plugin which can send data to a Microsoft Azure cloud")
{
    addSetting(new PluginSettingGroup("token", "Token", {
        new PluginSettingString("tokenApiUrl", "URL of the authentication API", "https://cloudtrollbridge.cloudapp.net/tokens/1"),
        new PluginSettingString("tokenRequestGatewayFqn", "Gateway FQN used in the request for access tokens", "MyEnterprise.Monitoring.rapravs"),
        new PluginSettingString("tokenRequestMacAddress", "MAC address used in the request for access tokens", "39-8E-2D-F6-C6-6F")
    }));
    addSetting(new PluginSettingSelect("uploadType", { { "highSpeed", "High Speed" }, { "highThroughput", "High Throughput" } }, "Type of used endpoint", "highThroughput"));
    addSetting(new PluginSettingGroup("highSpeed", "High Speed", {
        new PluginSettingString("serverName", "Name of the server where to send generated files", "racloud-service")
    }));
    addSetting(new PluginSettingGroup("highThroughput", "High Throughput", {
        new PluginSettingString("storageContainerName", "Name of the container in the storage which should be used to store data", "racloud-service-transient"),
        new PluginSettingString("queueName", "Name of the queue where to send the request to process stored data", "racloud-service-transient-queue")
    }));
}

/**
 * Plugin initialization - SSL is initialized here.
 */
void bdg::core::pipeline::plugin::AzureCloudStorer::init()
{
    try
    {
        Poco::Net::HTTPStreamFactory::registerFactory();
        Poco::Net::HTTPSStreamFactory::registerFactory();

        Poco::Net::initializeSSL();
        Poco::Net::SSLManager::PrivateKeyPassphraseHandlerPtr passphraseHandler(new Poco::Net::KeyFileHandler(false));
        Poco::Net::SSLManager::InvalidCertificateHandlerPtr certificateHandler(new Poco::Net::AcceptCertificateHandler(false));
        Poco::Net::Context::Ptr context(new Poco::Net::Context(Poco::Net::Context::CLIENT_USE, ""));
        Poco::Net::SSLManager::instance().initializeClient(passphraseHandler, certificateHandler, context);
    }
    catch (Poco::Exception &e)
    {
        bdg_error(getLogger(), "Failed to initialize SSL!");
        bdg_debug(getLogger(), e.displayText());
        UNREFERENCED_VARIABLE(e);
    }
}

/**
 * Processes one generated file - the file is sent to the Azure Cloud configured in the
 * plugin settings.
 *
 * @param task Processed task.
 * @param inputFileStream Stream with content of file which should be processed.
 * @param inputFilePath Path to the file which should be processed.
 */
void bdg::core::pipeline::plugin::AzureCloudStorer::processOneFile(
    const bdg::core::pipeline::TransformerTask *task,
    std::unique_ptr<FileTransfererStream> &inputFileStream, 
    const Poco::Path &inputFilePath)
{
    // Get access tokens.
    if (usedTokenHolder.expires.empty() || Poco::Timestamp().epochTime() - tokenReceiveTime > RENEW_TOKEN_SECONDS)
    {
        usedTokenHolder = getAccessToken();
        tokenReceiveTime = Poco::Timestamp().epochTime();

        // Failed to obtain the token.
        if (usedTokenHolder.expires.empty())
        {
            throw bdg::utils::TaskFatalErrorException("Could not get access tokens from the authentication API for Windows Azure");
        }

        if (usedTokenHolder.storageUri.empty())
        {
            throw bdg::utils::TaskFatalErrorException("Could not get access token for blob storage - storage not found");
        }

        if (usedTokenHolder.queueUri.empty())
        {
            throw bdg::utils::TaskFatalErrorException("Could not get access token for queue - queue not found");
        }
    }

    // Upload generated file to the right storage.
    if (getSettingValue("uploadType") == "highSpeed")
    {
        uploadToCloudServer(task, inputFileStream, inputFilePath);
    }
    else
    {
        uploadToAzureStorage(task, inputFileStream, inputFilePath);
    }
}

/**
 * Uploads a generated file directly to the Azure Cloud.
 *
 * @param task Processed task.
 * @param inputFileStream Stream with content of file which should be processed.
 * @param inputFilePath Path to the file which should be processed.
 */
void bdg::core::pipeline::plugin::AzureCloudStorer::uploadToCloudServer(
    const bdg::core::pipeline::TransformerTask *task,
    std::unique_ptr<FileTransfererStream> &inputFileStream,
    const Poco::Path &inputFilePath) const
{
    const std::string serverToken = "SharedAccessSignature " + Poco::replace(Poco::replace(encode(usedTokenHolder.serverToken), "\r", ""), "\n", "");
    const std::string serverUrl = usedTokenHolder.serverUri + "/api/1/gatewaytimeseries";
    const bool useGzipCompression = inputFilePath.getExtension() == "gz" || inputFilePath.getExtension() == "gzjson" || inputFilePath.getExtension() == "gzbson";

    // Set content type.
    Poco::Net::HTTPResponse serverResponse;
    std::string serverResponseStr;
    std::string contentType;
    
    if (inputFilePath.getExtension() == "bson" || inputFilePath.getExtension() == "gzbson")
    {
        contentType = "application/bson";
    }
    else
    {
        contentType = "application/json";
    }

    // Create HTTP request headers.
    std::map<std::string, std::string> headers = {
        { "Content-Type", contentType },
        { "Accept", contentType },
        { "Authorization", serverToken }
    };

    if (useGzipCompression)
    {
        headers.emplace("Content-Encoding", "gzip");
    }

    // Send the file to the cloud server.
    serverResponseStr = sendHttpRequest(serverUrl, Poco::Net::HTTPRequest::HTTP_POST, inputFileStream->getStream(), inputFileStream->getSize(), headers, serverResponse);

    // Have we created a new blob in the storage?
    if (serverResponse.getStatus() == Poco::Net::HTTPResponse::HTTP_OK)
    {
        bdg_debug(getLogger(), "File sent to Microsoft Azure cloud API at [" + serverUrl + "]; "
            + "Content-Type: " + contentType + ", "
            + "Encoding: " + (useGzipCompression ? "gzip" : "none") + ", "
            + "ID: " + getFileId(task->getPluginIndex() - 1, task->getResultWorkerRank(), inputFileStream->getFileId()) + ", "
            + "Size: " + std::to_string(inputFileStream->getSize()) + " bytes");
    }
    else
    {
        bdg_error(getLogger(), "AzureCloudStorer ERROR: Cloud server returned code " + std::to_string(serverResponse.getStatus()) + "!");
        bdg_debug(getLogger(), serverResponseStr);

        throw bdg::utils::TaskFatalErrorException("Microsoft Azure cloud API returned invalid HTTP response code " + std::to_string(serverResponse.getStatus()));
    }
}

/**
 * Uploads a generated file to the Azure Cloud blob storage.
 *
 * @param task Processed task.
 * @param inputFileStream Stream with content of file which should be processed.
 * @param inputFilePath Path to the file which should be processed.
 */
void bdg::core::pipeline::plugin::AzureCloudStorer::uploadToAzureStorage(
    const bdg::core::pipeline::TransformerTask *task,
    std::unique_ptr<FileTransfererStream> &inputFileStream,
    const Poco::Path &inputFilePath) const
{
    const std::string fileName = inputFilePath.getFileName();
    const std::string storageUrl = usedTokenHolder.storageUri + "/" + fileName + usedTokenHolder.storageToken;
    const std::string queueUrl = usedTokenHolder.queueUri + "/messages" + usedTokenHolder.queueToken;

    // Create the message:
    const std::string addQueueMessageRequestStr = R"(
        <QueueMessage>
            <MessageText>)" + encode(fileName) + R"(</MessageText>
        </QueueMessage>)";
    std::stringstream addQueueMessageRequest(addQueueMessageRequestStr);

    // Send the file to the blob storage.
    Poco::Net::HTTPResponse storageResponse;
    Poco::Net::HTTPResponse queueResponse;
    std::string storageResponseStr;
    std::string queueResponseStr;

    storageResponseStr = sendHttpRequest(storageUrl, Poco::Net::HTTPRequest::HTTP_PUT, inputFileStream->getStream(), inputFileStream->getSize(), { { "x-ms-version", AZURE_API_VERSION }, { "x-ms-blob-type", "BlockBlob" } }, storageResponse);

    // Have we created a new blob in the storage?
    if (storageResponse.getStatus() == Poco::Net::HTTPResponse::HTTP_CREATED)
    {
        // Send the request to process the file to the queue.
        queueResponseStr = sendHttpRequest(queueUrl, Poco::Net::HTTPRequest::HTTP_POST, addQueueMessageRequest, addQueueMessageRequestStr.size(), { { "x-ms-version", AZURE_API_VERSION } }, queueResponse);

        // Was the queue operation successful?
        if (queueResponse.getStatus() == Poco::Net::HTTPResponse::HTTP_CREATED)
        {
            bdg_debug(getLogger(), "File sent to Microsoft Azure blob storage at [" + storageUrl + "]; "
                + "ID: " + getFileId(task->getPluginIndex() - 1, task->getResultWorkerRank(), inputFileStream->getFileId()) + ", "
                + "Size: " + std::to_string(inputFileStream->getSize()) + " bytes");
        }
        else
        {
            bdg_error(getLogger(), "AzureCloudStorer ERROR: Queue returned code " + std::to_string(queueResponse.getStatus()) + "!");
            bdg_debug(getLogger(), queueResponseStr);

            throw bdg::utils::TaskErrorException("Microsoft Azure queue returned invalid HTTP response code " + std::to_string(queueResponse.getStatus()));
        }
    }
    else
    {
        bdg_error(getLogger(), "AzureCloudStorer ERROR: Blob storage returned code " + std::to_string(storageResponse.getStatus()) + "!");
        bdg_debug(getLogger(), storageResponseStr);

        throw bdg::utils::TaskErrorException("Microsoft Azure blob storage returned invalid HTTP response code " + std::to_string(storageResponse.getStatus()));
    }
}

/**
 * Connects to the Rockwell authentizaion service and tries to obtain authorization token.
 *
 * @return Token returned from the Rockwell authentizaion service.
 */
bdg::core::pipeline::plugin::TokenHolder bdg::core::pipeline::plugin::AzureCloudStorer::getAccessToken() const
{
    const std::string tokenApiUrl = getSetting("token")->convert<PluginSettingGroup>()->getSetting("tokenApiUrl")->getValue();
    const std::string tokenRequestGatewayFqn = getSetting("token")->convert<PluginSettingGroup>()->getSetting("tokenRequestGatewayFqn")->getValue();
    const std::string tokenRequestMacAddress = getSetting("token")->convert<PluginSettingGroup>()->getSetting("tokenRequestMacAddress")->getValue();
    const std::string serverName = getSetting("highSpeed")->convert<PluginSettingGroup>()->getSetting("serverName")->getValue();
    const std::string storageContainerName = getSetting("highThroughput")->convert<PluginSettingGroup>()->getSetting("storageContainerName")->getValue();
    const std::string queueName = getSetting("highThroughput")->convert<PluginSettingGroup>()->getSetting("queueName")->getValue();
    
    TokenHolder tokenHolder;

    try
    {
        const std::string getTokensRequestStr = R"(
        {
            "getTokens": {
                "gatewayFqn": ")" + tokenRequestGatewayFqn + R"(",
                "macAddress": ")" + tokenRequestMacAddress + R"("
            }
        })";
        std::stringstream getTokensRequest(getTokensRequestStr);

        // Send the request to access token API.
        Poco::Net::HTTPResponse response;
        std::string responseStr = sendHttpRequest(tokenApiUrl, Poco::Net::HTTPRequest::HTTP_POST, getTokensRequest, getTokensRequestStr.size(), { { "Content-Type", "application/json" }, { "Accept", "application/json" } }, response);

        // Have we received access tokens?
        if (response.getStatus() == Poco::Net::HTTPResponse::HTTP_OK)
        {
            Poco::JSON::Parser parser;
            Poco::Dynamic::Var responseVar = parser.parse(responseStr);
            Poco::JSON::Object::Ptr responseJson = responseVar.extract<Poco::JSON::Object::Ptr>();
            Poco::JSON::Array::Ptr servers = responseJson->getArray("cloudServers");
            Poco::JSON::Array::Ptr storages = responseJson->getArray("azureStorage");
            Poco::JSON::Array::Ptr queues = responseJson->getArray("azureQueues");

            // Get the cloud server access token.
            for (const auto &server : *servers)
            {
                Poco::JSON::Object::Ptr serverJson = server.extract<Poco::JSON::Object::Ptr>();

                if (serverJson->getValue<std::string>("name") == serverName)
                {
                    tokenHolder.serverToken = decodeToken(serverJson->getValue<std::string>("sasTokenEx"));
                    tokenHolder.serverUri = serverJson->getValue<std::string>("uri");
                }
            }

            // Get the blob storage access token.
            for (const auto &storage : *storages)
            {
                Poco::JSON::Object::Ptr storageJson = storage.extract<Poco::JSON::Object::Ptr>();

                if (storageJson->getValue<std::string>("name") == storageContainerName)
                {
                    tokenHolder.storageToken = decodeToken(storageJson->getValue<std::string>("sasTokenEx"));
                    tokenHolder.storageUri = storageJson->getValue<std::string>("uri");
                }
            }

            // Get the queue access token.
            for (const auto &queue : *queues)
            {
                Poco::JSON::Object::Ptr queueJson = queue.extract<Poco::JSON::Object::Ptr>();

                if (queueJson->getValue<std::string>("name") == queueName)
                {
                    tokenHolder.queueToken = decodeToken(queueJson->getValue<std::string>("sasTokenEx"));
                    tokenHolder.queueUri = queueJson->getValue<std::string>("uri");
                }
            }

            tokenHolder.expires = responseJson->getValue<std::string>("earliestTokenExpires");
        }
        else
        {
            bdg_error(getLogger(), "AzureCloudStorer ERROR: Authentication API returned code " + std::to_string(response.getStatus()) + "!");
            bdg_debug(getLogger(), responseStr);
        }
    }
    catch (Poco::Exception &e)
    {
        bdg_error(getLogger(), "AzureCloudStorer ERROR: Could not get token from the authentication API [" + tokenApiUrl + "]!");
        bdg_debug(getLogger(), e.displayText());
        UNREFERENCED_VARIABLE(e);
    }

    return tokenHolder;
}

/**
 * Sends a HTTP request to the passed URI.
 *
 * @param uri URI where to send a HTTP request.
 * @param method Used HTTP method - GET, PUT, POST or DELETE.
 * @param dataStream Data sent in the request.
 * @param contentLength Size of data sent in the request.
 * @param headers Headers sent in the request.
 * @param response HTTP response to the request.
 *
 * @return Content of the HTTP response.
 */
std::string bdg::core::pipeline::plugin::AzureCloudStorer::sendHttpRequest(
    const std::string &uri, 
    const std::string &method, 
    std::istream &dataStream, 
    const Poco::UInt64 contentLength, 
    const std::map<std::string, std::string> &headers, 
    Poco::Net::HTTPResponse &response) const
{
    const Poco::URI url(uri);
    const std::string urlPath(url.getPathAndQuery());
    std::unique_ptr<Poco::Net::HTTPClientSession> session;

    // Set all the necessary headers.
    if (url.getPort() == 443)
    {
        session.reset(new Poco::Net::HTTPSClientSession(url.getHost(), url.getPort()));
    }
    else
    {
        session.reset(new Poco::Net::HTTPClientSession(url.getHost(), url.getPort()));
    }

    //Poco::Net::HTTPSClientSession session(url.getHost(), url.getPort());
    Poco::Net::HTTPRequest request(method, urlPath, Poco::Net::HTTPMessage::HTTP_1_1);
    request.setContentLength(contentLength);

    for (const auto &header : headers)
    {
        request.add(header.first, header.second);
    }

    // Send the HTTP request.
    std::ostream &requestStream = session->sendRequest(request);
    Poco::StreamCopier::copyStream(dataStream, requestStream);

    // Receive a HTTP response.
    std::string responseStr;
    std::istream &responseStream = session->receiveResponse(response);
    Poco::StreamCopier::copyToString(responseStream, responseStr);

    return responseStr;
}

/**
 * Base64 encode passed data.
 *
 * @param data Data to be encoded.
 *
 * @return Encoded data.
 */
std::string bdg::core::pipeline::plugin::AzureCloudStorer::encode(const std::string &data) const
{
    std::stringstream dataStream;
    Poco::Base64Encoder encoder(dataStream);
    encoder << data;
    encoder.close();

    return dataStream.str();
}

/**
 * Decodes a token obtained from the Rockwell authentizaion service. First it has to be
 * base 64 decoded and the un-gzipped.
 *
 * @param tokenEncoded Original token obtained from the Rockwell authentizaion service.
 *
 * @return Decoded token.
 */
std::string bdg::core::pipeline::plugin::AzureCloudStorer::decodeToken(const std::string &tokenEncoded) const
{
    std::istringstream tokenStream(tokenEncoded);
    Poco::Base64Decoder decoder(tokenStream);
    Poco::InflatingInputStream inflater(decoder, Poco::InflatingStreamBuf::STREAM_GZIP);
    std::string tokenDecoded;

    Poco::StreamCopier::copyToString(inflater, tokenDecoded);
    
    return tokenDecoded;
}

using bdg::core::pipeline::plugin::Plugin;
using bdg::core::pipeline::plugin::AzureCloudStorer;

/**
 * Export the plugin for the POCO class loader.
 */
POCO_BEGIN_MANIFEST(Plugin)
    POCO_EXPORT_CLASS(AzureCloudStorer)
POCO_END_MANIFEST
