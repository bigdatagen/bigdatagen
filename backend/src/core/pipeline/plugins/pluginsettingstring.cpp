/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/pluginsettingstring.h"      // Header

/**
 * Constructor of the setting.
 *
 * @param name Name of the setting.
 * @param description Description of the setting.
 * @param defaultValue Setting default value.
 * @param help Setting help.
 * @param isAdvancedSetting Should the setting be in advanced settings on the frontend?
 */
bdg::core::pipeline::plugin::PluginSettingString::PluginSettingString(
    const std::string &name,
    const std::string &description,
    const std::string &defaultValue,
    const std::string &help,
    const bool isAdvancedSetting
) :
    PluginSetting(name, description, defaultValue, help, isAdvancedSetting)
{
    // Nothing here.
}

/**
 * Clones the setting.
 *
 * @return Clone of the setting.
 */
bdg::core::pipeline::plugin::PluginSetting *bdg::core::pipeline::plugin::PluginSettingString::clone() const
{
    return new PluginSettingString(getName(), getDescription(), getDefaultValue(), getHelp(), isAdvanced());
}
