/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/pluginsettingarray.h"       // Header

/**
 * Constructor of the setting.
 *
 * @param multipliedSetting Setting that is being multiplied.
 */
bdg::core::pipeline::plugin::PluginSettingArray::PluginSettingArray(bdg::core::pipeline::plugin::PluginSetting *multipliedSetting) :
    PluginSetting(multipliedSetting->getName(), multipliedSetting->getDescription(), multipliedSetting->getDefaultValue(), multipliedSetting->getHelp(), multipliedSetting->isAdvanced()),
    multipliedSetting(multipliedSetting)
{
    if (multipliedSetting == nullptr)
    {
        throw Poco::NullPointerException("Setting array: Multiplied setting cannot be null");
    }

    if (multipliedSetting->getType() == SettingType::SETTING_TYPE_ARRAY)
    {
        throw Poco::InvalidArgumentException("Setting array cannot contain another array");
    }

    if (multipliedSetting->getType() == SettingType::SETTING_TYPE_GROUP && !multipliedSetting->containsScalars())
    {
        throw Poco::InvalidArgumentException("Too much recursion in the settings");
    }
}

/**
 * Clones the setting.
 *
 * @return Clone of the setting.
 */
bdg::core::pipeline::plugin::PluginSetting *bdg::core::pipeline::plugin::PluginSettingArray::clone() const
{
    return new PluginSettingArray(multipliedSetting->clone());
}

/**
 * Sets the setting value.
 *
 * @param valueJson Setting value.
 */
void bdg::core::pipeline::plugin::PluginSettingArray::setValue(const Poco::Dynamic::Var &valueJson)
{
    PluginSetting::setValue(valueJson);
    Poco::JSON::Array::Ptr valueArray;
    
    try
    {
        valueArray  = valueJson.extract<Poco::JSON::Array::Ptr>();
    }
    catch (...)
    {
        // We just need to catch exceptions.
    }
    
    settings.clear();

    if (!valueArray.isNull())
    {
        for (Poco::JSON::Array::ConstIterator it = valueArray->begin(); it != valueArray->end(); ++it)
        {
            PluginSetting *newSetting = multipliedSetting->clone();
            newSetting->setValue(*it);
            settings.push_back(std::unique_ptr<PluginSetting>(newSetting));
        }
    }
    else
    {
        PluginSetting *newSetting = multipliedSetting->clone();
        newSetting->setValue(valueJson);
        settings.push_back(std::unique_ptr<PluginSetting>(newSetting));
    }
}

/**
 * Returns whether the setting contains only scalar settings (i.e. no groups or arrays).
 *
 * @return Is it a setting which contains only scalar settings?
 */
bool bdg::core::pipeline::plugin::PluginSettingArray::containsScalars() const
{
    return multipliedSetting->getType() != SettingType::SETTING_TYPE_ARRAY && multipliedSetting->getType() != SettingType::SETTING_TYPE_GROUP;
}

/**
 * Serializes the setting to a JSON.
 *
 * @param json JSON object where the setting should be serialized.
 */
void bdg::core::pipeline::plugin::PluginSettingArray::serialize(Poco::JSON::Object &json) const
{
    multipliedSetting->serialize(json);

    std::string typeStr = json.getValue<std::string>("type");
    json.remove("type");
    json.set("type", typeStr + "[]");
}
