/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/pluginsettingselect.h"      // Header

/**
 * Constructor of the implementation of the setting.
 *
 * @param name Name of the setting.
 * @param items Items from which a user can select a value.
 * @param description Description of the setting.
 * @param defaultValue Setting default value.
 * @param help Setting help.
 * @param isAdvancedSetting Should the setting be in advanced settings on the frontend?
 */
bdg::core::pipeline::plugin::PluginSettingSelect::PluginSettingSelect(
    const std::string &name,
    const std::map<std::string, std::string> &items,
    const std::string &description,
    const std::string &defaultValue,
    const std::string &help,
    const bool isAdvancedSetting
) :
    PluginSetting(name, description, validate(defaultValue, items), help, isAdvancedSetting),
    items(items)
{
    if (items.size() == 0)
    {
        throw Poco::InvalidArgumentException("Select setting must have at least one item");
    }
}

/**
 * Clones the setting.
 *
 * @return Clone of the setting.
 */
bdg::core::pipeline::plugin::PluginSetting *bdg::core::pipeline::plugin::PluginSettingSelect::clone() const
{
    return new PluginSettingSelect(getName(), items, getDescription(), getDefaultValue(), getHelp(), isAdvanced());
}

/**
 * Validates the setting value to be one of the values specified in the constructor.
 * If the value is invalid a default value is returned.
 *
 * @param value Setting value.
 *
 * @return Validated setting value.
 */
std::string bdg::core::pipeline::plugin::PluginSettingSelect::validate(const std::string &value) const
{
    return validate(value, items);
}

/**
 * Serializes the setting to a JSON.
 *
 * @param json JSON object where the setting should be serialized.
 */
void bdg::core::pipeline::plugin::PluginSettingSelect::serialize(Poco::JSON::Object &json) const
{
    PluginSetting::serialize(json);
    Poco::JSON::Array itemsJson;

    // Loop through all the select items and add them to the JSON.
    for (std::map<std::string, std::string>::const_iterator it = items.begin(); it != items.end(); ++it)
    {
        Poco::JSON::Object oneItemJson;
        oneItemJson.set("value", it->first);
        oneItemJson.set("name", it->second);
        itemsJson.add(oneItemJson);
    }

    json.set("items", itemsJson);
}

/**
 * Validates the setting value to be one of the values specified in the constructor.
 * If the value is invalid a default value is returned.
 *
 * @param value Setting value.
 * @param itemsArg Setting allowed values.
 *
 * @return Validated setting value.
 */
std::string bdg::core::pipeline::plugin::PluginSettingSelect::validate(const std::string &value, const std::map<std::string, std::string> &itemsArg) const
{
    if (itemsArg.find(value) != itemsArg.end())
    {
        return value;
    }
    else if (itemsArg.size() > 0)
    {
        return itemsArg.begin()->first;
    }
    else
    {
        return "";
    }
}
