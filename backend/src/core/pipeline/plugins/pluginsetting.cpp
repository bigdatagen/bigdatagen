/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/pluginsetting.h"        // Header

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            namespace plugin
            {
                /**
                 * Implementation of the setting - PIMPL design pattern.
                 */
                struct PluginSettingImpl
                {
                public:
                    PluginSettingImpl(const std::string &name, const std::string &description, const std::string &defaultValue, const std::string &help, const bool isAdvancedSetting);
                    ~PluginSettingImpl() = default;

                    /**
                     * Name of the setting.
                     */
                    const std::string name;

                    /**
                     * Description of the setting.
                     */
                    const std::string description;

                    /**
                     * Setting default value.
                     */
                    const std::string defaultValue;

                    /**
                     * Setting help.
                     */
                    const std::string help;

                    /**
                     * Should the setting be in advanced settings on the frontend?
                     */
                    const bool isAdvancedSetting;

                    /**
                     * Value of the setting.
                     */
                    std::string value;
                };
            }
        }
    }
}

/**
 * Constructor of the implementation of the setting.
 *
 * @param name Name of the setting.
 * @param description Description of the setting.
 * @param defaultValue Setting default value.
 * @param help Setting help.
 * @param isAdvancedSetting Should the setting be in advanced settings on the frontend?
 */
bdg::core::pipeline::plugin::PluginSettingImpl::PluginSettingImpl(
    const std::string &name,
    const std::string &description,
    const std::string &defaultValue,
    const std::string &help,
    const bool isAdvancedSetting
) :
    name(name),
    description(description),
    defaultValue(defaultValue),
    help(help),
    isAdvancedSetting(isAdvancedSetting)
{
    // Nothing here.
}

/**
 * Constructor of the setting.
 *
 * @param name Name of the setting.
 * @param description Description of the setting.
 * @param defaultValue Setting default value.
 * @param help Setting help.
 * @param isAdvancedSetting Should the setting be in advanced settings on the frontend?
 */
bdg::core::pipeline::plugin::PluginSetting::PluginSetting(
    const std::string &name,
    const std::string &description,
    const std::string &defaultValue,
    const std::string &help,
    const bool isAdvancedSetting
) :
    impl(std::unique_ptr<PluginSettingImpl>(new PluginSettingImpl(name, description, defaultValue, help, isAdvancedSetting)))
{
    // Nothing here.
}

/**
 * Destructor of the setting.
 */
bdg::core::pipeline::plugin::PluginSetting::~PluginSetting()
{
    impl.reset();
}

/**
 * Returns name of the setting.
 *
 * @return Name of the setting.
 */
const std::string &bdg::core::pipeline::plugin::PluginSetting::getName() const
{
    return impl->name;
}

/**
 * Returns description of the setting.
 *
 * @return Description of the setting.
 */
const std::string &bdg::core::pipeline::plugin::PluginSetting::getDescription() const
{
    return impl->description;
}

/**
 * Returns setting default value.
 *
 * @return Setting default value.
 */
const std::string &bdg::core::pipeline::plugin::PluginSetting::getDefaultValue() const
{
    return impl->defaultValue;
}

/**
 * Returns setting help.
 *
 * @return Setting help.
 */
const std::string &bdg::core::pipeline::plugin::PluginSetting::getHelp() const
{
    return impl->help;
}

/**
 * Returns whether the setting should be in advanced settings on the frontend.
 *
 * @return Should the setting be in advanced settings?
 */
bool bdg::core::pipeline::plugin::PluginSetting::isAdvanced() const
{
    return impl->isAdvancedSetting;
}

/**
 * Sets a value of the setting.
 *
 * @param value Value of the setting.
 */
void bdg::core::pipeline::plugin::PluginSetting::setValue(const std::string &value)
{
    impl->value = validate(value);
}

/**
 * Returns a value of the setting.
 *
 * @return Current value of the setting.
 */
const std::string &bdg::core::pipeline::plugin::PluginSetting::getValue() const
{
    return !impl->value.empty() ? impl->value : impl->defaultValue;
}

/**
 * Sets a value of the setting.
 *
 * @param valueJson Value of the setting.
 */
void bdg::core::pipeline::plugin::PluginSetting::setValue(const Poco::Dynamic::Var &valueJson)
{
    setValue(valueJson.toString());
}

/**
 * Validates the setting value.
 *
 * @param value Original setting value.
 *
 * @return Valid setting value.
 */
std::string bdg::core::pipeline::plugin::PluginSetting::validate(const std::string &value) const
{
    return value;
}

/**
 * Returns a string representing the type of the setting.
 *
 * @return Type of the setting.
 */
std::string bdg::core::pipeline::plugin::PluginSetting::getTypeString() const
{
    switch (getType())
    {
        case SettingType::SETTING_TYPE_STRING:
            return "STRING";
            break;

        case SettingType::SETTING_TYPE_INT:
            return "INT";
            break;

        case SettingType::SETTING_TYPE_CHECKBOX:
            return "CHECKBOX";
            break;

        case SettingType::SETTING_TYPE_SELECT:
            return "SELECT";
            break;

        case SettingType::SETTING_TYPE_ARRAY:
            return "ARRAY";
            break;

        case SettingType::SETTING_TYPE_GROUP:
            return "GROUP";
            break;

        default:
            return "UNKNOWN";
            break;
    }
}

/**
 * Returns whether the setting contains only scalar settings (i.e. no groups or arrays).
 *
 * @return Is it a setting which contains only scalar settings?
 */
bool bdg::core::pipeline::plugin::PluginSetting::containsScalars() const
{
    return true;
}

/**
 * Serializes the setting to a JSON.
 *
 * @param json JSON object where the setting should be serialized.
 */
void bdg::core::pipeline::plugin::PluginSetting::serialize(Poco::JSON::Object &json) const
{
    json.set("name", impl->name);
    json.set("description", impl->description);
    json.set("value", impl->defaultValue);
    json.set("help", impl->help);
    json.set("isAdvanced", impl->isAdvancedSetting);
    json.set("type", Poco::toLower(getTypeString()));
}
