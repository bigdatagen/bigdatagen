/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/generatorplugin.h"      // Header

#include <Poco/File.h>          // Poco::File

const std::string bdg::core::pipeline::plugin::GeneratorPlugin::SCHEMA_SETTING = "schema";

/**
 * Constructor of the transformer plugin.
 *
 * @param name Name of the plugin.
 * @param description Description of the plugin.
 * @param extension File extension of files produced by the plugin.
 */
bdg::core::pipeline::plugin::GeneratorPlugin::GeneratorPlugin(const std::string &name, const std::string &description, const std::string &extension) :
    Plugin(name, description, "%j/" + convertToDashes(name) + "/rank-%r/%d/file-%i." + extension, false, true)
{
    addSetting(new PluginSettingString(SCHEMA_SETTING, "Schema for generator", "", "", true));
}

/**
 * Method called on the worker to create the right instance of the task. Task serialized in the
 * string is then deserialized into that instance and passed back to the plugin to be processed.
 *
 * @return Type safe instance of a task.
 */
bdg::core::pipeline::Task *bdg::core::pipeline::plugin::GeneratorPlugin::taskFactory()
{
    return new bdg::core::pipeline::GeneratorTask;
}

/**
 * Method called on the master to create a task. This task is then sent to a worker to be processed.
 *
 * @param metadata Task metadata which define task properties.
 *
 * @return Task which should be sent to a worker to be processed.
 */
bdg::core::pipeline::Task *bdg::core::pipeline::plugin::GeneratorPlugin::createTask(const bdg::core::pipeline::TaskMetadata *metadata)
{
    return new bdg::core::pipeline::GeneratorTask(convertMetadata(metadata));
}

/**
 * Processes one task. This method is called on worker when it receives a task that should be processed.
 * This method calls generateOneFile method for each file that should be generated in the task. Method
 * generateOneFile is meant to be implemented by the concrete plugin - a descendant of this class.
 *
 * @param task Task to be processed.
 *
 * @return Result of task processing.
 */
bdg::core::pipeline::TaskResult *bdg::core::pipeline::plugin::GeneratorPlugin::processTaskImpl(const bdg::core::pipeline::Task *task)
{
    // Generator always uses global file ids, because it does not porcess any files from some plugins
    // that are in the pipeline before it (there are no such plugins, because it is a first plugin).
    nextFileIdLocal = task->getFileFrom();

    const auto generatorTask = convertTask(task);
    const auto filesCount = generatorTask->getFilesCount();
    const auto fileIdFrom = nextFileIdLocal;
    const auto storagePath = getOutputPath();
    
    bdg_information(getLogger(), "Processing " + getName() + " task; From: " + std::to_string(nextFileIdLocal) + ", Files: " + std::to_string(filesCount));

    size_t generatedFiles = 0;
    size_t errorFiles = 0;
    Poco::UInt64 generatedBytes = 0;
    Poco::Timestamp startTime;

    if (!createOutputDirectories(fileIdFrom, filesCount))
    {
        bdg_error(getLogger(), "Could not create output directory in plugin [" + getName() + "]");
        return new bdg::core::pipeline::ErrorTaskResult("Could not create directory output directory in plugin [" + getName() + "]");
    }

    // Generate files.
    while (generatedFiles < filesCount && canContinue(task, generatedFiles, generatedBytes, startTime))
    {
        const Poco::Path outputFilePath = getFileTransferer().expandFileMaskFileId(storagePath, nextFileIdLocal);
        bdg::core::pipeline::ErrorTaskResult *result = nullptr;

        // Generate the file.
        try
        {
            generatedBytes += generateOneFile(generatorTask, outputFilePath);
            ++nextFileIdLocal;
            ++generatedFiles;
        }
        catch (bdg::utils::TaskFatalErrorException &e)
        {
            result = new bdg::core::pipeline::FatalErrorTaskResult(e.message());
        }
        catch (bdg::utils::TaskErrorException &e)
        {
            result = new bdg::core::pipeline::ErrorTaskResult(e.message());
        }
        catch (Poco::Exception &e)
        {
            result = new bdg::core::pipeline::ErrorTaskResult("Could not generate file [" + outputFilePath.toString() + "]. " + e.displayText());
        }
        catch (std::exception &e)
        {
            result = new bdg::core::pipeline::ErrorTaskResult("Could not generate file [" + outputFilePath.toString() + "]. " + e.what());
        }

        // Did we reach the error threshold (ratio of error/target files is too high)?
        if (result != nullptr)
        {
            bdg_warning(getLogger(), result->getMessage());
            ++errorFiles;

            if (result->getType() == TaskResult::ResultType::RESULT_TYPE_FATAL_ERROR || (double)errorFiles / filesCount > MAX_ERROR_FILES_RATIO)
            {
                nextFileIdLocal = 1;
                return result;
            }
        }
    }

    nextFileIdLocal = 1;
    bdg_information(getLogger(), "Finished " + getName() + " task in " + std::to_string(startTime.elapsed() / 1000000) + "." + std::to_string((startTime.elapsed() / 100000) % 10) + "s");

    return new bdg::core::pipeline::SuccessTaskResult(storagePath, fileIdFrom, generatedFiles, generatedBytes, startTime.elapsed());
}

/**
 * Converts task metadata to the right type.
 *
 * @param metadata Task metadata to be converted.
 *
 * @return Type safe instance of task metadata.
 */
bdg::core::pipeline::GeneratorTaskMetadata *bdg::core::pipeline::plugin::GeneratorPlugin::convertMetadata(const bdg::core::pipeline::TaskMetadata *metadata) const
{
    return (GeneratorTaskMetadata*)metadata;
}

/**
 * Converts task to the right type.
 *
 * @param task Task to be converted.
 *
 * @return Type safe instance of the task.
 */
bdg::core::pipeline::GeneratorTask *bdg::core::pipeline::plugin::GeneratorPlugin::convertTask(const bdg::core::pipeline::Task *task) const
{
    return (GeneratorTask*)task;
}

/**
 * Returns the schema according to which plugin generates files.
 *
 * @return Schema for generating.
 */
std::string bdg::core::pipeline::plugin::GeneratorPlugin::getSchema() const
{
    return getSettingValue(SCHEMA_SETTING);
}

