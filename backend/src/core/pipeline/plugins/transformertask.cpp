/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/transformertask.h"          // Header

/**
 * Constructor of the task.
 *
 * @param metadata Task metadata.
 */
bdg::core::pipeline:: TransformerTask::TransformerTask(const TransformerTaskMetadata *metadata) :
    Task(metadata->getProperties()),
    resultWorkerFileMask(metadata->getResultWorkerFileMask()),
    resultWorkerRank(metadata->getResultWorkerRank())
{
    setFilesCount(metadata->getResultWorkerFilesCount());
    setFileFrom(metadata->getResultWorkerFileFrom());
    setPromissedFiles(metadata->getResultWorkerFilesCount());
    setPromissedBytes(metadata->getResultWorkerBytes());
}

/**
 * Serializes the task. Serialization is needed when the task is sent from the master to a worker.
 *
 * @param json JSON object where the serialized task is stored.
 */
void bdg::core::pipeline::TransformerTask::serializeAdditional(Poco::JSON::Object &json) const
{
    Task::serializeAdditional(json);
    json.set("resultWorkerFileMask", resultWorkerFileMask);
    json.set("resultWorkerRank", resultWorkerRank);
}

/**
 * Deserializes the task. Deserialization is performed on a worker, when a message with the task
 * is delivered.
 *
 * @param json JSON object where the serialized task is stored.
 */
void bdg::core::pipeline::TransformerTask::unserializeAdditional(const Poco::JSON::Object &json)
{
    Task::unserializeAdditional(json);
    resultWorkerFileMask = json.getValue<std::string>("resultWorkerFileMask");
    resultWorkerRank = json.getValue<int>("resultWorkerRank");
}
