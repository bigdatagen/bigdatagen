# Project for pipeline plugin API
project(BdgPluginApi CXX)

include_directories(
    ${HDR_DIR}
    ${MPI_INCLUDE_PATH}
)

# What kind of library should it be?
add_library(${PROJECT_NAME} SHARED
    "${HDR_DIR}/core/pipeline/plugins/common.h"
    "${SRC_DIR}/core/pipeline/plugins/plugin.cpp"
    "${HDR_DIR}/core/pipeline/plugins/plugin.h"
    "${SRC_DIR}/core/pipeline/plugins/generatorplugin.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generatorplugin.h"
    "${SRC_DIR}/core/pipeline/plugins/transformerplugin.cpp"
    "${HDR_DIR}/core/pipeline/plugins/transformerplugin.h"
    "${SRC_DIR}/core/pipeline/plugins/storerplugin.cpp"
    "${HDR_DIR}/core/pipeline/plugins/storerplugin.h"
    "${SRC_DIR}/core/pipeline/plugins/speedlimiter.cpp"
    "${HDR_DIR}/core/pipeline/plugins/speedlimiter.h"
    "${SRC_DIR}/core/pipeline/plugins/pluginsetting.cpp"
    "${HDR_DIR}/core/pipeline/plugins/pluginsetting.h"
    "${SRC_DIR}/core/pipeline/plugins/pluginsettingstring.cpp"
    "${HDR_DIR}/core/pipeline/plugins/pluginsettingstring.h"
    "${SRC_DIR}/core/pipeline/plugins/pluginsettingint.cpp"
    "${HDR_DIR}/core/pipeline/plugins/pluginsettingint.h"
    "${SRC_DIR}/core/pipeline/plugins/pluginsettingcheckbox.cpp"
    "${HDR_DIR}/core/pipeline/plugins/pluginsettingcheckbox.h"
    "${SRC_DIR}/core/pipeline/plugins/pluginsettingselect.cpp"
    "${HDR_DIR}/core/pipeline/plugins/pluginsettingselect.h"
    "${SRC_DIR}/core/pipeline/plugins/pluginsettinggroup.cpp"
    "${HDR_DIR}/core/pipeline/plugins/pluginsettinggroup.h"
    "${SRC_DIR}/core/pipeline/plugins/pluginsettingarray.cpp"
    "${HDR_DIR}/core/pipeline/plugins/pluginsettingarray.h"
    "${SRC_DIR}/core/pipeline/plugins/pluginpostprocessor.cpp"
    "${HDR_DIR}/core/pipeline/plugins/pluginpostprocessor.h"
    "${SRC_DIR}/core/pipeline/plugins/filefrequency.cpp"
    "${HDR_DIR}/core/pipeline/plugins/filefrequency.h"
    "${SRC_DIR}/core/pipeline/plugins/filetransferer.cpp"
    "${HDR_DIR}/core/pipeline/plugins/filetransferer.h"
    "${SRC_DIR}/core/pipeline/plugins/filetransferersharedstream.cpp"
    "${HDR_DIR}/core/pipeline/plugins/filetransferersharedstream.h"
    "${SRC_DIR}/core/pipeline/plugins/filetransfererstream.cpp"
    "${HDR_DIR}/core/pipeline/plugins/filetransfererstream.h"
    "${SRC_DIR}/core/pipeline/plugins/filetransfererstreamiterator.cpp"
    "${HDR_DIR}/core/pipeline/plugins/filetransfererstreamiterator.h"
    "${SRC_DIR}/core/pipeline/plugins/propertiesobject.cpp"
    "${HDR_DIR}/core/pipeline/plugins/propertiesobject.h"
    "${SRC_DIR}/core/pipeline/plugins/task.cpp"
    "${HDR_DIR}/core/pipeline/plugins/task.h"
    "${SRC_DIR}/core/pipeline/plugins/generatortask.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generatortask.h"
    "${SRC_DIR}/core/pipeline/plugins/transformertask.cpp"
    "${HDR_DIR}/core/pipeline/plugins/transformertask.h"
    "${SRC_DIR}/core/pipeline/plugins/storertask.cpp"
    "${HDR_DIR}/core/pipeline/plugins/storertask.h"
    "${SRC_DIR}/core/pipeline/plugins/taskmetadata.cpp"
    "${HDR_DIR}/core/pipeline/plugins/taskmetadata.h"
    "${SRC_DIR}/core/pipeline/plugins/generatortaskmetadata.cpp"
    "${HDR_DIR}/core/pipeline/plugins/generatortaskmetadata.h"
    "${SRC_DIR}/core/pipeline/plugins/transformertaskmetadata.cpp"
    "${HDR_DIR}/core/pipeline/plugins/transformertaskmetadata.h"
    "${SRC_DIR}/core/pipeline/plugins/storertaskmetadata.cpp"
    "${HDR_DIR}/core/pipeline/plugins/storertaskmetadata.h"
    "${SRC_DIR}/core/pipeline/plugins/taskresult.cpp"
    "${HDR_DIR}/core/pipeline/plugins/taskresult.h"
    "${SRC_DIR}/core/pipeline/plugins/successtaskresult.cpp"
    "${HDR_DIR}/core/pipeline/plugins/successtaskresult.h"
    "${SRC_DIR}/core/pipeline/plugins/errortaskresult.cpp"
    "${HDR_DIR}/core/pipeline/plugins/errortaskresult.h"
    "${SRC_DIR}/core/pipeline/plugins/fatalerrortaskresult.cpp"
    "${HDR_DIR}/core/pipeline/plugins/fatalerrortaskresult.h"
    "${SRC_DIR}/core/pipeline/plugins/valuewithunit.cpp"
    "${HDR_DIR}/core/pipeline/plugins/valuewithunit.h"
    "${SRC_DIR}/utils/mpiproxy.cpp"
    "${HDR_DIR}/utils/mpiproxy.h"
    "${SRC_DIR}/utils/exception.cpp"
    "${HDR_DIR}/utils/exception.h"
)

target_link_libraries(${PROJECT_NAME}
    BdgUtil
    Foundation
    Util
    JSON
    ${MPI_CXX_LIBRARIES}
)

set_target_properties("${PROJECT_NAME}"
    PROPERTIES
    DEFINE_SYMBOL BDG_PLUGIN_API_EXPORTS
)

POST_BUILD_LIB_COPY()

install (
    TARGETS ${PROJECT_NAME}
    RUNTIME DESTINATION ${BDG_BIN_DIR_FULL}/pipelineplugins
    LIBRARY DESTINATION ${BDG_BIN_DIR_FULL}/pipelineplugins
)
install (
    TARGETS ${PROJECT_NAME}
    RUNTIME DESTINATION ${BDG_BIN_DIR_FULL}/plugins
    LIBRARY DESTINATION ${BDG_BIN_DIR_FULL}/plugins
)
install (
    TARGETS ${PROJECT_NAME}
    RUNTIME DESTINATION ${BDG_BIN_DIR_FULL}
    LIBRARY DESTINATION ${BDG_BIN_DIR_FULL}
)
