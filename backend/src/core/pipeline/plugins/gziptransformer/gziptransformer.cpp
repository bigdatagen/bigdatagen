/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Transformer
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/gziptransformer/gziptransformer.h"      // Header
#include <Poco/DeflatingStream.h>       // Poco::DeflatingStream

/**
 * Constructor of the plugin - registers all its settings.
 */
bdg::core::pipeline::plugin::GzipTransformer::GzipTransformer() :
    TransformerPlugin("GzipTransformer", "Plugin which can compress all the generated files to GZIP", "gz")
{
    // Nothing here.
}

/**
 * Processes one generated file - the file is gzipped and stored to the location
 * configured in the "outputPath" setting.
 *
 * @param task Processed task.
 * @param inputFileStream Stream with content of file which should be processed.
 * @param inputFilePath Path to the file which should be processed.
 * @param outputFilePath Path where the result of the transformation should be stored.
 * @param newlyCreatedFilesCount Output parameter for the number of files created in this method.
 */
void bdg::core::pipeline::plugin::GzipTransformer::processOneFile(
    const bdg::core::pipeline::TransformerTask *task,
    std::unique_ptr<FileTransfererStream> inputFileStream,
    const Poco::Path &inputFilePath,
    const Poco::Path &outputFilePath,
    size_t &newlyCreatedFilesCount)
{
    if (inputFilePath.toString() != outputFilePath.toString())
    {
        std::ofstream outputStream(outputFilePath.toString(), std::ios::binary);
        Poco::DeflatingOutputStream deflater(outputStream, Poco::DeflatingStreamBuf::STREAM_GZIP);
        Poco::StreamCopier::copyStream(inputFileStream->getStream(), deflater);

        deflater.close();
        outputStream.close();

        bdg_debug(getLogger(), "Created GZIP archive [" + outputFilePath.toString() + "]; "
            + "ID from: " + getFileId(task->getPluginIndex() - 1, task->getResultWorkerRank(), inputFileStream->getFileId()) + ", "
            + "ID: " + getFileId(task->getPluginIndex(), getMpi().getRank(), nextFileIdLocal) + ", "
            + "Size: " + std::to_string(Poco::File(outputFilePath).getSize()) + " bytes");
    }
    else
    {
        throw bdg::utils::TaskFatalErrorException("Gzip: Cannot compress the file [" + inputFilePath.toString() + "] to the same location where it already is");
    }
}

using bdg::core::pipeline::plugin::Plugin;
using bdg::core::pipeline::plugin::GzipTransformer;

/**
 * Export the plugin for the POCO class loader.
 */
POCO_BEGIN_MANIFEST(Plugin)
    POCO_EXPORT_CLASS(GzipTransformer)
POCO_END_MANIFEST
