/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/generatortaskmetadata.h"        // Header

/**
 * Constructor of the task metadata.
 *
 * @param properties Task metadata properties.
 */
bdg::core::pipeline::GeneratorTaskMetadata::GeneratorTaskMetadata(const Properties &properties) :
    TaskMetadata(properties)
{
    // Nothing here.
}
