/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/taskresult.h"       // Header

/**
 * Serializes values of the error result. Serialization is needed when the error result is
 * sent from a worker to the master.
 *
 * @param json JSON object where the serialized error result is stored.
 */
void bdg::core::pipeline::TaskResult::serializeAdditional(Poco::JSON::Object &json) const
{
    PropertiesObject::serializeAdditional(json);
    json.set("pluginIndex", pluginIndex);
    json.set("rank", rank);
    json.set("fileFrom", fileFrom);
    json.set("filesCount", filesCount);
}

/**
 * Deserializes values of the error result. Deserialization is performed on the master,
 * when a message with error result is delivered.
 *
 * @param json JSON object where the serialized error result is stored.
 */
void bdg::core::pipeline::TaskResult::unserializeAdditional(const Poco::JSON::Object &json)
{
    PropertiesObject::unserializeAdditional(json);
    pluginIndex = json.getValue<int>("pluginIndex");
    rank = json.getValue<int>("rank");
    fileFrom = json.getValue<Poco::UInt64>("fileFrom");
    filesCount = json.getValue<size_t>("filesCount");
}
