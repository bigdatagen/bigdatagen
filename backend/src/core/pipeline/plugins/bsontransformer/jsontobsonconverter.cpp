/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Transformer
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/bsontransformer/jsontobsonconverter.h"          // Header

#include <Poco/JSON/Parser.h>           // Poco::JSON::Parser
#include <Poco/MongoDB/Array.h>         // Poco::MongoDB::Array
#include <Poco/DateTimeParser.h>        // Poco::DateTimeParser
#include <Poco/DateTime.h>              // Poco::DateTime
#include <Poco/Timestamp.h>             // Poco::Timestamp

/**
 * Converts an input given in JSON format to the BSON format.
 *
 * @param istr Input stream with data in JSON format.
 * @param ostr Output stream where data in BSON format will be stored.
 */
void bdg::core::pipeline::plugin::JsonToBsonConverter::convert(std::istream &istr, std::ostream &ostr)
{
    Poco::Dynamic::Var any = Poco::JSON::Parser().parse(istr);
    Poco::BinaryWriter writer(ostr);

    Poco::MongoDB::Element::Ptr rootElement = convert("", any);
    Poco::MongoDB::Document::Ptr document = static_cast<Poco::MongoDB::ConcreteElement<Poco::MongoDB::Document::Ptr>*>(rootElement.get())->value();

    document->write(writer);
}

/**
 * Resursively converts a given JSON element to BSON element.
 *
 * @param fieldName Name of the converted field.
 * @param any Converted JSON element.
 */
Poco::MongoDB::Element::Ptr bdg::core::pipeline::plugin::JsonToBsonConverter::convert(const std::string &fieldName, const Poco::Dynamic::Var &any)
{
    if (any.type() == typeid(Poco::JSON::Object))
    {
        return convertObject(fieldName, any.extract<Poco::JSON::Object>());
    }
    else if (any.type() == typeid(Poco::JSON::Array))
    {
        return convertArray(fieldName, any.extract<Poco::JSON::Array>());
    }
    else if (any.type() == typeid(Poco::JSON::Object::Ptr))
    {
        return convertObject(fieldName, *any.extract<Poco::JSON::Object::Ptr>());
    }
    else if (any.type() == typeid(Poco::JSON::Array::Ptr))
    {
        return convertArray(fieldName, *any.extract<Poco::JSON::Array::Ptr>());
    }
    else if (any.isInteger())
    {
        try
        {
            return new Poco::MongoDB::ConcreteElement<Poco::Int64>(fieldName, any.extract<Poco::Int64>());
        }
        catch (...)
        {
            return new Poco::MongoDB::ConcreteElement<Poco::Int32>(fieldName, any.extract<Poco::Int32>());
        }
    }
    else if (any.isNumeric())
    {
        return new Poco::MongoDB::ConcreteElement<double>(fieldName, any.extract<double>());
    }
    else if (any.isBoolean())
    {
        return new Poco::MongoDB::ConcreteElement<bool>(fieldName, any.extract<bool>());
    }
    else if (any.isEmpty())
    {
        return new Poco::MongoDB::ConcreteElement<Poco::MongoDB::NullValue>(fieldName, Poco::MongoDB::NullValue());
    }
    else
    {
        std::string value = any.extract<std::string>();
        Poco::DateTime dateTime;
        int timeZoneDiff;

        if (Poco::DateTimeParser::tryParse(value, dateTime, timeZoneDiff))
        {
            return new Poco::MongoDB::ConcreteElement<Poco::Timestamp>(fieldName, Poco::Timestamp(dateTime.timestamp().raw()));
        }
        else
        {
            return new Poco::MongoDB::ConcreteElement<std::string>(fieldName, value);
        }
    }
}

/**
 * Resursively converts a given JSON object to BSON object.
 *
 * @param fieldName Name of the converted field.
 * @param json Converted JSON object.
 */
Poco::MongoDB::Element::Ptr bdg::core::pipeline::plugin::JsonToBsonConverter::convertObject(const std::string &fieldName, const Poco::JSON::Object &json)
{
    Poco::MongoDB::Document::Ptr doc = new Poco::MongoDB::Document();

    for (const auto &jsonPair : json)
    {
        doc->addElement(convert(jsonPair.first, jsonPair.second));
    }

    return new Poco::MongoDB::ConcreteElement<Poco::MongoDB::Document::Ptr>(fieldName, doc);
}

/**
 * Resursively converts a given JSON array to BSON array.
 *
 * @param fieldName Name of the converted field.
 * @param json Converted JSON array.
 */
Poco::MongoDB::Element::Ptr bdg::core::pipeline::plugin::JsonToBsonConverter::convertArray(const std::string &fieldName, const Poco::JSON::Array &json)
{
    Poco::MongoDB::Array::Ptr doc = new Poco::MongoDB::Array();
    int i = 0;

    for (const auto &jsonElement : json)
    {
        doc->addElement(convert(std::to_string(i), jsonElement));
        ++i;
    }

    return new Poco::MongoDB::ConcreteElement<Poco::MongoDB::Array::Ptr>(fieldName, doc);
}
