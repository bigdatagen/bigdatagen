/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Transformer
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/bsontransformer/bsontransformer.h"          // Header
#include "core/pipeline/plugins/bsontransformer/jsontobsonconverter.h"      // JsonToBsonConverter
#include "utils/common.h"                                                   // UNREFERENCED_VARIABLE

#include <Poco/JSON/JSONException.h>        // Poco::JSON::JSONException

/**
 * Constructor of the plugin - registers all its settings.
 */
bdg::core::pipeline::plugin::BsonTransformer::BsonTransformer() :
    TransformerPlugin("BsonTransformer", "Plugin which can convert files from JSON to BSON", "bson")
{
    // Nothing here.
}

/**
 * Processes one generated file - the file is transformed from JSON to BSON and stored to
 * the location configured in the "outputPath" setting.
 *
 * @param task Processed task.
 * @param inputFileStream Stream with content of file which should be processed.
 * @param inputFilePath Path to the file which should be processed.
 * @param outputFilePath Path where the result of the transformation should be stored.
 * @param newlyCreatedFilesCount Output parameter for the number of files created in this method.
 */
void bdg::core::pipeline::plugin::BsonTransformer::processOneFile(
    const bdg::core::pipeline::TransformerTask *task,
    std::unique_ptr<FileTransfererStream> inputFileStream,
    const Poco::Path &inputFilePath,
    const Poco::Path &outputFilePath,
    size_t &newlyCreatedFilesCount)
{
    std::ofstream outputStream(outputFilePath.toString(), std::ios::binary);

    try
    {
        JsonToBsonConverter::convert(inputFileStream->getStream(), outputStream);
    }
    catch (Poco::JSON::JSONException &e)
    {
        bdg_debug(getLogger(), e.displayText());
        UNREFERENCED_VARIABLE(e);
        throw bdg::utils::TaskFatalErrorException("Could not convert JSON to BSON");
    }

    outputStream.close();

    bdg_debug(getLogger(), "Transformed file to BSON [" + outputFilePath.toString() + "]; "
        + "ID from: " + getFileId(task->getPluginIndex() - 1, task->getResultWorkerRank(), inputFileStream->getFileId()) + ", "
        + "ID: " + getFileId(task->getPluginIndex(), getMpi().getRank(), nextFileIdLocal) + ", "
        + "Size: " + std::to_string(Poco::File(outputFilePath).getSize()) + " bytes");
}

using bdg::core::pipeline::plugin::Plugin;
using bdg::core::pipeline::plugin::BsonTransformer;

/**
 * Export the plugin for the POCO class loader.
 */
POCO_BEGIN_MANIFEST(Plugin)
    POCO_EXPORT_CLASS(BsonTransformer)
POCO_END_MANIFEST
