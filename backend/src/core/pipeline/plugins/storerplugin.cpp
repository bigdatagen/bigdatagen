/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/storerplugin.h"         // Header
#include "core/pipeline/plugins/speedlimiter.h"         // SpeedLimiter

/**
 * Constructor of the storer plugin.
 *
 * @param name Name of the plugin.
 * @param description Description of the plugin.
 * @param extension File extension of files produced by the plugin.
 * @param outputPathIsMultiplied Can the plugin have multiple output paths?
 */
bdg::core::pipeline::plugin::StorerPlugin::StorerPlugin(const std::string &name, const std::string &description, const std::string &extension, const bool outputPathIsMultiplied) :
    Plugin(name, description, !extension.empty() ? "%j/" + convertToDashes(name) + "/rank-%r/%d/file-%i." + extension : "", outputPathIsMultiplied, false)
{
    // Nothing here.
}

/**
 * Method called on the worker to create the right instance of the task. Task serialized in the
 * string is then deserialized into that instance and passed back to the plugin to be processed.
 *
 * @return Type safe instance of a task.
 */
bdg::core::pipeline::Task *bdg::core::pipeline::plugin::StorerPlugin::taskFactory()
{
    return new bdg::core::pipeline::StorerTask;
}

/**
 * Method called on the master to create a task. This task is then sent to a worker to be processed.
 *
 * @param metadata Task metadata which define task properties.
 *
 * @return Task which should be sent to a worker to be processed.
 */
bdg::core::pipeline::Task *bdg::core::pipeline::plugin::StorerPlugin::createTask(const bdg::core::pipeline::TaskMetadata *metadata)
{
    return new bdg::core::pipeline::StorerTask(convertMetadata(metadata));
}

/**
 * Processes one task. This method is called on worker when it receives a task that should be processed.
 * This method calls processOneFile method for each file that should be processed in the task. Method
 * processOneFile is meant to be implemented by the concrete plugin - a descendant of this class. In
 * this method the speed limitation algorithm is implemented.
 *
 * @param task Task to be processed.
 *
 * @return Result of task processing.
 */
bdg::core::pipeline::TaskResult *bdg::core::pipeline::plugin::StorerPlugin::processTaskImpl(const bdg::core::pipeline::Task *task)
{
    // Do we use global file ids?
    if (useGlobalFileIds())
    {
        nextFileIdLocal = task->getFileFrom();
    }

    auto storerTask = convertTask(task);
    const auto resultFileMask = storerTask->getResultWorkerFileMask();
    const auto resultRank = storerTask->getResultWorkerRank();
    const auto resultRankFileFrom = storerTask->getFileFrom();
    const auto filesCount = storerTask->getFilesCount();
    const auto fileIdFrom = nextFileIdLocal;
    const auto storagePath = getOutputPath();
    const auto &fileFrequency = storerTask->getFrequency();

    bdg_information(getLogger(), "Processing " + getName() + " task; Rank: " + std::to_string(resultRank) + ", From: " + std::to_string(resultRankFileFrom) + ", Files: " + std::to_string(filesCount));

    FileTransfererStreamIterator fileIterator(getFileTransferer().getFilesFromWorker(resultFileMask, resultRank, resultRankFileFrom, filesCount));
    SpeedLimiter speedLimiter(fileFrequency, filesCount);

    std::string usedExtension = "";
    size_t processedFiles = 0;
    size_t errorFiles = 0;
    Poco::UInt64 processedBytes = 0;
    Poco::Timestamp startTime;
    std::vector<std::string> inputFilesPaths;

    if (!createOutputDirectories(fileIdFrom, filesCount))
    {
        bdg_error(getLogger(), "Could not create output directory in plugin [" + getName() + "]");
        return new bdg::core::pipeline::ErrorTaskResult("Could not create directory output directory in plugin [" + getName() + "]");
    }
    
    // Store files from the worker.
    while (fileIterator.canMoveToNextFile() && canContinue(task, processedFiles, processedBytes, startTime))
    {
        // Can we store the file right now?
        if (speedLimiter.canProcessFile())
        {
            fileIterator.moveToNextFile();
            
            const Poco::Path inputFilePath(fileIterator.getCurrentPath());
            
            Poco::UInt64 bytes = 0;
            bdg::core::pipeline::ErrorTaskResult *result = nullptr;
            usedExtension = inputFilePath.getExtension();
            inputFilesPaths.push_back(inputFilePath.toString());

            // Store the file.
            try
            {
                std::unique_ptr<bdg::core::pipeline::FileTransfererStream> currentFile = fileIterator.getCurrentFile();     // This line may throw an exception when the file does not exist.
                processOneFile(storerTask, currentFile, inputFilePath);
                bytes = currentFile->getSize();
            }
            catch (bdg::utils::TaskFatalErrorException &e)
            {
                result = new bdg::core::pipeline::FatalErrorTaskResult(e.message());
            }
            catch (bdg::utils::TaskErrorException &e)
            {
                result = new bdg::core::pipeline::ErrorTaskResult(e.message());
            }
            catch (Poco::Exception &e)
            {
                result = new bdg::core::pipeline::ErrorTaskResult("Could not store file [" + inputFilePath.toString() + "] from worker " + std::to_string(resultRank) + ". " + e.displayText());
            }
            catch (std::exception &e)
            {
                result = new bdg::core::pipeline::ErrorTaskResult("Could not store file [" + inputFilePath.toString() + "] from worker " + std::to_string(resultRank) + ". " + e.what());
            }

            // We have to count everything out of the try-catch block, because storer plugins are
            // obliged to output as many files as were given in the task.
            processedBytes += bytes;

            ++nextFileIdLocal;
            ++processedFiles;

            // Did we reach the error threshold (ratio of error/target files is too high)?
            if (result != nullptr)
            {
                bdg_warning(getLogger(), result->getMessage());
                ++errorFiles;

                if (result->getType() == TaskResult::ResultType::RESULT_TYPE_FATAL_ERROR || (double)errorFiles / filesCount > MAX_ERROR_FILES_RATIO)
                {
                    nextFileIdLocal = useGlobalFileIds() ? 1 : fileIdFrom;
                    return result;
                }
            }
            // If necessary, send the message about my speed to the master node. 
            else
            {
                speedLimiter.fileProcessed(getMpi(), bytes, fileIterator.canMoveToNextFile() && canContinue(task, processedFiles, processedBytes, startTime));
            }
        }
        // Wait for the moment when a next batch of files can be sent.
        else
        {
            speedLimiter.waitForNextBatch();
        }
    }

    // Actively wait to end with the period, i.e. if we are too quick in storing, we want to be
    // globally (all storers together) precise.
    speedLimiter.waitTillPeriodEnd();

    // Do we use global file ids?
    if (useGlobalFileIds())
    {
        nextFileIdLocal = 1;
    }

    // Do continuous clean up.
    if (getCleanUpType() == CleanUpType::CLEAN_UP_CONTINUOUS || getCleanUpType() == CleanUpType::CLEAN_UP_CONTINUOUS_POSTPROCESS)
    {
        for (const auto &inputFilePath : inputFilesPaths)
        {
            try
            {
                Poco::File(inputFilePath).remove();
            }
            catch (Poco::Exception &e)
            {
                bdg_warning(getLogger(), "Could not remove file [" + inputFilePath + "] in continuous clean up!");
                bdg_debug(getLogger(), e.displayText());
                UNREFERENCED_VARIABLE(e);
            }
        }
    }

    bdg_information(getLogger(), "Finished " + getName() + " task in " + std::to_string(startTime.elapsed() / 1000000) + "." + std::to_string((startTime.elapsed() / 100000) % 10) + "s");

    // If we do not store the files somewhere locally, we return the original location of files.
    if (storagePath.empty())
    {
        return new bdg::core::pipeline::SuccessTaskResult(resultFileMask, resultRankFileFrom, processedFiles, processedBytes, startTime.elapsed());
    }
    else
    {
        return new bdg::core::pipeline::SuccessTaskResult(getFileTransferer().expandFileExtension(storagePath, usedExtension), fileIdFrom, processedFiles, processedBytes, startTime.elapsed());
    }
}

/**
 * Converts task metadata to the right type.
 *
 * @param metadata Task metadata to be converted.
 *
 * @return Type safe instance of task metadata.
 */
bdg::core::pipeline::StorerTaskMetadata *bdg::core::pipeline::plugin::StorerPlugin::convertMetadata(const bdg::core::pipeline::TaskMetadata *metadata) const
{
    return (StorerTaskMetadata*)metadata;
}

/**
 * Converts task to the right type.
 *
 * @param task Task to be converted.
 *
 * @return Type safe instance of the task.
 */
bdg::core::pipeline::StorerTask *bdg::core::pipeline::plugin::StorerPlugin::convertTask(const bdg::core::pipeline::Task *task) const
{
    return (StorerTask*)task;
}
