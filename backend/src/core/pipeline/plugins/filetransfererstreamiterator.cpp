/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/filetransfererstreamiterator.h"     // Header
#include "core/pipeline/plugins/filetransferer.h"                   // FileTransferer
#include "utils/config.h"                                           // Config

#include <Poco/Path.h>              // Poco::Path

/**
 * Constructor of the file iterator.
 *
 * @param fileTransferer File transferer.
 * @param workerFileMask Mask of the path to read files.
 * @param workerRank Rank of the worker from which we want to read files.
 * @param fileFrom Id of the first file that should be read.
 * @param filesCount Total number of read files.
 */
bdg::core::pipeline::FileTransfererStreamIterator::FileTransfererStreamIterator(
    const FileTransferer *fileTransferer,
    const std::string &workerFileMask,
    const int workerRank,
    const Poco::UInt64 fileFrom,
    const size_t filesCount
) :
    fileTransferer(fileTransferer),
    workerFileMask(workerFileMask),
    workerRank(workerRank),
    fileFrom(fileFrom),
    filesCount(filesCount),
    currentFile(fileFrom - 1)
{
    // Nothing here.
}

/**
 * Moves the iterator to the next file.
 *
 * @return Has the iterator moved to the next file?
 */
bool bdg::core::pipeline::FileTransfererStreamIterator::moveToNextFile()
{
    if (canMoveToNextFile())
    {
        ++currentFile;
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * Returns the path to the current file that the iterator points to.
 *
 * @return Path to the current file.
 */
std::string bdg::core::pipeline::FileTransfererStreamIterator::getCurrentPath() const
{
    return fileTransferer->expandFileMaskFileId(workerFileMask, currentFile);
}

/**
 * Returns stream for reading the current file that the iterator points to.
 *
 * @return Stream for reading the current file.
 */
std::unique_ptr<bdg::core::pipeline::FileTransfererStream> bdg::core::pipeline::FileTransfererStreamIterator::getCurrentFile() const
{
    return fileTransferer->getFileFromRank(getCurrentPath(), workerRank, currentFile);
}
