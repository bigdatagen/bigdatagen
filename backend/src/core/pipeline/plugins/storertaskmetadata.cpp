/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/storertaskmetadata.h"       // Header

/**
 * Constructor of the task metadata.
 *
 * @param properties Task metadata properties.
 */
bdg::core::pipeline::StorerTaskMetadata::StorerTaskMetadata(const Properties &properties) :
    TransformerTaskMetadata(properties)
{
    // Nothing here.
}

/**
 * Constructor of the task metadata.
 *
 * @param taskRestult Task result from we will create a new task.
 */
bdg::core::pipeline::StorerTaskMetadata::StorerTaskMetadata(const SuccessTaskResult &taskRestult) :
    TransformerTaskMetadata(taskRestult)
{
    // Nothing here.
}
