/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/pluginsettinggroup.h"       // Header

/**
 * Constructor of the setting.
 *
 * @param nameName of the setting.
 * @param caption Caption of the setting.
 * @param groupSettings Settings contained in the group.
 * @param isAdvancedSetting Should the setting be in advanced settings on the frontend?
 */
bdg::core::pipeline::plugin::PluginSettingGroup::PluginSettingGroup(
    const std::string &name,
    const std::string &caption,
    const std::vector<PluginSetting*> &groupSettings,
    const bool isAdvancedSetting
) :
    PluginSetting(name, "", "", "", isAdvancedSetting),
    caption(caption)
{
    if (groupSettings.size() == 0)
    {
        throw Poco::InvalidArgumentException("A group must contain at least one setting");
    }

    for (const auto &setting : groupSettings)
    {
        if (setting->getType() == SettingType::SETTING_TYPE_GROUP)
        {
            throw Poco::InvalidArgumentException("Setting group cannot contain another group");
        }

        if (setting->getType() == SettingType::SETTING_TYPE_ARRAY && !setting->containsScalars())
        {
            throw Poco::InvalidArgumentException("Too much recursion in the settings");
        }

        settings.push_back(std::unique_ptr<PluginSetting>(setting));
    }
}

/**
 * Returns a setting contained in the group which has a given name.
 *
 * @param settingName Name of the searched setting.
 *
 * @return Setting with a given name.
 */
bdg::core::pipeline::plugin::PluginSetting *bdg::core::pipeline::plugin::PluginSettingGroup::getSetting(const std::string &settingName) const
{
    for (const auto &setting : settings)
    {
        if (setting->getName() == settingName)
        {
            return setting.get();
        }
    }

    return nullptr;
}

/**
 * Clones the setting.
 *
 * @return Clone of the setting.
 */
bdg::core::pipeline::plugin::PluginSetting *bdg::core::pipeline::plugin::PluginSettingGroup::clone() const
{
    std::vector<PluginSetting*> groupSettings;

    for (const auto &setting : settings)
    {
        groupSettings.emplace_back(setting->clone());
    }

    return new PluginSettingGroup(getName(), caption, groupSettings, isAdvanced());
}

/**
 * Sets the setting value.
 *
 * @param valueJson Setting value.
 */
void bdg::core::pipeline::plugin::PluginSettingGroup::setValue(const Poco::Dynamic::Var &valueJson)
{
    PluginSetting::setValue(valueJson);
    Poco::JSON::Object::Ptr valueObject;

    try
    {
        valueObject = valueJson.extract<Poco::JSON::Object::Ptr>(); valueJson.extract<Poco::JSON::Object::Ptr>();
    }
    catch (...)
    {
        // We just need to catch exceptions.
    }

    if (!valueObject.isNull())
    {
        for (Poco::JSON::Object::ConstIterator it = valueObject->begin(); it != valueObject->end(); ++it)
        {
            PluginSetting *setting = getSetting(it->first);

            if (setting != nullptr)
            {
                setting->setValue(it->second);
            }
        }
    }
}

/**
 * Returns whether the setting contains only scalar settings (i.e. no groups or arrays).
 *
 * @return Is it a setting which contains only scalar settings?
 */
bool bdg::core::pipeline::plugin::PluginSettingGroup::containsScalars() const
{
    for (const auto &setting : settings)
    {
        if (setting->getType() == SettingType::SETTING_TYPE_ARRAY || setting->getType() == SettingType::SETTING_TYPE_GROUP)
        {
            return false;
        }
    }

    return true;
}

/**
 * Serializes the setting to a JSON.
 *
 * @param json JSON object where the setting should be serialized.
 */
void bdg::core::pipeline::plugin::PluginSettingGroup::serialize(Poco::JSON::Object &json) const
{
    PluginSetting::serialize(json);
    Poco::JSON::Array settingsJson;

    json.remove("description");
    json.remove("value");

    for (const auto &setting : settings)
    {
        Poco::JSON::Object settingJson;
        setting->serialize(settingJson);
        settingsJson.add(settingJson);
    }

    json.set("caption", caption);
    json.set("settings", settingsJson);
}
