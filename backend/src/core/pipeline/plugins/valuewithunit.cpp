/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/valuewithunit.h"            // Header

#include <Poco/String.h>        // Poco::String

const Poco::Int64 bdg::core::pipeline::ValueWithUnit::VALUE_UNDEFINED = -1;

/**
 * Constructor of the value with unit.
 */
bdg::core::pipeline::ValueWithUnit::ValueWithUnit() :
    value(VALUE_UNDEFINED),
    unit(UNIT_UNDEFINED)
{
    // Nothing here.
}

/**
 * Constructor of the value with unit.
 *
 * @param definition String which defines the value with unit.
 */
bdg::core::pipeline::ValueWithUnit::ValueWithUnit(const std::string &definition) :
    ValueWithUnit()
{
    // Get the value from definition string.
    try
    {
        value = std::stoi(definition);
    }
    catch (...)
    {
        // We just need to handle an exception singnaling the wrong number format.
    }

    // Parse the unit.
    if (value >= 0)
    {
        const std::string unitStr = Poco::toLower(Poco::trim(Poco::replace(definition, std::to_string(value).c_str(), "")));

        if (unitStr == "file" || unitStr == "files" || unitStr == "f")
        {
            unit = UNIT_FILE;
        }
        // For size units see: http://www.dr-lex.be/info-stuff/bytecalc.html
        else if (unitStr == "byte" || unitStr == "bytes" || unitStr == "b")
        {
            unit = UNIT_BYTE;
        }
        else if (unitStr == "kilobyte" || unitStr == "kilobytes" || unitStr == "kb")
        {
            unit = UNIT_BYTE;
            multiplyValue(1000, 1);
        }
        else if (unitStr == "kibibyte" || unitStr == "kibibytes" || unitStr == "kib")
        {
            unit = UNIT_BYTE;
            multiplyValue(1024, 1);
        }
        else if (unitStr == "megabyte" || unitStr == "megabytes" || unitStr == "mb")
        {
            unit = UNIT_BYTE;
            multiplyValue(1000, 2);
        }
        else if (unitStr == "mebibyte" || unitStr == "mebibytes" || unitStr == "mib")
        {
            unit = UNIT_BYTE;
            multiplyValue(1024, 2);
        }
        else if (unitStr == "gigabyte" || unitStr == "gigabytes" || unitStr == "gb")
        {
            unit = UNIT_BYTE;
            multiplyValue(1000, 3);
        }
        else if (unitStr == "gibibyte" || unitStr == "gibibytes" || unitStr == "gib")
        {
            unit = UNIT_BYTE;
            multiplyValue(1024, 3);
        }
        else if (unitStr == "terabyte" || unitStr == "terabytes" || unitStr == "tb")
        {
            unit = UNIT_BYTE;
            multiplyValue(1000, 4);
        }
        else if (unitStr == "tebibyte" || unitStr == "tebibytes" || unitStr == "tib")
        {
            unit = UNIT_BYTE;
            multiplyValue(1024, 4);
        }
        else if (unitStr == "petabyte" || unitStr == "petabytes" || unitStr == "pb")
        {
            unit = UNIT_BYTE;
            multiplyValue(1000, 5);
        }
        else if (unitStr == "pebibyte" || unitStr == "pebibytes" || unitStr == "pib")
        {
            unit = UNIT_BYTE;
            multiplyValue(1024, 5);
        }
        else if (unitStr == "second" || unitStr == "seconds" || unitStr == "s" || unitStr == "sec" || unitStr == "secs")
        {
            unit = UNIT_SECOND;
        }
        else if (unitStr == "minute" || unitStr == "minutes" || unitStr == "i" || unitStr == "min" || unitStr == "mins")
        {
            unit = UNIT_SECOND;
            value *= 60;
        }
        else if (unitStr == "hour" || unitStr == "hours" || unitStr == "h")
        {
            unit = UNIT_SECOND;
            value *= 3600;
        }
        else if (unitStr == "day" || unitStr == "days" || unitStr == "d")
        {
            unit = UNIT_SECOND;
            value *= 24 * 3600;
        }
        else if (unitStr == "week" || unitStr == "weeks" || unitStr == "w")
        {
            unit = UNIT_SECOND;
            value *= 7 * 24 * 3600;
        }
        else if (unitStr == "month" || unitStr == "months" || unitStr == "m")
        {
            unit = UNIT_SECOND;
            value *= 28 * 24 * 3600;
        }
        else if (unitStr == "year" || unitStr == "years" || unitStr == "y")
        {
            unit = UNIT_SECOND;
            value *= 365 * 24 * 3600;
        }
        else if (!unitStr.empty())
        {
            unit = UNIT_INVALID;
        }
    }
}

/**
 * Multiplies the value by a base with a given exponent.
 *
 * @param base Base.
 * @param times Exponent.
 */
void bdg::core::pipeline::ValueWithUnit::multiplyValue(const int base, const int times)
{
    for (int i = 0; i < times; ++i)
    {
        value *= base;
    }
}
