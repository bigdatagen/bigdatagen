/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/storertask.h"           // Header

/**
 * Constructor of the task.
 *
 * @param metadata Task metadata.
 * @param fileFrequency Speed which should be used for storing of generated data.
 */
bdg::core::pipeline::StorerTask::StorerTask(const StorerTaskMetadata *metadata) :
    TransformerTask(metadata)
{
    // Nothing here.
}

/**
 * Serializes the task. Serialization is needed when the task is sent from the master to a worker.
 *
 * @param json JSON object where the serialized task is stored.
 */
void bdg::core::pipeline::StorerTask::serializeAdditional(Poco::JSON::Object &json) const
{
    TransformerTask::serializeAdditional(json);
    json.set("frequency.bytes", fileFrequency.getBytes());
    json.set("frequency.files", fileFrequency.getFiles());
    json.set("frequency.seconds", fileFrequency.getSeconds());
}

/**
 * Deserializes the task. Deserialization is performed on a worker, when a message with the task
 * is delivered.
 *
 * @param json JSON object where the serialized task is stored.
 */
void bdg::core::pipeline::StorerTask::unserializeAdditional(const Poco::JSON::Object &json)
{
    TransformerTask::unserializeAdditional(json);
    fileFrequency.setBytes(json.getValue<Poco::Int64>("frequency.bytes"));
    fileFrequency.setFiles(json.getValue<Poco::Int64>("frequency.files"));
    fileFrequency.setSeconds(json.getValue<Poco::Int64>("frequency.seconds"));
}
