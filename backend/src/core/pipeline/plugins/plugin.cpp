/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/plugin.h"       // Header
#include "utils/config.h"                       // Config

#include <Poco/File.h>                          // Poco::File
#include <Poco/Path.h>                          // Poco::Path
#include <Poco/DirectoryIterator.h>             // Poco::DirectoryIterator

const std::string bdg::core::pipeline::plugin::Plugin::OUTPUT_PATH_SETTING = "outputPath";
const double bdg::core::pipeline::plugin::Plugin::MAX_ERROR_FILES_RATIO = 0.1;

namespace bdg
{
    namespace core
    {
        namespace pipeline
        {
            namespace plugin
            {
                /**
                 * Implementation of the pipeline plugin - PIMPL design pattern.
                 */
                struct PluginImpl
                {
                public:
                    PluginImpl(const std::string &name, const std::string &description);
                    ~PluginImpl() = default;

                    /**
                     * MPI wrapper.
                     */
                    bdg::utils::MPIProxy *mpi;

                    /**
                     * Object for reading files.
                     */
                    bdg::core::pipeline::FileTransferer fileTransferer;

                    /**
                     * Application logger.
                     */
                    Poco::Logger *logger;

                    /**
                     * Settings of the plugin.
                     */
                    Plugin::PluginSettings settings;

                    /**
                     * Collection of paths where the files generated in the plugin are stored.
                     */
                    std::vector<std::string> outputPaths;

                    /**
                     * Collection of masks of paths where the files generated in the plugin are stored.
                     */
                    std::vector<std::string> outputPathsMasks;

                    /**
                     * Name of the plugin.
                     */
                    const std::string name;

                    /**
                     * Description of the plugin.
                     */
                    const std::string description;

                    /**
                     * Relative position of the plugin in the pipeline. If more than one instances of
                     * the plugin are used in the pipeline (plugin is present multiple times), each
                     * instance has assigned a number - first instance has number 1, second instance
                     * has number two, etc.
                     */
                    int pluginRelativePosition = 0;

                    /**
                     * How many plugins are in the pipeline.
                     */
                    size_t pluginsCountInPipeline = 0;

                    /**
                     * Clean up policy.
                     */
                    Plugin::CleanUpType cleanUpType = Plugin::CleanUpType::CLEAN_UP_NONE;

                    /**
                     * Can we use globally unique ids for generated files? If not, local file ids are used.
                     */
                    bool canUseGlobalFileIds = true;
                };
            }
        }
    }
}

/**
 * Constructor of the implementation of the pipeline plugin.
 *
 * @param name Name of the plugin.
 * @param description Description of the plugin.
 */
bdg::core::pipeline::plugin::PluginImpl::PluginImpl(const std::string &name, const std::string &description) :
    name(name),
    description(description)
{
    // Nothing here.
}

/**
 * Constructor of the pipeline plugin. No computations should be done in the constructor,
 * only settings shoudl be registered there. This is because in the construcor, MPI, logger
 * and other instance are not present yet. Use init method to initialize the plugin.
 *
 * @param name Name of the plugin.
 * @param description Description of the plugin.
 * @param defaultOutputPath Output path used to store files generated in the plugin.
 * @param outputPathIsAdvancedSetting Is the output path an advanced setting? I.e. should it be shown in the advanced settings on the frontend.
 * @param outputPathIsMultiplied Can the plugin have multiple output paths?
 */
bdg::core::pipeline::plugin::Plugin::Plugin(
    const std::string &name,
    const std::string &description,
    const std::string &defaultOutputPath,
    const bool outputPathIsMultiplied,
    const bool outputPathIsAdvancedSetting
) :
    impl(std::unique_ptr<PluginImpl>(new PluginImpl(name, description)))
{
    if (!defaultOutputPath.empty())
    {
        const std::string help = R"(
            %j - job id which is automatically assigned
            %i - id of generated file
            %d - directory id
            %r - rank which generated the file
            %p - relative position of a plugin in the pipeline, makes sense only in relation with transformer plugin
            %e - replaced by the extension of the file generated in the previous stage of the pipeline, used only in relation with storer plugins
        )";

        if (outputPathIsMultiplied)
        {
            addSetting(new PluginSettingArray(new PluginSettingString(OUTPUT_PATH_SETTING, "Path on the cluster, where you want to store the files generated by this plugin", defaultOutputPath, help, outputPathIsAdvancedSetting)));
        }
        else
        {
            addSetting(new PluginSettingString(OUTPUT_PATH_SETTING, "Path on the cluster, where you want to store the files generated by this plugin", defaultOutputPath, help, outputPathIsAdvancedSetting));
        }
    }
}

/**
 * Destructor of the pipeline plugin.
 */
bdg::core::pipeline::plugin::Plugin::~Plugin()
{
    impl.reset();
}

/**
 * Returns plugin settings.
 *
 * @return Settings of the plugin.
 */
const bdg::core::pipeline::plugin::Plugin::PluginSettings &bdg::core::pipeline::plugin::Plugin::getSettings() const
{
    return impl->settings;
}

/**
 * Returns name of the plugin.
 *
 * @return Name of the plugin.
 */
const std::string &bdg::core::pipeline::plugin::Plugin::getName() const
{
    return impl->name;
}

/**
 * Returns description of the plugin.
 *
 * @return Description of the plugin.
 */
const std::string &bdg::core::pipeline::plugin::Plugin::getDescription() const
{
    return impl->description;
}

/**
 * Returns collection of paths where the files generated in the plugin are stored.
 *
 * @return Collection of paths where the files generated in the plugin are stored.
 */
const std::vector<std::string> &bdg::core::pipeline::plugin::Plugin::getOutputPaths() const
{
    return impl->outputPaths;
}

/**
 * Returns collection of masks of paths where the files generated in the plugin are stored.
 *
 * @return Collection of masks of paths where the files generated in the plugin are stored.
 */
const std::vector<std::string> &bdg::core::pipeline::plugin::Plugin::getOutputPathMasks() const
{
    return impl->outputPathsMasks;
}

/**
 * Method called on the worker. In this method passed task should be processed.
 *
 * @param task Task to be processed.
 *
 * @return Result of task processing.
 */
bdg::core::pipeline::TaskResult *bdg::core::pipeline::plugin::Plugin::processTask(const bdg::core::pipeline::Task *task)
{
    taskStart(task);
    bdg::core::pipeline::TaskResult *result = processTaskImpl(task);
    taskEnd(task, result);

    return result;
}

/**
 * Method intended to be overriden. This method is called on the master just before the task
 * is sent to the worker.
 *
 * @param task Task to be processed.
 */
void bdg::core::pipeline::plugin::Plugin::taskBeforePassToWorker(bdg::core::pipeline::Task *task)
{
    // Nothing here.
}

/**
 * Sets the value of a setting. If no such setting exists, a property with given key is set.
 *
 * @param settingName Name of the setting.
 * @param settingJson JSON representing a setting value.
 */
void bdg::core::pipeline::plugin::Plugin::setSettingValue(const std::string &settingName, const Poco::Dynamic::Var &settingJson)
{
    PluginSetting *setting = getSetting(settingName);

    if (setting != nullptr)
    {
        setting->setValue(settingJson);
    }
    else
    {
        setProperty(settingName, settingJson.toString());
    }
}

/**
 * Initializes the plugin. This method is called in every MPI thread when the plugin is
 * used in the pipeline.
 *
 * @param mpi MPI wrapper.
 * @param logger Application logger.
 * @param fileTransferer Object for reading files.
 * @param pluginRelativePosition Relative position of the plugin in the pipeline.
 * @param pluginsCountInPipeline How many plugins are in the pipeline.
 * @param canUseGlobalFileIds Can we use globally unique ids for generated files?
 */
void bdg::core::pipeline::plugin::Plugin::initialize(
    bdg::utils::MPIProxy &mpi,
    Poco::Logger &logger,
    const FileTransferer &fileTransferer,
    const int pluginRelativePosition,
    const size_t pluginsCountInPipeline,
    const CleanUpType cleanUpType,
    const bool canUseGlobalFileIds)
{
    impl->mpi = &mpi;
    impl->fileTransferer = fileTransferer;
    impl->logger = &logger;
    impl->pluginRelativePosition = pluginRelativePosition;
    impl->pluginsCountInPipeline = pluginsCountInPipeline;
    impl->cleanUpType = cleanUpType;
    impl->canUseGlobalFileIds = canUseGlobalFileIds;

    // Prepare output paths.
    initOutputPaths();

    // User defined initialization.
    init();
}

/**
 * Initializes paths where the plugin should store generated files.
 */
void bdg::core::pipeline::plugin::Plugin::initOutputPaths()
{
    const PluginSetting *outputPathSetting = getSetting(OUTPUT_PATH_SETTING);

    if (outputPathSetting != nullptr)
    {
        impl->outputPaths.clear();
        impl->outputPathsMasks.clear();

        if (outputPathSetting->getType() == PluginSetting::SettingType::SETTING_TYPE_ARRAY)
        {
            const PluginSettingArray::SettingsCollection &settings = outputPathSetting->convert<PluginSettingArray>()->getSettings();

            if (settings.size() > 0)
            {
                for (const auto &setting : settings)
                {
                    const std::string outputPath = getFileTransferer().expandPathMask(setting->getValue(), getPluginRelativePosition(), getMpi().getRank(), useGlobalFileIds());
                    impl->outputPaths.emplace_back(outputPath);
                    impl->outputPathsMasks.emplace_back(setting->getValue());
                }
            }
            else
            {
                const std::string outputPath = getFileTransferer().expandPathMask(outputPathSetting->getDefaultValue(), getPluginRelativePosition(), getMpi().getRank(), useGlobalFileIds());
                impl->outputPaths.emplace_back(outputPath);
                impl->outputPathsMasks.emplace_back(outputPathSetting->getDefaultValue());
            }
        }
        else
        {
            const std::string outputPath = getFileTransferer().expandPathMask(outputPathSetting->getValue(), getPluginRelativePosition(), getMpi().getRank(), useGlobalFileIds());
            impl->outputPaths.emplace_back(outputPath);
            impl->outputPathsMasks.emplace_back(outputPathSetting->getValue());
        }
    }
}

/**
 * Creates directories where the plugin should store generated files.
 */
void bdg::core::pipeline::plugin::Plugin::createOutputPaths() const
{
    // Create output directories.
    if (!getFileTransferer().isSharedFileSystem() || getMpi().isMaster())
    {
        for (auto outputPathMask : getOutputPathMasks())
        {
            Poco::Path outputPath = Poco::Path(getCommonOutputPath(outputPathMask)).parent();

            // Create the directory if it does not exist yet.
            try
            {
                Poco::File directory(outputPath);

                if (!directory.exists())
                {
                    directory.createDirectories();
                }
            }
            catch (Poco::Exception &e)
            {
                // If we cannot create the directory, it should be created when a task is processed.
                // This is meant just as pre-creation of directories, because in the task processing
                // there might occur a problem with concurrency.
                bdg_warning(getLogger(), "Could not create output directory [" + outputPath.toString() + "]");
                bdg_debug(getLogger(), "Exception: " + e.displayText());
                UNREFERENCED_VARIABLE(e);
            }
        }
    }

    // Create local output directory.
    const std::string outputPathLocal = bdg::utils::Config::getInstance().getOutputStoragePathLocal();

    if (!outputPathLocal.empty())
    {
        try
        {
            Poco::File directory(outputPathLocal);

            if (!directory.exists())
            {
                directory.createDirectories();
            }
        }
        catch (Poco::Exception &e)
        {
            // If we cannot create the directory, we might fail with an exception when processing a task.
            bdg_warning(getLogger(), "Could not create local output directory [" + outputPathLocal + "]");
            bdg_debug(getLogger(), "Exception: " + e.displayText());
            UNREFERENCED_VARIABLE(e);
        }

        try
        {
            Poco::File directory(outputPathLocal + Poco::Path::separator() + std::to_string(getMpi().getRank()));

            if (!directory.exists())
            {
                directory.createDirectories();
            }
        }
        catch (Poco::Exception &e)
        {
            // If we cannot create the directory, we might fail with an exception when processing a task.
            bdg_warning(getLogger(), "Could not create local output directory [" + outputPathLocal + Poco::Path::separator() + std::to_string(getMpi().getRank()) + "]");
            bdg_debug(getLogger(), "Exception: " + e.displayText());
            UNREFERENCED_VARIABLE(e);
        }
    }
}

/**
 * Method called in every MPI thread when the job is finished. Note that this method is
 * called only when the "cleanUp" setting of the job is turned on. This method deletes
 * all temporary files generated by the plugins.
 */
void bdg::core::pipeline::plugin::Plugin::cleanUp()
{
    // User defined clean up.
    clean();

    // Remove all the temporary output paths.
    for (auto outputPathMask : getOutputPathMasks())
    {
        const std::string outputPathStr = getCommonOutputPath(outputPathMask);
        Poco::File outputDirectory(outputPathStr);

        // Remove the directory.
        if (outputDirectory.exists())
        {
            try
            {
                outputDirectory.remove(true);
            }
            catch (Poco::Exception &e)
            {
                bdg_warning(getLogger(), "Could not delete output directory [" + outputPathStr + "]");
                bdg_debug(getLogger(), "Exception: " + e.displayText());
                UNREFERENCED_VARIABLE(e);
            }
        }

        if (!getFileTransferer().isSharedFileSystem() || getMpi().isMaster())
        {
            try
            {
                // Try to remove the parent directories - only when they are empty, i.e. we do not
                // use recursive remove. When the directory is not empty, we get an exception and
                // we jump into the catch block.
                Poco::Path outputPath(outputPathStr);

                while (!outputPath.toString().empty())
                {
                    outputPath = outputPath.parent();
                    Poco::File(outputPath).remove();
                }
            }
            catch (Poco::Exception&)
            {
                // Here we just need to catch exceptions to end the while cycle.
            }
        }
    }
}

/**
 * Method called in every MPI thread when the job is finished. This method deleted the
 * tempoprary directory in the local file system when this experimental option was
 * turned on.
 */
void bdg::core::pipeline::plugin::Plugin::terminatePlugin()
{
    terminate();

    // Remove local tmp directory.
    if (getCleanUpType() == CleanUpType::CLEAN_UP_POSTPROCESS || getCleanUpType() == CleanUpType::CLEAN_UP_CONTINUOUS_POSTPROCESS)
    {
        const std::string outputPathLocal = bdg::utils::Config::getInstance().getOutputStoragePathLocal() + Poco::Path::separator() + std::to_string(getMpi().getRank());

        if (!outputPathLocal.empty())
        {
            try
            {
                Poco::File directory(outputPathLocal);

                if (directory.exists())
                {
                    directory.remove(true);
                }
            }
            catch (Poco::Exception &e)
            {
                bdg_warning(getLogger(), "Could not delete local output directory [" + outputPathLocal + "]");
                bdg_debug(getLogger(), "Exception: " + e.displayText());
                UNREFERENCED_VARIABLE(e);
            }
        }
    }
}

/**
 * Returns string reprezentation of the plugin type.
 *
 * @return String reprezentation of the plugin type.
 */
const std::string bdg::core::pipeline::plugin::Plugin::getTypeString() const
{
    switch (getType())
    {
        case PluginType::PLUGIN_TYPE_GENERATOR:
            return "GENERATOR";
            break;

        case PluginType::PLUGIN_TYPE_TRANSFORMER:
            return "TRANSFORMER";
            break;

        case PluginType::PLUGIN_TYPE_STORER:
            return "STORER";
            break;

        default:
            return "UNKNOWN";
            break;
    }
}

/**
 * Returns the first path where files generated in the plugin are stored.
 *
 * @return Path where files generated in the plugin are stored.
 */
const std::string bdg::core::pipeline::plugin::Plugin::getOutputPath() const
{
    if (getOutputPaths().size() > 0)
    {
        return getOutputPaths().front();
    }
    else
    {
        return "";
    }
}

/**
 * Returns the first path mask where files generated in the plugin are stored.
 *
 * @return Path mask where files generated in the plugin are stored.
 */
const std::string bdg::core::pipeline::plugin::Plugin::getOutputPathMask() const
{
    if (getOutputPathMasks().size() > 0)
    {
        return getOutputPathMasks().front();
    }
    else
    {
        return "";
    }
}

/**
 * Serialization of plugins is only for purpose of checkpoints. We do not want to
 * store there properties of every plugin of every worker, because those properties
 * are loaded anyway from the configuration when plugin data structure is restored
 * from the checkpoint. So there is no need for it to occupy the space in checkpoints.
 * This is the reason why override functionality of this method.
 *
 * @param json JSON where the plugin is serialized to.
 */
void bdg::core::pipeline::plugin::Plugin::serialize(Poco::JSON::Object &json) const
{
    serializeAdditional(json);
}

/**
 * Serializes the plugin into JSON.
 *
 * @param json JSON where the plugin is serialized to.
 */
void bdg::core::pipeline::plugin::Plugin::serializeAdditional(Poco::JSON::Object &json) const
{
    PropertiesObject::serializeAdditional(json);

    if (!useGlobalFileIds())
    {
        json.set("file", nextFileIdLocal);
    }
}

/**
 * Deserializes the plugin from JSON.
 *
 * @param json JSON where the plugin is serialized.
 */
void bdg::core::pipeline::plugin::Plugin::unserializeAdditional(const Poco::JSON::Object &json)
{
    PropertiesObject::unserializeAdditional(json);

    if (json.has("file"))
    {
        nextFileIdLocal = json.getValue<Poco::Int64>("file");
    }
}

/**
 * Method called in every MPI thread when the plugin has been initialized. This method is
 * meant to be overriden by concrete pipeline plugins.
 */
void bdg::core::pipeline::plugin::Plugin::init()
{
    // Nothing here.
}

/**
 * Method called in every MPI thread when the job is finished. Note that this method is
 * called only when the "cleanUp" setting of the job is turned on. This method is meant
 * to be overriden by concrete pipeline plugins.
 */
void bdg::core::pipeline::plugin::Plugin::clean()
{
    // Nothing here.
}

/**
 * Method called in every MPI thread when the job is finished. This method is meant
 * to be overriden by concrete pipeline plugins.
 */
void bdg::core::pipeline::plugin::Plugin::terminate()
{
    // Nothing here.
}

/**
 * Method intended to be overriden. This method is called just before a task is processed
 * on the worker.
 *
 * @param task Task to be processed.
 */
void bdg::core::pipeline::plugin::Plugin::taskStart(const bdg::core::pipeline::Task *task)
{
    // Nothing here.
}

/**
 * Method intended to be overriden. This method is called just after a task has been
 * processed on the worker.
 *
 * @param task Processed task.
 * @param result Result of task processing.
 */
void bdg::core::pipeline::plugin::Plugin::taskEnd(const bdg::core::pipeline::Task *task, const bdg::core::pipeline::TaskResult *result)
{
    // Nothing here.
}

/**
 * Creates directories where files with given ids should be stored.
 *
 * @param fileIdFrom Id of the first file that should be stored.
 * @param filesCount Total number of files that should be stored.
 */
bool bdg::core::pipeline::plugin::Plugin::createOutputDirectories(const Poco::UInt64 fileIdFrom, const size_t filesCount) const
{
    const int ATTEMPTS = 5;

    for (int i = 0; i < ATTEMPTS; ++i)
    {
        try
        {
            getFileTransferer().createDirectories(getOutputPaths(), fileIdFrom, filesCount);
            return true;
        }
        catch (Poco::Exception&)
        {
            // Catch exceptions that might occur when creating the output directories.
        }
    }

    return false;
}

/**
 * Converts the name given in a camel case, to the URL like form. E.g. name "JsonGeneratorPlugin"
 * is converted to "json-generator-plugin".
 *
 * @param str String to be converted.
 *
 * @return Converted string.
 */
std::string bdg::core::pipeline::plugin::Plugin::convertToDashes(const std::string &str) const
{
    std::string dashedStr = "";
    
    for (int i = 0; i < str.length(); ++i)
    {
        if (i > 0 && str[i] >= 'A' && str[i] <= 'Z')
        {
            dashedStr += "-";
            dashedStr += str[i];
        }
        else
        {
            dashedStr += str[i];
        }
    }
    
    return Poco::toLower(dashedStr);
}

/**
 * Returns the id that uniquely identies the file. When global file ids are used, each file is identified
 * by its id. When local file ids are used, each file is identified by triplet index of the plugin in the
 * pipeline, rank of the current MPI thread and local id of the file.
 *
 * @param pluginIndex Index of the plugin in the pipeline.
 * @param rank Rank of the current MPI thread.
 * @param fileId Id of the file (local or global).
 *
 * @return
 */
std::string bdg::core::pipeline::plugin::Plugin::getFileId(const int pluginIndex, const int rank, const Poco::UInt64 fileId) const
{
    // Do we use global file ids?
    if (useGlobalFileIds())
    {
        return std::to_string(fileId);
    }
    else
    {
        return std::to_string(pluginIndex) + "-" + std::to_string(rank) + "-" + std::to_string(fileId);
    }
}

/**
 * Returns the setting with a given name. When no such setting exists, nullptr is returned.
 *
 * @param settingName Name of searched setting.
 *
 * @return Setting with a given name or nullptr.
 */
bdg::core::pipeline::plugin::PluginSetting *bdg::core::pipeline::plugin::Plugin::getSetting(const std::string &settingName) const
{
    for (const auto &setting : getSettings())
    {
        if (setting->getName() == settingName)
        {
            return setting.get();
        }
    }

    return nullptr;
}

/**
 * Returns value of the setting with a given name. When no such setting exsits a value of
 * property with given key is returned. When no such property exists, an empty string is returned.
 *
 * @param settingName Name of searched setting or property.
 *
 * @return Value of the setting or property.
 */
std::string bdg::core::pipeline::plugin::Plugin::getSettingValue(const std::string &settingName) const
{
    const PluginSetting *setting = getSetting(settingName);

    if (setting != nullptr)
    {
        return setting->getValue();
    }
    else
    {
        return getProperty(settingName);
    }
}

/**
 * Returns value of the setting with a given name. When no such setting exsits a value of
 * property with given key is returned. When no such property exists, 0 is returned.
 *
 * @param settingName Name of searched setting or property.
 *
 * @return Value of the setting or property.
 */
int bdg::core::pipeline::plugin::Plugin::getSettingValueInt(const std::string &settingName) const
{
    return std::atoi(getSettingValue(settingName).c_str());
}

/**
 * Returns value of the setting with a given name. When no such setting exsits a value of
 * property with given key is returned. When no such property exists, false is returned.
 *
 * @param settingName Name of searched setting or property.
 *
 * @return Value of the setting or property.
 */
bool bdg::core::pipeline::plugin::Plugin::getSettingValueCheckbox(const std::string &settingName) const
{
    return getSettingValue(settingName) == "true" ? true : false;
}

/**
 * Adds a definition of new plugin setting. All plugin settings should be defined in the
 * plugin constructor, so this method should be called there.
 *
 * @param setting Definition of the setting.
 */
void bdg::core::pipeline::plugin::Plugin::addSetting(PluginSetting *setting)
{
    if (setting == nullptr)
    {
        throw Poco::NullPointerException("Added setting cannot be null");
    }

    impl->settings.emplace_back(std::unique_ptr<PluginSetting>(setting));
}

/**
 * Returns the part of the output path where no placeholders are.
 *
 * @param outputPathMask Mask of the output path.
 *
 * @return Part of the output path where no placeholders are.
 */
const std::string bdg::core::pipeline::plugin::Plugin::getCommonOutputPath(const std::string &outputPathMask) const
{
    std::string outputPathMaskAbsolute = outputPathMask;

    if (!Poco::Path(outputPathMaskAbsolute).isAbsolute())
    {
        outputPathMaskAbsolute = bdg::utils::Config::getInstance().getOutputStoragePath() + Poco::Path::separator() + outputPathMaskAbsolute;
    }

    Poco::Path outputPath(outputPathMaskAbsolute);

    // We want to get the path to the directory with '%r' - rank of the worker.
    while (outputPath.parent().toString().find("%i") != std::string::npos
        || outputPath.parent().toString().find("%d") != std::string::npos
        || outputPath.parent().toString().find("%r") != std::string::npos)
    {
        outputPath = outputPath.parent();
    }

    std::string outputPathStr = outputPath.toString();

    outputPathStr = getFileTransferer().expandFileMaskPluginRelativePosition(outputPathStr, getPluginRelativePosition());
    outputPathStr = getFileTransferer().expandFileMaskJobId(outputPathStr);
    outputPathStr = getFileTransferer().expandFileMaskRank(outputPathStr, getMpi().getRank());

    return outputPathStr;
}

/**
 * Returns MPI wrapper.
 *
 * @return MPI wrapper.
 */
const bdg::utils::MPIProxy &bdg::core::pipeline::plugin::Plugin::getMpi() const
{
    return *impl->mpi;
}

/**
 * Returns object for reading files.
 *
 * @return Object for reading files.
 */
const bdg::core::pipeline::FileTransferer &bdg::core::pipeline::plugin::Plugin::getFileTransferer() const
{
    return impl->fileTransferer;
}

/**
 * Returns application logger.
 *
 * @return Application logger.
 */
Poco::Logger &bdg::core::pipeline::plugin::Plugin::getLogger() const
{
    return *impl->logger;
}

/**
 * Returns relative position of the plugin in the pipeline. If more than one instances of
 * the plugin are used in the pipeline (plugin is present multiple times), each
 * instance has assigned a number - first instance has number 1, second instance
 * has number two, etc.
 *
 * @return Relative position of the plugin in the pipeline.
 */
const int bdg::core::pipeline::plugin::Plugin::getPluginRelativePosition() const
{
    return impl->pluginRelativePosition + 1;
}

/**
 * Returns how many plugins are in the pipeline.
 *
 * @return How many plugins are in the pipeline.
 */
const size_t bdg::core::pipeline::plugin::Plugin::getPluginsCountInPipeline() const
{
    return impl->pluginsCountInPipeline;
}

/**
 * Returns the clean up policy.
 *
 * @return Clean up policy.
 */
const bdg::core::pipeline::plugin::Plugin::CleanUpType bdg::core::pipeline::plugin::Plugin::getCleanUpType() const
{
    return impl->cleanUpType;
}

/**
 * Returns whether we can use globally unique ids for generated files? If not, local file
 * ids are used.
 *
 * @return Can we use globally unique ids for generated files? If not, local file ids are used.
 */
const bool bdg::core::pipeline::plugin::Plugin::useGlobalFileIds() const
{
    return impl->canUseGlobalFileIds;
}
