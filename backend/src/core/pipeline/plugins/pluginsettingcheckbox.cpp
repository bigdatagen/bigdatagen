/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/pluginsettingcheckbox.h"        // Header

#include <Poco/String.h>        // Poco::toLower

/**
 * Constructor of the setting.
 *
 * @param name Name of the setting.
 * @param description Description of the setting.
 * @param defaultValue Setting default value.
 * @param help Setting help.
 * @param isAdvancedSetting Should the setting be in advanced settings on the frontend?
 */
bdg::core::pipeline::plugin::PluginSettingCheckbox::PluginSettingCheckbox(
    const std::string &name,
    const std::string &description,
    const bool defaultValue,
    const std::string &help,
    const bool isAdvancedSetting
) :
    PluginSetting(name, description, std::to_string(defaultValue), help, isAdvancedSetting)
{
    // Nothing here.
}

/**
 * Clones the setting.
 *
 * @return Clone of the setting.
 */
bdg::core::pipeline::plugin::PluginSetting *bdg::core::pipeline::plugin::PluginSettingCheckbox::clone() const
{
    return new PluginSettingCheckbox(getName(), getDescription(), getDefaultValueBool(), getHelp(), isAdvanced());
}

/**
 * Validates the setting value to one of the strings "false" or "true".
 *
 * @param value Setting value.
 *
 * @return Validated setting value.
 */
std::string bdg::core::pipeline::plugin::PluginSettingCheckbox::validate(const std::string &value) const
{
    return Poco::toLower(value) == "true" || value == "1" ? "true" : "false";
}

/**
 * Serializes the setting to a JSON.
 *
 * @param json JSON object where the setting should be serialized.
 */
void bdg::core::pipeline::plugin::PluginSettingCheckbox::serialize(Poco::JSON::Object &json) const
{
    PluginSetting::serialize(json);
    json.remove("value");
    json.set("value", getDefaultValueBool());
}
