/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/errortaskresult.h"      // Header

/**
 * Constructor of the error result.
 *
 * @param msg Message which should be hold in the result and delivered to the master.
 */
bdg::core::pipeline::ErrorTaskResult::ErrorTaskResult(const std::string &msg) :
    TaskResult(),
    msg(msg)
{
    // Nothing here.
}

/**
 * Constructor of the error result.
 *
 * @param ex Thrown exception.
 */
bdg::core::pipeline::ErrorTaskResult::ErrorTaskResult(const std::exception &ex) :
    TaskResult(),
    msg(ex.what())
{
    // Nothing here.
}

/**
 * Constructor of the error result.
 *
 * @param ex Thrown exception.
 */
bdg::core::pipeline::ErrorTaskResult::ErrorTaskResult(const Poco::Exception &ex) :
    TaskResult(),
    msg(ex.displayText())
{
    // Nothing here.
}

/**
 * Serializes values of the error result. Serialization is needed when the error result is
 * sent from a worker to the master.
 *
 * @param json JSON object where the serialized error result is stored.
 */
void bdg::core::pipeline::ErrorTaskResult::serializeAdditional(Poco::JSON::Object &json) const
{
    TaskResult::serializeAdditional(json);
    json.set("msg", msg);
}

/**
 * Deserializes values of the error result. Deserialization is performed on the master,
 * when a message with error result is delivered.
 *
 * @param json JSON object where the serialized error result is stored.
 */
void bdg::core::pipeline::ErrorTaskResult::unserializeAdditional(const Poco::JSON::Object &json)
{
    TaskResult::unserializeAdditional(json);
    msg = json.getValue<std::string>("msg");
}
