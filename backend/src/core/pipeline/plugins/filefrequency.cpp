/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/filefrequency.h"        // Header

const Poco::Int64 bdg::core::pipeline::FileFrequency::UNDEFINED = -1;

/**
 * Constructor of the file frequency.
 */
bdg::core::pipeline::FileFrequency::FileFrequency() :
    bytes(UNDEFINED),
    files(UNDEFINED),
    seconds(UNDEFINED)
{
    // Nothing here.
}

/**
 * Constructor of the file frequency.
 *
 * @param bytes How many bytes should be stored in each second?
 * @param files How many files should be stored in each second?
 * @param seconds Determines how many seconds are used in the frequency definition.
 */
bdg::core::pipeline::FileFrequency::FileFrequency(const Poco::Int64 bytes, const Poco::Int64 files, const Poco::Int64 seconds) :
    bytes(bytes),
    files(files),
    seconds(seconds)
{
    // Nothing here.
}
