/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/speedlimiter.h"         // Header
#include "core/pipeline/messagetype.h"                  // MessageType

#include <Poco/Exception.h>         // Poco::InvalidArgumentException

const size_t bdg::core::pipeline::SpeedLimiter::BYTES_STORED_MESSAGE_PERIOD = 1000000;

/**
 * Constructor of the speed limiter.
 *
 * @param fileFrequency Spped of the file storing.
 * @param targetFilesCount How many files should be stored in the job.
 */
bdg::core::pipeline::SpeedLimiter::SpeedLimiter(const FileFrequency &fileFrequency, const int targetFilesCount)
{
    // Store speed is undefined - we use the best effort principle (highest possible speed).
    if (fileFrequency.isUndefined())
    {
        filesPerBatch = targetFilesCount;
        bytesPerBatch = 0;
        timePerBatch = 0;
    }
    // Count how much bytes can we send per second.
    else if (fileFrequency.getBytes() > 0)
    {
        filesPerBatch = 0;
        bytesPerBatch = fileFrequency.getBytes() / fileFrequency.getSeconds() + 1;
        timePerBatch = 1000000;
    }
    // Count how much time in microseconds we need to send a batch of files.
    else
    {
        filesPerBatch = 1;
        bytesPerBatch = 0;
        timePerBatch = (fileFrequency.getSeconds() * 1000000) / fileFrequency.getFiles();

        // We are over the maximum speed.
        if (timePerBatch == 0)
        {
            throw Poco::InvalidArgumentException("Store frequency is under the resolution of system timer - you can store maximally 1 000 000 files per second");
        }

        // Minimal time resolution for a batch of files is 0.1 s.
        if (timePerBatch < 100000)
        {
            filesPerBatch = fileFrequency.getFiles() / (fileFrequency.getSeconds() * 10);
            timePerBatch *= filesPerBatch;
        }
    }
}

/**
 * Called when one file has processed in the storer plugin. This method periodically sends
 * a message to the master informing about how many files has processed and what their size is.
 *
 * @param mpi MPI wrapper.
 * @param bytes Size of the processed file.
 * @param moreFilesToProcess Are there some more files to be processed?
 */
void bdg::core::pipeline::SpeedLimiter::fileProcessed(const bdg::utils::MPIProxy &mpi, const Poco::UInt64 bytes, const bool moreFilesToProcess)
{
    ++storedFilesInBatch;
    ++newlyStoredFiles;

    storedBytesInBatch += bytes;
    newlyStoredBytes += bytes;

    // We send the message back to master about new stored files/bytes.
    if (currentBytesPeriodTime.elapsed() > BYTES_STORED_MESSAGE_PERIOD || !moreFilesToProcess)
    {
        mpi.sendMessageToMaster(std::to_string(newlyStoredFiles) + ";" + std::to_string(newlyStoredBytes), MessageType::MESSAGE_TYPE_BYTES_STORED);
        currentBytesPeriodTime.update();

        newlyStoredFiles = 0;
        newlyStoredBytes = 0;
    }
}

/**
 * Should a new batch of files be sent? We just wait untill we can send a next batch of files.
 * We can wait actively, because this thread does not have anything better to do anyway.
 */
void bdg::core::pipeline::SpeedLimiter::waitForNextBatch()
{
    if (currentBatchTime.elapsed() >= timePerBatch)
    {
        currentBatchTime.update();
        storedFilesInBatch = 0;
        storedBytesInBatch -= bytesPerBatch;
    }
}

/**
 * Waits till the end of current period.  We can wait actively, because this thread does not
 * have anything better to do anyway.
 */
void bdg::core::pipeline::SpeedLimiter::waitTillPeriodEnd() const
{
    while (currentBatchTime.elapsed() < timePerBatch) { }
}
