/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/successtaskresult.h"        // Header

/**
 * Constructor of the task result.
 *
 * @param fileMask
 * @param fileFrom
 * @param filesCount
 * @param bytes
 * @param totalTime
 */
bdg::core::pipeline::SuccessTaskResult::SuccessTaskResult(const std::string &fileMask, const Poco::UInt64 fileFrom, const size_t filesCount, const Poco::UInt64 bytes, const Poco::UInt64 totalTime) :
    TaskResult(),
    fileMask(fileMask),
    bytes(bytes),
    totalTime(totalTime)
{
    setFileFrom(fileFrom);
    setFilesCount(filesCount);
}

/**
 * Serializes values of the error result. Serialization is needed when the error result is
 * sent from a worker to the master.
 *
 * @param json JSON object where the serialized error result is stored.
 */
void bdg::core::pipeline::SuccessTaskResult::serializeAdditional(Poco::JSON::Object &json) const
{
    TaskResult::serializeAdditional(json);
    json.set("fileMask", fileMask);
    json.set("bytes", bytes);
    json.set("totalTime", totalTime);
    json.set("pluginState", pluginState);
}

/**
 * Deserializes values of the error result. Deserialization is performed on the master,
 * when a message with error result is delivered.
 *
 * @param json JSON object where the serialized error result is stored.
 */
void bdg::core::pipeline::SuccessTaskResult::unserializeAdditional(const Poco::JSON::Object &json)
{
    TaskResult::unserializeAdditional(json);
    fileMask = json.getValue<std::string>("fileMask");
    bytes = json.getValue<Poco::UInt64>("bytes");
    totalTime = json.getValue<Poco::UInt64>("totalTime");
    pluginState = *json.getObject("pluginState");
}
