/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file jsongeneratorschemaparser.cpp
* @category Generators
* @author   Big Data Generator Team
*/

#include "core/pipeline/plugins/generators/jsonschemaparser/jsongeneratorschemaparser.h" // header
#include "core/pipeline/plugins/generators/jsontypefactory.h"

/**
* Ctor set's factories for all supported types.
*/
bdg::core::JsonGeneratorSchemaParser::JsonGeneratorSchemaParser(Generator& gen) :
    JsonSchemaParser(
        gen, 
        JsonTypeFactory::createUnknown,
        std::vector<TypeFactory::name_factory_fnc_t>{
            { Constants::ITEM_TYPE_INT, JsonTypeFactory::createSimpleType<int> },
            { Constants::ITEM_TYPE_FLOAT, JsonTypeFactory::createSimpleType<double> },
            { Constants::ITEM_TYPE_STRING, JsonTypeFactory::createSimpleType<std::string> },
            { Constants::ITEM_TYPE_DATETIME, JsonTypeFactory::createDateTime },
            { Constants::ITEM_TYPE_ARRAY, JsonTypeFactory::createArray },
            { Constants::ITEM_TYPE_OBJECT, JsonTypeFactory::createObject },
            { Constants::ITEM_TYPE_SEQUENCE, JsonTypeFactory::createSequence },
			{ Constants::ITEM_TYPE_PROBSET, JsonTypeFactory::createProbSet }
        })
{
}

