/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file jsongenerator.cpp
* @category Generator
* @author   Big Data Generator Team
*
*/

#include "core/pipeline/plugins/generators/jsongenerator/jsongenerator.h"       // Header
#include "utils/config.h"                                                       // Config

/**
 * Constructor of the plugin - registers all its settings.
 */
bdg::core::pipeline::plugin::JsonGenerator::JsonGenerator() :
    Generator("JsonGenerator", "Plugin which generates JSON files according to a given schema", "json")
{
    // Nothing here.
}

/**
 * Constructor of the post-processing plugin - registers all its settings.
 */
bdg::core::pipeline::plugin::JsonGeneratorPostprocessor::JsonGeneratorPostprocessor() :
    TransformerPlugin("JsonGeneratorPostprocessor", "Plugin which postprocesses generated JSON files (references, big files) - this plugin is automatically called only on one rank", "json")
{
    // Nothing here.
}

/**
 * Processes one generated file.
 *
 * @param task Processed task.
 * @param inputFileStream Stream with content of file which should be processed.
 * @param inputFilePath Path to the file which should be processed.
 * @param outputFilePath Path where the result of the transformation should be stored.
 * @param newlyCreatedFilesCount Output parameter for the number of files created in this method.
 */
void bdg::core::pipeline::plugin::JsonGeneratorPostprocessor::processOneFile(
    const bdg::core::pipeline::TransformerTask *task,
    std::unique_ptr<FileTransfererStream> inputFileStream,
    const Poco::Path &inputFilePath,
    const Poco::Path &outputFilePath,
    size_t &newlyCreatedFilesCount)
{
    // Copy the file to its destination on the rank, where the postprocessor plugin runs.
    std::ofstream outputStream(outputFilePath.toString(), std::ios::binary);
    Poco::StreamCopier::copyStream(inputFileStream->getStream(), outputStream);
    outputStream.close();

    bdg_debug(getLogger(), "Post-Generated JSON file [" + outputFilePath.toString() + "]; "
        + "ID from: " + getFileId(task->getPluginIndex() - 1, task->getResultWorkerRank(), inputFileStream->getFileId()) + ", "
        + "ID: " + getFileId(task->getPluginIndex(), getMpi().getRank(), nextFileIdLocal) + ", "
        + "Size: " + std::to_string(Poco::File(outputFilePath).getSize()) + " bytes");
}

using bdg::core::pipeline::plugin::Plugin;
using bdg::core::pipeline::plugin::JsonGenerator;
using bdg::core::pipeline::plugin::JsonGeneratorPostprocessor;

/**
 * Export the plugin for the POCO class loader.
 */
POCO_BEGIN_MANIFEST(Plugin)
    POCO_EXPORT_CLASS(JsonGenerator)

    // Note: when we do not export JsonGeneratorPostprocessor plugin, it is ment for
    // internal use only. This way, it cannot be added to any place in the pipeline
    // (because it is not known to the plugin manager) thus it is forced to be in the
    // pipeline after JsonGenerator plugin where it is automatically added, because
    // of the JsonGenerator::getPostprocessor() method. If we exported this plugin too
    // it would be possible for end user to place it also anywhere in the pipeline.
    //POCO_EXPORT_CLASS(JsonGeneratorPostprocessor)
POCO_END_MANIFEST
