/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Generator
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/generators/jsongenerator/jsongen.h"      // Header
#include "core/pipeline/plugins/generators/intgenerator.h"   // intgenerator

#include <algorithm> // transform
#include <numeric> // accumulate
#include <iterator> // back_inserter
#include <vector>  // vector

void bdg::core::JsonGen::setMaster()
{
    isMaster = true;
}

void bdg::core::JsonGen::openStorage(const std::string& fileName)
{
    assert(!isMaster);

    genStorer.open(fileName);
}

void bdg::core::JsonGen::closeStorage()
{
    assert(!isMaster);

    genStorer.close();
}

void bdg::core::JsonGen::generate()
{
    assert(!isMaster);

    for (auto& gFn : preGenFncs)
    {
        gFn(genStorer);
    }

	for (auto &genFn : genFncs)
	{
		genFn(genStorer);
	}
}

void bdg::core::JsonGen::setFilesCount(std::size_t fcount)
{
    assert(isMaster);

    genMult = fcount;
}

const std::vector<std::vector<int>>& bdg::core::JsonGen::genRemInts()
{
    assert(isMaster);

    std::transform(remIntGens.cbegin(), remIntGens.cend(), std::begin(remIntVals), [this](IntGenerator* ig)
    {
        std::vector<int> res(genMult);
        std::generate(std::begin(res), std::end(res), [ig]()
        {
            return ig->generateValue();
        });
        
        return std::move(res);
    });

    return remIntVals;
}

void bdg::core::JsonGen::regRemInts(const std::vector<std::vector<int>>& remInts)
{
    assert(!isMaster);
    assert(remInts.size() == remIntGens.size());

    for (size_t i = 0; i < remInts.size(); i++)
    {
        remIntGens[i]->regData(remInts[i]);
    }
}

const std::vector<std::size_t>& bdg::core::JsonGen::genSeqsStart()
{
    assert(isMaster);

    std::swap(lastSeqsIdx, seqsIdx);

    // Add to each sequence index last value of generator.
    std::transform(
        globSeqs.cbegin(), 
        globSeqs.cend(),
        std::begin(lastSeqsIdx), 
        std::begin(seqsIdx), 
        [this](const GrammarElement::GlobSeq& gs, std::size_t sIdx)
        {
            // Start with last idx + number of multiplication * simple count.
            int res = sIdx + genMult * gs.simpleCount;
            // Generated genMult generation of random numbers for each generator.
            for (size_t i = 0; i < genMult; i++)
            {
                res += std::accumulate(gs.depGens.cbegin(), gs.depGens.cend(), 1, [this, i](int mul, std::size_t genIdx)
                {
                    // Go through all dependant generators on current generator.
                    do
                    {
                        mul *= remIntVals[genIdx][i];
                        genIdx = remGensGen[genIdx];
                    } while (genIdx != remGensGen[genIdx]);

                    return mul;
                });
            }

            return res;
        });

    return lastSeqsIdx;
}

void bdg::core::JsonGen::setSeqsStart(const std::vector<std::size_t>& startIdcs)
{
    assert(!isMaster);

    for (size_t i = 0; i < globSeqs.size(); i++)
    {
        globSeqs[i].seq->setStart(startIdcs[i]);
    }
}
