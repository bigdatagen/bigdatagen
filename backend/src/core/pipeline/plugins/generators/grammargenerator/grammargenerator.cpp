/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Generator
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/generators/grammargenerator/grammargenerator.h"            // Header

/**
 * Constructor of the plugin - registers all its settings.
 */
bdg::core::pipeline::plugin::GrammarGenerator::GrammarGenerator() :
    Generator("GrammarGenerator", "Plugin which generates files according to a given grammar", "gr")
{
    // Nothing here.
}

using bdg::core::pipeline::plugin::Plugin;
using bdg::core::pipeline::plugin::GrammarGenerator;

/**
 * Export the plugin for the POCO class loader.
 */
POCO_BEGIN_MANIFEST(Plugin)
    POCO_EXPORT_CLASS(GrammarGenerator)
POCO_END_MANIFEST
