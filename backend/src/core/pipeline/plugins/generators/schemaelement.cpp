/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file schemaelement.cpp
* @category Generator
* @author   Big Data Generator Team
*
*/
	
#include "core/pipeline/plugins/generators/schemaelement.h" // header

// Abstract class