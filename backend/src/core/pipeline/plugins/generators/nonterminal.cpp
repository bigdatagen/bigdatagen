/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file nonterminal.cpp
* @category Generator
* @author   Big Data Generator Team
*
*/

#include "core/pipeline/plugins/generators/nonterminal.h" // header

#include <cassert>

bdg::core::Nonterminal::Nonterminal(const std::vector<bdg::core::GrammarElement::ptr_t>& elems):
    elements(elems)
{
}

bdg::core::Nonterminal::Nonterminal(std::vector<GrammarElement::ptr_t>&& elems):
    elements(std::move(elems))
{
}

bdg::core::Nonterminal::Nonterminal(std::initializer_list<GrammarElement::ptr_t> l):
    elements(l)
{
}

bdg::core::Nonterminal::Nonterminal(GrammarElement::ptr_t&& elem):
    elements{std::move(elem)}
{
}

/**
* Store element.
*/
void bdg::core::Nonterminal::push_back(const std::shared_ptr<GrammarElement>& element)
{
	elements.push_back(element);
}

void bdg::core::Nonterminal::push_back(std::shared_ptr<GrammarElement>&& element)
{
    elements.push_back(std::move(element));
}

/**
* Move vector of grammar elements.
*/
void bdg::core::Nonterminal::move(std::vector<GrammarElement::ptr_t>&& elems)
{
    assert(elements.empty());
    elements = std::move(elems);
}

/**
* Calls register function on each of grammar elements.
*/
void bdg::core::Nonterminal::registerGeneratorImpl(
    std::size_t& currIdx,
    std::vector<bdg::core::GrammarElement::gen_fn_t>& preGenFncs,
    std::vector<bdg::core::GrammarElement::gen_fn_t>& genFncs)
{
	for (auto &element : elements)
	{
		element->registerGenerator(currIdx, preGenFncs, genFncs);
	}
}

/**
* Calls repair function on each of grammar elemnts.
*/
void bdg::core::Nonterminal::repairImpl(
    const bdg::core::GrammarElement::element_map_t& parsedEls,
    const bdg::core::GrammarElement::seq_vector_t& locSeqs,
    bdg::core::GrammarElement::intGenTypeFnc_vec_t& locGens,
    GrammarElement::intGenTypeFnc_vec_t& remGens,
    std::vector<std::vector<GrammarElement::genIdxTypeFnc_t>>& remGensGens,
    std::vector<GrammarElement::GlobSeq>& globSeqs,
    std::vector<const GrammarElement*>& pathEls)
{
    for (auto& el : elements)
    {
        el->repair(parsedEls, locSeqs, locGens, remGens, remGensGens, globSeqs, pathEls);
    }
}

/**
* Returns i-th element.
*/
const std::shared_ptr<bdg::core::GrammarElement>& bdg::core::Nonterminal::get(size_t index) const
{
	return elements.at(index);
}

/**
* Returns i-th element.
*/
std::shared_ptr<bdg::core::GrammarElement>& bdg::core::Nonterminal::get(size_t index)
{
	return elements.at(index);
}

/**
* Returns number of elements.
*/
size_t bdg::core::Nonterminal::size() const
{
	return elements.size();
}