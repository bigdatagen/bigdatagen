/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file jsonschemaparser.cpp
* @category Generators
* @author   Big Data Generator Team
*/

#include "core/pipeline/plugins/generators/csvschemaparser/csvgeneratorschemaparser.h" // header
//#include "core/pipeline/plugins/generators/jsontypefactory.h"
#include "core/pipeline/plugins/generators/csvtypefactory.h"

bdg::core::CsvGeneratorSchemaParser::CsvGeneratorSchemaParser(Generator& gen) :
    CsvSchemaParser(
        gen, 
        CsvTypeFactory::createUnknown,
        std::vector<TypeFactory::name_factory_fnc_t>{
			{ Constants::ITEM_TYPE_ROOT, CsvTypeFactory::createRoot },
            { Constants::ITEM_TYPE_INT, CsvTypeFactory::createSimpleType<int> },
            { Constants::ITEM_TYPE_FLOAT, CsvTypeFactory::createSimpleType<double> },
            { Constants::ITEM_TYPE_STRING, CsvTypeFactory::createSimpleType<std::string> },
            { Constants::ITEM_TYPE_DATETIME, CsvTypeFactory::createDateTime },
            { Constants::ITEM_TYPE_SEQUENCE, CsvTypeFactory::createSequence },
			{ Constants::ITEM_TYPE_PROBSET, CsvTypeFactory::createProbSet }
        })
{
}

