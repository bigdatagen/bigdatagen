/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file jsonschemaparser.cpp
* @category Generators
* @author   Big Data Generator Team
*/

#include "core/pipeline/plugins/generators/csvschemaparser/csvschemaparser.h" // header
#include "core/pipeline/plugins/generators/exception.h" // Exception
#include "core/pipeline/plugins/generators/csvtypefactory.h" // JsonTypeFactory
#include "core/pipeline/plugins/generators/jsonschemaelement.h" // JsonSchemaElement
#include "core/pipeline/plugins/generators/jsonutils.h" // JsonUtils
#include "core/pipeline/plugins/generators/constants.h" // Constants

#include <Poco/JSON/Parser.h> // Parser
#include <Poco/Dynamic/Var.h> // Var

#include <memory> // unique_ptr
#include <algorithm> // for_each
#include <cassert> // assert

using std::string;

bdg::core::CsvSchemaParser::CsvSchemaParser(Generator& gen,
    const TypeFactory::factory_fnc_t& createUnknown,
    std::vector<TypeFactory::name_factory_fnc_t>&& factoryMethods) :
    SchemaParser(gen, TypeFactory::instance<CsvTypeFactory>(
        createUnknown,
        std::move(factoryMethods)))
{
}

void bdg::core::CsvSchemaParser::parseSchema(const std::string &schema)
{
    using namespace Poco;
    using namespace Poco::JSON;

    try
    {
        parsedSchema = Poco::JSON::Parser().parse(schema);
    }
    catch (std::exception &ex)
    {
        throw Exception(Error::INVALID_JSON + ex.what());
    }

    rootArray = JsonUtils::getArray(parsedSchema);
    nextIdx = 0;
}

bool bdg::core::CsvSchemaParser::hasNextUdt() const
{
    return nextIdx < rootArray->size();
}

bdg::core::SchemaElement::uptr_t bdg::core::CsvSchemaParser::getNextUdt()
{
    auto vobj = rootArray->getObject(nextIdx++);
    return SchemaElement::uptr_t(new JsonSchemaElement(vobj));
}



