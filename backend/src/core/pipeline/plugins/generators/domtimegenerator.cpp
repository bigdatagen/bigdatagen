/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file domtimegenerator.cpp
* @category Generator
* @author   Big Data Generator Team
*
*/
	
#include "core/pipeline/plugins/generators/domtimegenerator.h" // header
#include "utils/convert.h"

#include <iomanip> // put_time
#include <chrono> // time...
#include <sstream> // stringstream
#include <cassert> // assert
#include <cstdlib> // strtoul
#include <algorithm> // transorm
#include <iterator> // back_inserter.

bdg::core::DomTimeGenerator::DomTimeGenerator(const std::string& format, std::vector<date_time_t>&& vals):
    DomainGenerator(transformDt(format, std::move(vals)))
{
}

/**
* Function transorms all given datetime values by given format to strings. They are domain, so we can do it.
*/
std::vector<std::string> bdg::core::DomTimeGenerator::transformDt(const std::string& format, std::vector<date_time_t>&& vals)
{
    if (vals.empty())
        throw Exception(Error::EMPTY_DOM_VALS);

    static const std::string f = "%Y-%m-%d,%H:%M:%S";

    std::vector<std::string> res;
    res.reserve(vals.size());
    std::transform(std::begin(vals), std::end(vals), std::back_inserter(res), [&format](const date_time_t& dt)
    {
        std::string date;
        std::string time;
        std::tie(date, time) = dt;

        try
        {
            std::stringstream ss;
            ss << date + ',' + time;
            std::tm tt = {};
            ss >> std::get_time(&tt, f.c_str());

            std::ostringstream oss;
            oss << std::put_time(&tt, format.c_str());
            return oss.str();
        }
        catch (...)
        {
            throw Exception(Error::BAD_DTIME + "in dateTime domain generator.");
        }

        return std::string();
    });

    return res;
}
