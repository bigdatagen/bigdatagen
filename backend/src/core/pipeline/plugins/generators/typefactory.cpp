/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file typefactory.cpp
* @category Generator
* @author   Big Data Generator Team
*
*/
	
#include "core/pipeline/plugins/generators/typefactory.h" // header

#include <algorithm> // find_if

const std::string bdg::core::TypeFactory::NO_SUPP_TYPES = "No supported types.";
const std::string bdg::core::TypeFactory::TYPE_NOT_EXIST = "Type does not exist.";

/**
* Function returns factory method for given type.
*/
const bdg::core::TypeFactory::factory_fnc_t& bdg::core::TypeFactory::create(const std::string& type) const
{
    const auto foundIt = std::find_if(supTypes.cbegin(), supTypes.cend(), [&type](const name_factory_fnc_t& nf)
    {
        return type == nf.first;
    });

    if (foundIt == supTypes.cend())
    {
        return notFoundFnc;
    }
    else
    {
        return foundIt->second;
    }
}

bdg::core::TypeFactory::TypeFactory(const factory_fnc_t& notFoundFnc, std::vector<name_factory_fnc_t>&& init):
    notFoundFnc(notFoundFnc),
    supTypes(init)
{
}
