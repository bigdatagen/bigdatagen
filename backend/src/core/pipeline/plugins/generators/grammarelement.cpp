/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file grammarelement.cpp
* @category Generator
* @author   Big Data Generator Team
*
*/
	
#include "core/pipeline/plugins/generators/grammarelement.h" // header

/**
* Function registers generator in current grammar element.
*/
void bdg::core::GrammarElement::registerGenerator(
    std::size_t& currIdx,
    std::vector<bdg::core::GrammarElement::gen_fn_t>& preGenFncs,
    std::vector<bdg::core::GrammarElement::gen_fn_t>& genFncs)
{
    registerGeneratorImpl(currIdx, preGenFncs, genFncs);
}

/**
* Function is called on grammar element after parsing but before
* registering. Is for grammar elements to repair itself.
*/
void bdg::core::GrammarElement::repair(
    const bdg::core::GrammarElement::element_map_t& parsedEls,
    const bdg::core::GrammarElement::seq_vector_t& seqs,
    bdg::core::GrammarElement::intGenTypeFnc_vec_t& locGens,
    GrammarElement::intGenTypeFnc_vec_t& remGens,
    std::vector<std::vector<GrammarElement::genIdxTypeFnc_t>>& remGensGens,
    std::vector<GrammarElement::GlobSeq>& globSeqs,
    std::vector<const GrammarElement*>& pathEls)
{
    // Store this grammar element.
    pathEls.push_back(this);
    repairImpl(parsedEls, seqs, locGens, remGens, remGensGens, globSeqs, pathEls);
    // Remove this grammar element.
    pathEls.pop_back();
}

/**
* Default implementation does nothing.
*/
void bdg::core::GrammarElement::repairImpl(
    const bdg::core::GrammarElement::element_map_t& parsedEls,
    const bdg::core::GrammarElement::seq_vector_t& seqs,
    bdg::core::GrammarElement::intGenTypeFnc_vec_t& locGens,
    GrammarElement::intGenTypeFnc_vec_t& remGens,
    std::vector<std::vector<GrammarElement::genIdxTypeFnc_t>>& remGensGens,
    std::vector<GrammarElement::GlobSeq>& globSeqs,
    std::vector<const GrammarElement*>& pathEls)
{
    // Do nothing by default.
}

bdg::core::GrammarElement::CountTransFtor::CountTransFtor():
    sarg1Used{ false },
    sarg1{ 0 },
    sarg2Used{ false },
    sarg2{ 0 },
    iarg1Used{ false },
    iarg1{ 0 },
    iarg2Used{ false },
    iarg2{ 0 }
{
}

bdg::core::GrammarElement::CountTransFtor::CountTransFtor(const CountTransFtor& rhs)
{
    this->operator=(rhs);
}

bdg::core::GrammarElement::CountTransFtor::CountTransFtor(
    const CountTransFtor::fnc_t& fnc,
    size_t sa1, 
    size_t sa2):
f{ fnc },
sarg1Used{true},
sarg1{sa1},
sarg2Used{true},
sarg2{sa2},
iarg1Used{false},
iarg1{0},
iarg2Used{false},
iarg2{0}
{
}

bdg::core::GrammarElement::CountTransFtor::CountTransFtor(
    const CountTransFtor::fnc_t& fnc, 
    int ia1):
f{ fnc },
sarg1Used{ false },
sarg1{ 0 },
sarg2Used{ false },
sarg2{ 0 },
iarg1Used{ true },
iarg1{ ia1 },
iarg2Used{ false },
iarg2{ 0 }
{
}

bdg::core::GrammarElement::CountTransFtor::CountTransFtor(
    const CountTransFtor::fnc_t& fnc, 
    int ia1, 
    int ia2):
f{fnc},
sarg1Used{ false },
sarg1{ 0 },
sarg2Used{ false },
sarg2{ 0 },
iarg1Used{ true },
iarg1{ ia1 },
iarg2Used{ true },
iarg2{ ia2 }
{
}

bdg::core::GrammarElement::CountTransFtor& bdg::core::GrammarElement::CountTransFtor::operator=(const CountTransFtor& rhs)
{
    f = rhs.f;
    sarg1Used = rhs.sarg1Used;
    sarg1 = rhs.sarg1;
    sarg2Used = rhs.sarg2Used;
    sarg2 = rhs.sarg2;
    iarg1Used = rhs.iarg1Used;
    iarg1 = rhs.iarg1;
    iarg2Used = rhs.iarg2Used;
    iarg2 = rhs.iarg2;
    
    return *this;
}

int bdg::core::GrammarElement::CountTransFtor::operator()(int genValue)
{
    return f(*this, genValue);
}

bool bdg::core::GrammarElement::CountTransFtor::operator==(const CountTransFtor& rhs) const
{
    return
        sarg1Used == rhs.sarg1Used &&
        sarg1 == rhs.sarg1 &&
        sarg2Used == rhs.sarg2Used &&
        sarg2 == rhs.sarg2 &&
        iarg1Used == rhs.iarg1Used &&
        iarg1 == rhs.iarg1 &&
        iarg2Used == rhs.iarg2Used &&
        iarg2 == rhs.iarg2;
}
