/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file sequencegenerator.cpp
* @category Generator
* @author   Big Data Generator Team
*
*/

#include "core/pipeline/plugins/generators/sequencegenerator.h" // header

#include <algorithm> // count_if


void bdg::core::DynSeqGenerator::procSeq(
    const bdg::core::GrammarElement::element_map_t& parsedEls,
    const bdg::core::GrammarElement::seq_vector_t& seqs)
{
    // Stores reference name, history size and sequences that uses reference.
    using seqRec_t = std::tuple<std::string, int, std::vector<DynSeqGenerator*>>;

    // Maximal histories for each reference.
    std::vector<seqRec_t> maxHistory;
    for (auto* seq : seqs)
    {
        for (const auto& r : seq->getRefs())
        {
            auto seqIt = std::find_if(std::begin(maxHistory), std::end(maxHistory), [r](const seqRec_t& sr)
            {
                return std::get<0>(sr) == r.first;
            });

            // Find parsed element.
            const auto elIt = parsedEls.find(r.first);
            // Not found.
            if (elIt == std::cend(parsedEls))
                throw Exception(Error::UNKNOWN_REFERENCE + r.first);

            // Should be dynSeqGen.
            DynSeqGenerator* dsGen = dynamic_cast<DynSeqGenerator*>(elIt->second.get());
            if (dsGen == nullptr || seq != dsGen)
                throw Exception(Error::BAD_REFERENCE + elIt->first);

            // New record for reference.
            if (seqIt == std::end(maxHistory))
            {
                seqRec_t newSr;
                std::get<0>(newSr) = r.first;
                std::get<1>(newSr) = r.second;
                maxHistory.push_back(newSr);
                seqIt = std::end(maxHistory) - 1;
            }
            // Store maximum.
            else if (std::get<1>(*seqIt) < r.second)
            {
                std::get<1>(*seqIt) = r.second;
            }

            // Store sequence.
            std::get<2>(*seqIt).push_back(seq);
        }
    }

    for (auto& seqRec : maxHistory)
    {
        auto grElemIt = parsedEls.find(std::get<0>(seqRec));
        if (grElemIt == std::end(parsedEls))
            throw Exception(Error::UNKNOWN_REFERENCE + std::get<0>(seqRec));

        // Reference should be generator.
        DynGenerator* dynGen = dynamic_cast<DynGenerator*>(grElemIt->second.get());
        if (dynGen == nullptr)
            throw Exception(Error::BAD_REFERENCE + std::get<0>(seqRec));

        dynGen->regHistory(std::get<1>(seqRec));
        // For each sequence that use reference.
        for (auto* seqGen : std::get<2>(seqRec))
        {
            dynGen->regSeqRef(seqGen, std::get<0>(seqRec));
        }
    }
}

void bdg::core::DynSeqGenerator::procGlobSeq(
    GrammarElement::intGenTypeFnc_vec_t& locGens,
    GrammarElement::intGenTypeFnc_vec_t& remGens,
    std::vector<std::vector<GrammarElement::genIdxTypeFnc_t>>& remGensGens,
    std::vector<GrammarElement::GlobSeq>& globSeqs)
{
    auto fIt = std::find_if(std::begin(globSeqs), std::end(globSeqs), [this](GrammarElement::GlobSeq& gs)
    {
        return this == gs.seq;
    });

    if (fIt == std::end(globSeqs))
    {
        GrammarElement::GlobSeq gs;
        gs.seq = this;
        globSeqs.push_back(gs);
        fIt = std::end(globSeqs) - 1;
    }

    // Sequence appears but is not dependant on any generator.
    if (locGens.empty())
    {
        ++fIt->simpleCount;
    }
    else
    {
        // Previous generator idx. Only valid for i > 0!
        size_t lastGenIdx;
        // Goes from the oldest generator (top generator in tree structure).
        for (size_t i = 0; i < locGens.size(); i++)
        {
            IntGenerator* ig;
            GrammarElement::IntGenType type;
            GrammarElement::countTransformer_t fnc;
            std::tie(ig, type, fnc) = locGens[i];

            // Try find local generator in all remote generator.
            auto fIt = std::find_if(remGens.cbegin(), remGens.cend(), [fnc, ig](const GrammarElement::intGenTypeFnc_t& gtf)
            {
                return
                    ig == std::get<0>(gtf) &&
                    fnc == std::get<2>(gtf);
            });

            // Generator index in remGens. Works even if not included.
            int genIdx = fIt - remGens.cbegin();

            // First appearance of remote generator.
            if (fIt == remGens.cend())
            {
                // Create it.
                remGensGens.emplace_back();
                remGens.push_back(locGens[i]);
            }

            // All types have to be the same.
            assert(type == std::get<1>(*std::rbegin(remGens)));

            

            // Store only unique predecesor.
            if (i > 0 &&
                std::find_if(
                    std::cbegin(remGensGens[genIdx]), 
                    std::cend(remGensGens[genIdx]), 
                    [&, lastGenIdx](const GrammarElement::genIdxTypeFnc_t& itf)
                    {
                        GrammarElement::IntGenType t;
                        GrammarElement::countTransformer_t f;
                        std::tie(std::ignore, t, f) = remGens[lastGenIdx];

                        return
                            std::get<0>(itf) == lastGenIdx;/* &&
                            std::get<2>(itf) == f;*/
                    }) == std::cend(remGensGens[genIdx]))
            {
                // Store to dependant generator its type na transform function.
                remGensGens[genIdx].emplace_back(lastGenIdx, std::get<1>(remGens[lastGenIdx]), std::get<2>(remGens[lastGenIdx]));
            }

            lastGenIdx = genIdx;
        }
        
        // Set dependant generator for current global sequence.
        fIt->depGens.push_back(lastGenIdx);
        // Store predecesor type and transformer.
        fIt->typeFncs.emplace_back(std::get<1>(remGens[lastGenIdx]), std::get<2>(remGens[lastGenIdx]));
    }
}


const std::vector<bdg::core::DynSeqGenerator::refRecord_t>& bdg::core::DynSeqGenerator::getRefs() const
{
    return refs;
}

void bdg::core::DynSeqGenerator::setStart(std::size_t startIdx)
{
    setStartImpl(startIdx);
}