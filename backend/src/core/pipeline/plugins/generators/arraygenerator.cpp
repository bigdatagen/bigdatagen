/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file arraygenerator.cpp
* @category Generator
* @author   Big Data Generator Team
*
*/

#include "core/pipeline/plugins/generators/arraygenerator.h" // header
#include "core/pipeline/plugins/generators/grammarelement.h" // grammarElement
#include "core/pipeline/plugins/generators/jsonschemaparser/jsonschemaparser.h" // JsonParser
#include "core/pipeline/plugins/generators/terminal.h" // Terminal
#include "core/pipeline/plugins/generators/dyngenerator.h" // DynSeqGenerator

bdg::core::ArrayGenerator::ArrayGenerator(
    int minLen, 
    int maxLen, 
    const std::vector<GrammarElement::ptr_t>& elems, 
    const std::string &distrName,
    const std::string& brStart, 
    const std::string& brEnd, 
    const std::string& delim):
Cycle<ArrayGenerator>(elems, brStart, brEnd, delim),
sizeGen(minLen, maxLen, distrName)
{
}

bdg::core::ArrayGenerator::ArrayGenerator(
    int minLen, 
    int maxLen, 
    std::vector<GrammarElement::ptr_t>&& elems, 
    const std::string &distrName,
    const std::string& brStart, 
    const std::string& brEnd, 
    const std::string& delim):
Cycle<ArrayGenerator>(elems, brStart, brEnd, delim),
sizeGen(minLen, maxLen, distrName)
{
}

/**
* Generates array size and reset index
*/
void bdg::core::ArrayGenerator::initLoop()
{
    sizeVal = sizeGen.isRemote() ? sizeGen.remGenValue() : sizeGen.generateValue();
    i = 0;
}

/**
* Test whether we are at the end of generating
*/
bool bdg::core::ArrayGenerator::isEnd()
{
    return i >= sizeVal;
}

/**
* Increment index
*/
void bdg::core::ArrayGenerator::inc()
{
    ++i;
}

/**
* Stores to localGenerator pointer to current sizeGenerator with mark
* that says that we know number of iteration from generated value
* and function that count for each member how many times will be
* in current array.
*/
void bdg::core::ArrayGenerator::repairImpl(
    const bdg::core::GrammarElement::element_map_t& parsedEls,
    const bdg::core::GrammarElement::seq_vector_t& locSeqs,
    bdg::core::GrammarElement::intGenTypeFnc_vec_t& locGens,
    GrammarElement::intGenTypeFnc_vec_t& remGens,
    std::vector<std::vector<GrammarElement::genIdxTypeFnc_t>>& remGensGens,
    std::vector<GrammarElement::GlobSeq>& globSeqs,
    std::vector<const GrammarElement*>& pathEls)
{
    const size_t s = size();
    size_t i = 0;
    for (auto& el : elements)
    {
        locGens.emplace_back(
            &sizeGen,
            GrammarElement::IntGenType::ONE_TIME,
            CountTransFtor(
                [](const CountTransFtor& refThis, int genVal)
                {
                    //sarg1 = s;
                    //sarg2 = i;

                    // Return full count of iterations and if index is lower
                    // than we add 1.
                    return (genVal / refThis.sarg1) + ((genVal % refThis.sarg1) > refThis.sarg2);
                },
                s,
                i));

        el->repair(parsedEls, locSeqs, locGens, remGens, remGensGens, globSeqs, pathEls);
        locGens.pop_back();
        ++i;
    }
}
