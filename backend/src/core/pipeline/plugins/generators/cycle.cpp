/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file cycle.cpp
* @category Generator
* @author   Big Data Generator Team
*
*/

#include "core/pipeline/plugins/generators/cycle.h" // header

// Nothing here.