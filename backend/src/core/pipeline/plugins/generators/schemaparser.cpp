/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file schemaparser.cpp
* @category Generators
* @author   Big Data Generator Team
*/

#include "core/pipeline/plugins/generators/schemaparser.h" // header
#include "core/pipeline/plugins/generators/generator.h" // Generator
#include "core/pipeline/plugins/generators/intgenerator.h" // IntGenerator
#include "core/pipeline/plugins/generators/constants.h" // Constants

bdg::core::SchemaParser::SchemaParser(Generator &gen, TypeFactory& tf):
    factory(tf),
    gen(gen)
{
}

/**
* First call parser to schema and then repeatedly creates element from
* all UDTs. Then repairs them, prepares generating of sequences
* and registers generators.
*/
void bdg::core::SchemaParser::parse(const std::string& schema)
{
    parseSchema(schema);

    while (hasNextUdt())
    {
        (void)createElement<true>(factory, getNextUdt(), allElems, allSeqs);
    }

    auto rootIt = allElems.find(Constants::ITEM_TYPE_ROOT);
    if (rootIt == this->allElems.cend())
    {
        throw Exception(Error::MISSING_ROOT);
    }

    // Purpously added empty vectors, are used as buffer.
    GrammarElement::intGenTypeFnc_vec_t buff;
    std::vector<const GrammarElement*> grElems;
    rootIt->second->repair(
        allElems,
        allSeqs,
        buff,
        gen.remIntGens,
        gen.remGensGens,
        gen.globSeqs,
        grElems);

    // Each sequence starts with 0.
    gen.seqsIdx.resize(gen.globSeqs.size(), 0);
    gen.lastSeqsIdx.resize(gen.seqsIdx.size(), 0);
    gen.remIntVals.resize(gen.remIntGens.size());
    gen.sumGenVals.resize(gen.remIntGens.size());

    for (const auto& gtf : gen.remIntGens)
    {
        IntGenerator* rig;
        std::tie(rig, std::ignore, std::ignore) = gtf;
        if (!rig->isConst())
        {
            rig->setRemote();
        }
    }

    //setRemotes(seqsGens);
    size_t idx = 0;
    rootIt->second->registerGenerator(idx, gen.preGenFncs, gen.genFncs);
}
