/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file exception.cpp
* @category Generator
* @author   Big Data Generator Team
*
*/

#include "core/pipeline/plugins/generators/exception.h" // header

using std::string;

bdg::core::Exception::Exception(const std::string &msg) :
    std::runtime_error(msg)
{
}

bdg::core::Exception::Exception(
    const std::string &msg,
    const std::runtime_error &innerException) :
    std::runtime_error(msg + '(' + innerException.what() + ')')
{
}
