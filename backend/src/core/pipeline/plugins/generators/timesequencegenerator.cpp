/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file timesequencesgenerator.cpp
* @category Generator
* @author   Big Data Generator Team
*
*/
	
#include "core/pipeline/plugins/generators/timesequencegenerator.h" // header
#include "core/pipeline/plugins/generators/exception.h"
#include "core/pipeline/plugins/generators/constants.h"

#include <cstdlib> // strtol.
#include <iomanip> // get_time, put_time.
#include <sstream> // stringstream

const std::string bdg::core::TimeSeqGenerator::SEC = "sec";
const std::string bdg::core::TimeSeqGenerator::MIN = "min";
const std::string bdg::core::TimeSeqGenerator::HOUR = "hour";
const std::string bdg::core::TimeSeqGenerator::DAY = "day";
const std::string bdg::core::TimeSeqGenerator::MONTH = "month";
const std::string bdg::core::TimeSeqGenerator::YEAR = "year";
const std::string bdg::core::TimeSeqGenerator::TIME_FORMAT = "%H:%M:%S";
const std::string bdg::core::TimeSeqGenerator::DATE_FORMAT = "%Y-%m-%d";

bdg::core::TimeSeqGenerator::TimeSeqGenerator(
    const std::string& initDate, 
    const std::string& initTime,
    const std::string& step, 
    const std::string& format, 
    bool isLocal):
format("%FT%TZ"),
step(parseStep(step)),
initPoint(parseInitial(initTime, initDate)),
currTime(initPoint),
isLocal(isLocal)
{
}

/**
* We need to get to the point where current genetion of sequence starts.
*/
void bdg::core::TimeSeqGenerator::init(HistoryStorage<std::string>& h)
{
    while (idx++ < startIdx)
    {
        currTime += step;
    }
}

/**
* Generates step in time.
*/
std::string bdg::core::TimeSeqGenerator::generateValue()
{
    // Convert value.
    std::ostringstream ss;
    const std::time_t tt = std::chrono::system_clock::to_time_t(currTime);
    const std::tm* t = std::gmtime(&tt);
    ss << '\"' << std::put_time(t, format.c_str()) << '\"';

    // Do step.
    currTime += step;

    return ss.str();
}

void bdg::core::TimeSeqGenerator::generate(GenStorer& gs)
{
    gs.store(generateValue());
}

/**
* Parses initial date and time.
*/
std::chrono::system_clock::time_point bdg::core::TimeSeqGenerator::parseInitial(
    const std::string& initTime,
    const std::string& initDate)
{
    std::tm t = {};

    std::istringstream ss(initTime);
    ss >> std::get_time(&t, TIME_FORMAT.c_str());
    if (ss.fail())
        throw Exception(Error::TS_INVALID_TIME);

    ss = std::istringstream(initDate);
    ss >> std::get_time(&t, DATE_FORMAT.c_str());
    if (ss.fail())
        throw Exception(Error::TS_INVALID_DATE);

    std::time_t tt = std::mktime(&t);
    return std::chrono::system_clock::from_time_t(tt);
}

/**
* Parses given step from user.
*/
std::chrono::seconds bdg::core::TimeSeqGenerator::parseStep(const std::string& step)
{
    try
    {
        char* cstep;
        long s = std::strtol(step.c_str(), &cstep, 10);
        if (cstep == SEC)
        {
            return std::chrono::seconds(s);
        }
        else if (cstep == MIN)
        {
            return std::chrono::minutes(s);
        }
        else if (cstep == HOUR)
        {
            return std::chrono::minutes(s * 60);
        }
        else if (cstep == DAY)
        {
            return std::chrono::minutes(s * 60 * 24);
        }
        else
        {
            throw Exception(Error::TS_INVALID_STEP);
        }
    }
    catch (...)
    {
        throw Exception(Error::TS_INVALID_STEP);
    }
}

void bdg::core::TimeSeqGenerator::setStartImpl(std::size_t startIdx)
{
    this->startIdx = startIdx;
}

/**
* Repair sequences same way as real sequences.
*/
void bdg::core::TimeSeqGenerator::preGenImpl(std::vector<GrammarElement::gen_fn_t>& preGenFncs)
{
    if (!isPreGenRegistered)
    {
        if (isLocal)
        {
            const size_t nextIdx = preGenFncs.size() + 1;
            preGenFncs.push_back([nextIdx, this](GenStorer& gs)
            {
                idx = 0;
                currTime = initPoint;

                return nextIdx;
            });
        }

        isPreGenRegistered = true;
    }
}

void bdg::core::TimeSeqGenerator::repairImpl(
    const GrammarElement::element_map_t& parsedEls, 
    const GrammarElement::seq_vector_t& seqs, 
    GrammarElement::intGenTypeFnc_vec_t& locGens,
    GrammarElement::intGenTypeFnc_vec_t& remGens, 
    std::vector<std::vector<GrammarElement::genIdxTypeFnc_t>>& remGensGens,
    std::vector<GrammarElement::GlobSeq>& globSeqs,
    std::vector<const GrammarElement*>& pathEls)
{
    procSeq(parsedEls, seqs);
    if (!isLocal)
    {
        procGlobSeq(locGens, remGens, remGensGens, globSeqs);
    }
}
