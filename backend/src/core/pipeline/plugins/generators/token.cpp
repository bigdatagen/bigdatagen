/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file token.cpp
* @category Generator
* @author   Big Data Generator Team
*
*/

#include "core/pipeline/plugins/generators/token.h" // header
#include "core/pipeline/plugins/generators/exception.h" // Exception

// MINUS

bdg::core::Minus<std::string>::Minus():
    BinOp<std::string, Minus<std::string>>(6)
{}

std::string bdg::core::Minus<std::string>::eval(const std::string * p1, const std::string * p2) const
{
    throw Exception(Error::UNSUPPORTED_OPERATION + "string");
}

void bdg::core::Minus<std::string>::regSeqGenImpl(
    std::stack<const std::string*>& toks,
    const std::vector<dataRef_t<std::string>>& valRefs,
    std::vector<std::string>& consts,
    std::vector<std::string>& tmps,
    std::vector<seqFn_t<std::string>>& fncs)
{
    throw Exception(Error::UNSUPPORTED_OPERATION + "string");
}

// MUL

bdg::core::Mul<std::string>::Mul() :
    BinOp<std::string, Mul<std::string>>(5)
{}

std::string bdg::core::Mul<std::string>::eval(const std::string * p1, const std::string * p2) const
{
    throw Exception(Error::UNSUPPORTED_OPERATION + "string");
}

void bdg::core::Mul<std::string>::regSeqGenImpl(
    std::stack<const std::string*>& toks,
    const std::vector<dataRef_t<std::string>>& valRefs,
    std::vector<std::string>& consts,
    std::vector<std::string>& tmps,
    std::vector<seqFn_t<std::string>>& fncs)
{
    throw Exception(Error::UNSUPPORTED_OPERATION + "string");
}

// DIV

bdg::core::Div<std::string>::Div() :
    BinOp<std::string, Div<std::string>>(5)
{}

std::string bdg::core::Div<std::string>::eval(const std::string * p1, const std::string * p2) const
{
    throw Exception(Error::UNSUPPORTED_OPERATION + "string");
}

void bdg::core::Div<std::string>::regSeqGenImpl(
    std::stack<const std::string*>& toks,
    const std::vector<dataRef_t<std::string>>& valRefs,
    std::vector<std::string>& consts,
    std::vector<std::string>& tmps,
    std::vector<seqFn_t<std::string>>& fncs)
{
    throw Exception(Error::UNSUPPORTED_OPERATION + "string");
}

// MOD

bdg::core::Mod<double>::Mod() :
    BinOp<double, Mul<double>>(5)
{}

double bdg::core::Mod<double>::eval(const double * p1, const double * p2) const
{
    throw Exception(Error::UNSUPPORTED_OPERATION + "double");
}

void bdg::core::Mod<double>::regSeqGenImpl(
    std::stack<const double*>& toks,
    const std::vector<dataRef_t<double>>& valRefs,
    std::vector<double>& consts,
    std::vector<double>& tmps,
    std::vector<seqFn_t<double>>& fncs)
{
    throw Exception(Error::UNSUPPORTED_OPERATION + "double");
}

bdg::core::Mod<std::string>::Mod() :
    BinOp<std::string, Mod<std::string>>(5)
{}

std::string bdg::core::Mod<std::string>::eval(const std::string * p1, const std::string * p2) const
{
    throw Exception(Error::UNSUPPORTED_OPERATION + "string");
}

void bdg::core::Mod<std::string>::regSeqGenImpl(
    std::stack<const std::string*>& toks,
    const std::vector<dataRef_t<std::string>>& valRefs,
    std::vector<std::string>& consts,
    std::vector<std::string>& tmps,
    std::vector<seqFn_t<std::string>>& fncs)
{
    throw Exception(Error::UNSUPPORTED_OPERATION + "string");
}