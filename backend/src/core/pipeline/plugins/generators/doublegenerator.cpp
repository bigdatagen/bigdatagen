/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file doublegenerator.cpp
* @category Generator
* @author   Big Data Generator Team
*
*/
	
#include "core/pipeline/plugins/generators/doublegenerator.h" // header
#include "core/pipeline/plugins/generators/constants.h" // Constants
#include "utils/exception.h" 

#include <chrono> // chrono (random seed)

bdg::core::DoubleGenerator::DoubleGenerator(double minValue, double maxValue, const std::string &distrName):
	gen(static_cast<unsigned int>(std::chrono::system_clock::now().time_since_epoch().count()))
{
    if (minValue > maxValue)
    {
        throw bdg::utils::BadBoundariesException("minValue: " + std::to_string(minValue) + ", maxValue: " + std::to_string(maxValue));
    }

    if (distrName == Constants::DISTR_UNIFORM)
    {
        distr = DistrBase<double>::ptr_t(new SmartDistr<double, std::uniform_real_distribution<double>>(minValue, maxValue));
    }
    else if (distrName == Constants::DISTR_NORMAL)
    {
        distr = DistrBase<double>::ptr_t(new NormalDistr<double>(minValue, maxValue));
    }
    else if (distrName == Constants::DISTR_EXP)
    {
        distr = DistrBase<double>::ptr_t(new ExpDistr<double>(minValue, maxValue));
    }
    else
    {
        throw bdg::utils::UnsupportedDistributionException();
    }
}

void bdg::core::DoubleGenerator::init(HistoryStorage<double>& h)
{
    // Do nothing.
}

double bdg::core::DoubleGenerator::generateValue()
{
	return distr->generate(gen);
}

void bdg::core::DoubleGenerator::generate(GenStorer& gs)
{
    gs.store(std::move(bdg::utils::toString(generateValue())));
}
