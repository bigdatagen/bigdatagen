/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file terminal.cpp
* @category Generator
* @author   Big Data Generator Team
*
*/
	
#include "core/pipeline/plugins/generators/terminal.h" // header
#include "core/pipeline/plugins/generators/genstorer.h" // GenStorer

bdg::core::Terminal::Terminal(const std::string& value):
	value(value)
{
}

bdg::core::Terminal::Terminal(std::string&& value):
    value(value)
{
}

/**
* Register function that prints value holding by temrinal.
*/
void bdg::core::Terminal::registerGeneratorImpl(
    std::size_t& currIdx,
    std::vector<bdg::core::GrammarElement::gen_fn_t>& preGenFncs,
    std::vector<bdg::core::GrammarElement::gen_fn_t>& genFncs)
{
    const std::size_t idx = ++currIdx;
	genFncs.push_back([this, idx](GenStorer& gs)
	{
		gs.store(value);
        return idx;
	});
}