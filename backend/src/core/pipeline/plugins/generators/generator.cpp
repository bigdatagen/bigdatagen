/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file generator.cpp
* @category Generator
* @author   Big Data Generator Team
*
*/

#include "core/pipeline/plugins/generators/generator.h"      // Header.
#include "core/pipeline/plugins/generators/grammarelement.h"    // GrammarElement.
#include "core/pipeline/plugins/generators/intgenerator.h"    // Intgenerator

#include <cassert> // assert.
#include <algorithm> // transform, generate
#include <numeric> // accumulate

using std::begin;
using std::end;
using std::cbegin;
using std::cend;

void bdg::core::Generator::setRank(const int rank)
{
    this->rank = rank;
}

void bdg::core::Generator::openStorage(const std::string& fileName)
{
    assert(rank != 0);

    genStorer.open(fileName, rank);
}

void bdg::core::Generator::closeStorage()
{
    assert(rank != 0);

    genStorer.close();
}

void bdg::core::Generator::flushStorageFiles()
{
    assert(rank != 0);

    genStorer.flushFiles();
}

void bdg::core::Generator::generate()
{
    assert(rank != 0);

    const size_t preMaxIdx = preGenFncs.size();
    size_t idx = 0;
    while(idx < preMaxIdx)
    {
        idx = preGenFncs[idx](genStorer);
    }

	const size_t maxIdx = genFncs.size();
	idx = 0;
	while (idx < maxIdx)
    {
		idx = genFncs[idx](genStorer);
    }
}

void bdg::core::Generator::setFilesCount(std::size_t fcount)
{
    assert(rank == 0);

    genMult = fcount;
}

const std::vector<std::vector<int>>& bdg::core::Generator::genRemInts()
{
    size_t genIdx = 0;
    // For each generator generate needed values.
    std::transform(remIntGens.cbegin(), remIntGens.cend(), begin(remIntVals), [&, this](const GrammarElement::intGenTypeFnc_t& gtf)
    {
        IntGenerator* ig;
        GrammarElement::IntGenType type;
        GrammarElement::countTransformer_t transformFnc;
        std::tie(ig, type, transformFnc) = gtf;

        int valsCount = 0;

        // No dependant value, they are highest in hierarchy.
        if (remGensGens[genIdx].empty())
        {
            // We will generate them in each multiplication (file generation).
            valsCount = genMult;
        }
        else
        {
            int acc = 0;
            // For each generator on which is current generator dependant.
            for (const GrammarElement::genIdxTypeFnc_t& itf : remGensGens[genIdx])
            {
                size_t depGenIdx;
                GrammarElement::IntGenType t;
                GrammarElement::countTransformer_t fnc;
                std::tie(depGenIdx, t, fnc) = itf;

                if (t == GrammarElement::IntGenType::ONE_TIME)
                {
                    // Go through all generated values -> transform them and sum them.
                    acc += std::accumulate(
                        std::cbegin(remIntVals[depGenIdx]),
                        std::cend(remIntVals[depGenIdx]),
                        0,
                        [&fnc](int sum, int remInt)
                        {
                            return sum + fnc(remInt);
                        });
                }
                else
                {
                    assert(t == GrammarElement::IntGenType::CONTINUOUS);

                    // Go through all generated values a count if they are used.
                    acc += std::count_if(
                        std::cbegin(remIntVals[depGenIdx]),
                        std::cend(remIntVals[depGenIdx]),
                        fnc);
                }

                valsCount += acc;
            }
        }

        // Generate values for current generator.
        std::vector<int> res;

        int sum = 0;
        // If generator is const.
        if (ig->isConst())
        {
            // We don't need to generate values of current generator, we know them.
            // Just store sum of generated values.
            sum = ig->getMin() * valsCount;
            res.resize(valsCount, ig->getMin());
        }
        else
        {
            res.resize(valsCount);
            std::generate(std::begin(res), std::end(res), [ig, &sum]() 
            { 
                const int v = ig->generateValue();
                sum += v;
                return v;
            });
        }

        // Store sum of all generated values.
        sumGenVals[genIdx] = sum;
        ++genIdx;
        return res;
    });

    return remIntVals;
}

void bdg::core::Generator::regRemInts(const std::vector<std::vector<int>>& remInts)
{
    assert(rank != 0);
    assert(remInts.size() == remIntGens.size());

    for (size_t i = 0; i < remInts.size(); i++)
    {
        IntGenerator* ig;
        std::tie(ig, std::ignore, std::ignore) = remIntGens[i];
        ig->regData(remInts[i]);
    }
}

void bdg::core::Generator::resetRemote()
{
    for (const auto& gtf : remIntGens)
    {
        IntGenerator* ig;
        std::tie(ig, std::ignore, std::ignore) = gtf;
        ig->resetRemote();
    }
}

const std::vector<std::size_t>& bdg::core::Generator::genSeqsStart()
{
    assert(rank == 0);

    std::swap(lastSeqsIdx, seqsIdx);

    // Add to each sequence index last value of generator.
    std::transform(
        globSeqs.cbegin(),
        globSeqs.cend(),
        std::begin(lastSeqsIdx),
        std::begin(seqsIdx),
        [this](const GrammarElement::GlobSeq& gs, std::size_t sIdx)
    {
        size_t idx = 0;
        return sIdx + genMult * gs.simpleCount + 
            std::accumulate(gs.depGens.cbegin(), gs.depGens.cend(), 0, [&,this](int sum, size_t depGenIdx)
            {
                GrammarElement::IntGenType t;
                GrammarElement::countTransformer_t fnc;
                std::tie(t, fnc) = gs.typeFncs[idx];

                int acc = 0;
                if (t == GrammarElement::IntGenType::ONE_TIME)
                {
                    acc = std::accumulate(
                        std::cbegin(remIntVals[depGenIdx]),
                        std::cend(remIntVals[depGenIdx]),
                        0,
                        [&fnc](int a, int remVal)
                        {
                            return a + fnc(remVal);
                        });
                }
                else
                {
                    assert(t == GrammarElement::IntGenType::CONTINUOUS);

                    acc = std::count_if(
                        std::cbegin(remIntVals[depGenIdx]),
                        std::cend(remIntVals[depGenIdx]),
                        fnc);
                }

                return sum + acc;
            });
    });

    return lastSeqsIdx;
}

void bdg::core::Generator::setSeqsStart(const std::vector<std::size_t>& startIdcs)
{
    assert(rank != 0);
	assert(globSeqs.size() == startIdcs.size());

    for (size_t i = 0; i < globSeqs.size(); i++)
    {
        globSeqs[i].seq->setStart(startIdcs[i]);
    }
}
