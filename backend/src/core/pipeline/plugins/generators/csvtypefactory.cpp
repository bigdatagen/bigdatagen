/**
*
* @file elementfactory.cpp
*
*/
	
#include "core/pipeline/plugins/generators/csvtypefactory.h" // header
#include "core/pipeline/plugins/generators/jsonutils.h" 
#include "core/pipeline/plugins/generators/exception.h" 
#include "core/pipeline/plugins/generators/complexgenerator.h" 
#include "core/pipeline/plugins/generators/nonterminal.h"
#include "core/pipeline/plugins/generators/terminal.h"
#include "core/pipeline/plugins/generators/arraygenerator.h"
#include "core/pipeline/plugins/generators/schemaparser.h"
#include "core/pipeline/plugins/generators/referenceelement.h"

bdg::core::GrammarElement::ptr_t  bdg::core::CsvTypeFactory::createRoot(
    TypeFactory& tf,
    const SchemaElement::uptr_t& se,
    SchemaParser& parser)
{
    JsonSchemaElement& jse = *dynamic_cast<JsonSchemaElement*>(se.get());
    return createRootImpl(parser, tf, jse.obj, parser.allElems, parser.allSeqs);
}

bdg::core::CsvTypeFactory::CsvTypeFactory(const factory_fnc_t& nonFoundFnc, std::vector<name_factory_fnc_t>&& init):
    JsonTypeFactory(nonFoundFnc, std::move(init))
{
}

bdg::core::GrammarElement::ptr_t bdg::core::CsvTypeFactory::createRootImpl(
    SchemaParser& parser,
    TypeFactory& tf,
    Poco::JSON::Object::Ptr const& jsonObject,
    GrammarElement::element_map_t& parsedElements,
    std::vector<DynSeqGenerator*>& seqs)
{
	const std::string sep = JsonUtils::getPropertyValue<std::string>(jsonObject, Constants::ITEM_SEPARATOR);
	int minLen = JsonUtils::getPropertyValue<int>(jsonObject, Constants::PROPERTY_MIN);
	int maxLen = JsonUtils::getPropertyValue<int>(jsonObject, Constants::PROPERTY_MAX);

    std::vector<GrammarElement::ptr_t> ges;
	ges.push_back(createRowElement(
		parser,
	    tf,
		jsonObject,
        parsedElements,
        seqs,
		sep));

    // todo: Parse domain values here.

    static const std::string ROW_DELIM{ "\n" };
    static const std::string EMPTY{ "" };

    return GrammarElement::makeGe<ArrayGenerator>(minLen, maxLen, std::move(ges), Constants::DISTR_NORMAL, "", "", ROW_DELIM);
}

bdg::core::GrammarElement::ptr_t bdg::core::CsvTypeFactory::createRowElement(
    SchemaParser& parser,
    TypeFactory& tf,
    Poco::JSON::Object::Ptr const& jsonObject,
    GrammarElement::element_map_t& parsedElements,
    std::vector<DynSeqGenerator*>& seqs,
    const std::string &delim)
{
    const auto vcols = JsonUtils::getPropertyArray(jsonObject, Constants::ITEM_COLUMNS);

    // Parse json.
    std::vector<ComplexGenerator::prop_elem_t> pes;
    pes.reserve(vcols->size());
    std::transform(std::cbegin(*vcols), std::cend(*vcols), std::back_inserter(pes), 
        [&parser, &tf, &parsedElements, &seqs](const Poco::Dynamic::Var vobj)->ComplexGenerator::prop_elem_t
        {
            return{
                "",
                parser.createElement<false>(
                    tf,
                    SchemaElement::uptr_t(new JsonSchemaElement(JsonUtils::getObject(vobj))),
                    parsedElements,
                    seqs)
            };
        });

    static const std::string EMPTY{ "" };
    // Create object.
    return GrammarElement::makeGe<ComplexGenerator>(
        pes,
        EMPTY,
        EMPTY,
        delim,
        [](const ComplexGenerator::prop_elem_t& pe)->GrammarElement::ptr_t
        {
            return pe.second;
        });
}
