/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file jsonutils.cpp
* @category Generators
* @author   Big Data Generator Team
*/

#include "core/pipeline/plugins/generators/jsonutils.h" // header
#include "core/pipeline/plugins/generators/exception.h" // Exception
#include "core/pipeline/plugins/generators/grammarelement.h" // GrammarElement

#include <Poco/JSON/Object.h>
#include <Poco/JSON/Array.h>
#include <Poco/JSON/JSON.h>
#include <Poco/JSON/Stringifier.h>
#include <Poco/JSON/JSONException.h>
#include <Poco/JSON/Parser.h> // Parser
#include <Poco/Dynamic/Var.h> // Var

#include <memory> // unique_ptr
#include <algorithm> // for_each
#include <cassert> // assert

using std::string;

/**
* Return property of type array from object.
*/
Poco::JSON::Array::Ptr bdg::core::JsonUtils::getPropertyArray(
    const Poco::JSON::Object::Ptr &obj,
    const std::string &propertyName)
{
    auto varr = obj->get(propertyName);
    if (varr.isEmpty())
    {
        throw Exception(Error::MISSING_PROPERTY + propertyName);
    }

    if (!obj->isArray(propertyName))
    {
        throw Exception(Error::BAD_PROPERTY_TYPE + propertyName);
    }

    return obj->getArray(propertyName);
}

/**
* Returns property of type object from object.
*/
Poco::JSON::Object::Ptr bdg::core::JsonUtils::getPropertyObject(
    const Poco::JSON::Object::Ptr &obj,
    const std::string & propertyName)
{
    auto vobj = obj->get(propertyName);
    if (vobj.isEmpty())
    {
        throw Exception(Error::MISSING_PROPERTY + propertyName);
    }

    if (!obj->isObject(propertyName))
    {
        throw Exception(Error::BAD_PROPERTY_TYPE + propertyName);
    }

    return obj->getObject(propertyName);
}

/**
* Converts dynamic value into object.
*/
Poco::JSON::Object::Ptr bdg::core::JsonUtils::getObject(const Poco::Dynamic::Var &vobj)
{
    try
    {
        return vobj.extract<Poco::JSON::Object::Ptr>();
    }
    catch (...)
    {
        throw Exception(Error::NOT_OBJECT);
    }
}

/**
* Converts dynamic value into array.
*/
Poco::JSON::Array::Ptr bdg::core::JsonUtils::getArray(const Poco::Dynamic::Var& varr)
{
    try
    {
        return varr.extract<Poco::JSON::Array::Ptr>();
    }
    catch (...)
    {
        throw Exception(Error::NOT_ARRAY);
    }
}