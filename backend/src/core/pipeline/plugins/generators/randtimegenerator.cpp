/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file randtimegenerator.cpp
* @category Generator
* @author   Big Data Generator Team
*
*/
	
#include "core/pipeline/plugins/generators/randtimegenerator.h" // header
#include "utils/convert.h"

#include <iomanip> // put_time
#include <chrono> // time...
#include <sstream> // stringstream
#include <cassert> // assert
#include <cstdlib> // strtoul

bdg::core::RandTimeGenerator::RandTimeGenerator(
    const std::string& format, 
    const std::string& minDate, 
    const std::string& minTime, 
    const std::string& maxDate, 
    const std::string& maxTime):
initDt(parseDateTime(minDate, minTime)),
endDt(parseDateTime(maxDate, maxTime)),
format(format),
intGen(0, static_cast<int>(std::chrono::duration_cast<std::chrono::seconds>(endDt - initDt).count()))
{
    if (endDt < initDt)
        throw Exception(Error::BAD_PROPERTY_VALUE + "end date time is lower than start date time.");

}

void bdg::core::RandTimeGenerator::init(HistoryStorage<std::string>& h)
{
    // Do nothing.
}

/**
* Just simply add random number to initial time and convert it back to date and time.
*/
std::string bdg::core::RandTimeGenerator::generateValue()
{
    // Convert value.
    std::ostringstream ss;
    const std::time_t tt = std::chrono::system_clock::to_time_t(initDt + std::chrono::seconds(intGen.generateValue()));
    const std::tm* t = std::gmtime(&tt);
    ss << '\"' << std::put_time(t, format.c_str()) << '\"';

    return ss.str();
}

void bdg::core::RandTimeGenerator::generate(bdg::core::GenStorer& gs)
{
    gs.store(std::move(generateValue()));
}

/**
* Parses date and time from strings.
*/
std::chrono::system_clock::time_point bdg::core::RandTimeGenerator::parseDateTime(const std::string& date, const std::string& time)
{
    try
    {
        std::stringstream dt; 
        dt << date + ',' + time;
        static const std::string f = "%Y-%m-%d,%H:%M:%S";

        std::tm tt = {};
        dt >> std::get_time(&tt, f.c_str());
        const std::time_t t = std::mktime(&tt);
        return std::chrono::system_clock::from_time_t(t);
    }
    catch (...)
    {
        throw Exception(Error::BAD_DTIME + date + ',' + time);
    }
}

