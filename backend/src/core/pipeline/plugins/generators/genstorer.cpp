/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file genstorer.cpp
* @category Generator
* @author   Big Data Generator Team
*
*/

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS         // Disable warning with fopen.
#endif

#include "core/pipeline/plugins/generators/genstorer.h"     // Header
#include "utils/config.h"                                   // Config

#include <Poco/File.h>          // Poco::File
#include <Poco/Path.h>          // Poco::Path
#include <Poco/String.h>        // Poco::replace

#include <algorithm>            // std::min, std::max
#include <cstring>              // std::memcpy
#include <stdexcept>            // std::runtime_error

bdg::core::GenStorer::GenStorer(std::size_t buffSize):
    buffSize(buffSize > 0 ? buffSize : bdg::utils::Config::getInstance().getOutputBufferSizeBytes())
{
    buff = (char*)std::malloc(sizeof(char) * (this->buffSize > 0 ? this->buffSize : 4096));

    if (buff == nullptr)
    {
        throw std::runtime_error("No memory");
    }
}

bdg::core::GenStorer::~GenStorer()
{
    close();
    std::free(buff);
}

void bdg::core::GenStorer::open(const std::string& fName, const int rank)
{
    if (file != nullptr)
    {
        throw std::runtime_error("Already open file");
    }

    if (!bdg::utils::Config::getInstance().getOutputStoragePathLocal().empty())
    {
        fileNameLocal = bdg::utils::Config::getInstance().getOutputStoragePathLocal() + Poco::Path::separator() + std::to_string(rank) + Poco::Path::separator() + Poco::Path(fName).getFileName();
    }
    else
    {
        fileNameLocal = fName;
    }

    fileName = fName;
    file = std::fopen(fileNameLocal.c_str(), "w");
    
    if (file == nullptr)
    {
        throw std::runtime_error("Can't open file.");
    }

    storedBytes = 0;
}

void bdg::core::GenStorer::close()
{
    if (file != nullptr)
    {
        if (size > 0)
        {
            flush();
        }
        
        if (std::fclose(file))
        {
            file = nullptr;
            throw std::runtime_error("Can't close file.");
        }

        if (fileNameLocal != fileName)
        {
            // Close the file asynchronously when we store more than 0.5 MB of data. This way
            // we can rapidly improve performance.
            if (storedBytes > 500 * 1024)
            {
                fileFutures.push_back(std::async(std::launch::async, [](const std::string &copyFrom, const std::string &copyTo)
                {
                    if (bdg::utils::Config::getInstance().pipelineContinuousCleanUp())
                    {
                        Poco::File(copyFrom).moveTo(copyTo);
                    }
                    else
                    {
                        Poco::File(copyFrom).copyTo(copyTo);
                    }

                    return 0;
                }, fileNameLocal, fileName));
            }
            else
            {
                if (bdg::utils::Config::getInstance().pipelineContinuousCleanUp())
                {
                    Poco::File(fileNameLocal).moveTo(fileName);
                }
                else
                {
                    Poco::File(fileNameLocal).copyTo(fileName);
                }
            }
        }

        file = nullptr;
    }
}

void bdg::core::GenStorer::flushFiles()
{
    for (auto &future : fileFutures)
    {
        future.get();
    }

    fileFutures.clear();
}

void bdg::core::GenStorer::store(char c)
{
    if (size == buffSize)
    {
        flush();
    }

    buff[size++] = c;
    ++storedBytes;
}

void bdg::core::GenStorer::store(std::string&& str)
{
    store(str);
}

void bdg::core::GenStorer::store(const std::string& str)
{
    std::size_t i = 0;

    while (i < str.size())
    {
        if (size == buffSize)
        {
            flush();
        }

        std::size_t m = std::min<std::size_t>(str.size() - i, buffSize - size);
        std::memcpy(&buff[size], &str[i], m);

        i += m;
        size += m;
        storedBytes += m;
    }
}

void bdg::core::GenStorer::flush()
{
    size_t wSize = std::fwrite(&buff[0], sizeof(std::string::value_type), size, file);

    if (wSize != size)
    {
        std::fclose(file);
        file = nullptr;
        size = 0;
        throw std::runtime_error("Data not written");
    }

    size = 0;
}
