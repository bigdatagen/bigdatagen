/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file randomintgenerator.cpp
* @category Generator
* @author   Big Data Generator Team
*
*/
	
#include "core/pipeline/plugins/generators/intgenerator.h" // header
#include "core/pipeline/plugins/generators/exception.h" // Exception
#include "core/pipeline/plugins/generators/constants.h" // Constants
#include "utils/exception.h"

#include <chrono> // chrono

bdg::core::IntGenerator::IntGenerator(int minValue, int maxValue, const std::string &distrName) :
    gen(static_cast<unsigned int>(std::chrono::system_clock::now().time_since_epoch().count()))
{
	if (minValue > maxValue)
	{
		throw bdg::utils::BadBoundariesException("minValue: " + std::to_string(minValue) + ", maxValue: " + std::to_string(maxValue));
	}

	if (distrName == Constants::DISTR_UNIFORM)
	{
		distr = DistrBase<int>::ptr_t(new SmartDistr<int, std::uniform_int_distribution<int>>(minValue, maxValue));
	}
	else if (distrName == Constants::DISTR_NORMAL)
	{
		distr = DistrBase<int>::ptr_t(new NormalDistr<int>(minValue, maxValue));
	}
	else if (distrName == Constants::DISTR_POISSON)
	{
		distr = DistrBase<int>::ptr_t(new PoissonDistr<int>(minValue, maxValue));
	}
	else if (distrName == Constants::DISTR_EXP)
	{
		distr = DistrBase<int>::ptr_t(new ExpDistr<int>(minValue, maxValue));
	}
	else if (distrName == Constants::DISTR_BINOMIAL)
	{
		distr = DistrBase<int>::ptr_t(new BinomDistr<int>(minValue, maxValue));
	}
    else if (distrName == Constants::DISTR_GEOMETRIC)
    {
        if (minValue != maxValue) { distr = DistrBase<int>::ptr_t(new GeomDistr<int>(minValue, maxValue)); }
    }
	else
	{
		throw bdg::utils::UnsupportedDistributionException();
	}

	/* When we have one-element interval, we can simply use uniform distribution. */
	if (minValue == maxValue)
	{
		distr = DistrBase<int>::ptr_t(new SmartDistr<int, std::uniform_int_distribution<int>>(minValue, maxValue));
	}
}

/**
* Returns generated minimum.
*/
int bdg::core::IntGenerator::getMin() const
{
	return distr->getMin();
}

/**
* Returns generated maximum.
*/
int bdg::core::IntGenerator::getMax() const
{
	return distr->getMax();
}

/**
* Returns true if generator is constant and needs no generating.
*/
bool bdg::core::IntGenerator::isConst() const
{
    return getMin() == getMax();
}

/**
* Unsed.
* @todo: Remove me.
*/
void bdg::core::IntGenerator::registerInc(const std::function<bool(int)>& incrementer)
{
    incFnc = incrementer;
}

/**
* Unused.
* @todo: Remove me.
*/
const std::function<bool(int)>& bdg::core::IntGenerator::getInc() const
{
    return incFnc;
}

/**
* Don't need to initilize int generator.
*/
void bdg::core::IntGenerator::init(HistoryStorage<int>& h)
{
    // Do nothing.
}

/**
* Generate value from random generator.
*/
int bdg::core::IntGenerator::generateValue()
{
    return distr->generate(gen);	
}

/**
* Generate value from remote generator.
*/
int bdg::core::IntGenerator::remGenValue()
{
    return remote.generate();
}

void bdg::core::IntGenerator::generate(GenStorer& gs)
{
    gs.store(std::move(bdg::utils::toString(generateValue())));
}
