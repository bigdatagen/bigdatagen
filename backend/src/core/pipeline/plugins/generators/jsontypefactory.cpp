/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file jsontypefactory.cpp
* @category Generator
* @author   Big Data Generator Team
*
*/
	
#include "core/pipeline/plugins/generators/jsontypefactory.h" // header
#include "core/pipeline/plugins/generators/jsonutils.h" 
#include "core/pipeline/plugins/generators/exception.h" 
#include "core/pipeline/plugins/generators/complexgenerator.h" 
#include "core/pipeline/plugins/generators/nonterminal.h"
#include "core/pipeline/plugins/generators/terminal.h"
#include "core/pipeline/plugins/generators/arraygenerator.h"
#include "core/pipeline/plugins/generators/probsetgenerator.h"
#include "core/pipeline/plugins/generators/schemaparser.h"
#include "core/pipeline/plugins/generators/referenceelement.h"
#include "core/pipeline/plugins/generators/timesequencegenerator.h"
#include "core/pipeline/plugins/generators/randtimegenerator.h"
#include "core/pipeline/plugins/generators/domtimegenerator.h"

#include <numeric> // accumulate
#include <iterator> // back_inserter

using std::string;

bdg::core::GrammarElement::ptr_t bdg::core::JsonTypeFactory::createDateTime(
    TypeFactory &tf, 
    const SchemaElement::uptr_t& se, 
    SchemaParser& parser)
{
    JsonSchemaElement* jse = dynamic_cast<JsonSchemaElement*>(se.get());
    return createDateTimeImpl(jse->obj);
}

bdg::core::GrammarElement::ptr_t  bdg::core::JsonTypeFactory::createObject(
    TypeFactory& tf,
    const SchemaElement::uptr_t& se,
    SchemaParser& parser)
{
    JsonSchemaElement* jse = dynamic_cast<JsonSchemaElement*>(se.get());
    return createObjectImpl(parser, tf, jse->obj, parser.allElems, parser.allSeqs, "{", "}");
}

bdg::core::GrammarElement::ptr_t  bdg::core::JsonTypeFactory::createArray(
    TypeFactory& tf,
    const SchemaElement::uptr_t& se,
    SchemaParser& parser)
{
    JsonSchemaElement* jse = dynamic_cast<JsonSchemaElement*>(se.get());
    return createArrayImpl(parser, tf, jse->obj, parser.allElems, parser.allSeqs);
}

bdg::core::GrammarElement::ptr_t bdg::core::JsonTypeFactory::createProbSet(
    TypeFactory& tf, 
    const SchemaElement::uptr_t& se, 
    SchemaParser & parser)
{
    JsonSchemaElement* jse = dynamic_cast<JsonSchemaElement*>(se.get());
    return createProbSetImpl(parser, tf, jse->obj, jse->hasProps, parser.allElems, parser.allSeqs);
}

bdg::core::GrammarElement::ptr_t bdg::core::JsonTypeFactory::createSequence(
    TypeFactory& tf,
    const SchemaElement::uptr_t& se,
    SchemaParser& parser)
{
    (void)tf;
    JsonSchemaElement* jse = dynamic_cast<JsonSchemaElement*>(se.get());
    return createSequenceTypeImpl(jse->obj, parser.allSeqs);
}

bdg::core::GrammarElement::ptr_t bdg::core::JsonTypeFactory::createUnknown(
    TypeFactory& tf,
    const SchemaElement::uptr_t& se, 
    SchemaParser& parser)
{
    JsonSchemaElement* jse = dynamic_cast<JsonSchemaElement*>(se.get());
    return createUnknownImpl(jse->obj, parser);
}

bdg::core::JsonTypeFactory::JsonTypeFactory(const factory_fnc_t& nonFoundFnc, std::vector<name_factory_fnc_t>&& init):
    TypeFactory(nonFoundFnc, std::move(init))
{
}

/**
* Creates date time generator - random and domain type.
*/
inline bdg::core::GrammarElement::ptr_t bdg::core::JsonTypeFactory::createDateTimeImpl(const Poco::JSON::Object::Ptr& jsonObject)
{
    auto vprops = JsonUtils::getPropertyObject(jsonObject, Constants::ITEM_PROPERTIES);
    string format = JsonUtils::getPropertyValue<string>(vprops, Constants::PROPERTY_FORMAT);
    string source = JsonUtils::getPropertyValue<string>(vprops, Constants::PROPERTY_SOURCE);
    if (source == Constants::PROPERTY_RANDOM)
    {
        return GrammarElement::makeGe<RandTimeGenerator>(
            format,
            JsonUtils::getPropertyValue<string>(vprops, Constants::MIN_DATE),
            JsonUtils::getPropertyValue<string>(vprops, Constants::MIN_TIME),
            JsonUtils::getPropertyValue<string>(vprops, Constants::MAX_DATE),
            JsonUtils::getPropertyValue<string>(vprops, Constants::MAX_TIME));
    }
    else if (source == Constants::PROPERTY_DOMAIN)
    {
        auto vdom = JsonUtils::getPropertyArray(vprops, Constants::PROPERTY_DOMAIN);
        
        std::vector<std::pair<string, string>> res;
        res.reserve(vdom->size());
        std::transform(std::begin(*vdom), std::end(*vdom), std::back_inserter(res), [](const Poco::Dynamic::Var& v)
        {
            auto o = JsonUtils::getObject(v);
            return std::make_pair<string, string>(
                JsonUtils::getPropertyValue<string>(o, Constants::DATE),
                JsonUtils::getPropertyValue<string>(o, Constants::TIME)
                );
        });

        return GrammarElement::makeGe<DomTimeGenerator>(format, std::move(res));
    }
    else
        throw Exception(Error::BAD_PROPERTY_VALUE + source + "in dateTime");
}

/**
* Creates json object generator.
*	startDelim = "{", endDelim = "}" ... if we are creating object
*	startDelim = "", endDelim = "" ... if we are creating ProbSet member (can be member of object, is not object itself)
*/
bdg::core::GrammarElement::ptr_t bdg::core::JsonTypeFactory::createObjectImpl(
    SchemaParser& parser,
    TypeFactory& tf,
    Poco::JSON::Object::Ptr const& jsonObject,
    GrammarElement::element_map_t& parsedElements,
    std::vector<DynSeqGenerator*>& seqs,
    const std::string &startDelim,
    const std::string &endDelim)
{
    const auto vprops = JsonUtils::getPropertyArray(jsonObject, Constants::ITEM_PROPERTIES);

    // Parse json.
    std::vector<ComplexGenerator::prop_elem_t> pes;
    // Array of probability sets.
    std::vector<GrammarElement::ptr_t> probSets;
    for (const auto& vobj : *vprops)
    {
        const auto obj = JsonUtils::getObject(vobj);
        const std::string type = JsonUtils::getPropertyValue<std::string>(obj, Constants::ITEM_TYPE);

        GrammarElement::ptr_t newGe = parser.createElement<false>(
            tf,
            std::unique_ptr<SchemaElement>(new JsonSchemaElement(obj, true)),
            parsedElements,
            seqs);

        std::string propName;
        if (type == Constants::ITEM_TYPE_PROBSET)
        {
            // Probability set has property name inside and it handles it on its own.
            probSets.push_back(newGe);
        }
        else
        {
            propName = JsonUtils::getPropertyValue<std::string>(obj, Constants::ITEM_PROPERTY_NAME);
            
        }

        pes.emplace_back(propName, newGe);
    }

    static const std::string delim = ",";
    // Create object.
    // Decorated prints properties except for probsets that handles it inside. It's ugly :(
    return GrammarElement::makeGe<ComplexGenerator>(
        pes, 
        startDelim, 
        endDelim, 
        delim,
        [probSets](const ComplexGenerator::prop_elem_t& pe)//->GrammarElement::ptr_t
        {
            // It is not probabily set.
            if (std::find(std::cbegin(probSets), std::cend(probSets), pe.second) == std::cend(probSets))
            {
                return GrammarElement::makeGe<bdg::core::Nonterminal>(
                    std::vector<GrammarElement::ptr_t>{ GrammarElement::makeGe<Terminal>("\"" + pe.first + "\":"), pe.second});
            }
            else
            {
                // Let probabily set decorates on its own.
                return pe.second;
            }
        });
}

bdg::core::GrammarElement::ptr_t bdg::core::JsonTypeFactory::createArrayImpl(
    SchemaParser& parser,
    TypeFactory& tf,
    Poco::JSON::Object::Ptr const& jsonObject,
    GrammarElement::element_map_t& parsedElements,
    std::vector<DynSeqGenerator*>& seqs)
{
    auto vprops = JsonUtils::getPropertyObject(jsonObject, Constants::ITEM_PROPERTIES);
    auto arr = JsonUtils::getPropertyArray(vprops, Constants::PROPERTY_INNER_TYPE);

    std::vector<GrammarElement::ptr_t> ges;
    for (const auto& vobj : *arr)
    {
        ges.push_back(parser.createElement<false>(
            tf,
            std::unique_ptr<SchemaElement>(new JsonSchemaElement(JsonUtils::getObject(vobj))),
            parsedElements,
            seqs));
        
    }

    // todo: Parse domain values here.

    const int minLen = JsonUtils::getPropertyValue<int>(JsonUtils::getObject(vprops), Constants::PROPERTY_MIN, true);
    const int maxLen = JsonUtils::getPropertyValue<int>(JsonUtils::getObject(vprops), Constants::PROPERTY_MAX, true);
	const std::string distr = JsonUtils::getPropertyValue<std::string>(vprops, Constants::PROPERTY_DISTRIBUTION);

    return GrammarElement::makeGe<ArrayGenerator>(minLen, maxLen, std::move(ges), distr);
}

/**
* Creates probability set generator.
*/
bdg::core::GrammarElement::ptr_t bdg::core::JsonTypeFactory::createProbSetImpl(
    SchemaParser& parser,
    TypeFactory& tf,
    Poco::JSON::Object::Ptr const& jsonObject,
    bool hasProps,
    GrammarElement::element_map_t& parsedElements,
    std::vector<DynSeqGenerator*>& seqs)
{
    auto varr = JsonUtils::getPropertyArray(jsonObject, Constants::ITEM_PROPERTIES);

    std::vector<GrammarElement::ptr_t> elems;
    elems.reserve(varr->size());
    std::vector<int> probs;
    probs.reserve(varr->size());

    std::transform(
        varr->begin(),
        varr->end(),
        std::back_inserter(elems),
        [&parser, &tf, &parsedElements, &seqs, &probs, hasProps](const Poco::Dynamic::Var& vo)
        {
            const auto vobj = JsonUtils::getObject(vo);

            probs.push_back(JsonUtils::getPropertyValue<int>(vobj, Constants::PROPERTY_VALUE, true));
            auto vprops = JsonUtils::getPropertyArray(vobj, Constants::ITEM_PROPERTIES);

            // Grammar Element and it propertyNames (if needed).
            std::vector<GrammarElement::ptr_t> ges;
            const size_t vpropsSize = vprops->size();
            
            // If has has empty probability set inner elements.
            if (vpropsSize == 0)
            {
                // Insert empty dummy.
                return GrammarElement::makeGe<Terminal>("");
            }
            else
            {
                ges.reserve(vpropsSize);
                size_t i = 0;
                std::transform(vprops->begin(), vprops->end(), std::back_inserter(ges),
                    [&parser, &tf, &parsedElements, &seqs, &i, vpropsSize, hasProps](const Poco::Dynamic::Var& v)
                {
                    const auto vo = JsonUtils::getObject(v);
                    const std::string t = JsonUtils::getPropertyValue<std::string>(vo, Constants::ITEM_TYPE);

                    Nonterminal* res = new Nonterminal();

                    // InnerType is not probability set and it has property name.
                    // If it's probset we have to let probset handle that on its own.
                    if (hasProps && t != Constants::ITEM_TYPE_PROBSET)
                    {
                        // Insert property name.
                        const std::string propName = JsonUtils::getPropertyValue<std::string>(vo, Constants::ITEM_PROPERTY_NAME);
                        res->push_back(GrammarElement::makeGe<Terminal>("\"" + propName + "\":"));
                    }

                    // Insert real element.
                    res->push_back(parser.createElement<false>(
                        tf,
                        std::unique_ptr<SchemaElement>(new JsonSchemaElement(JsonUtils::getObject(v), hasProps)),
                        parsedElements,
                        seqs));

                    // If not last property and element is not empty probability set.
                    if (i + 1 < vpropsSize)
                    {
                        static const std::string SEP = ",";
                        // Have to insert separator between elmeents.
                        res->push_back(GrammarElement::makeGe<Terminal>(SEP));
                    }

                    ++i;
                    return GrammarElement::ptr_t(res);
                });

                return GrammarElement::makeGe<Nonterminal>(std::move(ges));
            }
        }
    );

    if (std::accumulate(std::cbegin(probs), std::cend(probs), 0) != 100)
        throw Exception(Error::INVALID_PROB_SUM);

    return GrammarElement::makeGe<ProbSetGenerator>(std::move(elems), std::move(probs));
}

bdg::core::GrammarElement::ptr_t bdg::core::JsonTypeFactory::createSequenceTypeImpl(
    const Poco::JSON::Object::Ptr& jsonObject,
    std::vector<bdg::core::DynSeqGenerator*>& seqs)
{
    using std::string;

    const auto vprops = JsonUtils::getPropertyObject(jsonObject, Constants::ITEM_PROPERTIES);
    const std::string visibility = JsonUtils::getPropertyValue<string>(vprops, Constants::PROPERTY_VISIBILITY);
    bool locVisibility;
    if (visibility == Constants::PROP_VISIBILITY_LOCAL)
        locVisibility = true;
    else if (visibility == Constants::PROP_VISIBILITY_GLOBAL)
        locVisibility = false;
    else
        throw Exception(bdg::core::Error::BAD_PROPERTY_VALUE + Constants::PROPERTY_VISIBILITY);

    
    const string innerType = JsonUtils::getPropertyValue<string>(jsonObject, Constants::PROPERTY_INNER_TYPE);
    if (innerType == Constants::ITEM_TYPE_DATETIME)
    {
        return GrammarElement::makeGe<TimeSeqGenerator>(
            JsonUtils::getPropertyValue<std::string>(vprops, Constants::PROPERTY_INIT_DATE),
            JsonUtils::getPropertyValue<std::string>(vprops, Constants::PROPERTY_INIT_TIME),
            JsonUtils::getPropertyValue<std::string>(vprops, Constants::PROPERTY_STEP),
            JsonUtils::getPropertyValue<std::string>(vprops, Constants::PROPERTY_FORMAT),
            locVisibility);
    }
    else
    {
        const string value = JsonUtils::getPropertyValue<string>(vprops, Constants::PROPERTY_VALUE);
        if (innerType == Constants::ITEM_TYPE_INT)
        {
            return GrammarElement::ptr_t(createSeq<int>(vprops, seqs, value, locVisibility));
        }
        else if (innerType == Constants::ITEM_TYPE_FLOAT)
        {
            return GrammarElement::ptr_t(createSeq<double>(vprops, seqs, value, locVisibility));
        }
        else if (innerType == Constants::ITEM_TYPE_STRING)
        {
            return GrammarElement::ptr_t(createSeq<string>(vprops, seqs, value, locVisibility));
        }
        else
            throw Exception(bdg::core::Error::BAD_PROPERTY_VALUE + innerType);
    }
}

/**
* Factory method that is called if unknown type was found.
* Creates Reference element.
*/
bdg::core::GrammarElement::ptr_t bdg::core::JsonTypeFactory::createUnknownImpl(
    const Poco::JSON::Object::Ptr& jsonObject, 
    SchemaParser& parser)
{
    const std::string type = JsonUtils::getPropertyValue<std::string>(jsonObject, Constants::ITEM_TYPE);
    
    auto const& elementIt = parser.allElems.find(type);
    std::shared_ptr<GrammarElement> res;
    if (elementIt == parser.allElems.cend())
    {
        res = GrammarElement::makeGe<ReferenceElement>(type);
    }
    else
    {
        res = elementIt->second;
    }

    return res;
}
