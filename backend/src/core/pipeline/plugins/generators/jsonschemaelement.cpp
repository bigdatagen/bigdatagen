/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file jsonschemaelement.cpp
* @category Generator
* @author   Big Data Generator Team
*
*/
	
#include "core/pipeline/plugins/generators/jsonschemaelement.h" // header
#include "core/pipeline/plugins/generators/constants.h" 
#include "core/pipeline/plugins/generators/jsonutils.h"

bdg::core::JsonSchemaElement::JsonSchemaElement(const Poco::JSON::Object::Ptr& obj, bool hasProps):
    obj(obj),
    hasProps(hasProps)
{
}

std::string bdg::core::JsonSchemaElement::getTypeName() const
{
    return obj->has(Constants::ITEM_TYPE_NAME) ?
        JsonUtils::getPropertyValue<std::string>(obj, Constants::ITEM_TYPE_NAME) : "";
}

std::string bdg::core::JsonSchemaElement::getType() const
{
    return JsonUtils::getPropertyValue<std::string>(obj, Constants::ITEM_TYPE);
}