/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file complexgenerator.cpp
* @category Generator
* @author   Big Data Generator Team
*
*/
	
#include "core/pipeline/plugins/generators/complexgenerator.h" // header
#include "core/pipeline/plugins/generators/genstorer.h" // GenStorer
#include "core/pipeline/plugins/generators/terminal.h" // Terminal
#include "core/pipeline/plugins/generators/exception.h" // Exception

#include <algorithm> // transform
#include <iterator> // back_inserter

/**
* Assemblies complex generator from nonterminals and terminals.
*/
bdg::core::ComplexGenerator::ComplexGenerator(
    const std::vector<prop_elem_t>& propElems,
    const std::string& startDelim,
    const std::string& endDelim,
    const std::string& delim,
    const std::function<GrammarElement::ptr_t(const prop_elem_t&)>& dec)
{
    std::vector<GrammarElement::ptr_t> elems;
    // +1 for start delimiter. End is inserted together after last element.
    elems.reserve(propElems.size() + 1);

    elems.push_back(GrammarElement::makeGe<Terminal>(startDelim));

    if (propElems.empty())
        throw Exception(Error::EMPTY_PROPERTIES);

    // cend - 1 ~ Before last we don't want to insert delimiter.
    std::transform(
        propElems.cbegin(),
        propElems.cend() - 1,
        std::back_inserter(elems),
        [&dec, &delim](const prop_elem_t& pe)
    {
        return GrammarElement::makeGe<Nonterminal>(
            std::vector<GrammarElement::ptr_t>{ dec(pe), GrammarElement::makeGe<Terminal>(delim) });
    });

    // Insert last element together with end delimiter.
    elems.push_back(GrammarElement::makeGe<Nonterminal>(
        std::vector<GrammarElement::ptr_t>{ dec(*propElems.rbegin()), GrammarElement::makeGe<Terminal>(endDelim) }));

    move(std::move(elems));
}

/**
* Empty decorater does nothing.
*/
bdg::core::GrammarElement::ptr_t bdg::core::ComplexGenerator::emptyDec(const prop_elem_t& pe)
{
    return pe.second;
}
