/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file grammarschemagenparser.cpp
* @category Generators
* @author   Big Data Generator Team
*/


#include "core/pipeline/plugins/generators/grammarschemaparser/grammarschemaparser.h" // header
#include "core/pipeline/plugins/generators/grammarschemaparser/grammartypefactory.h"
#include "core/pipeline/plugins/generators/grammarschemaelement.h"

#include <Poco/Dynamic/Var.h>
#include <Poco/JSON/Parser.h>

bdg::core::GrammarSchemaParser::GrammarSchemaParser(Generator& gen,
    const TypeFactory::factory_fnc_t& createUnknown,
    std::vector<TypeFactory::name_factory_fnc_t>&& factoryMethods) :
SchemaParser(gen, TypeFactory::instance<GrammarTypeFactory>(
    createUnknown,
    std::move(factoryMethods)))
{
}

void bdg::core::GrammarSchemaParser::parseSchema(const std::string & schema)
{
    using namespace Poco;
    using namespace Poco::JSON;

    try
    {
        parsedSchema = Poco::JSON::Parser().parse(schema);
    }
    catch (std::exception &ex)
    {
        throw Exception(Error::INVALID_JSON + ex.what());
    }

    rootArray = JsonUtils::getArray(parsedSchema);
    nextIdx = 0;
}

bool bdg::core::GrammarSchemaParser::hasNextUdt() const
{
    return nextIdx < rootArray->size();
}

bdg::core::SchemaElement::uptr_t bdg::core::GrammarSchemaParser::getNextUdt()
{
    return SchemaElement::uptr_t(new GrammarSchemaElement(JsonUtils::getObject(rootArray->get(nextIdx++)), true));
}
