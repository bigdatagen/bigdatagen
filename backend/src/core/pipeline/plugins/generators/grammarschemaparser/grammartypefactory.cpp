/**
*
* @file grammartypefactory.cpp
*
*/
	
#include "core/pipeline/plugins/generators/grammarschemaparser/grammartypefactory.h" // header
#include "core/pipeline/plugins/generators/schemaparser.h"
#include "core/pipeline/plugins/generators/grammarschemaelement.h"
#include "core/pipeline/plugins/generators/jsonutils.h"
#include "core/pipeline/plugins/generators/arraygenerator.h"
#include "core/pipeline/plugins/generators/probsetgenerator.h"
#include "core/pipeline/plugins/generators/terminal.h"
#include "core/pipeline/plugins/generators/probcycle.h"


#include <algorithm> // transform
#include <iterator> // back_inserter

using std::string;

bdg::core::GrammarElement::ptr_t bdg::core::GrammarTypeFactory::createGrammar(
    TypeFactory& tf,
    const SchemaElement::uptr_t& se,
    SchemaParser & parser)
{
    GrammarSchemaElement* jse = dynamic_cast<GrammarSchemaElement*>(se.get());
    return createGrammarImpl(parser, tf, jse->obj, parser.allElems, parser.allSeqs);
}

bdg::core::GrammarElement::ptr_t bdg::core::GrammarTypeFactory::createComplex(
    TypeFactory& tf, 
    const SchemaElement::uptr_t& se, 
    SchemaParser & parser)
{
    GrammarSchemaElement* jse = dynamic_cast<GrammarSchemaElement*>(se.get());
    return createComplexImpl(parser, tf, jse->obj, parser.allElems, parser.allSeqs);
}

bdg::core::GrammarElement::ptr_t bdg::core::GrammarTypeFactory::createCycle(TypeFactory & tf, const SchemaElement::uptr_t & se, SchemaParser & parser)
{
    GrammarSchemaElement* jse = dynamic_cast<GrammarSchemaElement*>(se.get());
    return createCycleImpl(parser, tf, jse->obj, parser.allElems, parser.allSeqs);
}

bdg::core::GrammarElement::ptr_t bdg::core::GrammarTypeFactory::createAlternation(TypeFactory & tf, const SchemaElement::uptr_t & se, SchemaParser & parser)
{
    GrammarSchemaElement* jse = dynamic_cast<GrammarSchemaElement*>(se.get());
    return createAlternationImpl(parser, tf, jse->obj, parser.allElems, parser.allSeqs);
}

bdg::core::GrammarElement::ptr_t bdg::core::GrammarTypeFactory::createString(TypeFactory & tf, const SchemaElement::uptr_t & se, SchemaParser & parser)
{
    GrammarSchemaElement* jse = dynamic_cast<GrammarSchemaElement*>(se.get());
    return createStringImpl(parser, tf, jse->obj, parser.allElems, parser.allSeqs);
}

bdg::core::GrammarElement::ptr_t bdg::core::GrammarTypeFactory::createWhiteSpace(
    TypeFactory& tf, 
    const SchemaElement::uptr_t & se, 
    SchemaParser& parser)
{
    GrammarSchemaElement* jse = dynamic_cast<GrammarSchemaElement*>(se.get());
    return createWsImpl(parser, tf, jse->obj, parser.allElems, parser.allSeqs);
}

bdg::core::GrammarElement::ptr_t bdg::core::GrammarTypeFactory::createEmpty(TypeFactory & tf, const SchemaElement::uptr_t & se, SchemaParser & parser)
{
    GrammarSchemaElement* jse = dynamic_cast<GrammarSchemaElement*>(se.get());
    return createEmptyImpl(parser, tf, jse->obj, parser.allElems, parser.allSeqs);
}

bdg::core::GrammarTypeFactory::GrammarTypeFactory(const factory_fnc_t& nonFoundFnc, std::vector<name_factory_fnc_t>&& init):
    JsonTypeFactory(nonFoundFnc, std::move(init))
{
}

bdg::core::GrammarElement::ptr_t bdg::core::GrammarTypeFactory::createGrammarImpl(
    SchemaParser& parser,
    TypeFactory& tf,
    const Poco::JSON::Object::Ptr& jsonObject,
    GrammarElement::element_map_t& parsedElements,
    std::vector<DynSeqGenerator*>& seqs)
{
    auto vdef = JsonUtils::getPropertyObject(jsonObject, Constants::ITEM_DEFINITION);
    // Insert it into nonterminal.
    // This type is udt so can't create another.
    return GrammarElement::makeGe<Nonterminal>(parser.createElement<false>(
        tf,
        SchemaElement::uptr_t(new GrammarSchemaElement(vdef)),
        parsedElements,
        seqs));
}

bdg::core::GrammarElement::ptr_t bdg::core::GrammarTypeFactory::createComplexImpl(
    SchemaParser& parser, 
    TypeFactory& tf, 
    const Poco::JSON::Object::Ptr& jsonObject, 
    GrammarElement::element_map_t& parsedElements, 
    std::vector<DynSeqGenerator*>& seqs)
{
    auto vchildren = JsonUtils::getPropertyArray(jsonObject, Constants::ITEM_CHILDREN);
    std::vector<GrammarElement::ptr_t> ges;
    ges.reserve(vchildren->size());
    std::transform(std::begin(*vchildren), std::end(*vchildren), std::back_inserter(ges), 
        [&parser, &tf, &parsedElements, &seqs](const Poco::Dynamic::Var& v)
        {
            return parser.createElement<false>(
                tf,
                SchemaElement::uptr_t(new GrammarSchemaElement(JsonUtils::getObject(v))),
                parsedElements,
                seqs);
        });

    return GrammarElement::makeGe<Nonterminal>(std::move(ges));
}

bdg::core::GrammarElement::ptr_t bdg::core::GrammarTypeFactory::createCycleImpl(
    SchemaParser& parser,
    TypeFactory& tf,
    const Poco::JSON::Object::Ptr& jsonObject,
    GrammarElement::element_map_t& parsedElements,
    std::vector<DynSeqGenerator*>& seqs)
{
    auto ge = parser.createElement<false>(
        tf,
        SchemaElement::uptr_t(new GrammarSchemaElement(JsonUtils::getPropertyObject(jsonObject, Constants::PROPERTY_INNER_ELEMENT))),
        parsedElements,
        seqs);

    string cycleType = JsonUtils::getPropertyValue<string>(jsonObject, Constants::PROPERTY_CYCLE_TYPE);
    if (cycleType == Constants::PROP_CYCLE_REP)
    {
        int cMin = JsonUtils::getPropertyValue<int>(jsonObject, Constants::PROP_CYCLE_MIN);
        int cMax = JsonUtils::getPropertyValue<int>(jsonObject, Constants::PROP_CYCLE_MAX);
        return GrammarElement::makeGe<ArrayGenerator>(
            cMin,
            cMax,
            std::vector<GrammarElement::ptr_t>{ge},
            Constants::DISTR_NORMAL,
            Constants::EMPTY,
            Constants::EMPTY,
            Constants::EMPTY);
    }
    else if (cycleType == Constants::PROP_PROBABILITY)
    {
        int cycleProb = JsonUtils::getPropertyValue<int>(jsonObject, Constants::PROP_CYCLE_PROB);
        return GrammarElement::makeGe<ProbCycle>(std::move(ge), cycleProb);
    }
    else
        throw Exception(Error::BAD_PROPERTY_VALUE + "cycleType (" + cycleType + ")");
}

bdg::core::GrammarElement::ptr_t bdg::core::GrammarTypeFactory::createAlternationImpl(
    SchemaParser& parser,
    TypeFactory& tf,
    const Poco::JSON::Object::Ptr& jsonObject,
    GrammarElement::element_map_t& parsedElements,
    std::vector<DynSeqGenerator*>& seqs)
{
    auto vchildren = JsonUtils::getPropertyArray(jsonObject, Constants::ITEM_CHILDREN);
    std::vector<GrammarElement::ptr_t> ges;
    ges.reserve(vchildren->size());
    std::vector<int> probs;
    probs.reserve(vchildren->size());
    std::transform(std::begin(*vchildren), std::end(*vchildren), std::back_inserter(ges),
        [&parser, &tf, &parsedElements, &seqs, &probs](const Poco::Dynamic::Var& v)
        {
            probs.push_back(JsonUtils::getPropertyValue<int>(JsonUtils::getObject(v), Constants::PROP_PROBABILITY));
            return parser.createElement<false>(
                tf,
                SchemaElement::uptr_t(new GrammarSchemaElement(JsonUtils::getObject(v))),
                parsedElements,
                seqs);
        });

    return GrammarElement::makeGe<ProbSetGenerator>(std::move(ges), std::move(probs));
}

bdg::core::GrammarElement::ptr_t bdg::core::GrammarTypeFactory::createStringImpl(
    SchemaParser& parser,
    TypeFactory& tf,
    const Poco::JSON::Object::Ptr& jsonObject,
    GrammarElement::element_map_t& parsedElements,
    std::vector<DynSeqGenerator*>& seqs)
{
    const auto vprops = JsonUtils::getPropertyObject(jsonObject, Constants::ITEM_PROPERTIES);
    const std::string distr = JsonUtils::getPropertyValue<std::string>(vprops, Constants::PROPERTY_DISTRIBUTION);
    const std::string source = JsonUtils::getPropertyValue<std::string>(vprops, Constants::PROPERTY_SOURCE);

    if (source == Constants::PROPERTY_DOMAIN)
    {
        const auto arrayPtr = JsonUtils::getPropertyArray(vprops, Constants::PROPERTY_DOMAIN);
        return GrammarElement::makeGe<DomainGenerator<std::string>>(JsonUtils::getArraySimpleValues<std::string>(arrayPtr), distr, false);
    }
    else if (source == Constants::PROPERTY_RANDOM)
    {
        using LimitT = GenInfo<std::string>::limit_t;

        return ElementFactory<std::string>::create(
            JsonUtils::getPropertyValue<LimitT>(vprops, Constants::PROPERTY_MIN),
            JsonUtils::getPropertyValue<LimitT>(vprops, Constants::PROPERTY_MAX),
            distr,
            false);
    }
    else
    {
        throw Exception(Error::BAD_PROPERTY_VALUE + source);
    }
}

bdg::core::GrammarElement::ptr_t bdg::core::GrammarTypeFactory::createWsImpl(
    SchemaParser& parser,
    TypeFactory& tf,
    const Poco::JSON::Object::Ptr& jsonObject,
    GrammarElement::element_map_t& parsedElements,
    std::vector<DynSeqGenerator*>& seqs)
{
    string c = JsonUtils::getPropertyValue<string>(
        JsonUtils::getPropertyObject(jsonObject, Constants::ITEM_PROPERTIES),
        Constants::PROPERTY_CHAR);

    static const string TAB = "\t";
    static const string SPACE = " ";

    if (c == Constants::PROPERTY_CHAR_SPACE)
    {
        return GrammarElement::makeGe<Terminal>(SPACE);
    }
    else if (c == Constants::PROPERTY_CHAR_TAB)
    {
        return GrammarElement::makeGe<Terminal>(TAB);
    }
    else if (c == Constants::PROPERTY_CHAR_NEW_LINE)
    {
        // Multiplatform new line.
        std::stringstream ss;
        ss << std::endl;
        return GrammarElement::makeGe<Terminal>(ss.str());
    }
    else
        throw Exception(Error::BAD_PROPERTY_VALUE + "char in whiteSpace type (" + c + ").");
}

bdg::core::GrammarElement::ptr_t bdg::core::GrammarTypeFactory::createEmptyImpl(
    SchemaParser& parser,
    TypeFactory& tf,
    const Poco::JSON::Object::Ptr& jsonObject,
    GrammarElement::element_map_t& parsedElements,
    std::vector<DynSeqGenerator*>& seqs)
{
    // Return simply empty terminal.
    return GrammarElement::makeGe<Terminal>(Constants::EMPTY);
}