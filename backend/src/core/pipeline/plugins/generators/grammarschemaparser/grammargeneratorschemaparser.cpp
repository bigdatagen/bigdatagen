/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file grammarschemagenparser.cpp
* @category Generators
* @author   Big Data Generator Team
*/


#include "core/pipeline/plugins/generators/grammarschemaparser/grammargeneratorschemaparser.h" // header
#include "core/pipeline/plugins/generators/grammarschemaparser/grammartypefactory.h"

#include <Poco/Dynamic/Var.h>
#include <Poco/JSON/Parser.h>

bdg::core::GrammarGeneratorSchemaParser::GrammarGeneratorSchemaParser(Generator& gen):
    GrammarSchemaParser(
        gen, 
        GrammarTypeFactory::createUnknown,
        std::vector<GrammarTypeFactory::name_factory_fnc_t>{
            { Constants::ITEM_TYPE_GRAMMAR, GrammarTypeFactory::createGrammar },
            { Constants::ITEM_TYPE_ALT, GrammarTypeFactory::createAlternation },
            { Constants::ITEM_TYPE_COMPLEX, GrammarTypeFactory::createComplex },
            { Constants::ITEM_TYPE_CYCLE, GrammarTypeFactory::createCycle },
            { Constants::ITEM_TYPE_SEQUENCE, GrammarTypeFactory::createSequence },
            { Constants::ITEM_TYPE_INT, GrammarTypeFactory::createSimpleType<int> },
            { Constants::ITEM_TYPE_FLOAT, GrammarTypeFactory::createSimpleType<double> },
            { Constants::ITEM_TYPE_STRING, GrammarTypeFactory::createString },
            { Constants::ITEM_TYPE_DATETIME, GrammarTypeFactory::createDateTime },
            { Constants::ITEM_TYPE_WHITE_SPACE, GrammarTypeFactory::createWhiteSpace },
            { Constants::ITEM_TYPE_EMPTY, GrammarTypeFactory::createEmpty }
        })
{
}
