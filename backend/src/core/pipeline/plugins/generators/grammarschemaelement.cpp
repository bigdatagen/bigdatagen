/**
*
* @file grammarschemaelement.cpp
*
*/
	
#include "core/pipeline/plugins/generators/grammarschemaelement.h" // header
#include "core/pipeline/plugins/generators/constants.h" 
#include "core/pipeline/plugins/generators/jsonutils.h"

bdg::core::GrammarSchemaElement::GrammarSchemaElement(
    const Poco::JSON::Object::Ptr& obj, 
    bool hasTypeName):
JsonSchemaElement(obj),
typeName(hasTypeName ? JsonUtils::getPropertyValue<std::string>(obj, Constants::ITEM_TYPE_NAME) : "")
{
}

/*
bdg::core::GrammarSchemaElement::GrammarSchemaElement(
    const Poco::JSON::Object::Ptr& obj, 
    const std::string& typeName):
JsonSchemaElement(obj),
typeName(typeName)
{
}
*/

std::string bdg::core::GrammarSchemaElement::getTypeName() const
{
    return typeName;
}

std::string bdg::core::GrammarSchemaElement::getType() const
{
    return JsonUtils::getPropertyValue<std::string>(obj, Constants::ITEM_TYPE);
}