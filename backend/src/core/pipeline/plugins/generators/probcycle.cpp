/**
*
* @file grammarelement.h
*
*/

#include "core/pipeline/plugins/generators/probcycle.h" // header
#include "core/pipeline/plugins/generators/grammarelement.h" // grammarElement
#include "core/pipeline/plugins/generators/jsonschemaparser/jsonschemaparser.h" // JsonParser
#include "core/pipeline/plugins/generators/terminal.h" // Terminal
#include "core/pipeline/plugins/generators/dyngenerator.h" // DynSeqGenerator

bdg::core::ProbCycle::ProbCycle(const GrammarElement::ptr_t& elem, size_t cycleProb):
    Cycle<ProbCycle>(std::vector<GrammarElement::ptr_t>{elem}, "", "", ""),
    sizeGen(0, 100),
    cycleProb(cycleProb)
{
}

bdg::core::ProbCycle::ProbCycle(GrammarElement::ptr_t&& elem, size_t cycleProb):
    Cycle<ProbCycle>(std::vector<GrammarElement::ptr_t>{std::move(elem)}, "", "", ""),
    sizeGen(0, 100),
    cycleProb(cycleProb)
{
}

void bdg::core::ProbCycle::initLoop()
{
    if (sizeGen.isRemote())
    {
        gen = [this]() { return sizeGen.remGenValue(); };
    }
    else
    {
        gen = [this]() { return sizeGen.generateValue(); };
    }
}

bool bdg::core::ProbCycle::isEnd()
{
    // if in (0, cycleProb) than we cycle.
    return gen() >= cycleProb;
}

void bdg::core::ProbCycle::inc()
{
    // Do nothing.
}

void bdg::core::ProbCycle::repairImpl(
    const bdg::core::GrammarElement::element_map_t& parsedEls,
    const bdg::core::GrammarElement::seq_vector_t& locSeqs,
    bdg::core::GrammarElement::intGenTypeFnc_vec_t& locGens,
    GrammarElement::intGenTypeFnc_vec_t& remGens,
    std::vector<std::vector<GrammarElement::genIdxTypeFnc_t>>& remGensGens,
    std::vector<GrammarElement::GlobSeq>& globSeqs,
    std::vector<const GrammarElement*>& pathEls)
{
    const int cp = cycleProb;
    for (auto& el : elements)
    {
        locGens.emplace_back(
            &sizeGen, 
            GrammarElement::IntGenType::CONTINUOUS, 
            CountTransFtor(
                [](const CountTransFtor& refThis, int genVal)
                {
                    // iarg1 = cp;

                    // Return 1 if can continue cycle, otherwise 0.
                    return genVal < refThis.iarg1;
                }, 
                cp));

        el->repair(parsedEls, locSeqs, locGens, remGens, remGensGens, globSeqs, pathEls);
        locGens.pop_back();
    }
}
