/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Generator
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/generators/csvgenerator/csvgen.h"      // Header
#include "core/pipeline/plugins/generators/csvgenerator/intgenerator.h"   // intgenerator

#include <algorithm> // transform
#include <numeric> // accumulate
#include <iterator> // back_inserter
#include <vector>  // vector

void bdg::core::CsvGen::setMaster()
{
    isMaster = true;
}

void bdg::core::CsvGen::openStorage(const std::string& fileName)
{
    assert(!isMaster);

    genStorer.open(fileName);
}

void bdg::core::CsvGen::closeStorage()
{
    assert(!isMaster);

    genStorer.close();
}

void bdg::core::CsvGen::generate()
{
    assert(!isMaster);

    for (auto& gFn : preGenFncs)
    {
        gFn(genStorer);
    }

	for (auto &genFn : genFncs)
	{
		genFn(genStorer);
	}
}

void bdg::core::CsvGen::setFilesCount(std::size_t fcount)
{
    assert(isMaster);

    genMult = fcount;
}

const std::vector<int>& bdg::core::CsvGen::genRemInts()
{
    assert(isMaster);

	std::transform(remIntGens.cbegin(), remIntGens.cend(), std::begin(remIntVals), [this](IntGenerator* ig)
    {   
        std::vector<int> res(genMult); 
        std::generate(std::begin(res), std::end(res), [ig]()
        {   
            return ig->generateValue();
        });
        
        return std::move(res);
    });
    
    return remIntVals;

}

void bdg::core::CsvGen::regRemInts(const std::vector<int>& remInts)
{
    assert(!isMaster);
    assert(remInts.size() == remIntGens.size());

    for (size_t i = 0; i < remInts.size(); i++)
    {
        remIntGens[i]->regData(remInts[i]);
    }
}

const std::vector<std::size_t>& bdg::core::CsvGen::genSeqsStart()
{
    assert(isMaster);

    std::swap(lastSeqsIdx, seqsIdx);

    // Add to each sequence index last value of generator.
    std::transform(
        globSeqs.cbegin(),
        globSeqs.cend(),
        std::begin(lastSeqsIdx),
        std::begin(seqsIdx),
        [this](const GrammarElement::GlobSeq& gs, std::size_t sIdx)
        {
            // Start with last idx + number of multiplication * simple count.
            int res = sIdx + genMult * gs.simpleCount;
            // Generated genMult generation of random numbers for each generator.
            for (size_t i = 0; i < genMult; i++)
            {
                res += std::accumulate(gs.depGens.cbegin(), gs.depGens.cend(), 1, [this, i](int mul, std::size_t genIdx)
                {
                    // Go through all dependant generators on current generator.
                    do
                    {
                        mul *= remIntVals[genIdx][i];
                        genIdx = remGensGen[genIdx];
                    } while (genIdx != remGensGen[genIdx]);

                    return mul;
                });
            }

            return res;
        });

    return lastSeqsIdx;

}

void bdg::core::CsvGen::setSeqsStart(const std::vector<std::size_t>& startIdcs)
{
    assert(!isMaster);

    for (size_t i = 0; i < globSeqs.size(); i++)
    {
        globSeqs[i].seq->setStart(startIdcs[i]);
    }
}
