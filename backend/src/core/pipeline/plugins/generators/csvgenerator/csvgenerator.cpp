/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Generator
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/generators/csvgenerator/csvgenerator.h"        // Header

/**
 * Constructor of the plugin - registers all its settings.
 */
bdg::core::pipeline::plugin::CsvGenerator::CsvGenerator() :
    Generator("CsvGenerator", "Plugin which generates CSV files according to a given schema", "csv")
{
	// Nothing here.
}

using bdg::core::pipeline::plugin::Plugin;
using bdg::core::pipeline::plugin::CsvGenerator;

/**
 * Export the plugin for the POCO class loader.
 */
POCO_BEGIN_MANIFEST(Plugin)
    POCO_EXPORT_CLASS(CsvGenerator)
POCO_END_MANIFEST
