/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file probsetgeneratr.cpp
* @category Generator
* @author   Big Data Generator Team
*
*/
	
#include "core/pipeline/plugins/generators/probsetgenerator.h" // header
#include "core/pipeline/plugins/generators/grammarelement.h" // grammarElement
#include "core/pipeline/plugins/generators/doublegenerator.h" // DoubleGenerator
#include "core/pipeline/plugins/generators/dyngenerator.h" // DynSeqGenerator
#include "core/pipeline/plugins/generators/complexgenerator.h" // ComplexGenerator

#include "core/pipeline/plugins/generators/constants.h" // Constants
#include "core/pipeline/plugins/generators/jsonutils.h" // JsonUtils

#include <memory> // shared_ptr
#include <numeric> // partial_sum
#include <iterator> // back_inserter

bdg::core::ProbSetGenerator::ProbSetGenerator(const std::vector<GrammarElement::ptr_t>& els, const std::vector<int>& probs):
    Nonterminal(els),
    probs(transformProbs(probs)),
    chooser(0, 100)
{
}

bdg::core::ProbSetGenerator::ProbSetGenerator(std::vector<GrammarElement::ptr_t>&& els, std::vector<int>&& probs):
    Nonterminal(els),
    probs(transformProbs(probs)),
    chooser(0, 100)
{
}

/**
* Registers generator. At start it insert function that generates index of 
* chosen branch. Generators for each nonterminal are stored in global array
* and class remmebers starting indeces for each of them. After generation
* we set next index to proper nonterminal generator function starts and after that
* we jump after all nonterminals.
*/
void
bdg::core::ProbSetGenerator::registerGeneratorImpl(
	std::size_t& currIdx,
	std::vector<GrammarElement::gen_fn_t> &preGenFncs,
	std::vector<GrammarElement::gen_fn_t> &genFncs)
{
    assert(size() > 0);

    ++currIdx;
    // Set "header" function that generates chooser.
    if (chooser.isRemote())
    {
        genFncs.push_back([this, currIdx](GenStorer& gs)
        {
            prob = chooser.remGenValue();
            return currIdx;
        });
    }
    else
    {
        genFncs.push_back([this, currIdx](GenStorer& gs)
        {
            prob = chooser.generateValue();
            return currIdx;
        });
    }

    ++currIdx;
    // Placeholder.
    const size_t nonStart = genFncs.size();
    genFncs.push_back([](GenStorer& gs)
    {
        (void)gs;
        assert(false);
        return static_cast<size_t>(-1);
    });

    // Indices for each nonterminal - i-th is <i, i+1).
    nonIdcs.reserve(size() + 1);
    for (size_t i = 0; i < size(); i++)
    {
        nonIdcs.push_back(currIdx);
        get(i)->registerGenerator(currIdx, preGenFncs, genFncs);
    }
    nonIdcs.push_back(currIdx);

    // Replace placeholder with array header, that does looping.
    genFncs[nonStart] = std::move([this, &genFncs](GenStorer& gs)
    {
        // Find choosen element.
        auto cIt = std::find_if(std::cbegin(probs), std::cend(probs), [this](int p)
        {
            return prob < p;
        });

        const std::size_t idx = cIt - std::cbegin(probs);
        const size_t endIdx = *nonIdcs.rbegin();
        
        // Go through registred space.
        for (size_t i = nonIdcs[idx]; i < nonIdcs[idx + 1];)
        {
            i = genFncs[i](gs);
        }
        
        // Return index to next element after all nonterminals.
        return endIdx;
    });
}

void bdg::core::ProbSetGenerator::repairImpl(
    const GrammarElement::element_map_t& parsedEls, 
    const GrammarElement::seq_vector_t& locSeqs, 
    GrammarElement::intGenTypeFnc_vec_t& locGens, 
    GrammarElement::intGenTypeFnc_vec_t& remGens, 
    std::vector<std::vector<GrammarElement::genIdxTypeFnc_t>>& remGensGens, 
    std::vector<GrammarElement::GlobSeq>& globSeqs, 
    std::vector<const GrammarElement*>& pathEls)
{
    // If empty than do nothing.
    if (size() == 0)
    {
        assert(false);
        return;
    }
    
    size_t i = 0;
    // Lower probability.
    // Initized in first loop.
    int lowProb;
    // Upper probability.
    int upperProb = 0;
    for (auto& el : elements)
    {
        // Low is previous probability.
        lowProb = upperProb;
        upperProb = probs[i];
        locGens.emplace_back(
            &chooser, 
            GrammarElement::IntGenType::CONTINUOUS, 
            CountTransFtor(
                [](const CountTransFtor& refThis, int genValue)
                {
                    //iarg1 = lowProb;
                    //iarg2 = upperProb;

                    // If is between borders.
                    return refThis.iarg1 <= genValue && genValue < refThis.iarg2;
                },
                lowProb,
                upperProb));

        el->repair(parsedEls, locSeqs, locGens, remGens, remGensGens, globSeqs, pathEls);
        locGens.pop_back();
        ++i;
    }
}

/**
* Create partial sums of probabilty sets. 
*/
std::vector<int> bdg::core::ProbSetGenerator::transformProbs(const std::vector<int>& probs)
{
    std::vector<int> res;
    res.reserve(probs.size());
    std::partial_sum(std::cbegin(probs), std::cend(probs), std::back_inserter(res));
    return res;
}
