/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file stringgenerator.cpp
* @category Generator
* @author   Big Data Generator Team
*
*/
	
#include "core/pipeline/plugins/generators/stringgenerator.h" // header
#include "core/pipeline/plugins/generators/constants.h" // Constants

#include <string>
#include <algorithm> // std::generate

bdg::core::StringGenerator::StringGenerator(
    int minLength, 
    int maxLength, 
    const std::string &distrName,
    bool decorate):
genValImpl(decorate ? &StringGenerator::genValDecImpl : &StringGenerator::genValUndecImpl),
lengthGenerator(minLength, maxLength, distrName),
characterGenerator(0, lastChar - firstChar, Constants::DISTR_UNIFORM)
{
    if (minLength < 0)
    {
        throw bdg::utils::BadBoundariesException("Negative length parameter");
    }

}

void bdg::core::StringGenerator::init(HistoryStorage<std::string>& h)
{
    // Do nothing.
}

std::string bdg::core::StringGenerator::generateValue()
{
    // Call member function.
    return (this->*genValImpl)();
}

void bdg::core::StringGenerator::generate(GenStorer& gs)
{
    gs.store(generateValue());
}

/**
* Generates decorated strings.
*/
std::string bdg::core::StringGenerator::genValDecImpl()
{
    std::string res;
    res.resize(lengthGenerator.generateValue() + 2);
    *std::begin(res) = START_CHAR;
    std::generate(begin(res) + 1, end(res) - 1, [this]()
    {
        return firstChar + characterGenerator.generateValue();
    });
    *res.rbegin() = END_CHAR;
    return res;
}

/**
* Generates undecorated string.
*/
std::string bdg::core::StringGenerator::genValUndecImpl()
{
    std::string res;
    res.resize(lengthGenerator.generateValue());
    std::generate(begin(res), end(res), [this]()
    {
        return firstChar + characterGenerator.generateValue();
    });
    return res;
}
