/**
* Copyright Big Data Generator
*
* This file is part of the Big Data Generator SW Project.
*
* @file referenceelement.h
* @category Generator
* @author   Big Data Generator Team
*
*/
	
#include "core/pipeline/plugins/generators/referenceelement.h" // header
#include "core/pipeline/plugins/generators/exception.h" // exception

#include <cassert> // assert
#include <algorithm> // find

bdg::core::ReferenceElement::ReferenceElement(const std::string& type):
    name(type)
{
}

/**
* Tries to find wanted element by name in all parsed elements.
* Also checks recursions, because recursive type has to be at 
* some point reference element.
*/
void bdg::core::ReferenceElement::repairImpl(
    const bdg::core::GrammarElement::element_map_t& parsedEls,
    const bdg::core::GrammarElement::seq_vector_t& seqs,
    bdg::core::GrammarElement::intGenTypeFnc_vec_t& locGens,
    GrammarElement::intGenTypeFnc_vec_t& remGens,
    std::vector<std::vector<GrammarElement::genIdxTypeFnc_t>>& remGensGens,
    std::vector<GrammarElement::GlobSeq>& globSeqs,
    std::vector<const GrammarElement*>& pathEls)
{
    // Try find while we have all types.
    auto foundElementIt = parsedEls.find(name);
    if (foundElementIt == parsedEls.cend())
    {
        throw Exception(Error::TYPE_NOT_EXISTS + name);
    }

    // If not empty it means that we've already repaired, so are in cycle -> recursion.
    // It happens f.e. in circular dependency A<-B, B<-C, C<-B
    if (size() > 0)
        throw Exception(Error::RECURSIVE_TYPE + name + ".");

    // Replace it with found type.
    this->push_back(foundElementIt->second);

    checkRecursion(pathEls, name);

    // Call repair to element.
    foundElementIt->second->repair(parsedEls, seqs, locGens, remGens, remGensGens, globSeqs, pathEls);
}

/**
 * @brief Checks recursion.
 *  Goes through pathEls and tries to find itself. If found,
 *  it means that we have recursion. Because we are parsing schema
 *  DFS way - recursion has to use Reference Element, because
 *  it references itself - that is not yet resolved.
 *  Last element of pathEls is this GrammarElement.
 */
void bdg::core::ReferenceElement::checkRecursion(
    const std::vector<const GrammarElement*>& pathEls,
    const std::string& typeName)
{
    assert(!pathEls.empty());
    
    const GrammarElement* const ge = pathEls.back();
    // Using end() - 1, because we are looking for last element.
    if (std::find(std::cbegin(pathEls), std::cend(pathEls) - 1, ge) != std::cend(pathEls) - 1)
    {
        throw Exception(Error::RECURSIVE_TYPE + typeName + ".");
    }
}
