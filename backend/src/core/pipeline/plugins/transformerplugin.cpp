/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/transformerplugin.h"        // Header

#include <Poco/File.h>          // Poco::File

/**
 * Constructor of the transformer plugin.
 *
 * @param name Name of the plugin.
 * @param description Description of the plugin.
 * @param extension File extension of files produced by the plugin.
 */
bdg::core::pipeline::plugin::TransformerPlugin::TransformerPlugin(const std::string &name, const std::string &description, const std::string &extension) :
    Plugin(name, description, "%j/" + convertToDashes(name) + "-%p/rank-%r/%d/file-%i." + extension, false, true)
{
    // Nothing here.
}

/**
 * Method called on the worker to create the right instance of the task. Task serialized in the
 * string is then deserialized into that instance and passed back to the plugin to be processed.
 *
 * @return Type safe instance of a task.
 */
bdg::core::pipeline::Task *bdg::core::pipeline::plugin::TransformerPlugin::taskFactory()
{
    return new bdg::core::pipeline::TransformerTask;
}

/**
 * Method called on the master to create a task. This task is then sent to a worker to be processed.
 *
 * @param metadata Task metadata which define task properties.
 *
 * @return Task which should be sent to a worker to be processed.
 */
bdg::core::pipeline::Task *bdg::core::pipeline::plugin::TransformerPlugin::createTask(const bdg::core::pipeline::TaskMetadata *metadata)
{
    return new bdg::core::pipeline::TransformerTask(convertMetadata(metadata));
}

/**
 * Processes one task. This method is called on worker when it receives a task that should be processed.
 * This method calls processOneFile method for each file that should be processed in the task. Method
 * processOneFile is meant to be implemented by the concrete plugin - a descendant of this class.
 *
 * @param task Task to be processed.
 *
 * @return Result of task processing.
 */
bdg::core::pipeline::TaskResult* bdg::core::pipeline::plugin::TransformerPlugin::processTaskImpl(const bdg::core::pipeline::Task *task)
{
    // Do we use global file ids?
    if (useGlobalFileIds())
    {
        nextFileIdLocal = task->getFileFrom();
    }

    auto transformerTask = convertTask(task);
    const auto resultFileMask = transformerTask->getResultWorkerFileMask();
    const auto resultRank = transformerTask->getResultWorkerRank();
    const auto resultRankFileFrom = transformerTask->getFileFrom();
    const auto filesCount = transformerTask->getFilesCount();
    const auto fileIdFrom = nextFileIdLocal;
    const auto storagePath = getOutputPath();

    bdg_information(getLogger(), "Processing " + getName() + " task; Rank: " + std::to_string(resultRank) + ", From: " + std::to_string(resultRankFileFrom) + ", Files: " + std::to_string(filesCount));

    FileTransfererStreamIterator fileIterator(getFileTransferer().getFilesFromWorker(resultFileMask, resultRank, resultRankFileFrom, filesCount));
    size_t processedFiles = 0;
    size_t errorFiles = 0;
    Poco::UInt64 generatedBytes = 0;
    Poco::Timestamp startTime;
    std::vector<std::string> inputFilesPaths;
    
    if (!createOutputDirectories(fileIdFrom, filesCount))
    {
        bdg_error(getLogger(), "Could not create output directory in plugin [" + getName() + "]");
        return new bdg::core::pipeline::ErrorTaskResult("Could not create output directory in plugin [" + getName() + "]");
    }

    // Transform files.
    while (fileIterator.moveToNextFile() && canContinue(task, processedFiles, generatedBytes, startTime))
    {
        const Poco::Path inputFilePath(fileIterator.getCurrentPath());
        const Poco::Path outputFilePath(getFileTransferer().expandFileMaskFileId(storagePath, nextFileIdLocal));
        
        bdg::core::pipeline::ErrorTaskResult *result = nullptr;
        size_t newlyCreatedFilesCount = 1;
        inputFilesPaths.push_back(inputFilePath.toString());

        // Transform the file.
        try
        {
            processOneFile(transformerTask, fileIterator.getCurrentFile(), inputFilePath, outputFilePath, newlyCreatedFilesCount);
        }
        catch (bdg::utils::TaskFatalErrorException &e)
        {
            result = new bdg::core::pipeline::FatalErrorTaskResult(e.message());
        }
        catch (bdg::utils::TaskErrorException &e)
        {
            result = new bdg::core::pipeline::ErrorTaskResult(e.message());
        }
        catch (Poco::Exception &e)
        {
            result = new bdg::core::pipeline::ErrorTaskResult("Could not transform file [" + inputFilePath.toString() + "] from worker " + std::to_string(resultRank) + " to file [" + outputFilePath.toString() + "]. " + e.displayText());
        }
        catch (std::exception &e)
        {
            result = new bdg::core::pipeline::ErrorTaskResult("Could not transform file [" + inputFilePath.toString() + "] from worker " + std::to_string(resultRank) + " to file [" + outputFilePath.toString() + "]. " + e.what());
        }

        // Did processOneFile method generate a new transformed file? This condition is there because
        // of the batch processing - more input files can be transformed to one output file.
        if (newlyCreatedFilesCount > 0)
        {
            for (int i = 0; i < newlyCreatedFilesCount; ++i)
            {
                const Poco::File generatedFile(getFileTransferer().expandFileMaskFileId(storagePath, nextFileIdLocal + i));

                try
                {
                    generatedBytes += generatedFile.getSize();
                }
                catch (...)
                {
                    // We just need to catch exceptions here, e.g. when the file does not exist.
                }
            }

            nextFileIdLocal += newlyCreatedFilesCount;
            processedFiles += newlyCreatedFilesCount;
        }

        // Did we reach the error threshold (ratio of error/target files is too high)?
        if (result != nullptr)
        {
            bdg_warning(getLogger(), result->getMessage());
            ++errorFiles;

            if (result->getType() == TaskResult::ResultType::RESULT_TYPE_FATAL_ERROR || (double)errorFiles / filesCount > MAX_ERROR_FILES_RATIO)
            {
                nextFileIdLocal = useGlobalFileIds() ? 1 : fileIdFrom;
                return result;
            }
        }
    }

    // Do we use global file ids?
    if (useGlobalFileIds())
    {
        nextFileIdLocal = 1;
    }

    // Do continuous clean up.
    if (getCleanUpType() == CleanUpType::CLEAN_UP_CONTINUOUS || getCleanUpType() == CleanUpType::CLEAN_UP_CONTINUOUS_POSTPROCESS)
    {
        for (const auto &inputFilePath : inputFilesPaths)
        {
            try
            {
                Poco::File(inputFilePath).remove();
            }
            catch (Poco::Exception &e)
            {
                bdg_warning(getLogger(), "Could not remove file [" + inputFilePath + "] in continuous clean up!");
                bdg_debug(getLogger(), e.displayText());
                UNREFERENCED_VARIABLE(e);
            }
        }
    }

    bdg_information(getLogger(), "Finished " + getName() + " task in " + std::to_string(startTime.elapsed() / 1000000) + "." + std::to_string((startTime.elapsed() / 100000) % 10) + "s");

    return new bdg::core::pipeline::SuccessTaskResult(storagePath, fileIdFrom, processedFiles, generatedBytes, startTime.elapsed());
}

/**
 * Converts task metadata to the right type.
 *
 * @param metadata Task metadata to be converted.
 *
 * @return Type safe instance of task metadata.
 */
bdg::core::pipeline::TransformerTaskMetadata *bdg::core::pipeline::plugin::TransformerPlugin::convertMetadata(const bdg::core::pipeline::TaskMetadata *metadata) const
{
    return (TransformerTaskMetadata*)metadata;
}

/**
 * Converts task to the right type.
 *
 * @param task Task to be converted.
 *
 * @return Type safe instance of the task.
 */
bdg::core::pipeline::TransformerTask *bdg::core::pipeline::plugin::TransformerPlugin::convertTask(const bdg::core::pipeline::Task *task) const
{
    return (TransformerTask*)task;
}
