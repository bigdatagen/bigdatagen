/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/transformertaskmetadata.h"      // Header

/**
 * Constructor of the task metadata.
 *
 * @param properties Task metadata properties.
 */
bdg::core::pipeline::TransformerTaskMetadata::TransformerTaskMetadata(const Properties &properties) :
    TaskMetadata(properties)
{
    // Nothing here.
}

/**
 * Constructor of the task metadata.
 *
 * @param taskRestult Task result from we will create a new task.
 */
bdg::core::pipeline::TransformerTaskMetadata::TransformerTaskMetadata(const SuccessTaskResult &taskRestult) :
    resultWorkerFileMask(taskRestult.getFileMask()),
    resultWorkerRank(taskRestult.getRank()),
    resultWorkerFileFrom(taskRestult.getFileFrom()),
    resultWorkerFilesCount(taskRestult.getFilesCount()),
    resultWorkerBytes(taskRestult.getBytes())
{
    // Nothing here.
}
