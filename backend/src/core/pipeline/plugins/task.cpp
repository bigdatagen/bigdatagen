/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/task.h"         // Header

/**
 * Constructor of the task.
 *
 * @param properties Task properties.
 */
bdg::core::pipeline::Task::Task(const Properties &properties) :
    PropertiesObject(properties)
{
    // Nothing here.
}

/**
 * Serializes the task. Serialization is needed when the task is sent from the master to a worker.
 *
 * @param json JSON object where the serialized task is stored.
 */
void bdg::core::pipeline::Task::serializeAdditional(Poco::JSON::Object &json) const
{
    PropertiesObject::serializeAdditional(json);
    json.set("pluginIndex", pluginIndex);
    json.set("restarts", restarts);
    json.set("filesCount", filesCount);
    json.set("fileFrom", fileFrom);
    json.set("promissedFiles", promissedFiles);
    json.set("promissedBytes", promissedBytes);
    json.set("filesLimit", filesLimit);
    json.set("bytesLimit", bytesLimit);
    json.set("secondsLimit", secondsLimit);
}

/**
 * Deserializes the task. Deserialization is performed on a worker, when a message with the task
 * is delivered.
 *
 * @param json JSON object where the serialized task is stored.
 */
void bdg::core::pipeline::Task::unserializeAdditional(const Poco::JSON::Object &json)
{
    PropertiesObject::unserializeAdditional(json);
    pluginIndex = json.getValue<int>("pluginIndex");
    restarts = json.getValue<size_t>("restarts");
    filesCount = json.getValue<size_t>("filesCount");
    fileFrom = json.getValue<Poco::UInt64>("fileFrom");
    promissedFiles = json.getValue<size_t>("promissedFiles");
    promissedBytes = json.getValue<Poco::UInt64>("promissedBytes");
    filesLimit = json.getValue<Poco::UInt64>("filesLimit");
    bytesLimit = json.getValue<Poco::UInt64>("bytesLimit");
    secondsLimit = json.getValue<time_t>("secondsLimit");
    
}
