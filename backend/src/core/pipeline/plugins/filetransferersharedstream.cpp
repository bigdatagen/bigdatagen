/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/plugins/filetransferersharedstream.h"       // Header

#include <Poco/Exception.h>             // Poco::FileNotFoundException
#include <Poco/File.h>                  // Poco::File

/**
 * Constructor of the stream.
 *
 * @param fileName Path to the file.
 * @param fileId Id of the file.
 */
bdg::core::pipeline::FileTransfererSharedStream::FileTransfererSharedStream(const std::string &fileName, const Poco::UInt64 fileId) :
    FileTransfererStream(),
    fileName(fileName),
    fileId(fileId)
{
    if (!Poco::File(fileName).exists())
    {
        throw Poco::FileNotFoundException("File [" + fileName + "] does not exist");
    }

    inputStream.open(fileName, std::ios::binary);
}

/**
 * Destructor of the stream.
 */
bdg::core::pipeline::FileTransfererSharedStream::~FileTransfererSharedStream()
{
    inputStream.close();
}
