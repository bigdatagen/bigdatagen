/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/pipeline.h"                 // Pipeline
#include "core/pipeline/plugins/filetransferer.h"   // FileTransferer
#include "utils/checkpointer.h"                     // DataCheckpointer
#include "utils/logger.h"                           // Logger
#include "utils/mpiproxy.h"                         // MPIProxy

#include <string>       // std::string
#include <vector>       // std::vector

using bdg::utils::Logger;
using bdg::utils::MPIProxy;
using bdg::core::pipeline::FileTransferer;
using bdg::core::pipeline::Pipeline;

/**
 * Handles errors that occured during initialization. In master thread it gathers errors
 * from all the threads and writes out errors.
 *
 * @param mpi Instance of MPI.
 * @param errors Errors that occured.
 *
 * @return Was there any error?
 */
bool handleError(const MPIProxy &mpi, std::vector<std::string> &errors)
{
    std::vector<int> errorBuffer(mpi.getSize());
    int isError = errors.size() > 0 ? 1 : 0;
    int tag;

    mpi.allGatherInts(isError, &errorBuffer[0]);

    for (int i = 0; i < mpi.getSize(); ++i)
    {
        // There was an error on some rank -> write it.
        if (errorBuffer[i])
        {
            // Write the errors on master.
            if (mpi.isMaster())
            {
                // Get the error messages from the error rank.
                if (i != mpi.getMasterRank())
                {
                    std::string error = mpi.receiveMessageFromWorker(i, tag);
                    errors.push_back(error);
                }

                // Write the detected errors.
                if (errors.size() > 0)
                {
                    for (std::vector<std::string>::const_iterator it = errors.begin(); it != errors.end(); ++it)
                    {
                        std::cerr << "ERROR: " << (*it) << std::endl;
                    }
                }
                else
                {
                    std::cerr << "There was an error on rank " << i << "!" << std::endl;
                }
            }
            // Send the error message to the master.
            else if (mpi.getRank() == i)
            {
                mpi.sendMessageToMaster(errors.front(), MPIProxy::NO_TAG);
            }
            
            // There was some error on some rank.
            isError = 1;
            break;
        }
    }

    // There was an error on some rank -> correctly finalize MPI.
    if (isError)
    {
        mpi.finalize();
    }

    return isError == 0 ? false : true;
}

/**
 * Loads the job id from the command line argument.
 *
 * @param mpi Instance of MPI.
 * @param commandLineArgument Value of the command line argument.
 * @param jobId Output parameter for the job id.
 *
 * @return Was the job id successfully parsed?
 */
bool loadJobId(const MPIProxy &mpi, const std::string &commandLineArgument, int &jobId)
{
    if (mpi.isMaster())
    {
        // Initialize pipeline.
        jobId = std::atoi(commandLineArgument.c_str());

        return jobId > 0;
    }
    else
    {
        return true;
    }
}

/**
 * Starting point of the program. It has one command line argument - it is a jobId
 * which is the id of job configuration file that should be loaded and used for the
 * generation. It can also be a path to the job configuration file. This program
 * must be launched on distributed FS or it doesn't work.
 *
 * @param argc Number of command line arguments.
 * @param argv Command line arguments.
 *
 * @return Return value of the program.
 */
int main(int argc, char **argv)
{
    MPIProxy mpi;
    std::vector<std::string> errors;

    try
    {
        mpi.init(argc, argv);
    }
    catch (Poco::Exception &e)
    {
        errors.push_back(e.displayText());
    }

    // Handle error.
    if (handleError(mpi, errors))
    {
        return 1;
    }

    // For debugging.
    /*if (mpi.isMaster())
    {
        int x;
        std::cout << "Debugging" << std::endl;
        std::cin >> x;
    }

    mpi.barrier();*/
    
    // Check configuration.
    if (argc < 2)
    {
#if defined(_WIN32)
        errors.push_back("Usage: Pipeline.exe [jobId]");
#else
        errors.push_back("Usage: ./Pipeline [jobId]");
#endif
    }

    // Check config file.
    try
    {
        bdg::utils::Config::getInstance();
    }
    catch (Poco::Exception &e)
    {
        errors.push_back(e.displayText());
    }
    catch (std::exception &e)
    {
        errors.push_back(e.what());
    }

    // Handle error.
    if (handleError(mpi, errors))
    {
        return 1;
    }

    // Get the id of job configuration.
    int jobId = 0;

    if (!loadJobId(mpi, argv[1], jobId))
    {
        errors.push_back("Invalid path to a job configuration passed as the first argument!");
    }

    mpi.broadcastInt(jobId);
    mpi.detectNodes();

    // Check the filesystem.
    if (!FileTransferer::detectSharedFileSystem(mpi, jobId))
    {
        errors.push_back("Generator cannot run without a shared file system!");
    }

    // Check logger.
    try
    {
        Logger::setJobId(jobId);
        Logger::getInstance().getLogger(mpi.getRank());
    }
    catch (Poco::Exception &e)
    {
        errors.push_back(e.displayText());
    }
    catch (std::exception &e)
    {
        errors.push_back(e.what());
    }

    // Handle error.
    if (handleError(mpi, errors))
    {
        return 1;
    }

    // Initialization.
    if (mpi.isMaster())
    {
        std::cout << "Initializing..." << std::endl;
        bdg_information(Logger::getInstance().getLogger(mpi.getRank()), "Using job ID: " + std::to_string(jobId));
    }

    // Run the pipeline.
    try
    {
        Pipeline pipeline(mpi, jobId);
        pipeline.run();
    }
    catch (Poco::Exception &e)
    {
        errors.push_back(e.displayText());
    }
    catch (std::exception &e)
    {
        errors.push_back(e.what());
    }

    // Handle error.
    if (handleError(mpi, errors))
    {
        return 1;
    }

    // Terminate MPI on all the nodes.
    mpi.finalize();
    return 0;
}
