/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/monitoringcheckpointerrestorer.h"       // Header

#include <Poco/File.h>              // Poco::File
#include <Poco/JSON/Parser.h>       // Poco::JSON::Parser
#include <Poco/StreamCopier.h>      // Poco::StreamCopier

/**
 * Constructor of the checkpointer.
 *
 * @param pipelineJobId
 */
bdg::core::pipeline::MonitoringCheckpointerRestorer::MonitoringCheckpointerRestorer(const int pipelineJobId) :
    Checkpointer(pipelineJobId)
{
    // Nothing here.
}

/**
 * Restores the checkpoint into the JSON object.
 *
 * @param json Output parameter for the JSON object where the checkpoint should be restored.
 */
void bdg::core::pipeline::MonitoringCheckpointerRestorer::restore(Poco::JSON::Object &json) const
{
    const std::string lastCheckpointPath = getPathToFile();

    if (Poco::File(lastCheckpointPath).exists())
    {
        try
        {
            // Parse the checkpoint stored in JSON format.
            std::ifstream checkpointStream(lastCheckpointPath, std::ios::binary);
            std::string checkpointData;

            Poco::StreamCopier::copyToString(checkpointStream, checkpointData);

            Poco::Dynamic::Var configurationVar = Poco::JSON::Parser().parse(checkpointData);
            Poco::JSON::Object::Ptr checkpointJson = configurationVar.extract<Poco::JSON::Object::Ptr>();

            json = *checkpointJson;
        }
        catch (...)
        {
            // We just need to catch exceptions.
        }
    }
}
