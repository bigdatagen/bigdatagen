/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/monitoringcheckpointer.h"       // Header
#include "core/pipeline/progresscounter.h"              // ProgressCounter
#include "core/pipeline/speedlimiterscheduler.h"        // SpeedLimiterScheduler
#include "core/pipeline/pipeline.h"                     // Pipeline
#include "core/pipeline/taskscheduler.h"                // TaskScheduler
#include "core/pipeline/plugins/plugin.h"               // Plugin

const size_t bdg::core::pipeline::MonitoringCheckpointer::CHECKPOINT_PERIOD_SECONDS = 1;

/**
 * Constructor of the checkpointer.
 *
 * @param pipelineJobId Id of currently running job.
 */
bdg::core::pipeline::MonitoringCheckpointer::MonitoringCheckpointer(const int pipelineJobId) :
    Checkpointer(pipelineJobId)
{
    // Nothing here.
}

/**
 * Stores the monitoring checkpoint.
 *
 * @param scheduler Instance of the task scheduler.
 * @param indent Indent used in the checkpoint file. If 0 is passed, no indentation is made.
 */
void bdg::core::pipeline::MonitoringCheckpointer::store(TaskScheduler *scheduler, const int indent)
{
    Poco::JSON::Object json;
    store(scheduler, json, false);

    // Store the checkpoint to the file.
    try
    {
        createStorageDir();

        std::ofstream file(getPathToFile(), std::ios::binary);
        json.stringify(file, indent);
        file.close();
    }
    catch (Poco::Exception &e)
    {
        bdg_warning(scheduler->getLogger(), "Failed to store the monitoring checkpoint");
        bdg_debug(scheduler->getLogger(), "Exception: " + e.displayText());
        UNREFERENCED_VARIABLE(e);
    }
    catch (std::exception &e)
    {
        bdg_warning(scheduler->getLogger(), "Failed to store the monitoring checkpoint");
        bdg_debug(scheduler->getLogger(), "Exception: " + std::string(e.what()));
        UNREFERENCED_VARIABLE(e);
    }
}

/**
 * Stores the monitoring checkpoint to a given JSON object.
 *
 * @param scheduler Instance of the task scheduler.
 * @param json JSON where the checkpoint should be stored to.
 * @param useUnits Do we want to use units in the statistical magnitutes. If set to false, only numbers without units are returned.
 */
void bdg::core::pipeline::MonitoringCheckpointer::store(TaskScheduler *scheduler, Poco::JSON::Object &json, bool useUnits)
{
    const ProgressCounter &progressCounter = scheduler->getProgressCounter();
    const StatisticsCounter &statisticsCounter = scheduler->getStatisticsCounter();
    const SpeedLimiterScheduler &speedLimiterScheduler = scheduler->getSpeedLimiterScheduler();
    const Pipeline &pipeline = scheduler->getPipeline();

    const Poco::UInt64 processedTasksCount = statisticsCounter.getProcessedTasksCount();
    const Poco::UInt64 successfulLastPluginTasksCount = statisticsCounter.getSuccessfulPluginTasks(pipeline.getPluginsCount() - 1);
    const Poco::Int64 generatedFiles = progressCounter.getGeneratedFiles();
    const Poco::Int64 generatedBytes = progressCounter.getGeneratedBytes();
    const time_t elapsedSeconds = progressCounter.getElapsedSeconds();

    const int workersCount = scheduler->getWorkers().getSize();
    const double avgFilesSpeed = progressCounter.getAverageSpeedFilesPerSecond();
    const double avgSizeSpeed = progressCounter.getAverageSpeedBytesPerSecond();
    const double avgSpeedSizePerWorker = avgSizeSpeed / workersCount;
    const double avgSizePerTask = successfulLastPluginTasksCount > 0 ? (double)generatedBytes / successfulLastPluginTasksCount : 0;

    json.set("progress", std::to_string(progressCounter.getProgressGenerated()));
    json.set("workersCount", std::to_string(workersCount));
    json.set("generatedFiles", std::to_string(generatedFiles));
    json.set("generatedSize", useUnits ? convertSize((double)generatedBytes) : std::to_string(generatedBytes));

    json.set("avgSpeedFiles", std::to_string(avgFilesSpeed) + (useUnits ? " files/s" : ""));
    json.set("avgSpeedSize", useUnits ? convertSize(avgSizeSpeed) + "/s" : std::to_string(avgSizeSpeed));
    json.set("avgSpeedFilesPerWorker", std::to_string(avgFilesSpeed / workersCount) + (useUnits ? " files/s" : ""));
    json.set("avgSpeedSizePerWorker", useUnits ? convertSize(avgSpeedSizePerWorker) + "/s" : std::to_string(avgSpeedSizePerWorker));
    json.set("avgFilesPerTask", successfulLastPluginTasksCount > 0 ? std::to_string((double)generatedFiles / successfulLastPluginTasksCount) : "0");
    json.set("avgSizePerTask", useUnits ? convertSize(avgSizePerTask) : std::to_string(avgSizePerTask));
    json.set("avgTaskLength", processedTasksCount > 0 ? std::to_string(((double)elapsedSeconds / processedTasksCount) * workersCount) : "0");

    json.set("totalTime", std::to_string(elapsedSeconds));
    json.set("totalTasks", std::to_string(processedTasksCount));
    json.set("errorTasks", std::to_string(statisticsCounter.getErrorTasksCount()));
    
    // Get task errors.
    Poco::JSON::Array taskErrorsJson;

    for (int i = 0; i <= workersCount; ++i)
    {
        const bdg::core::pipeline::Worker &worker = scheduler->getWorkers().getWorker(i);

        if (worker.getErrorTasks() > 0)
        {
            Poco::JSON::Object workerTaskErrorInfoJson;
            Poco::JSON::Array workerTaskErrorsJson;

            for (const std::string &error : worker.getTaskErrors())
            {
                workerTaskErrorsJson.add(error);
            }

            workerTaskErrorInfoJson.set("worker", i);
            workerTaskErrorInfoJson.set("errors", workerTaskErrorsJson);
            taskErrorsJson.add(workerTaskErrorInfoJson);
        }
    }

    json.set("taskErrors", taskErrorsJson);

    // Write statistics about optimal speed (only when some storer is used in the pipeline).
    if (!pipeline.getInfo().getSchedulerInfo().getFileFrequency().isUndefined() && pipeline.getPlugin(pipeline.getPluginsCount() - 1)->getType() == bdg::core::pipeline::plugin::Plugin::PluginType::PLUGIN_TYPE_STORER)
    {
        std::string demandedUnit = "files";
        std::string bestUnit = "files";
        size_t bestStorersCount;

        double demandedSpeed = speedLimiterScheduler.getDemandedSpeed();
        double bestSpeed = speedLimiterScheduler.getBestSpeed(bestStorersCount);

        if (pipeline.getInfo().getSchedulerInfo().getFileFrequency().getBytes() > 0)
        {
            demandedSpeed = convertSize(demandedSpeed, demandedUnit);
            bestSpeed = std::max<double>(bestSpeed, avgSizeSpeed);
            bestSpeed = convertSize(bestSpeed, bestUnit);
        }
        else
        {
            bestSpeed = std::max<double>(bestSpeed, avgFilesSpeed);
        }

        json.set("demandedSpeed", std::to_string(demandedSpeed) + " " + demandedUnit + "/s");
        json.set("bestSpeed", std::to_string(bestSpeed) + " " + bestUnit + "/s");
        json.set("optimalStorersCount", std::to_string(bestStorersCount));
    }
    else
    {
        json.set("demandedSpeed", "-");
        json.set("bestSpeed", "-");
        json.set("optimalStorersCount", "-");
    }
}
