/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/taskfactory.h"                          // Header
#include "core/pipeline/idletask.h"                             // IdleTask
#include "core/pipeline/plugins/generatortaskmetadata.h"        // GeneratorTaskMetadata
#include "core/pipeline/plugins/transformertaskmetadata.h"      // TransformerTaskMetadata
#include "core/pipeline/plugins/storertaskmetadata.h"           // StorerTaskMetadata
#include "core/pipeline/plugins/plugin.h"                       // Plugin

/**
 * Constructor of the task factory.
 *
 * @param pipeline Instance of the pipeline.
 * @param resultQueue Queue with task results.
 */
bdg::core::pipeline::TaskFactory::TaskFactory(const Pipeline &pipeline, ResultQueue &resultQueue) :
    pipeline(pipeline),
    resultQueue(resultQueue)
{
    // Nothing here.
}

/**
 * Creates a task that should be processed on some worker. It might happen that no task can be created
 * (for example when all the result queues are full), then an instance of IdleTask is returned.
 *
 * @param canCreateGeneratorTask Are we allowed to create a task for generator plugin?
 * @param requiredPluginIndex Index of the plugin for which we want to create the task. If -1 is passed, any plugin can be used.
 * @param workerRank Rank of currently running MPI thread.
 *
 * @return Created task.
 */
std::unique_ptr<bdg::core::pipeline::Task> bdg::core::pipeline::TaskFactory::createTask(
    const bool canCreateGeneratorTask,
    const int requiredPluginIndex,
    const int workerRank)
{
    const bdg::core::pipeline::PipelineInfo &pipelineInfo = pipeline.getInfo();
    const bdg::core::pipeline::SchedulerInfo &schedulerInfo = pipelineInfo.getSchedulerInfo();

    int pluginIndex = 0;
    bdg::core::pipeline::TaskMetadata *metadata;

    // Get the task result from the queue suitable for given worker rank.
    std::unique_ptr<bdg::core::pipeline::SuccessTaskResult> taskResult = resultQueue.popSuccessResult(requiredPluginIndex, workerRank);
    const bool createGeneratorTask = taskResult == nullptr;

    // There are no results we could base a new tasks at -> create new generator task.
    if (createGeneratorTask)
    {
        // If we wanted to create a task for specified plugin or generator task cannot run on specified
        // rank, we return idle task as signal, that the rank should be idle.
        if (requiredPluginIndex >= 1 || !schedulerInfo.canRunPluginAtRank(pluginIndex, workerRank) || !canCreateGeneratorTask)
        {
            std::unique_ptr<bdg::core::pipeline::Task> task(new bdg::core::pipeline::IdleTask());
            return task;
        }

        // First plugin in the pipeline is always generator and it is the only instance of a generator
        // plugin in the pipeline.
        bdg::core::pipeline::GeneratorTaskMetadata *taskMetadata = new bdg::core::pipeline::GeneratorTaskMetadata;
        taskMetadata->setSchema("1");       // Root element in the schema which should be used for generating.
        metadata = taskMetadata;
    }
    // There are some task results in the queue.
    else
    {
        pluginIndex = taskResult->getPluginIndex() + 1;

        if (pipeline.getPlugin(pluginIndex)->getType() == bdg::core::pipeline::plugin::Plugin::PluginType::PLUGIN_TYPE_TRANSFORMER)
        {
            metadata = new bdg::core::pipeline::TransformerTaskMetadata(*taskResult);
        }
        else
        {
            metadata = new bdg::core::pipeline::StorerTaskMetadata(*taskResult);
        }
    }

    // Create a task.
    std::unique_ptr<bdg::core::pipeline::Task> task(pipeline.getPlugin(pluginIndex)->createTask(metadata));
    task->setPluginIndex(pluginIndex);

    delete metadata;
    return task;
}
