/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/schedulercheckpointer.h"        // Header
#include "core/pipeline/taskscheduler.h"                // TaskScheduler

#include <Poco/JSON/Parser.h>       // Poco::JSON::Parser
#include <Poco/StreamCopier.h>      // Poco::StreamCopier

/**
 * Constructor of the checkpointer.
 *
 * @param pipelineJobId Id of currently running job.
 * @param checkpointPeriodSeconds How often should we store monitoring checkpoints.
 */
bdg::core::pipeline::SchedulerCheckpointer::SchedulerCheckpointer(const int pipelineJobId, const size_t checkpointPeriodSeconds) :
    Checkpointer(1),
    pipelineJobId(pipelineJobId),
    checkpointPeriodSeconds(checkpointPeriodSeconds)
{
    // Nothing here.
}

/**
 * Stores a new checkpoint.
 *
 * @param scheduler Instance of the task scheduler.
 * @param indent Indent used in the checkpoint file. If 0 is passed, no indentation is made.
 */
void bdg::core::pipeline::SchedulerCheckpointer::store(TaskScheduler *scheduler, const int indent)
{
    if (checkpointPeriodSeconds == 0)
    {
        return;
    }
    
    Poco::JSON::Object checkpointJson;
    Poco::JSON::Array workersJson;
    Poco::JSON::Object resultsJson;
    Poco::JSON::Object tasksJson;
    Poco::JSON::Object progressCounterJson;
    Poco::JSON::Object speedLimiterSchedulerJson;
    Poco::JSON::Object statisticsCounterJson;

    WorkerGroup &workers = scheduler->getWorkers();
    ProgressCounter &progressCounter = scheduler->getProgressCounter();

    const int workersCount = workers.getSize();
    const int pluginsCount = scheduler->getPipeline().getPluginsCount();

    Poco::Int64 promissedFilesFromRunningTasks = 0;
    Poco::Int64 promissedBytesFromRunningTasks = 0;

    // We store the checkpoint to a new file and then we rename this file to have an id 1.
    // This is because we want to prevent the situation of failure when storing a new checkpoint.
    checkpointId = 2;

    const std::string newCheckpointPath = getPathToFile();

    // Serialize the tasks that are currently processed at some worker. We add these tasks to
    // the begining of task queues, so when the scheduler is loaded from this checkpoint, it
    // assignes those tasks back to the workers as the first ones. It does not really matter
    // which worker will process those tasks, it is important that they will be processed.
    for (int i = 0; i <= workersCount; ++i)
    {
        const std::unique_ptr<bdg::core::pipeline::Task> &task = workers.getWorker(i).getProcessedTask();

        if (task != nullptr)
        {
            if (task->getPluginIndex() == pluginsCount - 1)
            {
                promissedFilesFromRunningTasks += task->getPromissedFiles();
                promissedBytesFromRunningTasks += task->getPromissedBytes();
            }
        }
    }

    // We have to subtract promissed files/bytes by currently processed tasks.
    progressCounter.addPromissedFiles(-promissedFilesFromRunningTasks);
    progressCounter.addPromissedBytes(-promissedBytesFromRunningTasks);

    // Serialize the result and task queues.
    scheduler->getResultQueue().serialize(resultsJson);
    scheduler->getTaskQueue().serialize(tasksJson);

    // Serialize other important objects of task scheduler which hold scheduler's state.
    scheduler->getWorkers().serialize(workersJson);
    scheduler->getProgressCounter().serialize(progressCounterJson);
    scheduler->getSpeedLimiterScheduler().serialize(speedLimiterSchedulerJson);
    scheduler->getStatisticsCounter().serialize(statisticsCounterJson);
    scheduler->serialize(checkpointJson);

    // We have to add promissed files/bytes by currently processed tasks back.
    progressCounter.addPromissedFiles(promissedFilesFromRunningTasks);
    progressCounter.addPromissedBytes(promissedBytesFromRunningTasks);

    // Create the final JSON object.
    checkpointJson.set("workers", workersJson);
    checkpointJson.set("results", resultsJson);
    checkpointJson.set("tasks", tasksJson);
    checkpointJson.set("progressCounter", progressCounterJson);
    checkpointJson.set("speedLimiterScheduler", speedLimiterSchedulerJson);
    checkpointJson.set("statisticsCounter", statisticsCounterJson);

    // Store checkpoint to the file.
    try
    {
        createStorageDir();

        std::ofstream file(newCheckpointPath, std::ios::binary);
        checkpointJson.stringify(file, indent);
        file.close();

        // Move checkpoint to the file with id 1.
        checkpointId = 1;

        const std::string oldCheckpointPath = getPathToFile();

        Poco::File(newCheckpointPath).moveTo(oldCheckpointPath);
    }
    catch (Poco::Exception &e)
    {
        bdg_warning(scheduler->getLogger(), "Failed to store the scheduler checkpoint");
        bdg_debug(scheduler->getLogger(), "Exception: " + e.displayText());
        UNREFERENCED_VARIABLE(e);
    }
    catch (std::exception &e)
    {
        bdg_warning(scheduler->getLogger(), "Failed to store the scheduler checkpoint");
        bdg_debug(scheduler->getLogger(), "Exception: " + std::string(e.what()));
        UNREFERENCED_VARIABLE(e);
    }
}

/**
 * Stores a new checkpoint if the period of time between two checkpoints has passed.
 *
 * @param scheduler Instance of the task scheduler.
 * @param indent Indent used in the checkpoint file. If 0 is passed, no indentation is made.
 */
void bdg::core::pipeline::SchedulerCheckpointer::tryStore(TaskScheduler *scheduler, const int indent)
{
    if (lastCheckpointTime.elapsed() / 1000000 >= checkpointPeriodSeconds)
    {
        store(scheduler, indent);

        // Update the timer, so the next checkpoint is stored again in the next CHECKPOINT_PERIOD_SECONDS seconds.
        lastCheckpointTime.update();
    }
}

/**
 * Restores the task scheduler state from the checkpoint if it exists.
 *
 * @param scheduler Instance of the task scheduler.
 *
 * @return Did we load the scheduler state from the checkpoint?
 */
bool bdg::core::pipeline::SchedulerCheckpointer::tryRestore(TaskScheduler *scheduler) const
{
    const std::string lastCheckpointPath = getPathToFile();

    if (Poco::File(lastCheckpointPath).exists())
    {
        try
        {
            // Parse the checkpoint stored in JSON format.
            std::ifstream checkpointStream(lastCheckpointPath, std::ios::binary);
            std::string checkpointData;

            Poco::StreamCopier::copyToString(checkpointStream, checkpointData);

            Poco::Dynamic::Var configurationVar = Poco::JSON::Parser().parse(checkpointData);
            Poco::JSON::Object::Ptr checkpointJson = configurationVar.extract<Poco::JSON::Object::Ptr>();

            // Get the checkpoint basic properties.
            Poco::JSON::Array::Ptr workersJson = checkpointJson->getArray("workers");
            Poco::JSON::Object::Ptr resultsJson = checkpointJson->getObject("results");
            Poco::JSON::Object::Ptr tasksJson = checkpointJson->getObject("tasks");
            Poco::JSON::Object::Ptr progressCounterJson = checkpointJson->getObject("progressCounter");
            Poco::JSON::Object::Ptr speedLimiterSchedulerJson = checkpointJson->getObject("speedLimiterScheduler");
            Poco::JSON::Object::Ptr statisticsCounterJson = checkpointJson->getObject("statisticsCounter");

            // Unserialize scheduler's data structures.
            scheduler->getWorkers().unserialize(*workersJson);
            scheduler->getResultQueue().unserialize(*resultsJson);
            scheduler->getTaskQueue().unserialize(*tasksJson);
            scheduler->getProgressCounter().unserialize(*progressCounterJson);
            scheduler->getSpeedLimiterScheduler().unserialize(*speedLimiterSchedulerJson);
            scheduler->getStatisticsCounter().unserialize(*statisticsCounterJson);
            scheduler->unserialize(*checkpointJson);

            return true;
        }
        catch (Poco::Exception &e)
        {
            bdg_warning(scheduler->getLogger(), "Failed to load the scheduler checkpoint");
            bdg_debug(scheduler->getLogger(), "Exception: " + e.displayText());
            UNREFERENCED_VARIABLE(e);
        }
        catch (std::exception &e)
        {
            bdg_warning(scheduler->getLogger(), "Failed to load the scheduler checkpoint");
            bdg_debug(scheduler->getLogger(), "Exception: " + std::string(e.what()));
            UNREFERENCED_VARIABLE(e);
        }
    }

    return false;
}

/**
 * Removes the checkpoint if it exsits. This method is called when the job is finished.
 */
void bdg::core::pipeline::SchedulerCheckpointer::cleanUp() const
{
    Poco::File checkpoint = Poco::File(getPathToFile());

    if (checkpoint.exists())
    {
        try
        {
            checkpoint.remove();
        }
        catch (...)
        {
            // We just need to catch exceptions.
        }
    }
}
