/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/workergroup.h"          // Header

/**
 * Constructor of the worker.
 *
 * @param pipeline Instance of the pipeline.
 * @param mpiSize Number of used MPI threads.
 * @param pluginsCount Size of the pipeline - number of used pipeline plugins.
 */
bdg::core::pipeline::WorkerGroup::WorkerGroup(const bdg::core::pipeline::Pipeline &pipeline, const int mpiSize, const int pluginsCount) :
    pipeline(pipeline),
    workers(mpiSize),
    idleWorkersCount(getSize())
{
    for (int i = 0; i < mpiSize; ++i)
    {
        workers[i].setPluginsCount(pluginsCount);
    }
}

/**
 * Sets the state of the worker to one of enumerated states.
 *
 * @param workerRank Rank of the worker.
 * @param state New state of the worker.
 */
void bdg::core::pipeline::WorkerGroup::setWorkerState(const int workerRank, const bdg::core::pipeline::Worker::State state)
{
    if (workerRank < 0 || workerRank >= workers.size())
    {
        throw Poco::InvalidArgumentException("Worker rank out of range!");
    }
    
    Worker &worker = workers[workerRank];

    if (worker.getState() != state)
    {
        switch (worker.getState())
        {
            case bdg::core::pipeline::Worker::State::STATE_IDLE:
                --idleWorkersCount;
                break;

            case bdg::core::pipeline::Worker::State::STATE_PROCESSING:
                --processingWorkersCount;
                break;

            case bdg::core::pipeline::Worker::State::STATE_FINISHING:
                --finishingWorkersCount;
                break;

            case bdg::core::pipeline::Worker::State::STATE_FINISHED:
                --finishedWorkersCount;
                break;
        }

        switch (state)
        {
            case bdg::core::pipeline::Worker::State::STATE_IDLE:
                worker.setIdle();
                ++idleWorkersCount;
                break;

            case bdg::core::pipeline::Worker::State::STATE_PROCESSING:
                worker.setProcessing();
                ++processingWorkersCount;
                break;

            case bdg::core::pipeline::Worker::State::STATE_FINISHING:
                worker.setFinishing();
                ++finishingWorkersCount;
                break;

            case bdg::core::pipeline::Worker::State::STATE_FINISHED:
                worker.setFinished();
                ++finishedWorkersCount;
                break;
        }
    }
}

/**
 * Sets the worker to the processing state.
 *
 * @param workerRank Rank of the worker.
 * @param task Task that is processed on the worker.
 */
void bdg::core::pipeline::WorkerGroup::setWorkerProcessing(const int workerRank, std::unique_ptr<bdg::core::pipeline::Task> &task)
{
    setWorkerState(workerRank, Worker::State::STATE_PROCESSING);
    workers[workerRank].setProcessing(task);
}

/**
 * Serializes the information about the state of all workers. This method is used for checkpoints.
 *
 * @param json JSON object to which we serialize information about workers.
 */
void bdg::core::pipeline::WorkerGroup::serialize(Poco::JSON::Array &json) const
{
    for (int i = 0; i < workers.size(); ++i)
    {
        Poco::JSON::Object workerJson;
        workers[i].serialize(workerJson);
        json.add(workerJson);
    }
}

/**
 * Deserializes the information about the state of all workers. This method is used when the job
 * is restored from a checkpoint during error recovery.
 *
 * @param json JSON object which contains information about all workers.
 */
void bdg::core::pipeline::WorkerGroup::unserialize(const Poco::JSON::Array &json)
{
    for (int i = 0; i < workers.size() && i < json.size(); ++i)
    {
        workers[i].unserialize(*json.getObject(i), pipeline);
    }
}

/**
 * Creates a file with the schedule of all tasks processed during the job run.
 *
 * @param jobId Id of the job.
 * @param elapsedMicroseconds How much time did it take to finish the job in microseconds.
 */
void bdg::core::pipeline::WorkerGroup::storeTaskSchedule(const int jobId, const Poco::UInt64 elapsedMicroseconds) const
{
    const size_t intervalsPerSecond = 10;           // We want to use 0.1s as an interval in the graph.
    const size_t intervalLengthMicroseconds = 1000000 / intervalsPerSecond;
    const size_t workersCount = workers.size();
    const size_t countedIntervalsCount = (size_t)(elapsedMicroseconds / intervalLengthMicroseconds);
    const size_t intervalsCount = countedIntervalsCount > 0 ? countedIntervalsCount : 1;
    const Poco::Int64 startTime = Poco::Timestamp().epochMicroseconds() - elapsedMicroseconds;
    std::vector<std::vector<std::string>> schedule(intervalsCount);

    // Init schedule two-dimensional vector.
    for (int interval = 0; interval < intervalsCount; ++interval)
    {
        schedule[interval] = std::vector<std::string>(workersCount);
    }

    // Count the schedule.
    for (int workerRank = 0; workerRank < workersCount; ++workerRank)
    {
        Poco::Int64 taskStartedTimeStamp = 0;

        for (const auto &event : workers[workerRank].getEvents())
        {
            if (event.getName() == WorkerEvent::EVENT_TASK_STARTED)
            {
                taskStartedTimeStamp = event.getTime().epochMicroseconds();
            }
            else if (event.getName() == WorkerEvent::EVENT_TASK_FINISHED && taskStartedTimeStamp > 0)
            {
                const int startIntervalNotSanitized = (int)((taskStartedTimeStamp - startTime) / intervalLengthMicroseconds);
                const int endIntervalNotSanitized = (int)((taskStartedTimeStamp + event.getIntNote() - startTime) / intervalLengthMicroseconds);
                const int startInterval = std::min<int>(std::max<int>(0, startIntervalNotSanitized), intervalsCount - 1);
                const int endInterval = std::min<int>(std::max<int>(0, endIntervalNotSanitized), intervalsCount - 1);
                const int taskLength = endInterval - startInterval;

                // Mark down intervals when task was running.
                for (int i = startInterval; i <= endInterval; ++i)
                {
                    schedule[i][workerRank] = "X";
                }

                // Mark task start time and its length in seconds.
                schedule[startInterval][workerRank] = "T" + std::to_string(taskLength / intervalsPerSecond) + "." + std::to_string(taskLength % intervalsPerSecond);
                taskStartedTimeStamp = 0;
            }
        }
    }

    // Store the schedule.
    std::string scheduleFilePath = bdg::utils::Config::getInstance().getLogStoragePath() + Poco::Path::separator() + "log-" + std::to_string(jobId) + Poco::Path::separator() + "task-schedule.txt";
    std::ofstream scheduleFile(scheduleFilePath);

    // Create the schedule header.
    scheduleFile << "\t";

    for (int workerRank = 0; workerRank < workersCount; ++workerRank)
    {
        scheduleFile << "W" << workerRank << "\t";
    }

    scheduleFile << std::endl;

    // Create the schedule body.
    for (int interval = 0; interval < intervalsCount; ++interval)
    {
        scheduleFile << (interval / intervalsPerSecond) << "." << (interval % intervalsPerSecond) << "\t";

        for (int workerRank = 0; workerRank < workersCount; ++workerRank)
        {
            scheduleFile << schedule[interval][workerRank] << "\t";
        }

        scheduleFile << std::endl;
    }

    // Create the schedule footer.
    scheduleFile << "Tasks\t";

    for (int workerRank = 0; workerRank < workersCount; ++workerRank)
    {
        scheduleFile << workers[workerRank].getProcessedTasks() << "\t";
    }

    scheduleFile << std::endl;
    scheduleFile.close();
}
