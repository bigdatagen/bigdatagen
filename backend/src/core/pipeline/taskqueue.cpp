/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/taskqueue.h"                // Header

/**
 * Constructor of the task queue.
 *
 * @param pipeline Instance of the pipeline.
 * @param workersCount Numer of available workers.
 * @param pluginsCount Size of the pipeline - number of used pipeline plugins.
 */
bdg::core::pipeline::TaskQueue::TaskQueue(const bdg::core::pipeline::Pipeline &pipeline, const size_t workersCount, const size_t pluginsCount) :
    MultiQueue<Task>(pipeline.getInfo().getSchedulerInfo(), workersCount, pluginsCount, std::vector<bool>(pluginsCount, false)),
    pipeline(pipeline)
{
    // Nothing here.
}

/**
 * Finds a suitable task in the queue.
 *
 * @param requiredPluginIndex If this parameter is specified, only tasks for the plugin with this index are returned.
 * @param workerRank Rank of currently running MPI thread.
 *
 * @return Task selected from the queue.
 */
std::unique_ptr<bdg::core::pipeline::Task> bdg::core::pipeline::TaskQueue::pop(const int requiredPluginIndex, const int workerRank)
{
    // Do we require to return task for some plugin?
    if (requiredPluginIndex >= 0)
    {
        if (!queues[requiredPluginIndex].empty() && schedulerInfo.canRunPluginAtRank(requiredPluginIndex, workerRank))
        {
            return getTaskFromQueueFront(requiredPluginIndex);
        }
    }
    else
    {
        // Return task for any plugin.
        for (int pluginIndex = queues.size() - 1; pluginIndex >= 0; --pluginIndex)
        {
            if (!queues[pluginIndex].empty() && schedulerInfo.canRunPluginAtRank(pluginIndex, workerRank))
            {
                return getTaskFromQueueFront(pluginIndex);
            }
        }
    }

    return nullptr;
}

/**
 * Creates a task - this method is needed during error recovery.
 *
 * @param entryJson JSON representing the task.
 *
 * @return Task that is stored in the queue.
 */
bdg::core::pipeline::Task *bdg::core::pipeline::TaskQueue::createEntry(const Poco::JSON::Object::Ptr entryJson) const
{
    return pipeline.getPlugin(entryJson->getValue<int>("pluginIndex"))->taskFactory();
}

/**
 * Returns the task from the beginning of the queue for a given stage of the pipeline.
 *
 * @param pluginIndex Plugin index which indicates the stage of the pipeline.
 *
 * @return Task from the beginning of the queue.
 */
std::unique_ptr<bdg::core::pipeline::Task> bdg::core::pipeline::TaskQueue::getTaskFromQueueFront(const int pluginIndex)
{
    std::unique_ptr<bdg::core::pipeline::Task> task = std::move(queues[pluginIndex].front());
    queues[pluginIndex].pop_front();

    return task;
}
