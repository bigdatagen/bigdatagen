/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/schedulerinfo.h"                // Header
#include "core/pipeline/plugins/valuewithunit.h"        // ValueWithUnit

#include <Poco/Exception.h>             // Poco::InvalidArgumentException
#include <Poco/Random.h>                // Poco::Random
#include <Poco/String.h>                // Poco::toLower
#include <Poco/StringTokenizer.h>       // Poco::StringTokenizer
 
/**
 * Constructor of the scheduler info.
 *
 * @param clusterSize Size of the cluster where BDG runs.
 */
bdg::core::pipeline::SchedulerInfo::SchedulerInfo(const size_t clusterSize) :
    clusterSize(clusterSize)
{
    schedulerCheckpointPeriodSeconds = bdg::utils::Config::getInstance().getPipelineCheckpointPeriodSeconds();
    doCleanUp = bdg::utils::Config::getInstance().pipelineCleanUp();
    doContinuousCleanUp = bdg::utils::Config::getInstance().pipelineContinuousCleanUp();
    canUseTaskQueue = bdg::utils::Config::getInstance().pipelineUseTaskQueue();
}

/**
 * Sets one rank where plugin is allowed to run. If no ranks are specified for a plugin,
 * it can run on aritrary rank.
 *
 * @param pluginIndex Index of the plugin in the pipeline.
 * @param rank Rank where the plugin is allowed to run.
 */
void bdg::core::pipeline::SchedulerInfo::setWherePluginCanRun(const int pluginIndex, const int rank)
{
    if (wherePluginsCanRun.find(pluginIndex) == wherePluginsCanRun.end())
    {
        wherePluginsCanRun[pluginIndex] = std::vector<int>();
    }

    wherePluginsCanRun[pluginIndex].emplace_back(rank);
}

/**
 * Sets what plugin can run on the given rank. When no plugins are specified for a rank, any
 * plugins are allowed to run there.
 *
 * @param rank Rank of the MPI thread.
 * @param pluginIndex Index of the plugin which is allowed to run on the rank.
 */
void bdg::core::pipeline::SchedulerInfo::setWhatCanRunOnRank(const int rank, const int pluginIndex)
{
    if (whatCanRunOnRanks.find(rank) == whatCanRunOnRanks.end())
    {
        whatCanRunOnRanks[rank] = std::vector<int>();
    }

    whatCanRunOnRanks[rank].emplace_back(pluginIndex);
}

/**
 * Sets the generation speed.
 *
 * @param speedStr String specifying the generation speed. 
 */
void bdg::core::pipeline::SchedulerInfo::setFileFrequency(const std::string &speedStr)
{
    Poco::StringTokenizer parts(Poco::toLower(speedStr), "/", Poco::StringTokenizer::TOK_TRIM);

    if (parts.count() == 2)
    {
        std::string frequencyAmountStr = parts[0];
        std::string frequencyPeriodStr = parts[1];

        // Parse the amount.
        if (!frequencyAmountStr.empty())
        {
            ValueWithUnit amount(frequencyAmountStr);

            // Check the amount.
            if (amount.getValue() > 0)
            {
                // Set the amount.
                if (amount.getUnit() == ValueWithUnit::Unit::UNIT_BYTE)
                {
                    fileFrequency.setBytes(amount.getValue());
                }
                // When the unit is file or undefined, we use the unit of file - it is a default unit.
                else if (amount.getUnit() == ValueWithUnit::Unit::UNIT_FILE)
                {
                    fileFrequency.setFiles(amount.getValue());
                }
            }
        }
            
        // Parse the period.
        if (!frequencyPeriodStr.empty())
        {
            // Allow only units for time interval.
            if (frequencyPeriodStr == "s" || frequencyPeriodStr == "sec" || frequencyPeriodStr == "second"
                || frequencyPeriodStr == "i" || frequencyPeriodStr == "min" || frequencyPeriodStr == "minute"
                || frequencyPeriodStr == "h" || frequencyPeriodStr == "hour"
                || frequencyPeriodStr == "d" || frequencyPeriodStr == "day"
                || frequencyPeriodStr == "w" || frequencyPeriodStr == "week"
                || frequencyPeriodStr == "m" || frequencyPeriodStr == "month"
                || frequencyPeriodStr == "y" || frequencyPeriodStr == "year")
            {
                frequencyPeriodStr = "1" + frequencyPeriodStr;
            }

            // Parse the period.
            ValueWithUnit period(frequencyPeriodStr);

            // Check the period.
            if (period.getValue() > 0 && period.getUnit() == ValueWithUnit::Unit::UNIT_SECOND)
            {
                fileFrequency.setSeconds(period.getValue());
            }
        }
    }
}

/**
 * Sets the generation target.
 *
 * @param totalStr String specifying the generation target.
 */
void bdg::core::pipeline::SchedulerInfo::setTotal(const std::string &totalStr)
{
    total = ValueWithUnit(totalStr);

    // Default unit is file.
    if (total.getValue() > 0 && total.getUnit() == ValueWithUnit::Unit::UNIT_UNDEFINED)
    {
        total.setUnit(ValueWithUnit::Unit::UNIT_FILE);
    }
}

/**
 * Loads the scheduler configuration from the given JSON object.
 *
 * @param schedulerConfiguration JSON object with scheduler configuration.
 */
void bdg::core::pipeline::SchedulerInfo::setSchedulerConfiguration(const Poco::JSON::Object::Ptr schedulerConfiguration)
{
    if (!schedulerConfiguration.isNull())
    {
        if (schedulerConfiguration->has("filesPerTask") && (schedulerConfiguration->get("filesPerTask").isInteger() || schedulerConfiguration->get("filesPerTask").isString()))
        {
            int newFilesPerTask = 0;

            if (schedulerConfiguration->get("filesPerTask").isInteger())
            {
                newFilesPerTask = schedulerConfiguration->getValue<int>("filesPerTask");
            }
            else
            {
                newFilesPerTask = atoi(schedulerConfiguration->getValue<std::string>("filesPerTask").c_str());
            }

            filesPerTask = std::max<int>(0, std::min<int>(newFilesPerTask, 10000000));
        }

        if (schedulerConfiguration->has("filesPerDirectory") && (schedulerConfiguration->get("filesPerDirectory").isInteger() || schedulerConfiguration->get("filesPerDirectory").isString()))
        {
            int newFilesPerDirectory = 0;
            
            if (schedulerConfiguration->get("filesPerDirectory").isInteger())
            {
                newFilesPerDirectory = schedulerConfiguration->getValue<int>("filesPerDirectory");
            }
            else
            {
                newFilesPerDirectory = atoi(schedulerConfiguration->getValue<std::string>("filesPerDirectory").c_str());
            }
            
            filesPerDirectory = std::max<int>(32, std::min<int>(newFilesPerDirectory, 10000000));
        }

        if (schedulerConfiguration->has("checkpointPeriod") && (schedulerConfiguration->get("checkpointPeriod").isInteger() || schedulerConfiguration->get("checkpointPeriod").isString()))
        {
            int newCheckpointPeriodSeconds = 0;

            if (schedulerConfiguration->get("checkpointPeriod").isInteger())
            {
                newCheckpointPeriodSeconds = schedulerConfiguration->getValue<int>("checkpointPeriod");
            }
            else
            {
                newCheckpointPeriodSeconds = atoi(schedulerConfiguration->getValue<std::string>("checkpointPeriod").c_str());
            }

            schedulerCheckpointPeriodSeconds = std::max<int>(0, std::min<int>(newCheckpointPeriodSeconds, 24 * 3600));
        }

        if (schedulerConfiguration->has("displayGeneratedFiles"))
        {
            doDisplayGeneratedFiles = schedulerConfiguration->getValue<bool>("displayGeneratedFiles");
        }

        if (schedulerConfiguration->has("preserveOrder"))
        {
            doPreserveFilesOrder = schedulerConfiguration->getValue<bool>("preserveOrder");
        }

        if (schedulerConfiguration->has("cleanUp"))
        {
            doCleanUp = schedulerConfiguration->getValue<bool>("cleanUp");
        }

        if (schedulerConfiguration->has("continuousCleanUp"))
        {
            doContinuousCleanUp = schedulerConfiguration->getValue<bool>("continuousCleanUp");
        }

        if (schedulerConfiguration->has("useTaskQueue"))
        {
            canUseTaskQueue = schedulerConfiguration->getValue<bool>("useTaskQueue");
        }

        if (schedulerConfiguration->has("useStorerPipelineOptimization"))
        {
            canUseStorerPipelineOptimization = schedulerConfiguration->getValue<bool>("useStorerPipelineOptimization");
        }

        if (schedulerConfiguration->has("useProgressiveStorersCount"))
        {
            canUseProgressiveStorersCount = schedulerConfiguration->getValue<bool>("useProgressiveStorersCount");
        }

        if (schedulerConfiguration->has("requirePipelineTasksOnSameNode"))
        {
            requirePipelineTasksOnSameNode = schedulerConfiguration->getValue<bool>("requirePipelineTasksOnSameNode");
        }

        // If we want to preserve order of generated files, we cannot use task queue, because in this
        // queue tasks could get mixed with newly generated tasks and it would be very difficult if
        // not impossible to preserve the order.
        if (doPreserveFilesOrder)
        {
            canUseTaskQueue = false;
        }
    }
}

/**
 * Returns whether a given plugin is allowed to run on a given rank.
 *
 * @param pluginIndex Index of the plugin in the pipeline.
 * @param rank Rank of the MPI thread.
 *
 * @return Can a plugin run on the given rank?
 */
bool bdg::core::pipeline::SchedulerInfo::canRunPluginAtRank(const int pluginIndex, const int rank) const
{
    // Rank 0 means not specified, i.e. we do not care at which rank the plugin will run.
    if (rank <= 0)
    {
        return true;
    }

    // Find if the plugin can run on certain rank and vice versa.
    IntToVectorMap::const_iterator plugIt = wherePluginsCanRun.find(pluginIndex);
    
    if (plugIt != wherePluginsCanRun.end() && std::find(plugIt->second.begin(), plugIt->second.end(), rank) == plugIt->second.end())
    {
        return false;
    }

    IntToVectorMap::const_iterator rankIt = whatCanRunOnRanks.find(rank);

    if (rankIt != whatCanRunOnRanks.end() && std::find(rankIt->second.begin(), rankIt->second.end(), pluginIndex) == rankIt->second.end())
    {
        return false;
    }

    return true;
}

/**
 * Simple algorithm to find the rank that is "least used" in the scheduler. This method
 * returns the rank that is mentioned the least number of times in the scheduler metadata.
 * We prefer a rank that is mentioned 0 times, i.e. we can dedicate it to a certain plugin.
 *
 * @return Least used rank of a MPI thread.
 */
int bdg::core::pipeline::SchedulerInfo::findLeastBusyRank() const
{
    int leastBusyRank = 0;
    int leastRankUsage = INT_MAX;

    // Compute the usage of ranks.
    for (int rank = 1; rank < clusterSize; ++rank)
    {
        int rankUsage = 0;

        // Find the usage of the rank.
        for (IntToVectorMap::const_iterator it = wherePluginsCanRun.begin(); it != wherePluginsCanRun.end(); ++it)
        {
            for (std::vector<int>::const_iterator rankIt = it->second.begin(); rankIt != it->second.end(); ++rankIt)
            {
                if ((*rankIt) == rank)
                {
                    ++rankUsage;
                }
            }
        }

        for (IntToVectorMap::const_iterator it = whatCanRunOnRanks.begin(); it != whatCanRunOnRanks.end(); ++it)
        {
            if (it->first == rank)
            {
                ++rankUsage;
            }
        }

        // We have found a rank which is used less.
        if (rankUsage < leastRankUsage)
        {
            leastBusyRank = rank;
            leastRankUsage = rankUsage;
        }
    }

    return leastBusyRank;
}

/**
 * Returns a random rank on which the plugin is allowed to run. This rank is preferably
 * different than a given rank. This method is used to find a different thread to run
 * at for a task when it is restarted. We want to pick a different rank, so we increase
 * the probability that task does not end with error again.
 *
 * @param pluginIndex Index of the plugin in the pipeline.
 * @param defaultRank Rank on which the plugin has just run.
 *
 * @return Random rank on which the plugin is allowed to run.
 */
int bdg::core::pipeline::SchedulerInfo::getRandomRankForPlugin(const int pluginIndex, const int defaultRank) const
{
    static Poco::Random random;
    const size_t attempts = std::max<size_t>(4, std::min<size_t>(clusterSize, 25));

    for (int i = 0; i < attempts; ++i)
    {
        const int rank = random.next(clusterSize - 1) + 1;

        if (rank != defaultRank && canRunPluginAtRank(pluginIndex, rank))
        {
            return rank;
        }
    }

    return defaultRank;
}

/**
 * Resets loaded scheduler configuration.
 */
void bdg::core::pipeline::SchedulerInfo::reset()
{
    wherePluginsCanRun.clear();
    whatCanRunOnRanks.clear();
}

/**
 * Method for debugging of mapping between plugins and ranks - what plugins can run
 * on what ranks and vice versa.
 *
 * @param output If set to true, debug information is outputed to stdout.
 *
 * @return Debug info.
 */
std::string bdg::core::pipeline::SchedulerInfo::debug(const bool output) const
{
    std::string debug = "Where plugins can run:\n";

    for (IntToVectorMap::const_iterator it = wherePluginsCanRun.begin(); it != wherePluginsCanRun.end(); ++it)
    {
        debug += "Plugin " + std::to_string(it->first) + ": ";

        for (std::vector<int>::const_iterator rankIt = it->second.begin(); rankIt != it->second.end(); ++rankIt)
        {
            debug += std::to_string(*rankIt) + " ";
        }

        debug += "\n";
    }

    debug += "What can run on ranks:\n";

    for (IntToVectorMap::const_iterator it = whatCanRunOnRanks.begin(); it != whatCanRunOnRanks.end(); ++it)
    {
        debug += "Rank " + std::to_string(it->first) + ": ";

        for (std::vector<int>::const_iterator plugIt = it->second.begin(); plugIt != it->second.end(); ++plugIt)
        {
            debug += std::to_string(*plugIt) + " ";
        }

        debug += "\n";
    }

    if (output)
    {
        std::cout << debug;
    }

    return debug;
}
