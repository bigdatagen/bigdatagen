/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/resultqueue.h"          // Header

/**
 * Constructor of the result queue.
 *
 * @param mpi MPI wrapper.
 * @param schedulerInfo Info about scheduling.
 * @param workersCount Numer of available workers.
 * @param pluginsCount Size of the pipeline - number of used pipeline plugins.
 * @param useAsPriorityQueue Should we use the queue as the priority queue?
 */
bdg::core::pipeline::ResultQueue::ResultQueue(
    const bdg::utils::MPIProxy &mpi,
    const bdg::core::pipeline::SchedulerInfo &schedulerInfo,
    const size_t workersCount,
    const size_t pluginsCount,
    const std::vector<bool> &useAsPriorityQueue
) :
    MultiQueue<TaskResult>(schedulerInfo, workersCount, pluginsCount, useAsPriorityQueue),
    mpi(mpi),
    requiredFileFromIds(pluginsCount, 1)
{
    // Nothing here.
}

/**
 * Adds a new task result to the end of the queue.
 *
 * @param result Task result added to the queue.
 */
void bdg::core::pipeline::ResultQueue::push(std::unique_ptr<bdg::core::pipeline::TaskResult> &result)
{
    const int pluginIndex = result->getPluginIndex();

    MultiQueue<TaskResult>::push(result);
    removeErrorsFromBeginningOfQueue(pluginIndex);
}

/**
 * Finds a suitable result in the queue.
 *
 * @param requiredPluginIndex If this parameter is specified, only task results from the plugin with this index-1 are returned.
 * @param workerRank Rank of currently running MPI thread.
 *
 * @return Task result selected from the queue.
 */
std::unique_ptr<bdg::core::pipeline::TaskResult> bdg::core::pipeline::ResultQueue::pop(const int requiredPluginIndex, const int workerRank)
{
    // Do we require to return result for some plugin?
    if (requiredPluginIndex >= 0)
    {
        const int pluginIndex = requiredPluginIndex - 1;

        if (requiredPluginIndex > 0 && !queues[pluginIndex].empty() && schedulerInfo.canRunPluginAtRank(requiredPluginIndex, workerRank))
        {
            // Prefer the result from the same rank as required.
            if (!useAsPriorityQueue[pluginIndex] && queues[pluginIndex].back()->getRank() == workerRank)
            {
                return getResultFromQueueBack(pluginIndex);
            }
            else
            {
                // If we use the queue as a priority queue, we have to wait for the result with the required file
                // id to prevent mixing of results - our goal is to preserve order of generated files.
                if (canPopFromFront(pluginIndex, workerRank))
                {
                    return getResultFromQueueFront(pluginIndex, workerRank);
                }
            }
        }
    }
    // Return result for any plugin.
    else
    {
        for (int pluginIndex = queues.size() - 1; pluginIndex >= 0; --pluginIndex)
        {
            if (!queues[pluginIndex].empty() && schedulerInfo.canRunPluginAtRank(pluginIndex + 1, workerRank))
            {
                // If we use the queue as a priority queue, we have to wait for the result with the required file
                // id to prevent mixing of results - our goal is to preserve order of generated files.
                if (canPopFromFront(pluginIndex, workerRank))
                {
                    return getResultFromQueueFront(pluginIndex, workerRank);
                }
            }
        }
    }

    return nullptr;
}

/**
 * Returns the success result from the queue. This method guarantees that error task results
 * are first removed from the begining of the queue.
 *
 * @param requiredPluginIndex If this parameter is specified, only task results from the plugin with this index-1 are returned.
 * @param workerRank Rank of currently running MPI thread.
 *
 * @return Task result selected from the queue.
 */
std::unique_ptr<bdg::core::pipeline::SuccessTaskResult> bdg::core::pipeline::ResultQueue::popSuccessResult(const int requiredPluginIndex, const int workerRank)
{
    const int pluginIndex = requiredPluginIndex - 1;

    // Throw away all the error task results.
    if (pluginIndex >= 0)
    {
        removeErrorsFromBeginningOfQueue(pluginIndex);
    }

    std::unique_ptr<TaskResult> result = pop(requiredPluginIndex, workerRank);

    // There are no suitable task results in the queue.
    if (result == nullptr)
    {
        return std::unique_ptr<SuccessTaskResult>();
    }
    // We can be sure that result has type == TaskResult::ResultType::RESULT_TYPE_SUCCESSS.
    else if (result->getType() == TaskResult::ResultType::RESULT_TYPE_SUCCESSS)
    {
        std::unique_ptr<SuccessTaskResult> successResult((SuccessTaskResult*)result.get());
        result.release();

        return successResult;
    }
    // This should never happen.
    else
    {
        std::cerr << "FATAL ERROR: Got error result from queue " << std::to_string(pluginIndex) << "; File from: " << std::to_string(result->getFileFrom()) << "; Required file id: " << std::to_string(requiredFileFromIds[pluginIndex]) << std::endl;
        throw new Poco::IllegalStateException("Got error result - algorithm fault");
    }
}

/**
 * Serializes the result queue.
 *
 * @param json JSON where the queue is going to be serialized.
 */
void bdg::core::pipeline::ResultQueue::serialize(Poco::JSON::Object &json) const
{
    MultiQueue<TaskResult>::serialize(json);
    Poco::JSON::Array requiredFileFromIdsJson;

    for (int i = 0; i < requiredFileFromIds.size(); ++i)
    {
        requiredFileFromIdsJson.add(requiredFileFromIds[i]);
    }

    json.set("requiredFileFromIds", requiredFileFromIdsJson);
}

/**
 * Deserializes the result queue.
 *
 * @param json JSON with serialized result queue.
 */
void bdg::core::pipeline::ResultQueue::unserialize(const Poco::JSON::Object &json)
{
    MultiQueue<TaskResult>::unserialize(json);
    Poco::JSON::Array::Ptr requiredFileFromIdsJson = json.getArray("requiredFileFromIds");
    
    for (int i = 0; i < requiredFileFromIds.size() && i < requiredFileFromIdsJson->size(); ++i)
    {
        requiredFileFromIds[i] = requiredFileFromIdsJson->getElement<Poco::UInt64>(i);
    }
}

/**
 * Creates a task result - this method is needed during error recovery.
 *
 * @param entryJson JSON representing the task result.
 *
 * @return Task result that is stored in the queue.
 */
bdg::core::pipeline::TaskResult *bdg::core::pipeline::ResultQueue::createEntry(const Poco::JSON::Object::Ptr entryJson) const
{
    if (entryJson->has("msg"))
    {
        return new ErrorTaskResult;
    }
    else
    {
        return new SuccessTaskResult;
    }
}

/**
 * Returns the task result from the end of the queue.
 *
 * @param pluginIndex Index of the inner queue.
 *
 * @return Task result from the end of the queue.
 */
std::unique_ptr<bdg::core::pipeline::TaskResult> bdg::core::pipeline::ResultQueue::getResultFromQueueBack(const int pluginIndex)
{
    std::unique_ptr<bdg::core::pipeline::TaskResult> result = std::move(queues[pluginIndex].back());
    queues[pluginIndex].pop_back();

    return result;
}

/**
 * Returns the task result from the beginning of the queue.
 *
 * @param pluginIndex Index of the inner queue.
 * @param workerRank Rank of currently running MPI thread.
 *
 * @return Task result from the beginning of the queue.
 */
std::unique_ptr<bdg::core::pipeline::TaskResult> bdg::core::pipeline::ResultQueue::getResultFromQueueFront(const int pluginIndex, const int workerRank)
{
    if (!useAsPriorityQueue[pluginIndex] && schedulerInfo.requiresPipelineTasksOnSameNode() && workerRank >= 0)
    {
        int index = 0;

        // Loop over the queue and try to find task result which can be ran on the same physical node
        // as worker which demands a result runs.
        for (auto &queueResult : queues[pluginIndex])
        {
            if (mpi.areOnSameNode(queueResult->getRank(), workerRank))
            {
                std::unique_ptr<bdg::core::pipeline::TaskResult> result = std::move(queueResult);
                queues[pluginIndex].erase(queues[pluginIndex].begin() + index);
                
                return result;
            }

            ++index;
        }

        return nullptr;
    }
    else
    {
        std::unique_ptr<bdg::core::pipeline::TaskResult> result = std::move(queues[pluginIndex].front());
        queues[pluginIndex].pop_front();
        requiredFileFromIds[pluginIndex] += result->getFilesCount();

        return result;
    }
}

/**
 * Returns whether we can return the task result from the from the beginning of the queue. This
 * is when we do not use the queue as the priority queue or when the task result in the beginning
 * of the queue is the one that is now on the turn.
 *
 * @param pluginIndex Index of the inner queue.
 * @param workerRank Rank of currently running MPI thread.
 *
 * @return Can we return the task result from the from the beginning of the queue?
 */
bool bdg::core::pipeline::ResultQueue::canPopFromFront(const int pluginIndex, const int workerRank)
{
    return !useAsPriorityQueue[pluginIndex] || (useAsPriorityQueue[pluginIndex] && queues[pluginIndex].front()->getFileFrom() <= requiredFileFromIds[pluginIndex]);
}

/**
 * Removes all the error task results from the beginning of the inner result queue.
 *
 * @param pluginIndex Index of the inner queue.
 */
void bdg::core::pipeline::ResultQueue::removeErrorsFromBeginningOfQueue(const int pluginIndex)
{
    while (!queues[pluginIndex].empty()
        && queues[pluginIndex].front()->getType() != TaskResult::ResultType::RESULT_TYPE_SUCCESSS
        && canPopFromFront(pluginIndex, -1))
    {
        getResultFromQueueFront(pluginIndex, -1);
    }
}
