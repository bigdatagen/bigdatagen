/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Pipeline
 * @author   Big Data Generator Team
 */

#include "core/pipeline/pipelineinfocheckpointer.h"         // Header

/**
 * Constructor of the checkpointer.
 *
 * @param pipelineJobId Id of currently running job.
 */
bdg::core::pipeline::PipelineInfoCheckpointer::PipelineInfoCheckpointer(const int pipelineJobId):
    Checkpointer(pipelineJobId)
{
    // Nothing here.
}

/**
 * Stores the job configuration into the job configuration file.
 *
 * @param pipelineInfoConfiguration JSON string with the job configuration.
 */
void bdg::core::pipeline::PipelineInfoCheckpointer::store(const std::string &pipelineInfoConfiguration) const
{
    createStorageDir();
    writeToFile(pipelineInfoConfiguration);
}
