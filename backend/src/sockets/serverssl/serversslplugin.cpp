/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Sockets
 * @author   Big Data Generator Team
 */

#include "sockets/serverssl/serversslplugin.h"      // Header
#include "utils/config.h"                           // Config

#include <Poco/Net/KeyFileHandler.h>                // Poco::Net::KeyFileHandler
#include <Poco/Net/AcceptCertificateHandler.h>      // Poco::Net::AcceptCertificateHandler
#include <Poco/Net/Context.h>                       // Poco::Net::Context
#include <Poco/Net/SSLManager.h>                    // Poco::Net::SSLManager
#include <Poco/Net/SecureServerSocket.h>            // Poco::Net::SecureServerSocket
#include <Poco/String.h>                            // Poco::toLower
#include <Poco/ClassLibrary.h>                      // POCO Export class

/**
 * Constructor of the plugin which initializes SSL.
 */
bdg::sockets::ServerSslPlugin::ServerSslPlugin()
{
    bdg::utils::Config &config = bdg::utils::Config::getInstance();
     
    Poco::Net::Context::VerificationMode verificationMode = Poco::Net::Context::VERIFY_RELAXED;
    const std::string verificationModeStr = Poco::toLower(config.getServerHttpsVerificationMode());
    
    if (verificationModeStr == "none")
    {
        verificationMode = Poco::Net::Context::VERIFY_NONE;
    }
    else if (verificationModeStr == "strict")
    {
        verificationMode = Poco::Net::Context::VERIFY_STRICT;
    }
    else if (verificationModeStr == "once")
    {
        verificationMode = Poco::Net::Context::VERIFY_ONCE;
    }

    Poco::Net::initializeSSL();
    Poco::Net::SSLManager::PrivateKeyPassphraseHandlerPtr passphraseHandler(new Poco::Net::KeyFileHandler(true));
    Poco::Net::SSLManager::InvalidCertificateHandlerPtr certificateHandler(new Poco::Net::AcceptCertificateHandler(true));
    Poco::Net::Context::Ptr context(new Poco::Net::Context(Poco::Net::Context::SERVER_USE, config.getServerHttpsPrivateKeyFile(), config.getServerHttpsCertificateFile(), config.getServerHttpsCaLocation(), verificationMode, 9, config.getServerHttpsLoadDefaultCAs(), "ALL:!ADH:!LOW:!EXP:!MD5:@STRENGTH"));
    Poco::Net::SSLManager::instance().initializeServer(passphraseHandler, certificateHandler, context);
}

/**
 * Creates a secured server socket.
 *
 * @return Secured server socket.
 */
Poco::Net::ServerSocket *bdg::sockets::ServerSslPlugin::createSocket()
{
    return new Poco::Net::SecureServerSocket(bdg::utils::Config::getInstance().getServerPort());
}

using bdg::server::SocketPlugin;
using bdg::sockets::ServerSslPlugin;

/**
 * Export the plugin for the POCO class loader.
 */
POCO_BEGIN_MANIFEST(SocketPlugin)
    POCO_EXPORT_CLASS(ServerSslPlugin)
POCO_END_MANIFEST
