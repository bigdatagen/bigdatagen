/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Components
 * @author   Big Data Generator Team
 */

#include "components/pipelinepluginmanager/pipelinepluginmanager.h"     // Header
#include "utils/commandline.h"                                          // CommandLine
#include "utils/config.h"                                               // Config

#include <Poco/Process.h>               // Poco::Process
#include <Poco/PipeStream.h>            // Poco::PipeInputStream
#include <Poco/JSON/Object.h>           // Poco::JSON::Object

/**
 * Method called by the server to process the plugin code. Each HTTP request to the server
 * requiring this plugin results in one call of this method. This plugin calls the
 * PipelinePluginManager program in order to obtain JSON with information about all available
 * pipeline plugins and their settings.
 *
 * @param parameters Parameters for the plugin run.
 * @param uploadedFiles Collection of uploaded files.
 *
 * @return Result of the plugin run.
 */
std::string bdg::components::PipelinePluginManager::run(const bdg::server::ServerPlugin::Parameters &parameters, const bdg::utils::FileUpload::FileUploadCollection &uploadedFiles)
{
    Poco::Pipe outputPipe;
    Poco::PipeInputStream outputPipeStream(outputPipe);
    std::stringstream responseStream;
    
    try
    {
        std::vector<std::string> args;
        const std::string origProgramCmd = bdg::utils::Config::getInstance().getPipelinePluginManagerProgram();
        const std::string programCmd = bdg::utils::CommandLine::parseCommandLineArguments(origProgramCmd, args);

        Poco::ProcessHandle programProcessHndl = Poco::Process::launch(programCmd, args, nullptr, &outputPipe, &outputPipe);
        Poco::StreamCopier::copyStream(outputPipeStream, responseStream, 64);

        int programReturnValue = Poco::Process::wait(programProcessHndl);

        if (programReturnValue != 0)
        {
            throw Poco::IllegalStateException("Error while retrieving pipeline plugins");
        }
    }
    catch (Poco::Exception &e)
    {
        Poco::JSON::Object response;
        response.set("error", e.displayText());
        response.stringify(responseStream);
    }

    return responseStream.str();
}

using bdg::server::ServerPlugin;
using bdg::components::PipelinePluginManager;

/**
 * Export the plugin for the POCO class loader.
 */
POCO_BEGIN_MANIFEST(ServerPlugin)
    POCO_EXPORT_CLASS(PipelinePluginManager)
POCO_END_MANIFEST
