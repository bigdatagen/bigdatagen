/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Components
 * @author   Big Data Generator Team
 */

#include "components/pipelinelauncher/pipelinelauncher.h"       // Header
#include "core/pipeline/pipelineinfocheckpointer.h"             // PipelinInfoCheckpointer

#include <Poco/JSON/Parser.h>           // Poco::JSON::Parser

const std::string bdg::components::PipelineLauncher::PARAM_PIPELINE_CONFIGURATION = "configuration";

/**
 * Method called by the server to process the plugin code. Each HTTP request to the server
 * requiring this plugin results in one call of this method. This plugin provides an interface
 * to launch a job. A job is one run of the pipeline program with a given configuration. Each
 * job can be paused, re-launched or stopped.
 *
 * @param parameters Parameters for the plugin run.
 * @param uploadedFiles Collection of uploaded files.
 *
 * @return Result of the plugin run.
 */
std::string bdg::components::PipelineLauncher::run(
    const bdg::server::ServerPlugin::Parameters &parameters,
    const bdg::utils::FileUpload::FileUploadCollection &uploadedFiles)
{
    // Get the command to process.
    const auto configurationIt = parameters.find(PARAM_PIPELINE_CONFIGURATION);
    Poco::JSON::Object::Ptr configurationJson;
    std::string configuration = "";
    std::string command = "launch";

    if (configurationIt != parameters.end())
    {
        try
        {
            configuration = configurationIt->second;
            Poco::Dynamic::Var configurationVar = Poco::JSON::Parser().parse(configuration);
            configurationJson = configurationVar.extract<Poco::JSON::Object::Ptr>();

            if (configurationJson->has("command"))
            {
                command = configurationJson->getValue<std::string>("command");
            }
        }
        catch (...)
        {
            // We just need to catch exceptions caused by invalid JSON.
        }
    }

    // Process the command.
    if (command == "launch")
    {
        return processCommandLaunch(parameters, configurationJson, configuration);
    }
    else if (command == "status")
    {
        return processCommandStatus(configurationJson);
    }
    else if (command == "play")
    {
        return processCommandPlay(configurationJson);
    }
    else if (command == "pause")
    {
        return processCommandPause(configurationJson);
    }
    else if (command == "stop")
    {
        return processCommandStop(configurationJson);
    }
    else if (command == "listjobs")
    {
        return processCommandListJobs(configurationJson);
    }
    else
    {
        return R"({"error": "Invalid command [)" + command + R"(]"})";
    }
}

/**
 * Launches a new job.
 *
 * @param parameters Parameters for the job run.
 * @param configurationJson Job configuration in JSON.
 * @param configuration Job configuration as string.
 *
 * @return Response to the call - JSON string.
 */
std::string bdg::components::PipelineLauncher::processCommandLaunch(
    const bdg::server::ServerPlugin::Parameters &parameters,
    const Poco::JSON::Object::Ptr configurationJson,
    const std::string &configuration)
{
    const int demandedJobId = !configurationJson.isNull() && configurationJson->has("jobid") ? std::atoi(configurationJson->getValue<std::string>("jobid").c_str()) : 0;
    size_t clusterSize = 0;
    bool waitForResponse = false;
    int jobId = 0;

    // Copy program output immediately to std output?
    if (parameters.find("waitForResponse") != parameters.end() && (Poco::toLower(parameters.find("waitForResponse")->second) == "true" || parameters.find("waitForResponse")->second == "1"))
    {
        waitForResponse = true;
    }

    // Do we have a path to the pipeline configuration?
    if (parameters.find("path") != parameters.end() || demandedJobId > 0)
    {
        const std::string path = demandedJobId > 0 ? std::to_string(demandedJobId) : parameters.find("path")->second;
        const std::string error = loadParametersFromFile(path, jobId, clusterSize, waitForResponse);

        if (!error.empty())
        {
            return R"({"error": ")" + error + R"("})";
        }
    }
    // Try to load pipeline info from passed parameters.
    else
    {
        const std::string error = loadParametersFromPipelineConfiguration(configurationJson, configuration, jobId, clusterSize, waitForResponse);

        if (!error.empty())
        {
            return R"({"error": ")" + error + R"("})";
        }
    }

    // Get the cluster size - it has a higher priority than cluster size set in the configuration file.
    if (parameters.find("clusterSize") != parameters.end())
    {
        clusterSize = atoi(parameters.find("clusterSize")->second.c_str());
    }

    Poco::Mutex::ScopedLock lock(jobsMutex);        // Lock the jobs collection.

    // Run the pipeline.
    if (jobs.find(jobId) == jobs.end())
    {
        Job *job = new Job(jobId, clusterSize, waitForResponse);
        jobs.emplace(jobId, std::unique_ptr<Job>(job));

        if (waitForResponse)
        {
            job->wait();
        }

        return R"({"jobid": ")" + std::to_string(jobId) + R"(", "state": "running"})";
    }
    else
    {
        return R"({"error": "Duplicate job id )" + std::to_string(jobId) + R"("})";
    }
}

/**
 * Returns status of the job with an id given in the job configuration. If no such job
 * exists, an error is returned.
 *
 * @param configurationJson Job configuration in JSON.
 *
 * @return Response to the call - JSON string.
 */
std::string bdg::components::PipelineLauncher::processCommandStatus(const Poco::JSON::Object::Ptr configurationJson)
{
    Job *job = getJob(configurationJson);

    if (job != nullptr)
    {
        Poco::JSON::Object stateJson = job->getJobState();

        std::stringstream responseStream;
        stateJson.stringify(responseStream);

        return responseStream.str();
    }
    else
    {
        return R"({"error": "Job not found"})";
    }
}

/**
 * Pauses the job with an id given in the job configuration. If no such job exists, an error is returned.
 *
 * @param configurationJson Job configuration in JSON.
 *
 * @return Response to the call - JSON string.
 */
std::string bdg::components::PipelineLauncher::processCommandPlay(const Poco::JSON::Object::Ptr configurationJson)
{
    Job *job = getJob(configurationJson);

    if (job != nullptr)
    {
        return job->play();
    }
    else
    {
        return R"({"error": "Job not found"})";
    }
}

/**
 * Re-launches the job with an id given in the job configuration. If no such job exists, an error is returned.
 *
 * @param configurationJson Job configuration in JSON.
 *
 * @return Response to the call - JSON string.
 */
std::string bdg::components::PipelineLauncher::processCommandPause(const Poco::JSON::Object::Ptr configurationJson)
{
    Job *job = getJob(configurationJson);

    if (job != nullptr)
    {
        return job->pause();
    }
    else
    {
        return R"({"error": "Job not found"})";
    }
}

/**
 * Stops the job with an id given in the job configuration. If no such job exists, an error is returned.
 *
 * @param configurationJson Job configuration in JSON.
 *
 * @return Response to the call - JSON string.
 */
std::string bdg::components::PipelineLauncher::processCommandStop(const Poco::JSON::Object::Ptr configurationJson)
{
    Job *job = getJob(configurationJson);

    if (job != nullptr)
    {
        return job->stop();
    }
    else
    {
        return R"({"error": "Job not found"})";
    }
}

/**
 * Returns a JSON with all the jobs - running, paused and stopped.
 *
 * @param configurationJson Job configuration in JSON.
 *
 * @return Response to the call - JSON string.
 */
std::string bdg::components::PipelineLauncher::processCommandListJobs(const Poco::JSON::Object::Ptr configurationJson)
{
    Poco::JSON::Object resultJson;
    Poco::JSON::Array jobsJson;

    size_t runningJobs = 0;
    size_t pausedJobs = 0;
    size_t finishedJobs = 0;

    Poco::Mutex::ScopedLock lock(jobsMutex);        // Lock the jobs collection.

    for (auto &job : jobs)
    {
        switch (job.second->getState())
        {
            case Job::State::STATE_RUNNING:
                ++runningJobs;
                break;

            case Job::State::STATE_PAUSED:
                ++pausedJobs;
                break;

            case Job::State::STATE_FINISHED:
                ++finishedJobs;
                break;
        }

        Poco::JSON::Object jobJson;
        jobJson.set("jobid", job.second->getId());
        jobJson.set("state", job.second->getStateStr());
        jobsJson.add(jobJson);
    }

    resultJson.set("jobs", jobsJson);
    resultJson.set("runningJobs", runningJobs);
    resultJson.set("pausedJobs", pausedJobs);
    resultJson.set("finishedJobs", finishedJobs);

    std::stringstream responseStream;
    resultJson.stringify(responseStream);

    return responseStream.str();
}

/**
 * Loads parameters for the job run from the job configuration file. This method is used when the
 * job is launched from the console.
 *
 * @param path Path to the job configuration file.
 * @param jobId Output parameter for the id of the job.
 * @param clusterSize Output parameter for number of MPI threads used in the job.
 * @param waitForResponse If set to true, all the errors that might occur are shown on the console.
 *
 * @return Empty string when everithing is all right, an error otherwise.
 */
std::string bdg::components::PipelineLauncher::loadParametersFromFile(
    const std::string &path,
    int &jobId,
    size_t &clusterSize,
    const bool waitForResponse)
{
    const int demandedJobId = std::atoi(path.c_str());
    std::string pathToLoad = "";
    
    // We have a job id of a job we want to launch.
    if (demandedJobId > 0)
    {
        pathToLoad = Poco::Path(bdg::core::pipeline::PipelineInfoCheckpointer(demandedJobId).getPathToFile()).absolute().toString();
        const Poco::File checkpointerFile(pathToLoad);

        if (checkpointerFile.exists() && checkpointerFile.isFile())
        {
            jobId = demandedJobId;
        }
        else
        {
            if (waitForResponse)
            {
                std::cerr << "ERROR: Job configuration file [" << pathToLoad << "] does not exists!" << std::endl;
            }
            
            return "Job configuration file [" + pathToLoad + "] does not exists";
        }
    }
    // We know that the path parameter was not an int - we try to look at it as on the path
    // and copy that file to the randomly generated checkpointer file and thus obtain a job id.
    else
    {
        pathToLoad = Poco::Path(path).absolute().toString();
        const Poco::File configFile(pathToLoad);

        if (configFile.exists() && configFile.isFile())
        {
            try
            {
                bdg::core::pipeline::PipelineInfoCheckpointer pipelineCheckpointer;
                pipelineCheckpointer.createStorageDir();

                configFile.copyTo(pipelineCheckpointer.getPathToFile());
                jobId = pipelineCheckpointer.getId();
            }
            catch (...)
            {
                // We just need to catch exceptions.
            }
        }

        // We were not able to copy the file.
        if (jobId <= 0)
        {
            if (waitForResponse)
            {
                std::cerr << "ERROR: Could not copy job configuration file [" << pathToLoad << "]!" << std::endl;
            }
            
            return "Could not copy job configuration file [" + pathToLoad + "]";
        }
    }

    // Load the demanded size of the cluster from the configuration.
    if (!pathToLoad.empty())
    {
        try
        {
            // Read the configuration of cluster.
            std::ifstream file(pathToLoad, std::ios::binary);
            std::string fileContents((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());

            try
            {
                Poco::Dynamic::Var configurationVar = Poco::JSON::Parser().parse(fileContents);
                Poco::JSON::Object::Ptr configurationJson = configurationVar.extract<Poco::JSON::Object::Ptr>();

                clusterSize = getClusterSizeFromConfiguration(configurationJson);
            }
            catch (...)
            {
                // We just need to catch exceptions caused by invalid JSON.
            }
        }
        catch (...)
        {
            // We just need to catch exceptions.
        }
    }

    return "";
}

/**
 * Loads parameters for the job run from the parameters given to the plugin. This method is used when the
 * job is launched from the frontend over HTTP.
 *
 * @param configurationJson Job configuration in JSON.
 * @param configuration Job configuration as string.
 * @param jobId Output parameter for the id of the job.
 * @param clusterSize Output parameter for number of MPI threads used in the job.
 * @param waitForResponse If set to true, all the errors that might occur are shown on the console.
 *
 * @return Empty string when everithing is all right, an error otherwise.
 */
std::string bdg::components::PipelineLauncher::loadParametersFromPipelineConfiguration(
    const Poco::JSON::Object::Ptr configurationJson,
    const std::string &configuration,
    int &jobId,
    size_t &clusterSize,
    const bool waitForResponse)
{
    // Store the request.
    try
    {
        bdg::core::pipeline::PipelineInfoCheckpointer pipelineCheckpointer;
        pipelineCheckpointer.store(configuration);

        // Get parameters from the pipeline info.
        jobId = pipelineCheckpointer.getId();
        clusterSize = getClusterSizeFromConfiguration(configurationJson);
    }
    catch (Poco::Exception &e)
    {
        if (waitForResponse)
        {
            std::cerr << "ERROR: " << e.displayText() << std::endl;
        }
        
        return e.displayText();
    }
    catch (std::exception &e)
    {
        if (waitForResponse)
        {
            std::cerr << "ERROR: " << e.what() << std::endl;
        }

        return e.what();
    }

    return "";
}

/**
 * Loads the number of MPI threads used in the job from the job configuration file.
 * 
 * @param configurationJson Job configuration in JSON.
 *
 * @return Number of MPI threads used in the job.
 */
size_t bdg::components::PipelineLauncher::getClusterSizeFromConfiguration(const Poco::JSON::Object::Ptr configurationJson) const
{
    Poco::Dynamic::Var clusterVar = configurationJson->get("cluster");

    if (clusterVar.type() == typeid(Poco::JSON::Object::Ptr))
    {
        Poco::JSON::Object::Ptr clusterJson = clusterVar.extract<Poco::JSON::Object::Ptr>();
        Poco::Dynamic::Var sizeVar = clusterJson->get("size");

        if (sizeVar.isInteger() || sizeVar.isString())
        {
            int newClusterSize = 0;

            if (sizeVar.isInteger())
            {
                newClusterSize = sizeVar.extract<int>();
            }
            else
            {
                newClusterSize = atoi(sizeVar.extract<std::string>().c_str());
            }

            return std::max<int>(0, std::min<int>(newClusterSize, 100000));
        }
    }

    return 0;
}

/**
 * Returns the job with a job id specified in the passed JSON. If no such job exists,
 * nullptr is returned.
 *
 * @param configurationJson Configuration with the job id.
 *
 * @return Job with a given id or nullptr when no such job exists.
 */
bdg::components::Job *bdg::components::PipelineLauncher::getJob(const Poco::JSON::Object::Ptr configurationJson)
{
    Poco::Mutex::ScopedLock lock(jobsMutex);        // Lock the jobs collection.

    if (configurationJson->has("jobid"))
    {
        int demandedJobId = std::atoi(configurationJson->getValue<std::string>("jobid").c_str());
        JobsCollection::const_iterator it = jobs.find(demandedJobId);
        
        if (it != jobs.end())
        {
            return it->second.get();
        }
        else
        {
            return nullptr;
        }
    }
    else
    {
        return nullptr;
    }
}

using bdg::server::ServerPlugin;
using bdg::components::PipelineLauncher;

/**
 * Export the plugin for the POCO class loader.
 */
POCO_BEGIN_MANIFEST(ServerPlugin)
    POCO_EXPORT_SINGLETON(PipelineLauncher)
POCO_END_MANIFEST
