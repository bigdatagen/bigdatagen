/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Components
 * @author   Big Data Generator Team
 */

#include "components/pipelinelauncher/job.h"                    // Header
#include "core/pipeline/schedulercheckpointer.h"                // SchedulerCheckpointer
#include "core/pipeline/monitoringcheckpointerrestorer.h"       // MonitoringCheckpointerRestorer
#include "utils/commandline.h"                                  // CommandLine

#include <Poco/Process.h>               // Poco::Process
#include <Poco/PipeStream.h>            // Poco::PipeInputStream
#include <Poco/StreamCopier.h>          // Poco::StreamCopier
#include <Poco/StringTokenizer.h>       // Poco::StringTokenizer

#include <thread>       // std::this_thread::sleep_for
#include <chrono>       // std::chrono::seconds

using Poco::Process;
using Poco::ProcessHandle;
using Poco::PipeInputStream;
using Poco::Pipe;

const int bdg::components::Job::MAX_NUM_OF_RELAUNCHES_WITHOUT_CHECKPOINT_CHANGE = 3;
const int bdg::components::Job::MAX_EVENT_WAIT_MS = 60000;
const int bdg::components::Job::PROGRAM_RETURN_VALUE_OK = 0;
const int bdg::components::Job::PROGRAM_RETURN_VALUE_ERROR = 1;
const int bdg::components::Job::PROGRAM_RETURN_VALUE_WRONG_PROGRAM = 72;

/**
 * Constructor of the job.
 *
 * @param jobId Id of the job.
 * @param clusterSize Number of MPI threads used to generate files in the job.
 * @param copyOutput When set to true, the whole output of pipeline program is copied to stdout.
 */
bdg::components::Job::Job(const int jobId, const size_t clusterSize, const bool copyOutput) :
    jobId(jobId),
    clusterSize(clusterSize),
    copyOutput(copyOutput),
    state(STATE_RUNNING),
    runnable(*this, &Job::runPipeline)
{
    thread.start(runnable);
}

/**
 * Returns the job state as a string.
 *
 * @return Job state as a string.
 */
std::string bdg::components::Job::getStateStr() const
{
    switch (state)
    {
        case State::STATE_RUNNING:
            return "running";

        case State::STATE_PAUSED:
            return "paused";

        case State::STATE_FINISHED:
            return "finished";
    }

    return "unknown";
}

/**
 * Returns the job state with all statistical information in JSON.
 *
 * @return Job state with all statistical information.
 */
Poco::JSON::Object bdg::components::Job::getJobState()
{
    Poco::Mutex::ScopedLock lock(resultMutex);      // Lock the job result.

    if (result.size() == 0)
    {
        Poco::JSON::Object stateJson;
        loadState(stateJson, std::vector<std::string>());

        return stateJson;
    }
    else
    {
        return result;
    }
}

/**
 * Re-launches the job.
 *
 * @return JSON with the result of the command.
 */
const std::string bdg::components::Job::play()
{
    Poco::Mutex::ScopedLock lock(stateMutex);       // Lock the job state.

    if (state == State::STATE_PAUSED)
    {
        if (!removeStopFile())
        {
            return R"({"jobid": ")" + std::to_string(jobId) + R"(", "error": "Play command failed"})";
        }
        
        runningStateReachedEvent.reset();           // This event might be already set -> reset it.
        state = State::STATE_RUNNING;

        // Signal that we can continue running the job.
        continueRunningEvent.set();

        // Wait until the "running" state is really reached.
        if (!runningStateReachedEvent.tryWait(MAX_EVENT_WAIT_MS))
        {
            return R"({"jobid": ")" + std::to_string(jobId) + R"(", "error": "Response from the job to get started took too long time"})";
        }
    }

    return R"({"jobid": ")" + std::to_string(jobId) + R"(", "state": ")" + getStateStr() + R"("})";
}

/**
 * Pauses the job.
 *
 * @return JSON with the result of the command.
 */
const std::string bdg::components::Job::pause()
{
    Poco::Mutex::ScopedLock lock(stateMutex);       // Lock the job state.

    if (state == State::STATE_RUNNING)
    {
        state = State::STATE_PAUSED;

        if (!createStopFile())
        {
            state = State::STATE_RUNNING;
            return R"({"jobid": ")" + std::to_string(jobId) + R"(", "error": "Pause command failed"})";
        }

        // Wait until the "paused" state is really reached.
        if (!pausedStateReachedEvent.tryWait(MAX_EVENT_WAIT_MS))
        {
            return R"({"jobid": ")" + std::to_string(jobId) + R"(", "error": "Response from the job to get paused took too long time"})";
        }
    }

    return R"({"jobid": ")" + std::to_string(jobId) + R"(", "state": ")" + getStateStr() + R"("})";
}

/**
 * Stops the job.
 *
 * @return JSON with the result of the command.
 */
const std::string bdg::components::Job::stop()
{
    Poco::Mutex::ScopedLock lock(stateMutex);       // Lock the job state.

    if (state == State::STATE_RUNNING)
    {
        state = State::STATE_FINISHED;

        if (!createStopFile())
        {
            state = State::STATE_RUNNING;
            return R"({"jobid": ")" + std::to_string(jobId) + R"(", "error": "Stop command failed"})";
        }

        // Wait until the "finished" state is really reached.
        if (!finishedStateReachedEvent.tryWait(MAX_EVENT_WAIT_MS))
        {
            return R"({"jobid": ")" + std::to_string(jobId) + R"(", "error": "Response from the job to get finished took too long time"})";
        }
    }
    else  if (state == State::STATE_PAUSED)
    {
        state = State::STATE_FINISHED;
        continueRunningEvent.set();

        // Wait until the "finished" state is really reached.
        if (!finishedStateReachedEvent.tryWait(MAX_EVENT_WAIT_MS))
        {
            return R"({"jobid": ")" + std::to_string(jobId) + R"(", "error": "Response from the job to get finished took too long time"})";
        }
    }

    return R"({"jobid": ")" + std::to_string(jobId) + R"(", "state": ")" + getStateStr() + R"("})";
}

/**
 * Waits until the job thread is terminated.
 */
void bdg::components::Job::wait()
{
    thread.join();
}

/**
 * This method is run in a separate thread. In this method the pipeline program is launched.
 * This method communicates with pause, play and stop method called from different threads
 * with events.
 */
void bdg::components::Job::runPipeline()
{
    std::vector<std::string> errors;
    std::vector<std::string> args;
    std::string lastCheckpoint = "";

    const std::string pipelineProgram = Poco::replace(bdg::utils::Config::getInstance().getPipelineProgram(), "\"", "");
    const std::string origProgramCmd = createCmd(pipelineProgram);
    const std::string programCmd = bdg::utils::CommandLine::parseCommandLineArguments(origProgramCmd + " " + std::to_string(jobId), args);

    int programReturnValue = -1;
    int numOfRelaunchesWithoutCheckpointChange = -1;
    bool pipelineProgramExists = false;

    try
    {
        pipelineProgramExists = Poco::File(pipelineProgram).exists();
    }
    catch (Poco::Exception &e)
    {
        errors.push_back("Error while testing pipeline program: " + e.displayText());
    }

    if (pipelineProgramExists)
    {
        // Continue until we are done (return code 0) or we get error handled in the pipeline
        // program (return code 1). Other return codes are considered as MPI error (e.g. node
        // failure) and we retry to launch the pipeline. Next time it starts from the last
        // checkpoint it has saved. We also want to terminate the cycle when the job gets
        // to the "finished" state, i.e. stop() method has been called.
        while (state != State::STATE_FINISHED
            && programReturnValue != PROGRAM_RETURN_VALUE_OK
            && programReturnValue != PROGRAM_RETURN_VALUE_ERROR
            && programReturnValue != PROGRAM_RETURN_VALUE_WRONG_PROGRAM
            && canRelaunch(lastCheckpoint, numOfRelaunchesWithoutCheckpointChange))
        {
            ++relaunches;

            Pipe outputPipe;
            Pipe errorPipe;
            PipeInputStream outputPipeStream(outputPipe);
            PipeInputStream errorPipeStream(errorPipe);

            try
            {
                ProcessHandle programProcessHndl = Process::launch(programCmd, args, nullptr, &outputPipe, &errorPipe);
                
                if (copyOutput)
                {
                    Poco::StreamCopier::copyStream(outputPipeStream, std::cout, 1);
                    Poco::StreamCopier::copyStream(errorPipeStream, std::cerr, 1);
                }

                runningStateReachedEvent.set();
                programReturnValue = Process::wait(programProcessHndl);
            }
            catch (...)
            {
                programReturnValue = PROGRAM_RETURN_VALUE_WRONG_PROGRAM;
            }
            
            // Handle errors.
            if (!copyOutput)
            {
                try
                {
                    std::string errorStr;
                    Poco::StreamCopier::copyToString(errorPipeStream, errorStr);    // The thread might get stuck on this line when pausing the job, we do not know the reason.
                    Poco::StringTokenizer errorStrings(errorStr, "\n");

                    for (const auto &error : errorStrings)
                    {
                        const std::string errorFormated = Poco::trim(Poco::replace(Poco::replace(Poco::replace(error, "FATAL ERROR: ", ""), "ERROR: ", ""), "!", ""));

                        if (!errorFormated.empty())
                        {
                            errors.push_back(errorFormated);
                        }
                    }
                }
                catch (Poco::Exception &e)
                {
                    errors.push_back(e.displayText());
                }
                catch (std::exception &e)
                {
                    errors.push_back(e.what());
                }
            }

            // Handle paused state.
            if (state == State::STATE_PAUSED)
            {
                // Signal that the "paused" state has been reached.
                pausedStateReachedEvent.set();

                // Wait on condv until play() or stop() methods are called.
                continueRunningEvent.wait();

                // Did we call play() method?
                if (state == State::STATE_RUNNING)
                {
                    // We want to re-launch the job, i.e. continue in the cycle.
                    programReturnValue = 2;
                    numOfRelaunchesWithoutCheckpointChange = 0;
                }
            }
        }

        if (programReturnValue != PROGRAM_RETURN_VALUE_OK && programReturnValue != PROGRAM_RETURN_VALUE_ERROR)
        {
            std::string error;

            if (programReturnValue == PROGRAM_RETURN_VALUE_WRONG_PROGRAM)
            {
                error = "Could not run program [" + origProgramCmd + "]";
            }
            else
            {
                error = "Pipeline program permanently failed, maximum number of relaunches was reached";
            }

            if (copyOutput)
            {
                std::cerr << error << "!" << std::endl;
            }
            else
            {
                errors.push_back(error);
            }
        }
    }
    // Invalid pipeline program configuration.
    else
    {
        if (copyOutput)
        {
            std::cerr << "Pipeline program [" << pipelineProgram << "] not found!" << std::endl;
        }
        else
        {
            errors.push_back("Pipeline program [" + pipelineProgram + "] not found");
        }
    }

    finishedStateReachedEvent.set();                // Signal that the "finished" state has been reached.
    removeStopFile();                               // Remove the stop file - clean up.

    state = State::STATE_FINISHED;

    Poco::Mutex::ScopedLock lock(resultMutex);      // Lock the job result.

    // Get the pipeline result.
    loadState(result, errors);

    result.set("command", origProgramCmd);
    result.set("returnValue", programReturnValue);
}

/**
 * Loads the state of the job from the monitoring checkpoint. This checkpoint is periodically
 * stored by the launched pipeline application.
 *
 * @param json Output parameter for the job status in the JSON.
 * @param errors Collection of errors that occured during the job run.
 */
void bdg::components::Job::loadState(Poco::JSON::Object &json, const std::vector<std::string> &errors)
{
    Poco::JSON::Array errorsJson;
    bdg::core::pipeline::MonitoringCheckpointerRestorer monitoringCheckpointer(jobId);
    monitoringCheckpointer.restore(json);

    if (json.size() == 0)
    {
        errorsJson.add("Could not read job status");
    }

    for (const auto &error : errors)
    {
        errorsJson.add(error);
    }

    // Quick fix of bug when the pipeline program gets stuck when it is finished, but it never returns.
    if (json.has("progress") && json.getValue<double>("progress") == 100)
    {
        state = State::STATE_FINISHED;
    }

    json.set("errors", errorsJson);
    json.set("jobid", jobId);
    json.set("state", getStateStr());
    json.set("relaunches", std::max<int>(0, relaunches));
}

/**
 * Returns the command to launch the pipeline application over MPI.
 *
 * @param pipelineProgram Path to the pipeline application.
 *
 * @return Command to launch the pipeline application.
 */
std::string bdg::components::Job::createCmd(const std::string &pipelineProgram) const
{
    return bdg::utils::Config::getInstance().getMpiProgram() + " "
        + (clusterSize > 1 ? "-n " + std::to_string(clusterSize) + " " : "")
        + "\""+ pipelineProgram + "\"";
}

/**
 * Returns whether the job can be re-launched.
 *
 * @param lastCheckpoint Content of the last job checkpoint.
 * @param numOfRelaunchesWithoutCheckpointChange How many times was the job re-launched without any progress?
 *
 * @return Can the job be re-launched?
 */
bool bdg::components::Job::canRelaunch(std::string &lastCheckpoint, int &numOfRelaunchesWithoutCheckpointChange) const
{
    std::string checkpoint = "";

    try
    {
        std::ifstream file(bdg::core::pipeline::SchedulerCheckpointer::getPathToFile(bdg::core::pipeline::SchedulerCheckpointer::getClassId(jobId), 1), std::ios::binary);
        checkpoint = std::string((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
    }
    catch (...)
    {
        // We just need to catch exceptions.
    }

    if (checkpoint != lastCheckpoint)
    {
        numOfRelaunchesWithoutCheckpointChange = 0;
        lastCheckpoint = checkpoint;
        
        return true;
    }
    else
    {
        ++numOfRelaunchesWithoutCheckpointChange;

        return numOfRelaunchesWithoutCheckpointChange < MAX_NUM_OF_RELAUNCHES_WITHOUT_CHECKPOINT_CHANGE;
    }
}

/**
 * Creates the file which stops the pipeline application. This way a pause and stop
 * commands are implemented - stop file is created, thus pipeline application is terminated
 * from inside.
 *
 * @return Was the file successfully created?
 */
bool bdg::components::Job::createStopFile() const
{
    Poco::File stopFile(bdg::core::pipeline::MonitoringCheckpointerRestorer::getPathToStopFile(jobId));

    if (!stopFile.exists())
    {
        try
        {
            // When this file exists, it is a signal for pipeline program to softly terminate.
            // Checkpoint is stored, so when the "play" command is processed, it continues
            // from the point is was paused at.
            stopFile.createFile();
        }
        catch (...)
        {
            return false;
        }
    }

    return true;
}

/**
 * Deleted the file which stops the pipeline application.T his way a pause and play
 * commands are implemented - stop file is removed and then the pipeline application is
 * re-launched and it loads its state from the last checkpoint.
 *
 * @return Was the file successfully removed?
 */
bool bdg::components::Job::removeStopFile() const
{
    Poco::File stopFile(bdg::core::pipeline::MonitoringCheckpointerRestorer::getPathToStopFile(jobId));

    if (stopFile.exists())
    {
        try
        {
            stopFile.remove();
        }
        catch (...)
        {
            return false;
        }
    }

    return true;
}
