#include "components/grammar/grammar.h"


namespace bdg {
    namespace components {
        namespace grammar {

using namespace symbolType;


/*
 *  
 */   
void
Grammar::loadGrammar(FILE* in)
{
    scanner = new Coco::Scanner(in);
    parser = new Coco::Parser(scanner);

    this->loadGrammar();
}

/*
 *
 */
void
Grammar::loadGrammar()
{
    /* The scanner and parser should be set by this point */

    /* Read the rules defined in the grammar */
    parser->Parse(this->rules);

    // TODO: error handling (from Parser)

    if (rules.empty())
    {
        // log_warn no rules in grammar
        return;
    }

    consistent = false;

    /* The first rule should contain starting symbol */
    start = rules.begin()->getLhs();
    
    /* Initialize terms and nterms */
    for (std::vector<Rule>::iterator it = rules.begin(); it != rules.end(); ++it)
    {
        this->updateSets(*it);
    }

    /* Generated parser doesn't make consistency checks */
    this->pruneGrammar();
}

/*
 */
void
Grammar::saveGrammar(std::ofstream& out)
{
    assert(out.is_open());
    this->pruneGrammar();

    for(std::vector<Rule>::iterator it = rules.begin(); it != rules.end(); ++it) 
    {
        out << it->toString() << std::endl;
    }
}

/*
 * Save grammar in the JSON format
 */
void
Grammar::toJSON(std::ofstream& out)
{
    this->pruneGrammar();
    
    out << "{ ";

    out << "\"startSymbol\": ";
    out << start->toJSON();

    out << ", \"rules\": [ ";
    for(std::vector<Rule>::const_iterator it = rules.begin(); it != rules.end(); ++it)
    {
        out << it->toJSON();
        out << ", ";
    }

    //TODO: lists of terminals, nonterminals?

    out << "}";
}

/*
 * Adds rule to the grammar (can break consistency)
 */
void
Grammar::addRule(const Rule& r)
{
    Symbol* lhs = r.getLhs();
    Symbol* rhs = r.getRhs();

    /* We assume, that the first rule added to an empty grammar defines the starting symbol */
    if (rules.empty())
    {
        start = lhs;
    }

    /* We go through the rules and see if there isn't a production already defined */
    bool newRule = true;
    for(std::vector<Rule>::iterator it = rules.begin(); it != rules.end(); ++it)
    {
        if(*(it->getLhs()) == *lhs)
        {
            /* If the production is different, we try to "merge" the rules */
            if(*(it->getRhs()) != *rhs)
            {
                it->merge(r);
            }
            newRule = false;
            break;
        }
    }

    if(newRule)
    {
        rules.push_back(r);
    }

    /* We add all new T and NT symbols involved in the Rule to the lists */
    updateSets(r);

    consistent = false;
}

/*
 * Remove a rule from a grammar (This may put grammar into inconsistent state)
 */
void
Grammar::removeRule(const Rule& r)
{
    rules.erase(std::remove(rules.begin(), rules.end(), r), rules.end());
    consistent = false;
}

void
Grammar::setStart(const Symbol* s)
{
    if(!isNT(s->getType()))
    {
        //log_err(Invalid start symbol type)
    }
    else
    {
        start = new SimpleSymbol(s->getValue(),s->getType());
    }
}

/*
 * After a rule is added, we want to update grammar's Ts and NTs
 */
void
Grammar::updateSets(const Rule& r)
{
    Symbol* s = r.getLhs();
    if (nterms.find(s->getValue()) == nterms.end())
    {
        nterms.insert(s->getValue());
    }

    s = r.getRhs();
    if (isSimple(s->getType()))
    {
        if (nterms.find(s->getValue()) == nterms.end())
        {
            nterms.insert(s->getValue());
        }
        else if (terms.find(s->getValue()) == terms.end())
        {
            terms.insert(s->getValue());
        }
    }
    else
    {
        /* Probably not the fastest solution */
        std::unordered_set<std::string> set1 = nterms;
        std::unordered_set<std::string> set2;
        (dynamic_cast<ComplexSymbol*>(s))->getSimpleSymbols(set2, NT);
        //std::set_union(set1.begin(), set1.end(), set2.begin(), set2.end(), std::inserter(nterms, nterms.begin()));

        set1 = terms;
        (dynamic_cast<ComplexSymbol*>(s))->getSimpleSymbols(set2, T);
        //std::set_union(set1.begin(), set1.end(), set2.begin(), set2.end(), std::inserter(terms, terms.begin()));
    }
}

/*
 * Throw away unreacheable NT/T and NT without productions
 * 
 * Use case: After series of manual add/remove operation, return possibly
 * incosistent grammar into consistent state
 */
void
Grammar::pruneGrammar()
{
    if(consistent) return;

    /* We check, which TERMs and NONTs are unreachable */
    /* Insert pruning code here */

    consistent = true;
}

Symbol*
Grammar::getStart() const
{
    return start;
}

/*
 * Returns a rule with "lhs" as a left-hand side
 */
Rule*
Grammar::getRule(const Symbol* lhs) 
{
    this->pruneGrammar();
    for(std::vector<Rule>::iterator it = rules.begin(); it != rules.end(); ++it)
    {
        if (*(it->getLhs()) == *lhs) return &(*it);
    }
    return nullptr;
}

/*
 * Returns vector of grammar rules
 */
void
Grammar::getRules(std::vector<Rule>& rls)
{
    this->pruneGrammar();
    rls = rules;
}

Grammar::~Grammar()
{
    delete scanner;
    delete parser;
}

        } // namespace grammar
    } // namespace components
} // namespace bdg
