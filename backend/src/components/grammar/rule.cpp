#include "components/grammar/rule.h"


namespace bdg {
    namespace components {
        namespace grammar {

using namespace symbolType;


/* Symbol class */

Symbol::Symbol(TSymbolType t): type(t)
{
}

TSymbolType
Symbol::getType() const
{
    return type;
}

std::string
Symbol::getValue() const
{
    //logger_warn("<Derived_class_name> doesn't define this method")
    return "";
}

void
Symbol::addChild(Symbol* s)
{
    //logger_warn("<Derived_class_name> doesn't define this method")
}

void
Symbol::getChildren(std::vector<Symbol*>& v) const
{
    //logger_warn("<Derived_class_name> doesn't define this method")
}

void
Symbol::getSimpleSymbols(std::unordered_set<std::string>& s, TSymbolType t) const
{
    //logger_warn("<Derived_class_name> doesn't define this method")
}
        
bool
Symbol::operator==(const Symbol& s) const
{
    if(this->getType() != s.getType())
    {
        return false;
    }

    /* This part looks little ugly (can it be done better?) */
    Symbol* sPtr = const_cast<Symbol*>(&s);
    if (isSimple(sPtr->getType()))
    {
        return this->equals(dynamic_cast<SimpleSymbol*>(sPtr));
    }
    else
    {
        return this->equals(dynamic_cast<ComplexSymbol*>(sPtr));
    }
}

bool
Symbol::operator!=(const Symbol& s) const
{
    return !(*this == s);
}

bool
Symbol::equals(const Symbol*) const
{
    //log_debug("we shouldn't get here at all")
    return false;
}

/* SimpleSymbol class*/

SimpleSymbol::SimpleSymbol(std::string s, TSymbolType t): Symbol(t), value(s)
{
    assert(isSimple(t));
    /* non-empty symbol must have non-empty value field */
    if (s == "")
    {
        this->type = EPS;
    }
}

std::string
SimpleSymbol::getValue() const
{
    return value;
}

std::string
SimpleSymbol::toString() const
{
    std::string res("");
    switch(type)
    {
        case T:
            res.append("\"");
            res.append(value);
            res.append("\"");
            break;
        case NT:
            res = value;
            break;
        default:
            break;
    }

    return res;
}

/*
 * Prints the Symbol in the JSON format
 * TODO: Indentation?
 */
std::string
SimpleSymbol::toJSON() const
{
    std::string res("");
    res.append("{ ");

    res.append("\"type\": \"");
    res.append(symbolType::toString(type));
    res.append("\", \"name\": \"");
    res.append(value);
    res.append("\" ");

    res.append("}");
    return res;
}

bool
SimpleSymbol::equals(const Symbol* s) const
{
    return (this->getType() == s->getType() && this->getValue() == s->getValue());
}

/* ComplexSymbol class */

ComplexSymbol::ComplexSymbol(TSymbolType t): Symbol(t)
{
    childPtrs.clear();
}

void
ComplexSymbol::addChild(Symbol* s)
{
    childPtrs.push_back(s);
}

void
ComplexSymbol::getChildren(std::vector<Symbol*>& v) const
{
    v = childPtrs;
}

/*
 * Returns an unordered_set of names of symbols of a specified type "t"
 */
void
ComplexSymbol::getSimpleSymbols(std::unordered_set<std::string>& s, TSymbolType t) const
{
    s.clear();
    for(std::vector<Symbol*>::const_iterator it = childPtrs.begin(); it != childPtrs.end(); ++it)
    {
        if (isSimple((*it)->getType()))
        {
            if ((*it)->getType() != t) { continue; }
            s.insert((*it)->getValue());
        }
        else
        {
            std::unordered_set<std::string> s1 = s;
            std::unordered_set<std::string> s2;
            (dynamic_cast<ComplexSymbol*>(*it))->getSimpleSymbols(s2, t);

            /* This might not be the fastest solution */
            //std::set_union(s1.begin(), s1.end(), s2.begin(), s2.end(), std::inserter(s, s.begin()));
        }
    }
}

std::string
ComplexSymbol::toString() const
{
    std::string res("");

    // TODO: print grouping of alterations
    switch(type)
    {
        case ALT:
            for(std::vector<Symbol*>::const_iterator it = childPtrs.begin(); it != childPtrs.end(); ++it)
            {
                if(it != childPtrs.begin()) { res.append(" | "); }
                res.append((*it)->toString());    
            }
            break;

        case ITER:
            if (childPtrs.size() == 0) { break; }
            res.append(" { ");
            for(std::vector<Symbol*>::const_iterator it = childPtrs.begin(); it != childPtrs.end(); ++it)
            {
                if(it != childPtrs.begin()) { res.append(" "); }
                res.append((*it)->toString());
            }
            res.append(" } ");

            break;

        case OPT:
            if (childPtrs.size() == 0) { break; }
            res.append(" [ ");
            for(std::vector<Symbol*>::const_iterator it = childPtrs.begin(); it != childPtrs.end(); ++it)
            {
                if(it != childPtrs.begin()) { res.append(" "); }
                res.append((*it)->toString());
            }
            res.append(" ] ");
            break;
        default:
            break;
    }
    return res;
}

/*
 * Recursively prints the Symbol in the JSON format
 * TODO: Indentation?
 */
std::string
ComplexSymbol::toJSON() const
{
    std::string res("");
    res.append("{ ");

    res.append("\"type\": \"");
    res.append(symbolType::toString(type));
    res.append("\", \"children\": [ ");
    for(std::vector<Symbol*>::const_iterator it = childPtrs.begin(); it != childPtrs.end(); ++it)
    {
        res.append((*it)->toJSON());
        res.append(", ");
    }
    res.append(" ] ");

    res.append("}");
    return res;
}

bool
ComplexSymbol::equals(const Symbol* s) const
{
    if (this->type != s->getType()) { return false; }

    std::vector<Symbol*> children;
    s->getChildren(children);
    if (this->childPtrs.size() != children.size()) { return false; }

    std::vector<Symbol*>::const_iterator s_it = children.begin();
    for(std::vector<Symbol*>::const_iterator this_it = childPtrs.begin(); this_it != childPtrs.end(); ++this_it, ++s_it)
    {
        if ( (*(*this_it)) != (*(*s_it)) ) { return false; }
    }
    return true;
}

ComplexSymbol::~ComplexSymbol()
{
    for(std::vector<Symbol*>::const_iterator it = childPtrs.begin(); it != childPtrs.end(); ++it)
    {
        delete *it;
    }
}

/* Symbol Factory */

Symbol*
SymbolCreator::createSymbol(TSymbolType t)
{
    assert(isValidType(t));
    if (isSimple(t))
    {
        return new SimpleSymbol("", t);
    }
    if (isComplex(t))
    {
        return new ComplexSymbol(t);
    }
    return nullptr; // ... or we can return UNDEF Symbol
}

/*
 * This should only be used to create T and NT symbols
 */
Symbol*
SymbolCreator::createSymbol(std::string s, TSymbolType t)
{
    assert(isSimple(t));
    return new SimpleSymbol(s, t);
}

/* Rule class */

Rule::Rule(Symbol* lhs, Symbol* rhs): lhsPtr(lhs), rhsPtr(rhs)
{
    assert(isNT(lhsPtr->getType()));
}

Rule::Rule(const Rule &r): lhsPtr(r.getLhs()), rhsPtr(r.getRhs())
{
}

Symbol*
Rule::getLhs() const
{
    return lhsPtr;
}

Symbol*
Rule::getRhs() const
{
    return rhsPtr;
}

/*
 * Returns an unordered_set of names of symbols of a specified type "t"
 */
void
Rule::getSimpleSymbols(std::unordered_set<std::string>& s, TSymbolType t) const
{
    s.clear();
    if(isSimple(rhsPtr->getType()) && rhsPtr->getType() != t)
    {
        s.insert(rhsPtr->getValue());
    }
    else
    {
        (dynamic_cast<ComplexSymbol*>(rhsPtr))->getSimpleSymbols(s, t);
    }
    s.insert(lhsPtr->getValue());
}

void
Rule::merge(const Rule& r)
{
    assert( *lhsPtr == *r.getLhs() );
    Symbol* rhsPtr2 = r.getRhs();

    /* Case 1: both rules have no alteration on right side */
    if(rhsPtr->getType() != ALT && rhsPtr2->getType() != ALT)
    {
        ComplexSymbol alt(ALT);
        alt.addChild(rhsPtr);
        alt.addChild(rhsPtr2);
        rhsPtr = &alt;
    }
    /* Case 2: at least on rule have alteration on the right side */
    else
    {
        /* we are modifiing "this" */
        if(rhsPtr->getType() != ALT)
        {
            std::swap(rhsPtr, rhsPtr2);
        }

        std::vector<Symbol*> children;
        rhsPtr->getChildren(children);

        /* add children to this->rhsPtr */
        if(rhsPtr2->getType() != ALT)
        {
            /* check for duplicates */
            bool notfound = true;
            for(std::vector<Symbol*>::iterator it = children.begin(); it != children.end(); ++it)
            {
                if(*rhsPtr2 == *(*it)) notfound = false;
            }
            if(notfound)
            {
                rhsPtr->addChild(rhsPtr2);
            }
        }
        else
        {
            std::vector<Symbol*> children2;
            rhsPtr2->getChildren(children2);
            for(std::vector<Symbol*>::iterator it = children2.begin(); it != children2.end(); ++it)
            {
                /* check for duplicates */
                bool notfound = true;
                for(std::vector<Symbol*>::iterator it2 = children.begin(); it2 != children.end(); ++it2)
                {
                    if(*(*it) == *(*it2)) notfound = false;
                }
                if(notfound)
                {
                    rhsPtr->addChild(*it);
                }
                
            }
        }
    }
}

Rule&
Rule::operator=(const Rule& r)
{
    if (this == &r) { return *this; }
    lhsPtr = r.getLhs();
    rhsPtr = r.getRhs();
    return *this;
}

bool
Rule::operator==(const Rule& r) const
{
    return ( *lhsPtr == *r.getLhs() && *rhsPtr == *r.getRhs() );
}

bool
Rule::operator!=(const Rule& r) const
{
    return !(*this == r);
}

void
Rule::print() const
{
    std::cout << this->toString() << std::endl;
}

std::string
Rule::toString() const
{
    std::string res("");
    res.append(lhsPtr->toString());
    res.append(" = ");
    res.append(rhsPtr->toString());
    return res;
}

std::string
Rule::toJSON() const
{
    std::string res("");
    res.append("{ ");

    res.append("\"leftHandSide\": ");
    res.append(lhsPtr->toJSON());
    res.append(", \"rightHandSide\": ");
    res.append(rhsPtr->toJSON());
    res.append(" ");

    res.append("}");
    return res;
}

Rule::~Rule()
{
    delete lhsPtr;
    delete rhsPtr;
}

        } // namespace grammar
    } // namespace components
} // namespace bdg
