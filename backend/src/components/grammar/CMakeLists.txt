project(Grammar CXX)

include_directories(
    ${HDR_DIR}
)

add_library(${PROJECT_NAME} SHARED
    "${SRC_DIR}/components/grammar/grammar.cpp"
    "${SRC_DIR}/components/grammar/rule.cpp"
    "${SRC_DIR}/components/grammar/parser.cpp"
    "${SRC_DIR}/components/grammar/scanner.cpp"
)

target_compile_features(${PROJECT_NAME} PRIVATE cxx_range_for)

install(TARGETS ${PROJECT_NAME} DESTINATION lib EXPORT ${PROJECT_NAME})
install(EXPORT ${PROJECT_NAME} DESTINATION lib/alib)
