/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Server
 * @author   Big Data Generator Team
 */

#include "components/schemainferer/jsoninferer/jsoninferer.h"       // Header

#include <Poco/JSON/Parser.h>           // Poco::JSON::Parser
#include <Poco/JSON/JSONException.h>    // Poco::JSON::JSONException
#include <Poco/Exception.h>             // Poco::FileNotFoundException
#include <Poco/ClassLibrary.h>          // POCO Export class

#include <vector>       // std::vector
#include <typeinfo>     // std::typeid
#include <fstream>      // std::ifstream

/**
 * Default constructor filling up the info about the inferrer.
 */
bdg::components::schemaInferer::JsonInferer::JsonInferer()
{
    name = "JsonInferer";
    description = "Inferer plugin for infering JSON schemas";
}

/**
 * Method that is accessible by the SchemaInfererPlugin that begins the execution of the inferrer.
 *
 * @param os Variable containing the inferrer output.
 * @param params Plugin parameters (ignored by the JsonInferer).
 */
void bdg::components::schemaInferer::JsonInferer::inferSchema(std::ostream &os, const Poco::JSON::Object::Ptr &params)
{
    if (filenames.empty())
    {
        throw Poco::InvalidArgumentException("(JSON Inferer) No files to process.");
    }
    else
    {
        Poco::JSON::Object::Ptr schemaRoot = new Poco::JSON::Object;
        std::string skippedFiles("");
        
        // We use the goto label to handle the invalid files.
        // This can be probably done more nicely.
        PROCESS_FILES: for(const std::string &file : filenames)
        {
            std::ifstream ifs(file);

            if (!ifs.is_open())
            {
                throw Poco::OpenFileException("(JSON Inferer) Cannot open file " + file + ".");
            }

            Poco::JSON::Object::Ptr newRoot;

            try
            {
                newRoot = jsonToSchema(ifs);

                if(schemaRoot->size() == 0)
                {
                    schemaRoot = newRoot;
                }
                else
                {
                    mergeRoot(schemaRoot, newRoot);
                }
            }
            // Report invalid syntax JSONExceptions as SyntaxExceptions due to unified processing of
            // these in the SchemaInferer plugin.
            catch (Poco::JSON::JSONException &e)
            {
                skippedFiles.append("[" + file + ": " + e.message() + "]");
                removeFilename(file);
                goto PROCESS_FILES;
            }
            catch (Poco::SyntaxException &e)
            {
                skippedFiles.append("[" + file + ": " + e.message() + "]");
                removeFilename(file);
                goto PROCESS_FILES;
            }
            catch (Poco::Exception &e)
            {
                e.rethrow();
            }
        }
        
        // Default root schema element is ARRAY, which contains ROOT object (and possibly UDT objects).
        Poco::JSON::Array::Ptr schemaRootArray = new Poco::JSON::Array;
        schemaRootArray->add(schemaRoot);
        schemaRootArray->stringify(os);

        if (skippedFiles != "")
        {
            throw Poco::RuntimeException("Skipped files: " + skippedFiles);
        }
    }
}

/**
 * Parses the input JSON file and generates proper schema representation. Schema is also
 * a JSON object, so we use Poco structures for representation.
 *
 * @param ifs Input stream containing the file to be inferred.
 *
 * @return JSON object containing the inferred schema.
 */
Poco::JSON::Object::Ptr bdg::components::schemaInferer::JsonInferer::jsonToSchema(std::istream &ifs)
{
    Poco::Dynamic::Var parsedJSON;

    try
    {
        parsedJSON = Poco::JSON::Parser().parse(ifs);
    }
    catch (Poco::Exception &e)
    {
        e.rethrow();
    }

    Poco::JSON::Object::Ptr schemaRoot = new Poco::JSON::Object;

    if (parsedJSON.type() == typeid(Poco::JSON::Object::Ptr))
    {
        Poco::JSON::Object::Ptr jsonRoot = parsedJSON.extract<Poco::JSON::Object::Ptr>();
        schemaRoot = getObjectSchema(jsonRoot, "ROOT");
    }
    else if (parsedJSON.type() == typeid(Poco::JSON::Array::Ptr))
    {
        Poco::JSON::Array::Ptr jsonRoot = parsedJSON.extract<Poco::JSON::Array::Ptr>();
        schemaRoot = getArraySchema(jsonRoot, "ROOT");
    }
    else
    {
        throw Poco::SyntaxException("(JSON Inferer) Invalid JSON root element.");
    }

    return schemaRoot;
}

/**
 * Method that creates that is used by the getObjectSchema method to process
 * the attribute names of a JSON object and create a schema for each of them.
 *
 * @param object Object whose properties schema is to be created.
 *
 * @return Array of the object properties transformed into their schemas.
 */
Poco::JSON::Array::Ptr bdg::components::schemaInferer::JsonInferer::getObjectProperties(const Poco::JSON::Object::Ptr &object)
{
    Poco::JSON::Array::Ptr properties = new Poco::JSON::Array;
    std::vector<std::string> attrNames;
    object->getNames(attrNames);

    for (std::string &attrName : attrNames)
    {
        if (object->isObject(attrName))
        {
            Poco::JSON::Object::Ptr attrVal = object->getObject(attrName);
            properties->add(getObjectSchema(attrVal, attrName));
        }
        else if(object->isArray(attrName))
        {
            Poco::JSON::Array::Ptr attrVal = object->getArray(attrName);
            properties->add(getArraySchema(attrVal, attrName));
        }
        else
        {
            Poco::Dynamic::Var attrVal = object->get(attrName);
            properties->add(getSimpleTypeSchema(attrVal, attrName));
        }
    }

    return properties;
}

/**
 * Method that infers all the required properties of the JSON array variable.
 * It also processes the members of the array and creates an innerType schema
 * for each of them.
 *
 * @param array The JSON array variable that is to be processed.
 *
 * @return JSON object containing the schema describing the variable.
 */
Poco::JSON::Object::Ptr bdg::components::schemaInferer::JsonInferer::getArrayProperties(const Poco::JSON::Array::Ptr &array)
{
    Poco::JSON::Object::Ptr result = new Poco::JSON::Object;
    result->set("source", "domain");
    result->set("distribution", "uniform");

    Poco::JSON::Array::Ptr domain = new Poco::JSON::Array;
    domain->add(array->size());
    result->set("domain", domain);
    result->set("min", array->size());
    result->set("max", array->size());
    
    Poco::JSON::Array::Ptr innerType = new Poco::JSON::Array;

    for (int i = 0; i < array->size(); ++i)
    {
        // Extract new type.
        Poco::Dynamic::Var element = array->get(i);
        Poco::JSON::Object::Ptr newType;

        if (element.type() == typeid(Poco::JSON::Object::Ptr))
        {
            newType = getObjectSchema(element.extract<Poco::JSON::Object::Ptr>(), "");
        }
        else if (element.type() == typeid(Poco::JSON::Array::Ptr))
        {
            newType = getArraySchema(element.extract<Poco::JSON::Array::Ptr>(), "");
        }
        else
        {
            newType = getSimpleTypeSchema(element, "");
        }

        newType->remove("propertyName");

        // Check, if the type cannot be merged with already processed types.
        bool merged = false;

        for (int j = 0; j < innerType->size(); ++j)
        {
            Poco::JSON::Object::Ptr type = innerType->get(j).extract<Poco::JSON::Object::Ptr>();
            
            if (canMerge(type, newType))
            {
                merge(type, newType);
                merged = true;
                break;
            }
        }

        if (!merged)
        {
            innerType->add(newType);
        }
    }

    result->set("innerType", innerType);

    return result;
}    

/**
 * Generates JSON schema object for the basic JSON variables (inf, float, string).
 *
 * @param val JSON variable to be inferred.
 * @param name Name of the variable.
 *
 * @return JSON object containing the schema of the variable.
 */
Poco::JSON::Object::Ptr bdg::components::schemaInferer::JsonInferer::getSimpleTypeSchema(const Poco::Dynamic::Var &val, const std::string &name)
{
    Poco::JSON::Object::Ptr result = new Poco::JSON::Object;
    result->set("propertyName", name);

    Poco::JSON::Array::Ptr domain = new Poco::JSON::Array;
    Poco::JSON::Object::Ptr properties = new Poco::JSON::Object;    
    properties->set("source", "domain");
    properties->set("distribution", "uniform");

    // Do we need to convert? (since the Poco::JSON::Array holds Dynamic::Var elements anyway).
    if (val.isInteger())
    {
        result->set("type", "int");
        domain->add(val.convert<int>());
        properties->set("domain", domain);
        properties->set("min", val.convert<int>());
        properties->set("max", val.convert<int>());
    }
    else if (val.isNumeric())
    {
        result->set("type", "float");
        domain->add(val.convert<double>());
        properties->set("domain", domain);
        properties->set("min", val.convert<double>());
        properties->set("max", val.convert<double>());
    }
    else if (val.isString())
    {
        result->set("type", "string");
        domain->add(val.convert<std::string>());
        properties->set("domain", domain);
        properties->set("min", val.convert<std::string>().length());
        properties->set("max", val.convert<std::string>().length());
    }
    else
    {
        throw Poco::RuntimeException("(JsonInferer::getSimpleTypeSchema) We shouldn't have got here.");
    }

    result->set("properties", properties);

    return result;
}

/**
 * Generates JSON schema object for the JSON object variable
 * by filling in all the values describing the JSON object.
 *
 * @param obj JSON object variable to be inferred.
 * @param name Name of the variable.
 *
 * @return JSON object containing the schema of the object variable.
 */
Poco::JSON::Object::Ptr bdg::components::schemaInferer::JsonInferer::getObjectSchema(const Poco::JSON::Object::Ptr &obj, const std::string &name)
{
    Poco::JSON::Object::Ptr result = new Poco::JSON::Object;

    if (name == "ROOT")
    {
        result->set("typeName", name);
    }
    else
    {
        result->set("propertyName", name);
    }

    result->set("type", "object");

    Poco::JSON::Array::Ptr properties = getObjectProperties(obj);
    result->set("properties", properties);

    Poco::JSON::Array::Ptr required = new Poco::JSON::Array;

    for (int i = 0; i < properties->size(); ++i)
    {
        Poco::JSON::Object::Ptr prop = properties->get(i).extract<Poco::JSON::Object::Ptr>();
        required->add(prop->get("propertyName"));
    }

    if (name != "ROOT")
    {
        result->set("required", required);
    }

    return result;
}

/**
 * Generate JSON schema object for the JSON array variable
 * by filling in all the values describing the JSON array.
 *
 * @param arr JSON array that is to be inferred.
 * @param name Name of the variable.
 *
 * @return JSON object containing the schema of the array variable.
 */
Poco::JSON::Object::Ptr bdg::components::schemaInferer::JsonInferer::getArraySchema(const Poco::JSON::Array::Ptr &arr, const std::string &name)
{
    Poco::JSON::Object::Ptr result = new Poco::JSON::Object;

    if (name == "ROOT")
    {
        result->set("typeName", name);
    }
    else
    {
        result->set("propertyName", name);
    }

    result->set("type", "array");

    Poco::JSON::Object::Ptr properties = getArrayProperties(arr);
    result->set("properties", properties);

    return result;
}

/**
 * Entry method for the schema merge. Merges two schema roots root1, root2, result is stored
 * in root1.
 *
 * @param root1 JSON schema. Contains the result of the merge.
 * @param root2 The other schema.
 */
void bdg::components::schemaInferer::JsonInferer::mergeRoot(Poco::JSON::Object::Ptr &root1, Poco::JSON::Object::Ptr &root2)
{
    if (!canMerge(root1, root2))
    {
        throw Poco::JSON::JSONException("(JSON Inferer) Incompatible schema root: inferedRoot=" + root1->get("type").toString() + " newRoot=" + root2->get("type").toString());
    }

    merge(root1, root2);
}

/**
 * General merging method. Merges two schema objects obj1, obj2, result is stored in obj1.
 *
 * @param obj1 JSON schema object. Contains result of the merge.
 * @param obj2 The other schema object.
 */
void bdg::components::schemaInferer::JsonInferer::merge(Poco::JSON::Object::Ptr &obj1, Poco::JSON::Object::Ptr &obj2)
{
    if (obj1->get("type") == "object")
    {
        mergeObjects(obj1, obj2);
    }
    else if (obj1->get("type") == "array")
    {
        mergeArrays(obj1, obj2);
    }
    else
    {
        mergeSimpleTypes(obj1, obj2);
    }
}

/**
 * Method for merging the basic JSON variable types. It combines the domains
 * (we don't care about value duplicates) and updates the min/max values.
 *
 * @param obj1 JSON schema object. Contains the result of the merge
 * @param obj2 The other schema object.
 */
void bdg::components::schemaInferer::JsonInferer::mergeSimpleTypes(Poco::JSON::Object::Ptr &obj1, Poco::JSON::Object::Ptr &obj2)
{
    Poco::JSON::Array::Ptr tmpDomain = obj2->getObject("properties")->getArray("domain");

    for (int i = 0; i < tmpDomain->size(); ++i)
    {
        obj1->getObject("properties")->getArray("domain")->add(tmpDomain->get(i));
    }
    
    if (obj1->getObject("properties")->get("min").isInteger() && obj2->getObject("properties")->get("min").isInteger())
    {
        updateMinMax<int>(obj1, obj2);
    }
    else if (obj1->getObject("properties")->get("min").isNumeric() && obj2->getObject("properties")->get("min").isNumeric())
    {
        updateMinMax<double>(obj1, obj2);
    }
    else
    {
        updateMinMax<std::string>(obj1, obj2);
    }
}

/**
 * Method for merging the schemas of the JSON object variables. 
 * For each attribute of obj2, check if we can merge it with any of obj1 attributes.
 * Attributes with same name but a different type are allowed. They are not merged
 * since the resulting schema is still a valid JSON file.
 *
 * @param obj1 JSON schema object. Contains the result of the merge.
 * @param obj2 The other schema object.
 */
void bdg::components::schemaInferer::JsonInferer::mergeObjects(Poco::JSON::Object::Ptr &obj1, Poco::JSON::Object::Ptr &obj2)
{
    Poco::JSON::Array::Ptr properties1 = obj1->getArray("properties");
    Poco::JSON::Array::Ptr properties2 = obj2->getArray("properties");

    std::unordered_set<std::string> required;

    for (int j = 0; j < properties2->size(); ++j)
    {
        bool merged = false;
        Poco::JSON::Object::Ptr prop2 = properties2->get(j).extract<Poco::JSON::Object::Ptr>();

        for (int i = 0; i < properties1->size(); ++i)
        {
            Poco::JSON::Object::Ptr prop1 = properties1->get(i).extract<Poco::JSON::Object::Ptr>();
            
            if (canMerge(prop1, prop2))
            {
                merge(prop1, prop2);
                merged = true;
                required.emplace(prop1->getValue<std::string>("propertyName"));
                break;
            }
        }

        if (!merged)
        {
            properties1->add(prop2);
        }
    }

    Poco::JSON::Array::Ptr reqArr = new Poco::JSON::Array;

    for (std::string const &propName : required)
    {
        reqArr->add(propName);
    }

    obj1->set("required", reqArr);
}

/**
 * Method for merging the schemas of the JSON array variables.
 * Combines the domains of the arrays (with duplicates) and the inneTypes (merging them, when possible).
 *
 * @param obj1 JSON schema object. Contains the result of the merge.
 * @param obj2 The other schema object.
 */
void bdg::components::schemaInferer::JsonInferer::mergeArrays(Poco::JSON::Object::Ptr &obj1, Poco::JSON::Object::Ptr &obj2)
{
    // Merge domains.
    Poco::JSON::Array::Ptr tmpDomain = obj2->getObject("properties")->getArray("domain");

    for (int i = 0; i < tmpDomain->size(); ++i)
    {
        obj1->getObject("properties")->getArray("domain")->add(tmpDomain->get(i));
    }

    // Merge innerTypes.
    Poco::JSON::Array::Ptr innerType1 = obj1->getObject("properties")->getArray("innerType");
    Poco::JSON::Array::Ptr innerType2 = obj2->getObject("properties")->getArray("innerType");

    for (int j = 0; j < innerType2->size(); ++j)
    {
        bool merged = false;
        Poco::JSON::Object::Ptr iType2 = innerType2->get(j).extract<Poco::JSON::Object::Ptr>();
       
        for (int i = 0; i < innerType1->size(); ++i)
        {
            Poco::JSON::Object::Ptr iType1 = innerType1->get(i).extract<Poco::JSON::Object::Ptr>();
            
            if (canMerge(iType1, iType2))
            {
                merge(iType1, iType2);
                merged = true;
                break;
            }
        }

        if (!merged)
        {
            innerType1->add(iType2);
        }
    }

    updateMinMax<int>(obj1, obj2);
}

/**
 * Method that compares two existing schema objects and decides, whether they can be merged.
 *
 * @param obj1 JSON schema object.
 * @param obj2 The other schema object.
 */
bool bdg::components::schemaInferer::JsonInferer::canMerge(Poco::JSON::Object::Ptr &obj1, Poco::JSON::Object::Ptr &obj2)
{
    if (obj1->get("type").toString() != obj2->get("type").toString())
    {
        return false;
    }

    if (obj1->has("typeName") || obj2->has("typeName"))
    {
        if (obj1->get("typeName").toString() != obj2->get("typeName"))
        {
            return false;
        }

        return true;
    }

    if (obj1->has("propertyName") || obj2->has("propertyName"))
    {
        if (obj1->get("propertyName").toString() != obj2->get("propertyName"))
        {
            return false;
        }

        return true;
    }

    // From here on obj1, obj2 must represent itemTypes.
    // Two inner types of type "object" are considered of the same type, if they have identical "required" attribute.
    if (obj1->get("type") == "object" && obj2->get("type") == "object")
    {
        Poco::JSON::Array::Ptr req1 = obj1->getArray("required");
        Poco::JSON::Array::Ptr req2 = obj2->getArray("required");

        if (req1->size() != req2->size())
        {
            return false;
        }

        for (int i = 0; i < req1->size(); ++i)
        {
            bool found = false;
            
            for(int j = 0; j < req2->size(); ++j)
            {
                if (req1->get(i) == req2->get(j))
                {
                    found = true;
                }
            }

            if (!found)
            {
                return false;
            }
        }
    }

    // With arrays, for each inner type in array1, there must by at least one meargable inner type in array2
    // This can lead to recursion so it can get little expensive with non-trivial inner types.
    if (obj1->get("type") == "array" && obj2->get("type") == "array")
    {
        Poco::JSON::Array::Ptr types1 = obj1->getObject("properties")->getArray("innerType");
        Poco::JSON::Array::Ptr types2 = obj2->getObject("properties")->getArray("innerType");

        if (types1->size() != types2->size())
        {
            return false;
        }

        for (int i = 0; i < types1->size(); ++i)
        {
            Poco::JSON::Object::Ptr type1 = types1->get(i).extract<Poco::JSON::Object::Ptr>();
            bool found = false;
            
            for(int j = 0; j < types2->size(); ++j)
            {
                Poco::JSON::Object::Ptr type2 = types2->get(j).extract<Poco::JSON::Object::Ptr>();
                
                if (canMerge(type1, type2))
                {
                    found = true;
                }
            }

            if (!found)
            {
                return false;
            }
        }
    }

    return true;
}

/**
 * Method that updates the min/max values of the schema object after the merge.
 *
 * @param obj1 JSON schema object. Contains the result of the merge.
 * @param obj2 The other schema object.
 */
template<typename T>
void bdg::components::schemaInferer::JsonInferer::updateMinMax(Poco::JSON::Object::Ptr &obj1, Poco::JSON::Object::Ptr &obj2)
{
    T newMin = std::min(obj1->getObject("properties")->get("min").convert<T>(), obj2->getObject("properties")->get("min").convert<T>());
    T newMax = std::max(obj1->getObject("properties")->get("max").convert<T>(), obj2->getObject("properties")->get("max").convert<T>());
    
    // Update new min/max values.
    // For some reason, the values has to be removed first. 
    obj1->getObject("properties")->remove("min");
    obj1->getObject("properties")->remove("max");
    obj1->getObject("properties")->set("min", newMin);
    obj1->getObject("properties")->set("max", newMax);
}

using bdg::components::schemaInferer::InfererPlugin;
using bdg::components::schemaInferer::JsonInferer;

/**
 * Export the plugin for the POCO class loader.
 */
POCO_BEGIN_MANIFEST(InfererPlugin)
    POCO_EXPORT_CLASS(JsonInferer)
POCO_END_MANIFEST
