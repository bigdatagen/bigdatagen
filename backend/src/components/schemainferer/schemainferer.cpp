/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Components
 * @author   Big Data Generator Team
 */

#include "components/schemainferer/schemainferer.h"     // Header
#include "utils/logger.h"                               // Logger
#include "utils/config.h"                               // Config
#include "utils/fileupload.h"                           // FileUpload

#include <Poco/Exception.h>         // Poco::Exception
#include <Poco/JSON/Object.h>       // Poco::JSON::Object
#include <Poco/JSON/Array.h>        // Poco::JSON::Array
#include <Poco/JSON/Parser.h>       // Poco::JSON::Parser
#include <Poco/ClassLibrary.h>      // POCO Export class

#include <sstream>      // std::ostream

using namespace bdg::utils;

/**
 * Default constructor used by the server. Therefore a ServerLogger is used for logging.
 * If you want to use a different logger, use the other constructor.
 */
bdg::components::SchemaInferer::SchemaInferer() :
    logger(bdg::utils::Logger::getInstance().getServerLogger()), pluginManager(bdg::utils::Logger::getInstance().getServerLogger(), bdg::utils::Config::getInstance().getInfererPluginsPath("infererplugins"))
{
    // Nothing here.
}

/**
 * Constructor with a specified logger.
 *
 * @param logger Which logger should be used.
 */
bdg::components::SchemaInferer::SchemaInferer(Poco::Logger &logger) :
    logger(logger), pluginManager(logger, bdg::utils::Config::getInstance().getInfererPluginsPath("infererplugins"))
{
    // Nothing here.
}

/**
 * Implementation of the default base plugin runner method.
 * Exceptions thrown by the inferrer plugin are handle within this method.
 * If a "soft" exception (concerning only some of the inferred files, e.g. due to invalid syntax), warning is printed
 * and a schema is returned.
 * In case of other exceptions (e.g. invalid parameter value, missing files) a JSON file containing the exception description
 * is returned instead of a schema.
 *
 * @param parameters Parameters specifying the inferrer name and which action should be executed.
 *         "command":
 *             "list" - list all the available inferer plugins
 *             "run" - run the inferer (if "parameter.name" is provided)
 *         "name": name of the inferer plugin
 * @param uploadedFiles List of the files that will be inferred.
 *
 * @return String value containing the inferred schema or an exception description.
 */
std::string bdg::components::SchemaInferer::run(const bdg::server::ServerPlugin::Parameters &parameters, const FileUpload::FileUploadCollection &uploadedFiles)
{
    std::stringstream output("");
    std::string command = "run";
    std::string pluginName = "";
    Poco::JSON::Object::Ptr configurationObject;

    const auto configurationIt = parameters.find("configuration");

    if (configurationIt != parameters.end())
    {
        try
        {
            Poco::Dynamic::Var configurationVar = Poco::JSON::Parser().parse(configurationIt->second);
            configurationObject = configurationVar.extract<Poco::JSON::Object::Ptr>();

            if (configurationObject->has("command"))
            {
                command = configurationObject->getValue<std::string>("command");
            }

            if (configurationObject->has("name"))
            {   
                pluginName = configurationObject->getValue<std::string>("name");
            }
        }
        catch (...)
        {
            // We just need to catch exceptions caused by invalid JSON.
        }
    }

    // Process the parameters the parameters.
    if (command == "list")
    {
        Poco::JSON::Object::Ptr root = new Poco::JSON::Object;
        root->set("plugins", listPlugins());

        root->stringify(output);
    }
    else if (command == "run" && pluginName != "")
    {
        // Check, if there are any files present.
        if (uploadedFiles.size() == 0)
        {
            throw Poco::InvalidArgumentException("There are no uploaded files to infer.");
        }

        try
        {
            runInferer(pluginName, configurationObject, uploadedFiles, output);
        }
        // This is the way the inferers should be reporting files with invalid syntax.
        catch (Poco::RuntimeException &e)
        {
            bdg_warning(logger, "Warning: " + e.message());
        }
        catch (Poco::Exception &e)
        {
            return R"({"error": ")" + e.displayText() + R"("})";
        }
    }
    else
    {
        return R"({"error": "Invalid command: [)" + command + R"(] [)" + pluginName + R"(]"})";
    }

    // TODO: store into the file based on the parameters?
    return output.str();
}

/**
 * Method that actually executes the selected inferrer plugin.
 * Inferrer is fetched from the pluginManager. Filenames are registered to the inferrer
 * and a configuration object is passed during the inferrer execution to further specify
 * the behavior of the inferrer.
 *
 * @param name Name of the inferrer plugin.
 * @param configObject Configuration containing parameters of the plugins.
 * @param uploadedFiles Files that should be inferred.
 * @param os Variable containing the inferrer output.
 */
void bdg::components::SchemaInferer::runInferer(
    std::string &name,
    const Poco::JSON::Object::Ptr &configObject,
    const bdg::utils::FileUpload::FileUploadCollection &uploadedFiles,
    std::stringstream &os)
{
    try
    {
        std::unique_ptr<bdg::components::schemaInferer::InfererPlugin> inferer = pluginManager.getPlugin(name);

        for (const auto &it : uploadedFiles)
        {
            inferer->addFile(it.second->getTemporaryFile());
        }

        inferer->inferSchema(os, configObject);
    }
    catch (Poco::Exception &e)
    {
        // Just propagate the exception to SchemaInferer::run().
        e.rethrow();
    }
}

using bdg::server::ServerPlugin;
using bdg::components::SchemaInferer;

/**
 * Export the plugin for the POCO class loader.
 */
POCO_BEGIN_MANIFEST(ServerPlugin)
    POCO_EXPORT_CLASS(SchemaInferer)
POCO_END_MANIFEST
