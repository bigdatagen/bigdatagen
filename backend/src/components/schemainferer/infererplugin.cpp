/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Transformer
 * @author   Big Data Generator Team
 */

#include "components/schemainferer/infererplugin.h"     // Header

#include <Poco/StreamCopier.h>          // Poco::StreamCopier
#include <Poco/InflatingStream.h>       // Poco::InflatingInputStream
#include <Poco/Zip/Decompress.h>        // Poco::Zip::Decompress
#include <Poco/UUID.h>                  // Poco::UUID
#include <Poco/UUIDGenerator.h>         // Poco::UUIDGenerator
#include <Poco/DirectoryIterator.h>     // Poco::DirectoryIterator
#include <Poco/Exception.h>             // Poco::FileNotFoundException

#include <fstream>      // std::ifstream

/**
 * Default constructor of the inferrer.
 */
bdg::components::schemaInferer::InfererPlugin::InfererPlugin()
{
    filenames.clear();
}

/**
 * Method that is used to process a specified directory and register all the present files
 * for the inferrence.
 *
 * @param relPath Relative path to the directory containing the files.
 */
void bdg::components::schemaInferer::InfererPlugin::addDirectory(const std::string &relPath)
{
    try
    {
        Poco::DirectoryIterator end;

        for (Poco::DirectoryIterator dirIt(relPath); dirIt != end; ++dirIt)
        {
            addFile(dirIt->path());
        }
    }
    catch(Poco::Exception &e)
    {
        e.rethrow();
    }
}

/*
 * If the file is a zip archive it unpacks the files into a temporary directory (randomly
 * generated directory name to avoid collisions) and inserts the filenames, name of the temporary
 * directory is returned for a future references. Otherwise, it just
 * inserts the filename to the list.
 *
 * @param relPath Relative path to the directory containing the files.
 *
 * @return tempDir Name of the temporary directory (empty if not zipped).
 */
std::string bdg::components::schemaInferer::InfererPlugin::addFile(const std::string &relPath)
{
    // TODO: Can we identify the .zip files in a better way?
    if (Poco::Path(relPath).getExtension() == "zip")
    {
        std::ifstream ifs(relPath, std::ios::binary);

        if (!ifs.is_open())
        {
            throw Poco::OpenFileException("(Inferer Plugin) Cannot open file " + relPath + ".");
        }

        // We decompress the files into a temporary directory to avoid collisions.
        std::string tempDir = Poco::Path(relPath).parent().toString() + Poco::UUIDGenerator::defaultGenerator().createRandom().toString();

        // Decompress the archive.
        Poco::Zip::Decompress dec(ifs, tempDir);
        dec.decompressAllFiles();

        // Add the decompressed files to the list.
        for (auto &file : dec.mapping())
        {
            Poco::Path filepath(tempDir, file.first);
            this->addFile(filepath.toString());
        }

        return tempDir;
    }
    else
    {
        filenames.insert(relPath);

        return "";
    }
}
