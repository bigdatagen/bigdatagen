/**
 * Copyright Big Data Generator
 *
 * This file is part of the Big Data Generator SW Project.
 *
 * @category Transformer
 * @author   Big Data Generator Team
 */

#include "components/schemainferer/csvinferer/csvinferer.h"         // Header

#include <Poco/JSON/JSONException.h>        // Poco::JSON::JSONException
#include <Poco/NumberParser.h>              // Poco::NumberParser
#include <Poco/ClassLibrary.h>              // POCO Export class

#include <fstream>      // std::ifstream
#include <cctype>       // std::isdigit

/**
 * Default constructor filling up the info about the inferrer. 
 */
bdg::components::schemaInferer::CsvInferer::CsvInferer()
{
    name = "CsvInferer";
    description = "Inferer plugin for infering CSV schemas";
    parameters["separator"] = "Field separator in the infered files (must be a single character).";
}

/**
 * Method that is accessible by the SchemaInfererPlugin that begins the execution of the inferrer.
 *
 * @param os Variable containing the inferrer output.
 * @param params Plugin parameters.
 */
void bdg::components::schemaInferer::CsvInferer::inferSchema(std::ostream &os, const Poco::JSON::Object::Ptr &params)
{
    if (filenames.empty())
    {
        throw Poco::InvalidArgumentException("(CSV Inferer) No files to process.");
    }
    else
    {
        std::string separator = ",";

        if (params->has("separator"))
        {
            separator = params->getValue<std::string>("separator");
        }

        if (separator.length() != 1)
        {
            throw Poco::InvalidArgumentException("(CSV Inferer) The columns separator should be a single character.");
        }

        std::string skippedFiles("");
        Poco::JSON::Object::Ptr schemaRoot = new Poco::JSON::Object();

        // We use the goto label to handle the invalid files.
        // This can be probably done more nicely.
        PROCESS_FILES: for(const std::string &file : filenames)
        {
            std::ifstream ifs(file, std::ios::binary);

            if (!ifs.is_open())
            {
                throw Poco::OpenFileException("(CSV Inferer) Cannot open file " + file + ".");
            }

            Poco::JSON::Object::Ptr newRoot;

            try
            {
                newRoot = csvToSchema(ifs, separator);

                if (schemaRoot->size() == 0)
                {
                    schemaRoot = newRoot;
                }
                else
                {
                    mergeRoot(schemaRoot, newRoot);
                }
            }
            // Rethrow, with a filename reported.
            catch (Poco::Exception &e)
            {
                skippedFiles.append("[" + file + ": " + e.message() + "]");
                removeFilename(file);

                goto PROCESS_FILES;
            }
        }

        // Default root schema element is ARRAY, which contains ROOT object (and possibly UDT objects).
        Poco::JSON::Array::Ptr schemaRootArray = new Poco::JSON::Array;
        schemaRootArray->add(schemaRoot);
        schemaRootArray->stringify(os);

        if (skippedFiles != "")
        {
            throw Poco::RuntimeException("Skipped files: " + skippedFiles);
        }
    }
}

/**
 * Parses the input CSV file and generates proper schema representation. Schema is a JSON 
 * object, so we use Poco structures for representation.
 *
 * @param ifs Input stream containing the file to be inferred.
 * @param separator Separator that is used to separate the entries on each line.
 *
 * @return JSON object containing the inferred schema.
 */
Poco::JSON::Object::Ptr bdg::components::schemaInferer::CsvInferer::csvToSchema(std::istream &ifs, std::string &separator)
{
    Poco::JSON::Object::Ptr schemaRoot = new Poco::JSON::Object;

    std::string line;
    int lineCount = 0;
    Poco::JSON::Array::Ptr lineArray = new Poco::JSON::Array;

    while (std::getline(ifs, line))
    {
        try
        {
            Poco::JSON::Array::Ptr newLineArray = getColumns(line, separator);
            
            if (lineArray->size() == 0)
            {
                lineArray = newLineArray;
            }
            else
            {
                mergeLines(lineArray, newLineArray);
            }
        }
        catch (Poco::Exception &e)
        {
            e.rethrow();
        }

        ++lineCount;
    }

    schemaRoot->set("type", "ROOT");
    schemaRoot->set("typeName", "ROOT");
    schemaRoot->set("columns", lineArray);
    schemaRoot->set("separator", separator);
    schemaRoot->set("min", lineCount);
    schemaRoot->set("max", lineCount);

    return schemaRoot;
}

/**
 * Method that creates an array of columns from the input line.
 *
 * @param line The line that is to be processed.
 * @param separator Separator that is used to split the values.
 *
 * @return JSON object that contains the schema describing the line.
 */
Poco::JSON::Array::Ptr bdg::components::schemaInferer::CsvInferer::getColumns(std::string &line, std::string &separator)
{
    Poco::JSON::Array::Ptr result = new Poco::JSON::Array;
    std::istringstream lineStream(line);
    std::string token;

    while (std::getline(lineStream, token, separator.at(0)))
    {
        Poco::JSON::Object::Ptr entry = new Poco::JSON::Object;
        Poco::JSON::Object::Ptr properties = new Poco::JSON::Object;
        properties->set("distribution", "uniform");
        properties->set("source", "domain");
        Poco::JSON::Array::Ptr domain = new Poco::JSON::Array;

        int valueInt;
        double valueFloat;

        // First, we check for missing token.
        if(token == "")
        {
            entry->set("type", "string");
            domain->add("");
            properties->set("min", 0);
            properties->set("max", 0);
            properties->set("domain", domain);
        }
        // We do not want to falsely identify strings starting with decimal-like notation as integers. 
        else if (std::isdigit(token.at(0)) && Poco::NumberParser::tryParse(token, valueInt))
        {
    
            entry->set("type", "int");
            domain->add(valueInt);
            properties->set("min", valueInt);
            properties->set("max", valueInt);
            properties->set("domain", domain);
        }
        else if (Poco::NumberParser::tryParseFloat(token, valueFloat))
        {
            entry->set("type", "float");
            domain->add(valueFloat);
            properties->set("min", valueFloat);
            properties->set("max", valueFloat);
            properties->set("domain", domain);
        }
        // String.
        else
        {
            entry->set("type", "string");
            domain->add(token);
            properties->set("min", token.length());
            properties->set("max", token.length());
            properties->set("domain", domain);
        }

        entry->set("properties", properties);
        result->add(entry);
    }

    return result;
}

/**
 * Merges two arrays of columns line1, line2 and returns line1 as a result.
 *
 * @param line1 First line.
 * @param line2 Second line.
 */
void bdg::components::schemaInferer::CsvInferer::mergeLines(Poco::JSON::Array::Ptr &line1, Poco::JSON::Array::Ptr &line2)
{
    // We expect consistency in the number of columns.
    if (line1->size() != line2->size())
    {
        throw Poco::InvalidArgumentException("(CSV Inferer) The number of columns is different between the CSV entries.");
    }

    for (int i = 0; i < line1->size(); ++i)
    {
        Poco::JSON::Object::Ptr obj1 = line1->getObject(i);
        Poco::JSON::Object::Ptr obj2 = line2->getObject(i);
        mergeColumns(obj1, obj2);
    } 
}

/**
 * Merges two column entries column1, column2 and returns column1 as a result.
 *
 * @param column1 First column to merge. Contains result of the merge.
 * @param column2 The other column.
 */
void bdg::components::schemaInferer::CsvInferer::mergeColumns(Poco::JSON::Object::Ptr &column1, Poco::JSON::Object::Ptr &column2)
{
    if (column1->get("type") == "string" || column2->get("type") == "string")
    {
        column1->set("type", "string");
        updateMinMax<std::string>(column1, column2);
    }
    else if (column1->get("type") == "float" || column2->get("type") == "float")
    {
        column1->set("type", "float");
        updateMinMax<double>(column1, column2);
    }
    else
    {
        column1->set("type", "int");
        updateMinMax<int>(column1, column2);
    }

    // Merge domains.
    Poco::JSON::Array::Ptr tmpDomain = column2->getObject("properties")->getArray("domain");

    for (int i = 0; i < tmpDomain->size(); ++i)
    {
        column1->getObject("properties")->getArray("domain")->add(tmpDomain->get(i));
    }
}

/**
 * Merges two schemas of the CSV files.
 *
 * @param obj1 The first file. Contains the result of the merge.
 * @param obj2 The second file.
 */
void bdg::components::schemaInferer::CsvInferer::mergeRoot(Poco::JSON::Object::Ptr &obj1, Poco::JSON::Object::Ptr &obj2)
{
    // Update the min/max values.
    int newMin = std::min(obj1->get("min").convert<int>(), obj2->get("min").convert<int>());
    int newMax = std::max(obj1->get("max").convert<int>(), obj2->get("max").convert<int>());

    // Update new min/max values.
    // For some reason, the values has to be removed first.
    obj1->remove("min");
    obj1->remove("max");
    obj1->set("min", newMin);
    obj1->set("max", newMax);

    Poco::JSON::Array::Ptr arr1 = obj1->getArray("columns");
    Poco::JSON::Array::Ptr arr2 = obj2->getArray("columns");
    mergeLines(arr1, arr2);
}

/**
 * Method that updates the min/max values after merging two schema objects.
 *
 * @param obj1 The first object.
 * @param obj2 The second object.
 */
template<class T>
void bdg::components::schemaInferer::CsvInferer::updateMinMax(Poco::JSON::Object::Ptr &obj1, Poco::JSON::Object::Ptr &obj2)
{
    T newMin = std::min(obj1->getObject("properties")->get("min").convert<T>(), obj2->getObject("properties")->get("min").convert<T>());
    T newMax = std::max(obj1->getObject("properties")->get("max").convert<T>(), obj2->getObject("properties")->get("max").convert<T>());

    // Update new min/max values.
    // For some reason, the values has to be removed first.
    obj1->getObject("properties")->remove("min");
    obj1->getObject("properties")->remove("max");
    obj1->getObject("properties")->set("min", newMin);
    obj1->getObject("properties")->set("max", newMax);

}

using bdg::components::schemaInferer::InfererPlugin;
using bdg::components::schemaInferer::CsvInferer;

/**
 * Export the plugin for the POCO class loader.
 */
POCO_BEGIN_MANIFEST(InfererPlugin)
    POCO_EXPORT_CLASS(CsvInferer)
POCO_END_MANIFEST
