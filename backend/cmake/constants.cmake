message("BDG Project ROOT directory: ${CMAKE_SOURCE_DIR}")


### COMMON CMAKE VARIABLES ###

# Set the compiler to standard c++14
set(CMAKE_CXX_STANDARD 14)
set(CXX_STANDARD_REQUIRED ON)

# Add project specific cmake modules
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/modules/")

# Find MPI specific directories (see "cmake --help-module FindMPI")
find_package(MPI REQUIRED)

# Set the MPI compiler wrapper and flags
message(STATUS "Using MPI compiler: ${MPI_CXX_COMPILER} ${MPI_CXX_COMPILE_FLAGS}")

# MPI-specific might not be present
if(${MPI_CXX_FOUND})
		if(MPI_CXX_COMPILER)
	        set(CMAKE_CXX_COMPILER ${MPI_CXX_COMPILER})
		endif()
		set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${MPI_CXX_COMPILE_FLAGS}")
		message(STATUS "Found MPI compiler (with flags): ${MPI_CXX_COMPILER} (${MPI_CXX_COMPILE_FLAGS})")
endif()
message(STATUS "Compiler set to (with flags): ${CMAKE_CXX_COMPILER} (${CMAKE_CXX_FLAGS})")

# Find OPENSSL
if(ENABLE_CRYPTO)
	find_package(OpenSSL REQUIRED)
endif()


### PROJECT SPECIFIC VARIABLES ###

set(BDG_BIN_DIR "${CMAKE_BINARY_DIR}/bin")
message(STATUS "Directory for built binaries: ${BDG_BIN_DIR}")

set(SRC_DIR ${CMAKE_SOURCE_DIR}/src)
set(HDR_DIR ${CMAKE_SOURCE_DIR}/include)
set(MISC_DIR ${CMAKE_SOURCE_DIR}/misc)
set(TEST_DIR ${CMAKE_SOURCE_DIR}/unit_tests)
set(3RD_PARTY_DIR ${CMAKE_SOURCE_DIR}/3rd_party)
set(BUILD_DIR ${CMAKE_SOURCE_DIR}/3rd_party)
set(CPPUNIT_ROOT_DIR ${3RD_PARTY_DIR}/cppunit) 

#option(BDG_WIN64 "Solution platform" OFF)
option(BDG_RELEASE "Solution configuration" OFF)

if (BDG_RELEASE)
	set(BDG_CONF "Release")
	set(CMAKE_BUILD_TYPE Release)
else()
	set(BDG_CONF "Debug")
	set(CMAKE_BUILD_TYPE Debug)
endif()

set(BDG_BIN_DIR_FULL "${BDG_BIN_DIR}/${BDG_CONF}")

if (UNIX)
	set(BDG_BIN_DIR_FULL "${BDG_BIN_DIR}")
endif()

if (UNIX)
	set(BDG_LIB_DIR "${CMAKE_BINARY_DIR}/lib")
else()
	# WIN: We want to put the dynamic libraries to the same dir as executables
	set(BDG_LIB_DIR "${BDG_BIN_DIR_FULL}")
endif()
