### SET PATH TO PROJECT LIBRARY ###
MACRO(SET_PROJECT_LIBRARY_PATH)
    if (UNIX)
        if (BDG_RELEASE)
            set(PROJ_PATH "${PROJECT_BINARY_DIR}/lib${PROJECT_NAME}.so")
        else()
            set(PROJ_PATH "${PROJECT_BINARY_DIR}/lib${PROJECT_NAME}d.so")
        endif()
    else()
        if (BDG_RELEASE)
            set(PROJ_PATH "${PROJECT_BINARY_DIR}/Release/${PROJECT_NAME}.dll")
        else()
            set(PROJ_PATH "${PROJECT_BINARY_DIR}/Debug/${PROJECT_NAME}d.dll")
        endif()
    endif()

    message(${PROJ_PATH})
ENDMACRO()

### COPY PLUGINS ###
MACRO(POST_BUILD_PLUGIN_COPY)
    SET_PROJECT_LIBRARY_PATH()

    add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy_if_different
        "${PROJ_PATH}"							# what
		"${CMAKE_BINARY_DIR}/pipelineplugins/"	# where to
    )
ENDMACRO()

### COPY COMPONENTS ###
MACRO(POST_BUILD_COMPONENT_COPY)
    SET_PROJECT_LIBRARY_PATH()

    add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy_if_different
        "${PROJ_PATH}"
		"${CMAKE_BINARY_DIR}/plugins/"
    )
ENDMACRO()

### COPY SOCKET PLUGINS ###
MACRO(POST_BUILD_SOCKET_COPY)
    SET_PROJECT_LIBRARY_PATH()

    add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy_if_different
        "${PROJ_PATH}"
		"${CMAKE_BINARY_DIR}/socketplugins/"
    )
ENDMACRO()

### COPY INFERERS ###
MACRO(POST_BUILD_INFERER_COPY)
	SET_PROJECT_LIBRARY_PATH()

	# Copy for project
	add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
		COMMAND ${CMAKE_COMMAND} -E copy_if_different
		"${PROJ_PATH}"
		"${CMAKE_BINARY_DIR}/infererplugins/"
	)

	# Copy for unit_tests
	add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
		COMMAND ${CMAKE_COMMAND} -E copy_if_different
		"${PROJ_PATH}"
		"${CMAKE_BINARY_DIR}/unit_tests/runner/infererplugins/"
	)
ENDMACRO()

### COPY LIBRARIES ###
MACRO(POST_BUILD_LIB_COPY)
    SET_PROJECT_LIBRARY_PATH()

    add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy_if_different
        "${PROJ_PATH}"		# what
		"${BDG_LIB_DIR}/"	# where to
    )
ENDMACRO()

### COPY TESTS ###
MACRO(POST_BUILD_TEST_COPY TESTSUITE REL_PATH)
	add_custom_command(TARGET ${TESTSUITE} POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy_directory
		"${TEST_DIR}/${REL_PATH}/data"		# what
		"${CMAKE_BINARY_DIR}/unit_tests/runner/data"	# where to
    )
ENDMACRO()

### COPY BINARIES ###
MACRO(POST_BUILD_BINARY_COPY)
	if (UNIX)
		set(PROJ_PATH "${PROJECT_BINARY_DIR}/${PROJECT_NAME}")
	else()
		if(BDG_RELEASE)
			set(PROJ_PATH "${PROJECT_BINARY_DIR}/Release/${PROJECT_NAME}.exe")
		else()
			set(PROJ_PATH "${PROJECT_BINARY_DIR}/Debug/${PROJECT_NAME}.exe")
		endif()
	endif()

	add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
		COMMAND ${CMAKE_COMMAND} -E copy_if_different
		"${PROJ_PATH}"
		"${BDG_BIN_DIR_FULL}/"
	)
ENDMACRO()
