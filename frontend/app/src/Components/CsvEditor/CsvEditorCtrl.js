'use strict';

angular.module('BDGApp.CsvEditor', ['ngDragDrop', 'ui.sortable', 'BDGApp.PropertyEditor', 'ui.bootstrap', 'at.multirange-slider'])

.controller('CsvEditorCtrl', ['$scope', '$state', '$stateParams','fileDownloadSvc',
    function($scope, $state, $stateParams, fileDownloadSvc) {

        /**
         * This if branch is used when the editor is entered by the option "From Template" of "Use Inferrer".
         */
        if ($stateParams.schema !== undefined){
            if($state.current.data.config.schema == undefined){
                $state.current.data.config.schema = angular.fromJson($stateParams.schema);
                $scope.csvEditorInput = $state.current.data.config.schema;
            }
        }
        //sample root schema
        if($state.current.data.config.schema == undefined){
            $state.current.data.config.schema = [
                {
                    type: "ROOT",
                    label: "Row",
                    typeName: "ROOT",
                    separator: ";",
                    min: 1,
                    max: 100,
                    columns: [
                    ]
                }
            ];
        }
        $scope.csvEditorInput = $state.current.data.config.schema;

        
        $scope.data = {
            uploadFile: undefined
        };

        /**
         * Callback on file upload. It replaces current schema of editor by the uploaded one.
         */
        $scope.$watch("data.uploadFile", function (newValue) {
            if (newValue) {
                $state.current.data.config.schema = angular.fromJson(newValue);
                $scope.csvEditorInput = $state.current.data.config.schema;
                newValue = undefined;
            }
        });

        /**
         * Downloads the actual schema.
         */
        $scope.download = function () {
            fileDownloadSvc.download($scope.csvEditorInput, 'configuration.json');
        };
    }
]);

