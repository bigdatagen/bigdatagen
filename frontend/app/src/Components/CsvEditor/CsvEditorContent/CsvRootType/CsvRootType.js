angular.module('BDGApp.CsvEditor')

/**
 * Type which defines resulting rows of generated CSV file.
 */
.directive('csvRootType', ['typeSvc',
    function (typeSvc) {
        "use strict";
        return {
            restrict: 'EA',
            replace: false,
            templateUrl: "src/Components/CsvEditor/CsvEditorContent/CsvRootType/CsvRootType.tpl.html",
            scope: {
                //Definition of the type's content
                input: "=",
                //Object which contains information about active type in current editor.
                //It is used for making element active.
                activeProperty: "="
            },
            link: function ($scope) {
                $scope.over = false;
                $scope.isArray = true;
                $scope.typeLabel = typeSvc.getTypeLabel($scope.input.type);

                /**
                 * Function called from child nodes. It deletes child at specified index.
                 * @param index
                 */
                $scope.deleteChild = function(index) {
                    $scope.activeProperty.property = undefined;
                    $scope.input.columns.splice(index, 1);
                };

                /**
                 * Callback function which is called when type from typeBox is dragged over the element.
                 * @param e
                 */
                $scope.enter = function (e) {
                    //If we are sorting, not dragging from typeBox we want to do nothing.
                    if (e.handleObj.namespace === "sortable") {
                        return;
                    }
                    $scope.over = true;
                    //We are outside of Angular environment since we are in callback, not in digest cycle
                    //so we need to start digest
                    $scope.$apply();
                };

                /**
                 * Callback function which is called when type from typeBox is dragged out of the element.
                 */
                $scope.leave = function () {
                    $scope.over = false;
                    //We are outside of Angular environment since we are in callback, not in digest cycle
                    //so we need to start digest
                    $scope.$apply();
                };

                $scope.drop = function () {
                    $scope.over = false;
                };

                /**
                 * Callback on mouse click. It makes element active.
                 */
                $scope.makeActive = function () {
                    $scope.activeProperty.property = $scope.input;
                };

                /**
                 * Checks whether the element should be visualized as active or not.
                 */
                $scope.$watch("activeProperty.property", function (newValue) {
                    if (newValue === $scope.input) {
                        $scope.active = true;
                    }
                    else {
                        $scope.active = false;
                    }
                });

                /**
                 * $scope.draggedItem is filled when type from typeBox is dropped over the element.
                 * If the droppedItem is defined, it means that there is an dropped type which was not processed yet.
                 * We need to add this element as another child.
                 */
                $scope.$watch("droppedItem", function (type) {
                    if (type !== undefined) {
                        $scope.droppedItem = undefined;
                        var obj = {
                            type: type.type,
                            properties: {}
                        };
                        if ($scope.input.columns === undefined) {
                            $scope.input.columns = [];
                        }
                        $scope.input.columns.push(obj);

                        //Making newly created item active
                        $scope.activeProperty.property = obj;
                    }
                });
            }
        };
    }
]);