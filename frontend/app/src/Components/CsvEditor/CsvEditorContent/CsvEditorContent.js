angular.module('BDGApp.CsvEditor')

/**
 * Directive for rendering content of CSV active type.
 */
.directive('csvEditorContent', [ "CsvEditorTypes", "SequenceType", "CsvEditorRootType", "CsvEditorDefaultType",
    function (CsvEditorTypes, SequenceType, CsvEditorRootType, CsvEditorDefaultType) {
        "use strict";
        return {
            restrict: 'EA',
            replace: false,
            templateUrl: "src/Components/CsvEditor/CsvEditorContent/CsvEditorContent.tpl.html",
            scope: {
                //Definition of the active type's content
                input: "=",
                //Object which contains information about active type in current editor.
                //Active type is rendered.
                activeType: "="
            },
            link: function ($scope) {
                $scope.typeMapping = {};
                $scope.isRoot = true;

                //maping between type name and its definition
                angular.forEach(CsvEditorTypes, function (value) {
                    $scope.typeMapping[value.type] = value;
                });

                $scope.data = {
                    activeProperty : {
                        property: undefined
                    }
                };

                $scope.activeForm = undefined;
                $scope.activeSchema = undefined;

                /**
                 * Deactivates the active element. It is used as callback when user clicks outside of any element.
                 */
                $scope.deactivate = function () {
                    $scope.activeForm = undefined;
                    $scope.activeSchema = undefined;
                    $scope.data.activeProperty.property = undefined;
                };

                /**
                 * Routine which is called when active property is changed.
                 * It propagates information about type to PropertyEditor.
                 */
                $scope.$watch("data.activeProperty.property", function (newValue) {
                    if (newValue && newValue.type === "sequence") {
                        $scope.activeForm = angular.copy(SequenceType[newValue.innerType].form);
                        $scope.activeSchema = angular.copy(SequenceType[newValue.innerType].schema);
                    }
                    else if (newValue && newValue.type === CsvEditorRootType.type) {
                        $scope.activeForm = angular.copy(CsvEditorRootType.form);
                        $scope.activeSchema = angular.copy(CsvEditorRootType.schema);
                    }
                    else if (newValue !== undefined) {
                        if ($scope.typeMapping[newValue.type]) {
                            $scope.activeForm = angular.copy($scope.typeMapping[newValue.type].form);
                            $scope.activeSchema = angular.copy($scope.typeMapping[newValue.type].schema);
                        }
                        else {
                            $scope.activeForm = CsvEditorDefaultType.form;
                            $scope.activeSchema = CsvEditorDefaultType.schema;
                        }
                    }
                    else {
                        $scope.deactivate();
                    }
                });

                /**
                 * propagates the information from typeBox about activeProperty the children node.
                 */
                $scope.$watch("activeType", function (newVal, oldVal) {
                    if (!angular.equals(newVal, oldVal)) {
                        $scope.data.activeProperty.property = newVal;
                    }
                });
            }
        };
    }
]);