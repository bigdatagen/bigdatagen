angular.module('BDGApp.CsvEditor')

.constant("CsvEditorTypes", [
    {type: "int", label: "Integer",
        schema: {
            type: "object",
            properties: {
                type: {
                    title: "Type",
                    type: "string"
                },

                properties: {
                    type: "object",
                    properties:{
                        source: {
                            title: "Source",
                            type: "string",
                            enum: ["random", "domain"],
                            default: "random"
                        },
                        min: {
                            title: "Minimum",
                            type: "integer",
                            default: 0
                        },
                        max: {
                            title: "Maximum",
                            type: "integer",
                            default: 100
                        },
                        domain:{
                            title: "Domain",
                            type: "array",
                            minItems: 1,
                            items: {
                                type: "integer"
                            }
                        },
                        distribution: {
                            title: "Distribution",
                            type: "string",
                            enum: ["uniform", "normal"],
                            default: "uniform"
                        }
                    }
                }
            }
        },
        form: [
            {
                "key": "type",
                "readonly": "true",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                "key": "properties.source",
                "disableSuccessState": true,
                "feedback": false
            },

            {
                "key":  "properties.min",
                "condition": "model.properties.source == 'random'",
                "disableSuccessState": true,
                "feedback": false,
                "validationMessage": {
                    wrongNumber: "Value of min must be less than maximum!"
                }
            },
            {
                "key":  "properties.max",
                "condition": "model.properties.source == 'random'",
                "disableSuccessState": true,
                "feedback": false,
                "validationMessage": {
                    wrongNumber: "Value of max must be greater than minimum!"
                }
            },
            {
                "key":  "properties.domain",
                "add" : "Add",
                "htmlClass": "domainAddBtn",
                "style": {
                    "add": "btn-success"
                },
                "fieldHtmlClass": "domainField",
                "items":[
                    {
                        "key": "properties.domain[]",
                        "notitle" : true,
                        "disableSuccessState": true,
                        "feedback": false
                    }
                ],

                "condition": "model.properties.source == 'domain'"
            },
            {
                "key": "properties.distribution",
                "disableSuccessState": true,
                "feedback": false,
                "condition": "model.properties.source == 'random'"
            }

        ]
    },
    {type: "float", label: "Float",
        schema: {
            type: "object",
            properties: {
                type: {
                    title: "Type",
                    type: "string"
                },
                properties: {
                    type: "object",
                    properties:{
                        source: {
                            title: "Source",
                            type: "string",
                            enum: ["random", "domain"],
                            default: "random"
                        },
                        min: {
                            title: "Minimum",
                            type: "number",
                            default: 0
                        },
                        max: {
                            title: "Maximum",
                            type: "number",
                            default: 100
                        },
                        domain:{
                            title: "Domain",
                            type: "array",
                            minItems: 1,
                            items: {
                                type: "number"
                            }
                        },
                        distribution: {
                            title: "Distribution",
                            type: "string",
                            enum: ["uniform", "normal"],
                            default: "uniform"
                        }
                    }
                }
            }
        },
        form: [
            {
                "key": "type",
                "readonly": "true",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                "key": "properties.source",
                "disableSuccessState": true,
                "feedback": false
            },

            {
                "key":  "properties.min",
                "condition": "model.properties.source == 'random'",
                "disableSuccessState": true,
                "feedback": false,
                "validationMessage": {
                    wrongNumber: "Value of min must be less than maximum!"
                }
            },
            {
                "key":  "properties.max",
                "condition": "model.properties.source == 'random'",
                "disableSuccessState": true,
                "feedback": false,
                "validationMessage": {
                    wrongNumber: "Value of max must be greater than minimum!"
                }
            },
            {
                "key":  "properties.domain",
                "add" : "Add",
                "htmlClass": "domainAddBtn",
                "style": {
                    "add": "btn-success"
                },
                "fieldHtmlClass": "domainField",
                "items":[
                    {
                        "key": "properties.domain[]",
                        "notitle" : true,
                        "disableSuccessState": true,
                        "feedback": false
                    }
                ],

                "condition": "model.properties.source == 'domain'"
            },
            {
                "key": "properties.distribution",
                "disableSuccessState": true,
                "feedback": false,
                "condition": "model.properties.source == 'random'"
            }

        ]
    },

    {type: "string", label: "String",
        schema: {
            type: "object",
            properties: {
                type: {
                    title: "Type",
                    type: "string"
                },

                properties: {
                    type: "object",
                    properties:{
                        source: {
                            title: "Source",
                            type: "string",
                            enum: ["random", "domain"],
                            default: "random"
                        },
                        min: {
                            title: "Minimum length",
                            type: "integer",
                            default: 1
                        },
                        max: {
                            title: "Maximum length",
                            type: "integer",
                            default: 10
                        },
                        domain:{
                            title: "Domain",
                            type: "array",
                            minItems: 1,
                            items: {
                                type: "string"
                            }
                        },
                        distribution: {
                            title: "Distribution",
                            type: "string",
                            enum: ["uniform", "normal"],
                            default: "uniform"
                        }
                    }
                }
            }
        },
        form: [
            {
                "key": "type",
                "readonly": "true",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                "key": "properties.source",
                "disableSuccessState": true,
                "feedback": false
            },

            {
                "key":  "properties.min",
                "condition": "model.properties.source == 'random'",
                "disableSuccessState": true,
                "feedback": false,
                "validationMessage": {
                    wrongNumber: "Value of min must be less than maximum!"
                }
            },
            {
                "key":  "properties.max",
                "condition": "model.properties.source == 'random'",
                "disableSuccessState": true,
                "feedback": false,
                "validationMessage": {
                    wrongNumber: "Value of max must be greater than minimum!"
                }
            },
            {
                "key":  "properties.domain",
                "add" : "Add",
                "htmlClass": "domainAddBtn",
                "style": {
                    "add": "btn-success"
                },
                "fieldHtmlClass": "domainField",
                "items":[
                    {
                        "key": "properties.domain[]",
                        "notitle" : true,
                        "disableSuccessState": true,
                        "feedback": false
                    }
                ],

                "condition": "model.properties.source == 'domain'"
            },
            {
                "key": "properties.distribution",
                "disableSuccessState": true,
                "feedback": false,
                "condition": "model.properties.source == 'random'"
            }

        ]
    },

    {type: "boolean", label: "Boolean", schema: {
        type: "object",
        properties: {
            type: {
                title: "Type",
                type: "string"
            },
            properties: {
                type: "object",
                properties: {
                    value: {
                        title: "Value",
                        type: "string",
                        enum: ["random", "true", "false"],
                        default: "random"
                    }
                }
            }
        }
    },
        form: [
            {
                "key": "type",
                "readonly": "true",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                "key": "properties.value",
                "type": "select",
                "disableSuccessState": true,
                "feedback": false,
                "titleMap": [
                    {
                        "value": "random",
                        "name": "random"
                    },
                    {
                        "value": "true",
                        "name": "true"
                    },
                    {
                        "value": "false",
                        "name": "false"
                    }
                ]
            }
        ]
    },

    {type: "datetime", label: "Datetime", schema: {
        type: "object",
        properties: {
            type: {
                title: "Type",
                type: "string"
            },

            properties: {
                type: "object",
                properties:{
                    source: {
                        title: "Source",
                        type: "string",
                        enum: ["random", "domain"],
                        default: "random"
                    },
                    minSource: {
                        title: "Minimum date & time",
                        type: "string",
                        enum: ["now", "custom"],
                        default: "now"
                    },
                    minDate: {
                        title: "Minimum date",
                        type: "string",
                        format: "date"
                    },
                    minTime: {
                        title: "Minimum time",
                        type: "string",
                        pattern: "^(([0-1][0-9])|([2][0-3])):([0-5][0-9]):([0-5][0-9])$"
                    },
                    maxSource: {
                        title: "Maximum date & time",
                        type: "string",
                        enum: ["now", "custom"],
                        default: "now"
                    },
                    maxDate: {
                        title: "Maximum date",
                        type: "string",
                        format: "date"
                    },
                    maxTime: {
                        title: "Maximum time",
                        type: "string",
                        pattern: "^(([0-1][0-9])|([2][0-3])):([0-5][0-9]):([0-5][0-9])$"
                    },
                    domain:{
                        title: "Domain",
                        type: "array",
                        minItems: 1,
                        items: {
                            type: "object",
                            properties: {
                                date: {
                                    type: "string",
                                    format: "date"
                                },
                                time: {
                                    type: "string",
                                    pattern: "^(([0-1][0-9])|([2][0-3])):([0-5][0-9]):([0-5][0-9])$"
                                }
                            }
                        }
                    },
                    format: {
                        title: "Format",
                        type: "string",
                        enum: ["%Y", "%Y-%m", "%Y-%m-%d", "%H:%M", "%H:%M:%S", "%Y-%m-%dT%H:%M:%SZ"],
                        default: "%Y-%m-%dT%H:%M:%SZ"
                    },
                    distribution: {
                        title: "Distribution",
                        type: "string",
                        enum: ["uniform", "normal"],
                        default: "uniform"
                    }
                }
            }
        }
    },
        form: [
            {
                "key": "type",
                "readonly": "true",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                "key": "properties.source",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                "key": "properties.minSource",
                "disableSuccessState": true,
                "feedback": false,
                "condition": "model.properties.source == 'random'"
            },
            {
                "key":  "properties.minDate",
                "condition": "model.properties.source == 'random' && model.properties.minSource == 'custom'",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                "key":  "properties.minTime",
                "condition": "model.properties.source == 'random' && model.properties.minSource == 'custom'",
                "disableSuccessState": true,
                "feedback": false,
                "validationMessage": {
                    "202": "Value must be valid time in format hh:mm:ss"
                }
            },
            {
                "key": "properties.maxSource",
                "disableSuccessState": true,
                "feedback": false,
                "condition": "model.properties.source == 'random'"
            },
            {
                "key":  "properties.maxDate",
                "condition": "model.properties.source == 'random' && model.properties.maxSource == 'custom'",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                "key":  "properties.maxTime",
                "condition": "model.properties.source == 'random' && model.properties.maxSource == 'custom'",
                "disableSuccessState": true,
                "feedback": false,
                "validationMessage": {
                    "202": "Value must be valid time in format hh:mm:ss"
                }
            },
            {
                "key":  "properties.domain",
                "add" : "Add",
                "htmlClass": "domainAddBtn",
                "style": {
                    "add": "btn-success"
                },
                "fieldHtmlClass": "dateTimeField",
                "items":[
                    {
                        "key": "properties.domain[].date",
                        "title" : "Date",
                        "disableSuccessState": true,
                        "feedback": false
                    },
                    {
                        "key": "properties.domain[].time",
                        "title" : "Time",
                        "disableSuccessState": true,
                        "feedback": false
                    }
                ],

                "condition": "model.properties.source == 'domain'"
            },
            {
                "key": "properties.format",
                "disableSuccessState": true,
                "feedback": false,
                "titleMap": [
                    {
                        "value": "%Y",
                        "name": "YYYY"
                    },
                    {
                        "value": "%Y-%m",
                        "name": "YYYY-MM"
                    },
                    {
                        "value": "%Y-%m-%d",
                        "name": "YYYY-MM-DD"
                    },
                    {
                        "value": "%H:%M",
                        "name": "hh:mm"
                    },
                    {
                        "value": "%H:%M:%S",
                        "name": "hh:mm:ss"
                    },
                    {
                        "value": "%Y-%m-%dT%H:%M:%SZ",
                        "name": "YYYY-MM-DDThh:mm:ssZ"
                    }
                ]
            },
            {
                "key": "properties.distribution",
                "disableSuccessState": true,
                "feedback": false,
                "condition": "model.properties.source == 'random'"
            }

        ]
    }
])
.constant("CsvEditorRootType",
{
    type: "ROOT",
    label: "Row",
    schema: {
        type: "object",
        properties: {
            type: {
                title: "Type",
                type: "string"
            },
            min: {
                title: "Minimum",
                type: "integer",
                default: 0
            },
            max: {
                title: "Maximum",
                type: "integer",
                default: 100
            },
            separatorType: {
                title: "Separator type",
                type: "string",
                enum: [
                    ";",
                    "\t"
                ],
                default: ";"

            },
            separator: {
                title: "Separator",
                type: "string",
                default: ";"
            }
        }
    },
    form: [
        {
            "key": "type",
            "readonly": "true",
            "disableSuccessState": true,
            "feedback": false
        },
        {
            "key":  "min",
            "disableSuccessState": true,
            "feedback": false,
            "validationMessage": {
                wrongNumber: "Value of min must be less than maximum!"
            }
        },
        {
            "key":  "max",
            "disableSuccessState": true,
            "feedback": false,
            "validationMessage": {
                wrongNumber: "Value of max must be greater than minimum!"
            }
        },
        {
            "key": "separatorType",
            "disableSuccessState": true,
            "feedback": false,
            "titleMap": [
                {
                    "value": ";",
                    "name": "String"
                },
                {
                    "value": "\t",
                    "name": "White Character"
                }
            ],
            //Hack to set default values
            copyValueTo: ["separator"]
        },
        {
            "key":  "separator",
            type: "select",
            "titleMap": [
                {
                    "value": "\t",
                    "name": "Tab"
                },
                {
                    "value": " ",
                    "name": "Space"
                }
            ],
            "condition": "model.separatorType == '\t'",
            "disableSuccessState": true
        },
        {
            "key":  "separator",
            "type": "string",
            "condition": "model.separatorType == ';'",
            $validators: {
                emptyName: function(value) {
                    if(value !== undefined)
                        if (value.length <= 0 ) {
                            return false;
                        }
                    return true
                },
                multiCharacter: function (value) {
                    if(value !== undefined)
                        if (value.length > 1 ) {
                            return false;
                        }
                    return true
                }
            },
            "validationMessage": {
                emptyName: "Separator is empty string",
                multiCharacter: "Separator has to be single character"
            },
            feedback: false,
            "disableSuccessState": true
        }
    ]
})
.constant("CsvEditorDefaultType",
{
    schema: {
        type: "object",
        properties: {
            type: {
                title: "Type",
                type: "string"
            }
        }
    },
    form: [
        {
            "key": "type",
            "readonly": "true",
            "disableSuccessState": true,
            "feedback": false
        }
    ]
});

