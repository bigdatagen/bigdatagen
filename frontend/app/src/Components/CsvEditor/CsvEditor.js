angular.module('BDGApp.CsvEditor')

/**
 * Directive which supports the communication between parent scope, typeBox and editor content
 */
.directive('csvEditor', [ "CsvEditorTypes",  'CsvEditorRootType', 'typeSvc',
    function (CsvEditorTypes, CsvEditorRootType, typeSvc) {
        "use strict";
        return {
            restrict: 'EA',
            replace: false,
            templateUrl: "src/Components/CsvEditor/CsvEditor.tpl.html",
            scope: {
                input: "="
            },
            controller: ['$scope', function ($scope) {
                $scope.CsvEditorTypes = CsvEditorTypes;
                $scope.EditorRootType = CsvEditorRootType.type;
                $scope.showCustomTypes = false;

                //Adding root type to typeSvc configuration
                var typeSvcTypes = angular.copy(CsvEditorTypes);
                typeSvcTypes.push(CsvEditorRootType);

                typeSvc.init(typeSvcTypes);
            }],
            link: function ($scope) {
                var notInitialized = true;

                function setRootTypeActive() {
                    angular.forEach($scope.input, function (value) {
                        if (value.typeName === CsvEditorRootType.type) {
                            $scope.activeType = value;
                        }
                    });
                }

                /**
                 * Sets root type active on initialization
                 */
                $scope.$watch("input", function (newValue) {
                    if (notInitialized && newValue !== undefined) {
                        setRootTypeActive();
                        notInitialized = false;
                    }
                }, true);

                /**
                 * This watch is called when object reference is changed - this is when new configuration is uploaded
                 */
                $scope.$watch("input", function (newValue) {
                    if (newValue !== undefined) {
                        setRootTypeActive();
                    }
                });
            }
        };
    }
]);