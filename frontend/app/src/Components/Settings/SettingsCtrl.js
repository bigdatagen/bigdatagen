'use strict';
/**
 * Definition of a SettingCtrl controller. This is the controller for a Settings page and holds all the data about
 * settings of generation job. As a child state of HomeCtrl it has access to application workflow data through
 * $state. Contains definition, data and help texts from form that is the only part of the Settings page.
 * The form is created by "Schema Form" framework.
 */
angular.module('BDGApp.Settings',['ngSanitize'])
    .controller('SettingsCtrl',['$scope', '$state','$sce', function($scope, $state, $sce){
        /*
        When the Settings page is first visited it does not contain settings data
        and therefore default data are set.
         */
        if($state.current.data.config.settings == undefined){
            $state.current.data.config.settings = {
                amount: {
                    amount: 100,
                    amountUnit: "files"
                },
                speed: {
                    speedAmount: 1000,
                    speedAmountUnit: "files",
                    speedTimeAmount: 1,
                    speedTimeUnit: "second"
                },
                clusterSize : 0,
                filesPerTask : 0,
                filesPerDirectory : 10000,
                checkpointPeriod: 10,
                preserveOrder : true,
                cleanUp : false,
                continuousCleanUp: false,
                useStorerPipelineOptimization: true,
                useProgressiveStorersCount: false,
                requirePipelineTasksOnSameNode: false

            };
        }
        //definition of units for Setting page form
        var amountUnit = [
            "files",
            "bytes",
            "kibibytes",
            "mebibytes",
            "gibibytes",
            "tebibytes",
            "seconds",
            "minutes",
            "hours",
            "days",
            "weeks",
            "months"
        ];

        var timeUnits = [
            "second",
            "minute",
            "hour",
            "day",
            "week",
            "month",
            "year"
        ];

        //definition of model for settings form
        $scope.model = $state.current.data.config.settings;

        //definition of form for settings form
        $scope.form = [
            {
                "type": "section",
                "htmlClass": "row",
                "items": [
                    {
                        "type": "section",
                        "htmlClass": "col-xs-6",
                        "items": [
                            {
                                "key":"amount.amount",
                                "disableSuccessState": true,
                                "feedback": false
                            }
                        ]
                    },
                    {
                        "type": "section",
                        "htmlClass": "col-xs-6",
                        "items": [
                            {
                                "key": "amount.amountUnit",
                                "labelHtmlClass": "noTitleLabel",
                                "type": "select",
                                "disableSuccessState": true,
                                "feedback": false,
                                "titleMap": [
                                    {
                                        "value": amountUnit[0],
                                        "name": amountUnit[0]
                                    },
                                    {
                                        "value": amountUnit[1],
                                        "name": amountUnit[1]
                                    },
                                    {
                                        "value": amountUnit[2],
                                        "name": "kB"
                                    },
                                    {
                                        "value": amountUnit[3],
                                        "name": "MB"
                                    },
                                    {
                                        "value": amountUnit[4],
                                        "name": "GB"
                                    },
                                    {
                                        "value": amountUnit[5],
                                        "name": "TB"
                                    },
                                    {
                                        "value": amountUnit[6],
                                        "name": amountUnit[6]
                                    },
                                    {
                                        "value": amountUnit[7],
                                        "name": amountUnit[7]
                                    },
                                    {
                                        "value": amountUnit[8],
                                        "name": amountUnit[8]
                                    },
                                    {
                                        "value": amountUnit[9],
                                        "name": amountUnit[9]
                                    },
                                    {
                                        "value": amountUnit[10],
                                        "name": amountUnit[10]
                                    },
                                    {
                                        "value": amountUnit[11],
                                        "name": amountUnit[11]
                                    }

                                ]

                            }

                        ]
                    }
                ]
            },
            {
                "type": "section",
                "htmlClass": "row",
                "items": [
                    {
                        "key": "speedLimiter",
                        "type": "checkbox",
                        "disableSuccessState": true,
                        "feedback": false,
                        "htmlClass": "speedLimiter"
                    }
                ]
            },
            {
                "type": "section",
                "htmlClass": "row",
                "items": [
                    {
                        "type": "section",
                        "htmlClass": "row leftLimiter",
                        "items": [
                            {
                                "type": "section",
                                "htmlClass": "col-xs-6",
                                "items": [
                                    {
                                        "key": "speed.speedAmount",
                                        "notitle": true,
                                        "disableSuccessState": true,
                                        "feedback": false,
                                        "condition": "model.speedLimiter"
                                    }
                                ]
                            },
                            {
                                "type": "section",
                                "htmlClass": "col-xs-6",
                                "items": [
                                    {
                                        "key": "speed.speedAmountUnit",
                                        "notitle": true,
                                        "type": "select",
                                        "disableSuccessState": true,
                                        "feedback": false,
                                        "condition": "model.speedLimiter",
                                        "titleMap": [
                                            {
                                                "value": amountUnit[0],
                                                "name": amountUnit[0]
                                            },
                                            {
                                                "value": amountUnit[1],
                                                "name": amountUnit[1]
                                            },
                                            {
                                                "value": amountUnit[2],
                                                "name": "kB"
                                            },
                                            {
                                                "value": amountUnit[3],
                                                "name": "MB"
                                            },
                                            {
                                                "value": amountUnit[4],
                                                "name": "GB"
                                            },
                                            {
                                                "value": amountUnit[5],
                                                "name": "TB"
                                            }
                                        ]
                                    }

                                ]
                            }
                        ]
                    },
                    {
                        "type": "section",
                        "items": [
                            {
                                "type": "template",
                                "template": '<div style="font-size: 30px; float: left; margin-top: 5px;">/</div>',
                                "condition": "model.speedLimiter"
                            }
                        ]
                    },
                    {
                        "type": "section",
                        "htmlClass": "row rightLimiter",
                        "items": [
                            {
                                "type": "section",
                                "htmlClass": "col-xs-6",
                                "items": [
                                    {
                                        "key": "speed.speedTimeAmount",
                                        "notitle": true,
                                        "disableSuccessState": true,
                                        "feedback": false,
                                        "condition": "model.speedLimiter"
                                    }
                                ]
                            },
                            {
                                "type": "section",
                                "htmlClass": "col-xs-6",
                                "items": [
                                    {
                                        "key": "speed.speedTimeUnit",
                                        "notitle": true,
                                        "type": "select",
                                        "disableSuccessState": true,
                                        "feedback": false,
                                        "condition": "model.speedLimiter"
                                    }

                                ]
                            }
                        ]
                    }
                ]
            },
            {
                "type": "section",
                "htmlClass": "row",
                "items": [
                    {
                        "type": "section",
                        "htmlClass": "col-xs-6",
                        "items": [
                            {
                                "key": "clusterSize",
                                "disableSuccessState": true,
                                "feedback": false
                            },
                            {
                                "key": "preserveOrder",
                                "type": "checkbox",
                                "disableSuccessState": true,
                                "feedback": false
                            }
                        ]
                    },
                    {
                        "type": "section",
                        "htmlClass": "col-xs-6",
                        "items": [
                            {
                                "key": "filesPerTask",
                                "disableSuccessState": true,
                                "feedback": false
                            },
                            {
                                "key": "cleanUp",
                                "type": "checkbox",
                                "disableSuccessState": true,
                                "feedback": false
                            }
                        ]
                    }
                ]
            },
            {
                "type": "section",
                "htmlClass": "row",
                "items": [
                    {
                        "key": "showAdvanced",
                        "type": "checkbox",
                        "disableSuccessState": true,
                        "feedback": false,
                        "htmlClass": "showAdvanced"
                    }
                ]
            },
            {
                "type": "section",
                "htmlClass": "row",
                "items": [
                    {
                        "type": "section",
                        "htmlClass": "col-xs-6",
                        "items": [
                            {
                                "key": "filesPerDirectory",
                                "disableSuccessState": true,
                                "feedback": false,
                                "condition": "model.showAdvanced"
                            },
                            {
                                "key": "continuousCleanUp",
                                "type": "checkbox",
                                "disableSuccessState": true,
                                "feedback": false,
                                "condition": "model.showAdvanced"
                            },
                            {
                                "key": "useStorerPipelineOptimization",
                                "type": "checkbox",
                                "disableSuccessState": true,
                                "feedback": false,
                                "condition": "model.showAdvanced"
                            }
                        ]
                    },
                    {
                        "type": "section",
                        "htmlClass": "col-xs-6",
                        "items": [
                            {
                                "key": "checkpointPeriod",
                                "disableSuccessState": true,
                                "feedback": false,
                                "condition": "model.showAdvanced"
                            },
                            {
                                "key": "useProgressiveStorersCount",
                                "type": "checkbox",
                                "disableSuccessState": true,
                                "feedback": false,
                                "condition": "model.showAdvanced"
                            },
                            {
                                "key": "requirePipelineTasksOnSameNode",
                                "type": "checkbox",
                                "disableSuccessState": true,
                                "feedback": false,
                                "condition": "model.showAdvanced"
                            }
                        ]
                    }
                ]
            }
        ];

        //definition of schema for settings form
        $scope.schema = {
            type: "object",
            properties: {
                showAdvanced: {
                    title: "Show advanced settings",
                    type: "boolean",
                    default: false
                },
                speedLimiter: {
                    title: "Generation speed limiter",
                    type: "boolean",
                    default: false
                },
                amount: {
                    type:"object",
                    
                    properties: {
                        amount: {
                            title: "Job target",
                            type : "number",
                            default: 10000,
                            minimum: 1
                        },
                        amountUnit: {
                            title: " ",
                            type: "string",
                            enum : amountUnit,
                            default: "files"
                        }
                    }
                },
                speed: {
                    type: "object",
                    properties: {
                        speedAmount: {
                            title: " ",
                            type: "number",
                            default: 1,
                            minimum: 1
                        },
                        speedAmountUnit: {
                            title: " ",
                            type: "string",
                            enum: amountUnit,
                            default: "files"
                        },
                        speedTimeAmount: {
                            title: " ",
                            minimum: 1,
                            type: "number",
                            default: 1
                        },
                        speedTimeUnit:{
                            title: " ",
                            type: "string",
                            enum: timeUnits,
                            default: "second"
                        }
                    }
                },
                clusterSize:{
                    title: "Size of cluster",
                    type: "number",
                    default: 0,
                    minimum: 0,
                    maximum: 100000
                },
                filesPerTask: {
                    title: "Number of files per task",
                    type: "number",
                    default: 0,
                    minimum: 0,
                    maximum: 10000000
                },
                filesPerDirectory: {
                    title: "Number of files per directory",
                    type: "number",
                    default: 10000,
                    minimum: 42,
                    maximum: 10000000
                },
                checkpointPeriod: {
                    title: "Time frequency of checkpoints",
                    type: "number",
                    default: 10,
                    minimum: 0,
                    maximum: 86400
                },
                preserveOrder: {
                    title: "Preserve order",
                    type: "boolean",
                    default: true
                },
                cleanUp: {
                    title: "Clean up",
                    type: "boolean",
                    default: false
                },
                continuousCleanUp: {
                    title: "Continuous clean up ",
                    type: "boolean",
                    default: false
                },
                useStorerPipelineOptimization: {
                    title: "Use storer pipeline optimization",
                    type: "boolean",
                    default: true
                },
                useProgressiveStorersCount: {
                    title: "Use progressive storers count ",
                    type: "boolean",
                    default: false
                },
                requirePipelineTasksOnSameNode: {
                    title: "Require pipeline tasks on same node ",
                    type: "boolean",
                    default: false
                }
            }
        }

        //template for help popover
        $scope.helpPopover = {
            templateUrl : "src/Components/Settings/help.tpl.html",
            isOpen : false
        };

        //text showed in the help popup window
        $scope.helpText = "";

        //definition of form for select box in help popup window
        $scope.helpForm = [
            {
                "key": "setting",
                "title": "Select setting",
                "type": "select",
                "disableSuccessState": true,
                "feedback": false,
                "titleMap": [
                    {
                        "value": 0,
                        "name": "Job Target"
                    },
                    {
                        "value": 1,
                        "name": "Generation speed limiter"
                    },
                    {
                        "value": 2,
                        "name": "Size of cluster"
                    },
                    {
                        "value": 3,
                        "name": "Number of files per task"
                    },
                    {
                        "value": 4,
                        "name": "Preserve order"
                    },
                    {
                        "value": 5,
                        "name": "Clean up"
                    },
                    {
                        "value": 6,
                        "name": "Number of files per directory"
                    },
                    {
                        "value": 7,
                        "name": "Time frequency of checkpoints"
                    },
                    {
                        "value": 8,
                        "name": "Use storer pipeline optimization"
                    },
                    {
                        "value": 9,
                        "name": "Continuous clean up"
                    },
                    {
                        "value": 10,
                        "name": "Use progressive storers count"
                    },
                    {
                        "value": 11,
                        "name": "Require pipeline tasks on same node"
                    }
                ]
            }
        ];

        //definition of schema for select box in help popup window
        $scope.helpSchema = {
            type: "object",
            properties: {
                setting: {
                    type: "number",
                    enum: [0,1,2,3,4,5,6,7,8,9,10,11]
                }
            }
        };

        //definition of model for select box in help popup window
        $scope.helpModel = {};

        //definition of help texts showed help popup window
        $scope.helpTexts = [
            '<div>Determines how much data will be generated in the job, i.e. in the last stage of the pipeline. ' +
            'It has three different modes, it is possible to set:<br><br> <ul>' +
            '<li>total number of generated files, e.g. "100 files",</li>' +
            '<li>total amount of generated data, e.g. "1 GB",</li>' +
            '<li>total length of the job, e.g. "1 hour".</li></ul></div>',
            '<div>Sets generation speed; when this setting is omitted, a maximum possible speed is used. This  ' +
            'setting is only valid when some storer plugin is present in the pipeline, because only those plugins are ' +
            'able to limit the generation speed. The resulting speed will probably not be precisely the one set, but ' +
            'it should be close over the longer period of time.</div>',
            'Determines total number of threads processing the job. When value "0" is set, maximum possible number of ' +
            'threads is used. If this does not work, set the number by hand. For example when this setting is set to 32, ' +
            'then master and 31 workers processing tasks are launched.</div>',
            '<div>Determines how many files are going to be processed in each task. When this setting is set to 0, ' +
            'task scheduler tries to determine its value dynamically. When generating larger files (tens of MB per file), ' +
            'we recommend to set this setting to “1”, for intermediate files (up to MB), we recommend a value of "10" and ' +
            'for small files, we recommend value of "100".</div>',
            '<div>When turned on, order of generated files is preserved between all the stages of the pipeline. This is ' +
            'useful when you want to send files to the external storage, e.g. cluster, in the same order as they were ' +
            'generated, for example because sequences are used in those files, so it depends on their order.<br><br>' +
            'This setting should be always set to true, when sequences are used in a schema. Then it is guaranteed ' +
            'that no files are skipped (unless the task where they are processed ends in error state) in the last ' +
            'stage of the pipeline. When no sequences are used, it is possible to turn this setting off in order to ' +
            'gain a better performance.</div>',
            '<div>When turned on, all the generated temporary files will be deleted when a job is finished. I.e. all the ' +
            'files from all the stages of the pipeline except the last one.</div>',
            '<div>Determines how many files can be stored in one directory in the file system. It applies to all the ' +
            'stages of the pipeline, but it applies only when the property “outputPath” of a plugin in the pipeline ' +
            'contains placeholder “%d”. E.g. when this setting is set to “100”, files with ids 1-100 will be stored in the ' +
            'directory with id 1, files 101-200 will be stored in the directory with id 2, etc.</div>',
            '<div>This value is in seconds. It determines how often should scheduler store checkpoints about ' +
            'progress, i.e. when the job crashes and it is re-launched, how much time of progress might get wasted. ' +
            'Default and recommended value is 10, i.e. every 10 seconds.</div>',
            '<div>This setting turns on and off the automatic optimization of pipeline which includes removing of FileStorer ' +
            'plugin from the pipeline on certain circumstances which are:<br><br>' +
            '<ul><li>There is no speed limit set.</li>' +
            '<li>FileStorer plugin stores files only to single location – it is not configured to store to multiple locations.</li>' +
            '<li>There are at least two workers able to run FileStorer plugin.</li></ul>' +
            'Then a plugin which is configured to run before FileStorer plugin is reconfigured to store files to the location ' +
            'where FileStorer plugin should and FileStorer can be removed from the pipeline, because the result of a job is ' +
            'the same as if it would stay there.</div>',
            '<div>When turned on, continuous clean up algorithm is used. It automatically removes processed files from ' +
            'all the stages of the pipeline except the last one, but in contrast with "Clean up" setting, it does it ' +
            'while the job is running. When a task for transformer or storer is successfully processed, all the files ' +
            'processed in that task are deleted. This setting might cause some problems in relation with ' +
            'error recovery and checkpoints.</div>',
            '<div>When turned on, the algorithm for automatic scaling of simultaneously running storer tasks ' +
            'is used. It is used only with combination with speed limitation. It starts with one only one task ' +
            'in maximum and computes statistics about final speed when only this one task is set to run.<br><br>If the generation speed is 80% or less of demanded speed then it increases the number of used storer tasks and computes statistics again. After a certain amount of time it should find the number of simultaneously running storer tasks which stores files at least with 80% of demanded speed or at least it gives the best possible performance when demanded speed cannot be reached for some reason.</div>',
            '<div>When turned on, the scheduler guarantees that a following task always runs at least on the same node as the previous one. We recommend to turn this setting on when files from some stage of the pipeline are stored in the local file system. If files are only stored in the shared file system, it can be left off.</div>'
        ];

        /*
        When value of select box in help popup changes show help text according to the selected setting
         */
        $scope.$watch('helpModel.setting',function (newVal) {
            if(newVal != undefined){
                $scope.helpText = $sce.trustAsHtml($scope.helpTexts[newVal]);
            }
        })
    }]);