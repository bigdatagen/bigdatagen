/**
 * Definition of directive for PropertyEditor.tpl.html template . This directive receives model, schema and form of
 * selected item in editor from parent scope and populates property form.
 */
angular.module('BDGApp.PropertyEditor', ['schemaForm'])

.directive('propertyEditor', [ '$timeout',
    function ($timeout) {
        "use strict";
        return {
            restrict: 'EA',
            replace: false,
            templateUrl: "src/Components/PropertyEditor/PropertyEditor.tpl.html",
            scope: {
                model: "=",
                schema: "=",
                form: "="
            },
            link: function ($scope, element) {
                /*
                 Update of model values when source is changed
                 */
                var typeWatch;
                $scope.$watch("model.type", function (newValue) {
                    if (typeWatch !== undefined) {
                        typeWatch();
                    }
                    switch (newValue) {
                        case "int":
                        case "float":
                            typeWatch = $scope.$watch("model.properties.source", function (newSource) {
                                if ($scope.model.properties !== undefined ){
                                    switch (newSource){
                                        case "domain":
                                            delete $scope.model.properties.min;
                                            delete $scope.model.properties.max;
                                            break;
                                        case "random":
                                            delete $scope.model.properties.domain;
                                            break;
                                    }
                                }
                            });
                            break;
                        case "string":
                            typeWatch = $scope.$watch("model.properties.source", function (newSource) {
                                if ($scope.model.properties !== undefined ){
                                    switch (newSource){
                                        case "domain":
                                            delete $scope.model.properties.min;
                                            delete $scope.model.properties.max;
                                            break;
                                        case "random":
                                            delete $scope.model.properties.domain;
                                            break;
                                    }
                                }
                            });
                            break;
                    }
                });

                /*
                Setting focus on first input -> this leads to better user experience
                 */
                $scope.$watch('model', function () {
                    $timeout(function () {
                        element.find("input").first().focus();
                    });
                });
            }
        };
    }
]);