angular.module('BDGApp.JsonEditor')

//TODO: define type's schema
.constant("JsonEditorTypes", [
    {type: "int", label: "Integer",
        schema: {
            type: "object",
            properties: {
                propertyName: {
                    title: "Name",
                    type: "string",
                    default: ""
                },

                type: {
                    title: "Type",
                    type: "string"
                },

                properties: {
                    type: "object",
                    properties:{
                        source: {
                            title: "Source",
                            type: "string",
                            enum: ["random", "domain"],
                            default: "random"
                        },
                        min: {
                            title: "Minimum",
                            type: "integer",
                            default: 0
                        },
                        max: {
                            title: "Maximum",
                            type: "integer",
                            default: 100
                        },
                        domain:{
                            title: "Domain",
                            type: "array",
                            minItems: 1,
                            items: {
                                type: "integer"
                            }
                        },
                        distribution: {
                            title: "Distribution",
                            type: "string",
                            enum: ["uniform", "normal"],
                            default: "uniform"
                        }
                    }
                }
            }
        },
        form: [
            {
                "key": "propertyName",
                "disableSuccessState": true,
                "feedback": false,
                $validators: {
                    wrongName: function(value) {
                        if (value.length <= 0 ) {
                            return false;
                        }
                        return true
                    }
                },
                "validationMessage": {
                    wrongName: "Name is too short!"
                }
            },
            {
                "key": "type",
                "readonly": "true",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                "key": "properties.source",
                "disableSuccessState": true,
                "feedback": false
            },

            {
                "key":  "properties.min",
                "condition": "model.properties.source == 'random'",
                "disableSuccessState": true,
                "feedback": false,
                "validationMessage": {
                    wrongNumber: "Value of min must be less than maximum!"
                }
            },
            {
                "key":  "properties.max",
                "condition": "model.properties.source == 'random'",
                "disableSuccessState": true,
                "feedback": false,
                "validationMessage": {
                    wrongNumber: "Value of max must be greater than minimum!"
                }
            },
            {
                "key":  "properties.domain",
                "add" : "Add",
                "htmlClass": "domainAddBtn",
                "style": {
                    "add": "btn-success"
                },
                "fieldHtmlClass": "domainField",
                "items":[
                    {
                        "key": "properties.domain[]",
                        "notitle" : true,
                        "disableSuccessState": true,
                        "feedback": false
                    }
                ],

                "condition": "model.properties.source == 'domain'"
            },
            {
                "key": "properties.distribution",
                "disableSuccessState": true,
                "feedback": false,
                "condition": "model.properties.source == 'random'"
            }

        ]
    },
    {type: "float", label: "Float",
        schema: {
            type: "object",
            properties: {
                propertyName: {
                    title: "Name",
                    type: "string",
                    default : ""
                },

                type: {
                    title: "Type",
                    type: "string"
                },
                properties: {
                    type: "object",
                    properties:{
                        source: {
                            title: "Source",
                            type: "string",
                            enum: ["random", "domain"],
                            default: "random"
                        },
                        min: {
                            title: "Minimum",
                            type: "number",
                            default: 0
                        },
                        max: {
                            title: "Maximum",
                            type: "number",
                            default: 100
                        },
                        domain:{
                            title: "Domain",
                            type: "array",
                            minItems: 1,
                            items: {
                                type: "number"
                            }
                        },
                        distribution: {
                            title: "Distribution",
                            type: "string",
                            enum: ["uniform", "normal"],
                            default: "uniform"
                        }
                    }
                }
            }
        },
        form: [
            {
                "key": "propertyName",
                "disableSuccessState": true,
                "feedback": false,
                $validators: {
                    wrongName: function(value) {
                        if(value !== undefined)
                            if (value.length <= 0 ) {
                                return false;
                            }
                        return true
                    }
                },
                "validationMessage": {
                    wrongName: "Name is too short!"
                }
            },
            {
                "key": "type",
                "readonly": "true",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                "key": "properties.source",
                "disableSuccessState": true,
                "feedback": false
            },

            {
                "key":  "properties.min",
                "condition": "model.properties.source == 'random'",
                "disableSuccessState": true,
                "feedback": false,
                "validationMessage": {
                    wrongNumber: "Value of min must be less than maximum!"
                }
            },
            {
                "key":  "properties.max",
                "condition": "model.properties.source == 'random'",
                "disableSuccessState": true,
                "feedback": false,
                "validationMessage": {
                    wrongNumber: "Value of max must be greater than minimum!"
                }
            },
            {
                "key":  "properties.domain",
                "add" : "Add",
                "htmlClass": "domainAddBtn",
                "style": {
                    "add": "btn-success"
                },
                "fieldHtmlClass": "domainField",
                "items":[
                    {
                        "key": "properties.domain[]",
                        "notitle" : true,
                        "disableSuccessState": true,
                        "feedback": false
                    }
                ],

                "condition": "model.properties.source == 'domain'"
            },
            {
                "key": "properties.distribution",
                "disableSuccessState": true,
                "feedback": false,
                "condition": "model.properties.source == 'random'"
            }

        ]
    },

    {type: "string", label: "String",
        schema: {
            type: "object",
            properties: {
                propertyName: {
                    title: "Name",
                    type: "string",
                    default : ""
                },

                type: {
                    title: "Type",
                    type: "string"
                },

                properties: {
                    type: "object",
                    properties:{
                        source: {
                            title: "Source",
                            type: "string",
                            enum: ["random", "domain"],
                            default: "random"
                        },
                        min: {
                            title: "Minimum length",
                            type: "integer",
                            default: 1
                        },
                        max: {
                            title: "Maximum length",
                            type: "integer",
                            default: 10
                        },
                        domain:{
                            title: "Domain",
                            type: "array",
                            minItems: 1,
                            items: {
                                type: "string"
                            }
                        },
                        distribution: {
                            title: "Distribution",
                            type: "string",
                            enum: ["uniform", "normal"],
                            default: "uniform"
                        }
                    }
                }
            }
        },
        form: [
            {
                "key": "propertyName",
                "disableSuccessState": true,
                "feedback": false,
                $validators: {
                    wrongName: function(value) {
                        if(value !== undefined)
                            if (value.length <= 0 ) {
                                return false;
                            }
                        return true
                    }
                },
                "validationMessage": {
                    wrongName: "Name is too short!"
                }
            },
            {
                "key": "type",
                "readonly": "true",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                "key": "properties.source",
                "disableSuccessState": true,
                "feedback": false
            },

            {
                "key":  "properties.min",
                "condition": "model.properties.source == 'random'",
                "disableSuccessState": true,
                "feedback": false,
                "validationMessage": {
                    wrongNumber: "Value of min must be less than maximum!"
                }
            },
            {
                "key":  "properties.max",
                "condition": "model.properties.source == 'random'",
                "disableSuccessState": true,
                "feedback": false,
                "validationMessage": {
                    wrongNumber: "Value of max must be greater than minimum!"
                }
            },
            {
                "key":  "properties.domain",
                "add" : "Add",
                "htmlClass": "domainAddBtn",
                "style": {
                    "add": "btn-success"
                },
                "fieldHtmlClass": "domainField",
                "items":[
                    {
                        "key": "properties.domain[]",
                        "notitle" : true,
                        "disableSuccessState": true,
                        "feedback": false
                    }
                ],

                "condition": "model.properties.source == 'domain'"
            },
            {
                "key": "properties.distribution",
                "disableSuccessState": true,
                "feedback": false,
                "condition": "model.properties.source == 'random'"
            }

        ]
    },

    {type: "boolean", label: "Boolean", schema: {
        type: "object",
        properties: {
            propertyName: {
                title: "Name",
                type: "string",
                default: ""
            },
            type: {
                title: "Type",
                type: "string"
            },
            properties: {
                type: "object",
                properties: {
                    value: {
                        title: "Value",
                        type: "string",
                        enum: ["random", "true", "false"],
                        default: "random"
                    }
                }
            }
        }
    },
        form: [
            {
                "key": "propertyName",
                "disableSuccessState": true,
                "feedback": false,
                $validators: {
                    wrongName: function(value) {
                        if(value !== undefined)
                            if (value.length <= 0 ) {
                                return false;
                            }
                        return true
                    }
                },
                "validationMessage": {
                    wrongName: "Name is too short!"
                }
            },
            {
                "key": "type",
                "readonly": "true",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                "key": "properties.value",
                "type": "select",
                "disableSuccessState": true,
                "feedback": false,
                "titleMap": [
                    {
                        "value": "random",
                        "name": "random"
                    },
                    {
                        "value": "true",
                        "name": "true"
                    },
                    {
                        "value": "false",
                        "name": "false"
                    }
                ]
            }
        ]
    },

    {type: "datetime", label: "Datetime", schema: {
        type: "object",
        properties: {
            propertyName: {
                title: "Name",
                type: "string",
                default: ""
            },

            type: {
                title: "Type",
                type: "string"
            },

            properties: {
                type: "object",
                properties:{
                    source: {
                        title: "Source",
                        type: "string",
                        enum: ["random", "domain"],
                        default: "random"
                    },
                    minSource: {
                        title: "Minimum date & time",
                        type: "string",
                        enum: ["now", "custom"],
                        default: "now"
                    },
                    minDate: {
                        title: "Minimum date",
                        type: "string",
                        format: "date"
                    },
                    minTime: {
                        title: "Minimum time",
                        type: "string",
                        pattern: "^(([0-1][0-9])|([2][0-3])):([0-5][0-9]):([0-5][0-9])$"
                    },
                    maxSource: {
                        title: "Maximum date & time",
                        type: "string",
                        enum: ["now", "custom"],
                        default: "now"
                    },
                    maxDate: {
                        title: "Maximum date",
                        type: "string",
                        format: "date"
                    },
                    maxTime: {
                        title: "Maximum time",
                        type: "string",
                        pattern: "^(([0-1][0-9])|([2][0-3])):([0-5][0-9]):([0-5][0-9])$"
                    },
                    domain:{
                        title: "Domain",
                        type: "array",
                        minItems: 1,
                        items: {
                            type: "object",
                            properties: {
                                date: {
                                    type: "string",
                                    format: "date"
                                },
                                time: {
                                    type: "string",
                                    pattern: "^(([0-1][0-9])|([2][0-3])):([0-5][0-9]):([0-5][0-9])$"
                                }
                            }
                        }
                    },
                    format: {
                        title: "Format",
                        type: "string",
                        enum: ["%Y", "%Y-%m", "%Y-%m-%d", "%H:%M", "%H:%M:%S", "%Y-%m-%dT%H:%M:%SZ"],
                        default: "%Y-%m-%dT%H:%M:%SZ"
                    },
                    distribution: {
                        title: "Distribution",
                        type: "string",
                        enum: ["uniform", "normal"],
                        default: "uniform"
                    }
                }
            }
        }
    },
        form: [
            {
                "key": "propertyName",
                "disableSuccessState": true,
                "feedback": false,
                $validators: {
                    wrongName: function(value) {
                        if(value !== undefined)
                            if (value.length <= 0 ) {
                                return false;
                            }
                        return true
                    }
                },
                "validationMessage": {
                    wrongName: "Name is too short!"
                }
            },
            {
                "key": "type",
                "readonly": "true",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                "key": "properties.source",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                "key": "properties.minSource",
                "disableSuccessState": true,
                "feedback": false,
                "condition": "model.properties.source == 'random'"
            },
            {
                "key":  "properties.minDate",
                "condition": "model.properties.source == 'random' && model.properties.minSource == 'custom'",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                "key":  "properties.minTime",
                "condition": "model.properties.source == 'random' && model.properties.minSource == 'custom'",
                "disableSuccessState": true,
                "feedback": false,
                "validationMessage": {
                    "202": "Value must be valid time in format hh:mm:ss"
                }
            },
            {
                "key": "properties.maxSource",
                "disableSuccessState": true,
                "feedback": false,
                "condition": "model.properties.source == 'random'"
            },
            {
                "key":  "properties.maxDate",
                "condition": "model.properties.source == 'random' && model.properties.maxSource == 'custom'",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                "key":  "properties.maxTime",
                "condition": "model.properties.source == 'random' && model.properties.maxSource == 'custom'",
                "disableSuccessState": true,
                "feedback": false,
                "validationMessage": {
                    "202": "Value must be valid time in format hh:mm:ss"
                }
            },
            {
                "key":  "properties.domain",
                "add" : "Add",
                "htmlClass": "domainAddBtn",
                "style": {
                    "add": "btn-success"
                },
                "fieldHtmlClass": "dateTimeField",
                "items":[
                    {
                        "key": "properties.domain[].date",
                        "title" : "Date",
                        "disableSuccessState": true,
                        "feedback": false
                    },
                    {
                        "key": "properties.domain[].time",
                        "title" : "Time",
                        "disableSuccessState": true,
                        "feedback": false,
                        "validationMessage": {
                            "202": "Value must be valid time in format hh:mm:ss"
                        }
                    }
                ],

                "condition": "model.properties.source == 'domain'"
            },
            {
                "key": "properties.format",
                "disableSuccessState": true,
                "feedback": false,
                "titleMap": [
                    {
                        "value": "%Y",
                        "name": "YYYY"
                    },
                    {
                        "value": "%Y-%m",
                        "name": "YYYY-MM"
                    },
                    {
                        "value": "%Y-%m-%d",
                        "name": "YYYY-MM-DD"
                    },
                    {
                        "value": "%H:%M",
                        "name": "hh:mm"
                    },
                    {
                        "value": "%H:%M:%S",
                        "name": "hh:mm:ss"
                    },
                    {
                        "value": "%Y-%m-%dT%H:%M:%SZ",
                        "name": "YYYY-MM-DDThh:mm:ssZ"
                    }
                ]
            },
            {
                "key": "properties.distribution",
                "disableSuccessState": true,
                "feedback": false,
                "condition": "model.properties.source == 'random'"
            }

        ]
    },
    {type: "array", label: "Array",
        schema: {
        type: "object",
        properties: {
            propertyName: {
                title: "Name",
                type: "string",
                default: ""
            },
            type: {
                title: "Type",
                type: "string"
            },
            properties: {
                type: "object",
                properties:{
                    min: {
                        title: "Minimum length",
                        type: "integer",
                        default: 1
                    },
                    max: {
                        title: "Maximum length",
                        type: "integer",
                        default: 10
                    },
                    distribution: {
                        title: "Distribution",
                        type: "string",
                        enum: ["uniform", "normal"],
                        default: "uniform"
                    }
                }
            }
        }
    },
        form: [
            {
                "key": "propertyName",
                "disableSuccessState": true,
                "feedback": false,
                $validators: {
                    wrongName: function(value) {
                        if(value !== undefined)
                            if (value.length <= 0 ) {
                                return false;
                            }
                        return true
                    }
                },
                "validationMessage": {
                    wrongName: "Name is too short!"
                }
            },
            {
                "key": "type",
                "readonly": "true",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                "key":  "properties.min",
                "disableSuccessState": true,
                "feedback": false,
                "validationMessage": {
                    wrongNumber: "Value of min must be less than maximum!"
                }
            },
            {
                "key":  "properties.max",
                "disableSuccessState": true,
                "feedback": false,
                "validationMessage": {
                    wrongNumber: "Value of max must be greater than minimum!"
                }
            },
            {
                "key": "properties.distribution",
                "disableSuccessState": true,
                "feedback": false
            }

        ]},
    {type: "probSet", label: "ProbabilitySet", schema: {test: "testProb"}, form: {test: "testProb"}},
    {type: "object", label: "Object",
        schema: {
        type: "object",
        properties: {
            propertyName: {
                title: "Name",
                type: "string",
                minLength: 1
            },

            type: {
                title: "Type",
                type: "string"
            }
        }
    },
        form: [
            {
                "key": "propertyName",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                "key": "type",
                "readonly": "true",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                "key": "properties.source",
                "disableSuccessState": true,
                "feedback": false
            }
        ]}
])
//TODO: put these to some common place
.constant("EditorRootType", "ROOT")
.constant("EditorArrayType", "array")
.constant("JsonEditorDefaultType",
{
    schema: {
        type: "object",
        properties: {
            propertyName: {
                title: "Name",
                type: "string",
                default: ""
            },

            type: {
                title: "Type",
                type: "string"
            }
        }
    },
    form: [
        {
            "key": "propertyName",
            "disableSuccessState": true,
            "feedback": false,
            $validators: {
                wrongName: function(value) {
                    if(value !== undefined)
                        if (value.length <= 0 ) {
                            return false;
                        }
                    return true
                }
            },
            "validationMessage": {
                wrongName: "Name is too short!"
            }
        },
        {
            "key": "type",
            "readonly": "true",
            "disableSuccessState": true,
            "feedback": false
        }
    ]
})
.constant("SequenceType",{
    int: {
        schema: {
            type: "object",
            properties: {
                type: {
                    title: "Type",
                    type: "string"
                },
                innerType: {
                    title: "Base Type",
                    type: "string"
                },
                properties:{
                    type: "object",
                    properties : {
                        visibility: {
                            title: "Visibility",
                            type: "string",
                            enum: ["global", "local"],
                            default: "global"
                        },
                        value: {
                            title: "Sequence expression",
                            type: "string"
                        },
                        initialValues:{
                            title: "Initial Values",
                            type: "array",
                            minItems: 1,
                            items: {
                                type: "integer"
                            }
                        }
                    }
                }

            }
        },
        form: [
            {
                "key": "type",
                "readonly": "true",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                "key": "innerType",
                "readonly": "true",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                key: "properties.visibility",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                key: "properties.value",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                "key":  "properties.initialValues",
                "add" : "Add",
                "htmlClass": "domainAddBtn",
                "style": {
                    "add": "btn-success"
                },
                "fieldHtmlClass": "domainField",
                "items":[
                    {
                        "key": "properties.initialValues[]",
                        "notitle" : true,
                        "disableSuccessState": true,
                        "feedback": false
                    }
                ]
            }
        ]
    },
    float: {
        schema: {
            type: "object",
            properties: {
                type: {
                    title: "Type",
                    type: "string"
                },
                innerType: {
                    title: "Base Type",
                    type: "string"
                },
                properties:{
                    type: "object",
                    properties : {
                        visibility: {
                            title: "Visibility",
                            type: "string",
                            enum: ["global", "local"],
                            default: "global"
                        },
                        value: {
                            title: "Sequence expression",
                            type: "string"
                        },
                        initialValues:{
                            title: "Initial Values",
                            type: "array",
                            minItems: 1,
                            items: {
                                type: "float"
                            }
                        }
                    }
                }
            }
        },
        form: [
            {
                "key": "type",
                "readonly": "true",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                "key": "innerType",
                "readonly": "true",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                key: "properties.visibility",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                key: "properties.value",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                "key":  "properties.initialValues",
                "add" : "Add",
                "htmlClass": "domainAddBtn",
                "style": {
                    "add": "btn-success"
                },
                "fieldHtmlClass": "domainField",
                "items":[
                    {
                        "key": "properties.initialValues[]",
                        "notitle" : true,
                        "disableSuccessState": true,
                        "feedback": false
                    }
                ]
            }
        ]
    },
    string: {
        schema: {
            type: "object",
            properties: {
                type: {
                    title: "Type",
                    type: "string"
                },
                innerType: {
                    title: "Base Type",
                    type: "string"
                },
                properties:{
                    type: "object",
                    properties : {
                        visibility: {
                            title: "Visibility",
                            type: "string",
                            enum: ["global", "local"],
                            default: "global"
                        },
                        value: {
                            title: "Sequence expression",
                            type: "string"
                        },
                        initialValues:{
                            title: "Initial Values",
                            type: "array",
                            minItems: 1,
                            items: {
                                type: "string"
                            }
                        }
                    }
                }
            }
        },
        form: [
            {
                "key": "type",
                "readonly": "true",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                "key": "innerType",
                "readonly": "true",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                key: "properties.visibility",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                key: "properties.value",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                "key":  "properties.initialValues",
                "add" : "Add",
                "htmlClass": "domainAddBtn",
                "style": {
                    "add": "btn-success"
                },
                "fieldHtmlClass": "domainField",
                "items":[
                    {
                        "key": "properties.initialValues[]",
                        "notitle" : true,
                        "disableSuccessState": true,
                        "feedback": false
                    }
                ]
            }
        ]
    },
    datetime: {
        schema: {
            type: "object",
            properties: {
                type: {
                    title: "Type",
                    type: "string"
                },
                innerType: {
                    title: "Base Type",
                    type: "string"
                },
                properties: {
                    type: "object",
                    properties: {
                        visibility: {
                            title: "Visibility",
                            type: "string",
                            enum: ["global", "local"],
                            default: "global"
                        },
                        step: {
                            title: "Step",
                            type: "string"
                        },
                        initialSource: {
                            title: "Initial date & time",
                            type: "string",
                            enum: ["now", "custom"],
                            default: "now"
                        },
                        initialDate: {
                            title: "Initial date",
                            type: "string",
                            format: "date"
                        },
                        initialTime: {
                            title: "Initial time",
                            type: "string",
                            pattern: "^(([0-1][0-9])|([2][0-3])):([0-5][0-9]):([0-5][0-9])$"
                        },
                        format: {
                            title: "Format",
                            type: "string",
                            enum: ["%Y", "%Y-%m", "%Y-%m-%d", "%H:%M", "%H:%M:%S", "%Y-%m-%dT%H:%M:%SZ"],
                            default: "%Y-%m-%dT%H:%M:%SZ"
                        }
                    }
                }

            }
        },
        form: [
            {
                "key": "type",
                "readonly": "true",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                "key": "innerType",
                "readonly": "true",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                "key": "properties.visibility",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                "key": "properties.step",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                "key": "properties.initialSource",
                "disableSuccessState": true,
                "feedback": false
            },
            {
                "key": "properties.initialDate",
                "format": "yyyy-mm-dd",
                "disableSuccessState": true,
                "feedback": false,
                "condition": "model.properties.initialSource == 'custom'"
                
            },
            {
                "key": "properties.initialTime",
                "validationMessage": {
                    "202": "Value must be valid time in format hh:mm:ss"
                },
                "disableSuccessState": true,
                "feedback": false,
                "condition": "model.properties.initialSource == 'custom'"
            },
            {
                "key": "properties.format",
                "disableSuccessState": true,
                "feedback": false,
                "titleMap": [
                    {
                        "value": "%Y",
                        "name": "YYYY"
                    },
                    {
                        "value": "%Y-%m",
                        "name": "YYYY-MM"
                    },
                    {
                        "value": "%Y-%m-%d",
                        "name": "YYYY-MM-DD"
                    },
                    {
                        "value": "%H:%M",
                        "name": "hh:mm"
                    },
                    {
                        "value": "%H:%M:%S",
                        "name": "hh:mm:ss"
                    },
                    {
                        "value": "%Y-%m-%dT%H:%M:%SZ",
                        "name": "YYYY-MM-DDThh:mm:ssZ"
                    }
                ]
            }
        ]
    }
});
