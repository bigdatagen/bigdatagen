angular.module('BDGApp.JsonEditor')

/**
 * Directive which supports the communication between parent scope, typeBox and editor content
 */
.directive('jsonEditor', [ "JsonEditorTypes",  'EditorRootType', 'typeSvc',
    function (JsonEditorTypes, EditorRootType, typeSvc) {
        "use strict";
        return {
            restrict: 'EA',
            replace: false,
            templateUrl: "src/Components/JsonEditor/JsonEditor.tpl.html",
            scope: {
                input: "="
            },
            controller: ['$scope', function ($scope) {
                $scope.JsonEditorTypes = JsonEditorTypes;
                $scope.EditorRootType = EditorRootType;
                $scope.showCustomTypes = true;

                typeSvc.init(JsonEditorTypes);
            }],
            link: function ($scope) {
                var notInitialized = true;

                /**
                 * Sets the content of new UDT created in typeBox
                 */
                $scope.newTypeContent = function (type) {
                    if (type === "object") {
                        return {
                            properties: []
                        };
                    }
                    else {
                        return {
                            properties: {}
                        }
                    }

                };

                /**
                 * Sets the root element active by default
                 */
                var init = function () {
                    if ($scope.input === undefined) {
                        return;
                    }

                    angular.forEach($scope.input, function (value) {
                        if (value.typeName === EditorRootType) {
                            $scope.activeType = value;
                        }
                    });
                };

                init();

                //Set root type active on initialization
                var initWatch = $scope.$watch("input", function (newValue) {
                    if (notInitialized && newValue !== undefined) {
                        init();
                        notInitialized = false;
                        //Deleting initialization watch
                        initWatch();
                    }
                }, true);

                //This watch is called when object reference is changed - this is when new configuration is uploaded
                $scope.$watch("input", function (newValue) {
                    if (newValue !== undefined) {
                        init();
                    }
                });
            }
        };
    }
]);