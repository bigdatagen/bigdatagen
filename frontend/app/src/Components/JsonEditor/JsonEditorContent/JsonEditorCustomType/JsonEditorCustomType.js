angular.module('BDGApp.JsonEditor')

/**
 * Directive for rendering Object and Array types
 */
.directive('jsonEditorCustomType', ['typeSvc', 'EditorArrayType',
    function (typeSvc, EditorArrayType) {
        "use strict";
        return {
            restrict: 'EA',
            replace: false,
            templateUrl: "src/Components/JsonEditor/JsonEditorContent/JsonEditorCustomType/JsonEditorCustomType.tpl.html",
            scope: {
                input: "=",
                activeProperty: "=",
                isRoot: "=",
                inArray: "=",
                onDelete: "&"
            },
            link: function ($scope, element) {
                $scope.over = false;
                $scope.isArray = $scope.input.type === EditorArrayType;
                $scope.typeLabel = typeSvc.getTypeLabel($scope.input.type);

                $scope.hovered = false;

                element.children().children(".selectable").on("mouseenter", function (e) {
                    e.stopPropagation();
                    $scope.hovered = true;
                    element.parent().trigger("mouseleave");
                    $scope.$apply();
                });

                element.children().children(".selectable").on("mouseleave", function () {
                    $scope.hovered = false;
                    element.parent().trigger("mouseenter");
                    $scope.$apply();
                });

                /**
                 * Deletes child from specified index
                 * @param index
                 */
                $scope.deleteChild = function(index) {
                    $scope.activeProperty.property = undefined;
                    if ($scope.isArray) {
                        $scope.input.properties.innerType.splice(index, 1);
                    }
                    else {
                        $scope.input.properties.splice(index, 1);
                    }
                };

                /**
                 * Callback function which is called when type from typeBox is dragged over the element.
                 * @param e
                 */
                $scope.enter = function (e) {
                    if (e.handleObj.namespace === "sortable") {
                        return;
                    }
                    $scope.over = true;
                    $scope.$apply();
                };

                /**
                 * Callback function which is called when type from typeBox is dragged out of the element.
                 */
                $scope.leave = function () {
                    $scope.over = false;
                    $scope.$apply();
                };

                $scope.drop = function () {
                    $scope.over = false;
                };

                /**
                 * Calls onDelete callback in parent scope.
                 */
                $scope.delete = function () {
                    if (typeof $scope.onDelete === "function") {
                        $scope.onDelete();
                    }
                };

                /**
                 * Callback on mouse click. It makes element active.
                 */
                $scope.makeActive = function () {
                    $scope.activeProperty.property = $scope.input;
                    $scope.activeProperty.inArray = $scope.inArray;
                };

                /**
                 * Checks whether the element should be visualized as active or not.
                 */
                $scope.$watch("activeProperty.property", function (newValue) {
                    if (newValue === $scope.input) {
                        $scope.active = true;
                    }
                    else {
                        $scope.active = false;
                    }
                });

                /**
                 * $scope.draggedItem is filled when type from typeBox is dropped over the element.
                 * If the droppedItem is defined, it means that there is an dropped type which was not processed yet.
                 * We need to add this element as another child.
                 */
                $scope.$watch("droppedItem", function (type) {
                    if (type !== undefined) {
                        $scope.droppedItem = undefined;
                        var obj = {
                            type: type.type
                        };
                        if ($scope.isArray) {
                            if ($scope.input.properties === undefined) {
                                $scope.input.properties = {
                                    innerType: []
                                };
                            }
                            else if ($scope.input.properties.innerType === undefined) {
                                $scope.input.properties.innerType = [];
                            }
                            $scope.input.properties.innerType.push(obj);
                        }
                        else {
                            if ($scope.input.properties === undefined) {
                                $scope.input.properties = [];
                            }
                            $scope.input.properties.push(obj);
                        }

                        //Making newly created item active
                        $scope.activeProperty.inArray = $scope.isArray;
                        $scope.activeProperty.property = obj;
                    }
                });
            }
        };
    }
]);