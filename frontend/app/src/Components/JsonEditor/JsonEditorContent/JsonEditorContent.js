angular.module('BDGApp.JsonEditor')

/**
 * Directive for rendering content of CSV active type.
 */
.directive('jsonEditorContent', [ "JsonEditorTypes", "SequenceType", "$timeout", "JsonEditorDefaultType",
    function (JsonEditorTypes, SequenceType, $timeout, JsonEditorDefaultType) {
        "use strict";
        return {
            restrict: 'EA',
            replace: false,
            templateUrl: "src/Components/JsonEditor/JsonEditorContent/JsonEditorContent.tpl.html",
            scope: {
                input: "=",
                activeType: "="
            },
            link: function ($scope) {
                $scope.typeMapping = {};

                //maping between type name and its definition
                angular.forEach(JsonEditorTypes, function (value) {
                    $scope.typeMapping[value.type] = value;
                });

                $scope.data = {
                    activeProperty : {
                        property: undefined
                    }
                };

                $scope.activeForm = undefined;
                $scope.activeSchema = undefined;

                /**
                 * Deactivates the active element. It is used as callback when user clicks outside of any element.
                 */
                $scope.deactivate = function () {
                    $scope.activeForm = undefined;
                    $scope.activeSchema = undefined;
                    $scope.data.activeProperty.property = undefined;
                };

                /**
                 * Deletes propertyName property if the parent is array or root.
                 * In this case propertyName property does not make sense.
                 */
                function deletePropertyNameIfNecessary() {
                    if ($scope.data.activeProperty.inArray || angular.equals($scope.data.activeProperty.property, $scope.activeType)) {
                        delete $scope.activeSchema.properties.propertyName;
                        for (var i = 0; i < $scope.activeForm.length; i++) {
                            if ($scope.activeForm[i].key === "propertyName") {
                                $scope.activeForm.splice(i, 1);
                                break;
                            }
                        }
                    }
                }

                /**
                 * Routine which is called when active property is changed.
                 * It propagates information about type to PropertyEditor.
                 */
                $scope.$watch("data.activeProperty.property", function (newValue) {
                    if (newValue && newValue.type === "sequence") {
                        $scope.activeForm = angular.copy(SequenceType[newValue.innerType].form);
                        $scope.activeSchema = angular.copy(SequenceType[newValue.innerType].schema);
                        deletePropertyNameIfNecessary();
                    }
                    else if (newValue !== undefined) {
                        if ($scope.typeMapping[newValue.type]) {
                            $scope.activeForm = angular.copy($scope.typeMapping[newValue.type].form);
                            $scope.activeSchema = angular.copy($scope.typeMapping[newValue.type].schema);
                        }
                        else {
                            $scope.activeForm = angular.copy(JsonEditorDefaultType.form);
                            $scope.activeSchema = angular.copy(JsonEditorDefaultType.schema);
                        }
                        deletePropertyNameIfNecessary();
                    }
                    else {
                        $scope.deactivate();
                    }
                });

                /**
                 * propagates the information from typeBox about activeProperty the children node.
                 */
                $scope.$watch("activeType", function (newVal, oldVal) {
                    if (!angular.equals(newVal, oldVal)) {
                        $scope.data.activeProperty.property = newVal;
                    }
                });
            }
        };
    }
]);