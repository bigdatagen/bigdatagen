angular.module('BDGApp.JsonEditor')

/**
 * Directive responsible for rendering correct type.
 */
.directive('jsonEditorTypeWrapper', ['$compile',
    function ($compile) {
        "use strict";
        return {
            restrict: 'EA',
            replace: false,
            templateUrl: "src/Components/JsonEditor/JsonEditorContent/JsonEditorTypeWrapper/JsonEditorTypeWrapper.tpl.html",
            scope: {
                input: "=",
                activeProperty: "=",
                inArray: "=",
                onDelete: "&"
            },
            link: function ($scope, element) {

                //Types which need to be compiled dynamically because they're temlates may be recursive
                var dynamicallyCompiledTypes = ["object", "array", "probSet"];

                /**
                 * Handles the situation of possibility of recursion of types. Angular does not support
                 * recursive directives in templates
                 */
                function init() {
                    element.children("json-editor-custom-type").remove();
                    element.children("json-editor-prob-set").remove();
                    if ($scope.input && dynamicallyCompiledTypes.indexOf($scope.input.type) > -1) {
                        var directive;

                        switch($scope.input.type) {
                            case "object":
                            case "array":
                                directive = "json-editor-custom-type";
                                break;
                            case "probSet":
                                directive = "json-editor-prob-set";
                                break;
                        }
                        var innerElement = $compile(
                            '<' + directive + ' input="input" in-array="inArray" ' +
                            'active-property="activeProperty" is-root="isRoot" on-delete="onDelete()"></' + directive + '>'
                        )($scope);

                        element.append(innerElement);
                    }
                }

                $scope.isRoot = element.parents("json-editor-type-wrapper").length === 0;

                init();

                $scope.$watch("input.type", function (newVal, oldVal) {
                    if (!angular.equals(newVal, oldVal)) {
                        init();
                    }
                });
            }
        };
    }
]);