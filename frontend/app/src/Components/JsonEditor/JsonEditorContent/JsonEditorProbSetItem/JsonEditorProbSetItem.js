angular.module('BDGApp.JsonEditor')

/**
 * Directive representing one set in ProbSet
 */
.directive('jsonEditorProbSetItem', [
    function () {
        "use strict";
        return {
            restrict: 'EA',
            replace: false,
            templateUrl: "src/Components/JsonEditor/JsonEditorContent/JsonEditorProbSetItem/JsonEditorProbSetItem.tpl.html",
            scope: {
                input: "=",
                activeProperty: "=",
                inArray: "=",
                onDelete: "&"
            },
            link: function ($scope) {

                /**
                 * Function called from child nodes. It deletes child at specified index.
                 * @param index
                 */
                $scope.deleteChild = function(index) {
                    $scope.activeProperty.property = undefined;
                    $scope.input.properties.splice(index, 1);
                };

                /**
                 * Callback function which is called when type from typeBox is dragged over the element.
                 * @param e
                 */
                $scope.enter = function (e) {
                    if (e.handleObj.namespace === "sortable") {
                        return;
                    }
                    $scope.over = true;
                    $scope.$apply();
                };

                /**
                 * Callback function which is called when type from typeBox is dragged out of the element.
                 */
                $scope.leave = function () {
                    $scope.over = false;
                    $scope.$apply();
                };

                $scope.drop = function () {
                    $scope.over = false;
                };

                /**
                 * Calls onDelete callback in parent scope.
                 */
                $scope.delete = function () {
                    if (typeof $scope.onDelete === "function") {
                        $scope.onDelete();
                    }
                };

                /**
                 * $scope.draggedItem is filled when type from typeBox is dropped over the element.
                 * If the droppedItem is defined, it means that there is an dropped type which was not processed yet.
                 * We need to add this element as another child.
                 */
                $scope.$watch("droppedItem", function (type) {
                    if (type !== undefined) {
                        $scope.droppedItem = undefined;
                        var obj = {
                            type: type.type
                        };
                        if ($scope.input.properties === undefined) {
                            $scope.input.properties = [];
                        }
                        $scope.input.properties.push(obj);

                        //Making newly created item active
                        $scope.activeProperty.inArray = $scope.inArray;
                        $scope.activeProperty.property = obj;
                    }
                });
            }
        };
    }
]);