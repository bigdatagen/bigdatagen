angular.module('BDGApp.JsonEditor')

/**
 * Directive responsible for rendering ProbSet type.
 */
.directive('jsonEditorProbSet', ['typeSvc',
    function (typeSvc) {
        "use strict";
        return {
            restrict: 'EA',
            replace: false,
            templateUrl: "src/Components/JsonEditor/JsonEditorContent/JsonEditorProbSet/JsonEditorProbSet.tpl.html",
            scope: {
                input: "=",
                activeProperty: "=",
                inArray: "=",
                onDelete: "&"
            },
            link: function ($scope, element) {

                element.on("mouseenter", function (e) {
                    e.stopPropagation();
                    element.parent().trigger("mouseleave");
                });

                element.on("mouseleave", function () {
                    element.parent().trigger("mouseenter");
                });

                $scope.typeLabel = typeSvc.getTypeLabel($scope.input.type);

                /**
                 * Default state of probSet
                 */
                if ($scope.input.properties === undefined) {
                    $scope.input.properties = [
                        {
                            value: 50,
                            properties: []
                        },
                        {
                            value: 50,
                            properties: []
                        }
                    ];
                }

                /**
                 * Function called from child nodes. It deletes child at specified index.
                 * @param index
                 */
                $scope.deleteChild = function(index) {
                    if (index === 0 && $scope.input.properties.length > 1) {
                        $scope.input.properties[1].value += $scope.input.properties[0].value;
                    }
                    else if ($scope.input.properties.length > 1) {
                        $scope.input.properties[index - 1].value += $scope.input.properties[index].value;
                    }

                    $scope.input.properties.splice(index, 1);
                };

                /**
                 * Adds new child. It also counts probability of new child and adjust probabilities of other children.
                 */
                $scope.addProb = function () {
                    var prob;
                    var length = $scope.input.properties.length;

                    if (length === 0) {
                        prob = 100;
                    }
                    else {
                        for (var i = length - 1; i >= 0; i--) {
                            if ($scope.input.properties[i].value > 1) {
                                if ($scope.input.properties[i].value % 2 === 0) {
                                    prob = $scope.input.properties[i].value / 2;
                                    $scope.input.properties[i].value /= 2;
                                }
                                else {
                                    prob = Math.ceil($scope.input.properties[i].value / 2);
                                    $scope.input.properties[i].value = Math.floor($scope.input.properties[i].value / 2);
                                }
                                break;
                            }
                        }
                    }

                    $scope.input.properties.push({
                        value: prob,
                        properties: []
                    });
                };

                /**
                 * Calls delete callback in parent scope
                 */
                $scope.delete = function () {
                    if (typeof $scope.onDelete === "function") {
                        $scope.onDelete();
                    }
                };
            }
        };
    }
]);