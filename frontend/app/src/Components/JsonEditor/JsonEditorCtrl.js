'use strict';

angular.module('BDGApp.JsonEditor', ['ngDragDrop', 'ui.sortable', 'BDGApp.PropertyEditor', 'ui.bootstrap', 'at.multirange-slider'])

.controller('JsonEditorCtrl', ['$scope', '$state','$stateParams', 'fileDownloadSvc',
    function($scope, $state, $stateParams, fileDownloadSvc) {

        /**
         * This if branch is used when the editor is entered by the option "From Template" of "Use Inferrer".
         */
        if ($stateParams.schema !== undefined){
            if($state.current.data.config.schema == undefined){
                $state.current.data.config.schema = angular.fromJson($stateParams.schema);
                $scope.jsonEditorInput = $state.current.data.config.schema;
            }
        }
        //sample root schema
        if($state.current.data.config.schema == undefined){
            $state.current.data.config.schema = [
                {
                    label: "Root",
                    type: "object",
                    typeName: "ROOT",
                    properties: [
                    ]
                }
            ];
        }
        $scope.jsonEditorInput = $state.current.data.config.schema;

        $scope.data = {
            uploadFile: undefined
        };

        /**
         * Callback on file upload. It replaces current schema of editor by the uploaded one.
         */
        $scope.$watch("data.uploadFile", function (newValue) {
            if (newValue) {
                $state.current.data.config.schema = angular.fromJson(newValue);
                $scope.jsonEditorInput = $state.current.data.config.schema;
                newValue = undefined;
            }
        });

        /**
         * Downloads the actual schema.
         */
        $scope.download = function () {
            fileDownloadSvc.download($scope.jsonEditorInput, 'configuration.json');
        };
    }
]);

