angular.module('BDGApp')

.directive('typeBox', [
    function () {
        "use strict";
        return {
            restrict: 'EA',
            replace: false,
            templateUrl: "src/Components/TypeBox/TypeBox.tpl.html",
            scope: {
                //contains the reference to the object representing the selected type.
                activeType: "=",
                //array of objects. Each object defines one User defined type.
                customTypes: "=",
                //array which defines the implicit types.
                types: "=",
                //Indicates whether UDTs should be shown
                showCustomTypes: "=?",
                //Name of root type. If defined and shoeCustomTypes is false, Root type is rendered anyway
                rootType: "=?",
                //Function which result defines the content of new UDT
                newTypeContent: "="
            },
            controller: ['$scope', function ($scope) {
                /**
                 * Filters only UDTs (not sequences)
                 * @param type
                 * @returns {boolean}
                 */
                $scope.filterCustomTypes = function (type) {
                    return type.type !== "sequence";
                };

                /**
                 * Filters only sequences (not UDTs)
                 * @param type
                 * @returns {boolean}
                 */
                $scope.filterSequences = function (type) {
                    return type.type === "sequence";
                };

                /**
                 * Filters only root type
                 * @param type
                 * @returns {boolean}
                 */
                $scope.filterRootType = function (type) {
                    return type.typeName === $scope.rootType;
                };

                /**
                 * Initialization of typeBox - dividing customTypes into two categries - UDTs and sequences
                 */
                $scope.init = function () {
                    var tmpArr = [], tmpSeqArr = [];
                    $scope.typesUsedInCustoms = false;

                    angular.forEach($scope.customTypes, function (item) {
                        if (item.type === "sequence") {
                            tmpSeqArr.push({
                                type: item.typeName
                            });
                        }
                        else {
                            tmpArr.push({
                                type: item.typeName
                            });

                            if (item.type && item.type !== "grammar") {
                                $scope.typesUsedInCustoms = true;
                            }
                        }
                    });

                    $scope.dragArray = tmpArr;
                    $scope.dragSequenceArray = tmpSeqArr;
                };

                /**
                 * Resets addPopover object to initial state.
                 */
                $scope.resetAddPopover = function () {
                    $scope.addPopover.typeName = "";
                    $scope.addPopover.selected = undefined;
                };

                /**
                 * Adds new UDT based on configuration set in popover.
                 */
                $scope.addType = function () {
                    var newType = {
                        label: $scope.addPopover.typeName,
                        typeName: $scope.addPopover.typeName
                    };

                    if ($scope.typesUsedInCustoms) {
                        newType.type = $scope.addPopover.selected;
                    }

                    if (typeof $scope.newTypeContent === 'function') {
                        angular.extend(newType, $scope.newTypeContent(newType.type));
                    }

                    $scope.customTypes.push(newType);
                    $scope.addPopover.isOpen = false;
                };

                /**
                 * Callback on clicking on cross button of some UDT or sequence. It deleted it from customTypes array
                 * @param obj
                 */
                $scope.deleteType = function (obj) {
                    if ($scope.activeType === obj) {
                        $scope.activeType = undefined;
                    }
                    $scope.customTypes.splice($scope.customTypes.indexOf(obj), 1);
                };

                /**
                 * Closes the addPopover
                 */
                $scope.cancelAddPopover = function () {
                    $scope.addPopover.isOpen = false;
                };

                $scope.addPopover = {
                    templateUrl: 'src/Components/TypeBox/addType.tpl.html',
                    isOpen: false
                };

                /**
                 * Resets sequencePopover object to initial state.
                 */
                $scope.resetSequencePopover = function () {
                    $scope.sequencePopover.typeName = "";
                    $scope.sequencePopover.selected = undefined;
                };

                /**
                 * Adds new sequence based on configuration set in popover.
                 */
                $scope.addSequence = function () {
                    $scope.customTypes.push({
                        label: $scope.sequencePopover.typeName,
                        type: "sequence",
                        innerType: $scope.sequencePopover.selected,
                        typeName: $scope.sequencePopover.typeName,
                        properties: {}
                    });
                    $scope.sequencePopover.isOpen = false;
                };

                /**
                 * Closes the sequencePopover
                 */
                $scope.cancelSequencePopover = function () {
                    $scope.sequencePopover.isOpen = false;
                };

                $scope.sequencePopover = {
                    templateUrl: 'src/Components/TypeBox/addSequence.tpl.html',
                    isOpen: false
                };

                $scope.$watch("customTypes", function (newVal, oldVal) {
                    if (!angular.equals(newVal, oldVal)) {
                        $scope.init();
                    }
                }, true);

                /**
                 * Makes clicked customType active
                 * @param item
                 */
                $scope.makeActive = function (item) {
                    $scope.activeType = item;
                };

                /**
                 * Returns helper HTML element for dragging
                 * @param e
                 * @returns {*}
                 */
                $scope.getHelper = function (e) {
                    var el = angular.element(e.currentTarget);
                    var helper = el.clone();

                    helper.addClass("draggedItem");
                    return helper;
                };
            }],
            link: function ($scope) {
                $scope.init();
            }
        };
    }
]);