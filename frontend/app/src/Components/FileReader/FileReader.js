/**
 * Simple directive for reading uploaded files.
 */
angular.module('BDGApp').directive("fileReader", [function () {
    return {
        scope: {
            fileReader: "="
        },
        link: function ($scope, element) {
            /**
             * When file is uploaded, this function processes the file and add propagates its content to the parent
             * scope through isolate scope variable.
             */
            element.bind("change", function (changeEvent) {
                var reader = new FileReader();
                reader.onload = function (loadEvent) {
                    $scope.$apply(function () {
                        $scope.fileReader = loadEvent.target.result;
                    });
                };
                reader.readAsText(changeEvent.target.files[0]);
            });
        }
    }
}]);
