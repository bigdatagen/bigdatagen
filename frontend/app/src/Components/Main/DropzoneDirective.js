/**
 * Definition of directive for Dropzone. This directive specifies options for Dropzone form and event handlers when
 * files are uploaded to inferrer and inferred schema is received.
 */
angular.module('BDGApp')
    .directive('dropzone',[
        function(){
            "use strict";
            return{
                restrict: 'C',
                link: function($scope, element) {

                    //configuration of Dropzone
                    var config = {
                        url: '/plugin/schemainferer/',
                        maxFilesize: 100,
                        paramName: "uploadfile",
                        maxThumbnailFilesize: 10,
                        parallelUploads: 3,
                        autoProcessQueue: false,
                        maxFiles : 3,
                        uploadMultiple : true,
                        addRemoveLinks : true
                    };

                    //definiton of Dropzone event handlers
                    var eventHandlers = {
                        'addedfile': function(file) {
                            $scope.$apply(function() {
                                $scope.fileAdded = true;
                            });
                        },
                        'sendingmultiple': function(files,xhr,formData){
                            var data = {
                                command: "run",
                                name: $scope.selectedType.inferer
                            };
                            if ($scope.infererHasParameters){
                                $.extend(data,$scope.infererModel);
                            }
                            var dataString = angular.toJson(data);
                            formData.append('configuration', dataString);
                        },
                        'successmultiple': function (file, response) {
                            if(response.indexOf("error") > -1){
                                var error = angular.fromJson(response);
                                $scope.response = error.error;
                            }
                            else{
                                $scope.data.uploadFile = response;
                            }
                            $scope.$digest();
                            $scope.$apply();
                        },
                        'error': function(file, response, xhr){
                            $scope.response = response;
                        }


                    };

                    //create instance of Dropzone form
                    var dropzone = new Dropzone(element[0], config);
                    dropzone.autoDiscover = false;
                    angular.forEach(eventHandlers, function(handler, event) {
                        dropzone.on(event, handler);
                    });

                    /**
                     * Starts processing files inserted in Dropzone form
                     */
                    $scope.processDropzone = function() {
                        dropzone.processQueue();
                    };

                    /**
                     * Resets Dropzone form
                     */
                    $scope.resetDropzone = function() {
                        dropzone.removeAllFiles();
                    };

                    /**
                     * Sets the inferrer
                     * @param inferer - name of inferrer
                     */
                    $scope.setInferer = function(inferer){
                        $scope.inferer = inferer;
                    }
                }
            }
    }]);
