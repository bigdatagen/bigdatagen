'use strict';
/**
 * Definition of MainCtrl controller. This is the controller for Main page and holds all the data about this page.
 * Main function of controller is handling click operations of buttons on Main page. As a child state of HomeCtrl
 * it has access to application workflow data through $state.  Based on configuration stored in $state controller
 * decides what buttons should be shown on page.
 */
angular.module('BDGApp')
    .controller("MainCtrl", ['$scope', '$state', '$http', '$uibModal', function($scope, $state, $http, $uibModal){

        /**
         * Shows modal window with information about error state. Contains definition for
         * $uibModal bootstrap service for opening the modal window.
         * @param message - error message that populates the modal window
         */
        var openModal = function (message) {

            var modalInstance = $uibModal.open({
                templateUrl: 'src/Components/ErrorModal/ErrorModal.tpl.html',
                controller: 'ErrorModalCtrl',
                resolve: {
                    message: function () {
                        return message;
                    }
                }
            });
        };

        /**
         * Initialization of controller. Sends http request to
         * "Schema Inferer" with "list" command. Received response has
         * information about inferrers supported by server.
         */
        var init = function(){
            //check options for inferers
            var data = {
                command: "list"
            };
            $http({
                method: 'POST',
                url: '/plugin/schemainferer/',
                data: data
            }).then(
                function success(response) {
                    if(response.data.plugins == undefined){
                        openModal("Failed to load inferrers.");
                    }
                    else {
                        $scope.infererPlugins = response.data.plugins;
                    }
                },
                function error(reason) {
                    openModal("Failed to contact server for inferrers.");
                }
            );
            
            //selected type of data to generated (json, csv, grammar...)
            $scope.radioType = {
                value : undefined
            } ;
            //selected method for editor (from scratch, use template ...)
            $scope.radioMethod = {
                value : undefined
            };
            //uploaded schema
            $scope.data = {
                uploadFile: undefined
            };
            //types of editors from configuration
            $scope.types = [];
            angular.forEach($state.current.data.appConfig, function(item){
                var tmp = {
                    name: item.onPageName,
                    type: item.name,
                    inferer: item.inferer
                };
                $scope.types.push(tmp);
            });

            $scope.methods = ["From scratch", "Use template", "Use inferrer"];

            $scope.infererSelected = false;
            $scope.response = "";
            $scope.infererHasParameters = false;
            $scope.noInferer = false;
        };
        init();

        /*
         When type of data is changed set values for selected file type
         */
        $scope.$watch("radioType.value", function(newValue){
            if(newValue){
                $scope.selectedType = $scope.types[newValue];
                $scope.$$prevSibling.editorType = $scope.selectedType.type;
                $scope.infererSelected = false;
                $scope.data.uploadFile = undefined;
                $scope.response = "";
                $scope.reset();
                $scope.radioMethod.value = undefined;
                $scope.infererHasParameters = false;
                $scope.inferer = undefined;
                angular.forEach($scope.infererPlugins, function(plugin){
                    if (plugin.name === $scope.selectedType.inferer){
                        $scope.inferer = plugin;
                    }
                });
                if($scope.inferer === undefined){
                    $scope.noInferer = true;
                }
                else{
                    $scope.noInferer = false;
                }
            }
        });
        
        /*
         When file is uploaded set schema
         */
        $scope.$watch("data.uploadFile", function (newValue) {
            if (newValue) {
                $scope.$$prevSibling.workflowSteps[1].enabled = true;
                $scope.$$prevSibling.uploadedSchema.schema = newValue;
                $scope.$$prevSibling.moveToStep(1);
            }
        });

        /**
         * Handles click event from "From scratch" button
         */
        $scope.fromScratch = function(){
            $scope.$$prevSibling.workflowSteps[1].enabled = true;
            $scope.$$prevSibling.moveToStep(1);
        };

        /**
         * Handles click event from "Use template" button
         */
        $scope.useTemplate = function(){
            //$scope.$$prevSibling.refreshWorkflow();
            $scope.inferer= false;
        };

        /**
         * Handles click event from "Use inferrer" button. Creates form
         * for uploading parameters of inferrer for selected file type
         */
        $scope.useInferer = function(){
            //find selected inferer in the list
            $scope.infererDescription = $scope.inferer.description;
            if ($scope.inferer.parameters.length > 0){
                //construction of schema-form for inferer parameters
                $scope.infererHasParameters = true;
                $scope.infererModel = {};
                $scope.infererForm = [
                    {
                        "key": "parameterType",
                        "title": "Type of parameter",
                        "disableSuccessState": true,
                        "feedback": false,
                        "htmlClass": "parameterType",
                        "titleMap": [
                            {
                                "value": ";",
                                "name": "String"
                            },
                            {
                                "value": "\t",
                                "name": "White Character"
                            }
                        ]
                    }
                ];
                angular.forEach($scope.inferer.parameters, function(parameter){
                    var selectTpl = {
                        "key":  parameter.name,
                        "title": unCamelCase(parameter.name),
                        "description": parameter.description,
                        "type": "select",
                        "htmlClass": "parameter",
                        "titleMap": [
                            {
                                "value": "\t",
                                "name": "Tab"
                            },
                            {
                                "value": " ",
                                "name": "Space"
                            }
                        ],
                        "condition": "model.parameterType == '\t'",
                        "disableSuccessState": true
                    };

                    var inputTpl = {
                        "key" : parameter.name,
                        "title" : unCamelCase(parameter.name),
                        "description": parameter.description,
                        "htmlClass": "parameter",
                        "condition": "model.parameterType == ';'",
                        $validators: {
                            emptyName: function(value) {
                                if(value !== undefined)
                                    if (value.length <= 0 ) {
                                        return false;
                                    }
                                return true
                            }
                        },
                        "validationMessage": {
                            emptyName: "Separator is empty string",
                        },
                        feedback: false,
                        "disableSuccessState": true
                    };
                    $scope.infererForm.push(selectTpl);
                    $scope.infererForm.push(inputTpl);
                });
                $scope.infererSchema = {
                    type: "object",
                    properties: {
                        parameterType: {
                            type: "string",
                            enum: [";","\t"],
                            default: ';'
                        }
                    }
                };
                angular.forEach($scope.inferer.parameters, function(parameter){
                    $scope.infererSchema.properties[parameter.name] = {
                        type: "string",
                        default: ""
                    };
                });
            }
            $scope.infererSelected = true;

        };

        /**
         * Calls processDropzone() function to start uploading files when "Upload file"
         * button is clicked
         */
        $scope.uploadFile = function() {
            $scope.processDropzone();
        };

        /**
         * Calls resetDropzone() function to reset Dropzone when "Reset Dropzone"
         * button is clicked
         */
        $scope.reset = function() {
            $scope.resetDropzone();
        };

        /**
         * Transforms string in camel case form to readable form
         * @param str - string to uncamelcase
         * @returns string in readable form
         */
        var unCamelCase = function(str){
            return str
            // insert a space between lower & upper
                .replace(/([a-z])([A-Z])/g, '$1 $2')
                // space before last upper in a sequence followed by lower
                .replace(/\b([A-Z]+)([A-Z])([a-z])/, '$1 $2$3')
                // uppercase the first character
                .replace(/^./, function(str){ return str.toUpperCase(); })
        };
    }]);
