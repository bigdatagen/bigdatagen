/**
 * Service which distinguishing whether the type is terminal or non-terminal
 */
angular.module('BDGApp.GrammarEditor').factory('grammarEditorTypeSvc', [ 'EditorTypes',
    function (EditorTypes) {
        'use strict';
        var terminalTypes = [EditorTypes.IntegerType, EditorTypes.FloatType, EditorTypes.StringType, EditorTypes.DateTimeType, EditorTypes.WhiteSpace, EditorTypes.BooleanType];
        var dynamicallyCompiledTypes = [EditorTypes.ComplexType, EditorTypes.AlternationType, EditorTypes.CycleType];

        /**
         * Returns true if type is terminal.
         * @param type
         * @returns {boolean}
         */
        function isTerminalType(type) {
            return terminalTypes.indexOf(type) > -1;
        }

        /**
         * Returns true if type is nonterminal
         * @param type
         * @returns {boolean}
         */
        function isNonterminalType(type) {
            if (type !== undefined) {
                return dynamicallyCompiledTypes.concat(terminalTypes).concat([EditorTypes.EmptyType]).indexOf(type) === -1;
            }
            return false;
        }

        return {
            isTerminalType: isTerminalType,
            isNonterminalType: isNonterminalType
        };
    }
]);