'use strict';

angular.module('BDGApp.GrammarEditor', [])

.controller('GrammarEditorCtrl', ['$scope','$state', '$stateParams', 'fileDownloadSvc', function($scope, $state, $stateParams, fileDownloadSvc) {

    var grammarDefault = [
        {
            typeName: "ROOT",
            label: "Root Nonterminal",
            type: "grammar",
            definition: {
                type: "empty"
            }
        }

    ];

    /**
     * This if branch is used when the editor is entered by the option "From Template".
     */
    if ($stateParams.schema !== undefined){
        if($state.current.data.config.schema == undefined){
            $state.current.data.config.schema = angular.fromJson($stateParams.schema);
            $scope.editorInput = $state.current.data.config.schema;
        }
    }

    //sample root schema
    if($state.current.data.config.schema == undefined){
        $state.current.data.config.schema = angular.copy(grammarDefault);
    }

    $scope.editorInput = $state.current.data.config.schema;

    $scope.data = {
        uploadFile: undefined
    };

    /**
     * Callback on file upload. It replaces current schema of editor by the uploaded one.
     */
    $scope.$watch("data.uploadFile", function (newValue) {
        if (newValue) {
            $state.current.data.config.schema = angular.fromJson(newValue);
            $scope.editorInput = $state.current.data.config.schema;
            newValue = undefined;
        }
    });

    /**
     * Downloads the actual schema.
     */
    $scope.download = function () {
        fileDownloadSvc.download($scope.editorInput, 'configuration.json');
    };
}]);

