angular.module('BDGApp.GrammarEditor')

/**
 * Directive for rendering cycle around some component
 */
.directive('grammarCycleType', [ 'EditorConstants', 'uniqueIdProviderSvc',
    function (EditorConstants, uniqueIdProviderSvc) {
        "use strict";
        return {
            restrict: 'EA',
            replace: true,
            templateUrl: "src/Components/GrammarEditor/GrammarComponents/GrammarCycleType/GrammarCycleType.tpl.html",
            scope: {
                input: "=",
                activeObj: "=",
                getSize: "&"
            },
            controller: ['$scope', function ($scope) {
                $scope.EditorConstants = EditorConstants;
                $scope.component = {};
            }],
            link: function ($scope, element) {

                $scope.transparentPartSize = (EditorConstants.TransparentLineSize - EditorConstants.VisibleLineSize) / 2;
                /**
                 * Initialization function. Preparation getSize function for child
                 */
                function init () {
                    if ($scope.input.id === undefined) {
                        $scope.input.id = uniqueIdProviderSvc.getId();
                    }

                    $scope.component.getSize = function (size) {
                        $scope.component.size = size;
                        $scope.width = $scope.component.size.width;
                        $scope.height = $scope.component.size.height + EditorConstants.ComponentMarginY;
                        propagateSizeChange();
                    };
                }

                //region Watches

                init();
                $scope.$watch("input", function (newVal, oldVal) {
                    if (!angular.equals(oldVal, newVal)){
                        init();
                    }
                });

                /**
                 * Updates the size information of this component in the parent directive scope.
                 */
                function propagateSizeChange () {
                    if (angular.isFunction($scope.getSize)) {
                        $scope.getSize({
                            size: {
                                width: $scope.width,
                                height: $scope.height + EditorConstants.TransparentLineSize
                            }
                        });
                    }

                    $scope.activeStyle = {
                        width: $scope.width + 2 * EditorConstants.TransparentLineSize,
                        height: $scope.height + EditorConstants.TransparentLineSize,
                        left: - EditorConstants.TransparentLineSize
                    };
                }

                //endregion

                element.on('$destroy', function () {
                    $scope.$destroy();
                });
            }
        };
    }
]);