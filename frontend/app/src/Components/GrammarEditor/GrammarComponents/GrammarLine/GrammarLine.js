angular.module('BDGApp.GrammarEditor')

/**
 * Directive for rendering transparent wrapper of lines between components
 * + positioning specific line types inside the template
 *
 * Each line has a transparent wrapper. This was meant for making clicks on line more easy.
 * In current implementation clicking on lines does nothing. But we want to preserve this functionality for future
 * when clicking on lines will make some sense.
 */
.directive('grammarLine', [ 'EditorConstants', "LineTypeEnum", 'BasicLineTypeEnum', '$log',
    function (EditorConstants, LineTypeEnum, BasicLineTypeEnum, $log) {
        "use strict";
        return {
            restrict: 'EA',
            replace: true,
            templateUrl: "src/Components/GrammarEditor/GrammarComponents/GrammarLine/GrammarLine.tpl.html",
            scope: {
                type: "@", //Constant from the LineTypeEnum enumeration
                lineWidth: "=",
                lineHeight: "="
            },
            controller: ['$scope', function ($scope) {
                $scope.EditorConstants = EditorConstants;
                $scope.LineTypeEnum = LineTypeEnum;
                $scope.BasicLineTypeEnum = BasicLineTypeEnum;

                var transparentPartSize = EditorConstants.TransparentLineSize - EditorConstants.VisibleLineSize;
                $scope.transparentPartSize = transparentPartSize;

                /**
                 * Initialization of css rules for the concrete type of line
                 */
                $scope.init = function() {

                    $scope.topBendStyle = {
                        width: transparentPartSize / 2,
                        height: (transparentPartSize / 2) + EditorConstants.VisibleLineSize,
                        borderColor: EditorConstants.BorderColor,
                        borderStyle: "solid",
                        position: "absolute"
                    };

                    /**
                     * Dynamic CSS hell
                     */
                    var cornerCss = {
                        topLeft: {
                            top: (transparentPartSize / 2) + "px",
                            left: (transparentPartSize / 2) + "px",
                            borderTopWidth: EditorConstants.VisibleLineSize + "px",
                            borderLeftWidth: EditorConstants.VisibleLineSize + "px",
                            borderRightWidth: "0px",
                            borderBottomWidth: "0px",
                            borderTopLeftRadius: EditorConstants.LineBorderRadius
                        },
                        topRight: {
                            top: (transparentPartSize / 2) + "px",
                            borderTopWidth: EditorConstants.VisibleLineSize + "px",
                            borderRightWidth: EditorConstants.VisibleLineSize + "px",
                            borderLeftWidth: "0px",
                            borderBottomWidth: "0px",
                            borderTopRightRadius: EditorConstants.LineBorderRadius
                        },
                        bottomLeft: {
                            top: $scope.lineHeight - EditorConstants.VisibleLineSize + "px",
                            borderBottomWidth: EditorConstants.VisibleLineSize + "px",
                            borderLeftWidth: EditorConstants.VisibleLineSize + "px",
                            borderTopWidth: "0px",
                            borderRightWidth: "0px",
                            borderBottomLeftRadius: EditorConstants.LineBorderRadius,
                            left: (transparentPartSize / 2) + "px"
                        },
                        bottomRight: {
                            top: $scope.lineHeight - EditorConstants.VisibleLineSize + "px",
                            borderBottomWidth: EditorConstants.VisibleLineSize + "px",
                            borderRightWidth: EditorConstants.VisibleLineSize + "px",
                            borderTopWidth: "0px",
                            borderLeftWidth: "0px",
                            borderBottomRightRadius: EditorConstants.LineBorderRadius
                        }
                    };

                    switch ($scope.type) {
                        case LineTypeEnum.LeftRight:
                            break;
                        case LineTypeEnum.LeftDownRight:
                            $scope.bottomBendStyle = angular.copy($scope.topBendStyle);
                            angular.extend($scope.topBendStyle, cornerCss.topRight);
                            angular.extend($scope.bottomBendStyle, cornerCss.bottomLeft);
                            break;
                        case LineTypeEnum.LeftTopRight:
                            $scope.bottomBendStyle = angular.copy($scope.topBendStyle);
                            angular.extend($scope.topBendStyle, cornerCss.topLeft);
                            angular.extend($scope.bottomBendStyle, cornerCss.bottomRight);
                            break;
                        case LineTypeEnum.Cycle:
                            $scope.leftTopBendStyle = angular.copy($scope.topBendStyle);
                            $scope.leftBottomBendStyle = angular.copy($scope.topBendStyle);
                            $scope.rightTopBendStyle = angular.copy($scope.topBendStyle);
                            $scope.rightBottomBendStyle = angular.copy($scope.topBendStyle);
                            angular.extend($scope.leftTopBendStyle, cornerCss.topLeft);
                            angular.extend($scope.leftBottomBendStyle, cornerCss.bottomLeft);
                            angular.extend($scope.rightTopBendStyle, cornerCss.topRight);
                            angular.extend($scope.rightBottomBendStyle, cornerCss.bottomRight);
                            break;
                        default:
                            $log.error($scope.type + " type of line is not implemented");
                    }
                };

                $scope.init();
                $scope.oldValue = [$scope.type, $scope.lineWidth, $scope.lineHeight];
            }],

            link: function ($scope, element) {
                $scope.$watchCollection("[type, lineWidth, lineHeight]", function (newVal) {
                    if (!angular.equals(newVal, $scope.oldVal)) {
                        $scope.oldVal = newVal;
                        $scope.init();
                    }
                });

                element.on('$destroy', function () {
                    $scope.$destroy();
                });
            }
        };
    }
]);