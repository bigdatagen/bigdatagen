angular.module('BDGApp.GrammarEditor')

/**
 * Directive for selecting active scope and executing operations on active scope.
 */
.directive('grammarScopeSelector', [ 'EditorTypes', '$timeout', 'grammarEditorTypeSvc', '$document',
    function (EditorTypes, $timeout, grammarEditorTypeSvc, $document) {
        "use strict";
        return {
            restrict: 'EA',
            replace: true,
            templateUrl: "src/Components/GrammarEditor/GrammarComponents/GrammarScopeSelector/GrammarScopeSelector.tpl.html",
            scope: {
                activeObj: "="
            },
            controller: ['$scope', function ($scope) {
                $scope.EditorTypes = EditorTypes;
                $scope.grammarEditorTypeSvc = grammarEditorTypeSvc;
            }],
            link: function ($scope, element) {
                /**
                 * Initialization of scope selector when activeElement is changed.
                 */
                $scope.$watch("activeObj.activeElement", function (newVal) {
                    if (newVal !== undefined) {
                        //selecting all possible parents scopes (Complex, Alternation and Cycle types)
                        var parents = newVal.parents(".ng-isolate-scope.grammarComplexType, .ng-isolate-scope.grammarAlternationType, .ng-isolate-scope.grammarCycleType");
                        $scope.scopes = [$scope.activeObj.activeElement.isolateScope()];
                        angular.forEach(parents, function (el) {
                            $scope.scopes.push(angular.element(el).isolateScope());
                        });
                    }
                    else {
                        $scope.scopes = [];
                    }
                });

                $scope.activateScope = function (scope) {
                    $scope.activeObj.activeScope = scope;
                };

                /**
                 * Returns true if parent scope of ActiveScope is a Cycle
                 * @returns {boolean}
                 */
                $scope.isParentCycle = function () {
                    var index = $scope.scopes.indexOf($scope.activeObj.activeScope);

                    if (index === $scope.scopes.length - 1) {
                        return false;
                    }

                    return $scope.scopes[index + 1].input.type === EditorTypes.CycleType;
                };

                /**
                 * Adds child to selected scope at specified index
                 * @param index
                 */
                $scope.addChild = function (index) {
                    $scope.activeObj.activeScope.api.addChild({
                        type: EditorTypes.EmptyType
                    }, index);
                };

                /**
                 * Deletes the first child of active scope.
                 */
                $scope.deleteFirstChild = function () {
                    $scope.activeObj.activeScope.api.deleteChild(0);
                };

                /**
                 * Deletes the active scope element.
                 * @param fromCallback
                 */
                $scope.delete = function (fromCallback) {
                    var activeScopeIndex = $scope.scopes.indexOf($scope.activeObj.activeScope);

                    deleteChild($scope.activeObj.activeScope.input, activeScopeIndex);

                    $scope.activeObj.activeElement = undefined;
                    $scope.activeObj.activeNode = undefined;
                    $scope.activeObj.activeScope = undefined;

                    /**
                     * If the function was called as a callback on Delete key stroke then we have to call
                     * Angular digest cycle.
                     */
                    if (fromCallback) {
                        $scope.$apply();
                    }
                };

                /**
                 * Deletes specified child of specified scope. It also handles situation which
                 * breaks paradigms of Grammar Editor. These are situation such as Complex/Alternation type with
                 * only one child, Cycle with no child and Complex/Cycle/Alternation as a direct child of Complex/Cycle/Alternation.
                 * @param child
                 * @param childScopeIndex
                 */
                var deleteChild = function (child, childScopeIndex) {
                    //Root scope should not be deleted. It is replaced by empty element instead.
                    if (childScopeIndex === $scope.scopes.length - 1) {
                        $scope.empty($scope.scopes[childScopeIndex]);
                    }
                    else {
                        var parentScope = $scope.scopes[childScopeIndex + 1];
                        switch (parentScope.input.type) {
                            case EditorTypes.AlternationType:
                            case EditorTypes.ComplexType:
                                var elementIndex = parentScope.input.children.indexOf(child);

                                parentScope.api.deleteChild(elementIndex);

                                //Complex and Alternation should not contain only 1 child.
                                //If so, input of only child should be propagated one scope up.
                                if (parentScope.input.children.length === 1) {
                                    var childInput = angular.copy(parentScope.input.children[0]);

                                    //relocating probability property
                                    relocateProbabilityProperty(parentScope.input, childInput);

                                    angular.forEach(parentScope.input, function (val, key) {
                                        delete parentScope.input[key];
                                    });

                                    angular.extend(parentScope.input, childInput);

                                    //We should check whether the grandParent has not have the same type as child.
                                    if (childScopeIndex < $scope.scopes.length - 2) {
                                        var grandParentScope = $scope.scopes[childScopeIndex + 2];

                                        if (grandParentScope.input.type === parentScope.input.type) {
                                            switch (parentScope.input.type) {
                                                //Alternation in Alternation can be merged
                                                case EditorTypes.AlternationType:
                                                    //we have to adjust child probabilities to have the same sum
                                                    //as the parent probability
                                                    var parentProb = parentScope.input.probability;
                                                    var finalProb = 0;
                                                    angular.forEach(parentScope.input.children, function (val) {
                                                        val.probability = Math.round(parentProb * 0.01 * val.probability);
                                                        finalProb += val.probability;
                                                    });

                                                    var adjustingIndex = 0;
                                                    while (finalProb !== parentProb) {
                                                        if (finalProb > parentProb) {
                                                            parentScope.input.children[adjustingIndex].probability--;
                                                            finalProb--;
                                                        }
                                                        else {
                                                            parentScope.input.children[adjustingIndex].probability++;
                                                            finalProb++;
                                                        }
                                                        adjustingIndex++;

                                                        if (adjustingIndex === parentScope.input.children.length) {
                                                            adjustingIndex = 0;
                                                        }
                                                    }

                                                    var parentIndex = grandParentScope.input.children.indexOf(parentScope.input);
                                                    grandParentScope.api.deleteChildWithoutAdjusting(parentIndex);

                                                    angular.forEach(parentScope.input.children, function (child, index) {
                                                        grandParentScope.api.addChildWithoutAdjusting(child, parentIndex + index);
                                                    });

                                                    break;
                                                //Complex in Complex can be merged
                                                case EditorTypes.ComplexType:
                                                    var parentIndex = grandParentScope.input.children.indexOf(parentScope.input);

                                                    angular.forEach(parentScope.input.children, function (child, index) {
                                                        grandParentScope.api.addChild(child, parentIndex + index + 1);
                                                    });

                                                    grandParentScope.api.deleteChild(parentIndex);
                                                    break;
                                                //Cycle in cycle can be simplified to 1 cycle
                                                case EditorTypes.CycleType:
                                                    grandParentScope.input.innerElement = parentScope.input.innerElement;
                                                    break;
                                            }
                                        }
                                    }
                                }

                                break;
                            //If cycle innerElement is deleted, cycle itself should be also deleted since the empty cycle is forbidden.
                            case EditorTypes.CycleType:
                                deleteChild(parentScope.input, childScopeIndex + 1);
                                break;
                        }
                    }
                };

                /**
                 * Changes type of scope to Empty type and deletes all other properties of scope
                 * @param scope
                 */
                $scope.empty = function (scope) {
                    if (scope === undefined) {
                        scope = $scope.activeObj.activeScope;
                    }

                    angular.forEach(scope.input, function (val, key) {
                        if (key === "id") {
                            return;
                        }
                        delete scope.input[key];
                    });

                    scope.input.type = EditorTypes.EmptyType;
                };

                /**
                 * Adds activeScope element to cycle. If it is already in cycle, it does nothing
                 */
                $scope.addToCycle = function () {
                    var activeScopeIndex = $scope.scopes.indexOf($scope.activeObj.activeScope);

                    var elementId = $scope.activeObj.activeNode.id;
                    var scopeId = $scope.activeObj.activeScope.input.id;

                    //We need to create new parent scope with Cycle type
                    if ($scope.scopes.length === activeScopeIndex + 1 ||
                        $scope.scopes[activeScopeIndex + 1].input.type !== EditorTypes.CycleType) {

                        var elementInput = angular.copy($scope.activeObj.activeScope.input);

                        //relocating probability property
                        var possibleProbability = relocateProbabilityProperty(elementInput);

                        angular.forEach($scope.activeObj.activeScope.input, function (val, key) {
                            delete $scope.activeObj.activeScope.input[key];
                        });

                        $scope.activeObj.activeScope.input.innerElement = elementInput;
                        $scope.activeObj.activeScope.input.type = EditorTypes.CycleType;
                        angular.extend($scope.activeObj.activeScope.input, possibleProbability);

                        //set default probability
                        $scope.activeObj.activeScope.input.cycleType = "repetitions";
                        $scope.activeObj.activeScope.input.cycleMin = 1;
                        $scope.activeObj.activeScope.input.cycleMax = 10;
                        $scope.activeObj.activeScope.input.cycleProbability = 50;

                        findUpdatedActiveElement(elementId, scopeId);
                    }

                };

                /**
                 * Removes activeScope element rom cycle. If it is not in cycle, it does nothing
                 */
                $scope.removeFromCycle = function () {
                    var activeScopeIndex = $scope.scopes.indexOf($scope.activeObj.activeScope);

                    var elementId = $scope.activeObj.activeNode.id;
                    var scopeId = $scope.activeObj.activeScope.input.id;

                    if ($scope.scopes.length !== activeScopeIndex + 1 &&
                        $scope.scopes[activeScopeIndex + 1].input.type === EditorTypes.CycleType) {

                        var parentScope = $scope.scopes[activeScopeIndex + 1];
                        var elementInput = angular.copy($scope.activeObj.activeScope.input);

                        //relocating probability property
                        relocateProbabilityProperty(parentScope.input, elementInput);

                        angular.forEach(parentScope.input, function (val, key) {
                            delete parentScope.input[key];
                        });

                        angular.extend(parentScope.input, elementInput);

                        findUpdatedActiveElement(elementId, scopeId);
                    }
                };

                /**
                 * Adds sibling to activeScope element. Add after active element by default.
                 * If activeScope element is not a child of Complex type, it creates one
                 * @param before If true, new sibling will be added before activeScope element
                 */
                $scope.addSibling = function (before) {
                    addChild(EditorTypes.ComplexType, before);
                };

                /**
                 * Adds alternative to activeScope element. Add after active element by default.
                 * If activeScope element is not a child of Alternation type, it creates one
                 */
                $scope.addAlternative = function () {
                    addChild(EditorTypes.AlternationType);
                };


                /**
                 * Adds child to activeScope element. Add after active element by default.
                 * If activeScope element is not a child of specified parent type, it creates one
                 * @param parentType Type of parent scope in which new child should be added
                 * @param before If true, new child will be added before activeScope element
                 */
                var addChild = function (parentType, before) {
                    var activeScopeIndex = $scope.scopes.indexOf($scope.activeObj.activeScope);
                    var newElement = {
                        type: EditorTypes.EmptyType
                    };

                    var elementId = $scope.activeObj.activeNode.id;
                    var scopeId = $scope.activeObj.activeScope.input.id;


                    //We need to create new parent scope with specified type
                    if ($scope.scopes.length === activeScopeIndex + 1 ||
                        $scope.scopes[activeScopeIndex + 1].input.type !== parentType) {

                        var elementInput = angular.copy($scope.activeObj.activeScope.input);

                        //relocating probability property
                        var possibleProbability = relocateProbabilityProperty(elementInput);

                        angular.forEach($scope.activeObj.activeScope.input, function (val, key) {
                            delete $scope.activeObj.activeScope.input[key];
                        });

                        $scope.activeObj.activeScope.input.children = [];
                        if (before) {
                            $scope.activeObj.activeScope.input.children.push(newElement);
                            $scope.activeObj.activeScope.input.children.push(elementInput);
                        }
                        else {
                            $scope.activeObj.activeScope.input.children.push(elementInput);
                            $scope.activeObj.activeScope.input.children.push(newElement);
                        }
                        $scope.activeObj.activeScope.input.type = parentType;
                        angular.extend($scope.activeObj.activeScope.input, possibleProbability);

                        //Add default probabilities to children if new alternation scope was created
                        if (parentType === EditorTypes.AlternationType) {
                            $scope.activeObj.activeScope.input.children[0].probability = 50;
                            $scope.activeObj.activeScope.input.children[1].probability = 50;
                        }

                        findUpdatedActiveElement(elementId, scopeId);

                    }
                    //Parent scope is specified type
                    else {
                        var parentScope = $scope.scopes[activeScopeIndex + 1];
                        var elementIndex = parentScope.input.children.indexOf($scope.activeObj.activeScope.input);

                        parentScope.api.addChild(newElement, before ? elementIndex : elementIndex + 1);
                    }
                };

                /**
                 * Waits till next angular digest and makes the previously selected items active
                 * @param elementId ID of previously selected element
                 * @param scopeId ID of previously selected scope's element
                 */
                var findUpdatedActiveElement = function (elementId, scopeId) {
                    $timeout(function () {
                        $scope.activeObj.activeElement = angular.element("#" + elementId);
                        $scope.activeObj.activeNode = $scope.activeObj.activeElement.isolateScope().input;
                        $scope.activeObj.activeScope = angular.element("#" + scopeId).isolateScope();
                    }, 100);
                };

                /**
                 * Deletes probability property from input argument.
                 * If output is specified, it adds probability to this object.
                 * Otherwise it returns new object with probability
                 * @param input
                 * @param output
                 * @returns {{probability: *}}
                 */
                var relocateProbabilityProperty = function (input, output) {
                    var probability = input.probability;
                    delete input.probability;

                    if (output) {
                        output.probability = probability;
                    }
                    else {
                        return {
                            probability: probability
                        };
                    }
                };

                /**
                 * callback on Delete key stroke
                 * @param e
                 */
                var deleteCallback = function (e) {
                    if(e.keyCode == 46) {
                        $scope.delete(true);
                    }
                };

                /**
                 * Registers the callback on Delete key stroke.
                 */
                var registerDeleteAction = function () {
                    angular.element($document).on("keyup", deleteCallback);
                }();

                /**
                 * Unregisters the callback on Delete key stroke
                 */
                var unregisterDeleteAction = function () {
                    angular.element($document).off("keyup", deleteCallback);
                };

                element.on('$destroy', function () {
                    unregisterDeleteAction();
                    $scope.$destroy();
                });
            }
        };
    }
]);