angular.module('BDGApp.GrammarEditor')

/**
 * Directive responsible for rendering the correct type of component.
 */
.directive('grammarCommonType', [ '$compile','EditorTypes', 'grammarEditorTypeSvc',
    function ($compile, EditorTypes, grammarEditorTypeSvc) {
        "use strict";
        return {
            restrict: 'EA',
            replace: true,
            templateUrl: "src/Components/GrammarEditor/GrammarComponents/GrammarCommonType/GrammarCommonType.tpl.html",
            scope: {
                input: "=",
                activeObj: "=",
                getSize: "&"
            },
            controller: ['$scope', function ($scope) {
                $scope.EditorTypes = EditorTypes;
                $scope.grammarEditorTypeSvc = grammarEditorTypeSvc;
            }],
            link: function ($scope, element) {
                $scope.dirtyFlag = false;


                /**
                 * Check if the child is ComplexType, Alternation or cycle.. If so, we need to compile them dynamically
                 */
                function init () {
                    if (!$scope.input) {
                        return;
                    }
                    $scope.dirtyFlag = true;

                    $scope.$evalAsync(function () {
                        if ($scope.dirtyFlag) {
                            $scope.dirtyFlag = false;

                            element.children(".dynamicallyCompiled").empty();

                            if ($scope.input.type === EditorTypes.ComplexType) {
                                compileComplexTypeChild();
                            }

                            if ($scope.input.type === EditorTypes.AlternationType) {
                                compileAlternationTypeChild();
                            }

                            if ($scope.input.type === EditorTypes.CycleType) {
                                compileCycleTypeChild();
                            }
                        }
                    });
                }

                /**
                 * Handles the situation of recursive Complex types. Angular does not support
                 * recursive directives in templates -> complex type children have to be compiled dynamically
                 */
                function compileComplexTypeChild () {
                    var complexTypeElement = $compile(
                        '<grammar-complex-type get-size="getSize({size:size})" input="input" ' +
                        'active-obj="activeObj"></grammar-complex-type>'
                    )($scope);

                    element.children(".dynamicallyCompiled").append(complexTypeElement);
                }

                /**
                 * Handles the situation of recursive Alternation types. Angular does not support
                 * recursive directives in templates -> alternation type children have to be compiled dynamically
                 */
                function compileAlternationTypeChild () {
                    var alternationTypeElement = $compile(
                    '<grammar-alternation-type get-size="getSize({size:size})" input="input" ' +
                    'active-obj="activeObj"></grammar-alternation-type>'
                    )($scope);

                    element.children(".dynamicallyCompiled").append(alternationTypeElement);
                }

                /**
                 * Handles the situation of recursive Cycle types. Angular does not support
                 * recursive directives in templates -> cycle type children have to be compiled dynamically
                 */
                function compileCycleTypeChild () {
                    var cycleTypeElement = $compile(
                    '<grammar-cycle-type get-size="getSize({size:size})" input="input" ' +
                    'active-obj="activeObj"></grammar-cycle-type>'
                    )($scope);

                    element.children(".dynamicallyCompiled").append(cycleTypeElement);
                }

                init();

                $scope.$watch("input", function (newVal, oldVal) {
                    if (newVal && !angular.equals(newVal, oldVal)) {
                        init();
                    }
                });

                $scope.$watch("input.type", function (newVal, oldVal) {
                    if (newVal && !angular.equals(newVal, oldVal)) {
                        init();
                    }
                });

                element.on('$destroy', function () {
                    $scope.$destroy();
                });
            }
        };
    }
]);