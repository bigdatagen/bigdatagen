angular.module('BDGApp.GrammarEditor')

/**
 * Directive for rendering array of components in horizontal alignment
 */
.directive('grammarComplexType', [ '$compile', 'EditorConstants', 'uniqueIdProviderSvc',
    function ($compile, EditorConstants, uniqueIdProviderSvc) {
        "use strict";
        return {
            restrict: 'EA',
            replace: true,
            templateUrl: "src/Components/GrammarEditor/GrammarComponents/GrammarComplexType/GrammarComplexType.tpl.html",
            scope: {
                input: "=",
                activeObj: "=",
                getSize: "&"
            },
            controller: ['$scope', function ($scope) {
                $scope.EditorConstants = EditorConstants;
                $scope.components = [];
            }],
            link: function ($scope, element) {


                /**
                 * Initialization function. Preparation of components array and getSize functions
                 */
                function init () {
                    if ($scope.input.id === undefined) {
                        $scope.input.id = uniqueIdProviderSvc.getId();
                    }

                    angular.forEach($scope.input.children, function (child, index) {
                        if (!$scope.components[index]) {
                            $scope.components[index] = {};

                            setGetSizeFunction(index);
                        }
                    });
                }

                /**
                 * Sets getSize function to child at specified index.
                 * @param index
                 */
                function setGetSizeFunction(index) {
                    var comp = $scope.components[index];

                    comp.getSize = function (size) {
                        comp.size = size;
                        $scope.width = getElementWidth();
                        $scope.height = getElementHeight();
                        propagateSizeChange();
                    };
                }

                //region Positioning inner elements
                /**
                 * Returns the horizontal position of element at specified index
                 * @param index Index of element for which the position is computed
                 * @returns {string} CSS format for "left" property (Number + "px")
                 */
                $scope.getLeftPosition = function (index) {
                    var result = index * EditorConstants.ComponentMarginX;

                    for (var i = 0; i < index; i++) {
                        if ($scope.components[i].size) {
                            result += $scope.components[i].size.width;
                        }

                    }

                    return result + "px";
                };

                /**
                 * Returns the left position of the line succeeding specified component
                 * @param index Index of the component which precede the computed line
                 * @returns {string} Position in pixels + "px" suffix
                 */
                $scope.getLeftPositionOfLine = function (index) {
                    return parseInt($scope.getLeftPosition(index + 1)) - EditorConstants.ComponentMarginX + "px";
                };
                //endregion

                //region Watches

                init();
                $scope.oldChildren = angular.copy($scope.input.children);

                $scope.$watch("input", function (newVal, oldVal) {
                    if (newVal && !angular.equals(oldVal, newVal)){
                        $scope.components = [];
                        init();
                    }
                });

                /**
                 * Computes the width of whole Complex element
                 * @returns {Number}
                 */
                function getElementWidth () {
                    var width = parseInt($scope.getLeftPosition($scope.components.length));

                    //Subtract the additional margin after last element
                    if ($scope.components.length > 0) {
                        width -= EditorConstants.ComponentMarginX;
                    }
                    return width;
                }

                /**
                 * Computes the height of whole Complex element. It is the maximum of inner elements heights.
                 * @returns {number}
                 */
                function getElementHeight () {
                    var maxHeight = 0;

                    angular.forEach($scope.components, function (component) {
                        if (component.size && component.size.height > maxHeight) {
                            maxHeight = component.size.height;
                        }
                    });

                    return maxHeight;
                }

                /**
                 * Updates the size information of this component in the parent directive scope.
                 */
                function propagateSizeChange () {
                    if (angular.isFunction($scope.getSize)) {
                        $scope.getSize({
                            size: {
                                width: $scope.width,
                                height: $scope.height
                            }
                        });
                    }

                    $scope.activeStyle = {
                        width: $scope.width,
                        height: $scope.height
                    };
                }

                //endregion

                //region API

                /**
                 * Object used from ScopeSelector for processing operations over the element
                 */
                $scope.api = {
                    /**
                     * Adds child with specified input and index to the element.
                     * @param input
                     * @param index If not specified, new child is added as last one.
                     */
                    addChild: function (input, index) {
                        //By default the item is added to the end of array
                        if (index === undefined) {
                            index = $scope.input.children.length;
                        }

                        $scope.input.children.splice(index, 0, input);
                        $scope.components.splice(index, 0, {});

                        setGetSizeFunction(index);
                    },
                    /**
                     * Deletes child from specified index.
                     * @param index
                     */
                    deleteChild: function (index) {
                        $scope.input.children.splice(index, 1);
                        $scope.components.splice(index, 1);

                        $scope.width = getElementWidth();
                        $scope.height = getElementHeight();
                        propagateSizeChange();
                    }
                };

                //endregion

                element.on('$destroy', function () {
                    $scope.$destroy();
                });
            }
        };
    }
]);