angular.module('BDGApp.GrammarEditor')

/**
 * Directive responsible for rendering empty component
 */
.directive('grammarEmptyType', [ 'uniqueIdProviderSvc',
    function (uniqueIdProviderSvc) {
        "use strict";
        return {
            restrict: 'EA',
            replace: true,
            templateUrl: "src/Components/GrammarEditor/GrammarComponents/GrammarEmptyType/GrammarEmptyType.tpl.html",
            scope: {
                input: "=",
                activeObj: "=",
                getSize: "&"
            },
            link: function ($scope, element) {
                $scope.size = [0,0];

                if ($scope.input.id === undefined) {
                    $scope.input.id = uniqueIdProviderSvc.getId();
                }

                /**
                 * Size of the element is computed by jQuery. No need of extra computation, there are no child elements.
                 */
                $scope.$watch(function () {return [element.outerWidth(), element.outerHeight()]}, function (newVal) {
                    if (!angular.equals(newVal, $scope.size) && angular.isFunction($scope.getSize)) {
                        $scope.size = newVal;
                        $scope.getSize({
                            size: {
                                width: newVal[0],
                                height: newVal[1]
                            }
                        });
                    }
                },true);

                $scope.$watch("activeObj.activeNode", function (newValue) {
                    if (newValue === $scope.input) {
                        $scope.active = true;
                    }
                    else {
                        $scope.active = false;
                    }
                });

                /**
                 * Makes the element active
                 */
                $scope.makeActive = function () {
                    $scope.activeObj.activeNode = $scope.input;
                    $scope.activeObj.activeElement = element;
                    $scope.activeObj.activeScope = $scope;
                };

                /**
                 * Callback function which is called when type from typeBox is dragged over the element.
                 * @param e
                 */
                $scope.enter = function () {
                    $scope.over = true;
                    //We are outside of Angular environment since we are in callback, not in digest cycle
                    //so we need to start digest
                    $scope.$apply();
                };

                /**
                 * Callback function which is called when type from typeBox is dragged out of the element.
                 */
                $scope.leave = function () {
                    $scope.over = false;
                    //We are outside of Angular environment since we are in callback, not in digest cycle
                    //so we need to start digest
                    $scope.$apply();
                };

                $scope.drop = function () {
                    $scope.over = false;
                };

                /**
                 * $scope.draggedItem is filled when type from typeBox is dropped over the element.
                 * If the droppedItem is defined, it means that there is an dropped type which was not processed yet.
                 * We need to distinguish replace this directive by the one with specified type.
                 */
                $scope.$watch("droppedItem", function (type) {
                    if (type !== undefined) {
                        $scope.droppedItem = undefined;
                        $scope.input.properties = {};
                        $scope.input.type = type.type;
                    }
                });

                element.on('$destroy', function () {
                    $scope.$destroy();
                });
            }
        };
    }
]);