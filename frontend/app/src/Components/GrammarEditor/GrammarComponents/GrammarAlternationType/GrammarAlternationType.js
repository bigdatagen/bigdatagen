angular.module('BDGApp.GrammarEditor')

/**
 * Directive for rendering components which are alternatives (only 1 should be used).
 * Rendering its children in vertical direction.
 */
.directive('grammarAlternationType', [ 'EditorConstants', 'uniqueIdProviderSvc',
    function (EditorConstants, uniqueIdProviderSvc) {
        "use strict";
        return {
            restrict: 'EA',
            replace: true,
            templateUrl: "src/Components/GrammarEditor/GrammarComponents/GrammarAlternationType/GrammarAlternationType.tpl.html",
            scope: {
                input: "=",
                activeObj: "=",
                getSize: "&"
            },
            controller: ['$scope', function ($scope) {
                $scope.EditorConstants = EditorConstants;
                $scope.components = [];
            }],
            link: function ($scope, element) {

                /**
                 * Initialization function. Preparation of components array and getSize functions
                 */
                function init () {
                    if ($scope.input.id === undefined) {
                        $scope.input.id = uniqueIdProviderSvc.getId();
                    }

                    angular.forEach($scope.input.children, function (child, index) {
                        if (!$scope.components[index]) {
                            $scope.components[index] = {};

                            setGetSizeFunction(index);
                        }
                    });
                }

                /**
                 * Sets getSize function to child at specified index.
                 * @param index
                 */
                function setGetSizeFunction(index) {
                    var comp = $scope.components[index];

                    comp.getSize = function (size) {
                        comp.size = size;
                        $scope.width = getElementWidth();
                        $scope.height = getElementHeight();
                        propagateSizeChange();
                    };
                }

                //region Positioning inner elements

                /**
                 * Returns the vertical position of element at specified index
                 * @param index Index of element for which the position is computed
                 * @returns {string} CSS format for "top" property (Number + "px")
                 */
                $scope.getTopPosition = function (index) {
                    var result = index * EditorConstants.ComponentMarginY;

                    for (var i = 0; i < index; i++) {
                        if ($scope.components[i].size) {
                            result += $scope.components[i].size.height;
                        }

                    }

                    return result + "px";
                };

                /**
                 * Returns the vertical position of line at specified index
                 * @param index Index of element for which line the position is computed
                 * @returns {Number} Position in pixels
                 */
                $scope.getLineTopPosition = function (index) {
                    return parseInt($scope.getTopPosition(index));
                };

                /**
                 * Returns the horizontal position of element at specified index. This calculation considers the
                 * elements width and the width of the widest element in the parent alternation component. It means
                 * that all children will be centered.
                 * @param index
                 * @returns {string} CSS format for "left" property (Number + "px")
                 */
                $scope.getLeftPosition = function (index) {
                    if ($scope.components[index].size === undefined) {
                        return;
                    }

                    var maxWidth = function () {
                        var max = 0;
                        angular.forEach($scope.components, function (value) {
                            if (value.size && value.size.width > max) {
                                max = value.size.width;
                            }
                        });

                        return max;
                    }();

                    var elementOffset = (maxWidth - $scope.components[index].size.width) / 2;
                    var totalOffset = EditorConstants.ComponentMarginX + EditorConstants.TransparentLineSize + elementOffset;

                    return totalOffset + "px";
                };

                /**
                 * Returns the width of line at specified index
                 * @param index Index of element for which line the width is computed
                 * @returns {Number} Width in pixels
                 */
                $scope.getLineWidth = function (index) {
                    return parseInt($scope.getLeftPosition(index));
                };

                /**
                 * Returns the left position of exiting line for the element on specified index.
                 * @param index Index of element for which line the position is computed
                 * @returns {string} CSS format for "left" property (Number + "px")
                 */
                $scope.getRightLineLeftPosition = function (index) {
                    if ($scope.components[index].size !== undefined) {
                        return parseInt($scope.getLeftPosition(index)) + $scope.components[index].size.width + "px";
                    }
                };
                //endregion

                //region Watches

                init();
                $scope.$watch("input", function (newVal, oldVal) {
                    if (newVal && !angular.equals(oldVal, newVal)){
                        $scope.components = [];
                        init();
                    }
                });

                /**
                 * Computes the width of whole Alternation element.
                 * It is the maximum of inner elements widths + margins for lines.
                 * @returns {Number}
                 */
                function getElementWidth () {
                    var maxWidth = 0;

                    angular.forEach($scope.components, function (component) {
                        if (component.size && component.size.width > maxWidth) {
                            maxWidth = component.size.width;
                        }
                    });

                    return maxWidth + (2 * EditorConstants.ComponentMarginX) + (2 * EditorConstants.TransparentLineSize);
                }

                /**
                 * Computes the height of whole Alternation element.
                 * @returns {number}
                 */
                function getElementHeight () {
                    var height = parseInt($scope.getTopPosition($scope.components.length));

                     //Subtract the additional margin after last element
                     if ($scope.components.length > 0) {
                     height -= EditorConstants.ComponentMarginY;
                     }
                     return height;
                }

                /**
                 * Updates the size information of this component in the parent directive scope.
                 */
                function propagateSizeChange () {
                    if (angular.isFunction($scope.getSize)) {
                        $scope.getSize({
                            size: {
                                width: $scope.width,
                                height: $scope.height
                            }
                        });
                    }

                    $scope.activeStyle = {
                        width: $scope.width,
                        height: $scope.height
                    };
                }

                //endregion

                //region API

                /**
                 * Object used from ScopeSelector for processing operations over the element
                 */
                $scope.api = {
                    /**
                     * Adds child with specified input and index to the element.
                     * It adds default probability to new child and adjust probabilities of other children.
                     * @param input
                     * @param index If not specified, new child is added as last one.
                     */
                    addChild: function (input, index) {
                        //By default the item is added to the end of array
                        if (index === undefined) {
                            index = $scope.input.children.length;
                        }

                        var prob;
                        for (var i = $scope.input.children.length - 1; i >= 0; i--) {
                            if ($scope.input.children[i].probability > 1) {
                                if ($scope.input.children[i].probability % 2 === 0) {
                                    prob = $scope.input.children[i].probability / 2;
                                    $scope.input.children[i].probability /= 2;
                                }
                                else {
                                    prob = Math.ceil($scope.input.children[i].probability / 2);
                                    $scope.input.children[i].probability = Math.floor($scope.input.children[i].probability / 2);
                                }
                                break;
                            }
                        }

                        input.probability = prob;

                        $scope.api.addChildWithoutAdjusting(input, index);
                    },
                    /**
                     * Adds child with specified input and index to element.
                     * It does not adjust probabilities of other children.
                     * @param input
                     * @param index
                     */
                    addChildWithoutAdjusting: function (input, index) {
                        $scope.input.children.splice(index, 0, input);
                        $scope.components.splice(index, 0, {});

                        setGetSizeFunction(index);
                    },
                    /**
                     * Deletes child from specified index and adjust probabilities of other children.
                     * @param index
                     */
                    deleteChild: function (index) {
                        if (index === 0) {
                            $scope.input.children[1].probability += $scope.input.children[0].probability;
                        }
                        else {
                            $scope.input.children[index - 1].probability += $scope.input.children[index].probability;
                        }

                        $scope.api.deleteChildWithoutAdjusting(index);
                    },
                    /**
                     * Deletes child from specified index.
                     * @param index
                     */
                    deleteChildWithoutAdjusting: function (index) {
                        $scope.input.children.splice(index, 1);
                        $scope.components.splice(index, 1);

                        $scope.width = getElementWidth();
                        $scope.height = getElementHeight();
                        propagateSizeChange();
                    }
                };

                //endregion

                element.on('$destroy', function () {
                    $scope.$destroy();
                });
            }
        };
    }
]);