angular.module('BDGApp.GrammarEditor')

/**
 * Directive responsible for rendering nonterminal type. Just a rounded rectangle with text.
 */
.directive('grammarTerminalType', [ 'uniqueIdProviderSvc', 'typeSvc',
    function (uniqueIdProviderSvc, typeSvc) {
        "use strict";
        return {
            restrict: 'EA',
            replace: true,
            templateUrl: "src/Components/GrammarEditor/GrammarComponents/GrammarTerminalType/GrammarTerminalType.tpl.html",
            scope: {
                input: "=",
                activeObj: "=",
                getSize: "&"
            },
            link: function ($scope, element) {
                $scope.size = [0,0];

                $scope.label = typeSvc.getTypeLabel($scope.input.type);

                if ($scope.input.id === undefined) {
                    $scope.input.id = uniqueIdProviderSvc.getId();
                }

                /**
                 * Size of the element is computed by jQuery. No need of extra computation, there are no child elements.
                 */
                $scope.$watch(function () {return [element.outerWidth(), element.outerHeight()]}, function (newVal) {
                    if (!angular.equals(newVal, $scope.size) && angular.isFunction($scope.getSize)) {
                        $scope.size = newVal;
                        $scope.getSize({
                            size: {
                                width: newVal[0],
                                height: newVal[1]
                            }
                        });
                    }
                },true);

                $scope.$watch("activeObj.activeNode", function (newValue) {
                    if (newValue === $scope.input) {
                        $scope.active = true;
                    }
                    else {
                        $scope.active = false;
                    }
                });

                /**
                 * Callback on mouse click. It makes element active.
                 */
                $scope.makeActive = function () {
                    $scope.activeObj.activeNode = $scope.input;
                    $scope.activeObj.activeElement = element;
                    $scope.activeObj.activeScope = $scope;
                };

                element.on('$destroy', function () {
                    $scope.$destroy();
                });
            }
        };
    }
]);