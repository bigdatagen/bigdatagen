angular.module('BDGApp.GrammarEditor')

/**
 * Directive for rendering straight connection lines
 */
.directive('grammarBasicLine', ['BasicLineTypeEnum', 'EditorConstants',
    function (BasicLineTypeEnum, EditorConstants) {
        "use strict";
        return {
            restrict: 'EA',
            replace: true,
            templateUrl: "src/Components/GrammarEditor/GrammarComponents/GrammarBasicLine/GrammarBasicLine.tpl.html",
            scope: {
                type: "@"
            },
            link: function ($scope, element, attributes) {
                var offset =
                    (EditorConstants.TransparentLineSize / 2) - (EditorConstants.VisibleLineSize / 2);

                /**
                 * Init function - compute dynamic CSS which depends on line type
                 */
                function init() {
                    $scope.style = {
                        backgroundColor: EditorConstants.BorderColor
                    };

                    switch ($scope.type){
                        case BasicLineTypeEnum.Vertical:
                            $scope.style.left = offset + "px";
                            $scope.style.height = "100%";
                            $scope.style.width =  EditorConstants.VisibleLineSize + "px";
                            break;
                        case BasicLineTypeEnum.Horizontal:
                            $scope.style.top = offset + "px";
                            $scope.style.width = "100%";
                            $scope.style.height =  EditorConstants.VisibleLineSize + "px";
                            break;
                        default:
                            $log.error($scope.type + " type of basic line is not implemented");
                            return;
                    }

                    transformIfHasBends();
                }

                /**
                 *  If the "hasBends" attribute is present, we have to cut the line on both ends. The size of bend is
                 *  the same as the size of the transparent line.
                 */
                function transformIfHasBends() {
                    if (attributes.hasBends !== undefined) {
                        switch ($scope.type){
                            case BasicLineTypeEnum.Vertical:
                                $scope.style.top = EditorConstants.TransparentLineSize + "px";
                                $scope.style.height = "calc(100% - " + 2  * EditorConstants.TransparentLineSize + "px)";
                                break;
                            case BasicLineTypeEnum.Horizontal:
                                $scope.style.left = EditorConstants.TransparentLineSize + "px";
                                $scope.style.width = "calc(100% - " + 2  * EditorConstants.TransparentLineSize + "px)";
                                break;
                            default:
                                $log.error($scope.type + " type of basic line is not implemented");
                        }
                    }
                }

                init();

                $scope.$watch("type", init);

                element.on('$destroy', function () {
                    $scope.$destroy();
                });

            }
        };
    }
]);