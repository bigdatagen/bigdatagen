angular.module('BDGApp.GrammarEditor')

.directive('grammarEditor', [ "GrammarEditorTypes",  'GrammarEditorRootType', 'typeSvc', "EditorTypes", "SequenceType", "GrammarEditorDefaultType",
    function (GrammarEditorTypes, GrammarEditorRootType, typeSvc, EditorTypes, SequenceType, GrammarEditorDefaultType) {
        "use strict";
        return {
            restrict: 'EA',
            replace: true,
            templateUrl: "src/Components/GrammarEditor/GrammarEditor.tpl.html",
            scope: {
                input: "="
            },
            controller: ['$scope', function ($scope) {
                $scope.activeObj = {
                    activeNode: undefined,
                    activeElement: undefined,
                    activeScope: undefined
                };

                $scope.isRoot = true;

                $scope.data = {
                    activeProperty: {}
                };

                /**
                 * Deactivates active type and scope.
                 */
                $scope.deactivate = function () {
                    $scope.activeObj.activeNode = undefined;
                    $scope.activeObj.activeElement = undefined;
                    $scope.activeObj.activeScope = undefined;

                    $scope.data.activeProperty.property = undefined;
                };

                $scope.GrammarEditorTypes = GrammarEditorTypes;
                $scope.EditorRootType = GrammarEditorRootType.type;
                $scope.sequenceType = "sequence";
                $scope.showCustomTypes = true;

                /**
                 * Sets the content of new UDT created in typeBox
                 */
                $scope.newTypeContent = function () {
                    return {
                        type: "grammar",
                        definition: {
                            type: EditorTypes.EmptyType
                        }
                    }
                };

                $scope.typeMapping = {};
                angular.forEach(GrammarEditorTypes, function (value) {
                    $scope.typeMapping[value.type] = value;
                });

                typeSvc.init(GrammarEditorTypes);
            }],
            link: function ($scope) {
                var notInitialized = true;

                function setRootTypeActive() {
                    angular.forEach($scope.input, function (value) {
                        if (value.typeName === GrammarEditorRootType.type) {
                            $scope.activeType = value;
                        }
                    });
                }

                //Set root type active on initialization
                $scope.$watch("input", function (newValue) {
                    if (notInitialized && newValue !== undefined) {
                        setRootTypeActive();
                        notInitialized = false;
                    }
                }, true);

                //This watch is called when object reference is changed - this is when new configuration is uploaded
                $scope.$watch("input", function (newValue) {
                    if (newValue !== undefined) {
                        setRootTypeActive();
                    }
                });

                $scope.propertyWrapper = {
                    property: undefined
                };

                /**
                 * Routine which is called when active Node is changed. This is used for grammar types.
                 * It propagates information about type to PropertyEditor.
                 */
                $scope.$watch("activeObj.activeNode", function (newValue, oldValue) {
                    if (newValue && oldValue && newValue.id === oldValue.id) {
                        if (newValue.type === oldValue.type) {
                            return;
                        }
                    }

                    if (newValue !== undefined) {
                        if ($scope.typeMapping[newValue.type]) {
                            $scope.activeForm = angular.copy($scope.typeMapping[newValue.type].form);
                            $scope.activeSchema = angular.copy($scope.typeMapping[newValue.type].schema);
                        }
                        else {
                            $scope.activeForm = GrammarEditorDefaultType.form;
                            $scope.activeSchema = GrammarEditorDefaultType.schema;
                        }
                        $scope.propertyWrapper.property = newValue;
                    }
                    else {
                        $scope.activeForm = undefined;
                        $scope.activeSchema = undefined;
                    }
                }, true);

                /**
                 * Routine which is called when active property is changed. This is used for sequences.
                 * It propagates information about type to PropertyEditor.
                 */
                $scope.$watch("data.activeProperty.property", function (newValue, oldValue) {
                    if (newValue && newValue.type === "sequence") {
                        $scope.propertyWrapper.property = newValue;
                        $scope.activeForm = angular.copy(SequenceType[newValue.innerType].form);
                        $scope.activeSchema = angular.copy(SequenceType[newValue.innerType].schema);
                    }
                    else {
                        $scope.activeForm = undefined;
                        $scope.activeSchema = undefined;
                    }
                });

                $scope.$watch("activeType", function (newVal, oldVal) {
                    if (!angular.equals(newVal, oldVal)) {
                        $scope.deactivate();

                        if (newVal && newVal.type === "sequence") {
                            $scope.data.activeProperty.property = newVal;
                        }
                    }
                });
            }
        };
    }
]);