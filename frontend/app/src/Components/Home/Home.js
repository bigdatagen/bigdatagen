'use strict';
/**
 * Definition of HomeCtrl controller. This is the controller for Home page and holds all the data about this page.
 * Main function of controller is handling click operations of workflow buttons located in application header. It is
 * injected with runnerSvc for resetting this service when user returns to Main page and logoutSvc for calling
 * logout function in LoginCtrl when Logout button is clicked. This controllers is also used as root state that holds
 * configuration of whole application and workflow context. When user returns to Main page controller resets the
 * workflow context.
 */
angular.module('BDGApp.Home',[])
    .controller('HomeCtrl',['$scope', '$state', '$http', 'logoutSvc', 'runnerSvc' , '$uibModal', function($scope, $state, $http, logoutSvc, runnerSvc, $uibModal){

        /**
         * Initializes workflow states
         */
        var init = function (){
            $scope.workflowSteps = [
                {name:'Main',enabled: true},
                {name:'Editor',enabled: false},
                {name:'Pipeline',enabled: false},
                {name:'Settings',enabled: false},
                {name:'Runner',enabled: false}
            ];
            //CONFIGURATION of workflow with names of states for ui-router
            $scope.workflow = ['home.main', undefined, 'home.pipeline', 'home.settings', 'home.runner'];

            $scope.editorsStateMapping = {};

            angular.forEach($state.current.data.appConfig, function(item){
                $scope.editorsStateMapping[item.name] = {
                    stateName: "home." + item.name,
                    generator: item.generator
                }
                    
            });

            $scope.editorType = undefined;
            $scope.generatorType = {
                generator: undefined
            };

            $scope.uploadedSchema = {
                schema : undefined
            };
            
            $scope.currentStep = 0;
            $scope.firstStep = true;
            $scope.lastStep = false;

            var stateName = $state.current.name;
            angular.forEach($scope.editorsStateMapping, function(editorState){
                if(stateName == editorState.stateName){
                    $scope.generatorType.generator = editorState.generator;
                    $scope.currentStep = 1;
                    $scope.workflow[1] = stateName;
                    $scope.workflowSteps[$scope.currentStep].enabled = true;
                    $scope.workflowSteps[$scope.currentStep + 1].enabled = true;
                }
            });

            $scope.requiredLogin = true;

        };

        init();

        /**
         * Shows modal window with information about error state. Contains definition for
         * $uibModal bootstrap service for opening the modal window.
         * @param message - error message that populates the modal window
         */
        var openModal = function (message) {

            var modalInstance = $uibModal.open({
                templateUrl: 'src/Components/ErrorModal/ErrorModal.tpl.html',
                controller: 'ErrorModalCtrl',
                resolve: {
                    message: function () {
                        return message;
                    }
                }
            });
        };
        /**
         * Checks whether the login is required and if so the logout button in
         * header is displayed.
         */
        var checkRequiresLogin = function(){
            $http({
                method: 'POST',
                url: '/requires-login/'
            }).then(
                function success(response) {
                    if(response.data.result == false){
                        $scope.requiredLogin = false;
                    }
                    else {
                        $scope.requiredLogin = true;
                    }
                },
                function error(reason) {
                    openModal("Failed to contact server for login status.");
                }
            );
        };

        checkRequiresLogin();

        /*
        When number of current step is changed flags for first and last
        part of workflow.
         */
        $scope.$watch('currentStep', function(newValue){
            if(newValue == 0){
                $scope.firstStep = true;
                $scope.lastStep = false;
            }
            else if (newValue == 4){
                $scope.firstStep = false;
                $scope.lastStep = true;
            }
            else {
                $scope.firstStep = false;
                $scope.lastStep = false;
            }
        });

        /*
         When type of file is changed state name for this editor is added to workflow configuration.
         Also the name of generator for this type of file type is set.
         */
        $scope.$watch('editorType', function(newValue){
            if(newValue){
                $scope.workflow[1] = $scope.editorsStateMapping[newValue].stateName;
                $scope.generatorType.generator = $scope.editorsStateMapping[newValue].generator;
            }
        });

        /**
         * Resets the workflow context and runner statistics after user returns to the Main page.
         */
        $scope.refreshWorkflow = function(){
            angular.forEach($scope.workflowSteps, function(item){
                item.enabled = false;
            });
            $scope.workflowSteps[0].enabled = true;
            $scope.uploadedSchema.schema = undefined;
            $scope.editorType = undefined;
            $state.current.data.config.schema = undefined;
            $state.current.data.config.pipeline = undefined;
            $state.current.data.config.settings = undefined;
            runnerSvc.reset();
        };

        /**
         * Handles moving between parts fo workflow. According to the "stepNumber"
         * state of application is changed by calling ui-router function $state.go().
         * @param stepNumber - number of workflow step where to move
         */
        $scope.moveToStep = function(stepNumber){
            if($scope.workflowSteps[stepNumber].enabled){
                switch (stepNumber){
                    case 0:
                        //next step enabled after proper setup on main page
                        $state.go($scope.workflow[stepNumber]);   //change to main state
                        break;
                    case 1:
                        //pipeline step is enabled, should be enabled after editor validation
                        $scope.workflowSteps[stepNumber + 1].enabled = true;
                        $state.go($scope.workflow[stepNumber], $scope.uploadedSchema);    //change to editor state
                        break;
                    case 2:
                        //settings step is enabled, should be enabled after pipeline validation
                        $scope.workflowSteps[stepNumber + 1].enabled = true;
                        $state.go($scope.workflow[stepNumber], $scope.generatorType);
                        break;
                    default:
                        //runner step is enabled, should be enabled after settings validation
                        if(stepNumber == 3){
                            $scope.workflowSteps[stepNumber + 1].enabled = true;
                        }
                        $state.go($scope.workflow[stepNumber]);

                }
                $scope.currentStep = stepNumber;
                if(stepNumber == 0){
                    $scope.refreshWorkflow();
                }
            }
            
        };

        /**
         * Sends request to server for logout operation and also calls
         * logout function of LoginCtrl through logoutSvc.
         */
        $scope.logout = function(){
            $http({
                method: 'POST',
                url: '/logout/'
            }).then(
                function success(response) {
                    if(response.data.result == "success"){
                        logoutSvc.callLogout();
                        $scope.continueMain();
                    }
                    else {
                        openModal("Failed to log out.");
                    }
                },
                function error(reason) {
                    openModal("Failed to contact server for log out.");
                }
            );
        };

        //setting for warning popup
        $scope.warningPopover = {
            templateUrl: 'src/Components/Home/warningPopover.tpl.html',
            isOpen: false,
            isEnabled: false
        };

        /**
         * Checks the number of clicked workflow button.If clicked button is 1
         * and current step number is > 1 warning popup appears.
         * @param stepNumber - number of clicked workflow button
         */
        $scope.clickWorkflow = function(stepNumber){
            if($scope.currentStep > 0 && stepNumber == 0){
                $scope.warningPopover.isEnabled = true;
                $scope.warningPopover.isOpen = true;
            }
            else {
                $scope.moveToStep(stepNumber);
                $scope.warningPopover.isOpen = false;
                $scope.warningPopover.isEnabled = false;
            }
        };

        /**
         * Listens on "Continue" button click event in warning popup. When this button is clicked application
         * moves to Main page and refreshes the workflow.
         */
        $scope.continueMain = function(){
            $state.go($scope.workflow[0]);
            $scope.warningPopover.isOpen = false;
            $scope.currentStep = 0;
            $scope.refreshWorkflow();
            $scope.warningPopover.isEnabled = false;
        };

        /**
         * Listens on "Cancel" button click event in warning popup. When this button is clicked popup is closed
         * and application remains in current state.
         */
        $scope.cancelMain = function(){
            $scope.warningPopover.isOpen = false;
        };

    }]);