/**
 * Definition of directive for PipelineEditor.tpl.html template. This directive receives 4 references
 * (as parameters "plugins", "generatorName", "pipeline" and "schema") from PipelineCtrl and holds information
 * about plugin currently selected in editor of pipeline. Information about active plugin is sent to child scopes
 * to show configuration of this plugin.
 * @param plugins - array with specification of available plugins,
 * @param generatorName - string with name of generator for selected type of file
 * @param pipeline - array with created pipeline
 * @param schema - string with textual representation of schema from Editor page
 */
angular.module('BDGApp.Pipeline')
    .directive('pipelineEditor', [
        function(){
            "use strict";
            return{
                restrict :'EA',
                replace : false,
                templateUrl : "src/Components/Pipeline/PipelineEditor.tpl.html",
                scope : {
                    plugins : "=" ,   //available plugins from
                    generatorName : "=", //generator used for generation according to schema
                    pipeline : "=",    //information about created pipeline
                    schema: "="     //schema from editor
                },
                link: function($scope){
                    $scope.activeItem = {
                        item : undefined
                    };     //active plugin in pipeline
                    $scope.activeForm = undefined;     //form data for active plugin
                    $scope.activeSettings = undefined;  //settings of active data
                    $scope.nameMapping = {};    //name mapping of plugins


                    /**
                     * Sets flags isArray and isGroup when setting of plugin is type "array" or "group"
                     */
                    var valuesInit = function(){
                        //initialization of type for each setting
                        angular.forEach($scope.nameMapping,function(item){
                            angular.forEach(item.settings,function(setting){
                                //initialization if setting is array
                                if(setting.type.indexOf("[]") > -1) {
                                    setting.isArray = true;
                                    //initialization if setting is array and group
                                    setting.isGroup = setting.type.indexOf("group") > -1;
                                    angular.forEach(setting.settings, function(groupSetting){
                                        groupSetting.isArray = groupSetting.type.indexOf("[]") > -1;
                                    })
                                }
                                else{
                                    //initialization if setting is group but not array
                                    setting.isArray = false;
                                    setting.isGroup = false;
                                    if (setting.type.indexOf("group") > -1){
                                        setting.isGroup = true;
                                        angular.forEach(setting.settings,function(groupSetting){
                                            groupSetting.isArray = groupSetting.type.indexOf("[]") > -1;
                                        });
                                    }
                                }
                            });
                        });
                    };

                    /**
                     * Initialization of array that holds information about pipeline and creates
                     * name mapping of plugins to their names.
                     */
                    var init = function(){
                        //create mapping of plugins by their names
                        angular.forEach($scope.plugins, function(item){
                            $scope.nameMapping[item.name] = item;
                        });
                        valuesInit();
                        //if pipeline is not created
                        if($scope.pipeline.length == 0){
                            //prepare 3 sections for pipeline
                            $scope.pipeline.push([]);
                            $scope.pipeline.push([]);
                            $scope.pipeline.push([]);

                            //put generator in pipeline and set his id
                            var plugin = {
                                formSettings: $scope.nameMapping[$scope.generatorName],
                                settings: {
                                    ranksNumber: 0
                                },
                                id:0,
                                name: $scope.generatorName.toLowerCase()
                            };
                            $scope.pipeline[0].push(plugin);
                            $scope.activeItem.item = 0;
                        }
                    };

                    init();

                    /*
                    When value of selected item is changed send configuration of selected plugin
                     */
                    $scope.$watch("activeItem.item", function(newValue){
                        //newValue should contain id of active item in pipeline
                        if(newValue !== undefined){
                            angular.forEach($scope.pipeline, function(item){
                                angular.forEach(item, function(innerItem){
                                    if (innerItem.id === newValue) {
                                        $scope.activeForm = innerItem.formSettings;
                                        $scope.activeSettings = innerItem.settings;
                                    }
                                });
                            });
                        }
                        else{
                            $scope.activeForm = undefined;
                            $scope.activeSettings = undefined;
                        }
                    });

                    /**
                     * Deactivation of configuration of current plugin when user clicks
                     * inside the pipeline editor and selected plugin loses focus
                     */
                    $scope.deactivate = function(){
                        $scope.activeForm = undefined;
                        $scope.activeSettings = undefined;
                        $scope.activeItem.item = undefined;
                        
                    };
                }
            }
    }]);

