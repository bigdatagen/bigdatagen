/**
 * Definition of directive for PipelineEditorTypeBox.tpl.html template. This directive receives two reference
 * (as parameters "pipeline" and "activeItem") that contains information about created pipeline and currently selected plugin.
 * Directive handles "drag&drop" operations and when plugin is dropped in pipeline new object representing this plugin is
 * created in "pipeline". Because "pipeline" is passed as a reference changes are immediately visible in parent scope.
 * Through "activeItem" object directive sends information about selected plugin to parent scope. Pipeline is devided
 * into 3 sections(for generator, transformers and storer) and each of this sections has its own graphical validator that
 * reacts when user drags item from type box over a section. After the plugin is dropped it is marked with unique id as
 * plugins with same name can be inserted in pipeline multiple times.
 * @param pipeline - array with information about created pipeline
 * @param activeItem - object with information about selected plugin
 */
angular.module('BDGApp.Pipeline')
    .directive('pipelineWrapper',[function(){
        "use strict";
        return {
            restrict : 'EA',
            replace : false,
            templateUrl : "src/Components/Pipeline/PipelineEditorContent/PipelineWrapper/PipelineWrapper.tpl.html",
            scope : {
                pipeline : "=",     //information about created pipeline
                activeItem : "="    //information about selected plugin
            },
            link : function($scope){
                //flags for setting validity of parts of pipeline
                $scope.overStorer = false;
                $scope.overTransformer = false;
                $scope.gBoxWrong = false;
                $scope.tBoxWrong = false;
                $scope.sBoxWrong = false;
                var idCount = 1;

                /**
                 * Handles situation when user drags plugin over Generator Box.
                 * Item dragged over this bux must be generator, otherwise error message appears and box will become invalid.
                 * @param e - object with information about event
                 * @param u - object with DOM helper that contains dragged element
                 */
                $scope.enterGeneratorBox = function(e,u){
                    var draggedElement = angular.element(u.helper[0]);
                    $scope.gBoxWrong = true;
                    draggedElement.append("<div style=\"font-style: italic;\" class=\"wrongG\">This item cannot be dropped here</div>")
                    $scope.$apply();
                };

                /**
                 * Handles situation when user drags plugin over Transformer Box.
                 * Item dragged over this bux must be transformer, otherwise error message appears and box will become invalid.
                 * @param e - object with information about event
                 * @param u - object with DOM helper that contains dragged element
                 */
                $scope.enterTransformerBox = function(e,u){
                    var draggedItem = angular.element(u.draggable[0]).scope().item;
                    var draggedElement = angular.element(u.helper[0]);
                    if (draggedItem.type === "storer"){
                        $scope.tBoxWrong = true;
                        draggedElement.append("<div style=\"font-style: italic;\" class=\"wrongT\">This item cannot be dropped here</div>")
                    }
                    else{
                        $scope.overTransformer = true;
                    }
                    $scope.$apply();

                };

                /**
                 * Handles situation when user drags plugin over Storer Box.
                 * Item dragged over this bux must be storer, otherwise error message appears and box will become invalid.
                 * Validation also covers condition to have only one piece of storer in pipeline
                 * @param e - object with information about event
                 * @param u - object with DOM helper that contains dragged element
                 */
                $scope.enterStorerBox = function(e,u){
                    var draggedItem = angular.element(u.draggable[0]).scope().item;
                    var draggedElement = angular.element(u.helper[0]);
                    if (draggedItem.type === "transformer"){
                        $scope.sBoxWrong = true;
                        draggedElement.append("<div style=\"font-style: italic;\" class=\"wrongS\">This item cannot be dropped here</div>")
                    }
                    else if($scope.pipeline[2].length == 1){
                        $scope.sBoxWrong = true;
                        draggedElement.append("<div style=\"font-style: italic;\" class=\"wrongS\">You can use only one storer. First remove used storer.</div>")
                    }
                    else if(draggedItem.type == undefined){
                        $scope.sBoxWrong = true;
                        draggedElement.append("<div style=\"font-style: italic;\" class=\"wrongS\">This item cannot be dropped here</div>")
                    }
                    else if(draggedItem.type === "storer"){
                        $scope.overStorer = true;
                    }
                    $scope.$apply();
                };

                /**
                 * Handles situation when user leaves Storer Box.
                 * Hides error message and returns box to valid state.
                 */
                $scope.leaveStorerBox = function(){
                    $scope.overStorer = false;
                    $scope.sBoxWrong = false;
                    $( ".wrongS" ).remove();
                    $scope.$apply();
                };

                /**
                 * Handles situation when user leaves Transformer Box.
                 * Hides error message and returns box to valid state.
                 */
                $scope.leaveTransformerBox = function(){
                    $scope.overTransformer = false;
                    $scope.tBoxWrong = false;
                    $( ".wrongT" ).remove();
                    $scope.$apply();
                };

                /**
                 * Handles situation when user leaves Generator Box.
                 * Hides error message and returns box to valid state.
                 */
                $scope.leaveGeneratorBox = function(){
                    $scope.gBoxWrong = false;
                    $( ".wrongG" ).remove();
                    $scope.$apply();
                };

                /**
                 * Handles situation when plugin is dropped into a box.
                 * This function is used for all types of boxes.
                 * Hides error message and returns box to valid state.
                 */
                $scope.drop = function () {
                    $scope.overStorer = false;
                    $scope.overTransformer = false;
                    $scope.overStorer = false;
                    $scope.overTransformer = false;
                    $scope.gBoxWrong = false;
                    $scope.tBoxWrong = false;
                    $scope.sBoxWrong = false;
                    $( ".wrong" ).remove();
                    $( ".wrongS" ).remove();
                };

                /**
                 * Deletes transformer at position "index" from pipeline.
                 * @param index - index of deleted item from pipeline
                 */
                $scope.deleteItemT = function(index){
                    $scope.pipeline[1].splice(index,1);
                    $scope.activeItem.item = undefined;
                };

                /**
                 * Deletes storer at position "index" from pipeline.
                 * @param index - index of deleted item from pipeline
                 */
                $scope.deleteItemS = function(index){
                    $scope.pipeline[2].splice(index,1);
                    $scope.activeItem.item = undefined;
                };

                /**
                 * Initialization of counter for assigning ids to dropped plugins.
                 */
                var init = function(){
                    angular.forEach($scope.pipeline,function(item){
                        angular.forEach(item,function(item1){
                            if(item1.id >= idCount){
                                idCount++;
                            }
                        })
                    })
                };
                
                init();

                /*
                Validation of dropped plugin in transformer box.
                If dropped plugin is transformer than it is added to "pipeline" object
                 */
                $scope.$watch('droppedItemT', function(item){
                    if(item !== undefined){
                        $scope.droppedItemT = undefined;
                        if (item.type === "transformer"){
                            var tmp = {
                                name: item.name.toLowerCase(),
                                formSettings: angular.copy(item),
                                settings: {
                                    ranksNumber: 0
                                },
                                id: idCount
                            };
                            $scope.pipeline[1].push(tmp);
                            $scope.activeItem.item = idCount;
                            idCount++;
                        }
                    }
                });

                /*
                 Validation of dropped plugin in transformer box.
                 If dropped plugin is storer and there are no other storers in pipeline
                 than it is added to "pipeline" object
                 */
                $scope.$watch('droppedItemS', function(item){
                    if(item !== undefined){
                        $scope.droppedItemS = undefined;
                        if (item.type === "storer" && $scope.pipeline[2].length == 0){
                            var tmp = {
                                name: item.name.toLowerCase(),
                                formSettings: angular.copy(item),
                                settings: {
                                    ranksNumber: 0
                                },
                                id: idCount
                            };
                            $scope.pipeline[2].push(tmp);
                            $scope.activeItem.item = idCount;
                            idCount++;
                        }
                    }
                })
            }
        }
    }]);
