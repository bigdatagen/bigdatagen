/**
 * Definition of directive for PipelinePropertyBox.tpl.html template. This directive receives 3 references
 * (as parameters "activeForm", "settings", "schemaEditor") containing information about focused plugin form, plugin
 * settings values and schema from Editor page. Directive creates and holds form (created by "Schema Form" framework)
 * for selected plugin. The creation of the form is based on plugin configuration received in "activeForm" parameter.
 * Form is populated by data from "settings" parameter. If the form is being created for generator type of plugin than
 * schema from "schemaEditor" is passed to Ace editor and this editor is rendered in property box.
 * @param activeForm - object with form configuration for focused plugin
 * @param settings - object with settings for focused plugin
 * @param schemaEditor - string with textual representation of schema from Editor page
 */
angular.module('BDGApp.Pipeline')
    .directive('pipelinePropertyBox',[function(){
        "use strict";
        return {
            restrict : 'EA',
            replace : false,
            templateUrl : "src/Components/Pipeline/PipelineEditorContent/PipelinePropertyBox/PipelinePropertyBox.tpl.html",
            scope : {
                activeForm : "=",   //configuration for plugin property form
                settings: "=",      //settings values for plugin property form
                schemaEditor: "="   //editor from Editor page
            },
            link : function($scope){

                $scope.aceError = false;
                $scope.aceContent = angular.copy($scope.schemaEditor.text);
                $scope.hasAdvancedSetting = false;
                $scope.showEditorSchema = false;
                $scope.isEditorSchemaAdvanced = false;
                $scope.schemaEditorText = "";
                var nonAdvancedSettings = [];
                var advancedSettings = [];

                $scope.addPopover = {
                    templateUrl: 'src/Components/Pipeline/PipelineEditorContent/PipelinePropertyBox/helpContent.tpl.html',
                        isOpen: false,
                        appendToBody: true
                };
                $scope.helpContent = "Number of workers allocated just for this plugin, i.e. no other plugin is allowed to run on workers allocated for this one. If you set in total more allocated workers than available, you will receive an error."

                //basic object for schema of plugin property form
                $scope.schema = {
                    type : "object",
                    properties : {}
                };

                //basic array for form of plugin property form
                $scope.form = [];

                //predefined part of plugin property form for "Show advanced settings" checkbox
                $scope.advancedSchema = {
                    title: "Show advanced properties",
                    type: "boolean",
                    default: false
                };

                $scope.advancedFormTpl = {
                    key: "showAdvanced",
                    type: "checkbox",
                    disableSuccessState: true,
                    feedback: false
                };

                //definition of form for ranks form
                $scope.ranksNumberForm = [
                    {
                        type: "section",
                        htmlClass: "propertyBoxSection",
                        items: [
                            {
                                key: "ranksNumber",
                                title: "Reserved number of workers",
                                disableSuccessState: true,
                                feedback: false,
                                htmlClass: "propertyInput"
                            },
                            {
                                type : "template",
                                template: '<button class="addButton helpButton" uib-popover-template="form.addPopover.templateUrl" popover-is-open="form.addPopover.isOpen" popover-placement="left" popover-append-to-body="form.addPopover.appendToBody" popover-trigger="outsideClick">?</button>',
                                addPopover: {
                                    templateUrl: 'src/Components/Pipeline/PipelineEditorContent/PipelinePropertyBox/helpContent.tpl.html',
                                    isOpen: false,
                                    appendToBody: true
                                },
                                helpContent: "Number of workers allocated just for this plugin, i.e. no other plugin is allowed to run on workers allocated for this one. If you set in total more allocated workers than available, you will receive an error."
                            }
                        ]
                    }
                ];

                $scope.ranksNumberSchema = {
                    type : "object",
                    properties : {
                        ranksNumber: {
                            type: "number",
                            minimum: 0
                        }
                    }
                };

                //templates for help buttons
                var helpFormTpl = {
                    type : "template",
                    template: '<button class="addButton helpButton" uib-popover-template="form.addPopover.templateUrl" popover-is-open="form.addPopover.isOpen" popover-placement="left" popover-append-to-body="form.addPopover.appendToBody" popover-trigger="outsideClick">?</button>',
                    addPopover: {
                        templateUrl: 'src/Components/Pipeline/PipelineEditorContent/PipelinePropertyBox/helpContent.tpl.html',
                        isOpen: false,
                        appendToBody: true
                    },
                    helpContent: ""
                };

                var helpFormInnerTpl = {
                    type : "template",
                        template: '<button class="addButton helpButtonInner" uib-popover-template="form.addPopover.templateUrl" popover-is-open="form.addPopover.isOpen" popover-placement="left" popover-append-to-body="form.addPopover.appendToBody" popover-trigger="outsideClick">?</button>',
                        addPopover: {
                        templateUrl: 'src/Components/Pipeline/PipelineEditorContent/PipelinePropertyBox/helpContent.tpl.html',
                            isOpen: false
                    },
                    helpContent: ""
                };

                /**
                 * Transforms string in camel case form to readable form
                 * @param str - string to uncamelcase
                 * @returns string in readable form
                 */
                var unCamelCase = function(str){
                    return str
                    // insert a space between lower & upper
                        .replace(/([a-z])([A-Z])/g, '$1 $2')
                        // space before last upper in a sequence followed by lower
                        .replace(/\b([A-Z]+)([A-Z])([a-z])/, '$1 $2$3')
                        // uppercase the first character
                        .replace(/^./, function(str){ return str.toUpperCase(); })
                };

                var sectionFormTpl = {
                    type: "section",
                    htmlClass: "propertyBoxSection",
                    items: []
                };

                /**
                 * Checks if the focused plugin has advanced settings. If it contains
                 * than plugin property form will contain checkbox "Show advanced settings"
                 */
                var findAdvancedSetting = function(){
                    angular.forEach($scope.activeForm.settings, function(setting){
                        if(setting.isGroup){
                            angular.forEach(setting.settings, function(groupSetting){
                                if(groupSetting.isAdvanced){
                                    $scope.hasAdvancedSetting = true;
                                    return
                                }
                            });
                        }
                        else{
                            if(setting.isAdvanced){
                                $scope.hasAdvancedSetting = true;
                                return
                            }
                        }
                    });
                };

                /**
                 * Checks if the focused plugin is generator and has schema setting
                 */
                var initEditorSchema = function(){
                    if($scope.activeForm.type == "generator"){
                        angular.forEach($scope.activeForm.settings, function(setting){
                            if(setting.name == "schema" && setting.isAdvanced){
                                $scope.isEditorSchemaAdvanced = true;
                            }
                        })
                    }
                };

                /**
                 * Creates part of schema for plugin form
                 * @param setting - object containing information about one setting of plugin
                 * @returns object with created schema
                 */
                var getSchema = function(setting){
                    var schema = {
                        type: "",
                        default: setting.value
                    };
                    if (setting.value.constructor === Array){
                        schema.default = setting.value[0].text;
                    }
                    var str = setting.type;
                    //cut [] when type is a field
                    var type = str.replace('[]','');
                    switch (type){
                        case "string":
                            schema.type = "string";
                            break;
                        case "int":
                            schema.type = "number";
                            schema.minimum = setting.min;
                            schema.maximum = setting.max;
                            break;
                        case "checkbox":
                            schema.type = "boolean";
                            break;
                        case "select":
                            schema.type = "string";
                            schema.enum = [];
                            angular.forEach(setting.items,function(item){
                                schema.enum.push(item.value);
                            })
                    }
                    return schema;
                };

                /**
                 * Creates whole form for plugin property form based on plugin settings.
                 * Function is directly modifying base array "form".
                 * @param settings - array with settings of focused plugin.
                 */
                var initForm = function(settings){
                    var helpForm = {};
                    var sectionForm = {};
                    angular.forEach(settings, function(setting){
                        if(setting.isArray){
                            var formArray = {
                                key:  setting.name,
                                title: unCamelCase(setting.name),
                                add : "Add",
                                htmlClass: "domainAddBtn array",
                                style: {
                                    "add": "btn-success"
                                },
                                fieldHtmlClass: "domainField propertyArray",
                                items: []
                            };
                            if(setting.isGroup){
                                //adding group setting to array
                                angular.forEach(setting.settings, function(groupSetting){
                                    var item = {
                                        key : setting.name + "[]." + groupSetting.name,
                                        title: unCamelCase(groupSetting.name),
                                        description : groupSetting.description,
                                        disableSuccessState: true,
                                        feedback: false,
                                        htmlClass: "propertyInputGroupInner"
                                    };
                                    if(groupSetting.type == "checkbox"){
                                        item.type = "checkbox"
                                    }
                                    if(groupSetting.type == "select"){
                                        item.type = "select";
                                        item.titleMap = [];
                                        angular.forEach(groupSetting.items,function(selectItem){
                                            item.titleMap.push(selectItem);
                                        });
                                    }
                                    if(groupSetting.isAdvanced){
                                        item.condition = "model.showAdvanced"
                                    }
                                    formArray.items.push(item);
                                    if(groupSetting.help !== ""){
                                        helpForm = angular.copy(helpFormInnerTpl);
                                        helpForm.helpContent = groupSetting.help;
                                        if(groupSetting.isAdvanced){
                                            helpForm.condition = "model.showAdvanced"
                                        }
                                        formArray.items.push(helpForm);
                                    }
                                })
                            }
                            else{
                                var item = {
                                    key : setting.name + "[]",
                                    notitle : true,
                                    disableSuccessState: true,
                                    feedback: false,
                                    description: setting.description
                                };
                                if(setting.type == "checkbox[]"){
                                    item.type = "checkbox"
                                }
                                if(setting.type == "select[]"){
                                    item.key = setting.name + "[].selectDummy";
                                    item.type = "select";
                                    item.titleMap = [];
                                    angular.forEach(setting.items,function(selectItem){
                                        item.titleMap.push(selectItem);
                                    });
                                }
                                formArray.items.push(item);
                            }
                            if(setting.isAdvanced){
                                formArray.condition = "model.showAdvanced"
                            }
                            sectionForm = angular.copy(sectionFormTpl);
                            sectionForm.items[0] = formArray;
                            if(setting.help !== ""){
                                helpForm = angular.copy(helpFormTpl);
                                helpForm.helpContent = setting.help;
                                if(setting.isAdvanced){
                                    helpForm.condition = "model.showAdvanced"
                                }
                                sectionForm.items[1] = helpForm;
                            }
                            $scope.form.push(sectionForm);
                        }
                        else if(setting.isGroup){
                            var formGroup = {
                                type: "fieldset",
                                htmlClass: "domainAddBtn",
                                description : setting.description,
                                fieldHtmlClass: "domainField propertyArray",
                                title: unCamelCase(setting.name),
                                items: []
                            };
                            angular.forEach(setting.settings,function(groupSetting){
                                var formGroupSetting = {
                                    key:  setting.name + "." + groupSetting.name,
                                    title: unCamelCase(groupSetting.name)
                                };
                                if(groupSetting.isArray){
                                    formGroupSetting.add = "Add";
                                    formGroupSetting.htmlClass= "domainAddBtnInner";
                                    formGroupSetting.style = {
                                        "add": "btn-success"
                                    };
                                    formGroupSetting.fieldHtmlClass= "domainFieldInner propertyArray";
                                    formGroupSetting.items = [];
                                    var item = {
                                        key: setting.name + "." + groupSetting.name + "[]",
                                        notitle: true,
                                        disableSuccessState: true,
                                        feedback: false,
                                        description: groupSetting.description
                                    };
                                    if(groupSetting.type == "checkbox[]"){
                                        item.type = "checkbox";
                                    }
                                    if(groupSetting.type == "select[]"){
                                        item.key = setting.name + "." + groupSetting.name + "[].selectDummy";
                                        item.type = "select";
                                        item.titleMap = [];
                                        angular.forEach(groupSetting.items,function(selectItem){
                                            item.titleMap.push(selectItem);
                                        });
                                    }
                                    formGroupSetting.items.push(item);
                                }
                                else{
                                    formGroupSetting.disableSuccessState = true;
                                    formGroupSetting.feedback = false;
                                    formGroupSetting.description =  groupSetting.description;
                                    formGroupSetting.htmlClass = "propertyInputInner";
                                    if(groupSetting.type == "checkbox"){
                                        formGroupSetting.type = "checkbox";
                                    }
                                    if(groupSetting.type == "select"){
                                        formGroupSetting.type = "select";
                                        formGroupSetting.titleMap = [];
                                        angular.forEach(setting.items,function(selectItem){
                                            formGroupSetting.titleMap.push(selectItem);
                                        });
                                    }
                                }
                                if(groupSetting.isAdvanced){
                                    formGroupSetting.condition = "model.showAdvanced"
                                }
                                sectionForm = angular.copy(sectionFormTpl);
                                sectionForm.items[0] = formGroupSetting;
                                if(groupSetting.help !== ""){
                                    helpForm = angular.copy(helpFormInnerTpl);
                                    helpForm.helpContent = groupSetting.help;
                                    if(groupSetting.isAdvanced){
                                        helpForm.condition = "model.showAdvanced"
                                    }
                                    sectionForm.items[1] = helpForm;
                                }
                                formGroup.items.push(sectionForm);
                            });
                            if(setting.isAdvanced){
                                formGroup.condition = "model.showAdvanced";
                            }
                            sectionForm = angular.copy(sectionFormTpl);
                            sectionForm.items[0] = formGroup;
                            if(setting.help !== ""){
                                helpForm = angular.copy(helpFormTpl);
                                helpForm.helpContent = setting.help;
                                if(setting.isAdvanced){
                                    helpForm.condition = "model.showAdvanced";
                                }
                                sectionForm.items[1] = helpForm;
                            }
                            $scope.form.push(sectionForm);
                        }
                        else {
                            if(setting.name !== "schema"){
                                var form = {
                                    key : setting.name,
                                    title : unCamelCase(setting.name),
                                    disableSuccessState: true,
                                    feedback: false,
                                    description: setting.description,
                                    htmlClass: "propertyInput"
                                };
                                if(setting.type == "checkbox"){
                                    form.type = "checkbox";
                                }
                                if(setting.type == "select"){
                                    form.type = "select";
                                    form.titleMap = [];
                                    angular.forEach(setting.items,function(selectItem){
                                        form.titleMap.push(selectItem);
                                    });
                                }
                                if (setting.isAdvanced){
                                    form.condition = "model.showAdvanced";
                                }
                                sectionForm = angular.copy(sectionFormTpl);
                                sectionForm.items[0] = form;
                                if(setting.help !== ""){
                                    helpForm = angular.copy(helpFormTpl);
                                    helpForm.helpContent = setting.help;
                                    if (setting.isAdvanced){
                                        helpForm.condition = "model.showAdvanced";
                                    }
                                    sectionForm.items[1]= helpForm;

                                }
                                $scope.form.push(sectionForm);
                            }
                        }
                    });
                };

                /**
                 * Creates whole schema for plugin property form based on plugin settings.
                 * Function is directly modifying base object "schema".
                 * @param settings - array with settings of focused plugin.
                 */
                var initSchema = function(settings){
                    angular.forEach(settings, function(setting){
                        if(setting.isArray){
                            var arraySchema = {
                                type: "array",
                                minItems : 1,
                                items: {}
                            };
                            if(setting.isGroup){
                                var itemSchema = {
                                    type: "object",
                                    properties: {}
                                };
                                angular.forEach(setting.settings,function(groupSetting){
                                    itemSchema.properties[groupSetting.name] = getSchema(groupSetting);
                                });
                                arraySchema.items = itemSchema;
                            }
                            else {
                                //array of selects needs an object for item
                                if(setting.type == "select[]"){
                                    var arraySelectSchema = {
                                        type: "object",
                                        properties: {}
                                    };
                                    arraySelectSchema.properties["selectDummy"] = getSchema(setting);
                                    arraySchema.items = arraySelectSchema;
                                }
                                else{
                                    arraySchema.items = getSchema(setting);
                                }
                            }
                            $scope.schema.properties[setting.name] = arraySchema;
                        }
                        else if(setting.isGroup){
                            itemSchema = {
                                type: "object",
                                properties: {}
                            };
                            angular.forEach(setting.settings,function(groupSetting){
                                if(groupSetting.isArray){
                                    if(groupSetting.type == "select[]"){
                                        var arraySelectSchema = {
                                            type: "array",
                                            minItems: 1,
                                            items: {
                                                type: "object",
                                                properties: {}
                                            }
                                        };
                                        arraySelectSchema.items.properties["selectDummy"] = getSchema(groupSetting);
                                        itemSchema.properties[groupSetting.name] = arraySelectSchema;

                                    }
                                    else{
                                        var groupArraySchema = {
                                            type: "array",
                                            items: getSchema(groupSetting)
                                        };
                                        itemSchema.properties[groupSetting.name] = groupArraySchema;
                                    }

                                }
                                else{
                                    itemSchema.properties[groupSetting.name] = getSchema(groupSetting);
                                }
                            });
                            $scope.schema.properties[setting.name] = itemSchema;
                        }
                        else{
                            if(setting.name !== "schema"){
                                $scope.schema.properties[setting.name] = getSchema(setting);
                            }
                        }
                    });
                };

                /*
                When value of activeForm is changed new plugin property form must be created
                 */
                $scope.$watch("activeForm",function(newValue){
                    if(newValue !== undefined){
                        $scope.addPopover.isOpen = true;
                        $scope.addPopover.isOpen = false;
                        $scope.showProperties = true;
                        $scope.hasAdvancedSetting = false;
                        $scope.showEditorSchema = false;
                        $scope.isEditorSchemaAdvanced = false;
                        advancedSettings = [];
                        nonAdvancedSettings = [];
                        //$scope.schemaEditorText = angular.toJson($scope.schemaEditor);;
                        $scope.schema = {
                            type : "object",
                            properties : {}
                        };

                        $scope.form = [];

                        findAdvancedSetting();
                        sortOutAdvancedSettings($scope.activeForm.settings);
                        initForm(nonAdvancedSettings);
                        $scope.form.push(angular.copy($scope.advancedFormTpl));
                        initForm(advancedSettings);
                        $scope.schema.properties["showAdvanced"] = angular.copy($scope.advancedSchema);
                        initSchema($scope.activeForm.settings);
                    }
                    else{
                        $scope.schema = {
                            type : "object",
                            properties : {}
                        };

                        $scope.form = [];
                        $scope.showProperties = false;
                    }

                });

                /*
                When "Show advanced options" checkbox is checked Ace editor and ranks form must appear
                 */
                $scope.$watch("settings.showAdvanced",function(newValue){
                    if(newValue !== undefined){
                        initEditorSchema();
                        if(newValue == true && $scope.isEditorSchemaAdvanced){
                            $scope.showEditorSchema = true;
                        }
                        else {
                            $scope.showEditorSchema = false;
                        }
                        $scope.showRanksNumber = newValue == true;
                    }
                });

                /**
                 * Divides form settings configuration into advanced and non-advaced settings
                 * @param settings - array with plugin settings
                 */
                var sortOutAdvancedSettings = function(settings){
                    angular.forEach(settings, function(setting){
                        if(setting.isAdvanced){
                            advancedSettings.push(angular.copy(setting));
                        }
                        else {
                            nonAdvancedSettings.push(angular.copy(setting));
                        }
                    })
                }
            },
            controller: ["$scope", function ($scope) {
                /**
                 * Configuration of Ace editor. Editor is configured to
                 * change the value of schema from Editor page only when
                 * schema in Ace editor is valid.
                 * @param editor - current editor instance
                 */
                $scope.aceLoaded = function(editor){

                    editor.getSession().on("changeAnnotation", function(){
                        if (!editor.$isFocused) {
                            return;
                        }

                        var annotations = editor.getSession().getAnnotations();
                        $scope.aceError = false;
                        for(var i = 0; i < annotations.length; i++){
                            if(annotations[i].type == "error"){
                                $scope.aceError = true;
                                break
                            }
                        }
                        if(!$scope.aceError){
                            $scope.schemaEditor.text = editor.getValue();
                            $scope.$apply();
                        }
                    });
                };
            }]
        }
    }])
    /**
     * Filter for creating new lines from \n tags
     */
    .filter('newlines', function () {
        return function(text) {
            return text.split(/\n/g);
        }
    });
