/**
 * Definition of directive for PipelineItem.tpl.html template. This directive characterize one instance of plugin
 * inserted in pipeline boxes. It receives 3 references (as parameters "input", "activeItem", "onDelete")
 * containing information about this instance of plugin, object with Id for focused plugin and function for deleting
 * this instance of plugin. If ID of focused plugin is same as Id of this plugin than this plugin is highlighted.
 * @param input - object with information about this instance of plugin
 * @param activeItem - object with Id of focused plugin
 * @param onDelete - delete function for this instance of plugin
 */
angular.module('BDGApp.Pipeline')
    .directive('pipelineItem',[function(){
        "use strict";
        return {
            restrict : 'EA',
            replace : false,
            templateUrl : "src/Components/Pipeline/PipelineEditorContent/PipelineItem/PipelineItem.tpl.html",
            scope : {
                input : "=",        //object with information about this instance of plugin
                activeItem : "=",   //object with Id of focused plugin
                onDelete: "&"       //delete function for this instance of plugin
            },
            link : function($scope, element){
                $scope.active = false;
                $scope.isGenerator = false;
                $scope.isTransformer = false;
                $scope.isStorer = false;
                $scope.hovered = false;

                /**
                 * Initialization of this instance of plugin. Checks the type of plugin.
                 */
                var init = function(){
                    if($scope.input.formSettings.type === "generator"){
                        $scope.isGenerator = true;
                    }
                    else if($scope.input.formSettings.type === "transformer"){
                        $scope.isTransformer = true;
                    }
                    else if($scope.input.formSettings.type === "storer"){
                        $scope.isStorer = true;
                    }
                };

                init();

                /*
                Highlighting function for mouseenter event
                 */
                element.children(".selectable").on("mouseenter", function (e) {
                    e.stopPropagation();
                    $scope.hovered = true;
                    //element.parent().trigger("mouseleave");
                    $scope.$apply();
                });

                /*
                 Highlighting function for mouseleave event
                 */
                element.children(".selectable").on("mouseleave", function () {
                    $scope.hovered = false;
                    //element.parent().trigger("mouseenter");
                    $scope.$apply();
                });

                /**
                 * Changes the ID in activeItem object when this instance of plugin is focused
                 */
                $scope.makeActive = function(){
                    var s = $scope.input.id;
                    $scope.activeItem.item = $scope.input.id;
                };

                /*
                When value of activeItem ID is same as ID of this plugin than highlight this plugin,
                otherwise cancel highlight.
                 */
                $scope.$watch("activeItem.item",function(newValue){
                   if (newValue === $scope.input.id){
                        $scope.active = true;
                   }
                   else {
                        $scope.active = false;
                   }
                });

                /**
                 * Deletes this instance of plugin from pipeline box by calling assigned "onDelete()" function.
                 */
                $scope.delete = function(){
                    $scope.onDelete();
                }
            }
        }
    }]);