'use strict';
/**
 * Definition of PipelineCtrl. This is the controller for a Pipeline page and holds all the data about
 * pipeline. As a child state of HomeCtrl it has access to application workflow data through
 * $state. Initializes default pipeline configuration (when Pipeline page is first visited) and through
 * http request collects configuration of available plugins that can be inserted in pipeline.
 */
angular.module('BDGApp.Pipeline',['ngDragDrop', 'ui.sortable', 'ui.ace'])
    .controller('PipelineCtrl',['$scope', '$http', '$state', '$uibModal', function($scope, $http, $state, $uibModal){

        if($state.current.data.config.pipeline == undefined){
            $state.current.data.config.pipeline = [];
        }
        
        $scope.defaultUrl = '/plugin/';
        $scope.generatorName = $state.params.generator;
        $scope.loading = true;
        $scope.pipeline = $state.current.data.config.pipeline;

        /*
         Schema from editor received from shared $state object.
         This schema is later passed to Ace editor
         */
        $scope.data = {
            schema : {
                text: angular.toJson($state.current.data.config.schema, true)
            }
        };

        /**
         * Shows modal window with information about error state. Contains definition for
         * $uibModal bootstrap service for opening the modal window.
         * @param message - error message that populates the modal window
         */
        var openModal = function (message) {

            var modalInstance = $uibModal.open({
                templateUrl: 'src/Components/ErrorModal/ErrorModal.tpl.html',
                controller: 'ErrorModalCtrl',
                resolve: {
                    message: function () {
                        return message;
                    }
                }
            });
        };

        /**
         * Initialization of controller. Through http request contacts "PipelinePluginManager"
         * and receives configuration of available plugins.
         */
        var init = function(){
            if($state.current.data.config.plugins == undefined) {
                $http({
                    method: 'POST',
                    url: $scope.defaultUrl + 'pipelinepluginmanager/'
                }).then(
                    function success(response) {
                        if(response.data.plugins == undefined){
                            openModal("Failed to load plugins.");
                        }
                        else{
                            //store in formation about plugins in shared $state object
                            $state.current.data.config.plugins = response.data.plugins;
                            $scope.plugins = $state.current.data.config.plugins;
                            $scope.loading = false;
                        }
                    },
                    function error(reason) {
                        openModal("Failed to contact server for plugins.");
                    }
                );
            }
            else {
                //shared $state object already contains plugin configuration
                $scope.plugins = $state.current.data.config.plugins;
                $scope.loading = false;
            }

        };
        //initialize controller
        init();

        /*
        When schema in Ace editor changes update schema in $state
         */
        $scope.$watch("data.schema.text", function(newValue){
            if(newValue !== undefined){
                $state.current.data.config.schema = angular.fromJson(newValue);
            }
        });




    }]);