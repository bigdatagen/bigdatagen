/**
 * Definition of directive for PipelineEditorTypeBox.tpl.html template. This directive receives one reference
 * (as parameter "input") that contains information about available plugins. With this information on the scope
 * the assigned template can render this plugins in left box with pipeline types in Pipeline Page.
 * @param input - array with information about available plugins
 */
angular.module('BDGApp.Pipeline')
    .directive('pipelineEditorTypeBox',[function(){
        "use strict";
        return {
            restrict : 'EA',
            replace : false,
            templateUrl : "src/Components/Pipeline/PipelineEditorTypeBox/PipelineEditorTypeBox.tpl.html",
            scope : {
                input : "="
            },
            link : function($scope){
                var init = function(){
                    $scope.storers = [];
                    $scope.transformers = [];

                    angular.forEach($scope.input,function(item){
                        if (item.type === "storer"){
                            $scope.storers.push(item);
                        }
                        else if (item.type === "transformer"){
                            $scope.transformers.push(item);
                        }
                    })
                };

                init();
            }
        }
    }]);
