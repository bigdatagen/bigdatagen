/**
 * Very simple directive which renders specified content into the Footer.
 */
angular.module('BDGApp').directive("footer", [function () {
    return {
        scope: {
            config: "="
        },
        transclude: true,
        templateUrl: "src/Components/Footer/Footer.tpl.html"
    }
}]);
