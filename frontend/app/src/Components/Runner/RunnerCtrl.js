'use strict';
/**
 * Definition of RunnerCtrl controller. This is a controller for Runner page. As a child state of HomeCtrl it has access to
 * application workflow data through $state. Through injected runnerSvc it has access to generation job statistic
 * (object "runner") and job controlling functions (start(), pause() nad stop()) exposed by this service. When this
 * controller is initialized the "runner" object of runnerSvc is put on controller's scope and all UI components
 * are connected to service data. This ensures that when user moves within application workflow and comes back to
 * Runner page the data about generation job aren't lost. Graphs used in this page are created with "Google Graphs" framework.
 */
angular.module('BDGApp.Runner',['googlechart'])
    .controller('RunnerCtrl', ['$scope', '$state', 'runnerSvc', '$uibModal', function($scope, $state, runnerSvc, $uibModal) {

        /**
         * Initialization of this controller. Data from runnerSvc are put on
         * the scope of controller
         */
        function init() {
            $scope.runner = runnerSvc.runner;
            $scope.monitoringBox = runnerSvc.runner.monitoringContent;
        }

        init();

        /**
         * Shows modal window with information about error state. Contains definition for
         * $uibModal bootstrap service for opening the modal window.
         * @param message - error message that populates the modal window
         */
        var openModal = function (message) {

            var modalInstance = $uibModal.open({
                templateUrl: 'src/Components/ErrorModal/ErrorModal.tpl.html',
                controller: 'ErrorModalCtrl',
                resolve: {
                    message: function () {
                        return message;
                    }
                }
            });
        };

        /**
         * Control function for start operation when user clicks start button.
         * Collects and transforms all application workflow data stored in $state and creates a configuration object.
         * After creating this object is passed as a parameter to runnerSvc function start() and a generation job is
         * started.
         */
        $scope.send = function () {
            //if error occurs show modal window and terminate function
            if($state.current.data.config.pipeline == undefined || $state.current.data.config.pipeline.length == 0){
                openModal("Failed to load pipeline configuration.");
                return;
            }
            //collect configuration of schema, pipeline and settings from shared $state object
            $scope.pipeline = angular.copy($state.current.data.config.pipeline);
            $scope.schema = angular.copy($state.current.data.config.schema);
            $scope.settings = angular.copy($state.current.data.config.settings);
            //add timezone information
            var time = new Date();
            //set schema for generator that is in [0][0] position
            $scope.pipeline[0][0].settings["schema"] = angular.toJson($scope.schema);
            $scope.pipeline[0][0].settings["timeZone"]=time.getTimezoneOffset();
            //create configuration object from collected data
            var configuration = {
                pipelinePlugins: []
            };
            angular.forEach($scope.pipeline, function (pipelinePart) {
                angular.forEach(pipelinePart, function(plugin){
                    configuration.pipelinePlugins.push(plugin.name);
                    //each plugin in configuration is an array with selected options
                    if(configuration[plugin.name] === undefined){
                        configuration[plugin.name] = [];
                    }
                    var settings = angular.copy(plugin.settings);
                    delete settings.ranksNumber;
                    delete settings.showAdvanced;
                    configuration[plugin.name].push(settings);
                });
            });

            configuration.scheduler = {
                total: $scope.settings.amount.amount + " " + $scope.settings.amount.amountUnit,
                filesPerTask: $scope.settings.filesPerTask,
                filesPerDirectory: $scope.settings.filesPerDirectory,
                preserveOrder : $scope.settings.preserveOrder,
                cleanUp: $scope.settings.cleanUp,
                useTaskQueue: $scope.settings.useTaskQueue,
                checkpointPeriod: $scope.settings.checkpointPeriod,
                continuousCleanUp: $scope.settings.continuousCleanUp ,
                useStorerPipelineOptimization: $scope.settings.useStorerPipelineOptimization,
                useProgressiveStorersCount: $scope.settings.useProgressiveStorersCount,
                requirePipelineTasksOnSameNode : $scope.settings.requirePipelineTasksOnSameNode
            };

            if($scope.settings.speedLimiter){
                configuration.scheduler.speed = $scope.settings.speed.speedAmount + " " +
                    $scope.settings.speed.speedAmountUnit + "/" +
                    $scope.settings.speed.speedTimeAmount  + " " +
                    $scope.settings.speed.speedTimeUnit
            }
            var rankCnt = 1;
            var plugins = {};
            var ranks = {};
            angular.forEach($scope.pipeline, function (pipelinePart) {
                angular.forEach(pipelinePart, function (plugin) {
                    if(plugin.settings.ranksNumber > 0){
                        var ranksArray = [];
                        for(var i = 0; i < plugin.settings.ranksNumber; i++){
                            ranksArray[i] = rankCnt;
                            ranks[rankCnt] = {
                                plugins: [plugin.name]
                            };
                            rankCnt++;
                        }
                        plugins[plugin.name] = {
                            ranks : ranksArray
                        }
                    }
                })
            });

            configuration.scheduler.plugins = plugins;
            configuration.scheduler.ranks = ranks;
            configuration.cluster = {
                size : $scope.settings.clusterSize
            };
            configuration = angular.toJson(configuration);
            //start generation job with created configuration
            runnerSvc.start(configuration);
        };


        /**
         * Control function for pause/play operation when user clicks play/pause button.
         * Function calls pause() function in runnerSvc.
         */
        $scope.pausePlayJob = function(){
            runnerSvc.pause();
        };

        /**
         * Control function for stop operation when user clicks stop button.
         * Function calls pause() function in runnerSvc.
         */
        $scope.stopJob = function(){
            runnerSvc.stop();
        };

        /*
        When value of monitoring texts is changed redraw monitoring box
         */
        $scope.$watch('runner.monitoringContent.data.text',function (newTexts) {
            if(newTexts && newTexts != undefined){
                $('#monitoring').empty();
                var htmlDiv;
                for(var i = newTexts.length - 1; i >= 0; i--){
                    if(newTexts[i].indexOf("ERROR") === -1){
                        htmlDiv = '<div class="monitoringInfo">' + newTexts[i] + '</div>';
                    }
                    else {
                        htmlDiv = '<div class="monitoringError">' + newTexts[i] + '</div>';
                    }
                    var e = $(htmlDiv);
                    $('#monitoring').append(e);
                }
            }
        },true)
       

    }])
    /**
     * Filter for converting seconds to more readable form.
     * @return text converted from number of seconds
     */
    .filter('secondsToText', function($filter) {
        return function(seconds) {
            if (seconds > 86400){
                var numDays = Math.floor(seconds / 86400);
                var numHours = Math.floor((seconds % 86400) / 3600);
                var numMinutes = Math.floor(((seconds % 86400) % 3600) / 60);
                var numSeconds = ((seconds % 86400) % 3600) % 60;
                return numDays + " days, " + numHours + ":" + numMinutes + ":" + numSeconds;
            }
            else{
                return $filter('date')(new Date(0, 0, 0).setSeconds(seconds), 'HH:mm:ss');
            }
        };
    })
    /**
     * Filter for converting bytes to kB, MB and GB.
     * @return text with proper units converted from number of bytes
     */
    .filter('bytesToText', function(){
        return function(bytes) {
            if(bytes >= 0 && bytes < 1024){
                return bytes.toFixed(2) + " bytes";
            }
            if(bytes >= 1024 && bytes < 1048576){
                bytes = bytes / 1024;
                return bytes.toFixed(2) + " kB";
            }
            else if(bytes >= 1048576 && bytes < 1073741824){
                bytes = bytes / 1048576;
                return bytes.toFixed(2) + " MB";
            }
            else{
                bytes = bytes / 1073741824;
                return bytes.toFixed(2) + " GB";
            }
        };
    });