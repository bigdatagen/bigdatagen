angular.module('BDGApp')

/**
 * Directive for Basic Types. These are rendered just as rectangle with possible propertyName and type.
 */
.directive('basicType', ['typeSvc',
    function (typeSvc) {
        "use strict";
        return {
            restrict: 'EA',
            replace: false,
            templateUrl: "src/Components/BasicType/BasicType.tpl.html",
            scope: {
                //Definition of the type's content
                input: "=",
                //Object which contains information about active type in current editor.
                //It is used for making element active.
                activeProperty: "=",
                //If true, typeName is visualized instead of propertyName. If true, element cannot be deleted.
                isRoot: "=",
                //If true, propertyName is not visualized.
                inArray: "=",
                //Delete operation callback.
                onDelete: "&"
            },
            link: function ($scope, element) {

                $scope.hovered = false;

                element.on("mouseenter", function (e) {
                    e.stopPropagation();
                    $scope.hovered = true;
                    element.parent().trigger("mouseleave");
                    $scope.$apply();
                });

                element.on("mouseleave", function () {
                    $scope.hovered = false;
                    element.parent().trigger("mouseenter");
                    $scope.$apply();
                });

                /**
                 * On initialization it needs to get the label of containing type.
                 */
                function init() {
                    $scope.typeLabel = typeSvc.getTypeLabel($scope.input.type);
                }

                /**
                 * Callback function which deletes the element from its parent.
                 */
                $scope.delete = function () {
                    if (typeof $scope.onDelete === "function") {
                        $scope.onDelete();
                    }
                };

                /**
                 * Callback function which sets the element as active in $scope.activeProperty object
                 */
                $scope.makeActive = function () {
                    $scope.activeProperty.property = $scope.input;
                    $scope.activeProperty.inArray = $scope.inArray;
                };

                /**
                 * Checks whether the element should be visualized as active or not.
                 */
                $scope.$watch("activeProperty.property", function (newValue) {
                    if (newValue === $scope.input) {
                        $scope.active = true;
                    }
                    else {
                        $scope.active = false;
                    }
                });

                /**
                 * When the input is changed, directive should be re-initialized
                 */
                $scope.$watch("input", function () {
                    init();
                });
            }
        };
    }
]);