'use strict';
/**
 * Definition of ErrorModalCtrl controller. this is the controller for modal window that appears with error message.
 * The only functions of this controller is to close this window after "Ok" button is clicked and to hold message that
 * populates this window.
 */
angular.module('BDGApp').controller('ErrorModalCtrl',['$scope', '$uibModalInstance', 'message', function($scope, $uibModalInstance, message){
    $scope.message = message;

    /**
     * Hides this modal window after "Ok" button is clicked
     */
    $scope.ok = function (){
        $uibModalInstance.close();
    }
}]);