'use strict';

// Declare app level module which depends on views, and components
angular.module('BDGApp', [
    'ui.router',
    'ui.bootstrap',
    'BDGApp.GrammarEditor',
    'BDGApp.JsonEditor',
    'BDGApp.CsvEditor',
    'BDGApp.Pipeline',
    'BDGApp.Settings',
    'BDGApp.Runner',
    'BDGApp.Home'
])

.config(function($stateProvider, $urlRouterProvider, $httpProvider, configuration) {
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

    //CONFIGURATION for each type of editor
    var appConfig = configuration;

    // For any unmatched url, redirect to /main
    $urlRouterProvider.otherwise("/main");

    // Now set up the states
    $stateProvider
        //root state that holds configuration of BDG application
        .state('home', {
            templateUrl: "src/Components/Home/Home.tpl.html",
            controller: "HomeCtrl",
            data:{
                config: {
                    schema: undefined,
                    pipeline: undefined,
                    settings: undefined,
                    plugins: undefined
                },
                appConfig : appConfig
            }
        })
        //main state with initial settings
        .state('home.main', {
            url: "/main",
            templateUrl: "src/Components/Main/Main.tpl.html",
            controller: "MainCtrl"
        })
        //pipeline state
        .state('home.pipeline', {
            templateUrl: "src/Components/Pipeline/PipelineCtrl.tpl.html",
            params: {
                generator: undefined
            },
            controller: "PipelineCtrl"
        })
        //settings state
        .state('home.settings',{
            templateUrl: "src/Components/Settings/SettingsCtrl.tpl.html",
            controller: "SettingsCtrl"
        })
        //runner state
        .state('home.runner',{
            templateUrl: "src/Components/Runner/RunnerCtrl.tpl.html",
            controller: "RunnerCtrl"
        });
    //setup for configurable states
    angular.forEach(appConfig, function (format) {
        $stateProvider.state('home.' + format.name,
            {
                url: "/" + format.name,
                templateUrl: format.template,
                controller: format.controller,
                params: {
                    schema: undefined
                }
            }
        );
    });
})
/**
 * Definition of LoginCtrl. This is the controller for Login page. Contains definition and data from login form and
 * functions for log in/out operations. The login form is created by "Schema Form" framework.
 */
.controller('LoginCtrl', ['$scope', '$http','$timeout', 'logoutSvc', '$uibModal', function($scope, $http, $timeout, logoutSvc, $uibModal){
    //indication of successfull log in/out
    this.loginSuccess= undefined;
    var that = this;
    //error message after unsuccessful login
    $scope.error = "";

    //schema for login schemaform
    $scope.schema= {
        type: "object",
        properties:{
            username:{
                title: "Username",
                type: "string",
                default: ""
            },
            password:{
                title: "Password",
                type: "string",
                default: ""
            }
        }
    };

    //form for login schemaform
    $scope.form= [
        {
            "key": "username",
            "disableSuccessState": true,
            "feedback": false
        },
        {
            "key": "password",
            "disableSuccessState": true,
            "feedback": false,
            "type": "password"
        },
        {
            "type": "submit",
            "style": "btn-info",
            "title": "Log in"
        }
    ];

    //model for login schemaform
    $scope.model={};

    /**
     * Periodic checking if user is logged in.
     * Called every 10 seconds after successful log in.
     */
    var checkLogin = function(){
        $http({
            method: 'POST',
            url: '/check-login/'
        }).then(
            function success(response) {
                if(response.data.result == true){
                    that.loginSuccess = true;
                    $scope.error = "";
                    //check login status every 10 sec
                    $timeout(function () {
                        checkLogin();
                    }, 10000);
                }
                else {
                    that.loginSuccess = false;
                }
            },
            function error(reason) {
                openModal("Failed to contact server for login check");
            }
        );
    };

    /**
     * Logout function that sets the login indication to false.
     * This function is supposed to be called in logoutSvc
     */
    $scope.logout = function () {
        that.loginSuccess = false;
    };

    //registration of logout function
    logoutSvc.registerLogout($scope.logout);

    /**
     * Initialization of LoginCtrl. First checks whether the login is required.
     * If login is required sets the loginSuccess to false and login form appears.
     */
    var init = function(){
        $http({
            method: 'POST',
            url: '/requires-login/'
        }).then(
            function success(response) {
                if(response.data.result == false){
                    that.loginSuccess = true;
                    $scope.error = "";
                }
                else {
                    checkLogin();
                }
            },
            function error(reason) {
                openModal("Failed to contact server for login status");
            }
        );
    };

    init();

    /**
     * Submit function called after submitting the login form.
     * If login is not successful than the error message is assigned to $scope.error
     */
    $scope.onSubmit = function(){
        $http({
            method: 'POST',
            url: '/login/',
            data: {
                username: $scope.model.username,
                password: $scope.model.password
            }
        }).then(
            function success(response) {
                if(response.data.result == "success"){
                    that.loginSuccess = true;
                    $scope.error = "";
                    $scope.model={};
                    $timeout(function () {
                        checkLogin();
                    }, 10000);
                }
                else {
                    $scope.error = "Wrong username or password!"
                }
            },
            function error(reason) {
                openModal("Failed to contact server for credential check.");
            }
        );
    };

    /**
     * Shows modal window with information about error state. Contains definition for
     * $uibModal bootstrap service for opening the modal window.
     * @param message - error message that populates the modal window
     */
    var openModal = function (message) {

        var modalInstance = $uibModal.open({
            templateUrl: 'src/Components/ErrorModal/ErrorModal.tpl.html',
            controller: 'ErrorModalCtrl',
            resolve: {
                message: function () {
                    return message;
                }
            }
        });
    };
}]);
/*
   Configuration of custom template for array in schemaform. Default schemaform template for array
   doesn't support minItems property of array.
 */
angular.module('schemaForm').config(['schemaFormDecoratorsProvider', function(decoratorsProvider) {
    decoratorsProvider.addMapping(
        'bootstrapDecorator',
        'array',
        'src/Components/SchemaFormTemplates/array.tpl.html'
    )
}
]);
