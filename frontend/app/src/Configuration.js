'use strict';
/**
 * Constant with configuration for each file type editor.
 * Each configuration should contain this information:
 *   name: "custom", //Name of state for editor
 *   template: "src/Path/To/CustomEditorCtrl.tpl.html", //Template url
 *   controller: "CustomEditorCtrl", //Name of editor’s wrap controller
 *   onPageName: "Custom", //Displayed text on related icon on main page
 *   generator: "CustomGenerator", //Name of plugin at backend which should be used
 *   inferer: "CsvInferer" //Name of inferrer plugin which should be used [optional]
 */
angular.module('BDGApp').constant('configuration',[
    {
        name: "json",
        template: "src/Components/JsonEditor/JsonEditorCtrl.tpl.html",
        controller: "JsonEditorCtrl",
        onPageName: "JSON",
        generator: "JsonGenerator",
        inferer: "JsonInferer"

    },
    {
        name: "csv",
        template: "src/Components/CsvEditor/CsvEditorCtrl.tpl.html",
        controller: "CsvEditorCtrl",
        onPageName: "CSV",
        generator: "CsvGenerator",
        inferer: "CsvInferer"
    },
    {
        name: "grammar",
        template: "src/Components/GrammarEditor/GrammarEditorCtrl.tpl.html",
        controller: "GrammarEditorCtrl",
        onPageName: "Grammar",
        generator: "GrammarGenerator"
    }
]);