/**
 * Service for maintaining all information about UI components (buttons, graphs, texts) in Runner page and state of generation job.
 * All the data is stored in object "runner" of this service. This object is exposed and all contorllers
 * injected with this service can access it. Service also handles communication with server: periodical
 * check of generation status, start/play/pause/stop operations. Service also recounts statistical values
 * according to the current generation speed. This recounting is based on a state. If generation speed
 * during generation job exceeds certain limit the state is changed and graph data are recounted. States are numbered 1,2,3 and 4.
 * 1 -> statistic is in bytes/s
 * 2 -> statistic is in kB/s
 * 3 -> statistic is in MB/s
 * 4 -> statistic is in GB/s
 * State change is only one way: 1->2->3->4.
 * Limits are set as follows (values are in bytes/s):
 * state 1: 0 <= speed < 10 000
 * state 2: 10 000 <= speed < 10 000 000
 * state 3: 10 000 000 <= speed < 10 000 000 000
 * state 4: 10 000 000 000 <= speed
 * @return object with functions start(), pause(), stop(), reset() functions and data object "runner"
 */
angular.module('BDGApp').factory('runnerSvc', ['$http', '$timeout',
    function ($http, $timeout) {
        'use strict';

        //base object with all information about runner exposed by this service
        var runner = {};
        var url = '/plugin/';
        var mutex = false;
        var avgSpeedSizeTitleTpl = "Average data generation speed";
        var avgSpeedSizeWorkerTitleTpl = "Average data generation speed per worker";
        var avgSpeedFilesTitleTpl = "Average files generation speed ";
        var avgSpeedFilesWorkerTitleTpl = "Average files generation speed per worker";
        var responseData = {};

        //general potions for all graphs
        var graphOptions = {
            animation: {
                startup: true
            },
            colors:['#428bca'],
            width: 700,
            height: 300,
            legend: {position: 'none'},
            chartArea : {
                left: 100,
                right: 50
            },
            hAxis: {
                title: 'Time',
                titleTextStyle: {
                    color: '#FFFFFF'
                },
                gridlines: {
                    count: -1,
                    color: '#FFFFFF'
                },
                textStyle: {
                    color: '#FFFFFF'
                }
            },
            backgroundColor : "#5A5654",
            titleTextStyle: {
                color: '#FFFFFF'
            }
        };

        /**
         * Resets all information about UI components to initial values.
         * This function is automatically called within start() function
         * and within initialization of this service.
         */
        function reset() {
            runner.jobId = 0;
            runner.state = "No job started";
            runner.started = false;
            runner.timeCnt = 0;
            runner.devider = 1;
            runner.unitsState = 1;
            runner.units = " bytes/s";
            runner.progress = 0;
            runner.generatedFiles = 0;
            runner.generatedSize = 0;
            runner.totalTasks = 0;
            runner.errorTasks = 0;
            runner.paused = false;
            runner.monitoringContent = {
                data: {
                    text: []
                }
            };
            //initital value of an instant statistic
            responseData.avgSpeedSize = 0;
            responseData.avgSpeedSizePerWorker = 0;
            responseData.avgSpeedFiles = 0;
            responseData.avgSpeedFilesPerWorker = 0;
            responseData.progress = 0;
            responseData.generatedFiles = 0;
            responseData.generatedSize = 0;
            responseData.totalTasks = 0;
            responseData.errorTasks = 0;
            //initial state of buttons
            runner.blockStart = false;
            runner.blockPlay = true;
            runner.blockStop = true;
            //initial value of graphs statistic
            runner.avgSpeedSize = {};
            runner.avgSpeedSize.value = 0;
            runner.avgSpeedSize.lineChart = {};
            runner.avgSpeedSize.lineChart.type = "LineChart";
            runner.avgSpeedSize.lineChart.options = {
                title : avgSpeedSizeTitleTpl,
                vAxis: {
                    title: 'Data',
                    titleTextStyle: {
                        color: '#FFFFFF'
                    },
                    gridlines: {
                        count: -1,
                        color: '#FFFFFF'
                    },
                    textStyle: {
                        color: '#FFFFFF'
                    }
                }
            };
            angular.extend(runner.avgSpeedSize.lineChart.options, graphOptions);
            runner.avgSpeedSize.lineChart.data = [
                ['Time', 'Data'],
                [0,0]
            ];

            runner.avgSpeedSizeWorker = {};
            runner.avgSpeedSizeWorker.value = 0;
            runner.avgSpeedSizeWorker.lineChart = {};
            runner.avgSpeedSizeWorker.lineChart.type = "LineChart";
            runner.avgSpeedSizeWorker.lineChart.options = {
                title : avgSpeedSizeWorkerTitleTpl,
                vAxis: {
                    title: 'Data',
                    titleTextStyle: {
                        color: '#FFFFFF'
                    },
                    gridlines: {
                        count: -1,
                        color: '#FFFFFF'
                    },
                    textStyle: {
                        color: '#FFFFFF'
                    }
                }
            };
            angular.extend(runner.avgSpeedSizeWorker.lineChart.options, graphOptions);
            runner.avgSpeedSizeWorker.lineChart.data = [
                ['Time', 'Data'],
                [0,0]
            ];

            runner.avgSpeedFiles = {};
            runner.avgSpeedFiles.value = 0;
            runner.avgSpeedFiles.lineChart = {};
            runner.avgSpeedFiles.lineChart.type = "LineChart";
            runner.avgSpeedFiles.lineChart.options = {
                title : avgSpeedFilesTitleTpl,
                vAxis: {
                    title: 'Files',
                    titleTextStyle: {
                        color: '#FFFFFF'
                    },
                    gridlines: {
                        count: -1,
                        color: '#FFFFFF'
                    },
                    textStyle: {
                        color: '#FFFFFF'
                    }
                }
            };
            angular.extend(runner.avgSpeedFiles.lineChart.options, graphOptions);
            runner.avgSpeedFiles.lineChart.data = [
                ['Time', 'Files'],
                [0,0]
            ];

            runner.avgSpeedFilesWorker = {};
            runner.avgSpeedFilesWorker.value = 0;
            runner.avgSpeedFilesWorker.lineChart = {};
            runner.avgSpeedFilesWorker.lineChart.type = "LineChart";
            runner.avgSpeedFilesWorker.lineChart.options = {
                title : avgSpeedFilesWorkerTitleTpl,
                vAxis: {
                    title: 'Files',
                    titleTextStyle: {
                        color: '#FFFFFF'
                    },
                    gridlines: {
                        count: -1,
                        color: '#FFFFFF'
                    },
                    textStyle: {
                        color: '#FFFFFF'
                    }
                }
            };
            angular.extend(runner.avgSpeedFilesWorker.lineChart.options, graphOptions);
            runner.avgSpeedFilesWorker.lineChart.data = [
                ['Time', 'Files'],
                [0,0]
            ];

        }

        /**
         * Initialization function called when this service is created.
         */
        function init(){
            reset();
        }
        
        init();
        /**
         * Starts generation process. Accepts one parameter with configuration of whole
         * generation job (pipeline, schema, settings). Sends http start request to "PipelineLauncher"
         * with configuration data and after successful launch of job initiates periodical
         * check of status of generation job and sets the id od started job - "jobID".
         * @param configuration - configuration of generation job
         */
        function start(configuration) {
            reset();
            runner.state = "Starting";
            runner.blockStart = true;
            runner.blockPlay = true;
            runner.blockStop = true;
            updateMonitoring("Starting job", false);
            $http({
                method: 'POST',
                url: url + 'pipelinelauncher/',
                data: configuration
            }).then(
                function success(response) {
                    runner.jobId = response.data.jobid;
                    runner.avgSpeedSize.lineChart.options.title = avgSpeedSizeTitleTpl + " (in" + runner.units + ")";
                    runner.avgSpeedSizeWorker.lineChart.options.title = avgSpeedSizeWorkerTitleTpl + " (in" + runner.units + ")";
                    runner.avgSpeedFiles.lineChart.options.title = avgSpeedFilesTitleTpl + " (in files/s)";
                    runner.avgSpeedFilesWorker.lineChart.options.title = avgSpeedFilesWorkerTitleTpl + " (in files/s)";
                    $timeout(function () {
                        checkStatus();
                    }, 1000);
                },
                function error(reason) {
                    //if error occurs send message to monitoring box
                    updateMonitoring(reason.data,true);
                }
            );
        }

        /**
         * Handles operation for changing state of generation job from "running" to "paused" and vice-versa.
         * Sends http request to "PipelineLauncher" with "play" or "stop" command based on state of job and jobID.
         * If state of generation job is "running" sends "pause" command. If state is "paused" sends "play" command.
         * After the state of job is successfully changed UI components are updated.
         */
        function pause() {
            runner.blockStart = true;
            runner.blockPlay = true;
            runner.blockStop = true;
            var request = {
                jobid : runner.jobId
            };
            if(runner.paused){
                runner.state = "Resuming";
                updateMonitoring("Resuming job", false);
                request.command = "play";
                $http({
                    method: 'POST',
                    url: url + 'pipelinelauncher/',
                    data: request
                }).then(
                    function success(response){
                        //if error occurs send message to monitoring box
                        if(response.data.error != undefined && response.data.error != ""){
                            updateMonitoring(response.data.error,true);
                        }
                        if(response.data.state){
                            if(response.data.state == "running"){
                                runner.paused = false;
                                runner.blockPlay = false;
                                runner.blockStop = false;
                                updateMonitoring("Job " + runner.jobId + " resumed", false);
                                if(mutex){
                                    $timeout(500)
                                }
                                runner.state =  "Running";
                                $timeout(function () {
                                    checkStatus();
                                }, 1000);
                            }
                            else{
                                //if error occurs send message to monitoring box
                                updateMonitoring("State after play command is not running", true);
                            }
                        }
                        else {
                            //if error occurs send message to monitoring box
                            updateMonitoring(response.data.error, true);
                        }

                    },
                    function error(reason){
                        //if error occurs send message to monitoring box
                        updateMonitoring("Failed to contact server for resume operation", true);
                    }
                );
            }
            else {
                runner.state =  "Pausing";
                updateMonitoring("Pausing job", false);
                request.command = "pause";
                $http({
                    method: 'POST',
                    url: url + 'pipelinelauncher/',
                    data: request
                }).then(
                    function success(response){
                        //if error occurs send message to monitoring box
                        if(response.data.error != undefined && response.data.error != ""){
                            updateMonitoring(response.data.error,true);
                        }
                        if(response.data.state){
                            if(response.data.state == "paused"){
                                runner.paused = true;
                                runner.blockPlay = false;
                                runner.blockStop = false;
                                updateMonitoring("Job " + runner.jobId + " paused", false);
                                if(mutex){
                                    $timeout(500)
                                }
                                runner.state =  "Paused";
                            }
                            else{
                                //if error occurs send message to monitoring box
                                updateMonitoring("State after pause command is not paused", true);
                            }
                        }
                        else {
                            //if error occurs send message to monitoring box
                            updateMonitoring(response.data.error, true);
                        }
                    },
                    function error(reason){
                        //if error occurs send message to monitoring box
                        updateMonitoring("Failed to contact server for pause operation", true);
                    }
                );
            }
        }

        /**
         * Handles operation for changing state of generation job from "running" to "finished".
         * Sends http request to "PipelineLauncher" with "stop" command and jobID.
         * After the state of job is successfully changed UI components are updated.
         */
        function stop() {
            runner.blockStart = true;
            runner.blockPlay = true;
            runner.blockStop = true;
            runner.state = "Stopping";
            updateMonitoring("Stopping job", false);
            var request = {
                jobid : runner.jobId
            };
            request.command = "stop";
            $http({
                method: 'POST',
                url: url + 'pipelinelauncher/',
                data: request
            }).then(
                function success(response){
                    //if error occurs send message to monitoring box
                    if(response.data.error != undefined && response.data.error != ""){
                        updateMonitoring(response.data.error,true);
                    }
                    if(response.data.state){
                        if(response.data.state == "finished"){
                            runner.blockStart = false;
                            runner.blockPlay = true;
                            runner.blockStop = true;
                            updateMonitoring("Job " + runner.jobId + " stopped", false);
                            if(mutex){
                                $timeout(500)
                            }
                            runner.state =  "Finished";
                        }
                        else{
                            //if error occurs send message to monitoring box
                            updateMonitoring("State after stop command is not finished", true);
                        }
                    }
                    else {
                        //if error occurs send message to monitoring box
                        updateMonitoring(response.data.error, true);
                    }

                },
                function error(reason){
                    //if error occurs send message to monitoring box
                    updateMonitoring("Failed to contact server for stop operation", true);
                }
            );
        }

        /**
         *  Periodical check of status of generation job. This function sends http request
         *  to "PipelineLauncher" every second with "status" command and jobID. When response is
         *  received the numerical and text values of UI components are updated.
         */
        function checkStatus(){
            mutex = true;
            var request = {
                command: "status",
                jobid: runner.jobId
            };
            $http({
                method: 'POST',
                url: url + 'pipelinelauncher/',
                data: request
            }).then(
                function success(response) {
                    //if error occurs send message to monitoring box
                    if(response.data.error != undefined && response.data.error != ""){
                        updateMonitoring(response.data.error,true);
                    }
                    if(response.data.state == "running"){
                        if(response.data.errors.indexOf("Could not read job status") === -1){
                            if(!runner.started){
                                updateMonitoring("Job " + runner.jobId + " started", false);
                                runner.started = true;
                            }
                            runner.blockPlay = false;
                            runner.blockStop = false;
                            runner.state = "Running";
                            /*
                             If response contains numerical data save them.
                             This prevents UI from showing NaN values when error occurs and UI components
                             can't be updated with new data.
                             */
                            if(response.data.avgSpeedSize){
                                responseData = angular.copy(response.data);
                            }
                            //update UI components data with response
                            update(responseData);
                            //if error occurs send message to monitoring box
                            if(response.data.errors && (response.data.errors.length != 0)){
                                angular.forEach(response.data.errors, function (message) {
                                    updateMonitoring(message, true);
                                });
                            }
                        }
                        $timeout(function () {
                            checkStatus();
                        }, 1000);
                    }
                    if (response.data.state == 'finished') {
                        runner.state = "Finished";
                        updateMonitoring("Job " + runner.jobId + " finished", false);
                        runner.timeCnt++;
                        runner.blockStart = false;
                        runner.blockPlay = true;
                        runner.blockStop = true;
                        if(response.data.avgSpeedSize){
                            responseData = angular.copy(response.data);
                        }
                        update(responseData);
                        //if error occurs send message to monitoring box
                        if(response.data.errors && (response.data.errors.length != 0)){
                            angular.forEach(response.data.errors, function (message) {
                                updateMonitoring(message, true);
                            });
                        }
                    }
                },
                function error(reason) {
                    //if error occurs send message to monitoring box
                    updateMonitoring("Failed to contact server for server status check", true);
                }
            );
            mutex = false;
        }

        /**
         * Updates data of UI components. Function accepts one parameter data contains
         * response object from server with new statistic values. This object is parsed
         * and individual parts of UI components are updated.
         * @param data - object with new numerical data for UI components update
         */
        function update(data) {
            runner.timeCnt = parseInt(data.totalTime);
            changeState(data.avgSpeedSize);
            setAvgSpeedSize(data.avgSpeedSize);
            setAvgSpeedSizeWorker(data.avgSpeedSizePerWorker);
            setAvgSpeedFiles(data.avgSpeedFiles);
            setAvgSpeedFilesWorker(data.avgSpeedFilesPerWorker);
            runner.progress = parseInt(data.progress);
            runner.generatedFiles = parseInt(data.generatedFiles);
            runner.generatedSize = parseInt(data.generatedSize);
            runner.totalTasks = parseInt(data.totalTasks);
            runner.errorTasks = parseInt(data.errorTasks);
        }

        /**
         * Changes state if current speed of generation exceeds one of defined limits.
         * @param avgSpeedSize - current generation speed
         */
        function changeState(avgSpeedSize) {
            var number = parseFloat(avgSpeedSize);
            //number is between 10000 and 10000000
            if(number > 10000 && number <= 10000000){
                if(runner.unitsState < 2){
                    runner.unitsState = 2;
                    runner.units = " kB/s";
                    runner.devider = 1024;
                    recountStatistic();
                }
            }
            //number is between 10000000 and 10000000000
            else if(number > 10000000 && number <= 10000000000){
                if(runner.unitsState < 3){
                    runner.unitsState = 3;
                    runner.units = " MB/s";
                    runner.devider = 1048576;
                    recountStatistic();
                }
            }
            //number is bigger than 10000000000
            else if(number > 10000000000){
                if(runner.unitsState < 4){
                    runner.unitsState = 4;
                    runner.units = " GB/s";
                    runner.devider = 1073741824;
                    recountStatistic();
                }
            }
        }

        /**
         * Recounts statistical data of graphs from section Data generation speed
         * in Runner page UI.
         */
        function recountStatistic(){
            var i;
            var num;
            var data = runner.avgSpeedSize.lineChart.data;
            for(i=1; i< data.length; i++){
                num = data[i][1] / runner.devider;
                num = num.toFixed(2);
                data[i][1] = parseFloat(num);
            }
            data = runner.avgSpeedSizeWorker.lineChart.data;
            for(i=1; i< data.length; i++){
                num = data[i][1] / runner.devider;
                num = num.toFixed(2);
                data[i][1] = parseFloat(num);
            }
            runner.avgSpeedSize.lineChart.options.title = avgSpeedSizeTitleTpl + " (in" + runner.units + ")";
            runner.avgSpeedSizeWorker.lineChart.options.title = avgSpeedSizeWorkerTitleTpl + " (in" + runner.units + ")";
        }

        /**
         * Updates statistical data and current value for average data speed.
         * @param value - new current value
         */
        function setAvgSpeedSize(value){
            var number = parseFloat(value) / runner.devider;
            number = number.toFixed(2);
            runner.avgSpeedSize.value = number + runner.units;
            var tmp = [runner.timeCnt, parseFloat(number)];
            runner.avgSpeedSize.lineChart.data.push(tmp)
        }

        /**
         * Updates statistical data and current value for average data speed per worker.
         * @param value - new current value
         */
        function setAvgSpeedSizeWorker(value){
            var number = parseFloat(value) / runner.devider;
            number = number.toFixed(2);
            runner.avgSpeedSizeWorker.value = number + runner.units;
            var tmp = [runner.timeCnt, parseFloat(number)];
            runner.avgSpeedSizeWorker.lineChart.data.push(tmp)
        }

        /**
         * Updates statistical data and current value for average files speed.
         * @param value - new current value
         */
        function setAvgSpeedFiles(value){
            var number = parseFloat(value);
            number = number.toFixed(2);
            runner.avgSpeedFiles.value = number + " files/s";
            var tmp = [runner.timeCnt, number];
            runner.avgSpeedFiles.lineChart.data.push(tmp)
        }

        /**
         * Updates statistical data and current value for average files speed per worker.
         * @param value - new current value
         */
        function setAvgSpeedFilesWorker(value){
            var number = parseFloat(value);
            number = number.toFixed(2);
            runner.avgSpeedFilesWorker.value = number + " files/s";
            var tmp = [runner.timeCnt, number];
            runner.avgSpeedFilesWorker.lineChart.data.push(tmp)
        }

        /**
         * Updates monitoring box. Accepts two parameters with new message and boolean value
         * indicating that message is error.
         * @param message - new message
         * @param isError - if message is error set true
         */
        function updateMonitoring(message, isError) {
            if(message != undefined){
                if(message.indexOf("Could not read job status") === -1){
                    var time = new Date();
                    var timeStr = time.toLocaleString();
                    if (isError){
                        message = timeStr + " ERROR: "  + message;
                    }
                    else {
                        message = timeStr + " INFO: " + message;
                    }
                    runner.monitoringContent.data.text.push(message);
                }
            }
        }

        return {
            start: start,
            pause: pause,
            stop: stop,
            reset: reset,
            runner: runner
        };
    }
]);