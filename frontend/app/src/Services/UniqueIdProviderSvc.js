/**
 * Service which returns unique ID.
 */
angular.module('BDGApp').factory('uniqueIdProviderSvc', [
    function () {
        'use strict';

        var count = 0;

        /**
         * Provides unique id - each function call increments number in ID
         * @returns {string}
         */
        function getId() {
            return "element_" + count++;
        }

        return {
            getId: getId
        };
    }
]);