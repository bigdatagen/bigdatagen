/**
 * Services for downloading files generated from variables.
 */
angular.module('BDGApp').factory('fileDownloadSvc', ['$window',
    function ($window) {
        'use strict';

        var textFile = null;

        /**
         * Downloads the generated JSON file with specified name and content.
         * This function creates a blob in browser memory which is then downloaded
         * @param content JavaScript object which is stringified into JSON
         * @param filename Name of the downloaded file.
         */
        function download(content, filename) {
            var blob = new Blob([angular.toJson(content)], {type: 'application/json'});

            var ua = window.navigator.userAgent;
            var IE = ua.indexOf("MSIE ") > 0 || !!ua.match(/Trident.*rv\:11\./);

            if(IE) {
                window.navigator.msSaveOrOpenBlob(blob, filename);
            }
            else {
                // If we are replacing a previously generated file we need to
                // manually revoke the object URL to avoid memory leaks.
                if (textFile !== null) {
                    $window.URL.revokeObjectURL(textFile);
                }
                textFile = $window.URL.createObjectURL(blob);

                var link = angular.element('<a download="' + filename + '" style="display: none"></a>');
                link.attr("href", textFile);
                angular.element("body").append(link);
                link.get(0).click();
                link.remove();
            }
        }

        return {
            download: download
        };
    }
]);