/**
 * Service for easier manipulation with editor types
 */
angular.module('BDGApp').factory('typeSvc', [
    function () {
        'use strict';
        var typeArray;

        /**
         * Initialization function which sets the right set of types.
         * Each editor should call this method when initialized
         * @param types Array of types
         */
        function init(types) {
            typeArray = types;
        }

        /**
         * Returns label of given type.
         * For implicit types it returns their name from initialized type array.
         * For custom types it returns just the name of type.
         *
         * @param type Type name
         */
        function getTypeLabel (type) {
            var resultName;
            angular.forEach(typeArray, function (value) {
                if (value.type === type) {
                    resultName = value.label;
                }
            });

            if (resultName === undefined) {
                return type;
            }
            else {
                return resultName;
            }
        }

        return {
            init: init,
            getTypeLabel : getTypeLabel
        };
    }
]);