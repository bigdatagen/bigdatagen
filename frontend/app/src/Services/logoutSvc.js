/**
 * Logout operation implemented as a service. As a service logout function is exposed to controllers
 * that are injected with logoutSvc.
 * @return  object with registerLogout() and callLogout() functions
 */
angular.module('BDGApp').factory('logoutSvc', [
    function () {
        'use strict';
        var logout;

        /**
         * Registration of logout function
         * @param f - function implementing logout operation
         */
        function registerLogout(f) {
            logout = f;
        }

        /**
         * Execution of registered logout function
         */
        function callLogout() {
            if (typeof logout === 'function') {
                logout();
            }
        }

        return {
            registerLogout: registerLogout,
            callLogout: callLogout
        };
    }
]);